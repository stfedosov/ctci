package somecompany8;

/*
'(', '{', '[' are called "openers".
')', '}', ']' are called "closers".
Write an efficient function that tells us whether input string's openers and closers are properly nested.


Examples:
"{ [ ] ( ) }" -> true
"{ [ ( ] ) }" -> false
"{ [ }" -> false
"class Example { public do() { return; } }" -> true
"class Example { public do( { return; } }" -> false

=====

Add support for a new pair of opener/closer: <, >

=====

Add support for 3 self-closing parentheses: " ' |

|| -> true
'| -> false

*/

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Solution {
    public static void main(String args[] ) throws Exception {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT */
        assert !isValid("(((");
        assert isValid("(())");
        assert !isValid("{");
        assert !isValid(")(");
        assert isValid("");
        assert isValid("{([])}");
        assert !isValid("[{]");
        assert isValid("{abc[,,!8]}");
        assert !isValid("{abc[efkmww}");
        assert isValid("<ooo>");
        assert !isValid(">.<");
        assert !isValid("[|]");
        assert isValid("||");
        System.out.println("PASSED!");
    }

    public static boolean isValid(String input) {
        if (input == null) {
            throw new RuntimeException("Invalid input");
        }
        Map<Character, Character> mapping = new HashMap<>();
        mapping.put('(', ')');
        mapping.put('{', '}');
        mapping.put('<', '>');
        mapping.put('[', ']');

        Map<Character, Character> mapping2 = new HashMap<>();
        mapping2.put('|', '|');

        // [|]
        // push | -> leave it on the stack, push [, leave it, check ] -> remove [], -> push | check for peek, remove ||
        // push [ -> leave it on the stack, push |, leave it,  return false;

        Stack<Character> stack = new Stack<>();
        for (char c : input.toCharArray()) {
            if (mapping.containsKey(c) || mapping2.containsKey(c)) {
                stack.push(c);
            } else if (mapping.values().contains(c) || mapping2.containsKey(c)) {
                if (stack.isEmpty()) return false;
                if (mapping.get(stack.peek()) == c || mapping2.get(stack.peek()) == c) {
                    stack.pop();
                } else {
                    return false;
                }
            }
        }

        return stack.isEmpty();
        //[']' -> false
    }



}