package Chapter10;

public class MyBitSet {

    private byte[] bits;

    public MyBitSet(int size) {
        this.bits = new byte[(size << 5) + 1];
    }

    public void setBit(int n) {
        bits[n << 5] |= (1 << (n % 32));
    }

    public boolean getBit(int n) {
        return ((bits[n << 5] & (1 << (n % 32))) != 0);
    }

}