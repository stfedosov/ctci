package Leetcode4;

import java.util.HashMap;
import java.util.Map;

public class CountGoodSubarrays {

    public static void main(String[] args) {

    }

    /*
     1. now our left pointer pointing to an element where we have window
        which has less than k pairs
     2. but before that left we can form left number of valid subarrays
     3. subarrays are [0, right], [1, right], [2 , right]..... till [left, right]
     4. so adding left to the result
     */
    public static long countGood(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        int start = 0, pairs = 0;
        long result = 0;
        for (int end = 0; end < nums.length; end++) {
            pairs += map.getOrDefault(nums[end], 0);
            map.put(nums[end], map.getOrDefault(nums[end], 0) + 1);
            while (pairs >= k) {
                map.put(nums[start], map.get(nums[start]) - 1);
                pairs -= map.get(nums[start]);
                start++;
            }
            result += start;
        }
        return result;
    }

}
