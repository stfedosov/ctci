package Leetcode4;

import java.util.ArrayList;
import java.util.List;

public class LexicalOrder {

    public static void main(String[] args) {
        System.out.println(lexicalOrder(10)); // [1,10,11,12,13,2,3,4,5,6,7,8,9]
        System.out.println(lexicalOrderIterative(21)); // [1,10,11,12,13,2,3,4,5,6,7,8,9]
        System.out.println(lexicalOrder(2)); // [1,2]
    }

    // We continue dfs, always trying to go deeper first, and then moving to the next item when we can't go deeper.
    //                           1             2             3
    //                      / / | | \ \   / / | | \ \    / / | | \ \
    //                     10 11 ...  19  20 21 ... 29       ...
    //                / / | | \ \
    //               100 101 ... 109
    public static List<Integer> lexicalOrder(int n) {
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i <= 9; i++) {
            dfs(i, n, list);
        }
        return list;
    }

    // Space Complexity: O(log10(n))
    // The depth of recursion is proportional to the number of digits D in N.
    // If the maximum value for N is 50,000, the maximum number of digits D is 5

    // Time Complexity: O(n)
    private static void dfs(int current, int limit, List<Integer> list) {
        if (current > limit) {
            return;
        }
        list.add(current);
        for (int nextDigit = 0; nextDigit <= 9; nextDigit++) {
            int next = current * 10 + nextDigit;
            if (next <= limit) {
                dfs(next, limit, list);
            } else {
                break;
            }
        }
    }

    // iterative approach

    public static List<Integer> lexicalOrderIterative(int n) {
        List<Integer> res = new ArrayList<>();
        int current = 1;
        // Generate numbers from 1 to n
        for (int i = 0; i < n; i++) {
            res.add(current);
            // If multiplying the current number by 10 is still within the limit, do it
            // (This condition checks if we can add a subfolder)
            if (current * 10 <= n) {
                current *= 10;
            } else {
                // This checks if we're at the last item in the current folder (ends with 9) or if we've reached or exceeded our limit n.
                // If either of these is true, we go up a level by dividing by 10.
                // This is like closing the current folder and moving back to its parent.
                //
                // After going up as many levels as needed, we increment by 1.
                // This is like moving to the next item in the current folder.
                while (current % 10 == 9 || current >= n) {
                    current /= 10; // Remove the last digit
                }
                current++;
            }
        }
        return res;
    }

}
