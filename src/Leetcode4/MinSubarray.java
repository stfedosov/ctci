package Leetcode4;

import java.util.HashMap;
import java.util.Map;

public class MinSubarray {

    public static int minSubarray(int[] nums, int p) {
        int sum = 0;
        for (int num : nums) {
            sum = (sum + num) % p;
        }
        int target = sum % p;
        if (target == 0) {
            return 0;
        }
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0, -1);
        int minLen = nums.length;
        int currentSum = 0;
        for (int i = 0; i < nums.length; i++) {
            currentSum = (currentSum + nums[i]) % p;
            int needed = (currentSum - target + p) % p;
            if (map.containsKey(needed)) {
                minLen = Math.min(minLen, i - map.get(needed));
            }
            map.put(currentSum, i);
        }
        return minLen == nums.length ? -1 : minLen;
    }

}
