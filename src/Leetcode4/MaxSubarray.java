package Leetcode4;

public class MaxSubarray {

    /*
    Consider a non-empty subarray from nums that has the maximum possible bitwise AND.

    Let k be the maximum value of the bitwise AND of any subarray of nums.
    Then, only subarrays with a bitwise AND equal to k should be considered.
    Return the length of the longest such subarray.

    The bitwise AND of an array is the bitwise AND of all the numbers in it.

    A subarray is a contiguous sequence of elements within an array.
     */
    public static void main(String[] args) {
        System.out.println(longestSubarray(new int[]{1, 2, 3, 3, 2, 2}));
    }

    // Max bitwise AND subarray must include maximum element(s) ONLY.
    // Therefore, we only need to find the longest subarray including max only.
    public static int longestSubarray(int[] nums) {
        int max = 0;
        for (int num : nums) {
            max = Math.max(max, num);
        }
        int count = 0, longest = 0;
        for (int num : nums) {
            if (num == max) {
                longest = Math.max(longest, ++count);
            } else {
                count = 0;
            }
        }
        return longest;
    }

}
