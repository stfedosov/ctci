package Leetcode4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GraphCycleDetection {

    // Method to check if a graph contains a cycle
    public boolean hasCycle(int[][] edges, int numNodes) {
        // Create adjacency list using a Map
        Map<Integer, List<Integer>> graph = new HashMap<>();
        for (int i = 0; i < numNodes; i++) {
            graph.put(i, new ArrayList<>());
        }

        // Fill the adjacency map
        for (int[] edge : edges) {
            int u = edge[0];
            int v = edge[1];
            graph.get(u).add(v);
            graph.get(v).add(u);
        }

        // To track visited nodes
        boolean[] visited = new boolean[numNodes];

        // Check for cycles in each component
        for (int i = 0; i < numNodes; i++) {
            if (!visited[i]) {
                if (dfs(i, -1, visited, graph)) {
                    return true;
                }
            }
        }
        return false;
    }

    // DFS method to detect a cycle
    private boolean dfs(int current, int parent, boolean[] visited, Map<Integer, List<Integer>> graph) {
        visited[current] = true;

        for (int neighbor : graph.get(current)) {
            if (!visited[neighbor]) {
                if (dfs(neighbor, current, visited, graph)) {
                    return true;
                }
            } else if (neighbor != parent) {
                return true; // Found a cycle
            }
        }

        return false;
    }

    public static void main(String[] args) {
        GraphCycleDetection graph = new GraphCycleDetection();
        int[][] edges = {{0, 1}, {1, 2}, {2, 0}}; // Example with a cycle
        int numNodes = 3;

        boolean hasCycle = graph.hasCycle(edges, numNodes);
        System.out.println("Graph contains cycle: " + hasCycle);
    }
}