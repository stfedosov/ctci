package Leetcode4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NumIslands2 {

    private final static int[][] directions = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

    public static void main(String[] args) {
        System.out.println(numIslands2(3, 3, new int[][]{{0, 0}, {0, 1}, {1, 2}, {2, 1}})); // 1, 1, 2, 3
    }

    // Time complexity: O(m * n)
    public static List<Integer> numIslands2(int m, int n, int[][] positions) {
        int[][] grid = new int[m][n];
        List<Integer> list = new ArrayList<>();
        UnionFind uf = new UnionFind(m, n);

        for (int[] pos : positions) {
            int row = pos[0];
            int col = pos[1];

            grid[row][col] = 1;

            // converting 2D array into 1D array, to determine the index of the element use this formula
            // index = row * number_of_columns + col
            int index = row * n + col;
            uf.add(index);

            for (int[] dir : directions) {
                int newRow = row + dir[0];
                int newCol = col + dir[1];
                if (newRow >= 0 && newRow < m && newCol >= 0 && newCol < n && grid[newRow][newCol] == 1) {
                    int neighborIndex = newRow * n + newCol;
                    uf.union(index, neighborIndex);
                }
            }

            list.add(uf.getComponents());
        }

        return list;
    }

    private static class UnionFind {
        int[] parent;
        int components;

        public UnionFind(int m, int n) {
            parent = new int[m * n];
            Arrays.fill(parent, -1);
            components = 0;
        }

        public void add(int index) {
            if (parent[index] == -1) {
                parent[index] = index;
                components++;
            }
        }

        public void union(int index1, int index2) {
            int root1 = find(index1);
            int root2 = find(index2);

            if (root1 == root2) return;

            parent[root1] = root2;
            components--;
        }

        public int getComponents() {
            return components;
        }

        private int find(int index) {
            if (parent[index] != index) {
                parent[index] = find(parent[index]); // Path compression
            }
            return parent[index];
        }
    }

}
