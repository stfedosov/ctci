package Leetcode4;

import java.util.Arrays;

public class SameEndSubstringCount {

    public static final int ALPHABET_SIZE = 26;

    public static void main(String[] args) {
        System.out.println(Arrays.toString(sameEndSubstringCount("abcaab", new int[][]{{0, 0}, {1, 4}, {2, 5}, {0, 5}})));
    }


    // prefixSum allows us to quickly calculate how many times each character appears in any substring of s:
    //
    // prefixSum[0] = [1, 0, 0, 0, 0, 0, ...] // 'a'
    // prefixSum[1] = [1, 1, 0, 0, 0, 0, ...] // 'ab'
    // prefixSum[2] = [1, 1, 1, 0, 0, 0, ...] // 'abc'
    // prefixSum[3] = [2, 1, 1, 0, 0, 0, ...] // 'abca'
    // prefixSum[4] = [3, 1, 1, 0, 0, 0, ...] // 'abcaa'
    // prefixSum[5] = [3, 2, 1, 0, 0, 0, ...] // 'abcaab'

    public static int[] sameEndSubstringCount(String s, int[][] queries) {
        int[][] prefixSum = new int[s.length()][ALPHABET_SIZE];

        prefixSum[0][s.charAt(0) - 'a']++;
        for (int i = 1; i < s.length(); i++) {
            prefixSum[i] = Arrays.copyOf(prefixSum[i - 1], ALPHABET_SIZE);
            prefixSum[i][s.charAt(i) - 'a']++;
        }

        int[] ans = new int[queries.length];
        for (int idx = 0; idx < queries.length; idx++) {
            int start = queries[idx][0], end = queries[idx][1];
            for (int i = 0; i < ALPHABET_SIZE; i++) {
                int N;
                if (start == 0) {
                    N = prefixSum[end][i];
                } else {
                    N = prefixSum[end][i] - prefixSum[start - 1][i];
                }
                // Number of ways to choose two positions out of N positions is (N * (N - 1)) / 2
                //
                // If a character appears N times in a substring, each occurrence itself is a valid "same-end" substring,
                // Therefore, the number of single-character substrings is N
                ans[idx] += (N * (N - 1)) / 2 + N;
            }
        }
        return ans;
    }

}
