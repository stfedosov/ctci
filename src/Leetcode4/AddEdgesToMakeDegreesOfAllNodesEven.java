package Leetcode4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AddEdgesToMakeDegreesOfAllNodesEven {

    public static void main(String[] args) {

    }

    // Return true if it is possible to make the degree of each node in the graph even
    // ========================
    // Handshake lemma: in every finite undirected graph, the number of vertices that touch an odd number of edges is even
    // Which means, if we have odd number of edges with odd number of vertices, it's impossible to make them even
    public boolean isPossible(int n, List<List<Integer>> edges) {
        Map<Integer, Set<Integer>> graph = new HashMap<>();
        for (List<Integer> l : edges) {
            graph.computeIfAbsent(l.get(0), x -> new HashSet<>()).add(l.get(1));
            graph.computeIfAbsent(l.get(1), x -> new HashSet<>()).add(l.get(0));
        }
        List<Integer> oddNodes = new ArrayList<>();
        for (Map.Entry<Integer, Set<Integer>> e : graph.entrySet()) {
            if (e.getValue().size() % 2 != 0) {
                oddNodes.add(e.getKey());
            }
        }
        if (oddNodes.size() % 2 != 0) {
            return false;
        } else if (oddNodes.isEmpty()) {
            return true;
        } else if (oddNodes.size() == 2) {
            int a = oddNodes.get(0);
            int b = oddNodes.get(1);
            if (!graph.get(a).contains(b)) {
                return true;
            }
            for (int c = 1; c <= n; c++) {
                if (c != a && c != b && !graph.get(a).contains(c) && !graph.get(b).contains(c)) {
                    return true;
                }
            }
        } else if (oddNodes.size() == 4) {
            int a = oddNodes.get(0);
            int b = oddNodes.get(1);
            int c = oddNodes.get(2);
            int d = oddNodes.get(3);
            // Try all possible pairings to add two edges
            if ((!graph.get(a).contains(b) && !graph.get(c).contains(d)) ||
                    (!graph.get(a).contains(c) && !graph.get(b).contains(d)) ||
                    (!graph.get(a).contains(d) && !graph.get(b).contains(c))) {
                return true;
            }
        }
        return false;
    }
}
