package Leetcode4;

import java.util.Arrays;

public class FindLeftMostIndex {

    public static void main(String[] args) {
        int[] array = new int[]{1, 2, 3, 3, 3, 6, 9};
        System.out.println(binarySearch(array, 2)); // 1
        System.out.println(binarySearch(array, 3)); // 2
        System.out.println(binarySearchRightMost(array, 3)); // 2
        System.out.println(binarySearch(array, 9)); // 6
        System.out.println(binarySearch(array, 4)); // 5
        System.out.println(binarySearch(array, -5)); // 0
        System.out.println(binarySearch(array, 100)); // 7
        System.out.println(Arrays.toString(searchRange(array, 3)));
    }

    public static int[] searchRange(int[] nums, int target) {
        int l = binarySearch(nums, target);
        // target does not exist. No need to look for the last position.
        if (l == nums.length || nums[l] != target) return new int[]{-1, -1};
        // look for the index of target + 1
        int r = binarySearch(nums, target + 1);
        // last position is r - 1.
        return new int[]{l, r - 1};
    }

    private static int binarySearch(int[] nums, int target) {
        int l = 0, r = nums.length;
        while (l < r) {
            int m = l + (r - l) / 2;
            if (nums[m] < target) l = m + 1;
            else r = m;
        }
        return l;
    }

    private static int binarySearchRightMost(int[] nums, int target) {
        int l = 0, r = nums.length;
        while (l < r) {
            int m = l + (r - l) / 2;
            if (nums[m] <= target) {  // Change here to allow moving right even if target is found
                l = m + 1;
            } else {
                r = m;
            }
        }
        return l - 1; // Adjust the return to get the rightmost position
    }
}
