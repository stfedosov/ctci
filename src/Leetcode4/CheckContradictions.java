package Leetcode4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CheckContradictions {

    public static void main(String[] args) {
        System.out.println(checkContradictions(List.of(List.of("a", "b"), List.of("b", "c"), List.of("a", "c")),
                new double[]{3d, 0.5d, 1.5d})); // false;
        System.out.println(checkContradictions(List.of(List.of("le", "et"), List.of("le", "code"), List.of("code", "et")),
                new double[]{2d, 5d, 0.5d})); //true
    }

    public static boolean checkContradictions(List<List<String>> equations, double[] values) {
        int idx = 0;
        Map<String, List<Edge>> graph = new HashMap<>();
        for (List<String> eq : equations) {
            Double value = dfs(graph, eq.get(0), eq.get(1), new HashSet<>(), 1.0d);
            if (value != null && Math.abs(value - values[idx]) > Math.pow(10, -5)) {
                return true;
            }
            graph.computeIfAbsent(eq.get(0), x -> new ArrayList<>()).add(new Edge(eq.get(0), eq.get(1), values[idx]));
            graph.computeIfAbsent(eq.get(1), x -> new ArrayList<>()).add(new Edge(eq.get(1), eq.get(0), 1 / values[idx]));
            idx++;
        }
        return false;
    }

    private static Double dfs(Map<String, List<Edge>> graph, String start, String end, Set<String> seen, double res) {
        if (seen.contains(start)) {
            return null;
        }
        if (start.equals(end)) {
            return res;
        }
        seen.add(start);
        for (Edge edge : graph.getOrDefault(start, Collections.emptyList())) {
            Double re = dfs(graph, edge.end, end, seen, res * edge.value);
            if (re != null) {
                return re;
            }
        }
        return null;
    }

    private static class Edge {
        String start, end;
        double value;

        public Edge(String start, String end, double value) {
            this.start = start;
            this.end = end;
            this.value = value;
        }
    }


}
