package Leetcode4;

public class MinimumOperationsToWriteY {

    public static void main(String[] args) {
        minimumOperationsToWriteY(new int[][]{
                {1, 2, 2},
                {1, 1, 0},
                {0, 1, 0}
        }); // 3

        minimumOperationsToWriteY(new int[][]{
                {0, 1, 0, 1, 0},
                {2, 1, 0, 1, 2},
                {2, 2, 2, 0, 1},
                {2, 2, 2, 2, 2},
                {2, 1, 2, 2, 2}
        }); // 12
    }

    public static int minimumOperationsToWriteY(int[][] grid) {
        int countY_0 = 0, countY_1 = 0, countY_2 = 0;
        int countOutsideY_0 = 0, countOutsideY_1 = 0, countOutsideY_2 = 0;
        int center = grid.length / 2;

        for (int row = 0; row < grid.length; row++) {
            for (int col = 0; col < grid.length; col++) {
                // Check if the cell is part of the Y shape
                boolean isPartOfY = (row < center && (row == col || col == grid.length - 1 - row)) ||
                        (row >= center && col == center);

                if (isPartOfY) {
                    if (grid[row][col] == 0) {
                        countY_0++;
                    } else if (grid[row][col] == 1) {
                        countY_1++;
                    } else {
                        countY_2++;
                    }
                } else {
                    if (grid[row][col] == 0) {
                        countOutsideY_0++;
                    } else if (grid[row][col] == 1) {
                        countOutsideY_1++;
                    } else {
                        countOutsideY_2++;
                    }
                }
            }
        }

        // Calculate the minimum operations required for each configuration
        // to form Y from zeros we need to turn all 1's and 2's into 0 and outside should be a different number, so spend all outside 0's on it
        int operationsForY_0 = countY_1 + countY_2 + countOutsideY_0 + Math.min(countOutsideY_1, countOutsideY_2);
        int operationsForY_1 = countY_0 + countY_2 + countOutsideY_1 + Math.min(countOutsideY_0, countOutsideY_2);
        int operationsForY_2 = countY_0 + countY_1 + countOutsideY_2 + Math.min(countOutsideY_0, countOutsideY_1);

        // Return the minimum of the three possible configurations
        return Math.min(operationsForY_0, Math.min(operationsForY_1, operationsForY_2));
    }

}
