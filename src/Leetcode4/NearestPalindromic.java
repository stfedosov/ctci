package Leetcode4;

public class NearestPalindromic {

    public static void main(String[] args) {
        nearestPalindromic("1805170081"); // 1805110081
        nearestPalindromic("123"); // 121
        nearestPalindromic("11"); // 9
    }

    public static String nearestPalindromic(String n) {
        long num = Long.parseLong(n);
        int len = n.length();

        // edge cases
        // num is 10, 100, 1000... and so on
        if (num <= 10 || num % 10 == 0 && n.charAt(0) == '1' && Long.parseLong(n.substring(1)) == 0) {
            return String.valueOf(num - 1);
        }

        // num is 11, 101, 1001... and so on
        if (num == 11 || num % 10 == 1 && n.charAt(0) == '1' && Long.parseLong(n.substring(1, len - 1)) == 0) {
            return String.valueOf(num - 2);
        }

        // num is 9, 99, 999, 9999 and so on
        if (all9(n)) {
            return String.valueOf(num + 2);
        }

        // now primary scenarios
        boolean isEvenDigit = len % 2 == 0;
        String root = isEvenDigit ? n.substring(0, len / 2) : n.substring(0, (len / 2) + 1);
        long rootNum = Long.parseLong(root);
        // use the same num as is to get palindrome
        long sameNum = Long.parseLong(toPalindrome(String.valueOf(rootNum), isEvenDigit));
        long sameDiff = Math.abs(num - sameNum);
        // use next bigger num to get palindrome
        long bigNum = Long.parseLong(toPalindrome(String.valueOf(rootNum + 1), isEvenDigit));
        long bigDiff = Math.abs(num - bigNum);
        // use next lower num to get palindrome
        long smallNum = Long.parseLong(toPalindrome(String.valueOf(rootNum - 1), isEvenDigit));
        long smallDiff = Math.abs(num - smallNum);

        // pick closest between big and small
        long closest = bigDiff >= smallDiff ? smallNum : bigNum;
        long diff = Math.min(smallDiff, bigDiff);

        // if same num diff is not 0 meaning it's not a palindrome already then check for diff
        if (sameDiff != 0) {
            // if same diff pick smaller
            if (sameDiff == diff) {
                closest = Math.min(sameNum, closest);
            } else if (sameDiff < diff) {
                // if not pick same num one
                closest = sameNum;
            }
        }
        return String.valueOf(closest);
    }

    private static String toPalindrome(String s, boolean isEvenDigit) {
        StringBuilder n = new StringBuilder(s).reverse();
        if (isEvenDigit) {
            return s + n;
        } else {
            return s + n.deleteCharAt(0);
        }
    }

    private static boolean all9(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != '9') {
                return false;
            }
        }
        return true;
    }

}
