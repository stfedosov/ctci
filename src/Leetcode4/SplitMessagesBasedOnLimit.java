package Leetcode4;

// https://leetcode.com/problems/split-message-based-on-limit/
public class SplitMessagesBasedOnLimit {

    public static void main(String[] args) {

    }

    private static int getNumberOfMessages(int messageLength, int limit) {
        int singleMessageLength = limit - 5; // length of "<1/1>" is 5
        int curMaxNumberOfPages = 9;
        int extraSpace = 0;
        while (messageLength > singleMessageLength * curMaxNumberOfPages + extraSpace) {
            // Decreases the available length for the message part by 2
            // to account for the increasing length of the suffix as the number of pages grows
            singleMessageLength -= 2;
            // This accounts for the additional characters needed for the suffix
            // as the total number of pages grows larger
            extraSpace += curMaxNumberOfPages; // "***<5/23>" has extra space than "**<12/23>"
            curMaxNumberOfPages = Integer.parseInt(curMaxNumberOfPages + "9");
            if (singleMessageLength <= 0) {
                return 0;
            }
        }
        int result = (messageLength - extraSpace) / singleMessageLength;
        if ((messageLength - extraSpace) % singleMessageLength != 0) {
            result++;
        }
        return result;
    }

    public static String[] splitMessage(String message, int limit) {
        int pages = getNumberOfMessages(message.length(), limit);
        var res = new String[pages];
        int curPos = 0;
        for (int page = 0; page < pages; page++) {
            var tag = "<" + (page + 1) + "/" + pages + ">";
            int infoLen = limit - tag.length();
            res[page] = message.substring(curPos, Math.min(message.length(), curPos + infoLen)) + tag;
            curPos += infoLen;
        }
        return res;
    }
}
