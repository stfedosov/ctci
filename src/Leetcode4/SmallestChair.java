package Leetcode4;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class SmallestChair {

    /*
    There is a party where n friends numbered from 0 to n - 1 are attending.
    There is an infinite number of chairs in this party that are numbered from 0 to infinity.
    When a friend arrives at the party, they sit on the unoccupied chair with the smallest number.

    For example, if chairs 0, 1, and 5 are occupied when a friend comes, they will sit on chair number 2.
    When a friend leaves the party, their chair becomes unoccupied at the moment they leave.
    If another friend arrives at that same moment, they can sit in that chair.

    You are given a 0-indexed 2D integer array times, where times[i] = [arrival, leaving],
    indicating the arrival and leaving times of the ith friend respectively,
    and an integer targetFriend.

    Return the chair number that the friend numbered targetFriend will sit on.
     */
    public static void main(String[] args) {
        System.out.println(smallestChair(new int[][]{{3, 10}, {1, 5}, {2, 6}}, 0));
    }

    public static int smallestChair(int[][] times, int targetFriend) {
        Map<Integer, int[]> friendPerTime = new HashMap<>();
        for (int friend = 0; friend < times.length; friend++) {
            friendPerTime.put(friend, times[friend]);
        }

        // Convert map to list and sort based on arrival times
        List<Map.Entry<Integer, int[]>> list = new ArrayList<>(friendPerTime.entrySet());
        list.sort(Comparator.comparingInt(a -> a.getValue()[0]));

        // PriorityQueue for available chairs (min-heap)
        Queue<Integer> availableChairs = new PriorityQueue<>();
        // Initially, all chairs (index 0, 1, 2, ...) are available
        for (int i = 0; i < times.length; i++) {
            availableChairs.add(i);
        }

        // PriorityQueue to manage when chairs become free, sorted by leave time
        // Each element in the queue will be {leaveTime, chairIndex}
        Queue<int[]> leaveQueue = new PriorityQueue<>(Comparator.comparingInt(a -> a[0]));

        // Go through each friend in order of their arrival times
        for (Map.Entry<Integer, int[]> entry : list) {
            int friend = entry.getKey();
            int arriveTime = entry.getValue()[0];
            int leaveTime = entry.getValue()[1];

            // Free up chairs that should be freed before the current friend's arrival
            while (!leaveQueue.isEmpty() && leaveQueue.peek()[0] <= arriveTime) {
                availableChairs.add(leaveQueue.poll()[1]);
            }

            // Assign the smallest available chair
            int chair = availableChairs.poll();

            // If this is the target friend, return the assigned chair
            if (friend == targetFriend) {
                return chair;
            }

            // Add the friend to the leave queue with their leave time and assigned chair
            leaveQueue.add(new int[]{leaveTime, chair});
        }

        // This point should never be reached as the target friend is guaranteed to be seated
        return -1;
    }

}
