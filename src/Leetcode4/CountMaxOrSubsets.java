package Leetcode4;

public class CountMaxOrSubsets {

    /*
    Given an integer array nums, find the maximum possible bitwise OR of a subset of nums
    and return the number of different non-empty subsets with the maximum bitwise OR.

    An array a is a subset of an array b if a can be obtained from b by deleting some
    (possibly zero) elements of b.
    Two subsets are considered different if the indices of the elements chosen are different.

    The bitwise OR of an array a is equal to a[0] OR a[1] OR ... OR a[a.length - 1] (0-indexed).
     */

    private static int count = 0;

    public static void main(String[] args) {
        System.out.println(countMaxOrSubsets(new int[]{3, 2, 1, 5})); // 6
        System.out.println(countMaxOrSubsets(new int[]{2, 2, 2})); // 7
        System.out.println(countMaxOrSubsets(new int[]{3, 1})); // 2
    }

    // O(2^n), can be memoized memo[index][current]
    public static int countMaxOrSubsets(int[] nums) {
        int maxOr = 0;
        for (int num : nums) {
            maxOr |= num;
        }
        dfs(nums, 0, 0, maxOr);
        return count;
    }

    private static void dfs(int[] nums, int offset, int current, int maxOr) {
        if (nums.length == offset) {
            if (current == maxOr) {
                count++;
            }
        } else {
            dfs(nums, offset + 1, current | nums[offset], maxOr);
            dfs(nums, offset + 1, current, maxOr);
        }
    }

    // Alternative

    private static int maxCount = 0, maxOr = 0;

    public static int countMaxOrSubsets2(int[] nums) {
        for (int num : nums) {
            maxOr |= num;
        }

        dfs(nums, 0, 0);
        return maxCount;
    }

    public static void dfs(int[] nums, int idx, int mask) {
        if (mask == maxOr) {
            maxCount++;
        }

        for (int i = idx; i < nums.length; i++) {
            dfs(nums, i + 1, mask | nums[i]);
        }
    }

}
