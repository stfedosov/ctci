package Leetcode4;


import Leetcode.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class CousinsInBinaryTree {

    /*
    Given the root of a binary tree, replace the value of each node in the tree
    with the sum of all its cousins' values.

    Two nodes of a binary tree are cousins if they have the same depth with different parents.

    Return the root of the modified tree.

    Note that the depth of a node is the number of edges in the path from the root node to it.
    */
    public static void main(String[] args) {

    }

    public static TreeNode replaceValueInTree(TreeNode root) {
        Queue<TreeNode> q = new LinkedList<>();
        q.offer(root);
        List<Integer> list = new ArrayList<>();
        while (!q.isEmpty()) {
            int size = q.size();
            int sum = 0;
            while (size-- > 0) {
                TreeNode next = q.poll();
                if (next.left != null) {
                    q.offer(next.left);
                }
                if (next.right != null) {
                    q.offer(next.right);
                }
                sum += next.val;
            }
            list.add(sum);
        }
        q.offer(root);
        root.val = 0;
        int levelIndex = 1;
        while (!q.isEmpty()) {
            int size = q.size();
            while (size-- > 0) {
                TreeNode next = q.poll();
                int sumSiblings = (next.left != null ? next.left.val : 0)
                        + (next.right != null ? next.right.val : 0);
                if (next.left != null) {
                    next.left.val = list.get(levelIndex) - sumSiblings;
                    q.offer(next.left);
                }
                if (next.right != null) {
                    next.right.val = list.get(levelIndex) - sumSiblings;
                    q.offer(next.right);
                }
            }
            levelIndex++;
        }
        return root;
    }

}
