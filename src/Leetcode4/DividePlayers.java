package Leetcode4;

import java.util.HashMap;
import java.util.Map;

public class DividePlayers {

    // You are given a positive integer array skill of even length n where skill[i]
    // denotes the skill of the ith player.
    // Divide the players into n / 2 teams of size 2 such that
    // the total skill of each team is equal.
    //
    // The chemistry of a team is equal to the product of the skills of the players
    // on that team.
    //
    // Return the sum of the chemistry of all the teams,
    // or return -1 if there is no way to divide the players into teams such that
    // the total skill of each team is equal.
    public static void main(String[] args) {
        System.out.println(dividePlayers(new int[]{3, 2, 5, 1, 3, 4})); // 22
        System.out.println(dividePlayers(new int[]{1, 1, 2, 3})); // -1
    }

    public static long dividePlayers(int[] skill) {
        Map<Integer, Integer> map = new HashMap<>();
        int sum = 0;
        for (int skl : skill) {
            map.put(skl, map.getOrDefault(skl, 0) + 1);
            sum += skl;
        }
        if (sum % (skill.length / 2) != 0) {
            return -1;
        }
        int targetSkill = sum / (skill.length / 2);
        long answer = 0L;
        for (int skl : skill) {
            int remainingSkill = targetSkill - skl;
            if (!map.containsKey(remainingSkill) || map.get(remainingSkill) == 0) {
                return -1L;
            }
            map.put(remainingSkill, map.get(remainingSkill) - 1);
            answer += (long) skl * remainingSkill;
        }
        return answer / 2;
    }

}
