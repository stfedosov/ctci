package Leetcode4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CheckIfPrerequisite {

    // There are a total of numCourses courses you have to take, labeled from 0 to numCourses - 1.
    // You are given an array prerequisites where prerequisites[i] = [ai, bi] indicates that you
    // must take course ai first if you want to take course bi.
    //
    //  For example, the pair [0, 1] indicates that you have to take course 0 before you can take course 1.
    //  Prerequisites can also be indirect.
    //  If course a is a prerequisite of course b, and course b is a prerequisite of course c, then course a
    //  is a prerequisite of course c.
    //
    //  You are also given an array queries where queries[j] = [uj, vj]. For the jth query, you should answer
    //  whether course uj is a prerequisite of course vj or not.
    //
    //  Return a boolean array answer, where answer[j] is the answer to the jth query.
    public static void main(String[] args) {

    }

    // Simple DFS
    public List<Boolean> checkIfPrerequisite1(int numCourses, int[][] prerequisites, int[][] queries) {
        Map<Integer, List<Integer>> graph = new HashMap<>();
        for (int[] prereq : prerequisites) {
            graph.computeIfAbsent(prereq[0], x -> new ArrayList<>()).add(prereq[1]);
        }
        List<Boolean> answer = new ArrayList<>();
        Map<String, Boolean> memo = new HashMap<>();

        for (int[] query : queries) {
            answer.add(dfs(query[0], query[1], graph, new HashSet<>(), memo));
        }
        return answer;
    }

    private boolean dfs(int src, int target, Map<Integer, List<Integer>> graph, Set<Integer> visited, Map<String, Boolean> memo) {
        String key = src + "-" + target;
        if (memo.containsKey(key)) {
            return memo.get(key);
        }
        if (src == target) {
            return true;
        }
        visited.add(src);
        for (int next : graph.getOrDefault(src, Collections.emptyList())) {
            if (!visited.contains(next)) {
                boolean res = dfs(next, target, graph, visited, memo);
                if (res) {
                    memo.put(key, true);
                    return true;
                }
            }
        }
        memo.put(key, false);
        return false;
    }

    // Floyd Warshall algo
    public static List<Boolean> checkIfPrerequisite2(int n, int[][] prerequisites, int[][] queries) {
        int[][] dist = buildGraph(n, prerequisites);

        for (int k = 0; k < n; k++) {             //Try all intermediate node
            for (int i = 0; i < n; i++) {         //Try all possible starting node
                for (int j = 0; j < n; j++) {     //Try all possible ending node
                    if (dist[i][k] != Integer.MAX_VALUE && dist[k][j] != Integer.MAX_VALUE) {
                        dist[i][j] = 1;
                    }
                }
            }
        }

        List<Boolean> result = new ArrayList<>();
        for (int[] query : queries) {
            if (dist[query[0]][query[1]] != Integer.MAX_VALUE) {
                result.add(true);
            } else {
                result.add(false);
            }
        }

        return result;
    }

    private static int[][] buildGraph(int n, int[][] edges) {
        int[][] dist = new int[n][n];
        for (int i = 0; i < n; i++) {
            Arrays.fill(dist[i], Integer.MAX_VALUE);
            dist[i][i] = 1;
        }

        for (int[] edge : edges) {
            dist[edge[0]][edge[1]] = 1;
        }

        return dist;
    }


}
