package Leetcode4;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class MaximumImportance {

    public static void main(String[] args) {
        maximumImportance(5, new int[][]{{0, 1}, {1, 2}, {2, 3}, {0, 2}, {1, 3}, {2, 4}});
        maximumImportance(5, new int[][]{{0, 1}});
    }

    public static long maximumImportance(int n, int[][] roads) {
        Map<Integer, Integer> degree = new HashMap<>();
        for (int[] road : roads) {
            degree.put(road[0], degree.getOrDefault(road[0], 0) + 1);
            degree.put(road[1], degree.getOrDefault(road[1], 0) + 1);
        }
        Queue<Map.Entry<Integer, Integer>> q = new PriorityQueue<>((a, b) -> b.getValue() - a.getValue());
        for (Map.Entry<Integer, Integer> e : degree.entrySet()) {
            q.offer(e);
        }
        while (!q.isEmpty()) {
            var polled = q.poll();
            polled.setValue(n--);
        }
        return dfs(roads, degree);
    }

    private static long dfs(int[][] roads, Map<Integer, Integer> degree) {
        long sum = 0L;
        for (int[] road : roads) {
            sum += degree.get(road[0]) + degree.get(road[1]);
        }
        return sum;
    }

}
