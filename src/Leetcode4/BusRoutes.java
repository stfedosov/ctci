package Leetcode4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class BusRoutes {

    public static void main(String[] args) {

    }

    public static int numBusesToDestination(int[][] routes, int source, int target) {
        Map<Integer, List<Integer>> graph = new HashMap<>();
        for (int i = 0; i < routes.length; i++) {
            for (int j = 0; j < routes[i].length; j++) {
                graph.computeIfAbsent(routes[i][j], x -> new ArrayList<>()).add(i);
            }
        }
        int count = 1;
        Queue<Integer> q = new LinkedList<>();
        Set<Integer> visitedBus = new HashSet<>();
        Set<Integer> visitedBusStop = new HashSet<>();
        q.offer(source);
        visitedBusStop.add(source);
        while (!q.isEmpty()) {
            int size = q.size();
            while (size-- > 0) {
                int next = q.poll();
                if (next == target) return count - 1;
                for (int routeNum : graph.get(next)) {
                    if (!visitedBus.contains(routeNum)) {
                        for (int stop : routes[routeNum]) {
                            if (!visitedBusStop.contains(stop)) {
                                q.add(stop);
                                visitedBusStop.add(stop);
                            }
                        }
                        visitedBus.add(routeNum);
                    }
                }
            }
            count++;
        }
        return -1;
    }

}
