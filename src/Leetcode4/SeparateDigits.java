package Leetcode4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class SeparateDigits {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(separateDigits(new int[]{13, 25, 83, 77}))); // [1, 3, 2, 5, 8, 3, 7, 7]
        System.out.println(Arrays.toString(separateDigits(new int[]{7, 1, 3, 9}))); // [7,1,3,9]
    }

    // O(N * Average length of a num)
    public static int[] separateDigits(int[] nums) {
        Stack<Integer> stack = new Stack<>();
        for (int i = nums.length - 1; i >= 0; i--) {
            while (nums[i] > 0) {
                stack.push(nums[i] % 10);
                nums[i] /= 10;
            }
        }
        int[] res = new int[stack.size()];
        for (int i = 0; i < res.length; i++) {
            res[i] = stack.pop();
        }
        return res;
    }

}
