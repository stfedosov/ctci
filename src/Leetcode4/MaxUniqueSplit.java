package Leetcode4;

import java.util.HashSet;
import java.util.Set;

public class MaxUniqueSplit {

    /*
    Given a string s, return the maximum number of unique substrings
    that the given string can be split into.

    You can split string s into any list of non-empty substrings,
    where the concatenation of the substrings forms the original string.
    However, you must split the substrings such that all of them are unique.

    A substring is a contiguous sequence of characters within a string.
     */
    public static void main(String[] args) {
        System.out.println(maxUniqueSplitEfficient("ababccc")); // 5
        System.out.println(maxUniqueSplitEfficient("aba")); // 2
    }

    // Efficient solution including pruning
    public static int maxUniqueSplitEfficient(String s) {
        Set<String> seen = new HashSet<>();
        int[] maxCount = new int[1];
        backtrack(s, 0, seen, 0, maxCount);
        return maxCount[0];
    }

    // O(n * 2^n)
    // - there are 2^n unique string combinations and
    // - for each of the n starting positions, the algorithm potentially explores O(n) choices
    private static void backtrack(String s, int start, Set<String> seen, int count, int[] maxCount) {
        // Prune: If the current count plus remaining characters can't exceed maxCount, return
        if (count + (s.length() - start) <= maxCount[0]) return;

        // Base case: If we reach the end of the string, update maxCount
        if (start == s.length()) {
            maxCount[0] = Math.max(maxCount[0], count);
            return;
        }

        // Try every possible substring starting from 'start'
        for (int end = start + 1; end <= s.length(); end++) {
            var substring = s.substring(start, end);
            if (!seen.contains(substring)) {
                seen.add(substring);
                backtrack(s, end, seen, count + 1, maxCount);
                seen.remove(substring);
            }
        }
    }

}
