package Leetcode4;

import java.util.HashMap;
import java.util.Map;

// https://leetcode.com/problems/apply-discount-every-n-orders/
public class Cashier {

    public static void main(String[] args) {
        var cashier = new Cashier(3, 50,
                new int[]{1, 2, 3, 4, 5, 6, 7},
                new int[]{100, 200, 300, 400, 300, 200, 100});
        System.out.println(cashier.getBill(new int[]{1, 2}, new int[]{1, 2}) == 500);
        System.out.println(cashier.getBill(new int[]{3, 7}, new int[]{10, 10}) == 4000);
        System.out.println(cashier.getBill(new int[]{1, 2, 3, 4, 5, 6, 7}, new int[]{1, 1, 1, 1, 1, 1, 1}) == 800);
        System.out.println(cashier.getBill(new int[]{4}, new int[]{10}) == 4000);
        System.out.println(cashier.getBill(new int[]{7, 3}, new int[]{10, 10}) == 4000);
        System.out.println(cashier.getBill(new int[]{7, 5, 3, 1, 6, 4, 2}, new int[]{10, 10, 10, 9, 9, 9, 7}) == 7350);
        System.out.println(cashier.getBill(new int[]{2, 3, 5}, new int[]{5, 3, 2}) == 2500);
    }

    int n, discount, count = 0;
    private final Map<Integer, Integer> map = new HashMap<>();

    public Cashier(int n, int discount, int[] products, int[] prices) {
        for (int i = 0; i < products.length; i++) {
            map.put(products[i], prices[i]);
        }
        this.n = n;
        this.discount = discount;
    }

    public double getBill(int[] product, int[] amount) {
        var sum = 0d;
        count++;
        var applyDiscount = count > 0 && count % n == 0;
        for (int i = 0; i < product.length; i++) {
            sum += map.get(product[i]) * amount[i];
        }
        if (applyDiscount) {
            return sum * (double) (100 - discount) / 100;
        }
        return sum;
    }
}