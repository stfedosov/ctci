package Leetcode4;

public class NodesBetweenCriticalEndpoints {

    /*
    A critical point in a linked list is defined as either a local maxima or a local minima.

    A node is a local maxima if the current node has a value strictly greater than the
    previous node and the next node.

    A node is a local minima if the current node has a value strictly smaller than the
    previous node and the next node.

    Given a linked list head, return an array of length 2 containing [minDistance, maxDistance]
    where minDistance is the minimum distance between any two distinct critical points
    and maxDistance is the maximum distance between any two distinct critical points.

    If there are fewer than two critical points, return [-1, -1].
     */
    public static void main(String[] args) {

    }

    public int[] nodesBetweenCriticalPoints(ListNode head) {
        int[] result = {-1, -1};

        // Pointers to track the previous node, current node, and indices
        ListNode prev = head, tmp = head.next;
        int idx = 1, prevIndex = 0, firstIndex = 0, min = Integer.MAX_VALUE;

        while (tmp.next != null) {
            // Check if the current node is a local maxima or minima
            if ((tmp.val < prev.val && tmp.val < tmp.next.val)
                    || (tmp.val > prev.val && tmp.val > tmp.next.val)) {
                // If this is the first critical point found
                if (prevIndex == 0) {
                    prevIndex = idx;
                    firstIndex = idx;
                } else {
                    // Calculate the minimum distance between critical points
                    min = Math.min(min, idx - prevIndex);
                    prevIndex = idx;
                }
            }

            // Move to the next node and update indices
            idx++;
            prev = tmp;
            tmp = tmp.next;
        }

        // If at least two critical points were found
        if (min != Integer.MAX_VALUE) {
            result = new int[]{min, prevIndex - firstIndex};
        }

        return result;
    }

    public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

}
