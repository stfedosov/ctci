package Leetcode4;

import Leetcode.TreeNode;

import java.util.Stack;

public class RecoverFromPreorderTree {

    // https://leetcode.com/problems/recover-a-tree-from-preorder-traversal/
    public static void main(String[] args) {
        System.out.println(recoverFromPreorder("1-2--3--4-5--6--7"));
    }

    static int idx = 0;
    static int dash = 0;

    public static TreeNode recoverFromPreorder(String traversal) {
        return build(traversal, 0);
    }

    public static TreeNode build(String s, int depth) {
        if (depth != dash) {
            return null;
        }

        int num = 0;
        while (idx < s.length() && Character.isDigit(s.charAt(idx))) {
            num = num * 10 + (s.charAt(idx) - '0');
            idx++;
        }

        dash = 0;

        while (idx < s.length() && s.charAt(idx) == '-') {
            idx++;
            dash++;
        }

        TreeNode node = new TreeNode(num);
        node.left = build(s, depth + 1);
        node.right = build(s, depth + 1);
        return node;

    }

    public TreeNode recoverFromPreorderIterative(String traversal) {
        Stack<TreeNode> stack = new Stack<>();
        int i = 0;
        while (i < traversal.length()) {
            int depth = 0;
            while (i < traversal.length() && traversal.charAt(i) == '-') {
                i++;
                depth++;
            }
            int num = 0;
            while (i < traversal.length() && Character.isDigit(traversal.charAt(i))) {
                num = num * 10 + (traversal.charAt(i) - '0');
                i++;
            }
            while (stack.size() > depth) {
                stack.pop();
            }
            TreeNode node = new TreeNode(num);
            if (!stack.isEmpty()) {
                if (stack.peek().left == null) {
                    stack.peek().left = node;
                } else {
                    stack.peek().right = node;
                }
            }
            stack.push(node);
        }
        while (stack.size() > 1) {
            stack.pop();
        }
        return stack.peek();
    }

}
