package Leetcode4;

import java.util.HashMap;
import java.util.Map;

public class MaximumSubarraySum {

    // You are given an array nums of length n and a positive integer k.
    // A subarray of nums is called good if the absolute difference between
    // its first and last element is exactly k, i.e. the subarray nums[i...j] is good if |nums[i] - nums[j]| == k.
    // Return the maximum sum of a good subarray of nums. If there are no good subarrays, return 0.
    public static void main(String[] args) {
        System.out.println(maximumSubarraySum(new int[]{1, 2, 3, 4, 5, 6}, 1)); // 11
        System.out.println(maximumSubarraySum(new int[]{-1, 3, 2, 4, 5}, 3)); // 11
    }

    public static long maximumSubarraySum(int[] nums, int k) {
        Map<Integer, Long> valToMinPrefixSum = new HashMap<>();
        long maxSum = Long.MIN_VALUE, prefixSum = 0;
        for (int num : nums) {
            // store the min value here to ensure that when calculating the potential maximum subarray sum,
            // we are using the smallest possible prefix sum encountered up to that point.
            // This is important because the difference between the current prefixSum and the smallest prefix sum
            // that has been seen before a valid subarray (num + k or num - k) gives us the largest possible subarray sum
            valToMinPrefixSum.put(num, Math.min(prefixSum, valToMinPrefixSum.getOrDefault(num, Long.MAX_VALUE)));
            prefixSum += num;
            if (valToMinPrefixSum.containsKey(num + k)) {
                maxSum = Math.max(maxSum, prefixSum - valToMinPrefixSum.get(num + k));
            }
            if (valToMinPrefixSum.containsKey(num - k)) {
                maxSum = Math.max(maxSum, prefixSum - valToMinPrefixSum.get(num - k));
            }
        }
        return maxSum == Long.MIN_VALUE ? 0 : maxSum;
    }

}
