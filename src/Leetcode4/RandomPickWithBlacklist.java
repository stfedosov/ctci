package Leetcode4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class RandomPickWithBlacklist {

    private static class Solution1 {

        public static void main(String[] args) {
            Solution1 randomPickWithBlacklist = new Solution1(7, new int[]{2, 3, 5});
            System.out.println(randomPickWithBlacklist.pick());
            System.out.println(randomPickWithBlacklist.pick());
            System.out.println(randomPickWithBlacklist.pick());
            System.out.println(randomPickWithBlacklist.pick());
            System.out.println(randomPickWithBlacklist.pick());
            System.out.println(randomPickWithBlacklist.pick());
//        RandomPickWithBlacklist randomPickWithBlacklist = new RandomPickWithBlacklist(3, new int[]{2});
//        System.out.println(randomPickWithBlacklist.pick());
//        System.out.println(randomPickWithBlacklist.pick());
//        System.out.println(randomPickWithBlacklist.pick());
//        System.out.println(randomPickWithBlacklist.pick());
        }

        Random random = new Random();
        List<List<Integer>> list;

        // Based on the given black list, create a range pick for Random.next() as follows:
        // For example: Black List is [2,3,5] and N is 7, so our range pick will be;
        // [0,2), [4,5), and [6,7). This is going to be our Range Pick List.
        public Solution1(int N, int[] blacklist) {
            Arrays.sort(blacklist);
            list = new ArrayList<>();
            int start = 0, i = 0;
            while (i < blacklist.length) {
                if (start == blacklist[i]) {
                    start++;
                    i++;
                    continue;
                }
                list.add(List.of(start, blacklist[i]));
                start = blacklist[i] + 1;
                i++;
            }
            if (start < N) {
                list.add(List.of(start, N));
            }
        }

        public int pick() {
            int index = random.nextInt(list.size()); //Pick the Range Pick List Index First
            List<Integer> next = list.get(index); // Pick the Range from the list
            return random.nextInt(next.get(1) - next.get(0)) + next.get(0); //Call any number in the range
        }
    }

    private static class Solution2 {
        // ------

        private int range;
        private Map<Integer, Integer> mapping;
        private Random rand;

        public Solution2(int n, int[] blacklist) {
            mapping = new HashMap<>();
            rand = new Random();

            int blacklistLength = blacklist.length;
            range = n - blacklistLength;

            // Create a set of blacklisted numbers in the range [n - blacklistLength, n)
            Set<Integer> blackSet = new HashSet<>();
            for (int b : blacklist) {
                if (b >= range) {
                    blackSet.add(b);
                }
            }

            // Map blacklisted numbers < range to available valid numbers >= range
            int last = range;
            for (int b : blacklist) {
                if (b < range) {
                    // Find the next available valid number in [range, n) that's not in the blacklist
                    while (blackSet.contains(last)) {
                        last++;
                    }
                    mapping.put(b, last);
                    last++;
                }
            }
        }

        public int pick() {
            int randNum = rand.nextInt(range);
            // If the random number is blacklisted, return its mapped value, otherwise return the number itself
            return mapping.getOrDefault(randNum, randNum);
        }
    }

}
