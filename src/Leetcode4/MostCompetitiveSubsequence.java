package Leetcode4;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;
import java.util.Objects;

public class MostCompetitiveSubsequence {

    // Given an integer array nums and a positive integer k, return the most competitive subsequence of nums of size k.
    // An array's subsequence is a resulting sequence obtained by erasing some (possibly zero) elements from the array.
    // We define that a subsequence a is more competitive than a subsequence b (of the same length)
    // if in the first position where a and b differ, subsequence a has a number less than the corresponding number in b.
    // For example, [1,3,4] is more competitive than [1,3,5]
    // because the first position they differ is at the final number, and 4 is less than 5.
    public static void main(String[] args) {
        System.out.println(Arrays.toString(mostCompetitiveOptimal(new int[]{3, 5, 2, 6}, 2))); // [2, 6]
        System.out.println(Arrays.toString(mostCompetitiveOptimal(new int[]{2, 4, 3, 3, 5, 4, 9, 6}, 4))); // [2, 3, 3, 4]
    }


    public static int[] mostCompetitiveOptimal(int[] nums, int k) {
        Deque<Integer> stack = new ArrayDeque<>();
        int allowedToDropFromTheArray = nums.length - k;
        for (int num : nums) {
            while (!stack.isEmpty() && stack.peekLast() > num && allowedToDropFromTheArray > 0) {
                stack.pollLast();
                allowedToDropFromTheArray--;
            }
            stack.addLast(num);
        }
        int[] result = new int[k];
        for (int i = 0; i < k; i++) {
            result[i] = stack.pollFirst();
        }
        return result;
    }

    // brute force
    public static int[] mostCompetitiveBruteForce(int[] nums, int k) {
        List<List<Integer>> list = new ArrayList<>();
        generate(nums, k, 0, new ArrayList<>(), list);
        list.sort((o1, o2) -> {
            for (int i = 0; i < o1.size(); i++) {
                if (!Objects.equals(o1.get(i), o2.get(i))) {
                    return Integer.compare(o1.get(i), o2.get(i));
                }
            }
            return 0;
        });
        int[] res = new int[k];
        List<Integer> first = list.get(0);
        for (int i = 0; i < k; i++) {
            res[i] = first.get(i);
        }
        return res;
    }

    private static void generate(int[] nums, int k, int idx, List<Integer> tmp, List<List<Integer>> list) {
        if (tmp.size() == k) {
            list.add(new ArrayList<>(tmp));
            return;
        }
        for (int i = idx; i < nums.length; i++) {
            tmp.add(nums[i]);
            generate(nums, k, i + 1, tmp, list);
            tmp.remove(tmp.size() - 1);
        }
    }


}
