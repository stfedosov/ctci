package Leetcode4;

public class MinSwapsForMatrixOfOnes {

    // Given n x n matrix with 0s and 1s, given the min number of swaps (not necessary to be adjacent)
    // required to create a y x y (y<=n) matrix of 1s inside.

    public static int matrixSum(int[][] matrix) {
        int sum = 0;
        for (int[] row : matrix) {
            for (int val : row) {
                sum += val;
            }
        }
        return sum;
    }

    public static int calculateWindowSum(int[][] matrix, int startX, int startY, int y) {
        int sum = 0;
        for (int i = startX; i < startX + y; i++) {
            for (int j = startY; j < startY + y; j++) {
                sum += matrix[i][j];
            }
        }
        return sum;
    }

    public static int minSwaps(int[][] matrix, int y) {
        int n = matrix.length;

        // Total sum of 1s in the entire matrix
        int totalSum = matrixSum(matrix);

        int minSwaps = Integer.MAX_VALUE;

        // Traverse all possible y x y sub-matrices (sliding window)
        for (int i = 0; i <= n - y; i++) {
            for (int j = 0; j <= n - y; j++) {
                // Number of 1s in the current window
                int onesInWindow = calculateWindowSum(matrix, i, j, y);

                // Number of 0s in the current window
                int zerosInWindow = y * y - onesInWindow;

                // Check if there are enough 1s outside of this window
                int onesOutsideWindow = totalSum - onesInWindow;

                // If there are enough 1s outside the window to replace all the 0s inside, calculate swaps
                if (onesOutsideWindow >= zerosInWindow) {
                    minSwaps = Math.min(minSwaps, zerosInWindow);
                }
            }
        }

        // If no swaps are possible, return -1 (otherwise return the minimum swaps)
        return minSwaps == Integer.MAX_VALUE ? -1 : minSwaps;
    }

    public static void main(String[] args) {
        // Example usage:
        int[][] matrix = {
                {1, 0, 1, 0},
                {0, 1, 0, 1},
                {1, 0, 1, 0},
                {0, 1, 1, 1}
        };

        int y = 2;  // Size of the sub-matrix of 1s we want to create

        int result = minSwaps(matrix, y);
        System.out.println("Minimum number of swaps required: " + result);
    }
}
