package Leetcode4;

import java.util.ArrayList;
import java.util.List;

// You are implementing a program to use as your calendar.
// We can add a new event if adding the event will not cause a triple booking.
public class MyCalendarTwo {

    private final List<List<Integer>> bookings;
    private final List<List<Integer>> overlappedBookings;

    public MyCalendarTwo() {
        this.bookings = new ArrayList<>();
        this.overlappedBookings = new ArrayList<>();
    }

    public boolean book(int start, int end) {
        for (List<Integer> next : overlappedBookings) {
            if (isOverlap(next, start, end)) {
                return false;
            }
        }
        for (List<Integer> next : bookings) {
            if (isOverlap(next, start, end)) {
                overlappedBookings.add(getOverlap(next, start, end));
            }
        }
        bookings.add(List.of(start, end));
        return true;
    }

    private List<Integer> getOverlap(List<Integer> next, int start, int end) {
        int prevStart = next.get(0), prevEnd = next.get(1);
        return List.of(Math.max(prevStart, start), Math.min(prevEnd, end));
    }

    private boolean isOverlap(List<Integer> next, int start, int end) {
        int prevStart = next.get(0), prevEnd = next.get(1);
        return Math.max(prevStart, start) < Math.min(prevEnd, end);
    }
}