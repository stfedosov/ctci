package Leetcode4;

public class FindKthBitInNthBinaryString {

    /*
    Given two positive integers n and k, the binary string S(n) is formed as follows:

    S1 = "0"
    Si = Si - 1 + "1" + reverse(invert(Si - 1)) for i > 1

    Where reverse(x) returns the reversed string x,
    and invert(x) inverts all the bits in x (0 changes to 1 and 1 changes to 0).
     */
    public static void main(String[] args) {
        System.out.println(findKthBit(3, 1)); // 0
        System.out.println(findKthBit(4, 11)); // 1
    }

    // O(n)
    public static char findKthBit(int n, int k) {
        if (n == 1) {
            return '0';
        }

        // total length is 2 ^ n because:
        // 1. string doubles each time and then increases by one, so in total (2 ^ n - 1)
        // 2. since k starts with 1, we need to handle indices based on a 1-based index system, so we add ' + 1' = 2 ^ n
        int lengthOfRequiredString = 1 << n;

        if (k < lengthOfRequiredString / 2) {
            return findKthBit(n - 1, k); // in the first half of a number -> recurse on S(n - 1)
        } else if (k == lengthOfRequiredString / 2) {
            return '1'; // initial requirement S(n) = S(n - 1) + '1' + reverse(invert(S(n - 1))) -> return '1'
        }
        // k > lengthOfRequiredString / 2
        char result = findKthBit(n - 1, lengthOfRequiredString - k);
        return result == '0' ? '1' : '0'; // Invert the bit returned from the recursive call
    }

}
