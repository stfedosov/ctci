package Leetcode4;

public class AreSentencesSimilar {

    public static void main(String[] args) {
        System.out.println(areSentencesSimilar("Eating right now", "Eating"));
    }

    public static boolean areSentencesSimilar(String sentence1, String sentence2) {
        String[] split1 = sentence1.split(" ");
        String[] split2 = sentence2.split(" ");
        return split1.length <= split2.length ?
                areSentencesSimilar(split1, split2) : areSentencesSimilar(split2, split1);
    }

    private static boolean areSentencesSimilar(String[] shorter, String[] longer) {
        // Match prefix
        int start1 = 0;
        while (start1 < shorter.length && shorter[start1].equals(longer[start1])) {
            start1++;
        }

        // Match suffix
        int end1 = shorter.length - 1;
        int end2 = longer.length - 1;
        while (end1 >= start1 && shorter[end1].equals(longer[end2])) {
            end1--;
            end2--;
        }

        // If all words in shorter are matched, it means shorter can be made similar to longer
        return end1 < start1;
    }

}
