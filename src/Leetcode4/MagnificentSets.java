package Leetcode4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class MagnificentSets {

    public static void main(String[] args) {
//        System.out.println(magnificentSets(6, new int[][]{{1, 2}, {1, 4}, {1, 5}, {2, 6}, {2, 3}, {4, 6}})); // 4
        System.out.println(magnificentSets(3, new int[][]{{1, 2}, {2, 3}, {3, 1}})); // 4
    }

    /*
    You are also given a 2D integer array edges, where edges[i] = [ai, bi]
    indicates that there is a bidirectional edge between nodes ai and bi.
    Notice that the given graph may be disconnected.

    Divide the nodes of the graph into m groups (1-indexed) such that:

    Each node in the graph belongs to exactly one group.
    For every pair of nodes in the graph that are connected by an edge [ai, bi],
    if ai belongs to the group with index x, and bi belongs to the group with index y, then |y - x| = 1.
    Return the maximum number of groups into which you can divide the nodes.
    Return -1 if it is impossible to group the nodes with the given conditions.
     */

    // --------
    // APPROACH
    // --------
    // 1. construct a graph
    // 2. Find all disconnected components (dfs starting at each unvisited node)
    // 3. For each component: BFS to find the max levels starting at every node
    // if any of the components returns a -1, meaning there has to be two directly connected nodes in one level, then the whole graph is invalid
    // sum the number of levels/groups from each single component to return the final answer
    public static int magnificentSets(int n, int[][] edges) {
        Map<Integer, List<Integer>> graph = new HashMap<>();

        // construct a bi-directional graph
        for (int[] edge : edges) {
            graph.computeIfAbsent(edge[0], k -> new ArrayList<>()).add(edge[1]);
            graph.computeIfAbsent(edge[1], k -> new ArrayList<>()).add(edge[0]);
        }

        // get all connected components, the graph could be disconnected
        var components = getComponents(graph, n);

        int maxGroups = 0;
        for (List<Integer> component : components) {
            int groups = -1;
            for (int node : component) {
                // bfs to find the number of level starting at this node
                int numLevels = bfs(graph, node);
                if (numLevels == -1) {
                    return -1;
                }
                groups = Math.max(groups, numLevels);
            }
            maxGroups += groups;
        }

        return maxGroups;
    }

    private static List<List<Integer>> getComponents(Map<Integer, List<Integer>> graph, int n) {
        Set<Integer> visited = new HashSet<>();
        List<List<Integer>> components = new ArrayList<>();
        for (int i = 1; i <= n; i++) {
            if (!visited.contains(i)) {
                // dfs to get all node belonging to one component
                components.add(dfs(graph, i, new ArrayList<>(), visited));
            }
        }
        return components;
    }

    private static List<Integer> dfs(Map<Integer, List<Integer>> graph, int curr, List<Integer> nodes, Set<Integer> visited) {
        visited.add(curr);
        nodes.add(curr);
        for (int next : graph.getOrDefault(curr, Collections.emptyList())) {
            if (!visited.contains(next)) {
                dfs(graph, next, nodes, visited);
            }
        }
        return nodes;
    }

    private static int bfs(Map<Integer, List<Integer>> graph, int node) {
        Map<Integer, Node> visited = new HashMap<>();

        Queue<Node> queue = new LinkedList<>();
        Node n = new Node(node, 1);
        queue.offer(n);
        visited.put(node, n);

        int level = 1;
        while (!queue.isEmpty()) {
            Node curr = queue.poll();
            level = curr.level;
            for (int neighbor : graph.getOrDefault(curr.id, Collections.emptyList())) {
                if (!visited.containsKey(neighbor)) {
                    Node next = new Node(neighbor, curr.level + 1);
                    queue.offer(next);
                    visited.put(neighbor, next);
                } else if (visited.get(neighbor).level == curr.level) {
                    // if the neighbor has been seen in the same level as the current node,
                    // it means there has to be a direct connection at this level;
                    // marking the graph invalid
                    return -1;
                }
            }
        }
        return level;
    }

    static class Node {
        int id;
        int level;

        public Node(int id, int level) {
            this.id = id;
            this.level = level;
        }
    }

}
