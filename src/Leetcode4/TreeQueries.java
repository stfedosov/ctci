package Leetcode4;

import Leetcode.TreeNode;

import java.util.HashMap;
import java.util.Map;

public class TreeQueries {

    public static void main(String[] args) {

    }

    public static int[] treeQueries(TreeNode root, int[] queries) {
        Map<Integer, int[]> map = new HashMap<>();
        Map<Integer, Integer> removed = new HashMap<>();
        populateHeights(root, 0, map);
        calculateRemovedHeights(root, 0, map, removed);

        int[] output = new int[queries.length];
        for (int i = 0; i < queries.length; i++) {
            output[i] = removed.get(queries[i]);
        }
        return output;
    }

    // height is the max tree height with this node removed
    private static void calculateRemovedHeights(TreeNode node, int height, Map<Integer, int[]> map, Map<Integer, Integer> removed) {
        if (node == null) {
            return;
        }
        removed.put(node.val, height);
        // for each child, the height when removed is the max of the height
        // of the opposite child, or the passed-in height with this node removed
        // {1=0, 3=3, 2=3, 4=2, 6=3, 5=2, 7=2}
        // for ex, if remove 4, then the height of the tree is the max(subtree with 3
        // height, height) passed in
        // because 4 is removed, so height can't + 1, but remain the same
        calculateRemovedHeights(node.left, Math.max(height, map.get(node.val)[1]), map, removed);
        calculateRemovedHeights(node.right, Math.max(height, map.get(node.val)[0]), map, removed);
    }

    private static int populateHeights(TreeNode node, int height, Map<Integer, int[]> map) {
        if (node == null) {
            return height - 1;
        }

        int left = populateHeights(node.left, height + 1, map);
        int right = populateHeights(node.right, height + 1, map);
        map.put(node.val, new int[]{left, right});// cur node -> height of left child from root, if null, then cur node
        return Math.max(left, right);
    }
}
