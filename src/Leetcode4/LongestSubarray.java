package Leetcode4;

import java.util.Comparator;
import java.util.Deque;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class LongestSubarray {

    public static void main(String[] args) {
        System.out.println(longestSubarray(new int[]{10, 1, 2, 4, 7, 2}, 4));
    }

    public static int longestSubarray(int[] nums, int limit) {
        Deque<Integer> decreasingStack = new LinkedList<>();
        Deque<Integer> increasingStack = new LinkedList<>();
        int left = 0;
        int maxLength = 0;

        for (int right = 0; right < nums.length; right++) {
            // Maintain the decreasingStack in decreasing order
            while (!decreasingStack.isEmpty() && decreasingStack.peekLast() < nums[right]) {
                decreasingStack.pollLast();
            }
            decreasingStack.offerLast(nums[right]);

            // Maintain the increasingStack in increasing order
            while (!increasingStack.isEmpty() && increasingStack.peekLast() > nums[right]) {
                increasingStack.pollLast();
            }
            increasingStack.offerLast(nums[right]);

            // Check if the current window exceeds the limit
            while (decreasingStack.peekFirst() - increasingStack.peekFirst() > limit) {
                // Remove the elements that are out of the current window
                if (decreasingStack.peekFirst() == nums[left]) {
                    decreasingStack.pollFirst();
                }
                if (increasingStack.peekFirst() == nums[left]) {
                    increasingStack.pollFirst();
                }
                ++left;
            }

            maxLength = Math.max(maxLength, right - left + 1);
        }

        return maxLength;
    }

    public static int longestSubarray2(int[] nums, int limit) {
        Queue<int[]> maxHeap = new PriorityQueue<>((a, b) -> b[0] - a[0]);
        Queue<int[]> minHeap = new PriorityQueue<>(Comparator.comparingInt(a -> a[0]));

        int left = 0, maxLength = 0;

        for (int right = 0; right < nums.length; ++right) {
            maxHeap.offer(new int[]{nums[right], right});
            minHeap.offer(new int[]{nums[right], right});

            // Check if the absolute difference between the maximum and minimum values in the current window exceeds the limit
            while (maxHeap.peek()[0] - minHeap.peek()[0] > limit) {
                // Move the left pointer to the right until the condition is satisfied.
                // This ensures we remove the element causing the violation
                left = Math.min(maxHeap.peek()[1], minHeap.peek()[1]) + 1;

                // Remove elements from the heaps that are outside the current window
                while (maxHeap.peek()[1] < left) {
                    maxHeap.poll();
                }
                while (minHeap.peek()[1] < left) {
                    minHeap.poll();
                }
            }

            // Update maxLength with the length of the current valid window
            maxLength = Math.max(maxLength, right - left + 1);
        }

        return maxLength;
    }

}
