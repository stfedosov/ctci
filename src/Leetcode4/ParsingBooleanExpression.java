package Leetcode4;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class ParsingBooleanExpression {

    /*
    '!(subExpr)' that evaluates to the logical NOT of the inner expression subExpr.
    '&(subExpr1, subExpr2, ..., subExprn)' that evaluates to the logical AND of the inner expressions
     subExpr1, subExpr2, ..., subExprn where n >= 1.
    '|(subExpr1, subExpr2, ..., subExprn)' that evaluates to the logical OR of the inner expressions
     subExpr1, subExpr2, ..., subExprn where n >= 1.

    Given a string expression that represents a boolean expression, return the evaluation of that expression.
     */
    public static void main(String[] args) {
        System.out.println(parseBoolExpr("&(|(f))")); // false
        System.out.println(parseBoolExpr("|(f,f,f,t)")); // true
        System.out.println(parseBoolExpr("!(&(f,t))")); // true
    }

    public static boolean parseBoolExpr(String expression) {
        Queue<Character> q = new LinkedList<>();
        for (char c : expression.toCharArray()) {
            if (c != ',') {
                q.offer(c);
            }
        }
        return evaluate(q, '|');
    }

    private static boolean evaluate(Queue<Character> q, char op) {
        Stack<Boolean> stack = new Stack<>();
        while (!q.isEmpty()) {
            char c = q.poll();
            if (c == '&' || c == '|') {
                q.poll();
                stack.push(evaluate(q, c));
            } else if (c == '!') {
                q.poll();
                stack.push(!evaluate(q, '|'));
            } else if (c == ')') {
                break;
            } else {
                if (c == 'f') {
                    stack.push(Boolean.FALSE);
                } else {
                    stack.push(Boolean.TRUE);
                }
            }
        }
        boolean result = stack.pop();
        while (!stack.isEmpty()) {
            boolean next = stack.pop();
            // if there is at least one false and operand is 'AND' -> result will be false regardless of the others
            if (op == '&' && !next) {
                return false;
            }
            // if there is at least one true and operand is 'OR' -> result will be true regardless of the others
            if (op == '|' && next) {
                return true;
            }
        }
        return result;
    }

}
