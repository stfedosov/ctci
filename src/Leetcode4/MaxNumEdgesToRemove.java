package Leetcode4;

public class MaxNumEdgesToRemove {

    // https://leetcode.com/problems/remove-max-number-of-edges-to-keep-graph-fully-traversable/description/
    // Alice and Bob have an undirected graph of n nodes and three types of edges:
    //
    //  Type 1: Can be traversed by Alice only.
    //  Type 2: Can be traversed by Bob only.
    //  Type 3: Can be traversed by both Alice and Bob.
    //  Given an array edges where edges[i] = [typei, ui, vi] represents a bidirectional edge of type typei
    //  between nodes ui and vi, find the maximum number of edges you can remove so that after removing the edges,
    //  the graph can still be fully traversed by both Alice and Bob.
    //  The graph is fully traversed by Alice and Bob if starting from any node, they can reach all other nodes.
    //
    // Return the maximum number of edges you can remove, or return -1 if Alice and Bob cannot fully traverse the graph.

    public static void main(String[] args) {
        System.out.println(maxNumEdgesToRemove(4, new int[][]{{3, 1, 2}, {3, 2, 3}, {1, 1, 3}, {1, 2, 4}, {1, 1, 2}, {2, 3, 4}}));
    }

    public static int maxNumEdgesToRemove(int n, int[][] edges) {
        UnionFind alice = new UnionFind(n);
        UnionFind bob = new UnionFind(n);
        int usedEdges = 0;
        for (int[] edge : edges) {
            if (edge[0] == 3) {
                boolean al = alice.union(edge[1], edge[2]);
                boolean bo = bob.union(edge[1], edge[2]);
                if (al || bo) {
                    usedEdges++;
                }
            }
        }
        for (int[] edge : edges) {
            if (edge[0] == 1) {
                if (alice.union(edge[1], edge[2])) {
                    usedEdges++;
                }
            } else if (edge[0] == 2) {
                if (bob.union(edge[1], edge[2])) {
                    usedEdges++;
                }
            }
        }
        if (alice.components > 1 || bob.components > 1) {
            return -1;
        }
        return edges.length - usedEdges;
    }

    private static class UnionFind {
        int[] parent;
        int[] componentSize;
        int components;

        public UnionFind(int n) {
            this.components = n;
            this.parent = new int[n + 1];
            this.componentSize = new int[n + 1];
            for (int i = 0; i < n + 1; i++) {
                parent[i] = i;
                componentSize[i] = 1;
            }
        }

        int find(int x) {
            if (parent[x] == x) {
                return x;
            }
            return parent[x] = find(parent[x]);
        }

        boolean union(int x, int y) {
            x = find(x);
            y = find(y);
            if (x == y) {
                return false;
            }
            if (componentSize[x] > componentSize[y]) {
                componentSize[x] += componentSize[y];
                parent[y] = x;
            } else {
                componentSize[y] += componentSize[x];
                parent[x] = y;
            }
            components--;
            return true;
        }
    }

}
