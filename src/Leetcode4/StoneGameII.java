package Leetcode4;

import java.util.Arrays;

public class StoneGameII {

    public static void main(String[] args) {

    }

    public static int stoneGameII(int[] piles) {
        int[] prefix = Arrays.copyOf(piles, piles.length);
        for (int i = piles.length - 2; i >= 0; i--) {
            prefix[i] += prefix[i + 1];
        }
        return dfs(prefix, 1, 0);
    }

    private static int dfs(int[] prefix, int M, int pile) {
        if (pile + 2 * M >= prefix.length) {
            return prefix[pile];
        }
        int max = 0;
        for (int x = 1; x <= 2 * M; x++) {
            // the total sum of stones from the current pile to the last pile
            int take = prefix[pile];
            // Recursive 'dfs' call calculates the maximum number of stones Bob can collect if Alice leaves him starting at (pile + x)
            // Subtraction accounts for Bob’s optimal play after Alice takes her stones.
            max = Math.max(max, take - dfs(prefix, Math.max(x, M), pile + x));
        }
        return max;
    }

}
