package Leetcode4;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Skyline {

    public static void main(String[] args) {
        System.out.println(getSkyline_1(new int[][]{{2, 9, 10}, {3, 7, 15}, {5, 12, 12}, {15, 20, 10}, {19, 24, 8}}));
        System.out.println(getSkyline_2(new int[][]{{2, 9, 10}, {3, 7, 15}, {5, 12, 12}, {15, 20, 10}, {19, 24, 8}}));
    }

    // O(n^2)
    public static List<List<Integer>> getSkyline_1(int[][] buildings) {
        Set<Integer> sortedBuildings = new TreeSet<>();
        for (int[] building : buildings) {
            sortedBuildings.add(building[0]);
            sortedBuildings.add(building[1]);
        }
        List<List<Integer>> res = new ArrayList<>();
        for (int sortedBuilding : sortedBuildings) {
            int maxHeight = 0;
            for (int[] building : buildings) {
                if (sortedBuilding >= building[0] && sortedBuilding < building[1]) {
                    maxHeight = Math.max(maxHeight, building[2]);
                }
            }
            if (res.isEmpty() || res.get(res.size() - 1).get(1) != maxHeight) {
                res.add(List.of(sortedBuilding, maxHeight));
            }
        }
        return res;
    }

    // O(nlog(n))
    public static List<List<Integer>> getSkyline_2(int[][] buildings) {
        List<List<Integer>> height = new ArrayList<>();
        for (int[] building : buildings) {
            // start point has negative height value
            height.add(List.of(building[0], -building[2]));
            // end point has normal height value
            height.add(List.of(building[1], building[2]));
        }
        height.sort((a, b) -> {
            if (Objects.equals(a.get(0), b.get(0))) {
                return a.get(1) - b.get(1);
            } else {
                return a.get(0) - b.get(0);
            }
        });
        // Use a maxHeap to store possible heights
        // But priority queue does not support remove in lgn time
        // treemap support add, remove, get max in lgn time, so use treemap here
        // key: height, value: number of this height
        TreeMap<Integer, Integer> map = new TreeMap<>();
        map.put(0, 1);
        // Before starting, the previous max height is 0;
        int prev = 0;
        // visit all points in order
        List<List<Integer>> res = new ArrayList<>();
        for (List<Integer> h : height) {
            // start point, add height
            if (h.get(1) < 0) {
                map.put(-h.get(1), map.getOrDefault(-h.get(1), 0) + 1);
            } else {  // end point, remove height
                if (map.get(h.get(1)) > 1) {
                    map.put(h.get(1), map.get(h.get(1)) - 1);
                } else {
                    map.remove(h.get(1));
                }
            }
            int cur = map.lastKey();
            // compare current max height with previous max height, update result and
            // previous max height if necessary
            if (cur != prev) {
                res.add(List.of(h.get(0), cur));
                prev = cur;
            }
        }
        return res;
    }

}
