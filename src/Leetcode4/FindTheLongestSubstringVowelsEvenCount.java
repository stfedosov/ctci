package Leetcode4;

import java.util.HashMap;
import java.util.Map;

public class FindTheLongestSubstringVowelsEvenCount {

    /*
    Given the string s, return the size of the longest substring containing each vowel an even number of times.
    That is, 'a', 'e', 'i', 'o', and 'u' must appear an even number of times.
     */
    public static void main(String[] args) {
        System.out.println(findTheLongestSubstring("eleetminicoworoep")); // 13
        // The longest substring is "leetminicowor"
        // which contains two each of the vowels: e, i and o and zero of the vowels: a and u.
    }

    public static int findTheLongestSubstring(String s) {
        // Map to store the first occurrence of each state
        Map<Integer, Integer> stateIndexMap = new HashMap<>();

        // Initial state, before processing any characters
        stateIndexMap.put(0, -1);

        // Vowel positions: a -> 0, e -> 1, i -> 2, o -> 3, u -> 4
        int state = 0;
        int maxLength = 0;

        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);

            // Update the state if the character is a vowel
            if (ch == 'a') {
                state ^= (1 << 0); // toggle bit for 'a'
            } else if (ch == 'e') {
                state ^= (1 << 1); // toggle bit for 'e'
            } else if (ch == 'i') {
                state ^= (1 << 2); // toggle bit for 'i'
            } else if (ch == 'o') {
                state ^= (1 << 3); // toggle bit for 'o'
            } else if (ch == 'u') {
                state ^= (1 << 4); // toggle bit for 'u'
            }

            // If the state has been seen before, calculate the length of the substring
            if (stateIndexMap.containsKey(state)) {
                maxLength = Math.max(maxLength, i - stateIndexMap.get(state));
            } else {
                // Store the first occurrence of this state
                stateIndexMap.put(state, i);
            }
        }

        return maxLength;
    }

}
