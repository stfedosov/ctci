package Leetcode4;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class MaximumInvitations {

    public static void main(String[] args) {

    }

    public static int maximumInvitations(int[] favorite) {
        int n = favorite.length;
        int[] indegree = new int[n];

        // 1. Get indegrees of all nodes
        for (int favor : favorite) {
            if (favor == -1) continue;
            indegree[favor]++;
        }

        // 2. Trim nodes with 0 indegree iteratively to make a circle (topological sort: BFS)
        //    and calculate the depth of all nodes at the same time
        int[] depth = new int[n];
        Arrays.fill(depth, 1);
        for (int u = 0; u < n; u++) {
            while (indegree[u] == 0 && favorite[u] != -1) {
                int v = favorite[u];
                favorite[u] = -1;
                indegree[v]--;
                depth[v] = Math.max(depth[v], 1 + depth[u]);
                u = v;
            }
        }

        // 3. Find the longest cycle among all cycles after trimming 0 indegree nodes (BFS)
        //    If the cycle length is more than 3, just use max, if it is 2 use total to add all of them
        Set<Integer> visited = new HashSet<>();
        int res = 0, totalLinkLoop2 = 0;
        for (int u = 0; u < n; u++) {
            if (indegree[u] == 0 || visited.contains(u)) continue;

            int ans = 0;
            while (!visited.contains(favorite[u])) {
                visited.add(favorite[u]);
                int v = favorite[u];
                ans++;
                u = v;
            }

            if (ans == 2) {
                totalLinkLoop2 += depth[u] + depth[favorite[u]];
            } else if (ans >= 3) {
                res = Math.max(res, ans);
            }
        }

        return Math.max(res, totalLinkLoop2);
    }
}
