package Leetcode4;

import java.util.Arrays;

public class MinOperations {

    /*
    You are given an integer array nums.
    In one operation, you can replace any element in nums with any integer.

    nums is considered continuous if both of the following conditions are fulfilled:

    - All elements in nums are unique.
    - The difference between the max and the min elements = nums.length - 1.

    For example, nums = [4, 2, 5, 3] is continuous,
    but nums = [1, 2, 3, 5, 6] is not continuous.

    Return the minimum number of operations to make nums continuous.
    */
    public static void main(String[] args) {

    }

    // O(n*log(n))
    public static int minOperations(int[] nums) {
        Arrays.sort(nums);
        int dedupIdx = 1;
        int totalLength = nums.length;
        for (int i = 1; i < totalLength; i++) {
            if (nums[i] != nums[i - 1]) {
                nums[dedupIdx++] = nums[i];
            }
        }
        int min = totalLength;
        for (int start = 0, end = 0; start < dedupIdx; start++) {
            while (end < dedupIdx && nums[end] - nums[start] <= totalLength - 1) {
                end++;
            }
            min = Math.min(min, totalLength - (end - start));
        }
        return min;
    }

}
