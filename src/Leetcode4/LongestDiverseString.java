package Leetcode4;

import java.util.PriorityQueue;
import java.util.Queue;

public class LongestDiverseString {
    // distribute chars
    public static void main(String[] args) {
        System.out.println(longestDiverseString(7, 1, 1)); // aacaabaa
        System.out.println(longestDiverseString(7, 1, 0)); // aabaa
    }

    public static String longestDiverseString(int a, int b, int c) {
        Queue<Pair> q = new PriorityQueue<>((o1, o2) -> o2.num - o1.num);
        if (a > 0) {
            q.offer(new Pair("a", a));
        }
        if (b > 0) {
            q.offer(new Pair("b", b));
        }
        if (c > 0) {
            q.offer(new Pair("c", c));
        }
        StringBuilder sb = new StringBuilder();
        while (!q.isEmpty()) {
            Pair next = q.poll();
            if (sb.length() != 0 && sb.length() >= 2 && sb.substring(sb.length() - 2).equals(next.str + next.str)) {
                if (q.isEmpty()) {
                    continue;
                }
                Pair nextPair = q.poll();
                sb.append(nextPair.str);
                if (nextPair.num - 1 > 0) {
                    q.offer(new Pair(nextPair.str, nextPair.num - 1));
                }
            } else {
                next.num--;
                sb.append(next.str);
            }
            if (next.num > 0) {
                q.offer(new Pair(next.str, next.num));
            }
        }
        return sb.toString();
    }

    private static class Pair {
        String str;
        int num;

        public Pair(String str, int num) {
            this.str = str;
            this.num = num;
        }
    }

}
