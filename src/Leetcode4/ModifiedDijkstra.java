package Leetcode4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class ModifiedDijkstra {

    // Uber HCVs (High Capacity Vehicles) problem
    public static void main(String[] args) {
        System.out.println(findOptimalPath(5,
                new int[]{2, 10, 5, 100, 3},
                8,
                new int[][]{{0, 1, 2}, {0, 3, 5}, {0, 2, 1},
                        {1, 3, 10}, {1, 4, 15}, {2, 3, 7},
                        {2, 4, 90}, {3, 4, 90}},
                0,
                4));
    }

    public static int findOptimalPath(int stops,
                                      int[] credits,
                                      int numOfRoads,
                                      int[][] edgesWithDistances,
                                      int src,
                                      int dst) {
        Map<Integer, List<int[]>> graph = new HashMap<>();
        for (int[] edgeDistance : edgesWithDistances) {
            graph.computeIfAbsent(edgeDistance[0], k -> new ArrayList<>()).add(new int[]{edgeDistance[1], edgeDistance[2]});
            graph.computeIfAbsent(edgeDistance[1], k -> new ArrayList<>()).add(new int[]{edgeDistance[0], edgeDistance[2]});
        }
        int[][] dist = new int[stops][101];
        for (int[] d : dist) {
            Arrays.fill(d, Integer.MAX_VALUE);
        }
        dist[src][credits[src]] = 0;
        Queue<Node> minHeap = new PriorityQueue<>(Comparator.comparingInt(a -> a.distance));
        minHeap.add(new Node(src, 0, credits[src]));
        while (!minHeap.isEmpty()) {
            Node polled = minHeap.poll();
            if (polled.id == dst) {
                return polled.distance;
            }
            for (int[] neighbour : graph.get(polled.id)) {
                int nextNode = neighbour[0];
                int roadLength = neighbour[1];
                int nextCredits = polled.credits + credits[nextNode] - roadLength;

                if (nextCredits >= 0 && nextCredits <= 100 && dist[nextNode][nextCredits] > polled.distance + roadLength) {
                    dist[nextNode][nextCredits] = polled.distance + roadLength;
                    minHeap.offer(new Node(nextNode, dist[nextNode][nextCredits], nextCredits));
                }
            }
        }
        return -1;
    }

    private static class Node {
        private final int id;
        private final int distance;
        private final int credits;
        public Node(int id, int distance, int credits) {
            this.id = id;
            this.distance = distance;
            this.credits = credits;
        }
    }

}
