package Chapter2;

import java.util.HashSet;
import java.util.Set;

/**
 * @author sfedosov.
 */
public class Exercise2_1 {

    public static void main(String[] args) {
        // 1 -> 1 -> 1 -> 2 -> 3 -> 3 -> 4 -> 4 -> 5 -> 5 -> null :
        SinglyLinkedList listWithDuplicates = new SinglyLinkedList(1);
        listWithDuplicates.appendToTail(1);
        listWithDuplicates.appendToTail(1);
        listWithDuplicates.appendToTail(2);
        listWithDuplicates.appendToTail(3);
        listWithDuplicates.appendToTail(3);
        listWithDuplicates.appendToTail(4);
        listWithDuplicates.appendToTail(4);
        listWithDuplicates.appendToTail(5);
        listWithDuplicates.appendToTail(5);

        // 1 -> 2 -> 3 -> 4 -> 5 -> null
        // Approach#1:
        SinglyLinkedList copyOfTheInitialList1 = new SinglyLinkedList(listWithDuplicates);
        assert (copyOfTheInitialList1.length() == listWithDuplicates.length());
        removeDuplicatesWithExtraCursor(copyOfTheInitialList1);
        assert (copyOfTheInitialList1.length() < listWithDuplicates.length());

        // Approach#2:
        SinglyLinkedList copyOfTheInitialList2 = new SinglyLinkedList(listWithDuplicates);
        assert (copyOfTheInitialList2.length() == listWithDuplicates.length());
        removeDuplicatesWithHashSet(copyOfTheInitialList2);
        assert (copyOfTheInitialList2.length() < listWithDuplicates.length());
        // Both approaches are the same
        assert (copyOfTheInitialList1.length() == copyOfTheInitialList2.length());

    }

    private static void removeDuplicatesWithExtraCursor(SinglyLinkedList head) {
        while (head != null) {
            SinglyLinkedList current = head;
            while (current.next != null) {
                if (current.next.data == head.data) {
                    current.next = current.next.next;
                } else {
                    current = current.next;
                }
            }
            head = head.next;
        }
    }

    private static void removeDuplicatesWithHashSet(SinglyLinkedList head) {
        Set<Integer> set = new HashSet<>();
        SinglyLinkedList previous = null;
        while (head != null) {
            if (!set.add(head.data)) {
                previous.next = head.next;
            } else {
                previous = head;
            }
            head = head.next;
        }
    }

}
