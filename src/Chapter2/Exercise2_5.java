package Chapter2;

public class Exercise2_5 {

    public static void main(String[] args) {
        // 1 -> 2 -> 3 -> null
        SinglyLinkedList list1 = new SinglyLinkedList(1);
        list1.appendToTail(2);
        list1.appendToTail(3);

        // 2 -> 4 -> 5 -> null
        SinglyLinkedList list2 = new SinglyLinkedList(2);
        list2.appendToTail(4);
        list2.appendToTail(9);

        // 321 + 942 = 1263, i.e., we should have as a result 3 -> 6 -> 8 -> null
        assert ("3 -> 6 -> 2 -> 1 -> null".equals(sumLists(list1, list2).toString()));

        // 1 -> 2 -> 3 -> 4 -> 5 -> null
        SinglyLinkedList list3 = new SinglyLinkedList(1);
        list3.appendToTail(2);
        list3.appendToTail(3);
        list3.appendToTail(4);
        list3.appendToTail(5);

        // 2 -> 4 -> 5 -> null
        SinglyLinkedList list4 = new SinglyLinkedList(2);
        list4.appendToTail(4);
        list4.appendToTail(5);

        // 54321 + 542 = 54863, i.e., we should have as a result 3 -> 6 -> 8 -> 4 -> 5 -> null
        assert ("3 -> 6 -> 8 -> 4 -> 5 -> null".equals(sumLists(list3, list4).toString()));
    }

    private static SinglyLinkedList sumLists(SinglyLinkedList list1, SinglyLinkedList list2) {
        SinglyLinkedList iterator1 = list1;
        SinglyLinkedList iterator2 = list2;
        SinglyLinkedList result = null;
        int sum = 0;
        int excess = 0;
        while (iterator1 != null || iterator2 != null || excess != 0) {
            int data1 = iterator1 == null ? 0 : iterator1.data;
            int data2 = iterator2 == null ? 0 : iterator2.data;
            sum += data1 + data2 + excess;
            if (sum > 9) {
                excess = 1;
                sum %= 10;
            } else {
                excess = 0;
            }
            if (result == null) {
                result = new SinglyLinkedList(sum);
            } else {
                result.appendToTail(sum);
            }
            sum = 0;
            iterator1 = iterator1 == null ? null : iterator1.next;
            iterator2 = iterator2 == null ? null : iterator2.next;
        }
        return result;
    }

}
