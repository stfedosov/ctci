package Chapter2;

import java.util.HashSet;
import java.util.Set;

/**
 * @author sfedosov.
 */
public class Exercise2_8 {

    public static void main(String[] args) {
        // Not a circular list:
        // 0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6

        SinglyLinkedList list1 = new SinglyLinkedList(0);
        list1.appendToTail(1);
        list1.appendToTail(2);
        list1.appendToTail(3);
        list1.appendToTail(4);
        list1.appendToTail(5);
        list1.appendToTail(6);

        assert (!checkCircularityWithHashSet(list1));
        assert (findBeginning(list1) == null);

        // Circular list:
        // 0 -> 1 -> 2 -> 3 -> 4
        //               /      \
        //              10       5
        //              |        |
        //              9        6
        //               \      /
        //                8 <- 7

        SinglyLinkedList circularityNode = new SinglyLinkedList(3);

        SinglyLinkedList list2 = new SinglyLinkedList(0);
        list2.appendToTail(1);
        list2.appendToTail(2);
        list2.appendToTail(circularityNode);
        list2.appendToTail(4);
        list2.appendToTail(5);
        list2.appendToTail(6);
        list2.appendToTail(7);
        list2.appendToTail(8);
        list2.appendToTail(9);
        list2.appendToTail(10);
        list2.appendToTail(circularityNode);

        assert (checkCircularityWithHashSet(list2));

        SinglyLinkedList beginning = findBeginning(list2);
        assert (beginning != null);
        assert (beginning.data == 3);
    }

    private static boolean checkCircularityWithHashSet(SinglyLinkedList list) {
        Set<Integer> setOfValues = new HashSet<>();
        SinglyLinkedList iterator = list;
        while (iterator != null) {
            if (!setOfValues.add(iterator.data)) {
                System.out.println(iterator.data);
                return true;
            }
            iterator = iterator.next;
        }
        return false;
    }

    private static SinglyLinkedList findBeginning(SinglyLinkedList head) {
        SinglyLinkedList slow = head;
        SinglyLinkedList fast = head;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
            if (fast == slow) {
                break;
            }
        }
        if (fast == null || fast.next == null) {
            return null;
        }
        slow = head;
        while (slow != fast) {
            slow = slow.next;
            fast = fast.next;
        }
        return fast;

    }

}
