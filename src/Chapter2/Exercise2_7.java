package Chapter2;

/**
 * @author sfedosov.
 */
public class Exercise2_7 {

    public static void main(String[] args) {
        SinglyLinkedList list1 = new SinglyLinkedList(0);
        list1.appendToTail(1);
        list1.appendToTail(2);
        list1.appendToTail(3);
        list1.appendToTail(4);
        list1.appendToTail(5);
        list1.appendToTail(6);

        SinglyLinkedList list2 = new SinglyLinkedList(7);
        list2.appendToTail(8);
        list2.appendToTail(4);
        list2.appendToTail(5);
        list2.appendToTail(6);

        // Not intersecting lists:
        // 0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6
        // 7 -> 8 -> 4 -> 5 -> 6

        assert (!checkIntersection(list1, list2));

        SinglyLinkedList intersectionNode = new SinglyLinkedList(4);
        intersectionNode.appendToTail(5);
        intersectionNode.appendToTail(6);

        SinglyLinkedList list3 = new SinglyLinkedList(0);
        list3.appendToTail(1);
        list3.appendToTail(2);
        list3.appendToTail(3);
        list3.appendToTail(intersectionNode);

        SinglyLinkedList list4 = new SinglyLinkedList(7);
        list4.appendToTail(8);
        list4.appendToTail(intersectionNode);

        // Intersecting lists:
        // 0 -> 1 -> 2 -> 3
        //                 \
        //                  4 -> 5 -> 6
        //                 /
        //           7 -> 8
        assert (checkIntersection(list3, list4));

    }

    private static boolean checkIntersection(SinglyLinkedList list1,
                                             SinglyLinkedList list2) {
        int lengthOfList1 = list1.length();
        int lengthOfList2 = list2.length();
        int difference = Math.abs(lengthOfList1 - lengthOfList2);
        if (lengthOfList1 >= lengthOfList2) {
            return iterateThroughTheLists(list1, list2, difference);
        } else {
            return iterateThroughTheLists(list2, list1, difference);
        }
    }

    private static boolean iterateThroughTheLists(SinglyLinkedList list1,
                                                  SinglyLinkedList list2,
                                                  int nodesToSkip) {
        SinglyLinkedList iterator1 = list1;
        SinglyLinkedList iterator2 = list2;
        int i = 1;
        while (iterator1 != null) {
            if (i > nodesToSkip) {
                if (iterator1 == iterator2) {
                    System.out.println("Entered lists are intersecting with each other, intersection node is :" + iterator1.data);
                    return true;
                } else {
                    iterator2 = iterator2.next;
                }
            }
            i++;
            iterator1 = iterator1.next;
        }
        return false;
    }

}
