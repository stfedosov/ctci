package Chapter2;

/**
 * @author sfedosov.
 */
public class SinglyLinkedList {

    int data;
    SinglyLinkedList next = null;

    public SinglyLinkedList(int data) {
        this.data = data;
    }

    public SinglyLinkedList(SinglyLinkedList toCopy) {
        this(toCopy.data);
        this.next = toCopy.next;
    }

    public void appendToTail(int data) {
        SinglyLinkedList end = new SinglyLinkedList(data);
        SinglyLinkedList current = this;
        while (current.next != null) {
            current = current.next;
        }
        current.next = end;
    }

    public void appendToTail(SinglyLinkedList node) {
        SinglyLinkedList current = this;
        while (current.next != null) {
            current = current.next;
        }
        current.next = node;
    }

    public int length() {
        int length = 0;
        SinglyLinkedList current = this;
        while (current != null) {
            length++;
            current = current.next;
        }
        return length;
    }

    @Override
    public String toString() {
        return data + " -> " + (next == null ? "none" : next);
    }

}
