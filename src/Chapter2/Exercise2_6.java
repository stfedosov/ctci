package Chapter2;

/**
 * @author sfedosov.
 */
public class Exercise2_6 {

    public static void main(String[] args) {
        // Not a palindrome: 0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6
        SinglyLinkedList list1 = new SinglyLinkedList(0);
        list1.appendToTail(1);
        list1.appendToTail(2);
        list1.appendToTail(3);
        list1.appendToTail(4);
        list1.appendToTail(5);
        list1.appendToTail(6);
        assert (!isPalindrome(list1));
        // Palindrome: 0 -> 1 -> 2 -> 3 -> 2 -> 1 -> 0
        SinglyLinkedList list2 = new SinglyLinkedList(0);
        list2.appendToTail(1);
        list2.appendToTail(2);
        list2.appendToTail(3);
        list2.appendToTail(2);
        list2.appendToTail(1);
        list2.appendToTail(0);
        assert (isPalindrome(list2));
    }

    private static boolean isPalindrome(SinglyLinkedList list) {
        SinglyLinkedList copied = new SinglyLinkedList(list);
        SinglyLinkedList reversed = null;
        while (copied != null) {
            SinglyLinkedList n = new SinglyLinkedList(copied.data);
            n.next = reversed;
            reversed = n;
            copied = copied.next;
        }
        SinglyLinkedList iterator = list;
        while (iterator != null && reversed != null) {
            if (iterator.data != reversed.data) {
                return false;
            }
            iterator = iterator.next;
            reversed = reversed.next;
        }
        return true;
    }

}
