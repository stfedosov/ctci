import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.stream.Collectors;


public class Reverse {

    public static void main(String[] args) {
        reverseContent(args[0]);
//        reverseContentInPlace(args[0]);
//        reverseContentJava8(args[0]);
//        reverseByCharacterRandomAccessFile(args[0]);
    }

    // -- 1st approach

    private static void reverseContentJava8(String arg) {
        try {
            Path path = Paths.get(arg);
            Deque<String> stack = Files
                    .lines(path)
                    .map(s -> new StringBuilder(s).reverse().toString())
                    .collect(Collectors.toCollection(ArrayDeque::new));
            Files.write(path, stack);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    // -- 2nd approach

    private static void reverseContent(String arg) {
        File in = new File(arg);
        try (RandomAccessFile rw = new RandomAccessFile(in, "rw")) {
            reverseAndWriteData(rw, readContent(rw, (int) in.length()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static byte[] readContent(RandomAccessFile rw, int length) throws IOException {
        byte[] bytes = new byte[length];
        rw.readFully(bytes);
        return bytes;
    }

    private static void reverseAndWriteData(RandomAccessFile rw, byte[] bytes) throws IOException {
        reverseArray(bytes);
        rw.seek(0);
        rw.write(bytes);
    }

    private static void reverseArray(byte[] bytes) {
        for (int start = 0, end = bytes.length - 1; start < end; start++, end--) {
            byte tmp = bytes[start];
            bytes[start] = bytes[end];
            bytes[end] = tmp;
        }
    }

    // -- 3rd approach

    private static void reverseByCharacterRandomAccessFile(String filePath) {
        File in = new File(filePath);
        try (RandomAccessFile rw = new RandomAccessFile(in, "rw")) {
            int length = (int) in.length();
            writeData(rw, readDataReverseOrder(rw, length, length), 0);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void writeData(RandomAccessFile writer, int[] bytes, int seek) throws IOException {
        writer.seek(seek);
        for (int b : bytes) {
            writer.write(b);
        }
    }

    private static int[] readDataReverseOrder(RandomAccessFile reader, int seek, int length) throws IOException {
        int x = 0;
        int[] bytes = new int[length];
        while (seek > 0) {
            reader.seek(--seek);
            bytes[x++] = reader.read();
        }
        return bytes;
    }

    // -- 4th approach

    private static void reverseContentInPlace(String arg) {
        File in = new File(arg);
        try (RandomAccessFile rw = new RandomAccessFile(in, "rw")) {
            long start = 0, end = rw.length() - 1;
            while (start < end) {
                rw.seek(start);
                int first = rw.read();

                rw.seek(end);
                int last = rw.read();

                rw.write(first);

                rw.seek(start);
                rw.write(last);

                start++;
                end--;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}

