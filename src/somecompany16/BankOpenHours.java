package somecompany16;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class BankOpenHours {

    /*
    [QABANK] Bank Hours
Description
Problem Description
You are building a global trading platform. Your clients submit limit orders with a given time-to-live
to a selection of banks, each of which has different trading hours. You want to ensure that
at least one of the selected banks is available to fill the order at any given time.

Design a function to test if the selected banks' collective trading hours cover the
entire duration of the order without any gaps.

Sample inputs - Expected outputs
All the test cases use the following set of banks:

09:00-16:00 Royal Bank of Scotland
11:00-17:00 Morgan Stanley
14:00-20:00 JP Morgan
02:00-07:00 National Australia Bank
Test case is order hours followed by a list of banks. TRADE VALID or TRADE FAILED.

Test case #1 - expected result: SUCCESS

10:00-17:00

Test case #2 - expected result: FAILURE

15:00-21:00

Test case #3 - expected result: FAILURE

04:00-10:00

These test cases do not include wrapped bank or order hours.
     */

    static class Interval {
        LocalTime start;
        LocalTime end;

        Interval(String start, String end) {
            this.start = LocalTime.parse(start);
            this.end = LocalTime.parse(end);
        }

        boolean overlaps(Interval other) {
            return !this.end.isBefore(other.start) && !this.start.isAfter(other.end);
        }

        Interval merge(Interval other) {
            return new Interval(
                    this.start.isBefore(other.start) ? this.start.toString() : other.start.toString(),
                    this.end.isAfter(other.end) ? this.end.toString() : other.end.toString()
            );
        }
    }

    public static String validateTrade(String order, List<String> bankHours) {
        Interval orderInterval = parseInterval(order);
        List<Interval> bankIntervals = new ArrayList<>();

        for (String hours : bankHours) {
            bankIntervals.add(parseInterval(hours));
        }

        List<Interval> mergedIntervals = mergeIntervals(bankIntervals);

        for (Interval interval : mergedIntervals) {
            if (coversInterval(orderInterval, interval)) {
                return "TRADE VALID";
            }
        }

        return "TRADE FAILED";
    }

    private static Interval parseInterval(String hours) {
        String[] parts = hours.split("-");
        return new Interval(parts[0], parts[1]);
    }

    private static List<Interval> mergeIntervals(List<Interval> intervals) {
        intervals.sort(Comparator.comparing(a -> a.start));
        List<Interval> merged = new ArrayList<>();

        for (Interval current : intervals) {
            if (merged.isEmpty() || !merged.get(merged.size() - 1).overlaps(current)) {
                merged.add(current);
            } else {
                Interval last = merged.remove(merged.size() - 1);
                merged.add(last.merge(current));
            }
        }
        return merged;
    }

    private static boolean coversInterval(Interval order, Interval interval) {
        return !order.start.isBefore(interval.start) && !order.end.isAfter(interval.end);
    }

    public static void main(String[] args) {
        // Sample banks' trading hours
        List<String> banks = List.of(
                "09:00-16:00", // Royal Bank of Scotland
                "11:00-17:00", // Morgan Stanley
                "14:00-20:00", // JP Morgan
                "02:00-07:00"  // National Australia Bank
        );

        // Test cases
        System.out.println(validateTrade("10:00-17:00", banks)); // TRADE VALID
        System.out.println(validateTrade("15:00-21:00", banks)); // TRADE FAILED
        System.out.println(validateTrade("04:00-10:00", banks)); // TRADE FAILED
    }

}
