package somecompany16;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Queue;

public class FindMaxFreqNumberImpl implements FindMaxFreqNumber {

    public static void main(String[] args) {
        FindMaxFreqNumber findMaxFreqNumber = new FindMaxFreqNumberImpl();
        findMaxFreqNumber.add(1);
        findMaxFreqNumber.add(2);
        findMaxFreqNumber.add(2);
        findMaxFreqNumber.add(4);
        findMaxFreqNumber.add(4);
        findMaxFreqNumber.add(4);
        findMaxFreqNumber.add(4);
        findMaxFreqNumber.add(5);
        findMaxFreqNumber.add(5);
        findMaxFreqNumber.add(5);
        findMaxFreqNumber.add(3);
        findMaxFreqNumber.removeOldest();
        findMaxFreqNumber.removeOldest();
        findMaxFreqNumber.removeOldest();
        findMaxFreqNumber.removeOldest();
        findMaxFreqNumber.removeOldest();
        System.out.println(findMaxFreqNumber.getMaxFreqNumber());
    }

    private final List<Integer> list = new ArrayList<>();
    private final Map<Integer, Integer> map = new HashMap<>();
    private final Queue<Node> q = new PriorityQueue<>((a, b) -> b.freq - a.freq);

    @Override
    public void add(int num) {
        list.add(num);
        map.put(num, map.getOrDefault(num, 0) + 1);
        q.add(new Node(num, map.get(num)));
    }

    @Override
    public void removeOldest() {
        if (list.isEmpty()) {
            throw new RuntimeException("Sorry, no elements there yet!");
        }
        int toRemove = list.remove(0);
        int freq = map.get(toRemove);
        if (freq == 1) map.remove(toRemove);
        else map.put(toRemove, freq - 1);
        q.remove(new Node(toRemove, freq));
    }

    @Override
    public int getMaxFreqNumber() {
        if (q.isEmpty()) {
            throw new RuntimeException("Sorry, no elements there yet!");
        }
        return q.peek().val;
    }

    private static class Node {
        int val, freq;

        public Node(int val, int freq) {
            this.val = val;
            this.freq = freq;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node node = (Node) o;
            return val == node.val && freq == node.freq;
        }

        @Override
        public int hashCode() {
            return Objects.hash(val, freq);
        }
    }
}

interface FindMaxFreqNumber {
    void add(int num); // adds a number to the sequence

    void removeOldest(); // removes the oldest inserted number

    int getMaxFreqNumber(); // returns (not removes) max frequent number
}
