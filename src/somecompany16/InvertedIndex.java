package somecompany16;

import java.util.*;

public class InvertedIndex {

    /*
     * Click `Run` to execute the snippet below!
     */


    /*
     * To execute Java, please define "static void main" on a class
     * named Solution.
     *
     * If you need more classes, simply define them inline.
     */


    static List<String> livetailStream = List.of(
            "Q: database",
            "Q: Stacktrace",
            "Q: loading failed",
            "L: Database service started",
            "Q: snapshot loading",
            "Q: fail",
            "L: Started processing events",
            "L: Loading main DB snapshot",
            "L: Loading snapshot failed no stacktrace available"
    );

    static List<String> livetailOutput = List.of(
            "ACK: database; ID=1",
            "ACK: Stacktrace; ID=2",
            "ACK: loading failed; ID=3",
            "M: Database service started; Q=1",
            "ACK: snapshot loading; ID=4",
            "ACK: fail; ID=5",
            "M: Loading main DB snapshot; Q=4",
            "M: Loading snapshot failed no stacktrace available; Q=2,3,4"
    );

    // query -> split by ' ' -> term1, term2, ... -> lowercase -> sort (term1, term2, ...) -> put into lists
    // log -> split by ' ' -> term1, term2, ... -> lowercase -> sort (term1, term2, ...) ->

// stacktrace -> set1
// failed loading -> set2
// loading snapshot -> set3

    // available failed loading snapshot stacktrace no -> Set(terms)

    //


    // backtracking -> put all queries into [phrase1 , q1], ordering is hard to maintain

    //
    // query -> split by ' ' -> key1, key2, ... -> lowercase -> [key1, list<Q>], [key2, Q2], [...]
    // Map<Q, termCount>

    // log -> split by ' ' -> key1, key2, ... -> lowercase -> match (partial match, incomplete solution)


    public static void main(String[] args) {

        List<String> output = processStream(livetailStream);
        System.out.print(output);
    }

    private static List<String> processStream(List<String> livetailStream) {
        List<String> output = new ArrayList<>();
        Map<Integer, Set<String>> map = new HashMap<>();
        int queryNumber = 1;
        for (String input : livetailStream) {
            if (input.startsWith("Q")) {
                map.put(queryNumber, preprocess(input));
                System.out.println("ACK: " + queryNumber);
                queryNumber++;
            } else {
                //
                output.add(tryToMatch(input, map));
            }
            // output the original pharse
        }
        return output;
    }

    private static Set<String> preprocess(String input) {
        Set<String> output = new HashSet<>();
        for (String in : input.split("\\s+")) {
            if ("L:".equals(in) || "Q:".equals(in)) {
                continue;
            }
            output.add(in.toLowerCase());
        }
        return output;
    }

    private static String tryToMatch(String query, Map<Integer, Set<String>> map) {
        Set<String> querySet = preprocess(query);
        List<Integer> matchedQueries = new ArrayList<>();

        // increased number of Q -> N++
        // can we amortize N ? when doing a log match
        // query -> term1, term2,... -> [term1, Q1], [term2, Q2],
        // log -> term1, term2,... -> match Q1, Q2,...
        for (Map.Entry<Integer, Set<String>> entry : map.entrySet()) {
            if (querySet.containsAll(entry.getValue())) {
                matchedQueries.add(entry.getKey());
            }
        }
        if (!matchedQueries.isEmpty()) {
            System.out.println(matchedQueries);
            // put matched queries id
        }
        return "";
    }

}
