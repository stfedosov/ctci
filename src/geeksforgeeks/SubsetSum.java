package geeksforgeeks;

/**
 * @author sfedosov on 9/25/18.
 */
public class SubsetSum {

    public static void main(String[] args) {
        assert isSubset(new int[]{3, 34, 4, 12, 5, 2}, 6, 9);
        assert isSubset(new int[]{3, 34, 4, 12, 5, 2}, 6, 46);
        assert isSubsetDP(new int[]{3, 34, 4, 12, 5, 2}, 6, 9);
        assert isSubsetDP(new int[]{3, 34, 4, 12, 5, 2}, 6, 46);
    }

    private static boolean isSubset(int[] ints, int n, int sum) {
        if (sum == 0) return true;
        if (n == 0) return false;
        return isSubset(ints, n - 1, sum) || isSubset(ints, n - 1, sum - ints[n - 1]);
    }

    private static boolean isSubsetDP(final int[] arr, int n, final int sum) {
        final Boolean[][] matrix = new Boolean[sum + 1][n + 1];
        for (int i = 0; i <= sum; i++) {
            matrix[i][0] = false;
        }
        for (int i = 0; i <= arr.length; i++) {
            matrix[0][i] = true;
        }
        return isSubSetSumUtil(arr, matrix, sum, arr.length);
    }

    private static boolean isSubSetSumUtil(final int[] arr, final Boolean[][] matrix, int i, int j) {
        if (i < 0 || j < 0) {
            return false;
        }
        if (matrix[i][j] != null) {
            return matrix[i][j];
        }
        matrix[i][j] = isSubSetSumUtil(arr, matrix, i, j - 1) || isSubSetSumUtil(arr, matrix, i - arr[j - 1], j - 1);
        return matrix[i][j];
    }


}
