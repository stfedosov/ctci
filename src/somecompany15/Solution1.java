package somecompany15;

import java.util.Scanner;

public class Solution1 {

    public Solution1() {}

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int testcases = sc.nextInt();
        for (int testcase = 0; testcase < testcases; testcase++) {
            int N = sc.nextInt();
            int[] array = new int[N];
            for (int i = 0; i < array.length; i++) {
                array[i] = sc.nextInt();
            }
            System.out.printf("Case #%d: %d%n", (testcase + 1), doWork(array));
        }
    }

    private static int doWork(int[] array) {
        int totalCost = 0;
        for (int i = 0; i < array.length - 1; i++) {
            int min = Integer.MAX_VALUE;
            int found_j = 0;
            for (int j = i; j < array.length; j++) {
                if (min >= array[j]) {
                    min = array[j];
                    found_j = j;
                }
            }
            totalCost += (found_j - i + 1);
            reverse(array, i, found_j);
        }
        return totalCost;
    }

    private static void reverse(int[] array, int i, int found_j) {
        while (i < found_j) {
            int tmp = array[i];
            array[i] = array[found_j];
            array[found_j] = tmp;
            i++;
            found_j--;
        }
    }

}