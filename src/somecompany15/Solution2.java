package somecompany15;

import java.util.Arrays;
import java.util.Scanner;

public class Solution2 {

    public Solution2() {}

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int testcases = sc.nextInt();
        for (int testcase = 0; testcase < testcases; testcase++) {
            int X = sc.nextInt();
            int Y = sc.nextInt();
            String mural = sc.next();
            System.out.printf("Case #%d: %d%n", (testcase + 1), doWork(X, Y, mural.toCharArray()));
        }
    }

    private static int doWork(int x, int y, char[] mural) {
        int[][] dp = new int[mural.length][2];
        for (int[] dp_array : dp) Arrays.fill(dp_array, 9999999);
        for (int i = 0; i < mural.length; i++) {
            for (int j = 0; j < 2; j++) {
                if (j == 0 && mural[i] == 'J' || j == 1 && mural[i] == 'C') {
                    continue;
                }
                if (i == 0) {
                    dp[i][j] = 0;
                } else {
                    for (int k = 0; k < 2; k++) {
                        int previous = dp[i - 1][k];
                        if (k == 0 && j == 1) {
                            previous += x;
                        } else if (k == 1 && j == 0) {
                            previous += y;
                        }
                        dp[i][j] = Math.min(dp[i][j], previous);
                    }
                }
            }
        }
        return Math.min(dp[mural.length - 1][0], dp[mural.length - 1][1]);
    }

    private static int doWork(int x, int y, char[] mural, int idx, boolean hasQ) {
        if (!hasQ) {
            return countCost(x, y, mural);
        }
        else if (idx >= mural.length) {
            return countCost(x, y, mural);
        } else {
            int min = Integer.MAX_VALUE;
            for (char c : new char[]{'J', 'C'}) {
                if (mural[idx] != '?') return doWork(x, y, mural, idx + 1, hasQ);
                else {
                    mural[idx] = c;
                    min = Math.min(min, doWork(x, y, mural, idx + 1, hasQ));
                    mural[idx] = '?';
                }
            }
            return min == Integer.MAX_VALUE ? 0 : min;
        }
    }

    private static int countCost(int x, int y, char[] mural) {
        int k = 0, cj = 0, jc = 0;
        while (k < mural.length - 1) {
            if (mural[k] == 'C' && mural[k + 1] == 'J') cj++;
            else if (mural[k] == 'J' && mural[k + 1] == 'C') jc++;
            k++;
        }
        return cj * x + jc * y;
    }

}
