package Chapter5;

/**
 * @author sfedosov.
 */
public class Exercise5_6 {

    public static void main(String[] args) {
        assert countSwapBits(29, 15) == 2;
    }

    private static int countSwapBits(int a, int b) {
        int count = 0;
        for (int c = a ^ b; c != 0; c = c >> 1) {
            count += c & 1;
        }
        return count;
    }

}
