package Chapter5;

/**
 * @author sfedosov.
 */
public class Exercise5_1 {

    public static final String FORMAT = "%32s";

    public static void main(String[] args) {
        int N = 2048;                                         // 0000 1000 0000 0000
        int M = 19;                                           // 0000 0000 0001 0011

        int left = ~0 << 6 + 1;                               // 1111 1111 1000 0000

        int right = (1 << 2) - 1;                             // 0000 0000 0000 0011

        int mask = left | right;                              // 1111 1111 1000 0011

        int N_cleared_space_for_M = N & mask;                 // 0000 1000 0000 0000

        int M_left_shifted = M << 2;                          // 0000 0000 0100 1100

        int result = M_left_shifted | N_cleared_space_for_M;  // 0000 1000 0100 1100
    }

    public static void getStringRepresentation(int number) {
        System.err.println(String.format(FORMAT, Integer.toBinaryString(number)).replace(' ', '0'));
    }

}
