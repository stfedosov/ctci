package Chapter5;

/**
 * @author sfedosov.
 */
public class Exercise5_2 {

    public static final String ERROR = "ERROR";

    public static void main(String[] args) {
        assert convertDoubleToBinary(0.703125).equals("101101");
    }

    private static String convertDoubleToBinary(double number) {
        if (number < 0) {
            return ERROR;
        }
        StringBuilder sb = new StringBuilder();
        while (number > 0) {
            if (sb.length() > 32) {
                return ERROR;
            }
            number *= 2;
            if (number >= 1) {
                sb.append(1);
                number -= 1;
            } else {
                sb.append(0);
            }
        }
        return sb.toString();
    }

}
