package Chapter5;

public class Exercise5_5 {

    public static void main(String[] args) {
        assert checkThatNumberIsPowerOfTwo(2);
        assert !checkThatNumberIsPowerOfTwo(3);
        assert checkThatNumberIsPowerOfTwo(4);
    }

    private static boolean checkThatNumberIsPowerOfTwo(int num) {
        return (num & (num - 1)) == 0;
    }

}
