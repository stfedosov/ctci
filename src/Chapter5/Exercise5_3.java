package Chapter5;

/**
 * @author sfedosov.
 */
public class Exercise5_3 {

    public static void main(String[] args) {
        assert getLongestSequenceOf1s(convertToBinary(1775)) == 8;
        assert getLongestSequenceOf1s(1775) == 8;
    }


    // brute force method start ----------------------------------------
    private static int getLongestSequenceOf1s(StringBuilder string) {
        int maxLeftLength = 0;
        int maxRightLength = 0;
        Integer threshold = 0;
        // iterate from left side
        for (int i = 0, j = string.length() - 1; i < string.length(); j--, i++) {
            if (string.charAt(i) == '0') {
                if (threshold == 0) {
                    threshold++;
                } else {
                    break;
                }
            }
            maxLeftLength++;
        }
        threshold = 0;
        // iterate from right side
        for (int i = string.length() - 1; i > 0; i--) {
            if (string.charAt(i) == '0') {
                if (threshold == 0) {
                    threshold++;
                } else {
                    break;
                }
            }
            maxRightLength++;
        }
        return Math.max(maxLeftLength, maxRightLength);
    }

    private static StringBuilder convertToBinary(int number) {
        final StringBuilder sb = new StringBuilder();
        while (number >= 1) {
            if (number % 2 == 0) {
                sb.append(0);
            } else {
                sb.append(1);
            }
            number /= 2;
        }
        return sb;
    }
    // brute force method end ----------------------------------------

    // solution with using unary operators ---------------------------

    private static int getLongestSequenceOf1s(int number) {
        int currentLength = 0;
        int threshold = 0;
        int max = 1;
        while (number != 0) {
            if ((number & 1) == 0) {
                if (threshold == 0) {
                    threshold++;
                } else {
                    currentLength = 0;
                }
            }
            currentLength++;
            if (currentLength > max) {
                max = currentLength;
            }
            number >>>= 1;
        }
        return max;
    }
}
