package Chapter5;

import java.util.Arrays;

/**
 * @author sfedosov.
 */
public class Exercise5_4 {

    public static void main(String[] args) {
        Exercise5_1.getStringRepresentation(31);
        System.err.println(Arrays.toString(findPairWithTheSameNumberOf1s(31)));
    }

    private static int[] findPairWithTheSameNumberOf1s(int a) {
        int referenceCount = Integer.bitCount(a);
        return new int[]{searchLowestNumberWithSame1s(referenceCount, a), searchLargestNumberWithSame1s(referenceCount, a)};
    }

    private static int searchLargestNumberWithSame1s(int referenceCount, int a) {
        int actualCount = 0;
        while (referenceCount != actualCount) {
            a += 1;
            actualCount = Integer.bitCount(a);
        }
        return a;
    }

    private static int searchLowestNumberWithSame1s(int referenceCount, int a) {
        int actualCount = 0;
        while (referenceCount != actualCount) {
            a -= 1;
            actualCount = Integer.bitCount(a);
        }
        return a;
    }

}
