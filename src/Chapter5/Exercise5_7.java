package Chapter5;

import java.math.BigInteger;

/**
 * @author sfedosov.
 */
public class Exercise5_7 {

    public static void main(String[] args) {
        Exercise5_1.getStringRepresentation(4);
        Exercise5_1.getStringRepresentation(swapOddEvenBits(4));
        System.err.println(hexToBinary("aaaaaaaa"));
        System.err.println(hexToBinary("55555555"));
    }

    private static int swapOddEvenBits(int i) {
        return (((i & 0xaaaaaaaa) >>> 1) | ((i & 0x55555555) << 1));
    }

    private static String hexToBinary(String hex) {
        int len = hex.length() * 4;
        String bin = new BigInteger(hex, 16).toString(2);

        //left pad the string result with 0s if converting to BigInteger removes them.
        if(bin.length() < len){
            int diff = len - bin.length();
            StringBuilder pad = new StringBuilder();
            for(int i = 0; i < diff; ++i){
                pad = pad.append("0");
            }
            bin = pad.append(bin).toString();
        }
        return bin;
    }

}
