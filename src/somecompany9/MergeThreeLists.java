package somecompany9;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MergeThreeLists {

    public static void main(String[] args) {
        assert mergeLists(
                Arrays.asList(0, 1, 2, 3),
                Arrays.asList(1, 2, 4, 6),
                Arrays.asList(3, 6, 9, 12)).equals(Arrays.asList(0, 1, 2, 3, 4, 6, 9, 12));
    }

    public static List<Integer> mergeLists(List<Integer> a, List<Integer> b, List<Integer> c) {
        return mergeLists(mergeLists(c, b), a);
    }

    private static List<Integer> mergeLists(List<Integer> a, List<Integer> b) {
        List<Integer> result = new ArrayList<>();
        int i = 0, j = 0;
        while (i < a.size() && j < b.size()) {
            if (a.get(i).equals(b.get(j))) {
                result.add(a.get(i));
                i++;
                j++;
            } else if (a.get(i) > b.get(j)) {
                result.add(b.get(j));
                j++;
            } else {
                result.add(a.get(i));
                i++;
            }
        }
        while (i < a.size()) {
            result.add(a.get(i++));
        }
        while (j < b.size()) {
            result.add(b.get(j++));
        }
        return result;
    }

}
