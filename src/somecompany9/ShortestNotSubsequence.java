package somecompany9;

import java.util.LinkedList;
import java.util.Queue;

public class ShortestNotSubsequence {

    public static void main(String[] args) {
        assert shortestNotSubsequence("0").equals("1");
        assert shortestNotSubsequence("1").equals("0");
        assert shortestNotSubsequence("10").equals("0");
        assert shortestNotSubsequence("11").equals("0");
        assert shortestNotSubsequence("01").equals("0");
        assert shortestNotSubsequence("0101010").equals("1111");
        assert shortestNotSubsequence("00101").equals("100");
        assert shortestNotSubsequence("1111").equals("0");
        assert shortestNotSubsequence("00010000").equals("11");
    }

    public static String shortestNotSubsequence(String s) {
        Queue<String> q = new LinkedList<>();
        q.offer("0");
        q.offer("1");
        while (!q.isEmpty()) {
            String next = q.poll();
            if (!isSubsequence(s, next)) {
                // remove leading zeros
                while (next.length() > 1 && next.charAt(0) == '0') next = next.substring(1);
                return next;
            }
            q.offer(next + '0');
            q.offer(next + '1');
        }
        return "";
    }

    private static boolean isSubsequence(String origin, String subseq) {
        if (subseq.length() > origin.length()) return false;
        int idx = 0;
        for (char c : subseq.toCharArray()) {
            idx = origin.indexOf(c, idx);
            if (idx == -1) return false;
            idx++;
        }
        return true;
    }

}
