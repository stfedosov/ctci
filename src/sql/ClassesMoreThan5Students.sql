select class from (
    select class, COUNT(DISTINCT student) as num
    from courses
    group by class) as temp
where num >= 5;

-----------------------------------
Another approach to this problem:
-----------------------------------

select class from courses group by class having count(distinct student) >= 5;

+---------+------------+
| student | class      |
+---------+------------+
| A       | Math       |
| B       | English    |
| C       | Math       |
| D       | Biology    |
| E       | Math       |
| F       | Computer   |
| G       | Math       |
| H       | Math       |
| I       | Math       |
+---------+------------+

Should output:
+---------+
| class   |
+---------+
| Math    |
+---------+

------------------------------------
------------------------------------

+---------+------------+
| student | class      |
+---------+------------+
| A       | Math       |
| B       | English    |
| C       | Math       |
| D       | Biology    |
| E       | Math       |
| F       | Computer   |
| G       | Math       |
| H       | Math       |
| A       | Math       |
+---------+------------+

Should output:
+---------+
| class   |
+---------+