package somecompany14;

import java.util.Arrays;

public class NightRoute {

    public static void main(String[] args) {
        assert nightRoute(new int[][]{{-1, 5, 20}, {21, -1, 10}, {-1, 1, -1}}) == 15;
        assert nightRoute(new int[][]{{-1, 5, 2, 15}, {2, -1, 0, 3}, {1, -1, -1, 9}, {0, 0, 0, 1}}) == 8;
    }

    // Floyd Warshall algorithm for finding the shortest route to each point
    private static int nightRoute(int[][] city) {
        int N = city.length;
        int[][] dist = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                dist[i][j] = city[i][j] >= 0 ? city[i][j] : Integer.MAX_VALUE;
            }
        }

        for (int k = 0; k < N; k++) {
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    if (dist[i][k] != Integer.MAX_VALUE
                            && dist[k][j] != Integer.MAX_VALUE
                            && dist[i][k] + dist[k][j] < dist[i][j]) {
                        dist[i][j] = dist[i][k] + dist[k][j];
                    }
                }
            }
        }
        System.out.println(Arrays.deepToString(dist));
        return dist[0][dist.length - 1];
    }

}
