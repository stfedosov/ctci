package somecompany14;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FindAncestorInDAG {

    public static void main(String[] args) {
        System.out.println(commonAncestorInDAG(new int[][]{{1, 2}, {3, 4}, {1, 3}, {5, 4}}, 3, 4)); // true
        System.out.println(commonAncestorInDAG(new int[][]{{1, 2}, {3, 4}, {1, 3}, {5, 4}}, 3, 5)); // false
        System.out.println(commonAncestorInDAG(new int[][]{{1, 2}, {3, 4}, {1, 3}, {5, 4}}, 3, 2)); // true
    }

    /*

    1 ---> 2
    |
    V
    3 ---> 4 <--- 5

     */

    /*
    Approach:
    a. reverse the graph
    b. dfs from the two nodes and store all nodes reachable in a set
    c. intersection of the two sets has to be > 1
     */

    private static boolean commonAncestorInDAG(int[][] graph, int nodeA, int nodeB) {
        Map<Integer, List<Integer>> g = new HashMap<>();
        for (int[] edge : graph) g.computeIfAbsent(edge[1], x -> new ArrayList<>()).add(edge[0]);
        Set<Integer> setA = new HashSet<>(), setB = new HashSet<>();
        dfs(g, nodeA, setA);
        dfs(g, nodeB, setB);
        setA.retainAll(setB);
        return !setA.isEmpty();
    }

    private static void dfs(Map<Integer, List<Integer>> g, int node, Set<Integer> set) {
        set.add(node);
        for (int nextNode : g.getOrDefault(node, Collections.emptyList())) {
            if (!set.contains(nextNode))
                dfs(g, nextNode, set);
        }
    }


}
