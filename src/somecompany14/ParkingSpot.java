package somecompany14;

public class ParkingSpot {

    public static void main(String[] args) {
        assert parkingSpot(new int[]{3, 2},
                new int[][]{
                        {1, 0, 1, 0, 1, 0},
                        {0, 0, 0, 0, 1, 0},
                        {0, 0, 0, 0, 0, 1},
                        {1, 0, 1, 1, 1, 1}},
                new int[]{1, 1, 2, 3});
        assert !parkingSpot(new int[]{3, 2},
                new int[][]{
                        {1, 0, 1, 0, 1, 0},
                        {1, 0, 0, 0, 1, 0},
                        {0, 0, 0, 0, 0, 1},
                        {1, 0, 0, 0, 1, 1}},
                new int[]{1, 1, 2, 3});
        assert parkingSpot(new int[]{4, 1},
                new int[][]{
                        {1, 0, 1, 0, 1, 0},
                        {1, 0, 0, 0, 1, 0},
                        {0, 0, 0, 0, 0, 1},
                        {1, 0, 1, 0, 1, 1}},
                new int[]{0, 3, 3, 3});
        assert parkingSpot(new int[]{2, 1},
                new int[][]{
                        {1, 1, 1, 1},
                        {1, 0, 0, 0},
                        {1, 0, 1, 0}},
                new int[]{1, 2, 1, 3});
        assert parkingSpot(new int[]{7, 2},
                new int[][]{
                        {0, 0, 0, 0, 0, 0, 0, 0},
                        {1, 0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 0, 0, 0}},
                new int[]{1, 1, 2, 7});
    }

    private static boolean parkingSpot(int[] carDimensions, int[][] parkingLot, int[] luckySpot) {
        int top = luckySpot[0], bottom = luckySpot[2], left = luckySpot[1], right = luckySpot[3];

        if (Math.abs(top - bottom) > Math.abs(left - right)) { // park vertically
            return check(parkingLot, 0, bottom, left, right) || check(parkingLot, top, parkingLot.length - 1, left, right);
        } else { // park horizontally
            return check(parkingLot, top, bottom, 0, right) || check(parkingLot, top, bottom, left, parkingLot[0].length - 1);
        }
    }

    private static boolean check(int[][] parkingLot, int startRow, int endRow, int startColumn, int endColumn) {
        for (int i = startRow; i <= endRow; i++) {
            for (int j = startColumn; j <= endColumn; j++) {
                if (parkingLot[i][j] == 1) return false;
            }
        }
        return true;
    }

}
