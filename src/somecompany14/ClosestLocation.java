package somecompany14;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class ClosestLocation {

    public static void main(String[] args) {
        assert closestLocation("Cat",
                new int[][]{{-2, 0}, {1, 2}, {2, 1, 2, 4}, {-3, -1, 4, -1}},
                new String[]{"Bat building", "Cats exhibition", "At street", "Cat avenue"}).equals("Cat avenue");

        assert closestLocation("Cat",
                new int[][]{{-3, 0}, {1, 3}, {2, 1, 2, 4}, {-4, -3, 6, -3}},
                new String[]{"Bat building", "Cats exhibition", "At street", "Cat avenue"}).equals("At street");
        assert closestLocation("test",
                new int[][]{{2, 1, 2, 4}, {-1, 2}},
                new String[]{"est dfe", "trest mmm"}).equals("est dfe");

        assert closestLocation("Location",
                new int[][]{{100, -1, 100, 1}, {3, 3}},
                new String[]{"loocationnnn", "lcationaaaa"}).equals("lcationaaaa");

        assert closestLocation("Location",
                new int[][]{{1, 1}, {3, 3}},
                new String[]{"Locati",
                        "Location"}).equals("Location");

        assert closestLocation("Location",
                new int[][]{{100, 0, 200, 0}, {3, 3}},
                new String[]{"location1",
                        "location2"}).equals("location2");

        assert closestLocation("quiz",
                new int[][]{{2, 1, 2, 4}, {-1, 2}},
                new String[]{"uiz aaa",
                        "fuiz bb"}).equals("uiz aaa");

        assert closestLocation("Cat",
                new int[][]{{-3, 0},
                        {1, 3},
                        {2, 1, 2, 4},
                        {-4, -3, 6, -3}},
                new String[]{"Bat building",
                        "Cats exhibition",
                        "Cramp",
                        "cat avenue"}).equals("Bat building");
    }

    private static String closestLocation(String address, int[][] objects, String[] names) {
        Map<String, Object[]> nameToDistance = new HashMap<>();
        for (int i = 0; i < objects.length; i++) {
            nameToDistance.put(names[i], new Object[]{i, calcDist(objects[i])});
        }
        Queue<Object[]> closest = new PriorityQueue<>((a, b) -> {
            int compare = Double.compare((double) a[2], (double) b[2]);
            if (compare == 0) return Integer.compare((int) a[1], (int) b[1]);
            return compare;
        });
        for (String name : nameToDistance.keySet()) {
            String loweredFirstWordName = Character.toLowerCase(name.charAt(0)) + name.substring(1);
            String loweredFirstWordAddress = Character.toLowerCase(address.charAt(0)) + address.substring(1);
            if (oneCharOff(loweredFirstWordName, loweredFirstWordAddress, 0)) {
                Object[] objects1 = nameToDistance.get(name);
                closest.offer(new Object[]{name, objects1[0], objects1[1]});
            }
        }
        return (String) closest.peek()[0];
    }

    private static boolean oneCharOff(String comparable, String toCompare, int diff) {
        if (diff > 1 || comparable.isEmpty() && !toCompare.isEmpty()) return false;
        if (toCompare.isEmpty()) return true;
        if (comparable.charAt(0) == toCompare.charAt(0)) {
            return oneCharOff(comparable.substring(1), toCompare.substring(1), diff);
        } else {
            return oneCharOff(comparable.substring(1), toCompare.substring(1), diff + 1) ||
                    oneCharOff(comparable.substring(1), toCompare, diff + 1) ||
                    oneCharOff(comparable, toCompare.substring(1), diff + 1);
        }
    }

    private static double calcDist(int[] coordinates) {
        if (coordinates.length == 2) {
            int x1 = coordinates[0], y1 = coordinates[1];
            return Math.sqrt(Math.pow(y1, 2) + Math.pow(x1, 2));
        } else {
            int x1 = coordinates[0], x2 = coordinates[2], y1 = coordinates[1], y2 = coordinates[3];
            double min = Double.MAX_VALUE;
            for (int x = x1; x <= x2; x++) {
                for (int y = y1; y <= y2; y++) {
                    if ((x - x1) * (y2 - y1) - (y - y1) * (x2 - x1) == 0) {
                        min = Math.min(min, Math.pow(y, 2) + Math.pow(x, 2));
                    }
                }
            }
            return Math.sqrt(min);
        }
    }

}
