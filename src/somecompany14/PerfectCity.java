package somecompany14;

public class PerfectCity {

    public static void main(String[] args) {
        assert perfectCity(new double[]{2.4, 1}, new double[]{5, 7.3}) == 8.9;
        assert perfectCity(new double[]{0, 0.4}, new double[]{1, 0.6}) == 2;
        assert perfectCity(new double[]{0.4, 1}, new double[]{0.9, 3}) == 2.7;
    }

    static double perfectCity(double[] departure, double[] destination) {
        double x1 = departure[0], x2 = destination[0], y1 = departure[1], y2 = destination[1];
        double distX = Math.abs(x1 - x2), distY = Math.abs(y1 - y2);
        if (Math.floor(x1) == Math.floor(x2)) {
            distX = Math.min(Math.abs(x1 - Math.floor(x1) + x2 - Math.floor(x2)), Math.abs(Math.ceil(x1) - x1 + Math.ceil(x2) - x2));
        }
        if (Math.floor(y1) == Math.floor(y2)) {
            distY = Math.min(Math.abs(y1 - Math.floor(y1) + y2 - Math.floor(y2)), Math.abs(Math.ceil(y1) - y1 + Math.ceil(y2) - y2));
        }
        return distX + distY;
    }
}
