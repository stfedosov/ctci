package somecompany14;

import java.util.HashMap;
import java.util.Map;

public class VisitorService {

    /*
    Мilliоns оf customеrs visit our wеbsite еvеry day,
    fоr еаch customеr wе hаve а иniqиe idеntifier thаt is thе samе еvеry timе thеy visit.
    Wе hаve 2 kinds оf custоmers.
    Rеcurrent Visitоr that visit more than once and OnеTimе Visitоrs, whо sо fаr hаve visitеd the wеbsite оnly oncе.
    Wе wаnt to implеmеnt а service thаt has 2 fиnctionаlitiеs:
    pоstVisit
    gеtFirstSinglеTimеVisitоr
     */

    private static class Node {
        String id;
        Node prev;
        Node next;

        Node(String id) {
            this.id = id;
        }
    }

    private Map<String, Node> singleTimeVisitorMap;  // Stores the id -> Node for single-time visitors
    private Map<String, Boolean> visitMap;        // Stores the visit status of customers (true if recurrent)
    private Node head, tail;

    public VisitorService() {
        singleTimeVisitorMap = new HashMap<>();
        visitMap = new HashMap<>();
        head = new Node("HEAD");
        tail = new Node("TAIL");
        head.next = tail;
        tail.prev = head;
    }

    public void postVisit(String id) {
        if (visitMap.containsKey(id)) {
            // Recurrent visitor case
            if (!visitMap.get(id)) {
                // The visitor was a single-time visitor, now recurrent
                visitMap.put(id, true);
                // Remove from the doubly linked list of single-time visitors
                removeFromList(singleTimeVisitorMap.get(id));
                singleTimeVisitorMap.remove(id);
            }
        } else {
            // First time visitor case
            visitMap.put(id, false); // Mark as single-time visitor for now
            Node newNode = new Node(id);
            // Add to the end of the doubly linked list of single-time visitors
            addToEndOfList(newNode);
            singleTimeVisitorMap.put(id, newNode);
        }
    }

    public String getFirstSingleTimeVisitor() {
        if (head.next == tail) {
            return null; // No single-time visitors
        }
        return head.next.id; // Return the first customer in the single-time visitors list
    }

    // Helper function to remove a node from the doubly linked list
    private void removeFromList(Node node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }

    // Helper function to add a node to the end of the doubly linked list
    private void addToEndOfList(Node node) {
        node.prev = tail.prev;
        node.next = tail;
        tail.prev.next = node;
        tail.prev = node;
    }

    public static void main(String[] args) {
        VisitorService service = new VisitorService();

        service.postVisit("A");
        service.postVisit("B");
        service.postVisit("C");
        System.out.println(service.getFirstSingleTimeVisitor()); // Should return "A"

        service.postVisit("A"); // A becomes a recurrent visitor
        System.out.println(service.getFirstSingleTimeVisitor()); // Should return "B"

        service.postVisit("B"); // B becomes a recurrent visitor
        System.out.println(service.getFirstSingleTimeVisitor()); // Should return "C"

        service.postVisit("D");
        service.postVisit("D");
        service.postVisit("D");
        service.postVisit("S");
        service.postVisit("C");
        System.out.println(service.getFirstSingleTimeVisitor()); // Should return "S"
    }
}

