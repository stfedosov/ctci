package codility.lesson16;

public class MaxNonOverlappingSegments {

    public static void main(String[] args) {
        assert solution2(new int[]{1, 3, 7, 9, 9}, new int[]{5, 6, 8, 9, 10}) == 3;
        assert solution2(new int[]{0, 2, 100}, new int[]{0, 50, 1000}) == 3;
        assert solution2(new int[]{0, 2}, new int[]{0, 50}) == 2;
        assert solution2(new int[]{0}, new int[]{0}) == 1;
        assert solution2(new int[]{}, new int[]{}) == 0;
    }

    public static int solution2(int[] A, int[] B) {
        int N = A.length;
        if (N <= 1) {
            return N;
        }

        int cnt = 1;
        int prev_end = B[0];

        int curr;
        for (curr = 1; curr < N; curr++) {
            if (A[curr] > prev_end) {
                cnt++;
                prev_end = B[curr];
            }
        }

        return cnt;
    }

}
