package codility.lesson16;

/**
 * @author sfedosov on 6/13/18.
 */
public class TieRopes {

    public static void main(String[] args) {
        assert solution(4, new int[]{1, 2, 3, 4, 1, 1, 3}) == 3;
        assert solution(2, new int[]{1, 2}) == 1;
    }

    public static int solution(int K, int[] A) {
        // The number of tied ropes, whose lengths are greater than or equal to K.
        int count = 0;
        // The length of current rope (might be a tied one).
        int length = 0;
        for (int rope : A) {
            length += rope;  // Tied with the previous one.
            // Find a qualified rope. Prepare to find the next one.
            if (length >= K) {
                count += 1;
                length = 0;
            }
        }
        return count;
    }

}
