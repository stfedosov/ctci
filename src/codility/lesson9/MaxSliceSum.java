package codility.lesson9;

/**
 * @author sfedosov on 5/21/18.
 */
public class MaxSliceSum {

    public static void main(String[] args) {
        assert solution(new int[]{3, 2, -6, 4, 0}) == 5;
    }

    public static int solution(int[] A) {
        int max_ending = -1000000;
        int max_slice = -1000000;
        for (int x : A) {
            max_ending = Math.max(x, max_ending + x);
            max_slice = Math.max(max_ending, max_slice);
        }
        return max_slice;
    }

}
