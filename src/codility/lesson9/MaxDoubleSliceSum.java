package codility.lesson9;

/**
 * @author sfedosov on 5/21/18.
 */
public class MaxDoubleSliceSum {

    public static void main(String[] args) {
        assert solution(new int[]{3, 2, 6, -1, 4, 5, -1, 2}) == 17;
    }

    public static int solution(int[] A) {
        int[] fromLeft = new int[A.length];
        int[] fromRight = new int[A.length];
        int max = 0;
        for (int i = 2; i < A.length; i++) {
            fromLeft[i] = Math.max(0, fromLeft[i-1] + A[i-1]);
        }
        for (int i = A.length - 3; i >= 0; i--) {
            fromRight[i] = Math.max(0, fromRight[i+1] + A[i+1]);
        }
        for (int i = 1; i < A.length - 1; i++) {
            max = Math.max(max, fromLeft[i] + fromRight[i]);
        }
        return max;
    }

}
