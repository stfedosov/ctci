package codility;

import java.util.Arrays;

/**
 * @author sfedosov on 5/10/18.
 */
public class DigitalClockFormatter {

    public static void main(String[] args) {
        assert solution(1, 8, 3, 2, 6, 4).equals("12:36:48");
        assert solution(0, 0, 0, 0, 0, 0).equals("00:00:00");
        assert solution(0, 0, 0, 7, 8, 9).equals("07:08:09");
        assert solution(9, 9, 9, 9, 9, 9).equals("NOT POSSIBLE");
        assert solution(2, 4, 5, 9, 5, 9).equals("NOT POSSIBLE");
    }

    private static String solution(int A, int B, int C, int D, int E, int F) {
        int[] ints = new int[]{A, B, C, D, E, F};
        Arrays.sort(ints);
        if (ints[4] < 6 && ints[0] * 10 + ints[1] < 24) { // start from the end, since we have the biggest numbers there
            return "" + ints[0] + ints[1] + ":" + ints[2] + ints[3] + ":" + ints[4] + ints[5];
        }
        if (ints[3] < 6 && ints[0] * 10 + ints[1] < 24) {
            return "" + ints[0] + ints[1] + ":" + ints[2] + ints[4] + ":" + ints[3] + ints[5];
        }
        if (ints[2] < 6 && ints[0] * 10 + ints[3] < 24) {
            return "" + ints[0] + ints[3] + ":" + ints[1] + ints[4] + ":" + ints[2] + ints[5];
        }
        return "NOT POSSIBLE";
    }

}
