package codility.lesson7;

import java.util.Stack;

/**
 * @author sfedosov on 12/20/18.
 */
public class Brackets {

    public int solution(String S) {
        if (S.isEmpty()) return 1;
        Stack<Character> stack = new Stack<>();
        for (char c : S.toCharArray()) {
            switch (c) {
                case '[':
                case '(':
                case '{':
                    stack.push(c);
                    break;
                case ']':
                    if (!stack.isEmpty() && stack.peek() == '[') {
                        stack.pop();
                    } else {
                        stack.push(c);
                    }
                    break;
                case ')':
                    if (!stack.isEmpty() && stack.peek() == '(') {
                        stack.pop();
                    } else {
                        stack.push(c);
                    }
                    break;
                case '}':
                    if (!stack.isEmpty() && stack.peek() == '{') {
                        stack.pop();
                    } else {
                        stack.push(c);
                    }
                    break;
            }
        }
        return stack.isEmpty() ? 1 : 0;
    }

}
