package codility.lesson7;

import java.util.Stack;

/**
 * @author sfedosov on 5/17/18.
 */
public class Nesting {

    public static void main(String[] args) {
        assert solution("(()(())())") == 1;
        assert solution("())") == 0;
    }

    public static int solution(String S) {
        Stack<Character> stack = new Stack<>();
        for (char c : S.toCharArray()) {
            if (c == ')' && !stack.isEmpty()) {
                stack.pop();
            } else {
                stack.push(c);
            }
        }
        return stack.isEmpty() ? 1 : 0;
    }

}
