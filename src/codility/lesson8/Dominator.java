package codility.lesson8;

/**
 * @author sfedosov on 5/21/18.
 */
public class Dominator {

    public static void main(String[] args) {
       assert solution(new int[]{2, 1, 1, 1, 3}) == 3;
    }

    public static int solution(int[] A) {
        int size = 0;
        int candidate = 0;
        for (int aA : A) {
            if (size == 0) {
                size++;
                candidate = aA;
            } else {
                if (candidate != aA)
                    size--;
                else
                    size++;
            }
        }
        if (size <= 0) {
            return -1;
        }
        int count = 0;
        int index = -1;
        for (int i = 0; i < A.length; i++) {
            if (A[i] == candidate) {
                count++;
                index = i;
            }
        }
        if (count > (A.length / 2)) {
            return index;
        }
        return -1;
    }

}
