package codility.lesson8;

import java.util.Stack;

/**
 * @author sfedosov on 5/17/18.
 */
public class EquiLeader {

    public static void main(String[] args) {
        assert solution(new int[]{4, 3, 4, 4, 4, 2}) == 2;
        assert solution(new int[]{6, 8, 4, 6, 8, 6, 6}) == 0;
        assert solution(new int[]{1, 2}) == 0;
    }

    public static int solution(int[] A) {
        int[] ints = determineLeader(A);
        int leader = ints[0];
        int count = ints[1];
        if (leader == -1) {
            return 0;
        }
        int result = 0;
        int numOfLeaders = 0;
        int size = 0;
        for (int aA : A) {
            size++;
            if (aA == leader) {
                numOfLeaders++;
            }
            if (numOfLeaders > size / 2 && (count - numOfLeaders) > (A.length - size) / 2) {
                result++;
            }
        }
        return result;
    }

    private static int[] determineLeader(int[] A) {
        Stack<Integer> stack = new Stack<>();
        int candidate = -1;
        for (int x : A) {
            if (!stack.isEmpty() && stack.peek() != x) {
                stack.pop();
                if (stack.isEmpty()) {
                    candidate = -1;
                }
            } else {
                stack.push(x);
                candidate = x;
            }
        }
        int count = 0;
        if (candidate == -1) {
            return new int[]{-1, -1};
        }
        for (int x : A) {
            if (x == candidate) {
                count++;
            }
        }
        return new int[]{candidate, count};
    }

}
