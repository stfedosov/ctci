package codility.lesson11;

import java.util.Arrays;

/**
 * @author sfedosov on 12/9/18.
 */

/*
A prime is a positive integer X that has exactly two distinct divisors: 1 and X. The first few prime integers are 2, 3, 5, 7, 11 and 13.

A semiprime is a natural number that is the product of two (not necessarily distinct) prime numbers. The first few semiprimes are 4, 6, 9, 10, 14, 15, 21, 22, 25, 26.

You are given two non-empty arrays P and Q, each consisting of M integers. These arrays represent queries about the number of semiprimes within specified ranges.

Query K requires you to find the number of semiprimes within the range (P[K], Q[K]), where 1 ≤ P[K] ≤ Q[K] ≤ N.

For example, consider an integer N = 26 and arrays P, Q such that:

    P[0] = 1    Q[0] = 26
    P[1] = 4    Q[1] = 10
    P[2] = 16   Q[2] = 20

The number of semiprimes within each of these ranges is as follows:

(1, 26) is 10,
(4, 10) is 4,
(16, 20) is 0.

*/
public class CountSemiPrimes {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(solution(26, new int[]{1, 4, 16}, new int[]{26, 10, 20})));
    }

    public static int[] solution(int N, int[] P, int[] Q) {
        if (N < 4) return new int[]{0};
        boolean[] semiprimes = generateSemiPrimes(N);
        // compute "cumulative Count of semiprimes"
        int[] semiprimeCumulateCount = new int[N + 1];
        for (int i = 1; i <= N; i++) {
            semiprimeCumulateCount[i] = semiprimeCumulateCount[i - 1]; // cumulative
            if (semiprimes[i]) {
                semiprimeCumulateCount[i]++;
            }
        }

        // compute "results" (for each query)
        int numQuery = Q.length;
        int[] result = new int[numQuery];
        for (int i = 0; i < numQuery; i++) {
            result[i] = semiprimeCumulateCount[Q[i]] - semiprimeCumulateCount[P[i] - 1]; // note: "P[i]-1" (not included)
        }
        return result;
    }

    private static boolean[] generateSemiPrimes(int N) {
        boolean[] primes = new boolean[N + 1];
        Arrays.fill(primes, true);
        primes[0] = false;
        primes[1] = false;
        for (int i = 2; i < primes.length; i++) {
            if (primes[i]) {
                for (int j = 2; i * j < primes.length; j++) {
                    primes[i * j] = false;
                }
            }
        }
        boolean[] semiprimes = new boolean[N + 1];
        Arrays.fill(semiprimes, false);
        for (int k = 0; k < primes.length; k++) {
            for (int i = 0; i < primes.length; i++) {
                if (primes[i] && primes[k] && k * i < primes.length) {
                    semiprimes[k * i] = true;
                }
            }
        }
        return semiprimes;
    }

}
