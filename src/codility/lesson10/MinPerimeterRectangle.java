package codility.lesson10;

/**
 * @author sfedosov on 5/23/18.
 */
public class MinPerimeterRectangle {

    public static void main(String[] args) {
        // A * B = area
        // 2 * (A + B) = perimeter
        assert solution(48) == 28;
    }

    public static int solution(int N) {
        int min = Integer.MAX_VALUE;
        for (int i = 1; i <= Math.sqrt(N); i++) {
            if (N % i == 0) min = Math.min(min, 2 * (i + N / i));
        }
        return min;
    }

}
