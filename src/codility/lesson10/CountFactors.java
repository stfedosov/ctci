package codility.lesson10;

/**
 * @author sfedosov on 5/29/18.
 */
public class CountFactors {

    public static void main(String[] args) {
        assert solution(24) == 8;
    }

    public static int solution(int N) {
        int res = 0;
        int i = 1;
        while (i * i < N) {
            if (N % i == 0) {
                res += 2;
            }
            i++;
        }
        if (i * i == N) {
            res += 1;
        }
        return res;
    }

}
