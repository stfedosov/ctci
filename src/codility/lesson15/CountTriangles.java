package codility.lesson15;

import java.util.Arrays;

/**
 * @author sfedosov on 6/1/18.
 */
public class CountTriangles {

    public static void main(String[] args) {
        assert solution(new int[]{10, 2, 5, 1, 8, 12}) == 4;
    }

    public static int solution(int[] A) {
        if (A.length <= 2) {
            return 0;
        }
        Arrays.sort(A);
        int result = 0;
        for (int i = 0; i < A.length; i++) {
            int k = 2;
            for (int j = i + 1; j < A.length; j++) {
                while (k < A.length && A[i] + A[j] > A[k]) {
                    k++;
                }
                result += k - j - 1;
            }
        }
        return result;
    }

    private static boolean caterpillar(int[] A, int S) {
        int n = A.length;
        int front = 0, total = 0;
        for (int back : A) {
            while (front < n && total + A[front] <= S) {
                total += A[front];
                front += 1;
            }
            if (total == S) return true;
            total -= back;
        }
        return false;
    }

}
