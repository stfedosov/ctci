package codility.lesson15;

/**
 * @author sfedosov on 6/5/18.
 */
public class CountDistinctSlices {

    public static void main(String[] args) {
//        solution(5, new int[]{4, 3, 3});
        solution(6, new int[]{3, 4, 5, 5, 2});
        // (0,0); (1,2); (0,1); (2,2);
    }

    public static int solution(int M, int[] A) {
        boolean[] used = new boolean[M + 1];
        int distinctSliceNum = 0;
        int back = 0;
        for (int front = 0; front < A.length; front++) {
            while (used[A[front]]) {
                used[A[back]] = false;
                back++;
            }
            used[A[front]] = true;
            distinctSliceNum += front - back + 1;
        }
        return distinctSliceNum;
    }

}
