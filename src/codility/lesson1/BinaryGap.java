package codility.lesson1;

/**
 * @author sfedosov on 4/28/18.
 */
public class BinaryGap {

    public static void main(String[] args) {
        assert solution(20) == 1;
        assert solution(1041) == 5;
        assert solution(15) == 0;
    }

    public static int solution(int N) {
        int max = 0;
        while ((N & 1) != 1) {
            N >>>= 1;
        }
        int tmp = 0;
        while ((N = (N >>> 1)) != 0) {
            if ((N & 1) == 0) {
                tmp++;
                max = Math.max(tmp, max);
            } else {
                tmp = 0;
            }
        }
        return max;
    }

}
