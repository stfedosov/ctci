package codility.lesson2;

import java.util.Arrays;

/**
 * @author sfedosov on 4/28/18.
 */
public class CyclicRotation {

    public static void main(String[] args) {
        assert Arrays.equals(new int[]{9, 7, 6, 3, 8}, solution(new int[]{3, 8, 9, 7, 6}, 3));
        assert Arrays.equals(new int[]{9, 7, 6, 3, 8}, solution2(new int[]{3, 8, 9, 7, 6}, 3));
    }

    public static int[] solution(int[] A, int K) {
        if (A.length == 0) return A;
        while (K > 0) {
            K--;
            int last = A[A.length - 1];
            System.arraycopy(A, 0, A, 1, A.length - 1);
            A[0] = last;
        }
        return A;
    }

    public static int[] solution2(int[] A, int K) {
        if (A.length == 0) return A;
        int[] new_array = new int[A.length];
        for (int i = 0; i < A.length; i++) {
            int new_index = (i + K) % A.length;
            new_array[new_index] = A[i];
        }
        return new_array;
    }

}
