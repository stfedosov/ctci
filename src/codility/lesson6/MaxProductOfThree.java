package codility.lesson6;

import java.util.Arrays;

public class MaxProductOfThree {

    public static void main(String[] args) {
        assert solution(new int[]{-3, 1, 2, -2, 5, 6}) == 60;
    }

    public static int solution(int[] A) {
        Arrays.sort(A);
        int max = A[A.length - 3] * A[A.length - 2] * A[A.length - 1];
        int max2 = A[0] * A[1] * A[A.length - 1];
        return Math.max(max, max2);
    }

}
