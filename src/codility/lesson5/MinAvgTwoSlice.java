package codility.lesson5;

/**
 * @author sfedosov on 5/15/18.
 */
public class MinAvgTwoSlice {

    public static void main(String[] args) {
        assert solution(new int[]{4, 2, 2, 5, 1, 5, 8}) == 1;
        assert solution2(new int[]{4, 2, 2, 5, 1, 5, 8}) == 1;
    }

    // 60%
    public static int solution(int[] A) {
        float min = Float.MAX_VALUE;
        int x = 0;
        for (int i = 0; i < A.length; i++) {
            int k = A[i];
            for (int j = i + 1; j < A.length; j++) {
                k += A[j];
                float v = (float) k / (j - i + 1);
                if (min > v) {
                    min = v;
                    x = i;
                }
            }
        }
        return x;
    }

    // 100%
    public static int solution2(int[] A) {
        float min_avg_value = (A[0] + A[1]) / 2f;   // The minimal average
        int min_avg_pos = 0;     // The begin position of the first slice with minimal average

        for (int i = 0; i < A.length - 2; i++) {
            // Try the next 2 - element slice
            if ((A[i] + A[i + 1]) / 2f < min_avg_value) {
                min_avg_value = (A[i] + A[i + 1]) / 2f;
                min_avg_pos = i;
            }
            // Try the next 3 - element slice
            if ((A[i] + A[i + 1] + A[i + 2]) / 3f < min_avg_value) {
                min_avg_value = (A[i] + A[i + 1] + A[i + 2]) / 3f;
                min_avg_pos = i;
            }
        }

        // Try the last 2-element slice
        if ((A[A.length - 1] + A[A.length - 2]) / 2f < min_avg_value) {
            min_avg_pos = A.length - 2;
        }

        return min_avg_pos;
    }

}
