package codility.lesson5;

/**
 * @author sfedosov on 5/15/18.
 */
public class PassingCars {

    public static void main(String[] args) {
        assert solution(new int[]{0, 1, 0, 1, 1}) == 5;
    }

    public static int solution(int[] A) {
        int countOfZeros = 0, count = 0;
        for (int aA : A) {
            if (aA == 0) countOfZeros++;
            if (aA == 1) count += countOfZeros;
            if (count > 1000000000) return -1;
        }
        return count;
    }

}
