package somecompany2;

import java.util.ArrayList;
import java.util.List;

public class ReduceFraction {

    public static List<String> reduceFraction(List<String> fractions) {
        // Write your code here
        List<String> result = new ArrayList<>();
        for (String fraction : fractions) {
            String[] split = fraction.split("/");
            int num1 = Integer.parseInt(split[0]);
            int num2 = Integer.parseInt(split[1]);
            int greatestCommonDivisor = greatestCommonDivisor(num1, num2);
            result.add(num1 / greatestCommonDivisor + "/" + num2 / greatestCommonDivisor);
        }
        return result;
    }

    public static int greatestCommonDivisor(int a, int b) {
        if (b == 0) return a;
        return greatestCommonDivisor(b, a % b);
    }

    public static int greatestCommonDivisorIter(int a, int b) {
        while (b > 0) {
            int c = a % b;
            a = b;
            b = c;
        }
        return a;
    }


}
