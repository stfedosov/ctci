package somecompany12;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

public class RankArray {

    public static void main(String[] args) {
        assert Arrays.equals(rank(new int[]{4, 2, 3, 7}), new int[]{3, 1, 2, 4});
        assert Arrays.equals(rank(new int[]{-4, -2, -3, -7}), new int[]{2, 4, 3, 1});
    }

    private static int[] rank(int[] input) {
        Map<Integer, Integer> map = new TreeMap<>();
        int i = 1;
        for (int x : input) {
            map.put(x, i++);
        }
        // array value -> position in its initial array
        int rank = 1;
        for (Map.Entry<Integer, Integer> e : map.entrySet()) {
            input[e.getValue() - 1] = rank++;
            // get the appropriate position from sorted mappings and assign a new rank
        }
        return input;
    }

}
