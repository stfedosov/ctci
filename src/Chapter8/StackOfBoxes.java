package Chapter8;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 * @author sfedosov on 7/2/17.
 */
public class StackOfBoxes {

    public static void main(String[] args) {
        Box[] boxes = new Box[]{
                new Box("1", 1, 2, 3),
                new Box("2", 2, 3, 4)};
        System.err.println(createStack(boxes));
    }

    private static int createStack(Box[] boxes) {
        Collections.sort(Arrays.asList(boxes), new Comparator<Box>() {
            @Override
            public int compare(Box o1, Box o2) {
                return o1.getHeight() - o2.getHeight();
            }
        });
        int maxHeight = 0;
        for (int i = 0; i < boxes.length; i++) {
            int height = createStack(boxes, i);
            maxHeight = Math.max(maxHeight, height);
        }
        return maxHeight;
    }

    private static int createStack(Box[] boxes, int i) {
        Box bottom = boxes[i];
        int maxHeight = 0;
        for (int k = i + 1; k < boxes.length; k++) {
            if (boxes[k].canBeAbove(bottom)) {
                int height = createStack(boxes, i);
                maxHeight = Math.max(maxHeight, height);
            }
        }
        maxHeight += bottom.getHeight();
        return maxHeight;
    }

}

final class Box {

    private int width;
    private int height;
    private int depth;
    private String name;

    Box(String name, int height, int depth, int width) {
        this.width = width;
        this.height = height;
        this.depth = depth;
        this.name = name;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getDepth() {
        return depth;
    }

    public String getName() {
        return name;
    }

    public boolean canBeAbove(Box box) {
        return this.width < box.width
                && this.height < box.height
                && this.depth < box.depth;
    }

    @Override
    public String toString() {
        return "Box{" + "name='" + name + '}';
    }
}
