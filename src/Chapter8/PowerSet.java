package Chapter8;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sfedosov on 6/22/17.
 */
public class PowerSet {

    public static void main(String[] args) {
        List<Integer> set = new ArrayList<>();
        set.add(1);
        set.add(2);
        set.add(3);
        System.err.println(powerSet(set));
    }

    private static List<List<Integer>> powerSet(List<Integer> set) {
        int size = 1 << set.size();
        List<List<Integer>> list = new ArrayList<>();
        for (int k = 0; k < size; k++) {
            List<Integer> list1 = convertIntIntoList(k, set);
            list.add(list1);
        }
        return list;
    }

    private static List<Integer> convertIntIntoList(int k, List<Integer> set) {
        List<Integer> list = new ArrayList<>();
        int index = 0;
        for (; k > 0; k >>= 1) {
            if ((k & 1) == 1) {
                list.add(set.get(index));
            }
            index++;
        }
        return list;
    }

}
