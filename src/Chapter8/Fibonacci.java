package Chapter8;

/**
 * @author sfedosov on 6/20/17.
 */
public class Fibonacci {

    public static void main(String[] args) {
        int[] mass = new int[26];
        System.err.println(fib(25, mass));
        System.err.println(fib(25));
    }

    private static int fib(int n, int[] mass) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else if (mass[n] == 0) {
            mass[n] = fib(n - 1, mass) + fib(n - 2, mass);
        }
        return mass[n];
    }

    private static int fib(int n) {
        int first = 0;
        int second = 1;
        int nth = 0;
        for (int i = 2; i <= n; i++) {
            nth = first + second;
            first = second;
            second = nth;
        }
        return nth;
    }


}
