package Chapter8;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * @author sfedosov on 6/27/17.
 */
public class Permutations {

    public static void main(String args[]) {
        List<String> list = new ArrayList<>();
        permute("abc", "", list);
        assert new HashSet<>(list).containsAll(new ArrayList<String>() {{
            add("abc");
            add("cba");
            add("bac");
            add("bca");
            add("acb");
            add("cab");
        }});
    }

    private static void permute(String word, String prefix, List<String> combinations) {
        if (word.isEmpty()) {
            combinations.add(prefix);
        } else {
            for (int i = 0; i < word.length(); i++) {
                permute(word.substring(0, i) + word.substring(i + 1), prefix + word.charAt(i), combinations);
            }
        }
    }
}
