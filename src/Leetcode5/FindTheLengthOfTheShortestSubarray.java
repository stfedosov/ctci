package Leetcode5;

public class FindTheLengthOfTheShortestSubarray {

    public static void main(String[] args) {

    }

    public static int findLengthOfShortestSubarray(int[] arr) {
        int right = arr.length - 1;
        while (right > 0 && arr[right] >= arr[right - 1]) {
            right--;
        }
        int res = right;
        int left = 0;
        while (left < right && (left == 0 || arr[left - 1] <= arr[left])) {
            while (right < arr.length && arr[left] > arr[right]) {
                right++;
            }
            res = Math.min(res, right - left - 1);
            left++;
        }
        return res;
    }

}
