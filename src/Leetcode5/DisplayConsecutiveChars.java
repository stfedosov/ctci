package Leetcode5;

import java.util.*;

public class DisplayConsecutiveChars {

    // Return all characters in an array that appear consecutively max times
    public static List<Character> findMaxConsecutiveChars(char[] arr) {
        if (arr == null || arr.length == 0) return new ArrayList<>();

        List<Character> result = new ArrayList<>();
        int maxCount = 0, currentCount = 1;

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] == arr[i - 1]) {
                currentCount++;
            } else {
                if (currentCount > maxCount) {
                    maxCount = currentCount;
                    result.clear();
                    result.add(arr[i - 1]);
                } else if (currentCount == maxCount) {
                    result.add(arr[i - 1]);
                }
                currentCount = 1;
            }
        }

        // Handle the last sequence
        if (currentCount > maxCount) {
            result.clear();
            result.add(arr[arr.length - 1]);
        } else if (currentCount == maxCount) {
            result.add(arr[arr.length - 1]);
        }

        return result;
    }

    public static void main(String[] args) {
        char[] arr = {'a', 'a', 'b', 'b', 'b', 'c', 'c', 'c', 'c', 'a', 'a'};
        System.out.println(findMaxConsecutiveChars(arr)); // Output: [c]

        char[] arr2 = {'x', 'x', 'y', 'y', 'y', 'x', 'x', 'x'};
        System.out.println(findMaxConsecutiveChars(arr2)); // Output: [y, x]
    }


}
