package Leetcode5;

import Leetcode.RotateList.ListNode;

public class ReverseKGroups {

    public static void main(String[] args) {
        var listNode = new ListNode(1);
        listNode.next = new ListNode(2);
        listNode.next.next = new ListNode(3);
        listNode.next.next.next = new ListNode(4);
        listNode.next.next.next.next = new ListNode(5);
        listNode.next.next.next.next.next = new ListNode(6);
        listNode.next.next.next.next.next.next = new ListNode(7);
        listNode.next.next.next.next.next.next.next = new ListNode(8);
        System.out.println(reverseKGroup(listNode, 3));

        listNode = new ListNode(1);
        listNode.next = new ListNode(2);
        listNode.next.next = new ListNode(3);
        System.out.println(reverseKGroup(listNode, 3));
    }

    public static ListNode reverseKGroup(ListNode head, int k) {
        ListNode dummy = new ListNode(0, head), groupPrev = dummy;  // the node before the group

        while (true) {
            // Find the kth node in the current group
            ListNode kth = groupPrev;
            for (int i = 0; i < k && kth != null; i++) {
                kth = kth.next;
            }
            if (kth == null) {
                return dummy.next;  // less than k nodes remain, so break
            }

            ListNode groupNext = kth.next;  // node after the kth node
            kth.next = null;  // detach the group

            // Reverse the group and connect it back
            ListNode groupHead = reverseList(groupPrev.next);
            ListNode groupTail = groupPrev.next;  // after reversal, this becomes the tail

            groupPrev.next = groupHead;  // attach reversed group to previous part
            groupTail.next = groupNext;  // attach the rest of the list

            // Move groupPrev pointer to the tail of the reversed group
            groupPrev = groupTail;
        }
    }

    private static ListNode reverseList(ListNode head) {
        ListNode prev = null, current = head;
        while (current != null) {
            ListNode next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        return prev;
    }

}
