package Leetcode5;

public class MaxAbsoluteSum {

    public static void main(String[] args) {
        System.out.println(maxAbsoluteSum(new int[]{2, -5, 1, -4, 3, -2})); // 8
        System.out.println(maxAbsoluteSum(new int[]{1, -3, 2, 3, -4})); // 5
    }

    /*
    PrefixSum(end+1)-PrefixSum(start) gives the sum of numbers in [start, ....., end]
    We need to consider all possible (start, end) and find the maximum absolute difference in prefix sums.
    Instead, we greedily choose the maximum and minimum prefix sum and return the difference.
     */
    public static int maxAbsoluteSum(int[] nums) {
        int currentSum = 0, maxPrefixSum = 0, minPrefixSum = 0;
        for (int num : nums) {
            currentSum += num;
            minPrefixSum = Math.min(minPrefixSum, currentSum);
            maxPrefixSum = Math.max(maxPrefixSum, currentSum);
        }
        return maxPrefixSum - minPrefixSum;
    }

}
