package Leetcode5;

public class MyCircularQueue {

    // Different than a fixed size array,
    // a linked list could be more memory efficient,
    // since it does not pre-allocate memory for unused capacity.

    private int capacity;
    private int currentSize = 0;
    private final Node head = new Node(-1);
    private final Node tail = new Node(-1);

    public MyCircularQueue(int k) {
        capacity = k;
        head.next = tail;
        tail.prev = head;
    }

    public boolean enQueue(int value) {
        if (currentSize + 1 > capacity) {
            return false;
        }
        addToEndOfList(new Node(value));
        currentSize++;
        return true;
    }

    private void addToEndOfList(Node node) {
        node.prev = tail.prev;
        node.next = tail;
        tail.prev.next = node;
        tail.prev = node;
    }

    public boolean deQueue() {
        if (isEmpty()) {
            return false;
        }
        currentSize--;
        popHead();
        return true;
    }

    private void popHead() {
        Node poppedNode = head.next;
        poppedNode.prev.next = poppedNode.next;
        poppedNode.next.prev = poppedNode.prev;
    }

    public int Front() {
        return head.next.num;
    }

    public int Rear() {
        return tail.prev.num;
    }

    public boolean isEmpty() {
        return currentSize == 0;
    }

    public boolean isFull() {
        return currentSize == this.capacity;
    }

    static class Node {
        int num;
        Node prev;
        Node next;

        public Node(int num) {
            this.num = num;
        }
    }

}
