package Leetcode5;

import java.util.Arrays;

public class SpiralMatrixFromListNode {

    // convert linked list into spiral matrix
    public static void main(String[] args) {

    }


    public static int[][] spiralMatrix(int m, int n, ListNode head) {
        int[][] res = new int[m][n];
        for (int[] rs : res) {
            Arrays.fill(rs, -1);
        }
        ListNode tmp = head;
        int topBoundary = 0, rightBoundary = res[0].length - 1;
        int leftBoundary = 0, bottomBoundary = res.length - 1;
        while (tmp != null) {
            for (int column = leftBoundary; column <= rightBoundary && tmp != null; column++) {
                res[topBoundary][column] = tmp.val;
                tmp = tmp.next;
            }
            topBoundary++;

            for (int row = topBoundary; row <= bottomBoundary && tmp != null; row++) {
                res[row][rightBoundary] = tmp.val;
                tmp = tmp.next;
            }
            rightBoundary--;

            if (topBoundary <= bottomBoundary) {
                for (int column = rightBoundary; column >= leftBoundary && tmp != null; column--) {
                    res[bottomBoundary][column] = tmp.val;
                    tmp = tmp.next;
                }
                bottomBoundary--;
            }

            if (leftBoundary <= rightBoundary) {
                for (int row = bottomBoundary; row >= topBoundary && tmp != null; row--) {
                    res[row][leftBoundary] = tmp.val;
                    tmp = tmp.next;
                }
                leftBoundary++;
            }
        }
        return res;
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}
