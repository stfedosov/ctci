package Leetcode5;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class MinObstacles {

    public static void main(String[] args) {
        MinObstacles m = new MinObstacles();
        System.out.println(m.minimumObstacles(new int[][]{{0, 1, 1}, {1, 1, 0}, {1, 1, 0}}));
        System.out.println(m.minimumObstacles(new int[][]{{0, 0, 1, 1, 1, 1, 0, 0, 0, 1},
                {0, 1, 1, 1, 1, 1, 1, 0, 1, 1},
                {1, 1, 0, 1, 1, 1, 1, 0, 1, 0},
                {0, 0, 1, 1, 1, 1, 0, 0, 1, 1},
                {1, 0, 1, 0, 0, 0, 1, 1, 1, 0}}));
    }

    private static final int[][] directions = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
    private int globalMin = Integer.MAX_VALUE; // To store the minimum obstacles encountered

    public int minimumObstaclesBacktrack(int[][] grid) {
        int rows = grid.length, cols = grid[0].length;
        boolean[][] visited = new boolean[rows][cols];
        backtrack(grid, 0, 0, visited, 0);
        return globalMin;
    }

    private void backtrack(int[][] grid, int x, int y, boolean[][] visited, int obstacleCount) {
        // Base case: if we reach the bottom-right corner
        if (x == grid.length - 1 && y == grid[0].length - 1) {
            globalMin = Math.min(globalMin, obstacleCount);
            return;
        }

        // Prune: If obstacleCount is already worse than globalMin, stop
        if (obstacleCount >= globalMin) {
            return;
        }

        // Mark the current cell as visited
        visited[x][y] = true;

        for (int[] dir : directions) {
            int nx = x + dir[0], ny = y + dir[1];

            if (isValid(nx, ny, grid, visited)) {
                int newObstacleCount = obstacleCount + grid[nx][ny]; // Increment count if it's an obstacle
                backtrack(grid, nx, ny, visited, newObstacleCount);
            }
        }

        // Backtrack: Unmark the cell as visited
        visited[x][y] = false;
    }

    private boolean isValid(int x, int y, int[][] grid, boolean[][] visited) {
        return x >= 0 && y >= 0 && x < grid.length && y < grid[0].length && !visited[x][y];
    }


    // -----------
    // We can frame this problem as a shortest-path problem with a start and end point,
    // and from each cell, we can move in four directions (up, down, left, right).

    // There are two scenarios for movement:
    //
    //  - Moving to an empty cell costs nothing (edge weight = 0).
    //  - Moving to a cell with an obstacle costs 1 as we must remove it (edge weight = 1).

    // This turns our problem into a graph with edges weighted 0 or 1.
    // The goal is to find the shortest path from the start to the destination using Dijkstra's algorithm.

    // Time complexity: O(m⋅nlog(m⋅n))
    public int minimumObstacles(int[][] grid) {
        int m = grid.length, n = grid[0].length;

        int[][] minObstacles = new int[m][n];

        // Initialize all cells with a large value, representing unvisited cells
        for (int i = 0; i < m; i++) {
            Arrays.fill(minObstacles[i], Integer.MAX_VALUE);
        }

        // Start from the top-left corner, accounting for its obstacle value
        minObstacles[0][0] = grid[0][0];

        Queue<int[]> pq = new PriorityQueue<>(Comparator.comparingInt(a -> a[0]));

        // Add the starting cell to the priority queue
        pq.add(new int[]{minObstacles[0][0], 0, 0});

        while (!pq.isEmpty()) {
            // Process the cell with the fewest obstacles removed so far
            int[] current = pq.poll();
            int obstacles = current[0], row = current[1], col = current[2];

            // If we've reached the bottom-right corner, return the result
            if (row == m - 1 && col == n - 1) {
                return obstacles;
            }

            // Explore all four possible directions from the current cell
            for (int[] dir : directions) {
                int newRow = row + dir[0], newCol = col + dir[1];

                if (isValid(grid, newRow, newCol)) {
                    // Calculate the obstacles removed if moving to the new cell
                    int newObstacles = obstacles + grid[newRow][newCol];

                    // Update if we've found a path with fewer obstacles to the new cell
                    if (newObstacles < minObstacles[newRow][newCol]) {
                        minObstacles[newRow][newCol] = newObstacles;
                        pq.add(new int[]{newObstacles, newRow, newCol});
                    }
                }
            }
        }

        return minObstacles[m - 1][n - 1];
    }

    // Helper method to check if the cell is within the grid bounds
    private boolean isValid(int[][] grid, int row, int col) {
        return row >= 0 && col >= 0 && row < grid.length && col < grid[0].length;
    }

}
