package Leetcode5;

import java.util.Arrays;

public class CountGoodStrings {

    static int mod = 1_000_000_007;

    /*
    Given the integers zero, one, low, and high, we can construct a string
    by starting with an empty string, and then at each step perform either of the following:

    Append the character '0' zero times.
    Append the character '1' one times.

    This can be performed any number of times.

    A good string is a string constructed by the above process having a length between low and high (inclusive).

    Return the number of different good strings that can be constructed satisfying these properties.
    */
    public static void main(String[] args) {
        System.out.println(countGoodStrings(3, 3, 1, 1)); // 8 -> binary strings from "000" to "111"
        System.out.println(countGoodStrings(2, 3, 1, 2)); // 5 -> "00", "11", "000", "110", and "011"
    }

    // recurrence relation: dfs(length) = dfs(length - zero) + dfs(length - one)
    public static int countGoodStrings(int low, int high, int zero, int one) {
        int count = 0;
        int[] cache = new int[high + 1];
        Arrays.fill(cache, -1);
        cache[0] = 1; // base case
        for (int length = low; length <= high; length++) {
            count += dfs(length, zero, one, cache);
            count %= mod;
        }
        return count;
    }

    private static int dfs(int length, int zero, int one, int[] cache) {
        if (cache[length] != -1) {
            return cache[length];
        }
        int count = 0;
        if (length >= one) {
            count += dfs(length - one, zero, one, cache);
        }
        if (length >= zero) {
            count += dfs(length - zero, zero, one, cache);
        }
        cache[length] = count % mod;
        return cache[length];
    }

}
