package Leetcode5;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FindRepeatedDnaSequencesOfLengthK {

    private static final Map<Character, Integer> mapping = Map.of('A', 1, 'C', 2, 'G', 3, 'T', 4);

    public static Set<String> findRepeatedSequences(String dna, int k) {
        int n = dna.length();
        if (n <= k) {
            return Collections.emptySet();
        }

        // rolling hash parameters: base a
        int base = 4, basePowerOfK = (int) Math.pow(base, k);

        int[] nums = new int[n];
        for (int i = 0; i < n; ++i) {
            nums[i] = mapping.get(dna.charAt(i));
        }

        int hash = 0;
        Set<Integer> seen = new HashSet<>();
        Set<String> output = new HashSet<>();
        for (int i = 0; i < k; i++) {
            hash = hash * base + nums[i];
        }
        seen.add(hash);
        // iterate over all sequences of length L
        for (int i = 1; i <= n - k; i++) {
            // compute hash of the current sequence in O(1) time
            hash = hash * base - nums[i - 1] * basePowerOfK + nums[i + k - 1];

            // update output and hashset of seen sequences
            if (seen.contains(hash)) {
                output.add(dna.substring(i, i + k));
            }
            seen.add(hash);
        }
        return output;
    }

    // Driver code
    public static void main(String[] args) {
        List<String> inputsString = Arrays.asList("ACGT", "AGACCTAGAC", "AAAAACCCCCAAAAACCCCCC",
                "GGGGGGGGGGGGGGGGGGGGGGGGG", "TTTTTCCCCCCCTTTTTTCCCCCCCTTTTTTT", "TTTTTGGGTTTTCCA",
                "AAAAAACCCCCCCAAAAAAAACCCCCCCTG", "ATATATATATATATAT");
        List<Integer> inputsK = Arrays.asList(3, 3, 8, 12, 10, 14, 10, 6);

        for (int i = 0; i < inputsK.size(); i++) {
            System.out.println((i + 1) + ".\tInput sequence: " + inputsString.get(i) +
                    "\n\tk: " + inputsK.get(i) +
                    "\n\n\tRepeated sequences: " + findRepeatedSequences(inputsString.get(i), inputsK.get(i)));
            System.out.println("----------------");
        }
    }
}
