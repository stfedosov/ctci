package Leetcode5;

import java.util.Arrays;
import java.util.List;

public class MinMovesToMakePalindrome {

    public int minMovesToMakePalindrome(String s) {
        char[] chars = s.toCharArray();

        // Counter to keep track of the total number of swaps
        int moves = 0;

        // Loop to find a character from the right (s[end]) that
        // matches with a character from the left (s[start])
        int start = 0, end = chars.length - 1;
        while (start < end) {
            int k = end;
            while (k > start) {
                // If a matching character is found
                if (chars[start] == chars[k]) {
                    // Move the matching character to the correct position on the right
                    while (k < end) {
                        // Swap characters
                        swap(chars, k++);
                        // Increment count of swaps
                        moves++;
                    }
                    // Move the right pointer inwards
                    end--;
                    break;
                }
                k--;
            }
            // If no matching character is found, it must be moved to the center of palindrome
            if (k == start) {
                moves += chars.length / 2 - start;
            }
            start++;
        }
        return moves;
    }

    private static void swap(char[] chars, int k) {
        char tmp = chars[k];
        chars[k] = chars[k + 1];
        chars[k + 1] = tmp;
    }

    public static void main(String[] args) {
        List<String> strings = Arrays.asList("ccxx", "arcacer", "w", "ooooooo", "eggeekgbbeg");

        MinMovesToMakePalindrome minMovesToMakePalindrome = new MinMovesToMakePalindrome();
        for (int i = 0; i < strings.size(); ++i) {
            System.out.println((i + 1) + ".\ts: " + strings.get(i));
            System.out.println("\tMoves: " + minMovesToMakePalindrome.minMovesToMakePalindrome(strings.get(i)));
            System.out.println(new String(new char[100]).replace("\0", "-"));
        }
    }

}
