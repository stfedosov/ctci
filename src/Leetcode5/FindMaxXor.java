package Leetcode5;

import java.util.Arrays;

public class FindMaxXor {

    // You are given a sorted array nums of n non-negative integers and an integer maximumBit.
    // You want to perform the following query n times:
    //
    // Find a non-negative integer k < 2^maximumBit such that
    // nums[0] XOR nums[1] XOR ... XOR nums[nums.length-1] XOR k is maximized.
    //
    // k is the answer to the ith query.
    //
    // Remove the last element from the current array nums.
    // Return an array answer, where answer[i] is the answer to the ith query.
    public static void main(String[] args) {
        System.out.println(Arrays.toString(getMaximumXor(new int[]{0, 1, 1, 3}, 2))); // [0,3,2,3]
    }

    public static int[] getMaximumXor(int[] nums, int maximumBit) {
        int[] res = new int[nums.length];
        int xor = 0;
        for (int num : nums) {
            xor ^= num;
        }
        int mask = (1 << maximumBit) - 1;
        // max number of bits will be set if it's reversed:
        // i.e. 2 ^ 3 = 8 = 1000
        // 2 ^ 3 - 1 = 7 = 0111
        for (int i = 0; i < nums.length; i++) {
            res[i] = xor ^ mask;
            xor ^= nums[nums.length - 1 - i]; // remove last element
        }
        return res;
    }

}
