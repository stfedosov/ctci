package Leetcode5;

public class ShiftingLetters {

    public static void main(String[] args) {
        System.out.println(shiftingLetters("abcdert", new int[][]{{0, 4, 0}}));
    }

    // line sweep algorithm
    //
    public static String shiftingLetters(String s, int[][] shifts) {
        char[] chars = s.toCharArray();
        int[] count = new int[s.length() + 1];

        for (int[] shift : shifts) {
            int shiftedLetters = shift[2] == 1 ? 1 : -1;
            count[shift[0]] += shiftedLetters;
            count[shift[1] + 1] -= shiftedLetters;
        }

        int cumulativeSum = 0;
        for (int i = 0; i < count.length - 1; i++) {
            cumulativeSum += count[i];
            int newChar = ((chars[i] - 'a') + cumulativeSum) % 26;
            if (newChar < 0) {
                newChar += 26;
            }
            chars[i] = (char) ('a' + newChar);
        }

        return new String(chars);
    }

}
