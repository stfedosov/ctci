package Leetcode5;

import java.util.Collections;
import java.util.List;

public class CountPairs {

    // Given a 0-indexed integer array nums of length n and an integer target,
    // return the number of pairs (i, j) where 0 <= i < j < n and nums[i] + nums[j] < target.
    public static void main(String[] args) {
        System.out.println(countPairs(List.of(-1, 1, 2, 3, 1), 2)); // 3
        System.out.println(countPairs(List.of(-6, 2, 5, -2, -7, -1, 3), -2)); // 10
    }

    public static int countPairs(List<Integer> nums, int target) {
        Collections.sort(nums);
        int counter = 0, start = 0, end = nums.size() - 1;
        while (start < end) {
            if (nums.get(start) + nums.get(end) < target) {
                // we don't need to do counter++ here, we can do the following shortcut:
                // since 'end' points to the highest number, and it satisfies our main condition,
                // then all the pairs between [start ... end] will also satisfy it
                counter += end - start;
                start++;
            } else {
                end--;
            }
        }
        return counter;
    }

}
