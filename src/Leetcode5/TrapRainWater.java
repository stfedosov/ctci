package Leetcode5;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

class TrapRainWater {

    private static int[][] dirs = new int[][]{{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

    public static int trapRainWater(int[][] heightMap) {
        Queue<Cell> q = new PriorityQueue<>(Comparator.comparingInt(a -> a.height));
        boolean[][] visited = new boolean[heightMap.length][heightMap[0].length];
        // Add the first and last row cells to the boundary and mark them as visited
        for (int row = 0; row < heightMap.length; row++) {
            visited[row][0] = true;
            visited[row][heightMap[0].length - 1] = true;
            q.offer(new Cell(row, 0, heightMap[row][0]));
            q.offer(new Cell(row, heightMap[0].length - 1, heightMap[row][heightMap[0].length - 1]));
        }
        // Add the first and last column cells to the boundary and mark them as visited
        for (int column = 0; column < heightMap[0].length; column++) {
            visited[0][column] = true;
            visited[heightMap.length - 1][column] = true;
            q.offer(new Cell(0, column, heightMap[0][column]));
            q.offer(new Cell(heightMap.length - 1, column, heightMap[heightMap.length - 1][column]));
        }
        int totalWaterTrapped = 0;
        while (!q.isEmpty()) {
            Cell next = q.poll();
            for (int[] dir : dirs) {
                int newX = next.x + dir[0];
                int newY = next.y + dir[1];
                if (isValid(newX, newY, heightMap, visited)) {
                    visited[newX][newY] = true;
                    // If the neighbor's height is less than the current boundary height, water can be trapped
                    // Add the trapped water volume
                    totalWaterTrapped += Math.max(0, next.height - heightMap[newX][newY]);
                    // Push the neighbor into the boundary with updated height (to prevent water leakage)
                    q.offer(new Cell(newX, newY, Math.max(next.height, heightMap[newX][newY])));
                }
            }
        }
        return totalWaterTrapped;
    }

    private static boolean isValid(int x, int y, int[][] heightMap, boolean[][] visited) {
        return x >= 0 && x < heightMap.length && y >= 0 && y < heightMap[x].length && !visited[x][y];
    }

    public static class Cell {
        int x, y, height;

        public Cell(int x, int y, int height) {
            this.x = x;
            this.y = y;
            this.height = height;
        }
    }
}