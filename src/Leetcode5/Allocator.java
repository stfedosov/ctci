package Leetcode5;

import java.util.Map;
import java.util.TreeMap;

public class Allocator {

    private final TreeMap<Integer, Integer> freeBlocks;
    private final TreeMap<Integer, TreeMap<Integer, Integer>> allocatedBlocks;

    public Allocator(final int n) {
        this.freeBlocks = new TreeMap<>();
        this.allocatedBlocks = new TreeMap<>();
        this.freeBlocks.put(0, n);
    }

    public int allocate(final int size, final int mID) {
        for (Map.Entry<Integer, Integer> entry : freeBlocks.entrySet()) {
            int start = entry.getKey(), end = entry.getValue();
            if (end - start >= size) {
                allocatedBlocks.computeIfAbsent(mID, x -> new TreeMap<>()).put(start, start + size);
                freeBlocks.remove(start);
                if (start + size < end) {
                    freeBlocks.put(start + size, end);
                }
                return start;
            }
        }
        return -1;
    }

    public int free(final int mID) {
        if (!allocatedBlocks.containsKey(mID)) {
            return 0;
        }

        int totalFreed = 0;
        TreeMap<Integer, Integer> blocks = allocatedBlocks.get(mID);
        for (Map.Entry<Integer, Integer> entry : blocks.entrySet()) {
            int start = entry.getKey(), end = entry.getValue();
            totalFreed += end - start;
            if (freeBlocks.containsKey(end)) {
                int rightEnd = freeBlocks.get(end);
                freeBlocks.remove(end);
                end = rightEnd;
            }
            Map.Entry<Integer, Integer> leftEntry = freeBlocks.floorEntry(start);
            if (leftEntry != null && leftEntry.getValue() == start) {
                start = leftEntry.getKey();
                freeBlocks.remove(leftEntry.getKey());
            }
            freeBlocks.put(start, end);
        }
        allocatedBlocks.remove(mID);
        return totalFreed;
    }

    public static void main(String[] args) {
        Allocator allocator = new Allocator(50);
        System.out.println(allocator.allocate(12, 6));
        System.out.println(allocator.allocate(28, 16));
        System.out.println(allocator.allocate(17, 23));
        System.out.println(allocator.allocate(50, 23));
        System.out.println(allocator.free(6));
        System.out.println(allocator.free(10));
        System.out.println(allocator.free(10));
        System.out.println(allocator.allocate(16, 8));
        System.out.println(allocator.allocate(17, 41));
        System.out.println(allocator.allocate(44, 27));
        System.out.println(allocator.allocate(12, 45));
    }


}
