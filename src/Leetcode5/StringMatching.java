package Leetcode5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StringMatching {

    /*
    Given an array of string words, return all strings in words that is a substring of another word.
    A substring is a contiguous sequence of characters within a string
     */
    public static void main(String[] args) {
        StringMatching stringMatching = new StringMatching();
        System.out.println(stringMatching.stringMatching(new String[]{"mass", "as", "hero", "superhero"})); // as hero
    }

    // m - a - s - s
    // a - a - s
    // a - s
    // s

    private Trie root = new Trie('*');

    public List<String> stringMatching(String[] words) {
        // Insertions take time/Space complexity O(N * M^2)

        // For each word we insert substrings which takes roughly
        // M + (M - 1) + (M - 2) + ... + 1 = (M * (M + 1)) / 2 ~ M^2
        for (String word : words) {
            for (int i = 0; i < word.length(); i++) {
                convert(word.substring(i));
            }
        }
        // Time O(N * M)
        List<String> strings = new ArrayList<>();
        for (String word : words) {
            Trie node = root;
            for (char c : word.toCharArray()) {
                node = node.children.get(c);
            }
            if (node.counter > 1) {
                strings.add(word);
            }
        }
        return strings;
    }

    private void convert(String word) {
        Trie node = root;
        for (char c : word.toCharArray()) {
            Trie next = node.children.get(c);
            if (next == null) {
                next = new Trie(c);
                node.children.put(c, next);
            }
            node = next;
            node.counter++;
        }
        node.endWord = true;
    }

    static class Trie {
        char c;
        boolean endWord = false;
        int counter = 0;
        Map<Character, Trie> children = new HashMap<>();

        public Trie(char c) {
            this.c = c;
        }

        @Override
        public String toString() {
            return "Trie{" +
                    "c=" + c +
                    ", counter=" + counter +
                    ", children=" + children +
                    '}';
        }
    }

}
