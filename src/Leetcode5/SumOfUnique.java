package Leetcode5;

import java.util.Arrays;

public class SumOfUnique {

    public static void main(String[] args) {
        System.out.println(sumOfUnique(new int[]{1, 2, 3, 3, 3, 3, 4, 4, 5}));
        System.out.println(sumOfUnique(new int[]{1, 2, 3, 2}));
        System.out.println(sumOfUnique(new int[]{1, 1, 1, 1, 1}));
    }

    public static int sumOfUnique(int[] nums) {
        Arrays.sort(nums);
        int candidate = nums[0], sum = 0;
        boolean duplicate = false;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == candidate) {
                duplicate = true;
            } else {
                if (!duplicate) {
                    sum += candidate;
                }
                candidate = nums[i];
                duplicate = false;
            }
        }
        return sum + (duplicate ? 0 : candidate);
    }

}
