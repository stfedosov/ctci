package Leetcode5;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class CountPalindromicSubsequence {

    /*
    Given a string s, return the number of unique palindromes of
    length three that are a subsequence of s.

    Note that even if there are multiple ways to obtain the same subsequence,
    it is still only counted once.

    A palindrome is a string that reads the same forwards and backwards.

    A subsequence of a string is a new string generated from the original string
    with some characters (can be none) deleted without changing the relative order
    of the remaining characters.

    For example, "ace" is a subsequence of "abcde".
     */

    public static void main(String[] args) {
        System.out.println(countPalindromicSubsequence("aabca")); // 3
        // The 3 palindromic subsequences of length 3 are:
        //- "aba" (subsequence of "aabca")
        //- "aaa" (subsequence of "aabca")
        //- "aca" (subsequence of "aabca")
    }

    // fins first occurrence of the letter, and last occurrence
    // given that desired subsequence has max length of 3, any chars in between can form a palindrome
    public static int countPalindromicSubsequence(String s) {
        int[] firstOccurence = new int[26];
        Arrays.fill(firstOccurence, -1);
        int[] lastOccurence = new int[26];
        Arrays.fill(lastOccurence, -1);
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (firstOccurence[c - 'a'] == -1) {
                firstOccurence[c - 'a'] = i;
            }
            lastOccurence[c - 'a'] = i;
        }
        int count = 0;
        for (int i = 0; i < 26; i++) {
            Set<Character> inBetween = new HashSet<>();
            if (firstOccurence[i] == -1 || lastOccurence[i] == -1) {
                continue;
            }
            for (int j = firstOccurence[i] + 1; j < lastOccurence[i]; j++) {
                inBetween.add(s.charAt(j));
            }
            count += inBetween.size();
        }
        return count;
    }

}
