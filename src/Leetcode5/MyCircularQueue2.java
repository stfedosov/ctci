package Leetcode5;

public class MyCircularQueue2 {

    private int tail, head, size;
    private final int capacity;
    private final int[] nums;

    public MyCircularQueue2(int k) {
        this.capacity = k;
        this.nums = new int[capacity];
        this.tail = -1;
    }

    public boolean enQueue(int x) {
        if (size == capacity) {
            return false;
        }
        tail = (tail + 1) % capacity;
        nums[tail] = x;
        size++;
        return true;
    }

    public boolean deQueue() {
        if (size == 0) {
            return false;
        }
        head = (head + 1) % capacity;
        size--;
        return true;
    }

    public int Front() {
        if (size == 0) {
            return -1;
        }
        return nums[head];
    }

    public int Rear() {
        if (size == 0) {
            return -1;
        }
        return nums[tail];
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean isFull() {
        return size == capacity;
    }

}
