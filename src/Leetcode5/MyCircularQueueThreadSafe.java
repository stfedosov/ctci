package Leetcode5;

import java.util.concurrent.locks.ReentrantLock;

public class MyCircularQueueThreadSafe {

    // The ReentrantLock ensures that changes made by one thread are visible to others as soon as the lock is released.
    // So no volatile is necessary as declaring variables volatile can introduce unnecessary overhead.

    private MyCircularQueue.Node head, tail;
    private int count;
    private final int capacity;
    // Additional variable to secure the access of our queue
    private final ReentrantLock queueLock = new ReentrantLock();

    public MyCircularQueueThreadSafe(int k) {
        this.capacity = k;
    }

    public boolean enQueue(int value) {
        // ensure the exclusive access for the following block.
        queueLock.lock();
        try {
            // ... do stuff
        } finally {
            queueLock.unlock();
        }
        return true;
    }

}
