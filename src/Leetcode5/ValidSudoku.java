package Leetcode5;

import java.util.HashSet;
import java.util.Set;

public class ValidSudoku {

    public static void main(String[] args) {
        System.out.println(5 / 3);
        System.out.println(4 / 3);
        System.out.println(6 / 3);
    }

    public static boolean isValidSudoku(char[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == '.') {
                    continue;
                }
                if (!isValidRowsAndColumns(board, i, j) || !isValidQuadrants(board, i, j)) {
                    return false;
                }
            }
        }
        return true;
    }

    private static boolean isValidRowsAndColumns(char[][] board, int i, int j) {
        Set<Character> set = new HashSet<>();
        for (int row = i; row < board.length; row++) {
            if (board[row][j] == '.') {
                continue;
            }
            if (!set.add(board[row][j])) {
                return false;
            }
        }
        set.clear();
        for (int col = j; col < board[0].length; col++) {
            if (board[i][col] != '.') {
                continue;
            }
            if (!set.add(board[i][col])) {
                return false;
            }
        }
        return true;
    }

    private static boolean isValidQuadrants(char[][] board, int i, int j) {
        int rowQ = i / 3;
        int colQ = j / 3;
        int topLeftOfSubBoxRow = 3 * rowQ;
        int topLeftOfSubBoxCol = 3 * colQ;
        Set<Character> set = new HashSet<>();
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                char c = board[topLeftOfSubBoxRow + x][topLeftOfSubBoxCol + y];
                if (c != '.' && !set.add(c)) {
                    return false;
                }
            }
        }
        return true;
    }


}
