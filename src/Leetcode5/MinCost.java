package Leetcode5;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class MinCost {

    /*
    Given an m x n grid. Each cell of the grid has a sign pointing to the next cell you should visit
    if you are currently in this cell. The sign of grid[i][j] can be:

    1 which means go to the cell to the right. (i.e go from grid[i][j] to grid[i][j + 1])
    2 which means go to the cell to the left. (i.e go from grid[i][j] to grid[i][j - 1])
    3 which means go to the lower cell. (i.e go from grid[i][j] to grid[i + 1][j])
    4 which means go to the upper cell. (i.e go from grid[i][j] to grid[i - 1][j])
    Notice that there could be some signs on the cells of the grid that point outside the grid.

    You will initially start at the upper left cell (0, 0).
    A valid path in the grid is a path that starts from the upper left cell (0, 0)
    and ends at the bottom-right cell (m - 1, n - 1) following the signs on the grid.
    The valid path does not have to be the shortest.
     */
    public static void main(String[] args) {
        System.out.println(minCost(new int[][]{{1, 2}, {4, 3}})); // 1
    }

    static int[][] dirs = new int[][]{{0, 1}, {0, -1}, {1, 0}, {-1, 0}};

    // time complexity O(m*n*log(m*n))
    // space O(m*n)
    public static int minCost(int[][] grid) {
        Queue<int[]> q = new PriorityQueue<>(Comparator.comparingInt(a -> a[0]));
        q.offer(new int[]{0, 0, 0});
        int[][] minCost = new int[grid.length][grid[0].length];
        for (int[] minco : minCost) {
            Arrays.fill(minco, Integer.MAX_VALUE);
        }
        minCost[0][0] = 0;
        while (!q.isEmpty()) {
            int[] node = q.poll();
            int cost = node[0], row = node[1], col = node[2];
            for (int dir = 0; dir < 4; dir++) {
                int newRow = row + dirs[dir][0];
                int newCol = col + dirs[dir][1];
                if (isValid(newRow, newCol, grid)) {
                    int newCost = cost + (dir + 1 == grid[row][col] ? 0 : 1);
                    if (minCost[newRow][newCol] > newCost) {
                        minCost[newRow][newCol] = newCost;
                        q.offer(new int[]{newCost, newRow, newCol});
                    }
                }
            }
        }
        return minCost[grid.length - 1][grid[0].length - 1];
    }

    private static boolean isValid(int row, int col, int[][] grid) {
        return row >= 0 && row < grid.length && col >= 0 && col < grid[0].length;
    }

}
