package Leetcode5;


import Leetcode.RotateList.ListNode;

public class SwapNodes {

    public static void main(String[] args) {
        var list = new ListNode(7);
        list.next = new ListNode(4);
        list.next.next = new ListNode(6);
        list.next.next.next = new ListNode(1);
        list.next.next.next.next = new ListNode(5);
        list.next.next.next.next.next = new ListNode(8);
        System.out.println(swapNodes(list, 5));
    }

    public static ListNode swapNodes(ListNode head, int k) {
        ListNode current = head, front;
        int count = 1;
        // first, traverse to kth node
        while (count < k) {
            current = current.next;
            count++;
        }
        front = current; // found kth node from the start
        ListNode end = head; // kth node from the end
        // when current is at the latest node, it means we've traversed to exactly the kth from the end
        while (current.next != null) {
            end = end.next;
            current = current.next;
        }
        swap(front, end);
        return head;
    }

    private static void swap(ListNode front, ListNode end) {
        int tmpData = front.val;
        front.val = end.val;
        end.val = tmpData;
    }
}
