package Leetcode5;

import java.util.HashMap;
import java.util.Map;

public class CountServers {
    /*
    You are given a map of a server center, represented as a m * n integer matrix grid,
    where 1 means that on that cell there is a server and 0 means that it is no server.
    Two servers are said to communicate if they are on the same row or on the same column.

    Return the number of servers that communicate with any other server.
     */
    public static void main(String[] args) {
        System.out.println(countServers(new int[][]{{1, 0}, {1, 1}})); // 3
        System.out.println(countServers(new int[][]{{1, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}})); // 4
    }

    // idea is to count the number of servers in each row/column
    // then, if we know that some particular server is not isolated i.e. it has some other servers on that row/column,
    // we can increment the counter
    public static int countServers(int[][] grid) {
        Map<Integer, Integer> rowPerCount = new HashMap<>(), colPerCount = new HashMap<>();
        for (int row = 0; row < grid.length; row++) {
            for (int col = 0; col < grid[row].length; col++) {
                if (grid[row][col] == 1) {
                    rowPerCount.put(row, rowPerCount.getOrDefault(row, 0) + 1);
                    colPerCount.put(col, colPerCount.getOrDefault(col, 0) + 1);
                }
            }
        }
        int total = 0;
        for (int row = 0; row < grid.length; row++) {
            for (int col = 0; col < grid[row].length; col++) {
                if (grid[row][col] == 1
                        && (rowPerCount.get(row) > 1 || colPerCount.get(col) > 1)) {
                    total++;
                }
            }
        }
        return total;
    }

    // no additional memory, same time complexity
    public int countServersNoAdditionalMemory(int[][] grid) {
        int total = 0;

        for (int row = 0; row < grid.length; row++) {
            int rowCount = 0, serverColIdx = -1;

            // Count the number of servers in the current row and record the
            // first server's column index.
            for (int col = 0; col < grid[0].length; col++) {
                if (grid[row][col] == 1) {
                    if (rowCount == 0) {
                        serverColIdx = col;
                    }
                    rowCount++;
                }
            }

            // If more than one server in the row, it can communicate
            boolean canCommunicate = (rowCount != 1);

            // If there's only one server in the row, check if there's a server
            // in the same column in another row.
            if (!canCommunicate) {
                for (int i = 0; i < grid.length; ++i) {
                    if (i != row && grid[i][serverColIdx] == 1) {
                        canCommunicate = true;
                        break;
                    }
                }
            }

            // If the server can communicate, add rowCount to the result.
            if (canCommunicate) {
                total += rowCount;
            }
        }

        return total;
    }


}
