package Leetcode5;

import java.util.HashMap;
import java.util.Map;

public class FindTargetSumWays {

    public static void main(String[] args) {
        System.out.println(findTargetSumWays(new int[]{1, 1, 1, 1, 1}, 3)); // 5
        // There are 5 ways to assign symbols to make the sum of nums be target 3.
        // -1 + 1 + 1 + 1 + 1 = 3
        // +1 - 1 + 1 + 1 + 1 = 3
        // +1 + 1 - 1 + 1 + 1 = 3
        // +1 + 1 + 1 - 1 + 1 = 3
        // +1 + 1 + 1 + 1 - 1 = 3
    }

    public static int findTargetSumWays(int[] nums, int S) {
        return count(nums, S, 0, 0, new HashMap<>());
    }

    // Time complexity is O(n * Sum) as each unique (index, sum) combination is computed at most once due to memoization.
    // Space complexity is O(n * Sum) as the cache stores up to O(n * Sum) and recursion stack takes O(n)
    private static int count(int[] nums, int targetSum, int idx, int sum, Map<String, Integer> cache) {
        if (idx == nums.length && sum == targetSum) {
            return 1;
        }
        if (idx == nums.length) {
            return 0;
        }

        var key = sum + ":" + idx;
        if (cache.containsKey(key)) {
            return cache.get(key);
        }

        int result = 0;
        //first case we take negative value of current index
        result += count(nums, targetSum, idx + 1, sum - nums[idx], cache);
        //second case we take positive value of current index
        result += count(nums, targetSum, idx + 1, sum + nums[idx], cache);
        cache.put(key, result);
        return result;
    }

}
