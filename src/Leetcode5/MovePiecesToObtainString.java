package Leetcode5;

public class MovePiecesToObtainString {

    public static void main(String[] args) {
        System.out.println(canChange("_L__R__R_", "L______RR")); // true
        System.out.println(canChange("R_L_", "__LR")); // false
        System.out.println(canChange("_R", "R_")); // false
        System.out.println(canChange("___L___", "_L_____")); // true
    }

    public static boolean canChange(String start, String target) {
        int i = 0, j = 0;
        while (i < start.length() || j < target.length()) {
            while (i < start.length() && start.charAt(i) == '_') {
                i++;
            }
            while (j < target.length() && target.charAt(j) == '_') {
                j++;
            }
            if (i == start.length() || j == target.length()) {
                break;
            }
            if (start.charAt(i) != target.charAt(j)) {
                return false;
            }
            if (start.charAt(i) == 'L' && i < j) {
                return false;
            }
            if (target.charAt(j) == 'R' && i > j) {
                return false;
            }
            i++;
            j++;
        }
        return i == start.length() && j == target.length();
    }

}
