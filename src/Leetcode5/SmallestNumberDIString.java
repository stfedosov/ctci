package Leetcode5;

import java.util.Stack;

public class SmallestNumberDIString {

    public static void main(String[] args) {
//        System.out.println(smallestNumber("DDDD"));
        System.out.println(smallestNumber("DDDI"));
    }

    public static String smallestNumber(String pattern) {
        StringBuilder result = new StringBuilder();
        Stack<Integer> numStack = new Stack<>();

        for (int index = 0; index <= pattern.length(); index++) {
            // Every time we see a'D', we push the current number onto the stack and continue,
            // delaying its placement in the result.
            // This is because a 'D' means the next number should be smaller than the current one
            numStack.push(index + 1);

            // If 'I' is encountered, or we reach the end, pop all stack elements
            if (index == pattern.length() || pattern.charAt(index) == 'I') {
                while (!numStack.isEmpty()) {
                    result.append(numStack.pop());
                }
            }
        }
        return result.toString();
    }

}
