package codesignal;

import java.util.Arrays;

public class LongestSubarrayBySum {

    public static void main(String[] args) {
        assert Arrays.equals(findLongestSubarrayBySum(12, new int[]{1, 2, 3, 7, 5}), new int[]{2, 4});
        assert Arrays.equals(findLongestSubarrayBySum(15, new int[]{1, 2, 3, 4, 5, 0, 0, 0, 6, 7, 8, 9, 10}), new int[]{1, 8});
    }

    static int[] findLongestSubarrayBySum(int s, int[] arr) {
        int start = 0, end = 0, currSum = 0, max = 0;
        int[] res = new int[]{-1};
        while (end < arr.length) {
            currSum += arr[end++];
            while (start < end && currSum > s) {
                currSum -= arr[start++];
            }
            if (currSum == s && max < end - start) {
                max = end - start;
                res = new int[]{start + 1, end};
            }
        }
        return res;
    }

}
