package codesignal;

import java.util.HashSet;
import java.util.Set;

/**
 * @author sfedosov on 4/9/20.
 */
public class PossibleSums {

    public static void main(String[] args) {
        assert possibleSums(new int[]{10, 50, 100}, new int[]{1, 2, 1}) == 9;
        assert possibleSums(new int[]{1, 2}, new int[]{50000, 2}) == 50004;
        assert possibleSums(new int[]{1, 1}, new int[]{2, 3}) == 5;
    }

    private static void solve(int[] coins, int[] quantity, int idx, int sum, Set<Integer> sums) {
        if (idx == coins.length) {
            if (sum != 0) sums.add(sum);
        } else {
            for (int i = 0; i <= quantity[idx]; i++) {
                solve(coins, quantity, idx + 1, sum + coins[idx] * i, sums);
            }
        }
    }

    public static int possibleSums(int[] coins, int[] quantity) {
        Set<Integer> sums = new HashSet<>();
        solve(coins, quantity, 0, 0, sums);
        return sums.size();
    }

}
