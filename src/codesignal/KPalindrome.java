package codesignal;

public class KPalindrome {

    public static void main(String[] args) {
        System.out.println(kpalindrome("abrarbra", 1));
        System.out.println(isValidPalindrome("abrarbra", 1));
    }

    // The idea is to find the longest palindromic subsequence(lps) of the given string.
    // original string length - LPS <= k -> then the string is k-palindrome.

    public static boolean isValidPalindrome(String s, int k) {
        return s.length() - lps(s, new int[s.length() + 1][s.length() + 1], 0, s.length() - 1) <= k;
    }

    // O(n^2)
    private static int lps(String word, int[][] memo, int i, int j) {
        if (memo[i][j] != 0) {
            return memo[i][j];
        }
        if (word.charAt(i) == word.charAt(j)) {
            if (i == j) {
                memo[i][j] = 1;
            } else if (i < j) {
                memo[i][j] = 2 + lps(word, memo, i + 1, j - 1);
            }
        } else {
            memo[i][j] = Math.max(lps(word, memo, i + 1, j), lps(word, memo, i, j - 1));
        }
        return memo[i][j];
    }

    // ------------------============================------------------

    static boolean kpalindrome(String s, int k) {
        if (k >= s.length()) return true;
        if (k == 0) return isPal(s);
        if (s.charAt(0) != s.charAt(s.length() - 1)) {
            return kpalindrome(s.substring(1), k - 1) || kpalindrome(s.substring(0, s.length() - 1), k - 1);
        } else {
            return kpalindrome(s.substring(1, s.length() - 1), k);
        }
    }

    private static boolean isPal(String s) {
        int start = 0, end = s.length() - 1;
        while (start < end) {
            if (s.charAt(start) != s.charAt(end)) return false;
            start++;
            end--;
        }
        return true;
    }

}
