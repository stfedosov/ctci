package codesignal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class GraphDistances {

    public static void main(String[] args) {
        assert Arrays.equals(graphDistances(new int[][]{{-1, -1, 2}, {1, -1, 0}, {-1, 1, -1}}, 0), new int[]{0, 2, 2});
    }

    static int[] graphDistances(int[][] g, int s) {
        Graph graph = new Graph(g.length);
        for (int i = 0; i < g.length; i++) {
            for (int j = 0; j < g[i].length; j++) {
                graph.addEdge(i, j, g[i][j]);
            }
        }
        return graph.dijkstra(s);
    }

    static class Graph {
        int numOfVertices;
        ArrayList<int[]>[] adj;

        public Graph(int numOfVertices) {
            this.adj = new ArrayList[numOfVertices];
            for (int i = 0; i < numOfVertices; i++) this.adj[i] = new ArrayList<>();
            this.numOfVertices = numOfVertices;
        }

        public void addEdge(int s, int d, int w) {
            adj[s].add(new int[]{s, d, w});
        }

        public int[] dijkstra(int source) {
            int[] dist = new int[numOfVertices];
            boolean[] visited = new boolean[numOfVertices];
            Arrays.fill(dist, Integer.MAX_VALUE);
            Queue<int[]> minHeap = new PriorityQueue<>(Comparator.comparingInt(a -> a[1]));
            dist[source] = 0;
            minHeap.offer(new int[]{source, 0});
            while (!minHeap.isEmpty()) {
                int[] polled = minHeap.poll();
                if (!visited[polled[0]]) {
                    visited[polled[0]] = true;
                    for (int[] e : adj[polled[0]]) {
                        if (!visited[e[1]]) {
                            if (e[2] < 0) continue;
                            int newDistance = dist[polled[0]] + e[2];
                            if (newDistance < dist[e[1]]) {
                                dist[e[1]] = newDistance;
                                minHeap.offer(new int[]{e[1], newDistance});
                            }
                        }
                    }
                }
            }
            return dist;
        }

    }
}
