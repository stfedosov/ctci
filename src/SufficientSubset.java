import Leetcode.TreeNode;

/**
 * @author sfedosov on 8/11/19.
 */
public class SufficientSubset {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(5);
        TreeNode treeNode4 = new TreeNode(4);
        root.left = treeNode4;
        TreeNode treeNode11 = new TreeNode(11);
        treeNode4.left = treeNode11;
        treeNode11.left = new TreeNode(7);
        treeNode11.right = new TreeNode(1);
        TreeNode treeNode8 = new TreeNode(8);
        root.right = treeNode8;
        treeNode8.left = new TreeNode(17);
        TreeNode treeNode44 = new TreeNode(4);
        treeNode8.right = treeNode44;
        treeNode44.left = new TreeNode(5);
        treeNode44.right = new TreeNode(3);
        System.out.println(sufficientSubset2(root, 22));

        TreeNode treeNode1 = new TreeNode(1);
        root = treeNode1;
        TreeNode treeNode22 = new TreeNode(2);
        treeNode1.left = treeNode22;
        TreeNode treeNodeMin3 = new TreeNode(-3);
        treeNode1.right = treeNodeMin3;
        treeNodeMin3.left = new TreeNode(4);
        treeNode22.left = new TreeNode(-5);
        System.out.println(sufficientSubset2(root, -1));
        /*
              1
             / \
            2  -3
           /   /
          -5  4
         */
    }

    public static TreeNode sufficientSubset(TreeNode root, int limit) {
        int result = helper(root, limit, 0);
        return result < limit ? null : root;
    }

    private static int helper(TreeNode root, int limit, int current) {
        if (root == null) {
            return 0;
        }
        int left = helper(root.left, limit, current + root.val);
        int right = helper(root.right, limit, current + root.val);

        if (left + current + root.val < limit) {
            root.left = null;
        }
        if (right + current + root.val < limit) {
            root.right = null;
        }
        return Math.max(left + root.val, right + root.val);
    }


    // ------------------

    public static TreeNode sufficientSubset2(TreeNode node, int limit) {
        if (node == null) return null;
        if (isLeaf(node)) return node.val < limit ? null : node;

        node.left = sufficientSubset2(node.left, limit - node.val);
        node.right = sufficientSubset2(node.right, limit - node.val);
        /*
        If a node become a new leaf,
        it means it has no valid path leading to an original leaf,
        we need to remove it.
         */
        return isLeaf(node) ? null : node;
    }

    private static boolean isLeaf(TreeNode node) {
        return node.left == null && node.right == null;
    }

}
