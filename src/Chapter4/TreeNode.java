package Chapter4;

import java.util.Random;

/**
 * This data structure can be considered as a simple tree which contains BST-specific methods:
 * TreeNode#find(int)
 * TreeNode#insertInOrder(int)
 * TreeNode#getRandomNode()
 *
 * @author sfedosov.
 */
public class TreeNode {
    private int num;

    public TreeNode(int num) {
        this.num = num;
        this.size = 1;
    }

    public int getNum() {
        return num;
    }

    public TreeNode left;
    public TreeNode right;
    public TreeNode parent;
    public int size = 0;

    public TreeNode find(int data) {
        if (num == data) {
            return this;
        } else if (data <= num) {
            return left == null ? null : left.find(data);
        } else {
            return right == null ? null : right.find(data);
        }
    }

    public void insertInOrder(int data) {
        if (data <= num) {
            if (left != null) {
                left.insertInOrder(data);
            } else {
                left = new TreeNode(data);
            }
        } else {
            if (right != null) {
                right.insertInOrder(data);
            } else {
                right = new TreeNode(data);
            }
        }
        size++;
    }

    public TreeNode getRandomNode() {
        int random = new Random().nextInt(size);
        return getRandomNodeByIndex(random);
    }

    private TreeNode getRandomNodeByIndex(int random) {
        int leftSize = left != null ? left.size : 0;
        if (leftSize == random) {
            return this;
        } else if (random < leftSize) {
            return left.getRandomNodeByIndex(random);
        } else {
            return right.getRandomNodeByIndex(random - (leftSize + 1));
        }
    }

    @Override
    public String toString() {
        return "TreeNode{" +
                "num=" + num +
                ", left=" + (left == null ? "null" : left.getNum()) +
                ", right=" + (right == null ? "null" : right.getNum()) +
                ", left=" + (parent == null ? "null" : parent.getNum()) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TreeNode treeNode = (TreeNode) o;

        return num == treeNode.num;

    }

    @Override
    public int hashCode() {
        return num;
    }
}
