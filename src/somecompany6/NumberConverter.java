package somecompany6;

/**
 * @author sfedosov on 9/26/18.
 */
public class NumberConverter {

    public static void main(String[] args) {
        assert solution(123456) == 162534;
        assert solution(12224) == 14222;
        assert solution(130) == 103;
    }

    public static int solution(int A) {
        int totalNumberOfDigits = (int) Math.log10(A) + 1;
        int x = 0;
        int[] newIntDigits = new int[totalNumberOfDigits];
        while (totalNumberOfDigits != 0) {
            int first = A / (int) (Math.pow(10, totalNumberOfDigits - 1));
            newIntDigits[x] = first;
            if (x + 1 > newIntDigits.length - 1) {
                break;
            } else {
                x++;
            }
            int last = A % 10;
            newIntDigits[x] = last;
            A -= first * Math.pow(10, totalNumberOfDigits - 1);
            A -= last;
            A /= 10;
            totalNumberOfDigits -= 2;
            x++;
        }
        int newInt = 0;
        for (int i = 0; i < newIntDigits.length; i++) {
            newInt += newIntDigits[i] * Math.pow(10, newIntDigits.length - 1 - i);
        }
        return newInt;
    }
}
