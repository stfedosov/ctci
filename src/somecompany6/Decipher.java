package somecompany6;

/**
 * @author sfedosov on 4/25/19.
 */
public class Decipher {

    public static void main(String[] args) {
        System.out.println(encode("abC"));
        System.out.println(decode(encode("abC")));
    }

    private static String encode(String text) {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            c += c + i;
            b.append(c);
        }
        return b.reverse().toString();
    }

    static String decode(String encodedMessage) {
        StringBuilder b = new StringBuilder();
        int length = encodedMessage.length();
        for (int i = 0; i < length; i++) {
            char c = encodedMessage.charAt(i);
            b.append((char) (((int) c - (length - i - 1)) / 2));
        }
        return b.reverse().toString();
    }

}
