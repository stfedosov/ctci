package Leetcode;

import java.util.HashMap;
import java.util.Map;

public class MaxSubArrayLenEqualsToK {

    public static void main(String[] args) {
        System.out.println(maxSubArrayLen(new int[]{1, -1, 5, -2, 3}, 3));
        System.out.println(maxSubArrayLen(new int[]{1, 1, 1}, 2));
    }

    public static int maxSubArrayLen(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        int sum = 0;
        int ans = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            if (sum == k) {
                ans = i + 1;
            } else if (map.containsKey(sum - k)) {
                ans = Math.max(ans, i - map.get(sum - k));
            }
            map.putIfAbsent(sum, i);
        }
        return ans;
    }

}
