package Leetcode;

public class IsNumber {

    public static void main(String[] args) {
        assert isNumber(" -.7e+0435");
        assert isNumber("53.5e93");
        assert isNumber("-90e3");
        assert !isNumber(" 1e");
        assert isNumber(" 0.1 ");
        assert !isNumber("abc");
    }

    public static boolean isNumber(String s) {
        String trimmed = s.toLowerCase().trim();
        int len = trimmed.length();
        if (len == 0) return false;
        boolean seenSign = false, seenExp = false, seenDots = false, seenNums = false;
        int i = 0, count = 0;
        for (char c : trimmed.toCharArray()) {
            if (c >= '0' && c <= '9') {
                seenNums = true;
            } else if (c == '+' || c == '-') {
                if (seenSign && !seenExp || i != 0 && !seenExp || i == len - 1) return false;
                count++;
                if (count > 2) return false;
                seenSign = true;
            } else if (c == '.') {
                if (seenDots || seenExp || i == 0 && len == 1) return false;
                seenDots = true;
            } else if (c == 'e') {
                if (seenExp || i == 0 || i == len - 1 || i == 1 && (seenDots || seenSign)) return false;
                seenExp = true;
            } else if (c >= 'a' && c <= 'z' || c == ' ') {
                return false;
            }
            i++;
        }
        return !(seenDots && !seenNums);
    }
}
