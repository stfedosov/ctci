package Leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author sfedosov on 4/25/19.
 */
public class SmallestStringStartingFromLeaf {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode treeNode2 = new TreeNode(2);
        treeNode2.left = new TreeNode(4);
        root.left = treeNode2;
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode5 = new TreeNode(5);
        treeNode5.left = new TreeNode(7);
        treeNode3.left = treeNode5;
        treeNode3.right = new TreeNode(6);
        root.right = treeNode3;
        System.out.println(smallestFromLeaf(root));
        System.out.println(smallestFromLeaf2(root));
    }

    private final static String alphabet = "abcdefghijklmnopqrstuvwxyz";

    private static String smallestFromLeaf2(TreeNode root) {
        List<String> result = new ArrayList<>();
        traverse(result, "", root);
        Collections.sort(result);
        return result.get(0);
    }

    private static void traverse(List<String> result, String path, TreeNode root) {
        if (root != null) {
            path += alphabet.charAt(root.val);
            if (root.left == null && root.right == null) {
                result.add(new StringBuilder(path).reverse().toString());
            } else {
                traverse(result, path, root.left);
                traverse(result, path, root.right);
            }
        }
    }

    private static String smallestFromLeaf(TreeNode root) {
        if (root == null) return "";
        String l = smallestFromLeaf(root.left);
        String r = smallestFromLeaf(root.right);
        String tmp;
        if (l.length() > 0 && r.length() > 0)
            tmp = (l.compareTo(r) < 0) ? l : r;
        else if (l.length() > 0)
            tmp = l;
        else
            tmp = r;
        return tmp + (char) (root.val + 'a');
    }

}
