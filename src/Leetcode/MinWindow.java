package Leetcode;

import java.util.HashMap;
import java.util.Map;

public class MinWindow {

    public static void main(String[] args) {
        assert minWindow("ADOBECODEBANC", "ABC").equals("BANC");
        assert minWindow("xyyzyzyx", "xyz").equals("zyx");
        assert minWindow("acbbaca", "aba").equals("baca");
        assert minWindow("a", "b").equals("");
        assert minWindow("ABAACBAB", "ABC").equals("ACB");
    }

    public static String minWindow(String s, String t) {
        if (s == null || s.length() < t.length() || s.isEmpty()) return "";
        if (s.equals(t)) return s;
        Map<Character, Integer> map = new HashMap<>();
        for (char c : t.toCharArray()) map.put(c, map.getOrDefault(c, 0) + 1);
        int left = 0, right = 0, minLeft = 0, minLen = s.length(), count = 0;
        while (right < s.length()) {
            char rightChar = s.charAt(right);
            if (map.containsKey(rightChar)) {
                map.put(rightChar, map.get(rightChar) - 1);
                // very very important to remember! we can have a negative value,
                // this simply means that we're at repeated character appeared more time is S rather than in T
                // so just skip it for now
                if (map.get(rightChar) >= 0) {
                    count++;
                }
            }
            while (count == t.length()) {
                if (right - left + 1 < minLen) {
                    minLeft = left;
                    minLen = right - left + 1;
                }
                char leftChar = s.charAt(left);
                if (map.containsKey(leftChar)) {
                    map.put(leftChar, map.get(leftChar) + 1);
                    if (map.get(leftChar) > 0) {
                        count--;
                    }
                }
                left++;
            }
            right++;
        }
        if (minLen > s.length()) return "";
        return s.substring(minLeft, minLeft + minLen);

    }
}
