package Leetcode;

/**
 * @author sfedosov on 8/8/19.
 */
public class strStr {

    public static void main(String[] args) {
        assert strStrKnuthMorrisPratt("AAAAAB", "ABAAB") == -1;
        assert strStrKnuthMorrisPratt("mississippi", "pi") == 9;
        assert strStrBruteForce("AAAAAB", "ABAAB") == -1;
        assert strStrBruteForce("mississippi", "pi") == 9;
    }

    public static int strStrBruteForce(String haystack, String needle) {
        if (haystack.equals(needle)) return 0;
        for (int i = 0; i < haystack.length() - needle.length() + 1; i++) {
            if (haystack.substring(i, i + needle.length()).equals(needle)) return i;
        }
        return -1;
    }

    public static int strStrKnuthMorrisPratt(String haystack, String needle) {
        if (haystack.equals(needle) || needle.isEmpty()) return 0;
        int needleLength = needle.length();
        int[] longestPatternSuffix = new int[needleLength];
        preProcess(longestPatternSuffix, needle);
        int needleIndex = 0, haystackIndex = 0;
        while (haystackIndex < haystack.length()) {
            if (haystack.charAt(haystackIndex) == needle.charAt(needleIndex)) {
                haystackIndex++;
                needleIndex++;
            }
            if (needleIndex == needle.length()) {
                return haystackIndex - needleIndex;
                // for checking further entries
//                needleIndex = longestPatternSuffix[needleIndex - 1];
            } else if (haystackIndex < haystack.length() && haystack.charAt(haystackIndex) != needle.charAt(needleIndex)) {
                if (needleIndex != 0) {
                    needleIndex = longestPatternSuffix[needleIndex - 1];
                } else {
                    haystackIndex++;
                }
            }
        }
        return -1;
    }

    private static void preProcess(int[] longestPatternSuffix, String needle) {
        int end = 1, start = 0;
        longestPatternSuffix[0] = 0;
        while (end < needle.length()) {
            if (needle.charAt(end) == needle.charAt(start)) {
                start++;
                // increase the length of the longest prefix that we've seen so far
                longestPatternSuffix[end] = start;
                end++;
            } else {
                // return back to one position to last char that we've seen so far
                if (start != 0) {
                    start = longestPatternSuffix[start - 1];
                } else {
                    end++;
                }
                // explore our pattern further and count the appropriate length
            }
        }
    }
}
