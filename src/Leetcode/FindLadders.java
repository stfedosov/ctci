package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class FindLadders {

    public static void main(String[] args) {
        assert findLadders("hit", "cog", Arrays.asList("hot", "dot", "dog", "lot", "log", "cog")).equals(
                Arrays.asList(
                        Arrays.asList("hit", "hot", "dot", "dog", "cog"),
                        Arrays.asList("hit", "hot", "lot", "log", "cog")));

        assert findLadders("hit", "cog", Arrays.asList("hot", "dot", "dog", "lot", "log")).isEmpty();

        assert findLadders("a", "c", Arrays.asList("a", "b", "c")).equals(Collections.singletonList(Arrays.asList("a", "c")));

        assert findLadders("red", "tax", Arrays.asList("ted", "tex", "red", "tax", "tad", "den", "rex", "pee")).equals(
                Arrays.asList(
                        Arrays.asList("red", "ted", "tad", "tax"),
                        Arrays.asList("red", "ted", "tex", "tax"),
                        Arrays.asList("red", "rex", "tex", "tax")));
    }

    public static List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {
        Set<String> words = new HashSet<>(wordList);
        if (!words.contains(endWord)) {
            return Collections.emptyList();
        }

        words.add(beginWord);

        Map<String, List<String>> adjList = new HashMap<>();

        for (String word: words) {
            for (int i = 0; i < word.length(); i++) {
                String pattern = getPattern(word, i);
                adjList.computeIfAbsent(pattern, x -> new ArrayList<>()).add(word);
            }
        }

        Queue<String> queue = new LinkedList<>();
        Map<String, Set<String>> graph = new HashMap<>();
        Map<String, Integer> levels = new HashMap<>();

        queue.add(beginWord);
        levels.put(beginWord, 0);

        while (!queue.isEmpty()) {
            String cur = queue.poll();
            int curLevel = levels.get(cur);
            for (int i = 0; i < cur.length(); i++) {
                String pattern = getPattern(cur, i);
                for (String nei: adjList.get(pattern)) {
                    graph.computeIfAbsent(cur, x -> new HashSet<>()).add(nei);
                    if (!levels.containsKey(nei)) {
                        levels.put(nei, curLevel + 1);
                        queue.add(nei);
                    }
                }
            }
        }

        List<List<String>> ans = new ArrayList<>();
        backtracking(endWord, beginWord, new ArrayList<>(), ans, graph, levels);
        return ans;
    }

    private static void backtracking(String start, String endWord,
                              List<String> path, List<List<String>> ans,
                              Map<String, Set<String>> graph,
                              Map<String, Integer> levels) {
        path.add(start);

        if (start.equals(endWord)) {
            List<String> temp = new ArrayList<>(path);
            Collections.reverse(temp);
            ans.add(temp);
        } else {
            for (String next : graph.getOrDefault(start, Collections.emptySet())) {
                if (levels.get(next) == levels.get(start) - 1) {
                    backtracking(next, endWord, path, ans, graph, levels);
                }
            }
        }

        path.remove(path.size() - 1);
    }

    private static String getPattern(String word, int i) {
        return word.substring(0, i) + "*" + word.substring(i + 1);
    }

}
