package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FindAllAnagramsInAString {

    public static void main(String[] args) {
        assert findAnagrams("abab", "ab").equals(Arrays.asList(0, 1, 2));
        assert findAnagrams("cbaebabacd", "abc").equals(Arrays.asList(0, 6));
        assert findAnagrams("baa", "aa").equals(Collections.singletonList(1));
    }

    // O(S length)
    public static List<Integer> findAnagrams(String s, String p) {
        if (s == null || p == null || s.isEmpty() || p.isEmpty() || s.length() < p.length()) return Collections.emptyList();
        if (s.length() == p.length() && s.equals(p)) return Collections.singletonList(0);
        int[] expected = new int[26], actual = new int[26];
        for (char c : p.toCharArray()) {
            expected[c - 'a']++;
        }
        List<Integer> result = new ArrayList<>();
        int window = p.length() - 1, start = 0;
        for (int i = 0; i < s.length(); i++) {
            actual[s.charAt(i) - 'a']++;
            if (i < window) {
                continue;
            }
            // O(1) since we can have 26 chars max
            if (Arrays.equals(expected, actual)) {
                result.add(start);
            }
            actual[s.charAt(start++) - 'a']--;
        }
        return result;
    }


}
