package Leetcode;

import Leetcode.ReorderList.ListNode;

public class RemoveNthNodeFromTheEnd {

    public static void main(String[] args) {
        ListNode listNode = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        listNode.next = listNode2;
        ListNode listNode3 = new ListNode(3);
        listNode2.next = listNode3;
        ListNode listNode4 = new ListNode(4);
        listNode3.next = listNode4;
        listNode4.next = new ListNode(5);
        System.out.println(removeNthFromEnd(listNode, 2)); // before [1 -> 2 -> 3 -> 4 -> 5], after [1 -> 2 -> 3 -> 5]
        System.out.println(removeNthFromEnd(new ListNode(1), 1)); // before [1], after []
        ListNode node = new ListNode(1);
        node.next = new ListNode(2);
        System.out.println(removeNthFromEnd(node, 2));
    }

    // Move one pointer fast --> n + 1 places forward, to maintain a gap of n between the two pointers and
    // then move both at the same speed.
    // Finally, when the fast pointer reaches the end,
    // the slow pointer will be n+1 places behind - just the right spot for it to be able to skip the next node.
    public static ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode right = head;
        ListNode left = head;

        for (int i = 0; i < n; i++) {
            right = right.next;
        }

        if (right == null) {
            return head.next;
        }

        while (right.next != null) {
            right = right.next;
            left = left.next;
        }

        left.next = left.next.next;

        return head;
    }

}
