package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class MeetingRooms {

    public static void main(String[] args) {
        System.out.println(minMeetingRoomsLineSweep(new int[][]{{0, 30}, {5, 10}, {15, 20}})); // == 2;
        assert minMeetingRooms(new int[][]{{7, 10}, {2, 4}}) == 1;
        assert minMeetingRooms(new int[][]{{4, 9}, {9, 10}, {4, 17}}) == 2;
    }

    public static int minMeetingRooms(int[][] intervals) {
        if (intervals == null || intervals.length == 0) {
            return 0;
        }
        Arrays.sort(intervals, Comparator.comparingInt(a -> a[0]));
        Queue<int[]> q = new PriorityQueue<>(Comparator.comparingInt(a -> a[1]));
        for (int[] interval : intervals) {
            if (!q.isEmpty() && q.peek()[1] <= interval[0]) {
                q.poll();
            }
            q.offer(interval);
        }
        return q.size();
    }

    // Line sweep algorithm
    public static int minMeetingRoomsLineSweep(int[][] intervals) {
        if (intervals == null || intervals.length == 0) {
            return 0;
        }
        List<int[]> events = new ArrayList<>();
        for (int[] interval : intervals) {
            events.add(new int[]{interval[0], 1});
            events.add(new int[]{interval[1], -1});
        }
        events.sort((a, b) -> (a[0] != b[0]) ? a[0] - b[0] : a[1] - b[1]);
        int count = 0;
        int max = 0;
        for (int[] event : events) {
            count += event[1];
            max = Math.max(max, count);
        }
        return max;
    }

}
