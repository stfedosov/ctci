package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 2/5/19.
 */
public class MoveZeros {

    public static void main(String[] args) {
        int[] nums = new int[]{0, 1, 0, 3, 12};
        moveZeroes(nums);
        System.out.println(Arrays.toString(nums)); // 1, 3, 12, 0, 0
    }

    public static void moveZeroes(int[] nums) {
        int write_index = 0;
        int read_index = 0;
        while (read_index <= nums.length - 1) {
            if (nums[read_index] != 0) {
                nums[write_index++] = nums[read_index];
            }
            read_index++;
        }
        while (write_index < nums.length) {
            nums[write_index] = 0;
            write_index++;
        }
    }

}
