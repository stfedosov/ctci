package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author sfedosov on 12/29/19.
 */
public class IntersectionOfThreeSortedArrays {

    public static void main(String[] args) {
        assert arraysIntersectionOptimized(new int[]{1, 2, 3, 4, 5},
                new int[]{1, 2, 5, 7, 9},
                new int[]{1, 3, 4, 5, 8}).equals(Arrays.asList(1, 5));
        assert arraysIntersectionBruteForce(new int[]{1, 2, 3, 4, 5},
                new int[]{1, 2, 5, 7, 9},
                new int[]{1, 3, 4, 5, 8}).equals(Arrays.asList(1, 5));
    }

    // Alternative

    public static List<Integer> arraysIntersectionBruteForce(int[] arr1, int[] arr2, int[] arr3) {
        List<Integer> list = new ArrayList<>();
        for (int x : arr1) {
            if (Arrays.binarySearch(arr2, x) >= 0 && Arrays.binarySearch(arr3, x) >= 0) {
                list.add(x);
            }
        }
        return list;
    }

    // Optimized

    public static List<Integer> arraysIntersectionOptimized(int[] arr1, int[] arr2, int[] arr3) {
        List<Integer> result = new ArrayList<>();
        int i = 0;
        int j = 0;
        int k = 0;
        while (i < arr1.length && j < arr2.length && k < arr3.length) {
            if (arr1[i] == arr2[j] && arr2[j] == arr3[k]) {
                result.add(arr1[i]);
                i++;
                j++;
                k++;
            } else if (arr1[i] < arr2[j]) {
                i++;
            } else if (arr2[j] < arr3[k]) {
                j++;
            } else k++;
        }
        return result;

    }

}
