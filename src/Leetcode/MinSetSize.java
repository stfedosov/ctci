package Leetcode;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class MinSetSize {

    public static void main(String[] args) {
        assert minSetSize(new int[]{3, 3, 3, 3, 5, 5, 5, 2, 2, 7}) == 2;
        assert minSetSize(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}) == 5;
        assert minSetSize(new int[]{9, 77, 63, 22, 92, 9, 14, 54, 8, 38, 18, 19, 38, 68, 58, 19}) == 5;
        assert minSetSize(new int[]{1, 9}) == 1;
        assert minSetSize(new int[]{1000, 1000, 3, 7}) == 1;
    }

    public static int minSetSize(int[] arr) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int x : arr) map.put(x, map.getOrDefault(x, 0) + 1);
        if (map.size() == 1) return 1;
        Queue<Map.Entry<Integer, Integer>> q = new PriorityQueue<>((a, b) -> b.getValue() - a.getValue());
        q.addAll(map.entrySet());
        int count = 0;
        int half = arr.length / 2;
        while (!q.isEmpty()) {
            int tmp = q.poll().getValue();
            half -= tmp;
            count++;
            if (half <= 0) break;
        }
        return count;
    }


}
