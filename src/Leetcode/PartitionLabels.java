package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sfedosov on 7/31/19.
 */
public class PartitionLabels {

    public static void main(String[] args) {
        assert partitionLabels("ababcbacadefegdehijhklij").equals(Arrays.asList(9, 7, 8));
        assert partitionLabels("abcdefghijkl").equals(Arrays.asList(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1));
    }

    public static List<Integer> partitionLabels(String S) {
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < S.length(); i++) {
            map.put(S.charAt(i), i);
        }
        List<Integer> result = new ArrayList<>();
        int max = 0;
        int start = 0;
        for (int i = 0; i < S.length(); i++) {
            max = Math.max(max, map.get(S.charAt(i)));
            if (i == max) {
                result.add(max - start + 1);
                start = i + 1;
            }
        }
        return result;
    }

}
