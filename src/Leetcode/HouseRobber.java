package Leetcode;

public class HouseRobber {

    public static void main(String[] args) {
        rob(new int[]{1, 2, 3, 1}); // 4
        rob(new int[]{2, 7, 9, 3, 1}); // 12
    }

    public static int rob(int[] nums) {
        return rob(nums, 0);
    }

    private static int rob(int[] nums, int idx) {
        if (idx >= nums.length) return 0;
        return Math.max(nums[idx] + rob(nums, idx + 2), rob(nums, idx + 1));
    }

}
