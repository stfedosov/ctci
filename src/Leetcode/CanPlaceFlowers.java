package Leetcode;

public class CanPlaceFlowers {

    public static void main(String[] args) {
        assert canPlaceFlowers(new int[]{1, 0, 0, 0, 1}, 1);
        assert !canPlaceFlowers(new int[]{1, 0, 0, 0, 1}, 2);
        assert canPlaceFlowers(new int[]{1, 0, 0, 0, 0}, 2);
        assert canPlaceFlowers(new int[]{0, 0, 0, 0, 1}, 2);
        assert canPlaceFlowers(new int[]{0, 0, 0, 1, 0, 0, 0}, 2);
    }

    public static boolean canPlaceFlowers(int[] flowerbed, int n) {
        int i = 0;
        while (i < flowerbed.length && n > 0) {
            if (flowerbed[i] == 0 && (i == 0 || flowerbed[i - 1] == 0) && (i == flowerbed.length - 1 || flowerbed[i + 1] == 0)) {
                flowerbed[i] = 1;
                n--;
            }
            i++;
        }
        return n == 0;
    }

}
