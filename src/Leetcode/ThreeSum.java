package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThreeSum {

    public static void main(String[] args) {
        System.out.println(threeSum(new int[]{-1, 0, 1, 2, -1, -4}));
        System.out.println(threeSum(new int[]{-1, 0, 1, 2, -1, -4}));
    }

    // 3Sum
    public static List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> list = new ArrayList<>();
        for (int i = 0; i < nums.length - 2; i++) {
            // skipping duplicates
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            twoSum(nums, i, list);
        }
        return list;
    }

    private static void twoSum(int[] nums, int i, List<List<Integer>> list) {
        int left = i + 1, right = nums.length - 1;
        while (left < right) {
            int sum = nums[i] + nums[left] + nums[right];
            if (sum == 0) {
                list.add(List.of(nums[i], nums[left], nums[right]));
                // skipping duplicates
                while (left < right && nums[left] == nums[left + 1]) {
                    left++;
                }
                // skipping duplicates
                while (left < right && nums[right] == nums[right - 1]) {
                    right--;
                }
                right--;
                left++;
            } else if (sum < 0) {
                left++;
            } else {
                right--;
            }
        }
    }


}
