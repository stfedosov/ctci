package Leetcode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

/**
 * @author sfedosov on 12/8/18.
 */
public class FlattenNestedListIterator {

    public class NestedIterator implements Iterator<Integer> {

        private Iterator<Integer> iterator;

        public NestedIterator(List<NestedInteger> nestedList) {
            List<Integer> list = new ArrayList<>();
            Stack<NestedInteger> stack = new Stack<>();
            int i = 0;
            while (i != nestedList.size()) {
                stack.push(nestedList.get(i));
                while (!stack.isEmpty()) {
                    NestedInteger nested = stack.pop();
                    if (nested.isInteger()) {
                        list.add(nested.getInteger());
                    } else {
                        List<NestedInteger> nst = nested.getList();
                        for (int j = nst.size() - 1; j >= 0; j--) {
                            stack.push(nst.get(j));
                        }
                    }
                }
                i++;
            }
            iterator = list.iterator();
        }

        @Override
        public Integer next() {
            return iterator.next();
        }

        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }
    }

}
/**

 !!!!!!!!!!!!
 !!!!!!!!!!!!
 !!!!!!!!!!!!

    void flatten(Iterator<?> iterator, List<String> flattenedList) {
        for (Object o : iterator)  {
            if (o instanceof Iterator) {
                flatten((Iterator) o, flattenedList);
            } else {
                flattenedList.add((String) o);
            }
        }
    }
*/


class NestedIterator2 implements Iterator<Integer> {

    private Queue<Integer> queue = new LinkedList<>();

    public NestedIterator2(List<NestedInteger> nestedList) {
        helper(nestedList);
    }

    private void helper(List<NestedInteger> list) {
        if (list == null)
            return;

        for (NestedInteger in : list) {
            if (in.isInteger())
                queue.offer(in.getInteger());
            else {
                helper(in.getList());
            }

        }
    }

    @Override
    public Integer next() {
        if (hasNext()) {
            return queue.poll();
        } else
            return -1;
    }

    @Override
    public boolean hasNext() {
        return !queue.isEmpty();
    }
}

interface NestedInteger {
    boolean isInteger();

    Integer getInteger();

    List<NestedInteger> getList();
}
