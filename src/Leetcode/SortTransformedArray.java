package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 4/2/20.
 */
public class SortTransformedArray {

    public static void main(String[] args) {
        // ax^2 + bx + c is parabola
        // so if a > 0 then it can be drawn as high - low - high
        // otherwise, low - high - low
        // ----------
        // just pick the array value at the start and at the end
        // compare them and move forward those which value either grater or lower

        assert Arrays.equals(sortTransformedArray(new int[]{-4, -2, 2, 4}, 1, 3, 5), new int[]{3, 9, 15, 33});
        assert Arrays.equals(sortTransformedArray(new int[]{-4, -2, 2, 4}, -1, 3, 5), new int[]{-23, -5, 1, 7});
    }

    public static int[] sortTransformedArray(int[] nums, int a, int b, int c) {
        int[] result = new int[nums.length];
        int first_half = 0, end_half = nums.length - 1, start = 0, end = nums.length - 1;
        if (a > 0) {
            while (start <= end) {
                int y1 = a * nums[start] * nums[start] + b * nums[start] + c;
                int y2 = a * nums[end] * nums[end] + b * nums[end] + c;
                if (y1 > y2) {
                    result[end_half--] = y1;
                    start++;
                } else {
                    result[end_half--] = y2;
                    end--;
                }
            }
        } else {
            while (start <= end) {
                int y1 = a * nums[start] * nums[start] + b * nums[start] + c;
                int y2 = a * nums[end] * nums[end] + b * nums[end] + c;
                if (y1 > y2) {
                    result[first_half++] = y2;
                    end--;
                } else {
                    result[first_half++] = y1;
                    start++;
                }
            }
        }
        return result;
    }

}
