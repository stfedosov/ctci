package Leetcode;

/**
 * @author sfedosov on 11/24/19.
 */
public class RemoveAllAdjacentDuplicatesFromString2 {

    public static void main(String[] args) {
        assert removeDuplicates("deeedbbcccbdaa", 3).equals("aa");
        assert removeDuplicates("pbbcggttciiippooaais", 2).equals("ps");
        assert removeDuplicates("abcd", 2).equals("abcd");
    }

    public static String removeDuplicates(String s, int k) {
        int counter = 1, i = 1, start = 0;
        char prev = s.charAt(0);
        while (i < s.length()) {
            if (prev == s.charAt(i)) {
                counter++;
            } else {
                counter = 1;
                prev = s.charAt(i);
                start = i;
            }
            if (counter == k) {
                s = s.substring(0, start) + s.substring(i + 1);
                i = 0;
                start = i;
            } else {
                i++;
            }
        }
        return s;
    }

}
