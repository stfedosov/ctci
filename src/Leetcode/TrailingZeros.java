package Leetcode;

/**
 * @author sfedosov on 5/31/20.
 */
public class TrailingZeros {

    public static void main(String[] args) {
        // how many zeros will have a number n! at the end?
        // ====
        // BruteForce: multiply all the numbers and get the answer by counting zeros
        // ----
        // More optimal: every zero is a product of multiplication by 10,
        // so all we need to do is to count amount of 2s/5s and then take min among them:
        //
        // twos = 0
        // for i from 1 to n inclusive:
        //  remaining_i = i
        //      while remaining_i is divisible by 2:
        //          twos += 1
        //          remaining_i = remaining_i / 2
        //
        // fives = 0
        // for i from 1 to n inclusive:
        //  remaining_i = i
        //      while remaining_i is divisible by 5:
        //          fives += 1
        //          remaining_i = remaining_i / 5
        //
        // tens = min(twos, fives)

        // ----
        // Optimal: since there are too many 2s, we can avoid counting it and simply count only 5s.
        // Best:
        assert trailingZeroes(5) == 1; // 5! = 120 -> 1 zero at the end
        assert trailingZeroes(3) == 0; // 3! = 6, no trailing zero
    }

    public static int trailingZeroes(int n) {
        int zeroCount = 0;
        while (n > 0) {
            n /= 5;
            zeroCount += n;
        }
        return zeroCount;
    }

}
