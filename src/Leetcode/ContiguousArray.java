package Leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 1/25/19.
 */
public class ContiguousArray {

    public static void main(String[] args) {
        assert findMaxLength(new int[]{0, 0, 0, 1, 1, 1}) == 6;
        assert findMaxLength2(new int[]{0, 0, 0, 1, 1, 1}) == 6;
    }

    public static int findMaxLength2(int[] nums) {
        int max = 0;
        for (int start = 0; start < nums.length; start++) {
            int zeroCount = 0;
            int oneCount = 0;
            for (int end = start; end < nums.length; end++) {
                if (nums[end] == 0) {
                    zeroCount++;
                } else {
                    oneCount++;
                }
                if (zeroCount == oneCount) {
                    max = Math.max(max, end - start + 1);
                }
            }
        }
        return max;
    }

    public static int findMaxLength(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0, 0);
        int max = 0;
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) {
                count++;
            } else {
                count--;
            }
            if (!map.containsKey(count)) {
                map.put(count, i + 1);
            } else {
                max = Math.max(max, i - map.get(count) + 1);
            }
        }
        return max;
    }

}
