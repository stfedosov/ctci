package Leetcode;

/**
 * @author sfedosov on 9/20/19.
 */
public class Tree2Str {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(2);
        TreeNode node3 = new TreeNode(3);
        node3.left = new TreeNode(1);
        root.left = node3;
        TreeNode node4 = new TreeNode(4);
        root.right = node4;
        node4.left = new TreeNode(2);
        node4.right = new TreeNode(5);
        //     2
        //    / \
        //   3   4
        //  /   / \
        // 1   2   5
        assert tree2str(root).equals("2(3(1))(4(2)(5))");
    }

    public static String tree2str(TreeNode t) {
        String res = "";
        if (t == null) {
            return res;
        } else if (t.left == null && t.right == null) {
            res = t.val + "";
        } else if (t.left != null && t.right != null) {
            res = t.val + "(" + tree2str(t.left) + ")" + "(" + tree2str(t.right) + ")";
        } else if (t.left != null) {
            res = t.val + "(" + tree2str(t.left) + ")";
        } else {
            res = t.val + "()" + "(" + tree2str(t.right) + ")";
        }
        return res;
    }

}
