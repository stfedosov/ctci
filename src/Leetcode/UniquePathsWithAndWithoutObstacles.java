package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 10/2/18.
 */
public class UniquePathsWithAndWithoutObstacles {

    public static void main(String[] args) {
        assert uniquePathsWithObstacles(new int[][]{
                {0, 0, 0},
                {0, 1, 0},
                {0, 0, 0}}) == 2;
        assert uniquePathsWithoutObstacles(new int[][]{
                {0, 0, 0},
                {0, 0, 0},
                {0, 0, 0}}) == 6;
        assert uniquePathsWithObstacles(new int[][]{
                {0, 0, 0},
                {0, 0, 0},
                {0, 0, 0}}) == 6;
        assert uniquePathsWithoutObstacles(new int[][]{
                {0},
                {0}}) == 1;
        assert uniquePathsWithObstacles(new int[][]{
                {0},
                {0}}) == 1;
        assert uniquePathsWithoutObstacles(new int[][]{
                {0, 0}}) == 1;
        assert uniquePathsWithObstacles(new int[][]{
                {0, 0}}) == 1;
        assert uniquePathsWithObstacles(new int[][]{
                {1, 0}}) == 0;
        assert uniquePathsWithoutObstacles(new int[][]{
                {1}
        }) == 1;
    }

    public static int uniquePathsWithObstaclesRec(int[][] obstacleGrid) {
        int[][] memo = new int[obstacleGrid.length][obstacleGrid[0].length];
        for (int[] ints : memo) {
            Arrays.fill(ints, -1);
        }
        return uniquePathsWithObstaclesRec(obstacleGrid, obstacleGrid.length - 1, obstacleGrid[0].length - 1, memo);
    }

    public static int uniquePathsWithObstaclesRec(int[][] obstacleGrid, int R, int C, int[][] memo) {
        if (C < 0 || R < 0 || obstacleGrid[R][C] == 1) {
            return 0;
        }
        if (C == 0 && R == 0) {
            return obstacleGrid[0][0] == 1 ? 0 : 1;
        }
        int ways = 0;
        if (C > 0) {
            if (memo[R][C - 1] == -1) {
                memo[R][C - 1] = uniquePathsWithObstaclesRec(obstacleGrid, R, C - 1, memo);
            }
            ways += memo[R][C - 1];
        }
        if (R > 0) {
            if (memo[R - 1][C] == -1) {
                memo[R - 1][C] = uniquePathsWithObstaclesRec(obstacleGrid, R - 1, C, memo);
            }
            ways += memo[R - 1][C];
        }
        return ways;
    }

    private static int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int row = obstacleGrid.length;
        int column = obstacleGrid[0].length;
        int[][] dp = new int[row][column];
        for (int i = 0; i < row; i++) {
            if (obstacleGrid[i][0] == 1) break;
            dp[i][0] = 1;
        }
        for (int j = 0; j < column; j++) {
            if (obstacleGrid[0][j] == 1) break;
            dp[0][j] = 1;
        }
        for (int i = 1; i < row; i++) {
            for (int j = 1; j < column; j++) {
                if (obstacleGrid[i][j] != 1) {
                    dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
                }
            }
        }
        return dp[row - 1][column - 1];
    }

    private static int uniquePathsWithoutObstacles(int[][] grid) {
        int column = grid[0].length;
        int row = grid.length;
        for (int i = 0; i < row; i++) {
            grid[i][0] = 1;
        }
        for (int j = 0; j < column; j++) {
            grid[0][j] = 1;
        }
        for (int i = 1; i < row; i++) {
            for (int j = 1; j < column; j++) {
                grid[i][j] = grid[i - 1][j] + grid[i][j - 1];
            }
        }
        return grid[row - 1][column - 1];
    }

}
