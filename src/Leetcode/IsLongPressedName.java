package Leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 12/1/19.
 */
public class IsLongPressedName {

    public static void main(String[] args) {
        assert isLongPressedNameBruteForce("alex", "aaleex");
        assert !isLongPressedNameBruteForce("saeed", "ssaaedd");
        assert isLongPressedNameBruteForce("leelee", "lleeelee");
        assert isLongPressedNameBruteForce("laiden", "laiden");
    }

    // ---- optimized

    public static boolean isLongPressedNameOptimized(String name, String typed) {
        if (typed.equals(name)) return true;
        if (typed.length() < name.length()) return false;
        int i = 0, j = 0;
        while (j < typed.length()) {
            if (i < name.length() && name.charAt(i) == typed.charAt(j)) {
                i++;
            } else if (j == 0 || typed.charAt(j - 1) != typed.charAt(j)) {
                return false;
            }
            j++;
        }
        return i == name.length();
    }

    // ----- Brute FORCE!

    public static boolean isLongPressedNameBruteForce(String name, String typed) {
        if (typed.equals(name)) return true;
        if (typed.length() < name.length()) return false;
        Map<Character, Map<Integer, Integer>> map1 = new HashMap<>();
        Map<Character, Map<Integer, Integer>> map2 = new HashMap<>();
        fill(name, map1);
        fill(typed, map2);
        if (map1.size() != map2.size()) return false;
        for (Character key : map1.keySet()) {
            Map<Integer, Integer> integerIntegerMap1 = map1.get(key);
            Map<Integer, Integer> integerIntegerMap2 = map2.get(key);
            if (integerIntegerMap1.size() != integerIntegerMap2.size()) return false;
            else {
                for (Map.Entry<Integer, Integer> entry : integerIntegerMap1.entrySet()) {
                    if (!integerIntegerMap2.containsKey(entry.getKey()) || integerIntegerMap2.get(entry.getKey()) < entry.getValue())
                        return false;
                }
            }
        }
        return true;
    }

    private static void fill(String name, Map<Character, Map<Integer, Integer>> map1) {
        int idx = 0;
        char prev = name.charAt(0);
        Map<Integer, Integer> m = new HashMap<>();
        m.put(idx, 1);
        map1.put(prev, m);
        for (int i = 1; i < name.length(); i++) {
            char c = name.charAt(i);
            if (c == prev) {
                Integer val =
                        map1.get(c).get(idx);
                map1.get(c).put(idx, val + 1);
            } else {
                idx++;
                prev = c;
                if (map1.containsKey(prev)) {
                    map1.get(prev).put(idx, 1);
                } else {
                    Map<Integer, Integer> mm = new HashMap<>();
                    mm.put(idx, 1);
                    map1.put(prev, mm);
                }
            }
        }
    }

}
