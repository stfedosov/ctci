package Leetcode;

public class MaxConsecutiveOnes3 {

    // Given a binary array nums and an integer k,
    // return the maximum number of consecutive 1's in the array if you can flip at most k 0's.
    public static void main(String[] args) {
        assert longestOnes(new int[]{1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0}, 2) == 6;
        assert longestOnes(new int[]{0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1}, 3) == 10;
    }

    public static int longestOnes(int[] A, int K) {
        int zeroCounter = 0;
        int left = 0;
        int max = 0;
        for (int right = 0; right < A.length; right++) {
            if (A[right] == 0) zeroCounter++;
            while (zeroCounter > K) {
                if (A[left] == 0) zeroCounter--;
                left++;
            }
            max = Math.max(max, right - left + 1);
        }
        return max;
    }

}
