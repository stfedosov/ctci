package Leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author sfedosov on 2/3/20.
 */
public class LongestStringChain {

    public static void main(String[] args) {
        assert longestStrChain(new String[]{"a", "b", "ba", "bca", "bda", "bdca"}) == 4; // "a","ba","bda","bdca"
    }

    // O(N*K^2) N is word list size, K is avg word length
    public static int longestStrChain(String[] words) {
        int max = 0;
        Map<String, Integer> cache = new HashMap<>();
        for (String word : words) {
            max = Math.max(max, dfs(word, words, new HashSet<>(), cache));
        }
        return max;
    }

    private static int dfs(String word, String[] words, Set<String> seen, Map<String, Integer> cache) {
        if (cache.containsKey(word)) return cache.get(word);
        int max = 1;
        seen.add(word);
        for (String w : words) {
            if (!seen.contains(w) && w.length() > word.length() && isEditDistanceOne(w, word)) {
                max = Math.max(max, 1 + dfs(w, words, seen, cache));
            }
        }
        cache.put(word, max);
        return max;
    }

    private static boolean isEditDistanceOne(String s1, String s2) {
        int m = s1.length(), n = s2.length();

        if (Math.abs(m - n) > 1) return false;

        int count = 0;
        int i = 0, j = 0;
        while (i < m && j < n) {
            if (s1.charAt(i) != s2.charAt(j)) {
                if (count == 1) return false;
                if (m > n) {
                    i++;
                } else {
                    j++;
                }
                count++;
            } else {
                i++;
                j++;
            }
        }
        if (i < m || j < n) count++;
        return count == 1;
    }

}
