package Leetcode;

/**
 * @author sfedosov on 4/18/20.
 */
public class ExcelSheetColumnNumber {

    public static void main(String[] args) {
        assert titleToNumber("A") == 1;
        assert titleToNumber("AB") == 28;
        assert titleToNumber("ZY") == 701;
        assert titleToNumber("AAA") == 703;
    }

    public static int titleToNumber(String s) {
        int number = 0;
        for (char c : s.toCharArray()) {
            number = number * 26 + (c - 'A' + 1);
        }
        return number;
    }

}
