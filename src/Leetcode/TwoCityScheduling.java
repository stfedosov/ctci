package Leetcode;

import java.util.Arrays;

public class TwoCityScheduling {

    /*
    A company is planning to interview 2n people.
    Given the array costs where costs[i] = [aCosti, bCosti],
    the cost of flying the ith person to city a is aCosti, and the cost of flying the ith person to city b is bCosti.

    Return the minimum cost to fly every person to a city such that exactly n people arrive in each city.
     */
    public static void main(String[] args) {
        System.out.println(twoCitySchedCost(new int[][]{{10, 20}, {30, 200}, {400, 50}, {30, 20}}));
    }

    public static int twoCitySchedCost(int[][] costs) {
        // Looking at difference between cityA and cityB, no difference -> Means, they're equally affordable.
        // Looking at difference between cityA and cityB, cost of cityA > cityB -> Means, going to cityB is a much better choice.
        // Looking at difference between cityA and cityB, cost of cityA < cityB -> Means, going to cityA is a much better choice.
        Arrays.sort(costs, (a, b) -> Integer.compare(a[0] - a[1], b[0] - b[1]));

        int price = 0;
        for (int i = 0; i < costs.length / 2; i++) {
            price += costs[i][0];
        }
        for (int i = costs.length / 2; i < costs.length; i++) {
            price += costs[i][1];
        }
        return price;
    }

}
