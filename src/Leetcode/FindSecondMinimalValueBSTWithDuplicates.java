package Leetcode;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author sfedosov on 8/6/19.
 */
public class FindSecondMinimalValueBSTWithDuplicates {

    /*
           2
          / \
         2   5
            / \
           5   7

         Output: 5
         Explanation: The smallest value is 2, the second smallest value is 5.


           2
          / \
         2   2

         Output: -1
         Explanation: The smallest value is 2, but there isn't any second smallest value.
     */

    public int findSecondMinimumValue(TreeNode root) {
        Set<Integer> set = new TreeSet<>();
        helper(root, set);
        Iterator<Integer> it = set.iterator();
        it.next();
        return it.hasNext() ? it.next() : -1;
    }

    private void helper(TreeNode root, Set<Integer> set) {
        if (root == null) {
            return;
        }
        set.add(root.val);
        helper(root.left, set);
        helper(root.right, set);
    }

}
