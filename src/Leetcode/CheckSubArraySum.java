package Leetcode;

import java.util.HashMap;
import java.util.Map;

public class CheckSubArraySum {

    // Given an integer array nums and an integer k, return true if nums has a continuous subarray of size
    // at least two whose elements sum up to a multiple of k, or false otherwise.
    public static void main(String[] args) {
        assert checkSubarraySum(new int[]{5, 0, 0}, 0);
        assert checkSubarraySum(new int[]{0, 0}, 0);
        assert !checkSubarraySum(new int[]{0}, -1);
        assert !checkSubarraySum(new int[]{0, 1, 0}, 0);
        assert checkSubarraySum(new int[]{23, 2, 4, 6, 7}, 6);
        assert checkSubarraySum(new int[]{23, 2, 6, 4, 7}, 6);
    }

    // in case of the array [23,2,6,4,7] the running sum is [23,25,31,35,42] and the remainders are [5,1,1,5,0].
    // We got remainder 5 at index 0 and at index 3.
    // That means, in between these two indices we must have added a number which is multiple of the k.
    public static boolean checkSubarraySum(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        int sum = 0;
        map.put(0, -1);
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            int remainder = sum % k;
            if (map.containsKey(remainder)) {
                if (i - map.get(remainder) >= 2) {
                    return true;
                }
            } else {
                map.put(remainder, i);
            }
        }
        return false;
    }

}
