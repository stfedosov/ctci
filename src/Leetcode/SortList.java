package Leetcode;

import Leetcode.RotateList.ListNode;

/**
 * @author sfedosov on 11/3/19.
 */
public class SortList {

    public static void main(String[] args) {
        ListNode head = new ListNode(4);
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        head.next = node2;
        node2.next = node1;
        node1.next = node3;
        System.out.println(head); // 4 -> 2 -> 1 -> 3
        System.out.println(sortList(head)); // 1 -> 2 -> 3 -> 4
    }

    public static ListNode sortList(ListNode head) {
        if (head == null || head.next == null) return head;
        ListNode temp = head;
        ListNode slow = head;
        ListNode fast = head;
        while (fast != null && fast.next != null) {
            temp = slow;
            slow = slow.next;
            fast = fast.next.next;
        }
        temp.next = null;
        ListNode leftSide = sortList(head);
        ListNode rightSide = sortList(slow);
        return mergeTwoLists(leftSide, rightSide);
    }

    private static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode result = new ListNode(0);
        ListNode resultIt = result;
        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                resultIt.next = l1;
                l1 = l1.next;
            } else {
                resultIt.next = l2;
                l2 = l2.next;
            }
            resultIt = resultIt.next;
        }
        if (l1 != null) {
            resultIt.next = l1;
        }
        if (l2 != null) {
            resultIt.next = l2;
        }
        return result.next;
    }

}
