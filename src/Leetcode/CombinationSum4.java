package Leetcode;

import java.util.HashMap;
import java.util.Map;

public class CombinationSum4 {

    public static void main(String[] args) {
        assert combinationSum4(new int[]{5, 1, 8}, 24) == 982;
        assert combinationSum4(new int[]{1, 2, 3}, 4) == 7;
    }

    // Time: O(N^target)
    // Space: O(target)
    public static int combinationSum4(int[] nums, int target) {
        return combinationsSum4(nums, target, new HashMap<>());
    }


    private static int combinationsSum4(int[] nums, int target, Map<Integer, Integer> map) {
        if (target == 0) return 1;
        else if (target < 0) return 0;
        if (map.containsKey(target)) return map.get(target);
        int res = 0;
        for (int x : nums) {
            res += combinationsSum4(nums, target - x, map);
        }
        map.put(target, res);
        return res;
    }

}
