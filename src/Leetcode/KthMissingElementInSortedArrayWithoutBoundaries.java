package Leetcode;

import java.util.HashSet;
import java.util.Set;

/**
 * @author sfedosov on 11/17/19.
 */
public class KthMissingElementInSortedArrayWithoutBoundaries {

    public static void main(String[] args) {
        assert missingElementOptimal(new int[]{2, 3, 4, 7, 11}, 5) == 9;
        assert missingElementOptimal(new int[]{1, 2, 3, 4}, 2) == 6;

        assert missingElementBruteForce(new int[]{2, 3, 4, 7, 11}, 5) == 9;
        assert missingElementBruteForce(new int[]{1, 2, 3, 4}, 2) == 6;
    }

    // O(logN) space, time

    // We need a way to check on how many positive integers are missing before the given array element to use binary search.
    // To do that, let's compare the input array [2, 3, 4, 7, 11] with an array with no missing integers: [1, 2, 3, 4, 5].
    // The number of missing integers is a simple difference between the corresponding elements of these two arrays:
    //
    //Before 2, there is 2 - 1 = 1 missing integer.
    //
    //Before 3, there is 3 - 2 = 1 missing integer.
    //
    //Before 4, there is 4 - 3 = 1 missing integer.
    //
    //Before 7, there are 7 - 4 = 3 missing integers.
    //
    //Before 11, there are 11 - 5 = 6 missing integers.
    // The number of positive integers which are missing before the arr[idx] is equal to arr[idx] - idx - 1

    // We are maintaining such invariant throughout the loop: left + k <= ans <= right + k.
    // Obviously when the array is missing k or more elements in the beginning, ans == k;
    // when there is no missing elements, ans is (arr.length + k);
    public static int missingElementOptimal(int[] arr, int k) {
        int left = 0, right = arr.length - 1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            // If number of positive integers
            // which are missing before arr[pivot]
            // is less than k then continue to search on the right.
            if (arr[mid] - mid - 1 < k) {
                left = mid + 1;
                // Otherwise, go left.
            } else {
                right = mid - 1;
            }
        }

        // At the end of the loop, left = right + 1,
        // and the kth missing is in-between arr[right] and arr[left].
        // The number of integers missing before arr[right] is
        // arr[right] - right - 1 -->
        // the number to return is
        // arr[right] + k - (arr[right] - right - 1) = k + left
        return left + k;
    }


    // O(N) space
    // O(N) time
    public static int missingElementBruteForce(int[] arr, int k) {
        int i = 1;
        Set<Integer> set = new HashSet<>();
        int count = 0;
        for (int x : arr) set.add(x);
        int missing = 0;
        while (count != k) {
            if (!set.contains(i)) count++;
            if (count == k) {
                missing = i;
            }
            i++;
        }
        return missing;
    }

}
