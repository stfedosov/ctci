package Leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 12/2/19.
 */
public class SubarraySumsDivisibleByK {

    public static void main(String[] args) {
        assert subarraysDivByK(new int[]{4, 5, 0, -2, -3, 1}, 5) == 7;
    }

    // Let us say prefix sums are [.......sum1.......sum2.....].
    // Now, if the numbers between sum1 and sum2 provide a sum divisible by k, that means they must have the same remainder.
    // Why?
    //
    // sum1 = x*k + r1 and sum2=y*k + r2 the difference between these two is divisible by k only when r1 == r2
    // since (y-x)*k is divisble by k by definition.
    // So it is sufficient to track the remainders of prefix sums in a map and whenever we find the same remainder,
    // it means the numbers after the remainder from the map till current remainder will form a sum % k == 0

    public static int subarraysDivByK(int[] A, int K) {
        Map<Integer, Integer> map = new HashMap<>();
        int sum = 0, count = 0;
        map.put(0, 1);
        for (int x : A) {
            sum += x;
            int remainder = sum % K;
            if (remainder < 0) {
                // If the dividend is negative,
                // the remainder could be positive or negative, ex. -27 % 5 = -2 or 3, because -27 = -6x5 + 3 and -27=-5x5 + (-2).
                // But this would cause trouble in our case since we are checking how many times a particular remainder appeared before.
                // To transfer the negative remainder to positive, we can add K to it.
                remainder += K;
            }
            count += map.getOrDefault(remainder, 0);
            map.put(remainder, map.getOrDefault(remainder, 0) + 1);
        }
        return count;
    }

}
