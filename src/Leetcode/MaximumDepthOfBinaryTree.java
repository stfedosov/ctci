package Leetcode;

/**
 * @author sfedosov on 3/12/18.
 */
public class MaximumDepthOfBinaryTree {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(3);
        root.left = new TreeNode(9);
        TreeNode node20 = new TreeNode(20);
        root.right = node20;
        TreeNode node15 = new TreeNode(15);
        node15.left = new TreeNode(17);
        node15.right = new TreeNode(18);
        node20.left = node15;
        node20.right = new TreeNode(7);
        assert maxDepth(root) == 4;
    }

    public static int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return 1 + Math.max(maxDepth(root.left), maxDepth(root.right));
    }

}
