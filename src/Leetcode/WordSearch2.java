package Leetcode;

/**
 * @author sfedosov on 12/16/18.
 */
public class WordSearch2 {

    // Given an m x n grid of characters board and a string word, return true if word exists in the grid
    // space is O(L) where L is the length of the word; and time is O(M * N * 4^L)
    public boolean exist(char[][] board, String word) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (dfs(board, i, j, 0, word)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean dfs(char[][] board, int i, int j, int index, String word) {
        if (index == word.length()) {
            return true;
        }
        if (i < 0
                || j < 0
                || i >= board.length
                || j >= board[i].length
                || word.charAt(index) != board[i][j]) {
            return false;
        }
        char tmp = board[i][j];
        board[i][j] = '#';
        if (dfs(board, i + 1, j, index + 1, word) || dfs(board, i, j + 1, index + 1, word)
                || dfs(board, i - 1, j, index + 1, word) || dfs(board, i, j - 1, index + 1, word)) {
            return true;
        }
        board[i][j] = tmp;
        return false;
    }

}
