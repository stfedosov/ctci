package Leetcode;

import java.util.Arrays;

public class UniquePaths {

    public static void main(String[] args) {
        assert uniquePaths(3, 2) == 3;
        assert uniquePaths(7, 3) == 28;
    }

    public static int uniquePaths(int m, int n) {
        int[][] memo = new int[m + 1][n + 1];
        for (int[] ints : memo) {
            Arrays.fill(ints, -1);
        }
        return uniquePathsDP(m, n, 0, 0, memo);
    }

    private static int uniquePathsDP(int m, int n, int i, int j, int[][] memo) {
        if (i == m - 1 && j == n - 1) return 1;
        if (i > m || j > n || j < 0 || i < 0) return 0;
        if (memo[i][j] != -1) return memo[i][j];
        memo[i][j] = uniquePathsDP(m, n, i + 1, j, memo) + uniquePathsDP(m, n, i, j + 1, memo);
        return memo[i][j];
    }

}
