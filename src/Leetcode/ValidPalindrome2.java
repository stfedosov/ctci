package Leetcode;

import java.util.Arrays;

public class ValidPalindrome2 {

    public static void main(String[] args) {
        assert validPalindrome("aba");
        assert validPalindrome("abca");
        assert !validPalindrome("abc");
        assert validPalindrome("aaba");
    }

    public static boolean validPalindrome(String s) {
        return validPalindrome(s, 0, s.length() - 1, 1);
    }

    public static boolean validPalindrome(String s, int i, int j, int k) {
        while (i < j) {
            if (s.charAt(i) != s.charAt(j)) {
                if (k == 0) {
                    return false;
                }
                return (validPalindrome(s, i, j - 1, k - 1) || validPalindrome(s, i + 1, j, k - 1));
            }
            i++;
            j--;
        }
        return true;
    }

    // ------------------------------------------------------
    // ------------------------------------------------------
    // valid palindrome follow up - what if we can remove up to (k) characters instead of just one?
    // ------------------------------------------------------
    // ------------------------------------------------------

    public boolean isValidPalindrome(String s, int k) {
        if (k >= s.length()) {
            return true;
        }
        int[][] cache = new int[s.length()][s.length()];
        for (int[] c : cache) {
            Arrays.fill(c, -1);
        }
        return canMakeValidPalindrome(s, k, 0, s.length() - 1, cache) <= k;
    }

    private int canMakeValidPalindrome(String s, int k, int start, int end, int[][] cache) {
        if (cache[start][end] != -1) {
            return cache[start][end];
        }
        while (start < end) {
            if (s.charAt(start) != s.charAt(end)) {
                var result = 1 + Math.min(canMakeValidPalindrome(s, k - 1, start + 1, end, cache),
                        canMakeValidPalindrome(s, k - 1, start, end - 1, cache));
                cache[start][end] = result;
                return result;
            }
            start++;
            end--;
        }
        return 0;
    }

}
