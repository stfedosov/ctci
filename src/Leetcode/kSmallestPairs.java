package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @author sfedosov on 9/30/19.
 */
public class kSmallestPairs {

    public static void main(String[] args) {
        System.out.println(kSmallestPairsBruteForce(new int[]{1, 7, 11}, new int[]{2, 4, 6}, 3));
    }

    // ----- Brute force, n*k*log(k) runtime

    public static List<List<Integer>> kSmallestPairsBruteForce(int[] nums1, int[] nums2, int k) {
        if (nums1 == null || nums2 == null || nums1.length == 0 || nums2.length == 0) return Collections.emptyList();
        Queue<int[]> pairs = new PriorityQueue<>((a, b) -> (b[0] + b[1]) - (a[0] + a[1]));
        for (int num1 : nums1) {
            for (int num2 : nums2) {
                pairs.offer(new int[]{num1, num2});
                if (pairs.size() > k) {
                    pairs.poll();
                }
            }
        }
        List<List<Integer>> result = new ArrayList<>();
        for (int[] pair : pairs) {
            result.add(Arrays.asList(pair[0], pair[1]));
        }
        return result;
    }

    // ----- Optimized k*log(k) runtime

    public List<List<Integer>> kSmallestPairsOptimized2(int[] nums1, int[] nums2, int k) {
        Queue<int[]> queue = new PriorityQueue<>(Comparator.comparingInt(a -> (a[0] + a[1])));
        List<List<Integer>> res = new ArrayList<>();
        if (nums1.length == 0 || nums2.length == 0 || k == 0) return res;
        for (int i = 0; i < nums1.length && i < k; i++) {
            queue.offer(new int[]{nums1[i], nums2[0], 0});
        }
        while (k-- > 0 && !queue.isEmpty()) {
            int[] cur = queue.poll();
            res.add(Arrays.asList(cur[0], cur[1]));
            if (cur[2] < nums2.length - 1) {
                queue.offer(new int[]{cur[0], nums2[cur[2] + 1], cur[2] + 1});
            }
        }
        return res;
    }

    // ----- Another a little bit optimized version, but still slower than previous one

    public static List<List<Integer>> kSmallestPairsOptimized(int[] nums1, int[] nums2, int k) {
        if (nums1 == null || nums2 == null || nums1.length == 0 || nums2.length == 0) return Collections.emptyList();
        Queue<List<Integer>> pairs = new PriorityQueue<>((a, b) -> (b.get(0) + b.get(1)) - (a.get(0) + a.get(1)));
        for (int num1 : nums1) {
            for (int num2 : nums2) {
                if (pairs.size() < k) pairs.offer(Arrays.asList(num1, num2));
                else {
                    List<Integer> tmp = pairs.peek();
                    if (tmp.get(0) + tmp.get(1) > num1 + num2) {
                        pairs.poll();
                        pairs.offer(Arrays.asList(num1, num2));
                    }
                }
            }
        }
        return new ArrayList<>(pairs);
    }
}
