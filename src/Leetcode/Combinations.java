package Leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sfedosov on 11/21/18.
 */
public class Combinations {

    public static void main(String[] args) {
        System.out.println(combine(4, 2));
    }

    public static List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> result = new ArrayList<>();
        combine2(result, new ArrayList<>(), 1, n, k);
        return result;
    }

    private static void combine2(List<List<Integer>> result, List<Integer> temp, int index, int n, int k) {
        if (k == 0) {
            result.add(new ArrayList<>(temp));
        } else {
            for (int i = index; i <= n; i++) {
                temp.add(i);
                combine2(result, temp, i + 1, n, k - 1);
                temp.remove(temp.size() - 1);
            }
        }
    }

}
