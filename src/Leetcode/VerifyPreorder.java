package Leetcode;

import java.util.Stack;

/**
 * @author sfedosov on 8/24/19.
 */
public class VerifyPreorder {

    public static void main(String[] args) {
        assert !verifyPreorder(new int[]{5, 2, 6, 1, 3});
        assert verifyPreorder(new int[]{5, 2, 1, 3, 6});
        assert verifyPreorder(new int[]{1, 2, 3});
        assert verifyPreorder(new int[]{1, 2});
    }

    public static boolean verifyPreorder(int[] preorder) {
        if (preorder == null || preorder.length == 0 || preorder.length == 1) return true;
        Stack<Integer> stack = new Stack<>();
        int min = Integer.MIN_VALUE;
        for (int num : preorder) {
            if (num < min) {
                return false;
            }
            while (!stack.isEmpty() && num > stack.peek()) {
                min = stack.peek();
                stack.pop();
            }
            stack.push(num);
        }
        return true;
    }

}
