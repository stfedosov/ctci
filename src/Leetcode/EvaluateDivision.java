package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class EvaluateDivision {

    public static void main(String[] args) {
        List<List<String>> equations = new ArrayList<>();
        equations.add(Arrays.asList("a", "b"));
        equations.add(Arrays.asList("b", "c"));

        double[] values = new double[]{2.0d, 3.0d};

        List<List<String>> queries = new ArrayList<>();
        queries.add(Arrays.asList("a", "c"));
        queries.add(Arrays.asList("b", "a"));
        queries.add(Arrays.asList("a", "e"));
        queries.add(Arrays.asList("a", "a"));
        queries.add(Arrays.asList("x", "x"));

        System.out.println(Arrays.toString(calcEquation(equations, values, queries)));

        equations = new ArrayList<>();
        equations.add(Arrays.asList("a", "b"));

        values = new double[]{0.5d};

        queries = new ArrayList<>();
        queries.add(Arrays.asList("a", "b"));
        queries.add(Arrays.asList("b", "a"));
        queries.add(Arrays.asList("a", "c"));
        queries.add(Arrays.asList("x", "y"));

        System.out.println(Arrays.toString(calcEquation(equations, values, queries)));

        /*
        [["a","b"]]
[0.5]
[["a","b"],["b","a"],["a","c"],["x","y"]]
// expected
[0.5,2.0,-1.0,-1.0]
         */

        /*
        [["a","b"],["c","d"],["e","f"],["g","h"]]
[4.5,2.3,8.9,0.44]
[["a","c"],["d","f"],["h","e"],["b","e"],["d","h"],["g","f"],["c","g"]]
// expected
[-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0]
         */


    }

    public static double[] calcEquation(List<List<String>> equations, double[] values, List<List<String>> queries) {
        Map<String, List<Edge>> graph = new HashMap<>();
        for (int i = 0; i < equations.size(); i++) {
            List<String> equation = equations.get(i);
            graph.computeIfAbsent(equation.get(0), x -> new ArrayList<>())
                    .add(new Edge(equation.get(0), equation.get(1), values[i]));
            graph.computeIfAbsent(equation.get(1), x -> new ArrayList<>())
                    .add(new Edge(equation.get(1), equation.get(0), 1 / values[i]));
        }
        double[] result = new double[queries.size()];
        int i = 0;
        for (List<String> query : queries) {
            result[i++] = dfs(graph, query.get(0), query.get(1));
        }
        return result;
    }

    private static double dfs(Map<String, List<Edge>> graph, String start, String end) {
        double[] d = new double[]{1.0d};
        if (dfs(graph, start, end, new HashSet<>(), d, 1.0d)) return d[0];
        return -1;
    }

    private static boolean dfs(Map<String, List<Edge>> graph,
                        String start,
                        String end,
                        Set<String> seen,
                        double[] weight,
                        double res) {
        if (!graph.containsKey(start) || !graph.containsKey(end)) return false;
        if (start.equals(end)) {
            weight[0] = res;
            return true;
        }
        seen.add(start);
        for (Edge edge : graph.get(start)) {
            if (!seen.contains(edge.end) && dfs(graph, edge.end, end, seen, weight, edge.weight * res)) {
                return true;
            }
        }
        return false;
    }

    static class Edge {
        String start, end;
        double weight;

        Edge(String start, String end, double weight) {
            this.start = start;
            this.end = end;
            this.weight = weight;
        }
    }

}
