package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author sfedosov on 4/27/20.
 */
public class FindDiagonalOrdering {

    public static void main(String[] args) {
        assert Arrays.equals(findDiagonalOrder(
                Arrays.asList(
                        Arrays.asList(1, 2, 3),
                        Arrays.asList(4, 5, 6),
                        Arrays.asList(7, 8, 9))),
                new int[]{1, 4, 2, 7, 5, 3, 8, 6, 9});

        assert Arrays.equals(findDiagonalOrder(
                Arrays.asList(
                        Arrays.asList(1, 2, 3, 4, 5),
                        Arrays.asList(6, 7),
                        Arrays.asList(8),
                        Arrays.asList(9, 10, 11),
                        Arrays.asList(12, 13, 14, 15, 16))),
                new int[]{1, 6, 2, 8, 7, 3, 9, 4, 12, 10, 5, 13, 11, 14, 15, 16});

        assert Arrays.equals(findDiagonalOrder(
                Arrays.asList(
                        Arrays.asList(1, 2, 3, 4, 5))),
                new int[]{1, 2, 3, 4, 5});

    }

    public static int[] findDiagonalOrder(List<List<Integer>> nums) {
        int n = 0;
        Map<Integer, List<Integer>> map = new TreeMap<>();
        for (int i = 0; i < nums.size(); i++) {
            for (int j = 0; j < nums.get(i).size(); j++) {
                // !!! Numbers with equal sums of row and column indexes belong to the same diagonal !!!
                map.computeIfAbsent(i + j, x -> new ArrayList<>()).add(nums.get(i).get(j));
                n++;
            }
        }
        int[] result = new int[n];
        int idx = 0;
        for (int key : map.keySet()) {
            List<Integer> l = map.get(key);
            Collections.reverse(l);
            for (int x : l) result[idx++] = x;
        }
        return result;
    }

}
