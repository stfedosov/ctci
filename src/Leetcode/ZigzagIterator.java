package Leetcode;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author sfedosov on 8/12/19.
 */
public class ZigzagIterator {

    private Queue<Integer> queue;
    private List<Integer> v1, v2;
    private int i1 = 0, i2 = 0;

    public ZigzagIterator(List<Integer> v1, List<Integer> v2) {
        this.queue = new LinkedList<>();
        this.v1 = v1;
        this.v2 = v2;
        movePointers();
    }

    public int next() {
        int result = queue.poll();
        if (!hasNext()) {
            movePointers();
        }
        return result;
    }

    private void movePointers() {
        if (i1 < v1.size()) {
            queue.offer(v1.get(i1++));
        }
        if (i2 < v2.size()) {
            queue.offer(v2.get(i2++));
        }
    }

    public boolean hasNext() {
        return !queue.isEmpty();
    }
}

class ZigzagIteratorScaledToK {

    private Queue<Iterator<Integer>> queue;

    public ZigzagIteratorScaledToK(List<Integer> v1, List<Integer> v2) {
        this.queue = new LinkedList<>();
        if (!v1.isEmpty()) {
            queue.add(v1.iterator());
        }
        if (!v2.isEmpty()) {
            queue.add(v2.iterator());
        }
    }

    public int next() {
        Iterator<Integer> polled = queue.poll();
        int result = polled.next();
        if (polled.hasNext()) queue.add(polled);
        return result;
    }

    public boolean hasNext() {
        return !queue.isEmpty();
    }
}

