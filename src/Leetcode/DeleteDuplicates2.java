package Leetcode;

import Leetcode.RotateList.ListNode;

/**
 * @author sfedosov on 11/3/19.
 */
public class DeleteDuplicates2 {

    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        ListNode next1 = new ListNode(1);
        head.next = next1;
        ListNode next11 = new ListNode(1);
        next1.next = next11;
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        next11.next = node2;
        node2.next = node3;
        System.out.println(deleteDuplicates(head));


        head = new ListNode(1);
        ListNode next2 = new ListNode(2);
        head.next = next2;
        ListNode next3 = new ListNode(3);
        next2.next = next3;
        ListNode next33 = new ListNode(3);
        next3.next = next33;
        ListNode next4 = new ListNode(4);
        next33.next = next4;
        ListNode next44 = new ListNode(4);
        next4.next = next44;
        next44.next = new ListNode(5);
        System.out.println(deleteDuplicates(head));
    }

    public static ListNode deleteDuplicates(ListNode head) {
        ListNode dummy = new ListNode(0);
        ListNode dummyIterator = dummy;
        while (head != null) {
            if (head.next != null && head.val == head.next.val) {
                while (head.next != null && head.val == head.next.val) {
                    head = head.next;
                }
            } else {
                dummyIterator.next = head;
                dummyIterator = dummyIterator.next;
            }
            head = head.next;
        }
        dummyIterator.next = null;
        return dummy.next;
    }

}
