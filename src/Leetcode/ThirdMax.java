package Leetcode;

/**
 * @author sfedosov on 11/3/19.
 */
public class ThirdMax {

    public static void main(String[] args) {
        assert thirdMax(new int[]{1, 2, -2147483648}) == -2147483648;
        assert thirdMax(new int[]{1, 2}) == 2;
        assert thirdMax(new int[]{2, 2, 3, 1}) == 1;
    }

    public static int thirdMax(int[] nums) {
        long first = Long.MIN_VALUE, second = Long.MIN_VALUE, third = Long.MIN_VALUE;
        for (int x : nums) {
            if (x > first) {
                third = second;
                second = first;
                first = x;
            } else if (x != first && x > second) {
                third = second;
                second = x;
            } else if (x != first && x != second && x > third)
                third = x;
        }
        return third == Long.MIN_VALUE ? (int) first : (int) third;
    }

}
