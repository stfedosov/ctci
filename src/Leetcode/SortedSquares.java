package Leetcode;

import java.util.Arrays;

public class SortedSquares {

    /*
    Given an integer array nums sorted in non-decreasing order,
    return an array of the squares of each number sorted in non-decreasing order.

    Input: nums = [-4,-1,0,3,10]
    Output: [0,1,9,16,100]
     */
    public static void main(String[] args) {
        System.out.println(Arrays.toString(sortedSquares(new int[]{-4, -1, 0, 3, 10})));
        System.out.println(Arrays.toString(sortedSquares(new int[]{-7, -3, 2, 3, 11})));
    }

    public static int[] sortedSquares(int[] A) {
        if (A == null) return null;
        int[] ans = new int[A.length];
        int start = 0, end = A.length - 1;
        int i = end;
        while (start <= end) {
            int pow1 = A[start] * A[start];
            int pow2 = A[end] * A[end];
            if (pow1 > pow2) {
                ans[i--] = pow1;
                start++;
            } else {
                ans[i--] = pow2;
                end--;
            }
        }
        return ans;
    }

}
