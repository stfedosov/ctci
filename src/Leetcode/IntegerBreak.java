package Leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 10/11/19.
 */
public class IntegerBreak {

    // Given an integer n, break it into the sum of k positive integers, where k >= 2, and maximize the product of those integers.
    // Return the maximum product you can get.
    public static void main(String[] args) {
        System.out.println(integerBreakOptimized(2));
        System.out.println(integerBreakOptimized(3));
        System.out.println(integerBreakOptimized(10));
    }

    public static int integerBreakOptimized(int n) {
        Map<String, Integer> cache = new HashMap<>();
        return integerBreak(n, 0, 1, cache);
    }

    private static int integerBreak(int n, int depth, int multiply, Map<String, Integer> cache) {
        String key = n + "," + multiply;
        if (cache.containsKey(key)) return cache.get(key);
        if (n == 0 && depth >= 2) return multiply;
        int max = 0;
        for (int i = 1; i <= n; i++) {
            if (n - i >= 0) max = Math.max(max, integerBreak(n - i, depth + 1, multiply * i, cache));
        }
        cache.put(key, max);
        return max;
    }


}
