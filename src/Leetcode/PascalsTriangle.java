package Leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sfedosov on 2/12/19.
 */
public class PascalsTriangle {

    public static void main(String[] args) {
        System.out.println(generate(5));
        //      1
        //     1 1
        //    1 2 1
        //   1 3 3 1
        //  1 4 6 4 1
    }

    public static List<List<Integer>> generate(int numRows) {
        List<List<Integer>> list = new ArrayList<>();
        for (int line = 0; line < numRows; line++) {
            List<Integer> lineArray = new ArrayList<>();
            for (int i = 0; i <= line; i++) {
                if (line == i || i == 0) {
                    lineArray.add(1);
                } else {
                    lineArray.add(list.get(line - 1).get(i - 1) + list.get(line - 1).get(i));
                }
            }
            list.add(lineArray);
        }
        return list;
    }

}
