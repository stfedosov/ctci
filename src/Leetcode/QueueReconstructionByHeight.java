package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QueueReconstructionByHeight {

    public static void main(String[] args) {
        assert Arrays.deepEquals(
                reconstructQueue(
                        new int[][]{{7, 0}, {4, 4}, {7, 1}, {5, 0}, {6, 1}, {5, 2}}),
                new int[][]{{5, 0}, {7, 0}, {5, 2}, {6, 1}, {4, 4}, {7, 1}}
        );
    }

    public static int[][] reconstructQueue(int[][] people) {
        // [[7,0], [4,4], [7,1], [5,0], [6,1], [5,2]]
        Arrays.sort(people, (a, b) -> a[0] == b[0] ? a[1] - b[1] : b[0] - a[0]);

        List<int[]> l = new ArrayList<>();
        for (int[] p : people) {
            l.add(p[1], p);
        }
        return l.toArray(new int[people.length][2]);
    }
}
