package Leetcode;

/**
 * @author sfedosov on 10/8/19.
 */
public class BinaryTreeLongestConsecutiveSequence {

    static int max = 0;

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode right = new TreeNode(3);
        root.right = right;
        TreeNode rightLeft = new TreeNode(2);
        TreeNode rightRight = new TreeNode(4);
        right.left = rightLeft;
        right.right = rightRight;
        rightRight.right = new TreeNode(5);
        System.out.println(longestConsecutive(root));
    }

    public static int longestConsecutive(TreeNode root) {
        longestConsecutive(root, 1);
        return max;
    }

    private static void longestConsecutive(TreeNode root, int current) {
        if (root == null) return;
        max = Math.max(max, current);
        if (root.left != null)
            longestConsecutive(root.left, root.left.val - root.val == 1 ? current + 1 : 1);
        if (root.right != null)
            longestConsecutive(root.right, root.right.val - root.val == 1 ? current + 1 : 1);
    }

}
