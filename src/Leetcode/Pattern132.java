package Leetcode;

import java.util.Stack;

public class Pattern132 {

    public static void main(String[] args) {
        System.out.println(find132pattern(new int[]{3, 1, 4, 2}));
        System.out.println(find132pattern(new int[]{-2, 1, 2, -2, 1, 2})); // true
        System.out.println(find132pattern(new int[]{1, 2, 3, 4})); // false
        System.out.println(find132pattern(new int[]{3, 1, 4, 2})); // true
        System.out.println(find132pattern(new int[]{-1, 3, 2, 0})); // true
        System.out.println(find132pattern(new int[]{1, 0, 1, -4, -3})); // false
        System.out.println(find132pattern(new int[]{3, 5, 0, 3, 4})); // true
    }

    public static boolean find132pattern(int[] nums) {
        Stack<Integer> stack = new Stack<>();
        int prev = Integer.MIN_VALUE;
        for (int i = nums.length - 1; i >= 0; i--) {
            if (nums[i] < prev) return true;
            while (!stack.isEmpty() && stack.peek() < nums[i]) {
                prev = stack.pop();
            }
            stack.push(nums[i]);
        }
        return false;
    }

}
