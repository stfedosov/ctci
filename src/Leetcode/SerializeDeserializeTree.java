package Leetcode;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;

public class SerializeDeserializeTree {

    private static final String NULL_SYMBOL = "X";
    private static final String DELIMITER = ",";

    public static void main(String[] args) {
        deserialize("0,-4,-3,X,-1,8,X,X,3,X,-9,-2,X,4,6,X");
    }

    public String serialize(TreeNode root) {

        if (root == null) {
            return NULL_SYMBOL + DELIMITER;
        }
        return root.val + DELIMITER + serialize(root.left) + serialize(root.right);
    }

    public static TreeNode deserialize(String data) {
        Queue<String> nodesLeftToMaterialize = new LinkedList<>(Arrays.asList(data.split(DELIMITER)));
        return deserializeHelper(nodesLeftToMaterialize);
    }
    public static TreeNode deserializeHelper(Queue<String> nodesLeftToMaterialize) {

        String valueForNode = nodesLeftToMaterialize.poll();

        if (Objects.equals(valueForNode, NULL_SYMBOL)) {
            return null;
        }

        TreeNode newNode = new TreeNode(Integer.parseInt(valueForNode));
        newNode.left = deserializeHelper(nodesLeftToMaterialize);
        newNode.right = deserializeHelper(nodesLeftToMaterialize);
        return newNode;
    }

}