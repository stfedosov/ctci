package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class ReconstructItinerary {

    public static void main(String[] args) {
        System.out.println(findItinerary(Arrays.asList(
                Arrays.asList("MUC", "LHR"), Arrays.asList("JFK", "MUC"), Arrays.asList("SFO", "SJC"), Arrays.asList("LHR", "SFO"))));
        //["JFK","MUC","LHR","SFO","SJC"]

        System.out.println(findItinerary(Arrays.asList(
                Arrays.asList("JFK", "SFO"), Arrays.asList("JFK", "ATL"), Arrays.asList("SFO", "ATL"), Arrays.asList("ATL", "JFK"), Arrays.asList("ATL", "SFO"))));
        // "JFK","SFO","ATL","JFK","ATL","SFO"

        System.out.println(findItinerary(Arrays.asList(Arrays.asList("JFK", "KUL"), Arrays.asList("JFK", "NRT"), Arrays.asList("NRT", "JFK"))));
        // ["JFK","NRT","JFK","KUL"]

        System.out.println(findItinerary(Arrays.asList(
                Arrays.asList("EZE", "AXA"), Arrays.asList("TIA", "ANU"), Arrays.asList("ANU", "JFK"), Arrays.asList("JFK", "ANU"), Arrays.asList("ANU", "EZE"),
                Arrays.asList("TIA", "ANU"), Arrays.asList("AXA", "TIA"), Arrays.asList("TIA", "JFK"), Arrays.asList("ANU", "TIA"), Arrays.asList("JFK", "TIA"))));
        // ["JFK","ANU","EZE","AXA","TIA","ANU","JFK","TIA","ANU","TIA","JFK"]

    }


    // Time complexity: O(N^flights) where N is the number of total flights and power is the maximum number of flights from an airport
    public static List<String> findItinerary(List<List<String>> tickets) {
        List<String> result = new ArrayList<>();
        if (tickets == null || tickets.size() == 0) {
            return result;
        }

        Map<String, List<String>> graph = new HashMap<>();
        for (List<String> ticket : tickets) {
            graph.computeIfAbsent(ticket.get(0), x -> new ArrayList<>()).add(ticket.get(1));
        }

        graph.forEach((s, values) -> Collections.sort(values));

        backtrack("JFK", result, graph, tickets.size() + 1);

        return result;
    }

    private static boolean backtrack(String start, List<String> result, Map<String, List<String>> graph, int n) {
        result.add(start);
        if (result.size() == n) {
            return true;
        }
        List<String> arrivals = graph.getOrDefault(start, Collections.emptyList());
        for (int i = 0; i < arrivals.size(); i++) {
            String arrival = graph.get(start).remove(i);
            if (backtrack(arrival, result, graph, n)) {
                return true;
            }
            result.remove(result.size() - 1);
            arrivals.add(i, arrival);
        }
        return false;
    }

    // O(|E|*log(|E|/|V|)
    public List<String> findItinerary(String[][] tickets) {
        for (String[] ticket : tickets)
            targets.computeIfAbsent(ticket[0], k -> new PriorityQueue<>()).add(ticket[1]);
        visit("JFK");
        return route;
    }

    Map<String, PriorityQueue<String>> targets = new HashMap<>();
    List<String> route = new LinkedList<>();

    // post order DFS
    private void visit(String airport) {
        while(targets.containsKey(airport) && !targets.get(airport).isEmpty())
            visit(targets.get(airport).poll());
        route.add(0, airport);
    }
}
