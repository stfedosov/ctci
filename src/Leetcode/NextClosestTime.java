package Leetcode;

import java.util.HashSet;
import java.util.Set;

/**
 * @author sfedosov on 3/15/20.
 */
public class NextClosestTime {

    public static void main(String[] args) {
        assert nextClosestTime("19:34").equals("19:39");
        assert nextClosestTime("23:59").equals("22:22");
    }

    public static String nextClosestTime(String time) {
        Set<Character> initial = getAllChars(time);
        int hh = Integer.parseInt(time.substring(0, 2)), mm = Integer.parseInt(time.substring(3));
        String result;
        while (true) {
            mm += 1;
            if (mm == 60) {
                mm = 0;
                hh++;
            }
            if (hh == 24) {
                hh = 0;
                mm = 0;
            }
            String tmp = (hh < 10 ? "0" : "") + hh + ":" + (mm < 10 ? "0" : "") + mm;
            if (initial.containsAll(getAllChars(tmp))) {
                result = tmp;
                break;
            }
        }
        return result.isEmpty() ? time : result;
    }

    private static Set<Character> getAllChars(String time) {
        Set<Character> set = new HashSet<>();
        for (char c : time.toCharArray()) set.add(c);
        return set;
    }

}
