package Leetcode;

import java.util.Deque;
import java.util.LinkedList;

public class MinMaxStack {

    public static void main(String[] args) {
        MinMaxStack minMaxStack = new MinMaxStack();
        minMaxStack.push(1);
        minMaxStack.push(2);
        minMaxStack.push(3);
        minMaxStack.push(4);
        minMaxStack.push(5);
        System.out.println(minMaxStack.min());
        System.out.println(minMaxStack.max());
        minMaxStack.push(5);
        System.out.println(minMaxStack.min());
        System.out.println(minMaxStack.max());
        minMaxStack.push(-1);
        System.out.println(minMaxStack.min());
        System.out.println(minMaxStack.max());
    }

    private static class NodeWithCount {
        public int value;
        public int count;

        NodeWithCount(int value, int count) {
            this.value = value;
            this.count = count;
        }
    }

    private Deque<Integer> actualElements = new LinkedList<>();
    private Deque<NodeWithCount> maxes = new LinkedList<>();
    private Deque<NodeWithCount> minimums = new LinkedList<>();

    public boolean isEmpty() {
        return actualElements.isEmpty();
    }

    public int max() {
        if (isEmpty()) {
            throw new IllegalStateException("max(): empty stack");
        }
        return maxes.peekFirst().value;
    }

    public int min() {
        if (isEmpty()) {
            throw new IllegalStateException("min(): empty stack");
        }
        return minimums.peekFirst().value;
    }

    public int popMax() {
        int value = maxes.peekFirst().value;
        maxes.peekFirst().count--;
        if (maxes.peekFirst().count == 0) {
            maxes.removeFirst();
        }
        Deque<Integer> buffer = new LinkedList<>();
        while (!actualElements.isEmpty()) {
            buffer.push(actualElements.pop());
        }
        while (!buffer.isEmpty()) {
            push(buffer.pop());
        }
        return value;
    }

    public int pop() {
        if (isEmpty()) {
            throw new IllegalStateException("pop(): empty stack");
        }
        int popped = actualElements.removeFirst();
        remove(popped, maxes);
        remove(popped, minimums);
        return popped;
    }

    private void remove(int popped, Deque<NodeWithCount> deque) {
        if (popped == deque.peekFirst().value) {
            deque.peekFirst().count--;
            if (deque.peekFirst().count == 0) {
                deque.removeFirst();
            }
        }
    }

    public void push(int item) {
        actualElements.addFirst(item);
        fill(maxes, item, true);
        fill(minimums, item, false);
    }

    private void fill(Deque<NodeWithCount> deque, int item, boolean isMax) {
        if (deque.isEmpty()) {
            deque.addFirst(new NodeWithCount(item, 1));
        } else {
            if (item == deque.peekFirst().value) {
                deque.peekFirst().count++;
            } else if (isMax && item > deque.peekFirst().value || !isMax && item < deque.peekFirst().value) {
                deque.addFirst(new NodeWithCount(item, 1));
            }
        }
    }

}
