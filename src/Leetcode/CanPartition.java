package Leetcode;

public class CanPartition {

    public static void main(String[] args) {
        System.out.println(canPartition(new int[]{1, 5, 11, 5}));
    }

    public static boolean canPartition(int[] nums) {
        int sum = 0;
        for (int x : nums) {
            sum += x;
        }
        if (sum % 2 != 0) return false;
        return canPartition(nums, 0, 0, sum);
    }

    private static boolean canPartition(int[] nums, int index, int sum, int total) {
        if (sum > total / 2 || index >= nums.length) return false;
        if (sum * 2 == total) return true;
        return canPartition(nums, index + 1, sum, total)
                || canPartition(nums, index + 1, sum + nums[index], total);
    }

}
