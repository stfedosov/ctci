package Leetcode;

import java.util.HashMap;
import java.util.Map;

public class WordBreak2 {

    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>() {{
            put("abc", 1);
            put("ab", 1);
            put("cd", 1);
        }};

        assert wordBreak2("abcd", map);

        map = new HashMap<>() {{
            put("a", 2);
            put("b", 2);
        }};

        assert !wordBreak2("aaab", map);

        map = new HashMap<>() {{
            put("a", 3);
            put("b", 2);
        }};

        assert wordBreak2("aaab", map);

        map = new HashMap<>() {{
            put("face", 1);
            put("book", 1);
        }};

        assert wordBreak2("facebook", map);

        map = new HashMap<>() {{
            put("abc", 3);
            put("ab", 2);
            put("abca", 1);
        }};

        assert wordBreak2("abcabcabcabca", map);
        assert !wordBreak2("abcx", map);
    }

    public static boolean wordBreak2(String word, Map<String, Integer> map) {
        return canBreak(word, map);
    }

    private static boolean canBreak(String word, Map<String, Integer> map) {
        if (word.isEmpty()) return true;
        for (int i = 1; i <= word.length(); i++) {
            String substring = word.substring(0, i);
            if (map.containsKey(substring) && map.get(substring) > 0) {
                int value = map.get(substring);
                map.put(substring, value - 1);
                if (canBreak(word.substring(i), map)) {
                    return true;
                }
                map.put(substring, value);
            }
        }
        return false;
    }

}
