package Leetcode;

public class IsSameTree {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(1);

        TreeNode root2 = new TreeNode(1);
        root2.right = new TreeNode(1);
        System.out.println(isSameTree(root, root2));
    }

    public boolean isSameTree2(TreeNode p, TreeNode q) {
        if (p == null && q == null) return true;
        if (p == null || q == null) return false;
        if (p.val != q.val) return false;
        return isSameTree2(p.left, q.left) && isSameTree2(p.right, q.right);
    }

    public static boolean isSameTree(TreeNode p, TreeNode q) {
        if (q == null && p == null) return true;
        StringBuilder pStr = new StringBuilder();
        convert(p, pStr);
        StringBuilder qStr = new StringBuilder();
        convert(q, qStr);
        return pStr.toString().equals(qStr.toString());
    }

    private static void convert(TreeNode root, StringBuilder str) {
        if (root == null) {
            str.append("#");
            return;
        }
        str.append(root.val);
        convert(root.left, str);
        convert(root.right, str);
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

}
