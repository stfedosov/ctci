package Leetcode;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author sfedosov on 3/12/18.
 */
public class SymmetricTree {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode node2 = new TreeNode(2);
        TreeNode node21 = new TreeNode(2);
        TreeNode node3 = new TreeNode(3);
        TreeNode node31 = new TreeNode(3);
        TreeNode node4 = new TreeNode(4);
        TreeNode node41 = new TreeNode(4);
        root.left = node2;
        root.right = node21;
        node2.left = node3;
        node2.right = node4;
        node21.left = node41;
        node21.right = node31;
        assert isSymmetric(root);

        root = new TreeNode(1);
        TreeNode treeNode2 = new TreeNode(2);
        root.left = treeNode2;
        treeNode2.right = new TreeNode(3);
        TreeNode treeNode22 = new TreeNode(2);
        root.right = treeNode22;
        treeNode22.right = new TreeNode(3);
        assert !isSymmetric2(root);
    }

    // returns true if trees with roots as root1 and root2 are mirror
    static boolean isMirror(TreeNode node1, TreeNode node2) {
        // if both trees are empty, then they are mirror image
        if (node1 == null && node2 == null)
            return true;

        // For two trees to be mirror images, the following three
        // conditions must be true
        // 1 - Their root node's key must be same
        // 2 - left subtree of left tree and right subtree
        //      of right tree have to be mirror images
        // 3 - right subtree of left tree and left subtree
        //      of right tree have to be mirror images
        if (node1 != null && node2 != null && node1.val == node2.val) {
            return isMirror(node1.left, node2.right) && isMirror(node1.right, node2.left);
        }

        // if neither of the above conditions is true then
        // root1 and root2 are not mirror images
        return false;
    }

    // returns true if the tree is symmetric i.e
    // mirror image of itself
    static boolean isSymmetric(TreeNode node) {
        // check if tree is mirror of itself
        return isMirror(node, node);
    }

    public static boolean isSymmetric2(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<>();
        Deque<TreeNode> deque = new LinkedList<>();
        if (root == null) return true;
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            deque.addAll(queue);
            while (size-- > 0) {
                TreeNode polled = queue.poll();
                if (polled.val == 999) continue;
                if (polled.left != null) {
                    queue.offer(polled.left);
                } else {
                    queue.offer(new TreeNode(999));
                }
                if (polled.right != null) {
                    queue.offer(polled.right);
                } else {
                    queue.offer(new TreeNode(999));
                }
            }
            if (deque.size() == 1) deque.poll();
            while (!deque.isEmpty()) {
                if (deque.pollFirst().val != deque.pollLast().val) {
                    return false;
                }
            }
        }
        return true;
    }

}
