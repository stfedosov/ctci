package Leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 6/3/19.
 */
public class NumDecodings {

    public static void main(String[] args) {
        assert numDecodings("0") == 0;
        assert numDecodings("12") == 2;
        assert numDecodings("27") == 1;
    }

    public static Object palindrome(int[] array, int idx) {
        // Write your code here
        // Modify the return statement to "true" or "false"
        if ((array.length % 2 == 0 && idx == array.length / 2) ||
                (array.length % 2 != 0 && idx == array.length / 2 + 1)) {
            return true;
        }
        if (array[idx] == array[array.length - idx]) {
            return palindrome(array, idx + 1);
        }
        return false;
    }

    public static int numDecodings(String s) {
        return numDecodings(s, 0);
    }

    private static int numDecodings(String s, int index) {
        if (index == s.length()) {
            return 1;
        }

        if (index < s.length() && s.charAt(index) - '0' < 1) {
            return 0;
        }

        int ways = numDecodings(s, index + 1);
        if (index < s.length() - 1 && ((s.charAt(index) - '0' <= 2 && s.charAt(index + 1) - '0' < 7)
                || (s.charAt(index) - '0' <= 1))) {
            ways += numDecodings(s, index + 2);
        }
        return ways;
    }

    public static int numDecodings2(String s) {
        return numDecodings(s, new HashMap<>());
    }

    public static int numDecodings(String s, Map<String, Integer> cache) {
        if (s.isEmpty()) return 1;
        if (cache.containsKey(s)) return cache.get(s);
        int total = 0;
        int num = Integer.parseInt("" + s.charAt(0));
        if (num > 0 && num <= 26) {
            total += numDecodings(s.substring(1), cache);
        }
        if (s.length() >= 2) {
            if (!s.startsWith("0")) {
                num = Integer.parseInt(s.substring(0, 2));
                if (num > 0 && num <= 26) {
                    total += numDecodings(s.substring(2), cache);
                }
            }
        }
        cache.put(s, total);
        return total;
    }
}
