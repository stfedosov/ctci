package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 3/10/18.
 */
public class CountPrimes {

    public static void main(String[] args) {
        assert countPrimes(5) == 2;
        assert countPrimes(3) == 1;
    }

    public static int countPrimes(int n) {
        boolean[] primes = new boolean[n];
        Arrays.fill(primes, true);        // assume all integers are prime.
        primes[0] = primes[1] = false;       // we know 0 and 1 are not prime.
        for (int i = 2; i < primes.length; i++) {
            //if the number is prime,
            //then go through all its multiples and make their values false.
            if (primes[i]) {
                for (int j = 2; i * j < primes.length; j++) {
                    primes[i * j] = false;
                }
            }
        }
        int count = 0;
        for (boolean x : primes) {
            if (x) count++;
        }
        return count;
    }

}
