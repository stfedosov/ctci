package Leetcode;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author sfedosov on 9/12/19.
 */
public class TimeMap2 {

    public static void main(String[] args) {
        TimeMap2 timeMap2 = new TimeMap2();
        timeMap2.set("ctondw", "ztpearaw", 1);
        timeMap2.set("vrobykydll", "hwliiq", 2);
        timeMap2.set("gszaw", "ztpearaw", 3);
        timeMap2.set("ctondw", "gszaw", 4);
        System.out.println(timeMap2.get("gszaw", 5));
        System.out.println(timeMap2.get("ctondw", 6));
        System.out.println(timeMap2.get("ctondw", 7));
        System.out.println(timeMap2.get("gszaw", 8));
        System.out.println(timeMap2.get("vrobykydll", 9));
        System.out.println(timeMap2.get("ctondw", 10));
        timeMap2.set("vrobykydll", "kcvcjzzwx", 11);
        System.out.println(timeMap2.get("vrobykydll", 12));
        System.out.println(timeMap2.get("ctondw", 13));
        System.out.println(timeMap2.get("vrobykydll", 14));
        timeMap2.set("ztpearaw", "zondoubtib", 15);
        timeMap2.set("kcvcjzzwx", "hwliiq", 16);
        timeMap2.set("wtgbfvg", "vrobykydll", 17);
        timeMap2.set("hwliiq", "gzsiivks", 18);
        System.out.println(timeMap2.get("kcvcjzzwx", 19));
        System.out.println(timeMap2.get("ztpearaw", 20));
    }

    Map<String, TreeMap<Integer, String>> map;

    /**
     * Initialize your data structure here.
     */
    public TimeMap2() {
        this.map = new HashMap<>();
    }

    public void set(String key, String value, int timestamp) {
        map.putIfAbsent(key, new TreeMap<>());
        map.get(key).put(timestamp, value);
    }

    public String get(String key, int timestamp) {
        if (!map.containsKey(key)) return "";
        Map.Entry<Integer, String> entry = map.get(key).floorEntry(timestamp);
        return entry == null ? "" : entry.getValue();
    }

}
