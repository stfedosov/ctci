package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 12/31/18.
 */
public class GameOfLife {

    public static void main(String[] args) {
        int[][] board = {
                {0, 1, 0},
                {0, 0, 1},
                {1, 1, 1},
                {0, 0, 0}};
        gameOfLife(board);
        System.out.println(Arrays.deepToString(board));
    }

    private static int[][] directions = new int[][]{{-1, 1}, {-1, 0}, {1, -1}, {1, 0}, {0, 1}, {0, -1}, {-1, -1}, {1, 1}};

    public static void gameOfLife(int[][] board) {
        int[][] nextState = new int[board.length][board[0].length];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                nextState(board, nextState, i, j);
            }
        }
        for (int i = 0; i < board.length; i++) {
            System.arraycopy(nextState[i], 0, board[i], 0, board[0].length);
        }
    }

    public static void nextState(int[][] board, int[][] next, int m, int n) {
        int neighborCount = getNeighborCount(board, m, n);
        if (board[m][n] == 1) {
            if (neighborCount < 2 || neighborCount > 3) {
                next[m][n] = 0;
            } else {
                next[m][n] = 1;
            }
        } else if (neighborCount == 3) {
            next[m][n] = 1;
        } else {
            next[m][n] = 0;
        }
    }

    private static int getNeighborCount(int[][] board, int m, int n) {
        int count = 0;
        for (int[] direction : directions) {
            int newM = m + direction[0], newN = n + direction[1];
            if (newM < 0 || newM >= board.length || newN < 0 || newN >= board[0].length) continue;
            count += board[newM][newN];
        }
        return count;
    }
}
