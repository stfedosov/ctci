package Leetcode;

/**
 * @author sfedosov on 3/5/20.
 */
public class LongestSubstring {

    public static void main(String[] args) {
        assert longestSubstring("bbaaacbd", 3) == 3;
        assert longestSubstring("aaabb", 3) == 3;
        assert longestSubstring("ababbc", 2) == 5;
    }

    // Time complexity O(26 * N) ~ O(N)
    // Space O(1), as we won't go farther than size 26
    public static int longestSubstring(String s, int k) {
        int[] map = new int[26];
        for (char c : s.toCharArray()) map[c - 'a']++;
        for (int i = 0; i < s.length(); i++) {
            if (map[s.charAt(i) - 'a'] < k) {
                return Math.max(longestSubstring(s.substring(0, i), k), longestSubstring(s.substring(i + 1), k));
            }
        }
        return s.length();
    }

}
