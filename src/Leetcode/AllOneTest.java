package Leetcode;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

public class AllOneTest {

    public static void main(String[] args) {
        AllOne allOne = new AllOne();
        allOne.inc("hello");
        allOne.inc("hello");
        assert allOne.getMaxKey().equals("hello");
        assert allOne.getMinKey().equals("hello");
        allOne.inc("leet");
        assert allOne.getMaxKey().equals("hello");
        assert allOne.getMinKey().equals("leet");

        AllOneOptimal allOneOpt = new AllOneOptimal();
        allOneOpt.inc("hello");
        allOneOpt.inc("hello");
        assert allOneOpt.getMaxKey().equals("hello");
        assert allOneOpt.getMinKey().equals("hello");
        allOneOpt.inc("leet");
        assert allOneOpt.getMaxKey().equals("hello");
        assert allOneOpt.getMinKey().equals("leet");

        allOne = new AllOne();
        allOne.inc("hello");
        allOne.inc("goodbye");
        allOne.inc("hello");
        allOne.inc("hello");
        assert allOne.getMaxKey().equals("hello");
        allOne.inc("leet");
        allOne.inc("code");
        allOne.inc("leet");
        allOne.dec("hello");
        allOne.inc("leet");
        allOne.inc("code");
        allOne.inc("code");
        assert allOne.getMaxKey().equals("leet");

        allOne = new AllOne();
        allOne.inc("a");
        allOne.inc("b");
        allOne.inc("b");
        allOne.inc("c");
        allOne.inc("c");
        allOne.inc("c");
        allOne.dec("b");
        allOne.dec("b");
        assert allOne.getMinKey().equals("a");
        allOne.dec("a");
        assert allOne.getMaxKey().equals("c");
        assert allOne.getMinKey().equals("c");

        AllOneOptimal allOneOpt2 = new AllOneOptimal();
        allOneOpt2.inc("a");
        allOneOpt2.inc("b");
        allOneOpt2.inc("b");
        allOneOpt2.inc("c");
        allOneOpt2.inc("c");
        allOneOpt2.inc("c");
        allOneOpt2.dec("b");
        allOneOpt2.dec("b");
        assert allOneOpt2.getMinKey().equals("a");
        allOneOpt2.dec("a");
        assert allOneOpt2.getMaxKey().equals("c");
        assert allOneOpt2.getMinKey().equals("c");


    }


    public static class AllOneOptimal {

        Map<String, Node> map;
        Node head;
        Node tail;

        /**
         * Initialize your data structure here.
         */
        public AllOneOptimal() {
            this.map = new HashMap<>();
            this.head = new Node(-1);
            this.tail = new Node(-1);
            this.head.prev = tail;
            this.tail.next = head;
        }

        /**
         * Inserts a new key <Key> with value 1. Or increments an existing key by 1.
         */
        public void inc(String key) {
            if (map.containsKey(key)) {
                Node node = map.get(key);
                if (node.val + 1 == node.next.val) {
                    node.next.keys.add(key);
                    map.put(key, node.next);
                } else {
                    Node newNode = new Node(key, node.val + 1);
                    insertNext(node, newNode);
                    map.put(key, newNode);
                }
                removeKeyFromNode(key, node);
            } else {
                if (tail.next.val == 1) {
                    tail.next.keys.add(key);
                    map.put(key, tail.next);
                } else {
                    Node newNode = new Node(key, 1);
                    insertNext(tail, newNode);
                    map.put(key, newNode);
                }
            }
        }

        private void deleteNode(Node node) {
            node.next.prev = node.prev;
            node.prev.next = node.next;
        }

        private void removeKeyFromNode(String key, Node node) {
            node.keys.remove(key);
            if (node.keys.isEmpty()) {
                deleteNode(node);
            }
        }

        private void insertNext(Node node, Node newNode) {
            node.next.prev = newNode;
            newNode.next = node.next;
            node.next = newNode;
            newNode.prev = node;
        }

        /**
         * Decrements an existing key by 1. If Key's value is 1, remove it from the data structure.
         */
        public void dec(String key) {
            Node node = map.get(key);
            if (node.val == 1) {
                map.remove(key);
            } else if (node.val - 1 == node.prev.val) {
                node.prev.keys.add(key);
                map.put(key, node.prev);
            } else {
                Node newNode = new Node(key, node.val - 1);
                map.put(key, newNode);
                insertNext(node.prev, newNode);
            }
            removeKeyFromNode(key, node);
        }

        /**
         * Returns one of the keys with maximal value.
         */
        public String getMaxKey() {
            return head.prev.keys.isEmpty() ? "" : head.prev.keys.iterator().next();
        }

        /**
         * Returns one of the keys with Minimal value.
         */
        public String getMinKey() {
            return tail.next.keys.isEmpty() ? "" : tail.next.keys.iterator().next();
        }

        // Each node contains all keys with the same value.
        class Node {
            Set<String> keys;
            int val;
            Node prev = null;
            Node next = null;

            Node(int val) {
                this.val = val;
                this.keys = new HashSet<>();
            }

            Node(String key, int val) {
                this(val);
                this.keys.add(key);
            }
        }


    }


    // BRUTE FORCE - TWO HEAPS

    public static class AllOne {

        Map<String, Integer> map;
        Queue<Node> minHeap = new PriorityQueue<>(Comparator.comparingInt(a -> a.val));
        Queue<Node> maxHeap = new PriorityQueue<>((a, b) -> b.val - a.val);

        /**
         * Initialize your data structure here.
         */
        public AllOne() {
            this.map = new HashMap<>();
        }

        /**
         * Inserts a new key <Key> with value 1. Or increments an existing key by 1.
         */
        public void inc(String key) {
            if (map.containsKey(key)) {
                map.put(key, map.get(key) + 1);
                Node node = new Node(key, map.get(key) - 1);
                minHeap.remove(node);
                maxHeap.remove(node);
                node.val += 1;
                minHeap.offer(node);
                maxHeap.offer(node);
            } else {
                map.put(key, 1);
                Node node = new Node(key, 1);
                minHeap.offer(node);
                maxHeap.offer(node);
            }
        }

        /**
         * Decrements an existing key by 1. If Key's value is 1, remove it from the data structure.
         */
        public void dec(String key) {
            if (map.get(key) == 1) {
                map.remove(key);
                Node node = new Node(key, 1);
                minHeap.remove(node);
                maxHeap.remove(node);
            } else {
                map.put(key, map.get(key) - 1);
                Node node = new Node(key, map.get(key) + 1);
                minHeap.remove(node);
                maxHeap.remove(node);
                node.val -= 1;
                minHeap.offer(node);
                maxHeap.offer(node);
            }
        }

        /**
         * Returns one of the keys with maximal value.
         */
        public String getMaxKey() {
            Node peek = maxHeap.peek();
            return peek == null ? "" : peek.key;
        }

        /**
         * Returns one of the keys with Minimal value.
         */
        public String getMinKey() {
            Node peek = minHeap.peek();
            return peek == null ? "" : peek.key;
        }

        class Node {
            String key;
            Integer val;

            public Node(String key, Integer val) {
                this.key = key;
                this.val = val;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Node node = (Node) o;
                return Objects.equals(key, node.key) && Objects.equals(val, node.val);
            }

            @Override
            public int hashCode() {
                return Objects.hash(key, val);
            }
        }
    }

}
