package Leetcode;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Collector;

public class CustomSortSorting {

    public static void main(String[] args) {
        System.out.println(customSortStringBucketSort("cba", "abcd"));
        System.out.println(customSortString("awjeuldzxknyocsrfpvq", "itnmbfynwuktoeulvfrjowqqnburnvendrafgfcgjiaovffzoxocsonpxmuqfhluzhaiuvldldvrcvofsfnaqfxrdnkvoguxqbke"));
    }

    public static String customSortString(String S, String T) {
        Character[] chars = new Character[T.length()];
        int i = 0;
        for (char c : T.toCharArray()) {
            chars[i++] = c;
        }
        return Arrays.stream(chars)
                .sorted(Comparator.comparingInt(S::indexOf))
                .collect(Collector.of(StringBuilder::new, StringBuilder::append, StringBuilder::append, StringBuilder::toString));
    }

    public static String customSortStringBucketSort(String order, String s) {
        int[] count = new int[26];
        for (char c : s.toCharArray()) {
            count[c - 'a']++;  // count each char in T.
        }
        StringBuilder sb = new StringBuilder();
        for (char c : order.toCharArray()) {
            while (count[c - 'a']-- > 0) {
                sb.append(c);  // sort chars in 's' according to how they appear in 'order' string;
            }
        }
        for (char c = 'a'; c <= 'z'; c++) {
            while (count[c - 'a']-- > 0) {
                sb.append(c); // put remaining chars in T but not in S.
            }
        }
        return sb.toString();
    }

}
