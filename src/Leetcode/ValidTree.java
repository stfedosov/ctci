package Leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sfedosov on 10/28/19.
 */
public class ValidTree {

    public static void main(String[] args) {
        assert validTree(1, new int[][]{});
        assert !validTree(4, new int[][]{{2, 3}, {1, 2}, {1, 3}});
        assert validTree(5, new int[][]{{0, 1}, {0, 2}, {0, 3}, {0, 4}});
    }

    public static boolean validTree(int n, int[][] edges) {
        if (edges.length == 0) return n == 1;
        Map<Integer, List<Integer>> graph = new HashMap<>();
        for (int[] edge : edges) {
            graph.computeIfAbsent(edge[0], x -> new ArrayList<>()).add(edge[1]);
            graph.computeIfAbsent(edge[1], x -> new ArrayList<>()).add(edge[0]);
        }
        boolean[] visited = new boolean[n];
        if (hasCycle(edges[0][0], graph, visited, -1)) return false;
        for (boolean b : visited) if (!b) return false;
        return true;
    }

    private static boolean hasCycle(int start, Map<Integer, List<Integer>> graph, boolean[] visited, int parent) {
        visited[start] = true;
        for (int node : graph.getOrDefault(start, Collections.emptyList())) {
            if (node == parent) continue;
            if (visited[node] || hasCycle(node, graph, visited, start)) {
                return true;
            }
        }
        return false;
    }

}
