package Leetcode;

public class CherryPickup2 {

    public static void main(String[] args) {
        assert cherryPickup(new int[][]{
                {3, 1, 1},
                {2, 5, 1},
                {1, 5, 5},
                {2, 1, 1}
        }) == 24;
    }

    public static int cherryPickup(int[][] grid) {
        return dfs(0, 0, grid[0].length - 1, grid, new Integer[grid.length][grid[0].length][grid[0].length]);
    }

    private static int dfs(int x, int y1, int y2, int[][] grid, Integer[][][] cache) {
        if (x == grid.length) return 0;
        if (cache[x][y1][y2] != null) return cache[x][y1][y2];
        int max = 0;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                int new_y1 = y1 + i, new_y2 = y2 + j;
                if (new_y1 >= 0 && new_y2 >= 0 && new_y1 < grid[0].length && new_y2 < grid[0].length) {
                    max = Math.max(max, dfs(x + 1, new_y1, new_y2, grid, cache));
                }
            }
        }
        int sum = y1 == y2 ? grid[x][y1] : grid[x][y1] + grid[x][y2];
        cache[x][y1][y2] = sum + max;
        return cache[x][y1][y2];
    }

}
