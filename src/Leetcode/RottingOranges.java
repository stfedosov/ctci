package Leetcode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author sfedosov on 8/27/19.
 */
public class RottingOranges {

    public static void main(String[] args) {
        assert orangesRotting(new int[][]{{2, 1, 1}, {1, 1, 0}, {0, 1, 1}}) == 4;
        assert orangesRotting(new int[][]{{2, 1, 0, 2}}) == 1;
        assert orangesRotting(new int[][]{{2, 2, 2, 1, 1}}) == 2;
        assert orangesRotting(new int[][]{{1, 2, 1, 1, 2, 1, 1}}) == 2;
        assert orangesRotting(new int[][]{{0, 2}}) == 0;
    }

    public static int orangesRotting(int[][] grid) {
        Queue<int[]> queue = new LinkedList<>();

        // add for consideration all rotten oranges surrounded by the fresh ones
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == 2) {
                    queue.offer(new int[]{i, j, 0}); // current row, column and time to become rotten
                }
            }
        }

        // if there is not any, check is there any fresh
        if (queue.isEmpty()) {
            if (!hasFreshOne(grid)) return 0;
            return -1;
        }

        int time = 0;
        while (!queue.isEmpty()) {
            int[] rotten = queue.poll();
            int i = rotten[0];
            int j = rotten[1];
            int minute = rotten[2];
            time = minute;
            if (nextIsFresh(i + 1, j, grid)) {
                grid[i + 1][j] = 2;
                queue.offer(new int[]{i + 1, j, minute + 1});
            }
            if (nextIsFresh(i - 1, j, grid)) {
                grid[i - 1][j] = 2;
                queue.offer(new int[]{i - 1, j, minute + 1});
            }
            if (nextIsFresh(i, j + 1, grid)) {
                grid[i][j + 1] = 2;
                queue.offer(new int[]{i, j + 1, minute + 1});
            }
            if (nextIsFresh(i, j - 1, grid)) {
                grid[i][j - 1] = 2;
                queue.offer(new int[]{i, j - 1, minute + 1});
            }
        }
        // if there are still fresh ones we can consider them as not reachable
        if (hasFreshOne(grid)) return -1;
        return time;
    }

    private static boolean nextIsFresh(int i, int j, int[][] grid) {
        return j >= 0 && i >= 0 && i < grid.length && j < grid[i].length && grid[i][j] == 1;
    }

    private static boolean hasFreshOne(int[][] grid) {
        for (int[] row : grid) {
            for (int val : row) {
                if (val == 1) {
                    return true;
                }
            }
        }
        return false;
    }
}
