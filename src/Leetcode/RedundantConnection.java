package Leetcode;

import java.util.Arrays;

public class RedundantConnection {

    // Return an edge that can be removed so that the resulting graph is a tree of n nodes.
    // In tree we have only one unique way from one node to another
    public static void main(String[] args) {
        assert Arrays.equals(new int[]{2, 3}, findRedundantConnection(new int[][]{{1, 2}, {1, 3}, {2, 3}}));
        //    1
        //   / \
        //  2 - 3

        assert Arrays.equals(new int[]{1, 4}, findRedundantConnection(new int[][]{{1, 2}, {2, 3}, {3, 4}, {1, 4}, {1, 5}}));
        // 5 - 1 - 2
        //     |   |
        //     4 - 3
    }

    // Time: O(N * α(N)), where N is number of vertices in the graph.
    // Using both path compression (implemented) and union by size (not implemented) ensures that the amortized time
    // per UnionFind operation is only α(n), which is optimal, where α(n) is the inverse Ackermann function.
    // This function has a value α(n) < 5 for any value of n that can be written in this physical universe,
    // so the disjoint-set operations take place in essentially constant time.
    public static int[] findRedundantConnection(int[][] edges) {
        UnionFind u = new UnionFind(edges.length);
        for (int[] edge : edges) {
            // if u and v is already connected in the UnionFind then we return that redundant edge.
            if (!u.union(edge[0] - 1, edge[1] - 1)) return edge;
        }
        return new int[0];
    }

    private static class UnionFind {

        private final int[] parent;

        public UnionFind(int length) {
            parent = new int[length];
            Arrays.fill(parent, -1);
        }

        public boolean union(int x, int y) {
            int xx = find(x), yy = find(y);
            if (xx == yy) return false;
            parent[xx] = yy;
            return true;
        }

        private int find(int x) {
            if (parent[x] == -1) {
                return x;
            }
            return parent[x] = find(parent[x]); // Path compression
        }

    }

}
