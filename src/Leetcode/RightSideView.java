package Leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class RightSideView {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode treeNode2 = new TreeNode(2);
        root.left = treeNode2;
        treeNode2.right = new TreeNode(5);
        TreeNode treeNode3 = new TreeNode(3);
        root.right = treeNode3;
        treeNode3.right = new TreeNode(4);
        System.out.println(rightSideView(root));

        /*

        Output: 1 - 3 - 4

           1            <---
         /   \
        2     3         <---
         \     \
          5     4       <---

         */
    }

    // Time complexity: O(N)
    // Space complexity: O(D) to keep the queues, where D is a tree diameter
    public static List<Integer> rightSideView(TreeNode root) {
        if (root == null) return Collections.emptyList();
        Queue<TreeNode> q = new LinkedList<>();
        q.offer(root);
        List<Integer> list = new ArrayList<>();
        while (!q.isEmpty()) {
            int size = q.size();
            while (size > 0) {
                TreeNode polled = q.poll();
                if (size == 1) {
                    list.add(polled.val);
                }
                if (polled.left != null) {
                    q.offer(polled.left);
                }
                if (polled.right != null) {
                    q.offer(polled.right);
                }
                size--;
            }
        }
        return list;
    }

    public List<Integer> rightSideViewRecursive(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        rightSideView(root, 0, list);
        return list;
    }

    public void rightSideView(TreeNode root, int level, List<Integer> list) {
        if (root == null) return;
        if (list.size() == level) list.add(root.val);
        rightSideView(root.right, level + 1, list);
        rightSideView(root.left, level + 1, list);
    }

}
