package Leetcode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author sfedosov on 9/25/19.
 */
public class DeleteNodesAndReturnForest {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode treeNode2 = new TreeNode(2);
        root.left = treeNode2;
        treeNode2.left = new TreeNode(4);
        treeNode2.right = new TreeNode(3);
        System.out.println(delNodes(root, new int[]{2, 3}));
    }

    public static List<TreeNode> delNodes(TreeNode root, int[] to_delete) {
        List<TreeNode> list = new ArrayList<>();
        Set<Integer> set = new HashSet<>();
        for (int x : to_delete) set.add(x);
        delNodes(root, list, set);
        if (!set.contains(root.val)) list.add(root);
        return list;
    }

    private static TreeNode delNodes(TreeNode root, List<TreeNode> list, Set<Integer> toDelete) {
        if (root == null) return null;
        root.left = delNodes(root.left, list, toDelete);
        root.right = delNodes(root.right, list, toDelete);
        if (toDelete.contains(root.val)) {
            if (root.left != null) list.add(root.left);
            if (root.right != null) list.add(root.right);
            return null;
        }
        return root;
    }

}
