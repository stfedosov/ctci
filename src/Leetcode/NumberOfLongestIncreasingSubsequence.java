package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 7/29/19.
 */
public class NumberOfLongestIncreasingSubsequence {

    public static void main(String[] args) {
        assert findNumberOfLIS(new int[]{2, 2, 2, 2, 2}) == 5;
        // there are 5 longest increasing subsequences and all of them are 2

        assert findNumberOfLIS(new int[]{1, 3, 5, 4, 7}) == 2;
        // 1,3,4,7
        // 1,3,5,7

        System.out.println(findNumberOfLIS(new int[]{1, 2, 4, 3, 5, 4, 7, 2}));
        // 1,2,3,5,7
        // 1,2,4,4,7
        // 1,2,3,4,7
    }

    /*
    Suppose for sequences ending at nums[i], we knew the length length[i] of the longest sequence,
    and the number count[i] of such sequences with that length.

    For every i < j with A[i] < A[j], we might append A[j] to a longest subsequence ending at A[i].
    It means that we have demonstrated counts[i] subsequences of length lengths[i] + 1.

    Now, if those sequences are longer than lengths[j], then we know we have count[i] sequences of this length.
    If these sequences are equal in length to length[j],
    then we know that there are now counts[i] additional sequences to be counted of that length (ie. counts[j] += counts[i]).
     */

    public static int findNumberOfLIS(int[] nums) {
        int N = nums.length;
        if (N <= 1) return N;
        int[] lengths = new int[N]; // lengths[i] = length of longest ending in nums[i]
        int[] counts = new int[N]; // counts[i] = number of longest ending in nums[i]
        Arrays.fill(counts, 1);
        int longest = 0;

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < i; j++)
                if (nums[j] < nums[i]) {
                    if (lengths[j] + 1 == lengths[i]) {
                        counts[i] += counts[j];
                    } else {
                        lengths[i] = Math.max(lengths[i], lengths[j] + 1);
                        longest = Math.max(longest, lengths[j]);
                        counts[j] = counts[i];
                    }
                }
        }

        int ans = 0;
        for (int i = 0; i < N; ++i) {
            if (lengths[i] == longest) {
                ans += counts[i];
            }
        }
        return ans;
    }

}
