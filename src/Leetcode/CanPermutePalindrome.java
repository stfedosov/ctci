package Leetcode;

/**
 * @author sfedosov on 11/29/19.
 */
public class CanPermutePalindrome {

    public static void main(String[] args) {
        assert !canPermutePalindrome("code");
        assert canPermutePalindrome("aab");
        assert canPermutePalindrome("carerac");
    }

    public static boolean canPermutePalindrome(String s) {
        int[] chars = new int[128];
        for (char c : s.toCharArray()) chars[c]++;
        int count = 0;
        for (int x : chars) if (x % 2 != 0) count++;
        return count <= 1;
    }

}
