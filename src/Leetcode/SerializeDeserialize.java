package Leetcode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author sfedosov on 5/4/19.
 */
public class SerializeDeserialize {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode treeNode2 = new TreeNode(2);
        treeNode2.left = new TreeNode(4);
        root.left = treeNode2;
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode5 = new TreeNode(5);
        treeNode5.left = new TreeNode(70);
        treeNode3.left = treeNode5;
        treeNode3.right = new TreeNode(6);
        root.right = treeNode3;
        System.out.println(deserialize(serialize(root)));
    }

    // Encodes a tree to a single string.
    public static String serialize(TreeNode root) {
        if (root == null) {
            return "X";
        }
        return root.val + "," + serialize(root.left) + "," + serialize(root.right);
    }

    // Decodes your encoded data to tree.
    public static TreeNode deserialize(String data) {
        System.out.println(data);
        String[] split = data.split(",");
        Queue<String> q = new LinkedList<>();
        for (String s : split) q.offer(s);
        return construct(q);
    }

    private static TreeNode construct(Queue<String> q) {
        if (q.isEmpty() || q.peek().equals("X")) {
            q.poll();
            return null;
        }
        TreeNode root = new TreeNode(Integer.parseInt(q.poll()));
        root.left = construct(q);
        root.right = construct(q);
        return root;
    }

}
