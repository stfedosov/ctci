package Leetcode;

import java.util.Stack;

public class ValidParenthesis {

    public static void main(String[] args) {
        assert !isValid("([)]");
        assert isValid("()[]{}");
    }

    public static boolean isValid(String s) {
        if (s == null) return false;
        if (s.isEmpty()) return true;
        Stack<Character> st = new Stack<>();
        char[] charArray = s.toCharArray();
        for (char ch : charArray) {
            switch (ch) {
                case '(':
                case '{':
                case '[':
                    st.push(ch);
                    break;
                case ')':
                    if (st.isEmpty() || st.pop() != '(') return false;
                    break;
                case '}':
                    if (st.isEmpty() || st.pop() != '{') return false;
                    break;
                case ']':
                    if (st.isEmpty() || st.pop() != '[') return false;
                    break;
                default:
                    return false;
            }
        }
        return st.isEmpty();
    }

}
