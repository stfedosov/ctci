package Leetcode;

import java.util.Stack;

public class LargestRectangle {

    // histogram
    public static void main(String[] args) {
        assert largestRectangleAreaBruteForce(new int[]{2, 1, 5, 6, 2, 3}) == 10;
        assert largestRectangleArea(new int[]{2, 1, 5, 6, 2, 3}) == 10;
        assert largestRectangleAreaBruteForce(new int[]{0, 9}) == 9;
        assert largestRectangleArea(new int[]{0, 9}) == 9;
        assert largestRectangleAreaBruteForce(new int[]{1}) == 1;
        assert largestRectangleArea(new int[]{1}) == 1;
        assert largestRectangleAreaBruteForce(new int[]{}) == 0;
        assert largestRectangleArea(new int[]{}) == 0;
        System.out.println(largestRectangleArea(new int[]{2, 1, 5, 6, 2, 3}));
    }

    public static int largestRectangleArea(int[] heights) {
        Stack<Integer> stack = new Stack<>();
        int length = heights.length;
        int maxArea = 0;
        for (int i = 0; i < length; i++) {
            while (!stack.isEmpty() && heights[stack.peek()] >= heights[i]) {
                int currentHeight = heights[stack.pop()];
                int currentWidth = i - (stack.isEmpty() ? 0 : stack.peek() + 1);
                maxArea = Math.max(maxArea, currentHeight * currentWidth);
            }
            stack.push(i);
        }
        while (!stack.isEmpty()) {
            int currentHeight = heights[stack.pop()];
            int currentWidth = length - (stack.isEmpty() ? 0 : stack.peek() + 1);
            maxArea = Math.max(maxArea, currentHeight * currentWidth);
        }
        return maxArea;
    }

    public static int largestRectangleAreaBruteForce(int[] heights) {
        int max = 0;
        for (int i = 0; i < heights.length; i++) {
            int minHeight = Integer.MAX_VALUE;
            for (int j = i; j < heights.length; j++) {
                minHeight = Math.min(minHeight, heights[j]);
                max = Math.max(max, (j - i + 1) * minHeight);
            }
        }
        return max;
    }

}
