package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DiagonalSort {

    public static void main(String[] args) {
        assert Arrays.deepEquals(diagonalSort(new int[][]{{3, 3, 1, 1}, {2, 2, 1, 2}, {1, 1, 1, 2}}),
                new int[][]{{1, 1, 1, 1}, {1, 2, 2, 2}, {1, 2, 3, 3}});
    }

    public static int[][] diagonalSort(int[][] mat) {
        for (int i = 0; i < mat.length; i++) {
            doSort(i, 0, mat);
        }
        for (int j = 1; j < mat[0].length; j++) {
            doSort(0, j, mat);
        }
        return mat;
    }

    private static void doSort(int xx, int yy, int[][] mat) {
        int x = xx, y = yy;
        List<Integer> list = new ArrayList<>();
        while (x < mat.length && y < mat[0].length) {
            list.add(mat[x++][y++]);
        }
        Collections.sort(list);
        int z = list.size() - 1;
        x--;
        y--;
        while (x >= xx && y >= yy) {
            mat[x--][y--] = list.get(z--);
        }
    }

}
