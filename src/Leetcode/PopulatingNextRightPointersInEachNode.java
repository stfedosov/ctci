package Leetcode;

import java.util.LinkedList;
import java.util.Queue;

public class PopulatingNextRightPointersInEachNode {

    public static void main(String[] args) {
        Node root1 = new Node();
        root1.val = 1;

        Node left2 = new Node();
        left2.val = 2;
        root1.left = left2;

        Node left2left = new Node();
        left2.left = left2left;
        left2left.val = 4;

        Node left2right = new Node();
        left2.right = left2right;
        left2right.val = 5;

        Node right3 = new Node();
        root1.right = right3;
        right3.val = 3;

        Node right3left = new Node();
        right3left.val = 6;
        right3.left = right3left;

        Node right3right = new Node();
        right3right.val = 7;
        right3.right = right3right;

        System.out.println(root1);

        connect(root1);
    }

    public static Node connect(Node root) {
        if (root == null) return null;
        Queue<Node> q = new LinkedList<>();
        q.offer(root);
        while (!q.isEmpty()) {
            int size = q.size();
            for (int i = 0; i < size; i++) {
                Node polled = q.poll();
                if (i < size - 1) {
                    polled.next = q.peek();
                }
                if (polled.left != null) {
                    q.offer(polled.left);
                }
                if (polled.right != null) {
                    q.offer(polled.right);
                }
            }
        }
        return root;
    }

    public Node connect2(Node root) {
        if (root == null) {
            return root;
        }

        // Start with the root node. There are no next pointers
        // that need to be set up on the first level
        Node leftmost = root;

        // Once we reach the final level, we are done
        while (leftmost.left != null) {
            // Iterate the "linked list" starting from the head
            // node and using the next pointers, establish the
            // corresponding links for the next level
            Node head = leftmost;

            while (head != null) {
                // CONNECTION 1
                head.left.next = head.right;

                // CONNECTION 2
                if (head.next != null) {
                    head.right.next = head.next.left;
                }

                // Progress along the list (nodes on the current level)
                head = head.next;
            }

            // Move onto the next level
            leftmost = leftmost.left;
        }

        return root;
    }

    static class Node {
        public int val;
        public Node left;
        public Node right;
        public Node next;

        public Node() {
        }

        @Override
        public String toString() {
            return "NodeCopy{" +
                    "val=" + val +
                    ", left=" + left +
                    ", right=" + right +
                    ", next=" + next +
                    '}';
        }

        public Node(int _val, Node _left, Node _right, Node _next) {
            val = _val;
            left = _left;
            right = _right;
            next = _next;
        }
    }

}
