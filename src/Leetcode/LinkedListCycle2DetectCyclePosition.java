package Leetcode;

import Leetcode.RotateList.ListNode;

/**
 * @author sfedosov on 1/24/19.
 */
public class LinkedListCycle2DetectCyclePosition {

    public static void main(String[] args) {
        ListNode root = new ListNode(3);
        ListNode listNode2 = new ListNode(2);
        root.next = listNode2;
        ListNode listNode0 = new ListNode(0);
        listNode2.next = listNode0;
        ListNode listNode4 = new ListNode(-4);
        listNode0.next = listNode4;
        listNode4.next = listNode2;
        System.out.println(detectCycle(root).val);

        // 3 -> 2 -> 0 -> -4 -\
        //      ^             |
        //      |-------------|
        // There is a cycle in the linked list, where tail connects to the second node.
    }

    public static ListNode detectCycle(ListNode head) {
        if (head == null || head.next == null) return null;
        if (head.next == head) return head;
        ListNode slow = head;
        ListNode fast = head;
        while (fast != null) {
            fast = fast.next;
            if (fast != null) {
                fast = fast.next;
                slow = slow.next;
            }
            if (slow == fast) break;
        }
        if (fast == null) return null;
        fast = head;
        while (fast != slow) {
            fast = fast.next;
            slow = slow.next;
        }
        return fast;
    }

}
