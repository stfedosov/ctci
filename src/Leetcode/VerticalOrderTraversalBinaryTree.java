package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class VerticalOrderTraversalBinaryTree {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(3);
        root.left = new TreeNode(9);
        TreeNode treeNode20 = new TreeNode(20);
        root.right = treeNode20;
        treeNode20.left = new TreeNode(15);
        treeNode20.right = new TreeNode(7);
        List<List<Integer>> expected = new ArrayList<>() {{
            add(Collections.singletonList(9));
            add(Arrays.asList(3, 15));
            add(Collections.singletonList(20));
            add(Collections.singletonList(7));
        }};
        //
        //       3
        //      / \
        //     9  20
        //        / \
        //       15  7
        //
        //  [9], [3, 15] [20] [7]
        assert verticalOrder(root).equals(expected);
    }

    // Time/Space: O(N)
    // case when we need to sort nodes only based on horizontal axe (1 dimension)
    // verticalTraversal
    public static List<List<Integer>> verticalOrder(TreeNode root) {
        Map<Integer,List<Integer>> map = new HashMap<>();
        Queue<Pair> q = new LinkedList<>();
        if (root != null) q.offer(new Pair(0, root));
        int min = 0, max = 0;
        while (!q.isEmpty()) {
            Pair pair = q.poll();
            map.computeIfAbsent(pair.col, x -> new ArrayList<>()).add(pair.node.val);
            min = Math.min(min, pair.col);
            max = Math.max(max, pair.col);

            if (pair.node.left != null) q.offer(new Pair(pair.col - 1, pair.node.left));
            if (pair.node.right != null) q.offer(new Pair(pair.col + 1, pair.node.right));
        }
        List<List<Integer>> ret = new ArrayList<>();
        for (int i = min; i <= max; i++) ret.add(map.get(i));
        return ret;
    }

    static class Pair {
        TreeNode node;
        int col;

        public Pair(int col, TreeNode node) {
            this.node = node;
            this.col = col;
        }
    }
}
