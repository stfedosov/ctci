package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author sfedosov on 4/10/20.
 */
public class Game24 {

    public static void main(String[] args) {
        assert judgePoint24(new int[]{4, 1, 8, 7});
        assert !judgePoint24(new int[]{1, 2, 1, 2});
    }

    public static boolean judgePoint24(int[] nums) {
        List<Double> doubles = new ArrayList<>();
        for (int x : nums) doubles.add((double) x);
        return dfs(doubles);
    }

    private static boolean dfs(List<Double> doubles) {
        if (doubles.size() == 1) {
            return Math.abs(doubles.get(0) - 24) <= 0.001;
        } else {
            for (int i = 1; i < doubles.size(); i++) {
                for (int j = 0; j < i; j++) {
                    double ii = doubles.get(i);
                    double jj = doubles.get(j);
                    List<Double> copy = new ArrayList<>(doubles);
                    copy.remove(i);
                    copy.remove(j);
                    List<Double> operations = Arrays.asList(ii + jj, ii - jj, jj - ii, ii * jj, ii / jj, jj / ii);
                    for (Double d : operations) {
                        copy.add(d);
                        if (dfs(copy)) return true;
                        copy.remove(copy.size() - 1);
                    }
                }
            }
            return false;
        }
    }

}
