package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LargestDivisibleSubset {

    public static void main(String[] args) {
        assert largestDivisibleSubset(new int[]{1, 2, 3}).containsAll(Arrays.asList(1, 2)) ||
                largestDivisibleSubset(new int[]{1, 2, 3}).containsAll(Arrays.asList(3, 1));
        assert largestDivisibleSubset(new int[]{1, 2, 4, 8}).containsAll(Arrays.asList(1, 2, 4, 8));
    }

    // Similar to LIS problem
    public static List<Integer> largestDivisibleSubset(int[] nums) {
        Arrays.sort(nums);
        int[] dp = new int[nums.length];
        int max = 1;
        Arrays.fill(dp, 1);
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < i; j++) {
                if (nums[i] % nums[j] == 0) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                    max = Math.max(max, dp[i]);
                }
            }
        }
        int prev = -1;
        List<Integer> list = new ArrayList<>();
        for (int i = dp.length - 1; i >= 0; i--) {
            if (dp[i] == max) {
                if (prev == -1 || prev % nums[i] == 0) {
                    list.add(nums[i]);
                    max--;
                    prev = nums[i];
                }
            }
        }
        return list;
    }

}
