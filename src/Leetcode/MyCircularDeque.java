package Leetcode;

/**
 * @author sfedosov on 4/1/20.
 */
public class MyCircularDeque {

    private int maxSize = 0;
    private int currentSize = 0;

    static class ListNode {
        int val;
        ListNode next;
        ListNode prev;

        public ListNode(int val) {
            this.val = val;
        }
    }

    private ListNode head;
    private ListNode tail;

    /**
     * Initialize your data structure here. Set the size of the deque to be k.
     */
    public MyCircularDeque(int k) {
        this.maxSize = k;
        head = null;
        tail = null;
    }

    /**
     * Adds an item at the front of Deque. Return true if the operation is successful.
     */
    public boolean insertFront(int value) {
        if (isFull()) return false;
        ListNode newHead = new ListNode(value);
        if (head == null) {
            head = newHead;
            tail = head;
        } else {
            tail.next = newHead;
            newHead.prev = tail;
            newHead.next = head;
            head.prev = newHead;
            head = newHead;
        }
        currentSize++;
        return true;
    }

    /**
     * Adds an item at the rear of Deque. Return true if the operation is successful.
     */
    public boolean insertLast(int value) {
        if (isFull()) return false;
        ListNode newTail = new ListNode(value);
        if (tail == null) {
            tail = newTail;
            head = tail;
        } else {
            tail.next = newTail;
            newTail.prev = tail;
            newTail.next = head;
            head.prev = newTail;
            tail = newTail;
        }
        currentSize++;
        return true;
    }

    /**
     * Deletes an item from the front of Deque. Return true if the operation is successful.
     */
    public boolean deleteFront() {
        if (isEmpty()) return false;
        currentSize--;
        if (currentSize == 0) {
            head = null;
            tail = null;
            return true;
        }
        head = head.next;
        tail.next = head;
        head.prev = tail;
        return true;
    }

    /**
     * Deletes an item from the rear of Deque. Return true if the operation is successful.
     */
    public boolean deleteLast() {
        if (isEmpty()) return false;
        currentSize--;
        if (currentSize == 0) {
            head = null;
            tail = null;
            return true;
        }
        tail = tail.prev;
        tail.next = head;
        head.prev = tail;
        return true;
    }

    /**
     * Get the front item from the deque.
     */
    public int getFront() {
        if (isEmpty()) return -1;
        return head.val;
    }

    /**
     * Get the last item from the deque.
     */
    public int getRear() {
        if (isEmpty()) return -1;
        return tail.val;
    }

    /**
     * Checks whether the circular deque is empty or not.
     */
    public boolean isEmpty() {
        return currentSize == 0;
    }

    /**
     * Checks whether the circular deque is full or not.
     */
    public boolean isFull() {
        return currentSize == maxSize;
    }

    public static void main(String[] args) {
        MyCircularDeque obj = new MyCircularDeque(5);
        assert obj.insertFront(7);
        assert obj.insertLast(0);
        assert obj.getFront() == 7;
        assert obj.insertLast(3);
        assert obj.getFront() == 7;
        assert obj.insertFront(9);
        assert obj.getRear() == 3;
        assert obj.getFront() == 9;
        assert obj.getFront() == 9;
        assert obj.deleteLast();
        assert obj.getRear() == 0;

        obj = new MyCircularDeque(97);
        assert obj.insertFront(35);
        assert obj.insertFront(10);
        assert obj.getFront() == 10;
        assert obj.insertLast(17);
        assert obj.insertLast(14);
        assert obj.getFront() == 10;
        assert !obj.isFull();
        assert !obj.isEmpty();
        assert obj.getFront() == 10;
        assert !obj.isFull();
        assert obj.deleteLast();
        assert obj.insertFront(6);
        assert obj.deleteLast();
        assert obj.insertLast(39);
        assert obj.getRear() == 39;
        assert obj.deleteLast();
        assert obj.getFront() == 6;
        assert obj.deleteFront();
        assert obj.getFront() == 10;
        assert obj.insertLast(8);
        assert obj.getFront() == 10;
        assert obj.insertFront(31);
        assert obj.getRear() == 8;
        assert obj.deleteFront();
        assert !obj.isFull();
        assert obj.getRear() == 8;
        assert obj.insertFront(91);
        assert obj.insertFront(0);
        assert obj.getRear() == 8;
        assert obj.insertFront(57);
        assert obj.getRear() == 8;
        assert obj.getRear() == 8;
        assert obj.getRear() == 8;
        assert !obj.isFull();
        assert obj.insertFront(56);
        assert obj.insertFront(28);
        assert obj.getRear() == 8;
        assert !obj.isFull();
        assert obj.deleteLast();
        assert obj.getFront() == 28;
        assert obj.getRear() == 35;
        assert !obj.isFull();
        assert obj.deleteLast();
        assert obj.insertFront(43);
        assert obj.deleteLast();
        assert obj.getFront() == 43;
        assert obj.deleteLast();
        System.out.println(obj.getRear());
        assert obj.getRear() == 0;
    }

}

/**
 * Your MyCircularDeque object will be instantiated and called as such:
 * MyCircularDeque obj = new MyCircularDeque(k);
 * boolean param_1 = obj.insertFront(value);
 * boolean param_2 = obj.insertLast(value);
 * boolean param_3 = obj.deleteFront();
 * boolean param_4 = obj.deleteLast();
 * int param_5 = obj.getFront();
 * int param_6 = obj.getRear();
 * boolean param_7 = obj.isEmpty();
 * boolean param_8 = obj.isFull();
 */