package Leetcode;

/**
 * @author sfedosov on 12/28/17.
 */
public class NumberOf1Bits {

    public static void main(String[] args) {
        assert hammingWeight(11) == 3;
    }

    public static int hammingWeight(int n) {
        int count = 0;
        while (n != 0) {
            count = count + (n & 1);
            n = n >>> 1;
        }
        return count;
    }
}
