package Leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 11/2/18.
 */
public class MaxProfitWithCoolDown {

    public static void main(String[] args) {
        assert maxProfit(new int[]{1, 2, 3, 0, 2}) == 3;
    }

    public static int maxProfit(int[] prices) {
        return maxProfit(prices, 0, new HashMap<>());
    }

    private static int maxProfit(int[] prices, int idx, Map<Integer, Integer> cache) {
        if (cache.containsKey(idx)) return cache.get(idx);
        if (idx >= prices.length) return 0;
        int max = 0;
        for (int i = idx + 1; i < prices.length; i++) {
            // sell today, but will become available for next buy only after at least one day
            int profit1 = prices[i] > prices[idx] ? (prices[i] - prices[idx]) + maxProfit(prices, i + 2, cache) : 0;
            // cooldown - skip today's buy
            int profit2 = maxProfit(prices, i, cache);
            max = Math.max(max, Math.max(profit1, profit2));
        }
        cache.put(idx, max);
        return max;
    }

}
