package Leetcode;

/**
 * @author sfedosov on 4/16/18.
 */
public class BuildBSTFromSortedArray {

    public static void main(String[] args) {
        TreeNode root = buildBST(new int[]{1, 2, 3, 4, 5, 6, 7});
        System.out.println(root);
    }

    private static TreeNode buildBST(int[] ints) {
        return buildRecursively(ints, 0, ints.length - 1);
    }

    private static TreeNode buildRecursively(int[] ints, int i, int j) {
        if (i > j) {
            return null;
        }
        int mid = (i + j) / 2;
        TreeNode root = new TreeNode(ints[mid]);
        root.left = buildRecursively(ints, i, mid - 1);
        root.right = buildRecursively(ints, mid + 1, j);
        return root;
    }

}
