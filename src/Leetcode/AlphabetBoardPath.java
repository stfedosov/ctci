package Leetcode;

public class AlphabetBoardPath {

    public static void main(String[] args) {
        assert alphabetBoardPath("leet").equals("DDR!UURRR!!DDD!");
        assert alphabetBoardPath("code").equals("RR!DDRR!UUL!R!");
        assert alphabetBoardPath("zdz").equals("DDDDD!UUUUURRR!DDDDLLLD!");
    }

    public static String alphabetBoardPath(String target) {
        int[][] map = new int[26][];
        int x = 0;
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 5; j++) {
                if (x >= 26) break;
                map[alphabet.charAt(x++) - 'a'] = new int[]{i, j};
            }
        }
        int[] prev = new int[]{0, 0};
        StringBuilder sb = new StringBuilder();
        for (char c : target.toCharArray()) {
            int[] next = map[c - 'a'];
            int diffX = next[0] - prev[0];
            int diffY = next[1] - prev[1];
            char toAppX = diffX > 0 ? 'D' : 'U';
            char toAppY = diffY > 0 ? 'R' : 'L';
            diffX = Math.abs(diffX);
            diffY = Math.abs(diffY);
            int remainX = 0;
            if (prev[0] + diffX == 5 && prev[1] + diffY != 0) {
                remainX++;
                diffX--;
            }
            while (diffX-- > 0) sb.append(toAppX);
            while (diffY-- > 0) sb.append(toAppY);
            while (remainX-- > 0) sb.append(toAppX);
            sb.append('!');
            prev = next;
        }
        return sb.toString();
    }
}
