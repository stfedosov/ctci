package Leetcode;

import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Queue;

public class KClosestToOrigin {

    public static void main(String[] args) {
        System.out.println(Arrays.deepToString(kClosest(new int[][]{{3, 3}, {5, -1}, {-2, 4}}, 2)));
    }

    public static int[][] kClosest(int[][] points, int k) {
        Queue<Point> pq = new PriorityQueue<>((a, b) -> b.dist - a.dist);
        for (int[] point : points) {
            Point p = new Point(point[0], point[1]);
            pq.add(p);
            if (pq.size() > k) {
                pq.poll();
            }
        }
        int[][] closest = new int[k][2];
        int x = 0;
        while (!pq.isEmpty()) {
            Point p = pq.poll();
            closest[x][0] = p.x;
            closest[x][1] = p.y;
            x++;
        }
        return closest;
    }

    static class Point {
        int x, y, dist;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
            dist = x * x + y * y;
        }
    }

    // Time complexity: O(n) best case since we're halving (roughly) on each iteration, O(n^2) worst case
    // Space complexity: O(k + log(n)) due to array creation (copy of range) and recursion, in the worst case O(k + n)
    public static int[][] kClosestQuickSelect(int[][] points, int k) {
        if (k == points.length) return points;
        quickSelect(points, 0, points.length - 1, k - 1);
        return Arrays.copyOfRange(points, 0, k);
    }

    public static void swap(int[][] points, int i, int j) {
        int[] temp = points[i];
        points[i] = points[j];
        points[j] = temp;
    }

    public static int partition(int[][] points, int start, int end) {
        int pIndex = start;
        for (int i = start; i < end; i++) {
            if (dist(points[i]) < dist(points[end])) { // points[end] is the pivot point here
                swap(points, i, pIndex);
                pIndex++;
            }
        }
        swap(points, pIndex, end);
        return pIndex;
    }

    public static void quickSelect(int[][] points, int low, int high, int k) {
        int pivot = partition(points, low, high);
        if (pivot < k) {
            quickSelect(points, pivot + 1, high, k);
        } else if (pivot > k) {
            quickSelect(points, low, pivot - 1, k);
        }
    }

    public static int dist(int[] arr) {
        return arr[0] * arr[0] + arr[1] * arr[1];
    }


}
