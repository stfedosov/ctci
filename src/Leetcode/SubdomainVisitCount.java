package Leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sfedosov on 4/3/18.
 */
public class SubdomainVisitCount {

    public static void main(String[] args) {
        System.out.println(subdomainVisits(new String[]{"900 google.mail.com", "50 yahoo.com", "1 intel.mail.com", "5 wiki.org"}));
    }

    public static List<String> subdomainVisits(String[] cpdomains) {
        Map<String, Integer> map = new HashMap<>();
        for (String s : cpdomains) {
            String[] split = s.split(" ");
            String[] splitByDot = split[1].split("\\.");
            for (int i = 0; i < splitByDot.length; i++) {
                StringBuilder stringBuilder = new StringBuilder();
                for (int j = i; j < splitByDot.length; j++) {
                    if (j == splitByDot.length - 1) {
                        stringBuilder.append(splitByDot[j]);
                    } else {
                        stringBuilder.append(splitByDot[j]).append(".");
                    }
                }
                String key = stringBuilder.toString();
                Integer integer = Integer.valueOf(split[0]);
                Integer integerOld = map.get(key);
                map.put(key, integerOld == null ? integer : integerOld + integer);
            }
        }
        List<String> result = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            result.add(entry.getValue() + " " + entry.getKey());
        }
        return result;
    }

}
