package Leetcode;

/**
 * @author sfedosov on 1/18/19.
 */
public class CoinChange2 {

    public static void main(String[] args) {
        assert coinChange(new int[]{1, 2, 5}, 11) == 3;
        assert coinChange(new int[]{2}, 3) == -1;
    }

    public static int coinChange(int[] coins, int amount) {
        if (amount < 1) return 0;
        int[] cache = new int[amount + 1];
        return coinChangeBackTracking(coins, cache, amount);
    }

    private static int coinChangeBackTracking(int[] coins, int[] cache, int amount) {
        if (amount == 0) return 0;
        if (amount < 0) return -1;
        if (cache[amount] != 0) return cache[amount];
        int min = Integer.MAX_VALUE;
        for (int coin : coins) {
            int result = coinChangeBackTracking(coins, cache, amount - coin);
            if (result >= 0) {
                min = Math.min(min, result + 1);
            }
        }
        cache[amount] = (min == Integer.MAX_VALUE) ? -1 : min;
        return cache[amount];
    }

    public int coinChangeNonCached(int[] coins, int amount) {
        if (amount < 1) return 0;
        return coinChangeBackTrackingNonCached(coins, amount);
    }

    private int coinChangeBackTrackingNonCached(int[] coins, int amount) {
        if (amount == 0) return 0;
        if (amount < 0) return -1;
        int min = Integer.MAX_VALUE;
        for (int coin : coins) {
            int result = coinChangeBackTrackingNonCached(coins, amount - coin);
            if (result >= 0) {
                min = Math.min(min, result + 1);
            }
        }
        return (min == Integer.MAX_VALUE) ? -1 : min;
    }

    public int change(int amount, int[] coins) {
        int[][] dp = new int[coins.length+1][amount+1];
        dp[0][0] = 1;

        for (int i = 1; i <= coins.length; i++) {

    /*
      Set the subproblem for the amount of 0 to 1 when
      solving this row
    */
            dp[i][0] = 1;

            for (int j = 1; j <= amount; j++) {

                int currentCoinValue = coins[i-1];

      /*
        dp[i][j] will be the sum of the ways to make change not considering
        this coin (dp[i-1][j]) and the ways to make change considering this
        coin (dp[i][j] - currentCoinValue] ONLY if currentCoinValue <= j, otherwise
        this coin can not contribute to the total # of ways to make change at this
        sub problem target amount)
      */
                int withoutThisCoin = dp[i-1][j];
                int withThisCoin = currentCoinValue <= j ? dp[i][j - currentCoinValue] : 0;

                dp[i][j] = withoutThisCoin + withThisCoin;
            }
        }

  /*
    The answer considering ALL coins for the FULL amount is what
    we want to return as the answer
  */
        return dp[coins.length][amount];
    }

}
