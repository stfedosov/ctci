package Leetcode;

import Leetcode.RotateList.ListNode;

public class PlusOneLinkedList {

    // Input: head = [1,2,3]
    // Output: [1,2,4]

    public static void main(String[] args) {
        ListNode root = new ListNode(1);
        ListNode next2 = new ListNode(2);
        root.next = next2;
        next2.next = new ListNode(3);
        System.out.println(plusOne(root));
    }

    public static ListNode plusOne(ListNode head) {
        ListNode newList = reverse(head);
        int remainder = 1;
        ListNode it = newList;
        ListNode dummy = new ListNode(0);
        ListNode dummyIt = dummy;
        while (it != null) {
            remainder += it.val;
            dummyIt.next = new ListNode(remainder % 10);
            remainder /= 10;
            dummyIt = dummyIt.next;
            it = it.next;
        }
        if (remainder != 0) {
            dummyIt.next = new ListNode(remainder % 10);
        }
        return reverse(dummy.next);
    }

    private static ListNode reverse(ListNode head) {
        ListNode prev = null;
        ListNode current = head;
        while (current != null) {
            ListNode next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        return prev;
    }

}
