package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 8/26/19.
 */
public class FloodFill {

    public static void main(String[] args) {
        System.out.println(Arrays.deepToString(floodFill(new int[][]{
                {1, 1, 1},
                {1, 1, 0},
                {1, 0, 1}}, 1, 1, 2)));
        System.out.println(Arrays.deepToString(floodFill(new int[][]{
                {1, 1, 1},
                {1, 1, 0},
                {1, 0, 1}}, 1, 1, 2)));
    }

    public static int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        if (image[sr][sc] != newColor) {
            dfs(image, sr, sc, newColor, image[sr][sc]);
        }
        return image;
    }

    private static void dfs(int[][] image, int i, int j, int newColor, int oldColor) {
        if (i < 0 || j < 0 || i >= image.length || j >= image[i].length || image[i][j] != oldColor) {
            return;
        }
        image[i][j] = newColor;
        dfs(image, i + 1, j, newColor, oldColor);
        dfs(image, i - 1, j, newColor, oldColor);
        dfs(image, i, j + 1, newColor, oldColor);
        dfs(image, i, j - 1, newColor, oldColor);
    }

}
