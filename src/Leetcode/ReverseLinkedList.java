package Leetcode;

/**
 * @author sfedosov on 1/21/18.
 */

import static Leetcode.RotateList.ListNode;

public class ReverseLinkedList {

    public static void main(String[] args) {
        RotateList.ListNode listNode = new RotateList.ListNode(1);
        RotateList.ListNode listNode2 = new RotateList.ListNode(2);
        RotateList.ListNode listNode3 = new RotateList.ListNode(3);
        RotateList.ListNode listNode4 = new RotateList.ListNode(4);
        listNode4.next = new RotateList.ListNode(5);
        listNode3.next = listNode4;
        listNode2.next = listNode3;
        listNode.next = listNode2;
        System.out.println(listNode);
        System.out.println(reverseList(listNode));
    }

    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode(int x) { val = x; }
     * }
     */
    public static ListNode reverseList(ListNode head) {
        ListNode prev = null;
        ListNode current = head;
        ListNode next;
        while (current != null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        head = prev;
        return head;
    }


}
