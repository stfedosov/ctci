package Leetcode;

import java.util.ArrayList;
import java.util.List;

public class Permute {

    private static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    private static List<List<Integer>> permute(int[] arr, int i, List<List<Integer>> res) {
        if (i == arr.length) {
            List<Integer> e = new ArrayList<>();
            for (int a : arr) {
                e.add(a);
            }
            res.add(e);
            return res;
        }
        for (int j = i; j < arr.length; j++) {
            swap(arr, i, j);
            permute(arr, i + 1, res);
            swap(arr, i, j);
        }
        return res;
    }

    public static List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        return permute(nums, 0, result);
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3};
        System.out.println(permute(arr));
    }

}
