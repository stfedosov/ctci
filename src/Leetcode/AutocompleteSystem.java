package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @author sfedosov on 8/30/19.
 */
class AutocompleteSystem {

    public static void main(String[] args) {
        AutocompleteSystem autocomplete = new AutocompleteSystem(
                new String[]{"i love you", "island", "ironman", "i love leetcode"},
                new int[]{5, 3, 2, 2});

        assert autocomplete.input('i').equals(Arrays.asList("i love you", "island", "i love leetcode"));
        assert autocomplete.input(' ').equals(Arrays.asList("i love you", "i love leetcode"));
        assert autocomplete.input('a').isEmpty();
        assert autocomplete.input('#').isEmpty();

        assert autocomplete.input('i').equals(Arrays.asList("i love you", "island", "i love leetcode"));
        assert autocomplete.input(' ').equals(Arrays.asList("i love you", "i love leetcode", "i a"));
        assert autocomplete.input('a').equals(Collections.singletonList("i a"));
        assert autocomplete.input('#').isEmpty();

        assert autocomplete.input('i').equals(Arrays.asList("i love you", "island", "i a"));
        assert autocomplete.input(' ').equals(Arrays.asList("i love you", "i a", "i love leetcode"));
        assert autocomplete.input('a').equals(Collections.singletonList("i a"));
        assert autocomplete.input('#').isEmpty();

        autocomplete = new AutocompleteSystem(new String[]{"abc", "abbc", "a"}, new int[]{3, 3, 3});
        assert autocomplete.input('b').isEmpty();
        assert autocomplete.input('c').isEmpty();
        assert autocomplete.input('#').isEmpty();
        assert autocomplete.input('b').equals(Collections.singletonList("bc"));
        assert autocomplete.input('c').equals(Collections.singletonList("bc"));

        assert autocomplete.input('#').isEmpty();
        assert autocomplete.input('a').equals(Arrays.asList("a", "abbc", "abc"));
        assert autocomplete.input('b').equals(Arrays.asList("abbc", "abc"));
        assert autocomplete.input('c').equals(Collections.singletonList("abc"));
        assert autocomplete.input('#').isEmpty();

        assert autocomplete.input('a').equals(Arrays.asList("abc", "a", "abbc"));
        assert autocomplete.input('b').equals(Arrays.asList("abc", "abbc"));
        assert autocomplete.input('c').equals(Collections.singletonList("abc"));
        assert autocomplete.input('#').isEmpty();
    }

    private TrieNode root = new TrieNode('*');
    private Queue<Pair> q = new PriorityQueue<>((o1, o2) -> {
        int compare = Integer.compare(o1.freq, o2.freq);
        if (compare == 0) return o2.sentence.compareTo(o1.sentence);
        return compare;
    });
    private String phrase = "";

    public AutocompleteSystem(String[] sentences, int[] times) {
        insert(sentences, times);
    }

    private void insert(String[] sentences, int[] times) {
        for (int i = 0; i < times.length; i++) {
            TrieNode tmp = root;
            for (char c : sentences[i].toCharArray()) {
                if (!tmp.child.containsKey(c)) {
                    tmp.child.put(c, new TrieNode(c));
                }
                tmp = tmp.child.get(c);
            }
            tmp.freq += times[i];
            tmp.isWord = true;
        }
    }

    public List<String> input(char c) {
        if (c == '#') {
            insert(new String[]{phrase}, new int[]{1});
            phrase = "";
            return Collections.emptyList();
        }
        phrase += c;
        return read(phrase, q);
    }

    private List<String> read(String phrase, Queue<Pair> q) {
        search(root.child.get(phrase.charAt(0)), phrase.toCharArray(), 0, "", q);
        List<String> result = new ArrayList<>();
        while (!q.isEmpty()) {
            result.add(0, q.poll().sentence);
        }
        return result;
    }

    private void search(TrieNode trieNode, char[] chars, int i, String current, Queue<Pair> q) {
        if (trieNode == null) {
            return;
        }
        current += trieNode.c;
        if (trieNode.isWord && i >= chars.length - 1) {
            q.offer(new Pair(current, trieNode.freq));
            if (q.size() > 3) {
                q.poll();
            }
        }
        if (i + 1 < chars.length) {
            search(trieNode.child.get(chars[i + 1]), chars, i + 1, current, q);
        } else {
            for (Map.Entry<Character, TrieNode> entry : trieNode.child.entrySet()) {
                search(entry.getValue(), chars, i, current, q);
            }
        }
    }
}

class Pair {
    String sentence;
    int freq;

    Pair(String sentence, int freq) {
        this.sentence = sentence;
        this.freq = freq;
    }
}

class TrieNode {
    Map<Character, TrieNode> child = new HashMap<>();
    char c;
    boolean isWord;
    int freq = 0;

    TrieNode(char c) {
        this.c = c;
    }
}