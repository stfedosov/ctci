package Leetcode;

/**
 * @author sfedosov on 8/9/20.
 */
public class MakeTheStringGreat {

    public static void main(String[] args) {
        assert makeGood("abBAcC").equals("");
        assert makeGood("S").equals("S");
        assert makeGood("leEeetcode").equals("leetcode");
    }

    public static String makeGood(String s) {
        int i = 0, j = 1;
        while (j < s.length()) {
            char a = s.charAt(i), b = s.charAt(j);
            if (a != b && Character.toLowerCase(a) == Character.toLowerCase(b)) {
                s = s.substring(0, i) + s.substring(j + 1);
                if (i > 0) i--;
                if (j > 1) j--;
            } else {
                i++;
                j++;
            }
        }
        return s;
    }

}
