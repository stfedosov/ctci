package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

/**
 * @author sfedosov on 8/9/19.
 */
public class WordLadder2 {

    public static void main(String[] args) {
        System.out.println(findLadders1("hit", "cog", Arrays.asList("hot", "dot", "dog", "lot", "log", "cog")));
        System.out.println(findLadders1("red", "tax", Arrays.asList("ted", "tex", "red", "tax", "tad", "den", "rex", "pee")));
    }

    // Time/Space Complexity: O(M^2 * N), where M is the length of words and N is the total number of words in the input word list
    public static List<List<String>> findLadders1(String beginWord, String endWord, List<String> wordList) {
        Map<String, List<String>> combinations = generateCombinations(wordList);
        Queue<Pair> queue = new LinkedList<>();
        List<String> list = new ArrayList<>();
        list.add(beginWord);
        queue.offer(new Pair(beginWord, list));
        Set<List<String>> result2 = new HashSet<>();
        Set<String> seen = new HashSet<>();
        seen.add(beginWord);
        while (!queue.isEmpty()) {
            Pair polled = queue.poll();
            String startWord = polled.word;
            List<String> path = polled.path;
            for (int i = 0; i < startWord.length(); i++) {
                String newWord = startWord.substring(0, i) + '*' + startWord.substring(i + 1);
                for (String combination : combinations.getOrDefault(newWord, new ArrayList<>())) {
                    if (combination.equals(endWord)) {
                        List<String> newPath = new ArrayList<>(path);
                        newPath.add(endWord);
                        result2.add(newPath);
                    } else if (!seen.contains(combination) && !path.contains(combination)) {
                        seen.add(combination);
                        List<String> newPath = new ArrayList<>(path);
                        newPath.add(combination);
                        queue.offer(new Pair(combination, newPath));
                    }
                }
            }
        }
        return new ArrayList<>(result2);
    }

    private static Map<String, List<String>> generateCombinations(List<String> wordList) {
        Map<String, List<String>> result = new HashMap<>();
        for (String word : wordList) {
            for (int i = 0; i < word.length(); i++) {
                String newWord = word.substring(0, i) + '*' + word.substring(i + 1);
                result.computeIfAbsent(newWord, x -> new ArrayList<>()).add(word);
            }
        }
        return result;
    }

    static class Pair {
        String word;
        List<String> path;

        Pair(String word, List<String> path) {
            this.word = word;
            this.path = path;
        }
    }

    // -------------------------


    public static List<List<String>> findLadders3(String start, String end, List<String> wordList) {
        HashSet<String> dict = new HashSet<String>(wordList);
        List<List<String>> res = new ArrayList<List<String>>();
        HashMap<String, ArrayList<String>> nodeNeighbors = new HashMap<String, ArrayList<String>>();// Neighbors for every node
        HashMap<String, Integer> distance = new HashMap<String, Integer>();// Distance of every node from the start node
        ArrayList<String> solution = new ArrayList<String>();

        dict.add(start);
        bfs(start, end, dict, nodeNeighbors, distance);
        dfs(start, end, dict, nodeNeighbors, distance, solution, res);
        return res;
    }

    // BFS: Trace every node's distance from the start node (level by level).
    private static void bfs(String start, String end, Set<String> dict, HashMap<String, ArrayList<String>> nodeNeighbors, HashMap<String, Integer> distance) {
        for (String str : dict)
            nodeNeighbors.put(str, new ArrayList<String>());

        Queue<String> queue = new LinkedList<String>();
        queue.offer(start);
        distance.put(start, 0);

        while (!queue.isEmpty()) {
            int count = queue.size();
            boolean foundEnd = false;
            for (int i = 0; i < count; i++) {
                String cur = queue.poll();
                int curDistance = distance.get(cur);
                ArrayList<String> neighbors = getNeighbors(cur, dict);

                for (String neighbor : neighbors) {
                    nodeNeighbors.get(cur).add(neighbor);
                    if (!distance.containsKey(neighbor)) {// Check if visited
                        distance.put(neighbor, curDistance + 1);
                        if (end.equals(neighbor))// Found the shortest path
                            foundEnd = true;
                        else
                            queue.offer(neighbor);
                    }
                }
            }

            if (foundEnd)
                break;
        }
    }

    // Find all next level nodes.
    private static ArrayList<String> getNeighbors(String node, Set<String> dict) {
        ArrayList<String> res = new ArrayList<String>();
        char chs[] = node.toCharArray();

        for (char ch = 'a'; ch <= 'z'; ch++) {
            for (int i = 0; i < chs.length; i++) {
                if (chs[i] == ch) continue;
                char old_ch = chs[i];
                chs[i] = ch;
                if (dict.contains(String.valueOf(chs))) {
                    res.add(String.valueOf(chs));
                }
                chs[i] = old_ch;
            }

        }
        return res;
    }

    // DFS: output all paths with the shortest distance.
    private static void dfs(String cur, String end, Set<String> dict, HashMap<String, ArrayList<String>> nodeNeighbors, HashMap<String, Integer> distance, ArrayList<String> solution, List<List<String>> res) {
        solution.add(cur);
        if (end.equals(cur)) {
            res.add(new ArrayList<String>(solution));
        } else {
            for (String next : nodeNeighbors.get(cur)) {
                if (distance.get(next) == distance.get(cur) + 1) {
                    dfs(next, end, dict, nodeNeighbors, distance, solution, res);
                }
            }
        }
        solution.remove(solution.size() - 1);
    }


}
