package Leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WordPattern {

    public static void main(String[] args) {
        assert wordPatternBruteForce("abba", "dog cat cat dog");
        assert wordPatternOptimized("abba", "dog cat cat dog");
        assert !wordPatternBruteForce("abba", "dog cat cat fish");
        assert !wordPatternOptimized("abba", "dog cat cat fish");
    }

    public static boolean wordPatternOptimized(String pattern, String str) {
        String[] words = str.split(" ");
        if (words.length != pattern.length()) {
            return false;
        }
        Map index = new HashMap();
        for (int i = 0; i < words.length; ++i) {
            if (index.put(pattern.charAt(i), i) != index.put(words[i], i)) {
                return false;
            }
        }
        return true;
    }

    public static boolean wordPatternBruteForce(String pattern, String str) {
        if (pattern.isEmpty()) return false;
        Map<Character, List<Integer>> map = new HashMap<>();
        int i = 0;
        for (char c : pattern.toCharArray()) {
            map.putIfAbsent(c, new ArrayList<>());
            map.get(c).add(i++);
        }
        i = 0;
        Map<String, List<Integer>> map2 = new HashMap<>();
        for (String s : str.split(" ")) {
            map2.putIfAbsent(s, new ArrayList<>());
            map2.get(s).add(i++);
        }
        return map2.values().containsAll(map.values());
    }

}
