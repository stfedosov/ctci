package Leetcode;

public class HouseRobber3 {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(3);
        TreeNode left = new TreeNode(2);
        root.left = left;
        left.right = new TreeNode(3);
        TreeNode right = new TreeNode(3);
        right.right = new TreeNode(1);
        root.right = right;
        assert rob(root) == 7;
        //     3
        //    / \
        //   2   3
        //    \   \
        //     3   1

        root = new TreeNode(2);
        root.right = new TreeNode(3);
        TreeNode new_left = new TreeNode(1);
        root.left = new_left;
        new_left.right = new TreeNode(4);
        //   2
        //  / \
        // 1   3
        //  \
        //   4
        assert rob(root) == 7;
    }

    public static int rob(TreeNode root) {
        if (root == null) return 0;
        int val = root.val;
        if (root.left != null) {
            val += rob(root.left.left) + rob(root.left.right);
        }
        if (root.right != null) {
            val += rob(root.right.left) + rob(root.right.right);
        }
        return Math.max(val, rob(root.left) + rob(root.right));
    }
}
