package Leetcode;

/**
 * @author sfedosov on 7/23/19.
 */
public class SecondLargestElementBST {



    public static int findSecondLargest(TreeNode rootNode) {
        if (rootNode == null || (rootNode.left == null
                && rootNode.right == null)) {
            throw new IllegalArgumentException("Tree must have at least 2 nodes");
        }

        // case: we're currently at largest, and largest has a left subtree,
        // so 2nd largest is largest in said subtree
        if (rootNode.left != null && rootNode.right == null) {
            return findSecondLargest(rootNode.left);
        }

        // case: we're at parent of largest, and largest has no left subtree,
        // so 2nd largest must be current node
        if (rootNode.right.left == null && rootNode.right.right == null) {
            return rootNode.val;
        }

        // otherwise: step right
        return findSecondLargest(rootNode.right);
    }

}
