package Leetcode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author sfedosov on 9/24/19.
 */
public class SnakeGame {

    public static void main(String[] args) {
        SnakeGame obj = new SnakeGame(3, 2, new int[][]{{1, 2}, {0, 1}});
        assert obj.move("R") == 0;
        assert obj.move("D") == 0;
        assert obj.move("R") == 1;
        assert obj.move("U") == 1;
        assert obj.move("L") == 2;
        assert obj.move("U") == -1;

        obj = new SnakeGame(3, 3, new int[][]{{2, 0}, {0, 0}, {0, 2}, {0, 1}, {2, 2}, {0, 1}});
        assert obj.move("D") == 0;
        assert obj.move("D") == 1;
        assert obj.move("R") == 1;
        assert obj.move("U") == 1;
        assert obj.move("U") == 1;
        assert obj.move("L") == 2;
        assert obj.move("D") == 2;
        assert obj.move("R") == 2;
        assert obj.move("R") == 2;
        assert obj.move("U") == 3;
        assert obj.move("L") == 4;
        assert obj.move("L") == 4;
        assert obj.move("D") == 4;
        assert obj.move("R") == 4;
        assert obj.move("U") == -1;

        obj = new SnakeGame(3, 3, new int[][]{{2, 0}, {0, 0}, {0, 2}, {2, 2}});
        assert obj.move("D") == 0;
        assert obj.move("D") == 1;
        assert obj.move("R") == 1;
        assert obj.move("U") == 1;
        assert obj.move("U") == 1;
        assert obj.move("L") == 2;
        assert obj.move("D") == 2;
        assert obj.move("R") == 2;
        assert obj.move("R") == 2;
        assert obj.move("U") == 3;
        assert obj.move("L") == 3;
        assert obj.move("D") == 3;
    }

    private int width, height, length = 0;
    private Queue<int[]> foodPoints;
    private ListNode head;

    /**
     * Initialize your data structure here.
     *
     * @param width  - screen width
     * @param height - screen height
     * @param food   - A list of food positions
     *               E.g food = [[1,1], [1,0]] means the first food is positioned at [1,1], the second is at [1,0].
     */
    public SnakeGame(int width, int height, int[][] food) {
        this.width = width;
        this.height = height;
        this.foodPoints = new LinkedList<>();
        for (int[] xy : food) {
            this.foodPoints.offer(xy);
        }
        head = new ListNode(0, 0);
    }

    /**
     * Moves the snake.
     *
     * @param direction - 'U' = Up, 'L' = Left, 'R' = Right, 'D' = Down
     * @return The game's score after the move. Return -1 if game over.
     * Game over when snake crosses the screen boundary or bites its body.
     */
    public int move(String direction) {
        int x = head.x, y = head.y;
        switch (direction) {
            case "U":
                x -= 1;
                break;
            case "L":
                y -= 1;
                break;
            case "R":
                y += 1;
                break;
            case "D":
                x += 1;
                break;
        }
        if (invalidPoint(x, y)) return -1;
        if (!foodPoints.isEmpty() && x == foodPoints.peek()[0] && y == foodPoints.peek()[1]) {
            ListNode newHead = new ListNode(x, y);
            newHead.next = head;
            head = newHead;
            foodPoints.poll();
            length++;
        } else {
            moveSnake(x, y);
        }
        return length;
    }

    private void moveSnake(int x, int y) {
        ListNode node = head;
        int prev_x = node.x, prev_y = node.y;
        node.x = x;
        node.y = y;
        node = node.next;
        while (node != null) {
            int tmp_x = node.x, tmp_y = node.y;
            node.x = prev_x;
            node.y = prev_y;
            prev_x = tmp_x;
            prev_y = tmp_y;
            node = node.next;
        }
    }

    private boolean invalidPoint(int x, int y) {
        // snake can cross itself
        ListNode node = head;
        while (node != null) {
            if (node.x == x && node.y == y && node.next != null) return true;
            node = node.next;
        }
        // or we could be out of bound
        return x < 0 || y < 0 || x >= height || y >= width;
    }
}

class ListNode {
    int x, y;
    ListNode next;

    ListNode(int x, int y) {
        this.x = x;
        this.y = y;
    }
}