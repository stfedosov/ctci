package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author sfedosov on 12/29/18.
 */
public class RestoreIPAddresses {

    public static void main(String[] args) {
        assert restoreIpAddresses("25525511135").equals(new ArrayList<>(Arrays.asList("255.255.11.135", "255.255.111.35")));
        assert restoreIpAddresses("0000").equals(new ArrayList<>(Collections.singletonList("0.0.0.0")));
    }

    public static List<String> restoreIpAddresses(String s) {
        List<String> result = new ArrayList<>();
        int[] ip = new int[4];
        Arrays.fill(ip, -1);
        helper(result, s, 0, ip, 0);
        return result;
    }

    private static void helper(List<String> acc, String s, int buildIndex, int[] path, int segment) {
        if (segment == 4 && buildIndex == s.length()) {
            acc.add(path[0] + "." + path[1] + "." + path[2] + "." + path[3]);
            return;
        } else if (segment == 4 || buildIndex == s.length()) {
            return;
        }
        for (int len = 1; len <= 3 && buildIndex + len <= s.length(); len++) {
            String snapshot = s.substring(buildIndex, buildIndex + len);
            int intValue = Integer.parseInt(snapshot);
            if (intValue > 255 || len >= 2 && s.charAt(buildIndex) == '0') {
                break;
            }
            path[segment] = intValue;
            helper(acc, s, buildIndex + len, path, segment + 1);
            path[segment] = -1;
        }
    }

}
