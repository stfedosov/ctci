package Leetcode;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @author sfedosov on 7/23/19.
 */
public class kthLargestElement {

    public int findKthLargestPQ(int[] nums, int k) {
        Queue<Integer> queue = new PriorityQueue<>();
        for (int num : nums) {
            queue.offer(num);
            if (queue.size() > k) {
                queue.poll();
            }
        }
        return queue.peek();
    }

    public int findKthLargest(int[] arr, int k) {
        int n = arr.length;
        int start = 0;
        int end = n - 1;
        while (start <= end) {
            int pivotIndex = partition(arr, start, end);
            if (pivotIndex == n - k) {
                return arr[pivotIndex];
            } else if (pivotIndex > n - k) {
                end = pivotIndex - 1;
            } else {
                start = pivotIndex + 1;
            }
        }
        return -1;
    }

    /*
      [ items < than the pivot ... pivotItem ... items > than the pivot]
    */
    private int partition(int[] arr, int start, int end) {
        int pivotValue = arr[end];
        int pivotIndex = start;
        for (int i = start; i < end; i++) {
            if (arr[i] <= pivotValue) {
                swap(arr, i, pivotIndex);
                pivotIndex++;
            }
        }
        // we've already moved all elements less than the pivot to the beginning of the array,
        // so it's time to move our chosen pivot to the place where it should be...
        swap(arr, end, pivotIndex);
        return pivotIndex;
    }

    private void swap(int[] arr, int first, int second) {
        int temp = arr[first];
        arr[first] = arr[second];
        arr[second] = temp;
    }

}
