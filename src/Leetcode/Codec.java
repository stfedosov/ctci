package Leetcode;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author sfedosov on 9/15/19.
 */
public class Codec {

    public static void main(String[] args) {
        Codec codec = new Codec();
        System.out.println(codec.decode(codec.encode(Arrays.asList("", ""))));
    }

    private static final String SEPARATOR = "SEPARATOR";
    private static final String EMPTY = "EMPTY";
    private static final String EMPTY_LIST = "EMPTY_LIST";

    // Encodes a list of strings to a single string.
    public String encode(List<String> strs) {
        if (strs.isEmpty()) return EMPTY_LIST;
        final StringBuilder sb = new StringBuilder();
        strs.forEach(s -> {
            sb.append(s.isEmpty() ? EMPTY : s);
            sb.append(SEPARATOR);
        });
        return sb.toString();
    }

    // Decodes a single string to a list of strings.
    public List<String> decode(String s) {
        if (s.equals(EMPTY_LIST)) return Collections.emptyList();
        return Arrays.stream(s.split(SEPARATOR))
                .map(s1 -> s1.equals(EMPTY) ? "" : s1)
                .collect(Collectors.toList());
    }
}