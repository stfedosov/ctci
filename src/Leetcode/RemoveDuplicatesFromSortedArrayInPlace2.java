package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 11/21/19.
 */
public class RemoveDuplicatesFromSortedArrayInPlace2 {

    public static void main(String[] args) {
        int[] nums = {0, 0, 1, 1, 1, 1, 2, 3, 3};
        int i = removeDuplicates(nums);
        assert i == 7;
        assert Arrays.equals(Arrays.copyOfRange(nums, 0, i), new int[]{0, 0, 1, 1, 2, 3, 3});

        nums = new int[]{1, 1, 1, 2, 2, 3};
        i = removeDuplicates(nums);
        assert i == 5;
        assert Arrays.equals(Arrays.copyOfRange(nums, 0, i), new int[]{1, 1, 2, 2, 3});
    }

    public static int removeDuplicates(int[] nums) {
        if (nums.length == 0) return 0;
        int writer = 1;
        int reader = 1;
        int count = 1;
        int prev = nums[0];
        while (reader < nums.length) {
            if (prev == nums[reader]) {
                count++;
            } else {
                count = 1;
                prev = nums[reader];
            }
            if (count <= 2) {
                nums[writer++] = nums[reader];
            }
            reader++;
        }
        return writer;
    }

}
