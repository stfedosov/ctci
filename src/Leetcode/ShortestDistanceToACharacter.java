package Leetcode;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author sfedosov on 9/14/19.
 */
public class ShortestDistanceToACharacter {

    public static void main(String[] args) {
        assert Arrays.equals(shortestToChar("loveleetcode", 'e'), new int[]{3, 2, 1, 0, 1, 0, 0, 1, 2, 2, 1, 0});
    }

    public static int[] shortestToChar(String S, char C) {
        int[] array = new int[S.length()];
        Queue<Integer> indices = new LinkedList<>();
        for (int i = 0; i < S.length(); i++) {
            if (S.charAt(i) == C) indices.offer(i);
        }
        // fill the beginning
        int start = indices.poll();
        for (int i = start; i >= 0; i--) {
            array[i] = start - i;
        }
        // fill the middle
        while (!indices.isEmpty()) {
            int next = indices.poll();
            int diff = next - start - 1, mid = diff / 2, k = 1;
            for (int i = start + 1; i <= start + mid; i++) {
                array[i] = k;
                array[next - k] = k;
                k++;
            }
            array[start + mid + 1] = array[start + mid] + (diff % 2 == 0 ? 0 : 1);
            start = next;
        }
        // fill the rest
        for (int i = start + 1, k = 1; i < S.length(); i++, k++) {
            array[i] = k;
        }
        return array;
    }

}
