package Leetcode;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @author sfedosov on 8/24/19.
 */
public class Maze2 {

    public static void main(String[] args) {
        /*
             0 0 1 0 0
             0 0 0 0 0
             0 0 0 1 0
             1 1 0 1 1
             0 0 0 0 0
         */
        int[][] maze = {
                {0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0},
                {0, 0, 0, 1, 0},
                {1, 1, 0, 1, 1},
                {0, 0, 0, 0, 0}};

        assert shortestDistance(maze,
                new int[]{0, 4},
                new int[]{4, 4}) == 12;

        assert shortestDistance(maze,
                new int[]{0, 4},
                new int[]{3, 2}) == -1;
    }

    private static int[][] directions = new int[][]{{0, -1}, {0, 1}, {1, 0}, {-1, 0}};

    public static int shortestDistance(int[][] maze, int[] start, int[] destination) {
        Queue<Node> q = new PriorityQueue<>(Comparator.comparingInt(p -> p.distance));
        q.offer(new Node(start[0], start[1], 0));
        boolean[][] visited = new boolean[maze.length][maze[0].length];
        while (!q.isEmpty()) {
            Node polled = q.poll();
            int p1 = polled.start;
            int p2 = polled.end;
            int distance = polled.distance;
            visited[p1][p2] = true;
            if (p1 == destination[0] && p2 == destination[1]) {
                return distance;
            } else {
                for (int[] direction : directions) {
                    int x = p1;
                    int y = p2;
                    int dest = distance;
                    while (x + direction[0] >= 0
                            && y + direction[1] >= 0
                            && x + direction[0] < maze.length
                            && y + direction[1] < maze[x].length
                            && maze[x + direction[0]][y + direction[1]] == 0) {
                        x += direction[0];
                        y += direction[1];
                        dest++;
                    }

                    if (!visited[x][y]) {
                        q.offer(new Node(x, y, dest));
                    }
                }
            }

        }
        return -1;
    }

    static class Node {
        int start, end, distance;

        Node(int start, int end, int distance) {
            this.start = start;
            this.end = end;
            this.distance = distance;
        }
    }

}
