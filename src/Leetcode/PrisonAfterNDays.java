package Leetcode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author sfedosov on 8/13/19.
 */
public class PrisonAfterNDays {

    public static void main(String[] args) {
        assert Arrays.equals(prisonAfterNDaysBruteForce(new int[]{0, 1, 0, 1, 1, 0, 0, 1}, 7), new int[]{0, 0, 1, 1, 0, 0, 0, 0});
        assert Arrays.equals(prisonAfterNDaysOptimized(new int[]{0, 1, 0, 1, 1, 0, 0, 1}, 7), new int[]{0, 0, 1, 1, 0, 0, 0, 0});
    }

    public static int[] prisonAfterNDaysBruteForce(int[] cells, int N) {
        int[] result = new int[cells.length];
        while (N-- > 0) {
            for (int i = 1; i + 1 < cells.length; i++) {
                result[i] = cells[i - 1] == cells[i + 1] ? 1 : 0;
            }
            cells = result;
            result = new int[cells.length];
        }
        return cells;
    }

    public static int[] prisonAfterNDaysOptimized(int[] cells, int N) {
        if (cells == null || cells.length == 0 || N <= 0) return cells;
        boolean hasCycle = false;
        Set<String> cache = new HashSet<>();
        while (N > 0) {
            int[] next = nextDay(cells);
            String strNext = Arrays.toString(next);
            if (!cache.add(strNext)) {
                hasCycle = true;
                break;
            }
            cells = next;
            N--;
        }
        if (hasCycle) {
            N %= cache.size();
            while (N > 0) {
                cells = nextDay(cells);
                N--;
            }
        }
        return cells;
    }


    private static int[] nextDay(int[] cells) {
        int[] tmp = new int[cells.length];
        for (int i = 1; i < cells.length - 1; i++) {
            tmp[i] = cells[i - 1] == cells[i + 1] ? 1 : 0;
        }
        return tmp;
    }

}
