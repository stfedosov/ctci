package Leetcode;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @author sfedosov on 12/16/19.
 */
public class ClosestKValues {

    private static Queue<Entry> q = new PriorityQueue<>((o1, o2) -> Double.compare(o2.diff, o1.diff));

    public static List<Integer> closestKValues(TreeNode root, double target, int k) {
        traverse(root, target, k);
        List<Integer> l = new ArrayList<>();
        while (!q.isEmpty()) l.add(q.poll().candidate);
        return l;
    }

    private static void traverse(TreeNode root, double target, int k) {
        if (root == null) return;
        q.offer(new Entry(Math.abs(root.val - target), root.val));
        if (q.size() > k) q.poll();
        traverse(root.left, target, k);
        traverse(root.right, target, k);
    }

    static class Entry {
        private double diff;
        private int candidate;

        Entry(double diff, int candidate) {
            this.diff = diff;
            this.candidate = candidate;
        }
    }

}
