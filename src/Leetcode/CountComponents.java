package Leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CountComponents {

    public static void main(String[] args) {
        assert countComponents(5, new int[][]{{0, 1}, {1, 2}, {3, 4}}) == 2;
        assert countComponents(5, new int[][]{{0, 1}, {1, 2}, {2, 3}, {3, 4}}) == 1;
        assert countComponents(2, new int[][]{{1, 0}}) == 1;
    }

    public static int countComponents(int n, int[][] edges) {
        Map<Integer, List<Integer>> graph = new HashMap<>();
        for (int[] edge : edges) {
            graph.computeIfAbsent(edge[0], x -> new ArrayList<>()).add(edge[1]);
            graph.computeIfAbsent(edge[1], x -> new ArrayList<>()).add(edge[0]);
        }
        int numOfComponents = 0;
        Set<Integer> seen = new HashSet<>();
        for (int i = 0; i < n; i++) {
            if (seen.add(i)) {
                dfs(i, graph, seen);
                numOfComponents++;
            }
        }
        return numOfComponents;
    }

    private static void dfs(int start, Map<Integer, List<Integer>> graph, Set<Integer> seen) {
        seen.add(start);
        for (int neighbour : graph.getOrDefault(start, Collections.emptyList())) {
            if (!seen.contains(neighbour)) dfs(neighbour, graph, seen);
        }
    }

}
