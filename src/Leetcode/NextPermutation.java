package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 7/24/19.
 */
public class NextPermutation {

    public static void main(String[] args) {
        int[] nums = {1, 2, 3};
        nextPermutation(nums);
        assert Arrays.equals(nums, new int[]{1, 3, 2});
    }

    // key observation: for any given sequence that is in descending order, no next larger permutation is possible
    // i.e. if the elements are increasing from the right, they are currently at their largest possible permutation
    // algorithm: find such i that nums[i] > nums[i - 1]
    // 1, 2, 3 <-- we're here
    // 1, 3, 2 <-- next permutation
    // 2, 1, 3
    // 2, 3, 1
    // 3, 1, 2
    // 3, 2, 1
    public static void nextPermutation(int[] nums) {
        int i = nums.length - 2;
        while (i >= 0 && nums[i + 1] <= nums[i]) {
            i--;
        }
        if (i >= 0) {
            int j = nums.length - 1;
            while (j >= 0 && nums[j] <= nums[i]) {
                j--;
            }
            swap(nums, i, j);
        }
        reverse(nums, i + 1);
    }

    private static void reverse(int[] nums, int start) {
        int i = start, j = nums.length - 1;
        while (i < j) {
            swap(nums, i, j);
            i++;
            j--;
        }
    }

    private static void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

}
