package Leetcode;

import java.util.Arrays;

public class CheckInclusion {

    /*
    Given two strings s1 and s2, return true if s2 contains a permutation of s1, or false otherwise.
    In other words, return true if one of s1's permutations is the substring of s2.
     */
    public static void main(String[] args) {
        assert checkInclusion("ab", "eidbaooo");
        assert !checkInclusion("ab", "eidboaoo");
    }

    public static boolean checkInclusion(String s1, String s2) {
        int[] expected = new int[26], actual = new int[26];
        for (char c : s1.toCharArray()) {
            expected[c - 'a']++;
        }
        int counter = 0;
        for (int i = 0; i < s2.length(); i++) {
            actual[s2.charAt(i) - 'a']++;
            counter++;
            if (counter == s1.length()) {
                if (Arrays.equals(expected, actual)) {
                    return true;
                }
                actual[s2.charAt(i - s1.length() + 1) - 'a']--;
                counter--;
            }
        }
        return false;
    }

}
