package Leetcode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

/**
 * @author sfedosov on 1/12/20.
 */
public class AssignBikes {

    public static void main(String[] args) {
        assert Arrays.equals(assignBikes(new int[][]{{0, 0}, {1, 1}, {2, 0}}, new int[][]{{1, 0}, {2, 2}, {2, 1}}), new int[]{0, 2, 1});
        assert Arrays.equals(assignBikes(new int[][]{{0, 0}, {2, 1}}, new int[][]{{1, 2}, {3, 3}}), new int[]{1, 0});
    }

    public static int[] assignBikes(int[][] workers, int[][] bikes) {
        Queue<int[]> queue = new PriorityQueue<>((a, b) -> {
            int compare = Integer.compare(a[0], b[0]);
            if (compare == 0) {
                compare = Integer.compare(a[1], b[1]);
                if (compare == 0) {
                    compare = Integer.compare(a[2], b[2]);
                }
            }
            return compare;
        });
        for (int i = 0; i < workers.length; i++) {
            for (int j = 0; j < bikes.length; j++) {
                queue.offer(new int[]{Math.abs(bikes[j][0] - workers[i][0]) + Math.abs(bikes[j][1] - workers[i][1]), i, j});
            }
        }

        int[] result = new int[workers.length];
        // How do we make sure someone else doesn't use the same bike?
        // Use a visited Set.

        // How do we not overwrite what we previously took for this worker?
        // Initialize array to invalid value like -1, then make sure we only populate the invalid indices

        Arrays.fill(result, -1);
        Set<Integer> visited = new HashSet<>();
        while (!queue.isEmpty()) {
            int[] q = queue.poll();
            if (result[q[1]] == -1 && !visited.contains(q[2])) {
                result[q[1]] = q[2];
                visited.add(q[2]);
            }
        }
        return result;
    }

}
