package Leetcode;

public class AngleClock {

    public static void main(String[] args) {
        assert Double.compare(angleClock(12, 30), 165.0) == 0;
        assert Double.compare(angleClock(3, 30), 75.0) == 0;
        assert Double.compare(angleClock(3, 15), 7.5) == 0;
        assert Double.compare(angleClock(4, 50), 155) == 0;
    }

    public static double angleClock(int hour, int minutes) {
        int minuteAngle = 6;
        int hourAngle = 30;
        double angleForMinutes = minutes * minuteAngle;
        double angleForHour = (hour + minutes / 60.0) * hourAngle;
        double diff = Math.abs(angleForHour - angleForMinutes);
        return Math.min(diff, 360 - diff);
    }

}
