package Leetcode;

public class KokoEatingBananas {

    public static void main(String[] args) {
        System.out.println(minEatingSpeed(new int[]{3, 6, 7, 11}, 8));
    }

    public static boolean isPoss(int mid, int H, int[] piles) {
        for (int val : piles) {
            H -= (int) Math.ceil((double) val / mid);
        }
        return H >= 0;
    }

    public static int minEatingSpeed(int[] piles, int H) {
        int l = 1, r = findMax(piles);
        while (l < r) {
            int mid = l + (r - l) / 2;
            if (isPoss(mid, H, piles))
                r = mid;
            else
                l = mid + 1;
        }
        return l;
    }

    private static int findMax(int[] piles) {
        int r = Integer.MIN_VALUE;
        for (int val : piles)
            r = Math.max(val, r);
        return r;
    }

}
