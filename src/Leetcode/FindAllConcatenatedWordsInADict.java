package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author sfedosov on 8/31/19.
 */
public class FindAllConcatenatedWordsInADict {

    public static void main(String[] args) {
        assert findAllConcatenatedWordsInADict(new String[]{
                "cat", "cats", "catsdogcats", "dog", "dogcatsdog", "hippopotamuses", "rat", "ratcatdogcat"})
                .equals(Arrays.asList("catsdogcats", "dogcatsdog", "ratcatdogcat"));

        System.out.println(findAllConcatenatedWordsInADict2(new String[]{
                "cat", "cats", "catsdogcats", "dog", "dogcatsdog", "hippopotamuses", "rat", "ratcatdogcat"}));
    }

    // Non-cached

    public static List<String> findAllConcatenatedWordsInADict(String[] words) {
        if (words == null || words.length == 0) {
            return Collections.emptyList();
        }
        Set<String> set = new HashSet<>(Arrays.asList(words));
        List<String> result = new ArrayList<>();
        for (String word : words) {
            if (checkBreak(word, set, 0)) {
                result.add(word);
            }
        }
        return result;
    }

    private static boolean checkBreak(String word, Set<String> set, int counter) {
        if (word.isEmpty()) {
            return counter > 1;
        }
        for (int i = 1; i <= word.length(); i++) {
            String substr = word.substring(0, i);
            if (set.contains(substr) && checkBreak(word.substring(i), set, counter + 1)) {
                return true;
            }
        }
        return false;
    }

    // Cached

    public static List<String> findAllConcatenatedWordsInADict2(String[] words) {
        if (words == null || words.length == 0) {
            return Collections.emptyList();
        }
        Set<String> set = new HashSet<>(Arrays.asList(words));
        List<String> result = new ArrayList<>();
        for (String word : words) {
            if (checkBreak2(word, set, 0, 0, new Boolean[word.length()])) {
                result.add(word);
            }
        }
        return result;
    }

    private static boolean checkBreak2(String word, Set<String> set, int counter, int start, Boolean[] memo) {
        if (start == word.length()) {
            return counter > 1;
        }
        if (memo[start] != null) {
            return memo[start];
        }
        for (int i = start + 1; i <= word.length(); i++) {
            String substr = word.substring(start, i);
            if (set.contains(substr) && checkBreak2(word, set, counter + 1, i, memo)) {
                return memo[start] = true;
            }
        }
        return memo[start] = false;
    }

}
