package Leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sfedosov on 4/23/19.
 */
public class CamelCaseMatching {

    public static void main(String[] args) {
        System.out.println(camelMatch(new String[]{"FooBar","FooBarTest","FootBall","FrameBuffer","ForceFeedBack"}, "FoBaT"));
        System.out.println(camelMatch(new String[]{"FooBar","FooBarTest","FootBall","FrameBuffer","ForceFeedBack"}, "FB"));
    }

    public static List<Boolean> camelMatch(String[] queries, String pattern) {
        List<Boolean> result = new ArrayList<>();
        for (String query : queries) {
            result.add(isMatch(query, pattern));
        }
        return result;
    }

    private static boolean isMatch(String query, String pattern) {
        int idx = 0;
        for (char c : query.toCharArray()) {
            if (idx < pattern.length() && c == pattern.charAt(idx)) {
                idx++;
            } else if (Character.isUpperCase(c)) {
                return false;
            }
        }
        return idx == pattern.length();
    }

}
