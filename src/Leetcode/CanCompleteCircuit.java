package Leetcode;

/**
 * @author sfedosov on 10/9/19.
 */
public class CanCompleteCircuit {

    public static void main(String[] args) {
        assert canCompleteCircuitBruteForce(new int[]{1, 2, 3, 4, 5}, new int[]{3, 4, 5, 1, 2}) == 3;
        assert canCompleteCircuitBruteForce(new int[]{2, 3, 4}, new int[]{3, 4, 3}) == -1;

        assert canCompleteCircuitOptimized(new int[]{1, 2, 3, 4, 5}, new int[]{3, 4, 5, 1, 2}) == 3;
        assert canCompleteCircuitOptimized(new int[]{2, 3, 4}, new int[]{3, 4, 3}) == -1;
    }

    public static int canCompleteCircuitBruteForce(int[] gas, int[] cost) {
        for (int index = 0; index < gas.length; index++) {
            if (canTravel(gas, cost, index)) return index;
        }
        return -1;
    }

    private static boolean canTravel(int[] gas, int[] cost, int index) {
        int tank = 0;
        for (int i = index; i < gas.length; i++) {
            tank += gas[i] - cost[i];
            if (tank < 0) return false;
        }
        for (int i = 0; i <= index; i++) {
            tank += gas[i] - cost[i];
            if (tank < 0) return false;
        }
        return true;
    }

    public static int canCompleteCircuitOptimized(int[] gas, int[] cost) {
        int n = gas.length;

        int total_tank = 0;
        int curr_tank = 0;
        int starting_station = 0;
        for (int i = 0; i < n; ++i) {
            total_tank += gas[i] - cost[i];
            curr_tank += gas[i] - cost[i];
            // If one couldn't get here,
            if (curr_tank < 0) {
                // Pick up the next station as the starting one.
                starting_station = i + 1;
                // Start with an empty tank.
                curr_tank = 0;
            }
        }
        return total_tank >= 0 ? starting_station : -1;
    }

}
