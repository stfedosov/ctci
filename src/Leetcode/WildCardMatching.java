package Leetcode;

/**
 * @author sfedosov on 7/25/19.
 */
public class WildCardMatching {

    public static void main(String[] args) {
        System.out.println(isMatch("adceb", "*a*b"));
        System.out.println(isMatch("acdcb", "a*c?b"));
    }

    public static boolean isMatch(String s, String p) {
        return isMatch(s, p, 0, 0);
    }

    private static boolean isMatch(String s, String p, int i, int j) {
        if (j == p.length() - 1 && p.charAt(j) == '*') {
            return true;
        } else {
            if (j == p.length()) {
                return i == s.length();
            }
            if (p.charAt(j) == '*') {
                //if * is empty sequence, go isMatch(s, p, i, j + 1) otherwise, go isMatch(s, p, i + 1, j)
                return isMatch(s, p, i, j + 1) || (i < s.length() && isMatch(s, p, i + 1, j));
            } else if (i < s.length() && ((p.charAt(j) == s.charAt(i) || p.charAt(j) == '?'))) {
                return isMatch(s, p, i + 1, j + 1);
            }
        }
        return false;
    }


}
