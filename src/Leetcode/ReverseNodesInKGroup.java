package Leetcode;

/**
 * @author sfedosov on 4/3/20.
 */
public class ReverseNodesInKGroup {

    public static void main(String[] args) {
        ListNode root = new ListNode(1);
        ListNode next = new ListNode(2);
        root.next = next;
        ListNode next1 = new ListNode(3);
        next.next = next1;
        ListNode next2 = new ListNode(4);
        next1.next = next2;
        next2.next = new ListNode(5);
        // 1 -> 2 -> 3 -> 4 -> 5
        System.out.println(reverseKGroup(root, 2));
    }

    public static ListNode reverseKGroup(ListNode head, int k) {
        ListNode current = head;
        int count = 0;
        while (current != null && count != k) {
            current = current.next;
            count++;
        }

        if (count == k) {
            current = reverseKGroup(current, k);
            while (count-- > 0) {
                ListNode next = head.next;
                head.next = current;
                current = head;
                head = next;
            }
            head = current;
        }
        return head;
    }

    static class ListNode {
        int x;
        ListNode next;

        ListNode(int x) {
            this.x = x;
        }

        @Override
        public String toString() {
            return "ListNode{" +
                    "x=" + x +
                    ", next=" + next +
                    '}';
        }
    }

}
