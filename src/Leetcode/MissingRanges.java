package Leetcode;

import java.util.ArrayList;
import java.util.List;

public class MissingRanges {

    /*
    You are given an inclusive range [lower, upper] and a sorted unique integer array nums,
    where all elements are within the inclusive range.

    A number x is considered missing if x is in the range [lower, upper] and x is not in nums.

    Return the shortest sorted list of ranges that exactly covers all the missing numbers.
    That is, no element of nums is included in any of the ranges,
    and each missing number is covered by one of the ranges.

    Add "->" optionally
     */
    public static void main(String[] args) {
        System.out.println(findMissingRanges(new int[]{0, 1, 3, 50, 75}, 0, 99));
        System.out.println(findMissingRanges(new int[]{1, 3}, 0, 9));
        System.out.println(findMissingRanges(new int[]{}, 1, 1));
        System.out.println(findMissingRanges(new int[]{}, -3, -1));
        System.out.println(findMissingRanges(new int[]{-1}, -1, -1));
        System.out.println(findMissingRanges(new int[]{-1}, -2, -1));
        // [2-4, 6, 7, 10-14, 17, 19, 21-87]
        System.out.println(findMissingRangesVariant(new int[]{5, 8, 9, 15, 16, 18, 20}, 2, 87));
        // [3-9, 11, 13, 14, 17-29, 32-34]
        System.out.println(findMissingRangesVariant(new int[]{10, 12, 15, 16, 30, 31}, 3, 34));
    }

    public static List<List<Integer>> findMissingRanges(int[] nums, int lower, int upper) {
        List<List<Integer>> res = new ArrayList<>();
        if (nums.length == 0) {
            res.add(List.of(lower, upper));
            return res;
        }
        if (lower < nums[0]) {
            res.add(List.of(lower, nums[0] - 1));
        }
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i + 1] - nums[i] > 1) {
                res.add(List.of(nums[i] + 1, nums[i + 1] - 1));
            }
        }
        if (nums[nums.length - 1] < upper) {
            res.add(List.of(nums[nums.length - 1] + 1, upper));
        }
        return res;
    }

    public static List<String> findMissingRangesVariant(int[] nums, int lower, int upper) {
        List<String> missingRanges = new ArrayList<>();
        int currLower = lower;
        for (int num : nums) {
            if (num - currLower > 2) {
                missingRanges.add(currLower + "-" + (num - 1));
            } else if (num - currLower == 2) {
                missingRanges.add(String.valueOf(currLower));
                missingRanges.add(String.valueOf(currLower + 1));
            } else if (num - currLower == 1) {
                missingRanges.add(String.valueOf(currLower));
            }
            currLower = num + 1;
        }
        if (upper - nums[nums.length - 1] > 2) {
            missingRanges.add((nums[nums.length - 1] + 1) + "-" + upper);
        } else if (upper - nums[nums.length - 1] == 2) {
            missingRanges.add(String.valueOf(upper - 1));
            missingRanges.add(String.valueOf(upper));
        } else if (upper - nums[nums.length - 1] == 1) {
            missingRanges.add(String.valueOf(upper));
        }
        return missingRanges;
    }

}
