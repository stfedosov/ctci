package Leetcode;

public class JumpGame {

    public static void main(String[] args) {
        assert canJump(new int[]{2, 5, 0, 0});
        assert canJump(new int[]{2, 0, 0});
        assert canJump(new int[]{2, 3, 1, 1, 4});
        assert !canJump(new int[]{3, 2, 1, 0, 4});

        assert canJumpMemo(new int[]{2, 5, 0, 0});
        assert canJumpMemo(new int[]{2, 0, 0});
        assert canJumpMemo(new int[]{2, 3, 1, 1, 4});
        assert !canJumpMemo(new int[]{3, 2, 1, 0, 4});
    }

    // ----- Non-cached

    public static boolean canJump(int[] nums) {
        return canJump(nums, 0);
    }

    private static boolean canJump(int[] nums, int i) {
        if (i >= nums.length - 1) {
            return true;
        }
        for (int x = 1; x <= nums[i]; x++) {
            if (canJump(nums, i + x)) {
                return true;
            }
        }
        return false;
    }

    // ----- Cached

    public static boolean canJumpMemo(int[] nums) {
        Boolean[] memo = new Boolean[nums.length];
        return canJumpMemo(nums, 0, memo);
    }

    private static boolean canJumpMemo(int[] nums, int i, Boolean[] memo) {
        if (memo[i] != null) {
            return memo[i];
        }
        if (i >= nums.length - 1) {
            return true;
        }
        for (int x = 1; x <= nums[i]; x++) {
            if (canJumpMemo(nums, i + x, memo)) {
                memo[i + x] = true;
                return memo[i + x];
            }
        }
        memo[i] = false;
        return memo[i];
    }

}
