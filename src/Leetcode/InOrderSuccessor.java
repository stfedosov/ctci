package Leetcode;

/**
 * @author sfedosov on 7/24/19.
 */
public class InOrderSuccessor {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(2);
        root.left = new TreeNode(1);
        root.right = new TreeNode(3);
        assert inorderSuccessor(root, new TreeNode(1)).equals(root);
    }

    public static TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        TreeNode successor = null;
        while (root != null) {
            if (p.val < root.val) {
                successor = root;
                root = root.left;
            } else {
                root = root.right;
            }
        }
        return successor;
    }

    // PREDECESSOR:

    public TreeNode predecessor(TreeNode root, TreeNode p) {
        if (root == null)
            return null;

        if (root.val < p.val) {
            TreeNode right = predecessor(root.right, p);
            return (right != null) ? right : root;
        } else {
            return predecessor(root.left, p);
        }
    }

    // SUCCESSOR:

    public TreeNode successor(TreeNode root, TreeNode p) {
        if (root == null)
            return null;

        if (root.val > p.val) {
            TreeNode left = successor(root.left, p);
            return (left != null) ? left : root;
        } else {
            return successor(root.right, p);
        }
    }

}
