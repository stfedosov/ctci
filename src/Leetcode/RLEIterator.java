package Leetcode;

/**
 * @author sfedosov on 3/17/19.
 */
class RLEIteratorMain {

    public static void main(String[] args) {
        RLEIterator iterator = new RLEIterator(new int[]{3, 8, 0, 9, 2, 5});
        assert iterator.next(2) == 8;
        assert iterator.next(1) == 8;
        assert iterator.next(1) == 5;
        assert iterator.next(2) == -1;
    }

    static class RLEIterator {
        int[] A;
        int i, q;

        RLEIterator(int[] A) {
            this.A = A;
            i = q = 0;
        }

        public int next(int n) {
            while (i < A.length) {
                if (q + n > A[i]) {
                    n -= A[i] - q;
                    q = 0;
                    i += 2;
                } else {
                    q += n;
                    return A[i + 1];
                }
            }

            return -1;
        }
    }


}
