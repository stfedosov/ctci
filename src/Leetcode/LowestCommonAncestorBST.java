package Leetcode;

/**
 * @author sfedosov on 4/13/18.
 */
public class LowestCommonAncestorBST {

    public static void main(String[] args) {
        // case 1: BST

        TreeNode root = new TreeNode(20);
        root.right = new TreeNode(22);
        TreeNode node8 = new TreeNode(8);
        node8.left = new TreeNode(4);
        TreeNode node12 = new TreeNode(12);
        node8.right = node12;
        node12.left = new TreeNode(10);
        node12.right = new TreeNode(14);
        root.left = node8;

        //        20
        //       /  \
        //      8   22
        //     / \
        //    4  12
        //      /  \
        //     10  14

        assert lca(root, 10, 14).val == 12;
        assert lca(root, 14, 8).val == 8;
        // case 2: binary tree
    }

    // Time complexity: O(H = log(N) on average), Space: O(1)
    private static TreeNode lca(TreeNode root, int n1, int n2) {
        if (root == null) {
            return null;
        }
        if (root.val > n1 && root.val > n2) {
            return lca(root.left, n1, n2);
        }
        if (root.val < n1 && root.val < n2) {
            return lca(root.right, n1, n2);
        }
        return root;
    }

}
