package Leetcode;

import java.util.LinkedList;
import java.util.Queue;

public class IsGraphBipartite {

    public static void main(String[] args) {
        assert isBipartite(new int[][]{{1, 3}, {0, 2}, {1, 3}, {0, 2}});
        assert !isBipartite(new int[][]{{1, 2, 3}, {0, 2}, {0, 1, 3}, {0, 2}});
    }

    public static boolean isBipartite(int[][] graph) {
        int[] visited = new int[graph.length];
        // 0 - non-visited
        // 1 - black
        // 2 - red
        for (int i = 0; i < graph.length; i++) {
            if (visited[i] == 0) {
                Queue<Integer> queue = new LinkedList<>();
                queue.offer(i);
                visited[i] = 1;
                while (!queue.isEmpty()) {
                    int node = queue.poll();
                    for (int child : graph[node]) {
                        if (visited[child] == 0) {
                            queue.offer(child);
                            visited[child] = visited[node] == 1 ? 2 : 1;
                        } else if (visited[child] == visited[node]) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

}
