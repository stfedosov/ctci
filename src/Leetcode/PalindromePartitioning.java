package Leetcode;

import java.util.HashMap;
import java.util.Map;

public class PalindromePartitioning {

    public static void main(String[] args) {
        assert minCut("aab") == 1;
        assert minCut("aabb") == 1;
        assert minCut("aabcb") == 1;
        assert minCut("fifgbeajcacehiicccfecbfhhgfiiecdcjjffbghdidbhbdbfbfjccgbbdcjheccfbhafehieabbdfeigbiaggchaeghaijfbjhi") == 75;
    }

    public static int minCut(String s) {
        return find(s, new HashMap<>());
    }

    private static int find(String s, Map<String, Integer> cache) {
        if (isPal(s)) return 0;
        if (cache.containsKey(s)) return cache.get(s);
        int min = Integer.MAX_VALUE;
        for (int i = 1; i < s.length(); i++) {
            if (isPal(s.substring(0, i))) {
                min = Math.min(min, 1 + find(s.substring(i), cache));
            }
        }
        cache.put(s, min);
        return min;
    }


    private static boolean isPal(String s) {
        int start = 0, end = s.length() - 1;
        while (start < end) {
            if (s.charAt(start++) != s.charAt(end--)) {
                return false;
            }
        }
        return true;
    }

}
