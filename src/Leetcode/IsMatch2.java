package Leetcode;

import java.util.Arrays;

public class IsMatch2 {

    public static void main(String[] args) {
        assert !isMatch("cb", "?a");
        assert isMatch("aa", "*");
        assert !isMatch("aa", "a");
        assert isMatch("adceb", "*a*b");
        assert !isMatch("acdcb", "a*c?b");
    }

    public static boolean isMatch(String s, String p) {
        int[][] cache = new int[s.length() + 1][p.length() + 1];
        for (int[] c : cache) Arrays.fill(c, -1);
        return isMatch(s, p, 0, 0, cache);
    }

    private static boolean isMatch(String s, String p, int i, int j, int[][] cache) {
        if (j == p.length()) return i == s.length();
        if (cache[i][j] != -1) return cache[i][j] == 1;
        boolean result;
        if (p.charAt(j) == '*') {
            result = i != s.length() && isMatch(s, p, i + 1, j, cache) || isMatch(s, p, i, j + 1, cache);
        } else {
            result = i != s.length() && (p.charAt(j) == s.charAt(i) || p.charAt(j) == '?') && isMatch(s, p, i + 1, j + 1, cache);
        }
        cache[i][j] = result ? 1 : 0;
        return result;
    }

}
