package Leetcode;

/**
 * @author sfedosov on 12/9/18.
 */
public class LongestUnivaluePath {

    private int count = 0;

    public int longestUnivaluePath(TreeNode root) {
        if (root == null)
            return 0;
        dfs(root);
        return count;
    }

    public int dfs(TreeNode node) {
        if (node == null)
            return 0;
        int l = dfs(node.left);
        int r = dfs(node.right);
        int cur = 0;
        if (node.left != null && node.val == node.left.val)
            cur = Math.max(cur, l + 1);
        if (node.right != null && node.val == node.right.val)
            cur = Math.max(cur, r + 1);
        if (node.left != null && node.val == node.left.val && node.right != null && node.val == node.right.val) {
            count = Math.max(count, l + r + 2);
        }
        count = Math.max(count, cur);
        return cur;
    }

}
