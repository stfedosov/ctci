package Leetcode;

import java.util.Queue;

import java.util.LinkedList;

/**
 * @author sfedosov on 8/30/19.
 */
public class SnakesAndLadders {

    public static void main(String[] args) {
        assert snakesAndLadders(new int[][]{
                {-1, -1, -1, -1, -1, -1},
                {-1, -1, -1, -1, -1, -1},
                {-1, -1, -1, -1, -1, -1},
                {-1, 35, -1, -1, 13, -1},
                {-1, -1, -1, -1, -1, -1},
                {-1, 15, -1, -1, -1, -1}
        }) == 4;

        assert snakesAndLadders(new int[][]{
                {-1, -1},
                {-1, 3}
        }) == 1;

        assert snakesAndLadders(new int[][]{
                {-1, -1},
                {-1, 1}
        }) == 1;

        assert snakesAndLadders(new int[][]{
                {-1, -1, -1},
                {-1, 9, 8},
                {-1, 8, 9}
        }) == 1;

        assert snakesAndLadders(new int[][]{
                {-1, -1, 19, 10, -1},
                {2, -1, -1, 6, -1},
                {-1, 17, -1, 19, -1},
                {25, -1, 20, -1, -1},
                {-1, -1, -1, -1, 15}
        }) == 2;
    }

    public static int snakesAndLadders(int[][] board) {
        int N = board.length;
        if (N == 0) return 0;

        // transform the board to one-dimensional array
        int[] nBoard = new int[N * N + 1];
        int t = 1;
        boolean moveRight = true;
        for (int i = N - 1; i >= 0; i--) {
            if (moveRight) {
                for (int j = 0; j < N; j++) {
                    nBoard[t++] = board[i][j];
                }
            } else {
                for (int j = N - 1; j >= 0; j--) {
                    nBoard[t++] = board[i][j];
                }
            }
            moveRight = !moveRight;
        }

        Queue<Integer> q = new LinkedList<>();
        q.add(1);
        int steps = 0;
        boolean[] visited = new boolean[N * N + 1];
        visited[1] = true;

        while (!q.isEmpty()) {
            steps++;
            int curCnt = q.size();
            for (int i = 0; i < curCnt; i++) {
                int square = q.poll();
                for (int j = 1; j <= 6; j++) {
                    int next_square = square + j;
                    if (nBoard[next_square] != -1) next_square = nBoard[next_square];
                    if (visited[next_square]) continue;

                    if (next_square == N * N) return steps;
                    visited[next_square] = true;
                    q.offer(next_square);
                }
            }
        }
        return -1;
    }

}
