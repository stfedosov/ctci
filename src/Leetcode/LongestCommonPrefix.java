package Leetcode;

public class LongestCommonPrefix {

    public static void main(String[] args) {
        System.out.println(longestCommonPrefix(new String[]{"abc", "abn", "ab", "a"}));
        System.out.println(longestCommonPrefix(new String[]{"abc", "abn", "ab", "a"}));
        System.out.println(longestCommonPrefix(new String[]{"", "b"}));
        System.out.println(longestCommonPrefix(new String[]{"aa", "a"}));
    }

    // Time complexity : O(N) , where N is the sum of all characters in all strings.
    public static String longestCommonPrefix(String[] strs) {
        StringBuilder prefix = new StringBuilder();
        if (strs.length == 0) return prefix.toString();
        int x = 0;
        for (char c : strs[0].toCharArray()) {
            for (int i = 1; i < strs.length; i++) {
                if (x >= strs[i].length() || strs[i].charAt(x) != c) return prefix.toString();
            }
            prefix.append(c);
            x++;
        }
        return prefix.toString();
    }

}
