package Leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

public class JumpGame4 {

    /*
    Given an array of integers arr, you are initially positioned at the first index of the array.

    In one step you can jump from index i to index:

    i + 1 where: i + 1 < arr.length.
    i - 1 where: i - 1 >= 0.
    j where: arr[i] == arr[j] and i != j.

    Return the minimum number of steps to reach the last index of the array.
     */

    public static void main(String[] args) {
        assert minJumps(new int[]{100, -23, -23, 404, 100, 23, 23, 23, 3, 404}) == 3;
        assert minJumps(new int[]{6, 1, 9}) == 2;
        assert minJumps(new int[]{7}) == 0;
        assert minJumps(new int[]{11, 22, 7, 7, 7, 7, 7, 7, 7, 22, 13}) == 3;
    }

    public static int minJumps(int[] arr) {
        Map<Integer, List<Integer>> indices = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            indices.computeIfAbsent(arr[i], x -> new ArrayList<>()).add(i);
        }
        Queue<Integer> q = new LinkedList<>();
        Set<Integer> visited = new HashSet<>();
        q.offer(0);
        int level = 0;
        while (!q.isEmpty()) {
            int size = q.size();
            while (size > 0) {
                int cur = q.poll();
                if (cur == arr.length - 1) return level;
                if (cur - 1 >= 0 && !visited.contains(cur - 1)) {
                    visited.add(cur - 1);
                    q.offer(cur - 1);
                }
                if (cur + 1 < arr.length && !visited.contains(cur + 1)) {
                    visited.add(cur + 1);
                    q.offer(cur + 1);
                }
                for (int x : indices.get(arr[cur])) {
                    if (x != cur && !visited.contains(x)) {
                        visited.add(x);
                        q.offer(x);
                    }
                }
                // for corner cases when we have a huge amount of same numbers in the array
                indices.get(arr[cur]).clear();
                size--;
            }
            level++;
        }
        return -1;
    }
}
