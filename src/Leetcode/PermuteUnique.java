package Leetcode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author sfedosov on 11/19/18.
 */
public class PermuteUnique {

    public static void main(String[] args) {
        List<List<Integer>> answer = new ArrayList<>();
        answer.add(new ArrayList<Integer>() {{
            add(1);
            add(1);
            add(2);
        }});
        answer.add(new ArrayList<Integer>() {{
            add(1);
            add(2);
            add(1);
        }});
        answer.add(new ArrayList<Integer>() {{
            add(2);
            add(1);
            add(1);
        }});
        assert permuteUnique(new int[]{1, 1, 2}).equals(answer);
        System.out.println(permuteUnique(new int[]{1, 1, 2, 2}));
    }

    public static List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> ans = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return ans;
        }
        permute(ans, nums, 0);
        return ans;
    }

    private static void permute(List<List<Integer>> ans, int[] nums, int index) {
        if (index == nums.length) {
            List<Integer> temp = new ArrayList<>();
            for (int num : nums) {
                temp.add(num);
            }
            ans.add(temp);
            return;
        }
        Set<Integer> appeared = new HashSet<>();
        for (int i = index; i < nums.length; ++i) {
            if (appeared.add(nums[i])) {
                swap(nums, index, i);
                permute(ans, nums, index + 1);
                swap(nums, index, i);
            }
        }
    }

    private static void swap(int[] nums, int i, int j) {
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }

}
