package Leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class TimeMap {

    public static void main(String[] args) {
        TimeMap map = new TimeMap();
        map.set("love","high",10);
        map.set("love","low",20);
//        System.out.println(map.get("foo", 1));
        System.out.println(map.get("love", 5));
        System.out.println(map.get("love", 10));
        System.out.println(map.get("love", 15));
        System.out.println(map.get("love", 20));
    }

    private Map<String, List<EntryObject>> map;

    /**
     * Initialize your data structure here.
     */
    public TimeMap() {
        this.map = new HashMap<>();
    }

    public void set(String key, String value, int timestamp) {
        map.computeIfAbsent(key, x -> new ArrayList<>()).add(new EntryObject(value, timestamp));
    }

    public String get(String key, int timestamp) {
        List<EntryObject> list = map.get(key);
        int left = 0, right = list.size() - 1;
        while (left <= right) {
            int m = left + (right - left) / 2;
            int currTime = list.get(m).getTimestamp();
            if (currTime == timestamp) {
                return list.get(m).getValue();
            }
            if (currTime < timestamp) {
                left = m + 1;
            } else {
                right = m - 1;
            }
        }
        if (right < 0) return "";
        else return list.get(right).getValue();
    }

}

class EntryObject {
    String value;
    int timestamp;

    public EntryObject(String value, int timestamp) {
        this.value = value;
        this.timestamp = timestamp;
    }

    public String getValue() {
        return value;
    }

    public int getTimestamp() {
        return timestamp;
    }
}

/**
 * Your TimeMap object will be instantiated and called as such:
 * TimeMap obj = new TimeMap();
 * obj.set(key,value,timestamp);
 * String param_2 = obj.get(key,timestamp);
 */