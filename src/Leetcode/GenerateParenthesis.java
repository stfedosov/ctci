package Leetcode;

import java.util.ArrayList;
import java.util.List;

public class GenerateParenthesis {

    public static void main(String[] args) {
        System.out.println(generateParenthesis(4));
    }

    /*
        Time complexity: O(4^n)

        Given a tree with branching of B and a maximum depth of M, the maximum number of nodes in this tree would be:

        Sum(1 + B + B^2 + ... + B^(M-1)) ~ B^M

        We have a branching of 2 and maximum depth of 2n as the length of the output will be the sum of n opening and
        n closing parentheses.

        Hence, O(2^2*n) = O(4^n)

     */
    public static List<String> generateParenthesis(int n) {
        List<String> result = new ArrayList<>();
        dfs(result, "", 0, 0, n);
        return result;
    }

    private static void dfs(List<String> result, String tmp, int open, int closed, int n) {
        if (open == n && closed == n) {
            result.add(tmp);
            return;
        }
        if (open < n) {
            dfs(result, tmp + "(", open + 1, closed, n);
        }
        if (closed < open) {
            dfs(result, tmp + ")", open, closed + 1, n);
        }
    }
}
