package Leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 4/25/20.
 */
public class HTMLEntityParser {

    public static void main(String[] args) {
        System.out.println(entityParser("&;&;"));
        System.out.println(entityParser("&"));
        System.out.println(entityParser(""));
        System.out.println(entityParser("njkvnevn"));
        System.out.println(entityParser("&njkvnevn;"));
        System.out.println(entityParser("&am&p;"));
    }

    public static String entityParser(String text) {
        Map<String, String> map = new HashMap<String, String>() {{
            put("&quot;", "\"");
            put("&apos;", "'");
            put("&amp;", "&");
            put("&gt;", ">");
            put("&lt;", "<");
            put("&frasl;", "/");
        }};
        StringBuilder sb = new StringBuilder();
        boolean specStarted = false;
        StringBuilder toMatch = new StringBuilder();
        for (char c : text.toCharArray()) {
            if (!specStarted) {
                if (c == '&') {
                    specStarted = true;
                    toMatch.append(c);
                } else {
                    sb.append(c);
                }
            } else if (c == '&') {
                sb.append(toMatch);
                toMatch.setLength(0);
                toMatch.append(c);
            } else {
                toMatch.append(c);
                if (c == ';') {
                    sb.append(map.getOrDefault(toMatch.toString(), toMatch.toString()));
                    toMatch.setLength(0);
                    specStarted = false;
                }
            }
        }
        if (toMatch.length() != 0) sb.append(toMatch);
        return sb.toString();
    }

}
