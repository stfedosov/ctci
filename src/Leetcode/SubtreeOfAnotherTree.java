package Leetcode;

public class SubtreeOfAnotherTree {

    // O(M + N)
    public boolean isSubtree(TreeNode s, TreeNode t) {
        if ((s.left == null && s.right == null)
                && (t.left == null && t.right == null)) {
            return s.val == t.val;
        }
        String s1 = toString(s);
        String s2 = toString(t);
        return s1.contains(s2);
    }

    private String toString(TreeNode root) {
        if (root == null) return "X";
        String s = root.val + " ";
        s += toString(root.left);
        s += toString(root.right);
        return s;
    }

    // --------====== ALTERNATIVE O(M*N) ======--------

    public boolean isSubtree2(TreeNode root, TreeNode subRoot) {
        if (root == null) return false;
        if (isSame(root, subRoot)) return true;
        return isSubtree2(root.left, subRoot) || isSubtree2(root.right, subRoot);
    }

    private boolean isSame(TreeNode root, TreeNode subRoot) {
        if (root == null && subRoot == null) return true;
        if (root == null || subRoot == null) return false;

        return root.val == subRoot.val && isSame(root.left, subRoot.left) && isSame(root.right, subRoot.right);
    }

}
