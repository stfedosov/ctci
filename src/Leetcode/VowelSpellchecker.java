package Leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author sfedosov on 9/11/19.
 */
public class VowelSpellchecker {

    static Set<Character> vowels = new HashSet<>();

    static {
        vowels.add('a');
        vowels.add('e');
        vowels.add('i');
        vowels.add('o');
        vowels.add('u');
    }

    public static void main(String[] args) {
        assert Arrays.equals(spellchecker(new String[]{"KiTe", "kite", "hare", "Hare"},
                new String[]{"kite", "Kite", "KiTe", "Hare", "HARE", "Hear", "hear", "keti", "keet", "keto"}),

                new String[]{"kite", "KiTe", "KiTe", "Hare", "hare", "", "", "KiTe", "", "KiTe"});

        assert Arrays.equals(spellchecker(new String[]{"YellOw"}, new String[]{"yeellow"}), new String[]{""});

        assert Arrays.equals(spellchecker(new String[]{"ae", "aa"}, new String[]{"UU"}), new String[]{"ae"});

        assert Arrays.equals(spellchecker(new String[]{"wg", "uo", "as", "kv", "ra", "mw", "gi", "we", "og", "zu"},
                new String[]{"AS", "in", "yc", "kv", "mw", "ov", "lc", "os", "wm", "Mw"}),

                new String[]{"as", "", "", "kv", "mw", "", "", "as", "", "mw"});

    }

    public static String[] spellchecker(String[] wordlist, String[] queries) {
        Set<String> words = new HashSet<>(Arrays.asList(wordlist));
        Map<String, String> map = new HashMap<>();
        Map<String, String> devowel = new HashMap<>();
        for (String word : wordlist) {
            map.putIfAbsent(word.toLowerCase(), word);
            devowel.putIfAbsent(devowel(word), word);
        }
        String[] result = new String[queries.length];
        int i = 0;
        for (String query : queries) {
            String devoweled = devowel(query);
            String res = "";
            if (words.contains(query)) {
                res = query;
            } else if (map.containsKey(query.toLowerCase())) {
                res = map.get(query.toLowerCase());
            } else if (devowel.containsKey(devoweled)) {
                res = devowel.get(devoweled);
            }
            result[i++] = res;
        }
        return result;
    }

    private static String devowel(String word) {
        return word.toLowerCase().replaceAll("[aeiou]", "#");
    }
}
