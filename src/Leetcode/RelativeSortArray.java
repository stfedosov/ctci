package Leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author sfedosov on 8/12/19.
 */
public class RelativeSortArray {

    public static void main(String[] args) {
        assert Arrays.equals(relativeSortArrayNLogN(new int[]{2, 3, 1, 3, 2, 4, 6, 7, 9, 2, 19}, new int[]{2, 1, 4, 3, 9, 6}),
                new int[]{2, 2, 2, 1, 4, 3, 3, 9, 6, 7, 19});
        assert Arrays.equals(relativeSortArrayNLogN(new int[]{28, 6, 22, 8, 44, 17}, new int[]{22, 28, 8, 6}),
                new int[]{22, 28, 8, 6, 17, 44});
        assert Arrays.equals(relativeSortArrayCountingSort(new int[]{28, 6, 22, 8, 44, 17}, new int[]{22, 28, 8, 6}),
                new int[]{22, 28, 8, 6, 17, 44});
        assert Arrays.equals(relativeSortArrayCountingSort(new int[]{2, 3, 1, 3, 2, 4, 6, 7, 9, 2, 19}, new int[]{2, 1, 4, 3, 9, 6}),
                new int[]{2, 2, 2, 1, 4, 3, 3, 9, 6, 7, 19});
    }

    public static int[] relativeSortArrayNLogN(int[] arr1, int[] arr2) {
        Map<Integer, Integer> map = new HashMap<>();
        int idx = 0;
        for (int x : arr2) {
            map.put(x, idx++);
        }
        List<Integer> list = Arrays
                .stream(arr1)
                .boxed().sorted((a, b) -> {
                    int diff = map.getOrDefault(a, Integer.MAX_VALUE).compareTo(map.getOrDefault(b, Integer.MAX_VALUE));
                    return diff == 0 ? a - b : diff;
                }).collect(Collectors.toList());
        int[] toReturn = new int[list.size()];
        idx = 0;
        for (int x : list) {
            toReturn[idx++] = x;
        }
        return toReturn;
    }

    public static int[] relativeSortArrayCountingSort(int[] arr1, int[] arr2) {
        int[] memo = new int[1001];
        for (int i : arr1) {
            memo[i]++;
        }
        int j = 0;
        for (int i : arr2) {
            while (memo[i] != 0) {
                arr1[j] = i;
                j++;
                memo[i]--;
            }
        }
        for (int i = 0; i < 1001; i++) {
            while (memo[i] != 0) {
                arr1[j] = i;
                j++;
                memo[i]--;
            }
        }
        return arr1;
    }

}
