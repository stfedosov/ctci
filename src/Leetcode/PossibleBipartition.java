package Leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author sfedosov on 3/29/20.
 */
public class PossibleBipartition {

    public static void main(String[] args) {
        assert !possibleBipartition(3, new int[][]{{1, 2}, {1, 3}, {2, 3}});
        assert possibleBipartition(4, new int[][]{{1, 2}, {1, 3}, {2, 4}});
        assert !possibleBipartition(5, new int[][]{{1, 2}, {2, 3}, {3, 4}, {4, 5}, {1, 5}});
    }

    public static boolean possibleBipartition(int N, int[][] dislikes) {
        int[] visited = new int[N + 1];
        // 0 - non-visited, 1 - black, 2 - red
        List<List<Integer>> graph = new ArrayList<>();
        for (int i = 0; i <= N; i++) graph.add(new ArrayList<>());
        for (int[] dislike : dislikes) {
            graph.get(dislike[0]).add(dislike[1]);
            graph.get(dislike[1]).add(dislike[0]);
        }
        for (int i = 0; i < graph.size(); i++) {
            if (visited[i] == 0) {
                Queue<Integer> q = new LinkedList<>();
                q.offer(i);
                visited[i] = 1;
                while (!q.isEmpty()) {
                    int polled = q.poll();
                    for (int child : graph.get(polled)) {
                        if (visited[child] == 0) {
                            q.offer(child);
                            visited[child] = visited[polled] == 1 ? 2 : 1;
                        } else if (visited[polled] == visited[child]){
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

}
