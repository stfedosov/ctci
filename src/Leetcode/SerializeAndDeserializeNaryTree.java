package Leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Queue;

public class SerializeAndDeserializeNaryTree {

    public static void main(String[] args) {
        Node initial = new Node(1, new ArrayList<>());
        Node node3 = new Node(3, new ArrayList<>());
        initial.children.add(node3);
        node3.children.add(new Node(5, new ArrayList<>()));
        node3.children.add(new Node(6, new ArrayList<>()));
        initial.children.add(new Node(2, new ArrayList<>()));
        initial.children.add(new Node(4, new ArrayList<>()));
        String serialize = serialize(initial);
        System.out.println(serialize);
        Node actual = deserialize(serialize);
        assert Objects.equals(actual, initial);
    }

    // Encodes a tree to a single string.
    public static String serialize(Node root) {
        if (root == null) return "";
        StringBuilder res = new StringBuilder(String.valueOf(root.val));
        for (Node child : root.children) {
            res.append(" ").append("[").append(serialize(child)).append("]");
        }
        return res.toString();
    }

    // Decodes your encoded data to tree.
    public static Node deserialize(String data) {
        if (data.isEmpty()) return null;
        Queue<String> q = new LinkedList<>();
        char[] array = data.toCharArray();
        int prev = Integer.MAX_VALUE;
        for (char c : array) {
            if (c == ']' || c == '[' || c == ' ') {
                if (prev != Integer.MAX_VALUE) q.offer(String.valueOf(prev));
                if (c != ' ') q.offer(String.valueOf(c));
                prev = Integer.MAX_VALUE;
            } else {
                if (prev == Integer.MAX_VALUE) prev = 0;
                prev = prev * 10 + (c - '0');
            }
        }
        if (prev != Integer.MAX_VALUE && q.isEmpty())
            return new Node(prev, new ArrayList<>());
        return helper(q);
    }

    private static Node helper(Queue<String> q) {
        if (q.isEmpty()) return null;
        Node root = new Node(Integer.parseInt(q.poll()), new ArrayList<>());
        while (!q.isEmpty()) {
            String c = q.poll();
            if (c.equals("[")) {
                Node n = helper(q);
                if (n != null) root.children.add(n);
            } else if (c.equals("]")) {
                break;
            } else {
                root.children.add(new Node(Integer.parseInt(c), new ArrayList<>()));
            }
        }
        return root;
    }

    static class Node {
        public int val;
        public List<Node> children;

        public Node(int _val) {
            val = _val;
            children = new ArrayList<>();
        }

        public Node(int _val, List<Node> _children) {
            val = _val;
            children = _children;
        }
    }

}
