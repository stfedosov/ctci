package Leetcode;

public class ValidWordAbbreviation {

    // i18n
    public static void main(String[] args) {
        assert validWordAbbreviation("internationalization", "i12iz4n");
        assert validWordAbbreviation("internationalization", "i5a11o1");
        assert !validWordAbbreviation("apple", "a2e");
        assert validWordAbbreviation("substitution", "su3i1u2on");
    }

    public static boolean validWordAbbreviation(String word, String abbr) {
        if (word == null || abbr == null) return false;
        int i = 0, j = 0;
        while (i < word.length() && j < abbr.length()) {
            char c1 = word.charAt(i), c2 = abbr.charAt(j);
            if (c1 == c2) {
                i++;
                j++;
            } else if (Character.isDigit(c2) && c2 != '0') {
                int skip = 0;
                while (j < abbr.length() && Character.isDigit(abbr.charAt(j))) {
                    skip = skip * 10 + (abbr.charAt(j) - '0');
                    j++;
                }
                i += skip;
            } else {
                return false;
            }
        }
        return i == word.length() && j == abbr.length();
    }

}
