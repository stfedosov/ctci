package Leetcode;

public class WaterTrap {

    public static void main(String[] args) {
        System.out.println(trapConstantSpace(new int[]{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1}));
    }

    // trapping water
    public static int trap(int[] height) {
        int max = 0;
        for (int i = 0; i < height.length; i++) {
            int max_left = 0;
            int max_right = 0;
            for (int j = i; j >= 0; j--) {
                max_left = Math.max(max_left, height[j]);
            }
            for (int j = i; j < height.length; j++) {
                max_right = Math.max(max_right, height[j]);
            }
            max += Math.min(max_left, max_right) - height[i];
        }
        return max;
    }

    public static int trapOptimized(int[] height) {
        if (height == null || height.length == 0) {
            return 0;
        }
        int ans = 0;
        int size = height.length;
        int[] left_max = new int[size];
        int[] right_max = new int[size];
        left_max[0] = height[0];
        // find max height upto the given point from left
        for (int i = 1; i < size; i++) {
            left_max[i] = Math.max(height[i], left_max[i - 1]);
        }
        right_max[size - 1] = height[size - 1];
        // find max height upto the given point from right
        for (int i = size - 2; i >= 0; i--) {
            right_max[i] = Math.max(height[i], right_max[i + 1]);
        }
        for (int i = 0; i < size - 1; i++) {
            // choose the minimum to find intersection between the two heights
            ans += Math.min(left_max[i], right_max[i]) - height[i];
        }
        return ans;
    }

    public static int trapConstantSpace(int[] height) {
        if (height.length == 0 || height == null) {
            return 0;
        }
        int ans = 0, leftMax = 0, rightMax = 0;
        int i = 0, j = height.length - 1;
        while (i < j) {
            leftMax = Math.max(leftMax, height[i]);
            rightMax = Math.max(rightMax, height[j]);
            if (leftMax < rightMax) {
                ans += Math.max(0, leftMax - height[i]);
                i++;
            } else {
                ans += Math.max(0, rightMax - height[j]);
                j--;
            }
        }
        return ans;
    }

}
