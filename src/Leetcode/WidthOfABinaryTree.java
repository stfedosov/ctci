package Leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 4/15/19.
 */
public class WidthOfABinaryTree {

    public static void main(String[] args) {

        TreeNode root = new TreeNode(1);
        TreeNode left = new TreeNode(3);
        left.left = new TreeNode(5);
        root.left = left;
        root.right = new TreeNode(2);

        //        1
        //       / \
        //      3   2
        //     /
        //    5

        assert widthOfBinaryTree(root) == 2;

        TreeNode root2 = new TreeNode(1);
        TreeNode three = new TreeNode(3);
        three.left = new TreeNode(5);
        three.right = new TreeNode(3);
        root2.left = three;

        assert widthOfBinaryTree(root2) == 2;

        TreeNode root3 = new TreeNode(1);
        TreeNode treeNode3 = new TreeNode(3);
        treeNode3.left = new TreeNode(5);
        treeNode3.right = new TreeNode(3);
        root3.left = treeNode3;
        TreeNode treeNode2 = new TreeNode(2);
        treeNode2.right = new TreeNode(9);
        root3.right = treeNode2;

        //      1
        //    /   \
        //   3     2
        //  / \     \
        // 5   3     9

        assert widthOfBinaryTree(root3) == 4;

    }

    static Map<Integer, int[]> map = new HashMap<>();

    public static int widthOfBinaryTree(TreeNode root) {
        if (root == null) return 0;
        map.clear();

        findMax(root, 0, 0);

        int res = 1;
        for (int[] rec : map.values()) {
            res = Math.max(res, rec[1] - rec[0] + 1);
        }
        return res;
    }

    private static void findMax(TreeNode root, int level, int pos) {
        if (root == null) return;

        int[] rec = map.get(level);
        if (rec == null) {
            rec = new int[2];
            rec[0] = Integer.MAX_VALUE;
            rec[1] = Integer.MIN_VALUE;
        }

        rec[0] = Math.min(rec[0], pos);
        rec[1] = Math.max(rec[1], pos);
        map.put(level, rec);

        findMax(root.left, level + 1, 2 * pos);
        findMax(root.right, level + 1, 2 * pos + 1);
    }

}
