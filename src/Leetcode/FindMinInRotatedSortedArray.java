package Leetcode;

/**
 * @author sfedosov on 10/21/19.
 */
public class FindMinInRotatedSortedArray {

    public static void main(String[] args) {
        assert findMin(new int[]{3, 4, 5, 1, 2}) == 1;
        assert findMin(new int[]{4, 5, 6, 7, 0, 1, 2}) == 0;
        assert findMin(new int[]{1, 2, 3}) == 1;
        assert findMin(new int[]{3, 1, 2}) == 1;
        assert findMin(new int[]{1, 2}) == 1;
        assert findMin(new int[]{1}) == 1;
        assert findMin(new int[]{2, 3, 4, 5, 1}) == 1;
        assert findMin(new int[]{2, 1}) == 1;
    }

    public static int findMin(int[] nums) {
        if (nums == null || nums.length == 0) return 0;
        if (nums.length == 1) return nums[0];
        return findRotationPoint(nums, 0, nums.length - 1);
    }

    private static int findRotationPoint(int[] nums, int start, int end) {
        while (start <= end) {
            int mid = start + (end - start) / 2;
            if (mid == 0 && nums[mid + 1] > nums[mid] || mid > 0 && nums[mid - 1] > nums[mid]) {
                return nums[mid];
            } else if (nums[mid] < nums[0]) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }
        // not rotated array -> return just its beginning
        return nums[0];
    }

}
