package Leetcode;

public class HasPathSum {

    /*

          5
         / \
        4   8
       /   / \
      11  13  4
     /  \      \
    7    2      1

     */

    public static boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) return false;
        if (root.left == null && root.right == null && sum - root.val == 0) return true;
        return hasPathSum(root.left, sum - root.val) || hasPathSum(root.right, sum - root.val);
    }

}
