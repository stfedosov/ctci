package Leetcode;

import java.util.HashMap;
import java.util.Map;

public class FindSumPairs {

    /*
    You are given two integer arrays nums1 and nums2. You are tasked to implement a data structure that supports queries of two types:

    Add a positive integer to an element of a given index in the array nums2.
    Count the number of pairs (i, j) such that nums1[i] + nums2[j] equals a given value (0 <= i < nums1.length and 0 <= j < nums2.length).
     */
    public static void main(String[] args) {
        FindSumPairs findSumPairs = new FindSumPairs(new int[]{1, 1, 2, 2, 2, 3}, new int[]{1, 4, 5, 2, 5, 4});
        System.out.println(findSumPairs.count(7));
        findSumPairs.add(3,2);
        System.out.println(findSumPairs.count(8));
    }

    private final int[] nums1, nums2;
    private final Map<Integer, Integer> map2 = new HashMap<>();

    public FindSumPairs(int[] nums1, int[] nums2) {
        this.nums1 = nums1;
        this.nums2 = nums2;
        for (int x : nums2) map2.put(x, map2.getOrDefault(x, 0) + 1);
    }

    public void add(int index, int val) {
        map2.put(nums2[index], map2.get(nums2[index]) - 1);
        if (map2.get(nums2[index]) == 0) map2.remove(nums2[index]);
        nums2[index] += val;
        map2.put(nums2[index], map2.getOrDefault(nums2[index], 0) + 1);
    }

    public int count(int tot) {
        int count = 0;
        for (int x : nums1) {
            count += map2.getOrDefault(tot - x, 0);
        }
        return count;
    }
}