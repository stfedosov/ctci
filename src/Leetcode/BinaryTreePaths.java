package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BinaryTreePaths {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode two = new TreeNode(2);
        two.right = new TreeNode(5);
        root.left = two;
        root.right = new TreeNode(3);
        System.out.println(binaryTreePaths(root).equals(Arrays.asList("1->2->5", "1->3")));
    }

    public static List<String> binaryTreePaths(TreeNode root) {
        List<String> list = new ArrayList<>();
        dfs(root, list, "");
        return list;
    }

    private static void dfs(TreeNode root, List<String> list, String tmp) {
        if (root == null) {
            return;
        }
        if (root.left == null && root.right == null) {
            list.add(tmp + root.val);
        }
        dfs(root.left, list, tmp + root.val + "->");
        dfs(root.right, list, tmp + root.val + "->");
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

}
