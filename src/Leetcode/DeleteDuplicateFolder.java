package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class DeleteDuplicateFolder {

    //     *
    // /   |   \
    // a   c   d
    // |   |   |
    // b   b   a
    /*
    Problem:
    Due to a bug, there are many duplicate folders in a file system.
    Two folders (not necessarily on the same level) are identical
    if they contain the same non-empty set of identical subfolders and underlying subfolder structure.

    Return the 2D array ans containing the paths of the remaining folders after deleting all the marked folders.
    The paths may be returned in any order.

    MergeIntervals:
    Main idea is to create a serialized collection of subtrees and check whether we have subtrees with the same structure.
    Mark for deletion those nodes which have such property and take this into account when printing the answer.

    */
    public static void main(String[] args) {
        System.out.println(deleteDuplicateFolder(Arrays.asList(Arrays.asList("a"), Arrays.asList("c"), Arrays.asList("d"),
                Arrays.asList("a", "b"), Arrays.asList("c", "b"), Arrays.asList("d", "a"))));
    }

    public static List<List<String>> deleteDuplicateFolder(List<List<String>> paths) {
        Node root = new Node("*");
        for (List<String> path : paths) {
            Node tmp = root;
            for (String s : path) {
                if (!tmp.map.containsKey(s)) tmp.map.put(s, new Node(s));
                tmp = tmp.map.get(s);
            }
        }
        Map<String, List<Node>> seen = new HashMap<>();
        serialize(root, seen);
        List<List<String>> res = new ArrayList<>();
        for (Map.Entry<String, List<Node>> entry : seen.entrySet()) {
            if (entry.getValue().size() >= 2) for (Node node : entry.getValue()) node.markForDeletion = true;
        }
        dfs(root, res, new ArrayList<>());
        return res;
    }

    private static String serialize(Node root, Map<String, List<Node>> seen) {
        if (root.map.isEmpty()) return root.c + "";
        StringBuilder sb = new StringBuilder("(");
        for (String childKey : root.map.keySet()) {
            sb.append(childKey)
                    .append(",")
                    .append(serialize(root.map.get(childKey), seen));
        }
        sb.append(")");
        String key = sb.toString();
        seen.computeIfAbsent(key, x -> new ArrayList<>()).add(root);
        return key;
    }

    private static void dfs(Node root, List<List<String>> res, List<String> tmp) {
        if (!tmp.isEmpty()) res.add(new ArrayList<>(tmp));
        for (String childKey : root.map.keySet()) {
            Node child = root.map.get(childKey);
            if (child.markForDeletion) continue;
            tmp.add(childKey);
            dfs(child, res, tmp);
            tmp.remove(tmp.size() - 1);
        }
    }

    private static class Node {
        String c;
        boolean markForDeletion = false;
        TreeMap<String, Node> map = new TreeMap<>();

        public Node(String c) {
            this.c = c;
        }
    }

}
