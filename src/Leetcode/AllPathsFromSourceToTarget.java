package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sfedosov on 11/15/19.
 */
public class AllPathsFromSourceToTarget {

    public static void main(String[] args) {
        assert allPathsSourceTarget(new int[][]{{1, 2}, {3}, {3}, {}}).equals(
                Arrays.asList(
                        Arrays.asList(0, 1, 3),
                        Arrays.asList(0, 2, 3)));
        assert allPathsSourceTarget(new int[][]{{4, 3, 1}, {3, 2, 4}, {3}, {4}, {}}).equals(
                Arrays.asList(
                        Arrays.asList(0, 4),
                        Arrays.asList(0, 3, 4),
                        Arrays.asList(0, 1, 3, 4),
                        Arrays.asList(0, 1, 2, 3, 4),
                        Arrays.asList(0, 1, 4)));
    }

    public static List<List<Integer>> allPathsSourceTarget(int[][] graph) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        for (int i = 0; i < graph.length; i++) {
            List<Integer> neighbours = map.getOrDefault(i, new ArrayList<>());
            for (int vertex : graph[i]) {
                neighbours.add(vertex);
            }
            map.put(i, neighbours);
        }
        List<List<Integer>> result = new ArrayList<>();
        dfs(map, 0, graph.length - 1, result, new ArrayList<>(Collections.singletonList(0)));
        return result;
    }

    private static void dfs(Map<Integer, List<Integer>> map,
                            int start,
                            int end,
                            List<List<Integer>> result,
                            List<Integer> list) {
        if (start == end) {
            result.add(new ArrayList<>(list));
        } else {
            for (Integer node : map.get(start)) {
                list.add(node);
                dfs(map, node, end, result, list);
                list.remove(list.size() - 1);
            }
        }
    }

}
