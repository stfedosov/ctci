package Leetcode;

public class MultiplyStrings {

    public static void main(String[] args) {
        System.out.println(multiply("123", "456"));
        System.out.println(multiply("123456789", "987654321"));
    }

    public static String multiply(String num1, String num2) {
        var products = new int[num1.length() + num2.length()];
        for (int i = num1.length() - 1; i >= 0; i--) {
            for (int j = num2.length() - 1; j >= 0; j--) {
                // Since digits in multi-digit numbers overlap when multiplying,
                // we may already have a value stored in result[i + j + 1] from previous calculations.
                // so we need to add prev num to the final SUM
                var tmpSum = (num1.charAt(i) - '0') * (num2.charAt(j) - '0') + products[i + j + 1];
                products[i + j + 1] = tmpSum % 10;
                products[i + j] += tmpSum / 10;
            }
        }
        var res = new StringBuilder();
        for (int product : products) {
            if (res.length() == 0 && product == 0) {
                continue;
            }
            res.append((char) (product + '0'));
        }
        return res.length() == 0 ? "0" : res.toString();
    }


}
