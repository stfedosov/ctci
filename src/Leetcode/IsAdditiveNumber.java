package Leetcode;

public class IsAdditiveNumber {

    public static void main(String[] args) {
        assert isAdditiveNumber("112358");
        assert isAdditiveNumber("199100199");
        assert !isAdditiveNumber("124");
    }

    public static boolean isAdditiveNumber(String num) {
        for (int i = 1; i < num.length(); i++) {
            for (int j = i; j < num.length(); j++) {
                String first = num.substring(0, i);
                String second = num.substring(i, j + 1);
                String third = num.substring(j + 1);
                if (isInvalid(first) || isInvalid(second) || isInvalid(third)) continue;
                String added = (Long.parseLong(first) + Long.parseLong(second)) + "";
                if (third.equals(added) || third.startsWith(added) && isAdditiveNumber(second + third))
                    return true;
            }
        }
        return false;
    }

    private static boolean isInvalid(String num) {
        return num.isEmpty() || num.length() > 1 && num.charAt(0) == '0';
    }
}
