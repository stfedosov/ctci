package Leetcode;

/**
 * @author sfedosov on 3/14/18.
 */
public class ValidateAnagram {

    public static void main(String[] args) {
        System.out.println(isAnagram("ab", "ba"));
    }

    public static boolean isAnagram(String s, String t) {
        int[] chars = new int[26];
        for (char c : s.toCharArray()) {
            chars[c - 'a']++;
        }
        for (char x : t.toCharArray()) {
            chars[x - 'a']--;
        }
        for (int i : chars) {
            if (i != 0) {
                return false;
            }
        }
        return true;
    }

}
