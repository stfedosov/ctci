package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 4/5/18.
 */
public class MinimumPathSum {

    public static void main(String[] args) {
        int[][] grid = new int[][]{
                {1, 3, 1},
                {1, 5, 1},
                {4, 2, 1},
                {2, 5, 3}
        };
        System.out.println(minPathSum(grid));
    }

    public static int minPathSum(int[][] grid) {
        int[][] memo = new int[grid.length + 1][grid[0].length];
        for (int[] mem : memo) {
            Arrays.fill(mem, -1);
        }
        return minPathSum(grid, 0, 0, memo);
    }

    private static int minPathSum(int[][] grid, int i, int j, int[][] memo) {
        if (!isValid(i, j, grid)) return Integer.MAX_VALUE;
        if (i == grid.length - 1 && j == grid[0].length - 1) return grid[i][j];
        if (memo[i][j] != -1) return memo[i][j];
        int min1 = minPathSum(grid, i + 1, j, memo);
        int min2 = minPathSum(grid, i, j + 1, memo);
        memo[i][j] = Math.min(min1, min2) + grid[i][j];
        return memo[i][j];
    }

    private static int minPathSumRecursive(int[][] grid, int i, int j) {
        if (!isValid(i, j, grid)) return Integer.MAX_VALUE;
        if (i == grid.length - 1 && j == grid[0].length - 1) return grid[i][j];
        int min1 = minPathSumRecursive(grid, i + 1, j);
        int min2 = minPathSumRecursive(grid, i, j + 1);
        return Math.min(min1, min2) + grid[i][j];
    }

    private static boolean isValid(int x, int y, int[][] map) {
        return x >= 0 && y >= 0 && x < map.length && y < map[0].length;
    }
}
