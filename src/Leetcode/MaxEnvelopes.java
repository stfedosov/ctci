package Leetcode;

import java.util.Arrays;
import java.util.Comparator;

public class MaxEnvelopes {

    public static void main(String[] args) {
        assert maxEnvelopes(new int[][]{{5, 4}, {6, 4}, {6, 7}, {2, 3}}) == 3;
        assert maxEnvelopes(new int[][]{{2, 100}, {3, 200}, {4, 300}, {5, 500}, {5, 400}, {5, 250}, {6, 370}, {6, 360}, {7, 380}}) == 5;
    }

    public static int maxEnvelopes(int[][] envelopes) {
        Arrays.sort(envelopes, Comparator.comparingInt(a -> a[0]));
        return count(envelopes, 0, new int[]{-1, -1});
    }

    private static int count(int[][] envelopes, int idx, int[] prev) {
        if (idx >= envelopes.length) return 0;
        int take = 0;
        if (prev[0] == -1 || (envelopes[idx][0] > prev[0] && envelopes[idx][1] > prev[1])) {
            take = 1 + count(envelopes, idx + 1, envelopes[idx]);
        }
        int notTake = count(envelopes, idx + 1, prev);
        take = Math.max(take, notTake);
        return take;
    }

}
