package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @author sfedosov on 8/1/19.
 */
public class CutOffTreesForGolfEvent {

    private static int counter = 0;
    private static int lastX = 0;
    private static int lastY = 0;

    public static void main(String[] args) {
        resetVariables();
        List<List<Integer>> forest = new ArrayList<>();
        forest.add(Arrays.asList(1, 2, 3));
        forest.add(Arrays.asList(0, 0, 0));
        forest.add(Arrays.asList(7, 6, 5));
        assert cutOffTree(forest) == -1;

        resetVariables();
        forest = new ArrayList<>();
        forest.add(Arrays.asList(1, 2, 3));
        forest.add(Arrays.asList(0, 0, 4));
        forest.add(Arrays.asList(7, 6, 5));
        assert cutOffTree(forest) == 6;

        resetVariables();
        forest = new ArrayList<>();
        forest.add(Arrays.asList(2, 3, 4));
        forest.add(Arrays.asList(0, 0, 5));
        forest.add(Arrays.asList(8, 7, 6));
        assert cutOffTree(forest) == 6;

        resetVariables();
        forest = new ArrayList<>();
        forest.add(Arrays.asList(54581641, 64080174, 24346381, 69107959));
        forest.add(Arrays.asList(86374198, 61363882, 68783324, 79706116));
        forest.add(Arrays.asList(668150, 92178815, 89819108, 94701471));
        forest.add(Arrays.asList(83920491, 22724204, 46281641, 47531096));
        forest.add(Arrays.asList(89078499, 18904913, 25462145, 60813308));
        assert cutOffTree(forest) == 57;
    }

    private static void resetVariables() {
        counter = 0;
        lastY = 0;
        lastX = 0;
    }

    public static int cutOffTree(List<List<Integer>> forest) {
        int[][] grid = new int[forest.size()][forest.get(0).size()];
        Queue<Integer> minHeap = new PriorityQueue<>();
        for (int i = 0; i < forest.size(); i++) {
            for (int j = 0; j < forest.get(0).size(); j++) {
                Integer value = forest.get(i).get(j);
                grid[i][j] = value;
                if (value > 1) {
                    minHeap.add(value);
                }
            }
        }
        while (!minHeap.isEmpty()) {
            bfs(grid, lastX, lastY, minHeap.poll());
            if (counter < 0) {
                return -1;
            }
        }
        return counter;
    }

    private static void bfs(int[][] grid, int startX, int startY, Integer toFind) {
        Queue<Node> queue = new LinkedList<>();
        queue.add(new Node(startX, startY, 0));

        boolean[][] visited = new boolean[grid.length][grid[0].length];
        int minDistance = Integer.MAX_VALUE;
        while (!queue.isEmpty()) {
            Node currentNode = queue.poll();
            int distance = currentNode.distance;
            int x = currentNode.x;
            int y = currentNode.y;
            if (grid[x][y] == toFind) {
                lastX = x;
                lastY = y;
                minDistance = Math.min(minDistance, distance);
            } else {
                if (isValid(grid, visited, x + 1, y)) {
                    visited[x + 1][y] = true;
                    queue.add(new Node(x + 1, y, distance + 1));
                }
                if (isValid(grid, visited, x - 1, y)) {
                    visited[x - 1][y] = true;
                    queue.add(new Node(x - 1, y, distance + 1));
                }
                if (isValid(grid, visited, x, y + 1)) {
                    visited[x][y + 1] = true;
                    queue.add(new Node(x, y + 1, distance + 1));
                }
                if (isValid(grid, visited, x, y - 1)) {
                    visited[x][y - 1] = true;
                    queue.add(new Node(x, y - 1, distance + 1));
                }
            }
        }
        if (minDistance == Integer.MAX_VALUE) {
            counter = -1;
        } else {
            counter += minDistance;
        }
    }

    private static boolean isValid(int[][] map, boolean[][] visited, int x, int y) {
        return x < map.length && y < map[0].length && x >= 0 && y >= 0 && map[x][y] != 0 && !visited[x][y];
    }

    static class Node {
        int x;
        int y;
        int distance;

        public Node(int x, int y, int distance) {
            this.x = x;
            this.y = y;
            this.distance = distance;
        }
    }


}
