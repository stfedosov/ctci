package Leetcode;

public class SumOfTwoIntegersWithoutPlusAndMinus {

    public static void main(String[] args) {
        assert getSum(1, 2) == 3;
        assert getSum(2, 3) == 5;
        assert getSum(5, 2) == 7;
    }

    // Given two integers a and b, return the sum of the two integers without using the operators + and -
    public static int getSum(int a, int b) {
        while (b != 0) {
            int sum = a ^ b;
            int carry = a & b;
            a = sum;
            b = carry << 1;
        }
        return a;
    }

}
