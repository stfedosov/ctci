package Leetcode;

import java.util.HashMap;
import java.util.Map;

public class LRU {

    public static void main(String[] args) {
        LRUCache cache = new LRUCache(2);
        cache.put(1, 1);
        cache.put(2, 2);
        System.out.println(cache.get(1));       // returns 1
        cache.put(3, 3);    // evicts key 2
        System.out.println(cache.get(2));       // returns -1 (not found)
        cache.put(4, 4);    // evicts key 1
        System.out.println(cache.get(1));       // returns -1 (not found)
        System.out.println(cache.get(3));       // returns 3
        System.out.println(cache.get(4));       // returns 4
    }

    static class LRUCache {

        private final int capacity;
        private final Map<Integer, Node> map;
        private final Node head;
        private final Node tail;

        public LRUCache(int capacity) {
            this.capacity = capacity;
            this.map = new HashMap<>();
            this.head = new Node(0, 0);
            this.tail = new Node(0, 0);
            this.head.next = this.tail;
            this.tail.prev = this.head;
        }

        public int get(int key) {
            if (map.get(key) == null) return -1;
            Node current = map.get(key);
            removeNode(current);
            addToHead(current);
            return current.value;
        }

        private Node popTail() {
            Node currentTail = tail.prev;
            removeNode(currentTail);
            return currentTail;
        }

        private void removeNode(Node node) {
            Node previous = node.prev;
            Node next = node.next;
            previous.next = next;
            next.prev = previous;
        }

        private void addToHead(Node node) {
            Node prevNext = this.head.next;
            this.head.next = node;
            node.next = prevNext;
            node.prev = this.head;
            prevNext.prev = node;
        }

        public void put(int key, int value) {
            Node newNode = new Node(key, value);
            if (!map.containsKey(key)) {
                if (map.size() >= capacity) {
                    Node currentTail = popTail();
                    map.remove(currentTail.key);
                }
            } else {
                Node current = map.get(key);
                removeNode(current);
            }
            addToHead(newNode);
            map.put(key, newNode);
        }
    }

    static class Node {
        int key;
        int value;
        public Node prev;
        public Node next;

        public Node(int key, int value) {
            this.key = key;
            this.value = value;
        }
    }

}
