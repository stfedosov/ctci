package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class HighFive {

    public static void main(String[] args) {
        assert Arrays.deepToString(highFive(new int[][]{{1, 91}, {1, 92}, {2, 93}, {2, 97}, {1, 60}, {2, 77}, {1, 65}, {1, 87}, {1, 100}, {2, 100}, {2, 76}}))
                .equals("[[1, 87], [2, 88]]");
        assert Arrays.deepToString(highFivePQ(new int[][]{{1, 91}, {1, 92}, {2, 93}, {2, 97}, {1, 60}, {2, 77}, {1, 65}, {1, 87}, {1, 100}, {2, 100}, {2, 76}}))
                .equals("[[1, 87], [2, 88]]");
    }

    // Brute force with sorting, ~ 36 ms

    public static int[][] highFive(int[][] items) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        for (int[] item : items) {
            map.putIfAbsent(item[0], new ArrayList<>());
            map.get(item[0]).add(item[1]);
        }
        int[][] result = new int[map.size()][];
        for (int i = 0; i < result.length; i++) {
            if (map.containsKey(i + 1)) result[i] = new int[]{i + 1, getSumWithSorting(map.get(i + 1))};
        }
        return result;
    }

    private static int getSumWithSorting(List<Integer> list) {
        list.sort((a, b) -> b - a);
        int sum = 0;
        for (int i = 0; i < 5; i++) {
            sum += list.get(i);
        }
        return sum / 5;
    }

    // More fast version, ~ 4ms

    public static int[][] highFivePQ(int[][] items) {
        Map<Integer, Queue<Integer>> map = new HashMap<>();
        for (int[] item : items) {
            map.putIfAbsent(item[0], new PriorityQueue<>(5));
            Queue<Integer> q = map.get(item[0]);
            q.offer(item[1]);
            if (q.size() > 5) q.poll();
        }
        int[][] result = new int[map.size()][];
        for (int i = 0; i < result.length; i++) {
            if (map.containsKey(i + 1)) result[i] = new int[]{i + 1, getSumWithoutSorting(map.get(i + 1))};
        }
        return result;
    }

    private static int getSumWithoutSorting(Queue<Integer> q) {
        int sum = 0;
        while (!q.isEmpty()) {
            sum += q.poll();
        }
        return sum / 5;
    }


}
