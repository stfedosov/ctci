package Leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @author sfedosov on 7/18/19.
 */
public class RearrangeBarcodes {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(rearrangeBarcodes(new int[]{1, 1, 1, 2, 2, 2})));
    }

    public static int[] rearrangeBarcodes(int[] barcodes) {
        Map<Integer, Integer> counts = new HashMap<>();
        for (int x : barcodes) {
            counts.put(x, counts.getOrDefault(x, 0) + 1);
        }
        Queue<int[]> q = new PriorityQueue<>((a, b) -> b[1] - a[1]);
        for (Map.Entry<Integer, Integer> entry : counts.entrySet()) {
            q.offer(new int[]{entry.getKey(), entry.getValue()});
        }
        int index = 0;
        while (!q.isEmpty()) {
            int[] polled1 = q.poll();
            int[] polled2 = q.poll();
            barcodes[index++] = polled1[0];
            polled1[1]--;
            if (polled2 != null) {
                barcodes[index++] = polled2[0];
                polled2[1]--;
            }
            if (polled1[1] > 0) {
                q.offer(new int[]{polled1[0], polled1[1]});
            }
            if (polled2 != null && polled2[1] > 0) {
                q.offer(new int[]{polled2[0], polled2[1]});
            }
        }
        return barcodes;
    }
}
