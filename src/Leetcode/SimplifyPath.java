package Leetcode;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;

public class SimplifyPath {

    public static void main(String[] args) {
        assert simplifyPath("/home/").equals("/home");
        assert simplifyPath("/a/./b/../../c/").equals("/c");
        assert simplifyPath("/../").equals("/");
        assert simplifyPath("/a//b////c/d//././/..").equals("/a/b/c");
        assert simplifyPath("/home/foo/.ssh/../.ssh2/authorized_keys/").equals("/home/foo/.ssh2/authorized_keys");
        assert simplifyPath("/").equals("/");
        assert simplifyPath("/.").equals("/");
    }

    // using stack
    public static String simplifyPath(String path) {
        Deque<String> stack = new ArrayDeque<>();
        Set<String> skip = new HashSet<>(Arrays.asList("..", ".", ""));
        for (String dir : path.split("/")) {
            if (dir.equals("..") && !stack.isEmpty()) stack.pop();
            else if (!skip.contains(dir)) stack.push(dir);
        }
        StringBuilder res = new StringBuilder();
        while (!stack.isEmpty()) {
            res.insert(0, "/" + stack.pop());
        }
        return (res.length() == 0) ? "/" : res.toString();
    }

    public String simplifyPathNoExtraSpace(String path) {
        var parts = path.split("/"); // Split the path into components
        int index = 0; // Pointer for the result position within the array

        for (String part : parts) {
            if (part.equals("..")) {
                if (index > 0) {
                    index--; // Simulate "pop" by moving index back
                }
            } else if (!part.isEmpty() && !part.equals(".")) {
                parts[index++] = part; // Simulate "push" by overwriting the array
            }
        }

        if (index == 0) {
            return "/"; // Special case for root
        }
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < index; i++) {
            result.append("/").append(parts[i]);
        }
        return result.toString();
    }

}
