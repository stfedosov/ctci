package Leetcode;

public class TotalOccurrencesOfKInSortedArray {

    private enum SearchType {
        FIRST,
        LAST
    }

    public static void main(String[] args) {
        int[] nums = {1, 1, 1, 1, 2, 3, 4};
        assert searchRange(nums, 1) == 4;
        nums = new int[]{1, 1, 2, 2, 2, 3};
        assert searchRange(nums, 2) == 3;
        nums = new int[]{1, 1, 2, 2, 2, 2, 3};
        assert searchRange(nums, 2) == 4;
    }

    public static int searchRange(int[] nums, int target) {
        int leftBoundIndex = search(nums, target, SearchType.FIRST);
        if (leftBoundIndex == -1) {
            return -1;
        }
        int rightBoundIndex = search(nums, target, SearchType.LAST);
        return rightBoundIndex - leftBoundIndex + 1;
    }

    private static int search(int[] nums, int target, SearchType searchType) {
        int left = 0, right = nums.length - 1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] == target) {
                switch (searchType) {
                    case FIRST:
                        if (mid - 1 >= 0 && nums[mid - 1] == target) {
                            right = mid - 1;
                        } else {
                            return mid;
                        }
                        break;
                    case LAST:
                        if (mid + 1 < nums.length && nums[mid + 1] == target) {
                            left = mid + 1;
                        } else {
                            return mid;

                        }
                        break;
                }
            } else if (nums[mid] < target) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return -1;
    }

    // ------------------------

    public int[] searchRange2(int[] nums, int target) {
        int left = search(nums, target, true);
        if (left == -1) return new int[]{-1, -1};
        return new int[]{left, search(nums, target, false)};
    }

    public int search(int[] nums, int target, boolean isFirst) {
        int l = 0, h = nums.length - 1, idx = -1;
        while (l <= h) {
            int mid = l + (h - l) / 2;
            if (nums[mid] == target) {
                idx = mid;
                if (isFirst) h = mid - 1;
                else l = mid + 1;
            } else if (nums[mid] < target) {
                l = mid + 1;
            } else {
                h = mid - 1;
            }
        }
        return idx;
    }

}
