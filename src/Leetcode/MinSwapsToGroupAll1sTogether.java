package Leetcode;

public class MinSwapsToGroupAll1sTogether {

    public static void main(String[] args) {
        assert minSwaps(new int[]{1, 0, 1, 0, 1}) == 1;
        assert minSwaps(new int[]{0, 0, 0, 1, 0}) == 0;
        assert minSwaps(new int[]{1, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1}) == 3;
        assert minSwaps(
                new int[]{1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1}) == 8;
    }

    public static int minSwaps(int[] data) {
        int zeros = 0, ones = 0;
        // count total number of ones - window size
        for (int x : data) {
            if (x == 1) ones++;
        }
        // count number of zeros within that window
        for (int i = 0; i < ones; i++) {
            if (data[i] == 0) zeros++;
        }
        int min = zeros;
        // sliding window
        for (int i = ones; i < data.length; i++) {
            if (data[i] == 0) {
                zeros++;
            }
            if (data[i - ones] == 0) {
                zeros--;
            }
            min = Math.min(min, zeros);
        }
        return min;
    }

    // case of a circular array
    public int minSwapsCircular(int[] data) {
        int zeros = 0, ones = 0;
        for (int x : data) {
            if (x == 1) ones++;
        }
        for (int i = 0; i < ones; i++) {
            if (data[i] == 0) zeros++;
        }
        int min = zeros;
        // since we have a circular array we need to traverse it twice
        for (int i = ones; i < 2 * data.length; i++) {
            if (data[i % data.length] == 0) {
                zeros++;
            }
            if (data[(i - ones) % data.length] == 0) {
                zeros--;
            }
            min = Math.min(min, zeros);
        }
        return min;
    }

}
