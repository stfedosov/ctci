package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * @author sfedosov on 6/4/19.
 */
public class IndexPairsOfAString {

    public static void main(String[] args) {
        System.out.println(Arrays.deepToString(indexPairs("thestoryofleetcodeandme", new String[]{"story", "fleet", "leetcode"})));
        System.out.println(Arrays.deepToString(indexPairs("ababa", new String[]{"aba", "ab"})));
    }

    public static int[][] indexPairs(String text, String[] words) {
        List<int[]> res = new ArrayList<>();
        for (int i = 0; i < text.length(); ++i) {
            for (String word : words) {
                if (text.startsWith(word, i)) {
                    res.add(new int[]{i, i + word.length() - 1});
                }
            }
        }
        res.sort((a, b) -> {
            if (a[0] != b[0]) return a[0] - b[0];
            else return a[1] - b[1];
        });
        return convert(res);
    }

    private static int[][] convert(List<int[]> listOfPairs) {
        int[][] result = new int[listOfPairs.size()][2];
        int i = 0;
        for (int[] ints : listOfPairs) {
            result[i++] = ints;
        }
        return result;
    }


}
