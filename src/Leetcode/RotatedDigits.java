package Leetcode;

/**
 * @author sfedosov on 4/8/20.
 */
public class RotatedDigits {

    public static void main(String[] args) {
        assert rotatedDigits(10) == 4;
        assert rotatedDigits(100) == 40;
        assert rotatedDigits(1000) == 316;
    }

    public static int rotatedDigits(int N) {
        int count = 0;
        for (int i = 1; i <= N; i++) {
            if (isValid(i)) count++;
        }
        return count;
    }

    private static boolean isValid(int N) {
        /*
	    Valid if N contains AT LEAST ONE 2, 5, 6, 9
	    AND NO 3, 4 or 7s
	    */
        boolean validFound = false;
        while (N > 0) {
            int d = N % 10;
            if (d == 2 || d == 5 || d == 6 || d == 9) validFound = true;
            if (d == 3 || d == 4 || d == 7) return false;
            N = N / 10;
        }
        return validFound;
    }
}
