package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;

/**
 * @author sfedosov on 12/6/18.
 */
public class TopKFrequentWords {

    public static void main(String[] args) {
        assert topKFrequent(new String[]{"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"}, 4).containsAll(Arrays.asList("the", "is", "sunny", "day"));
        assert topKFrequent(new String[]{"i", "love", "leetcode", "i", "love", "coding"}, 2).containsAll(Arrays.asList("i", "love"));
    }

    public static List<String> topKFrequent(String[] words, int k) {
        Map<String, Integer> counter = new HashMap<>();
        for (String s : words) {
            if (!counter.containsKey(s)) {
                counter.put(s, 1);
            } else {
                counter.put(s, counter.get(s) + 1);
            }
        }
        PriorityQueue<Map.Entry<String, Integer>> pq = new PriorityQueue<>(
                (a, b) -> Objects.equals(a.getValue(), b.getValue()) ? b.getKey().compareTo(a.getKey()) : a.getValue() - b.getValue()
        );
        for (Map.Entry<String, Integer> entry : counter.entrySet()) {
            pq.offer(entry);
            if (pq.size() > k)
                pq.poll();
        }
        List<String> result = new ArrayList<>();
        while (!pq.isEmpty()) {
            result.add(0, pq.poll().getKey());
        }
        return result;
    }

}
