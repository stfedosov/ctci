package Leetcode;

import java.util.Stack;

/**
 * @author sfedosov on 8/7/19.
 */
public class LongestAbsoluteFilePath {

    public static void main(String[] args) {
        assert lengthLongestPath("dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext") == 20;
        assert lengthLongestPath("dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext") == 32;
        assert lengthLongestPath("a") == 0;
        assert lengthLongestPath("dir\n\tsubdir1") == 0;
        assert lengthLongestPath("file name with  space.txt") == 25;
        assert lengthLongestPath("a\n\tb\n\t\tc") == 0;
        assert lengthLongestPath("a\n\tb.txt\na2\n\tb2.txt") == 9;
        assert lengthLongestPath("a\n\taa\n\t\taaa\n\t\t\tfile1234567890123.txt\naaaaaaaaaaaaaaaaaaaaa\n\tsth.png") == 30;
        assert lengthLongestPath("a\n\taa\n\t\taaa\n\t\t\tfile1234567890123.txt\naaaaaaaaaaaaaaaaaaaaa\n\tsth.png") == 30;
    }

    public static int lengthLongestPath(String input) {
        int max = 0;
        String[] dirs = input.split("\n");
        Stack<Integer> stack = new Stack<>(); // stores the length of dir/file till the current level

        for (String dir : dirs) {
            int numOfTabs = dir.lastIndexOf('\t') + 1;
            int currentLength = dir.length() - numOfTabs + 1;// we treat it as dir/

            while (numOfTabs < stack.size()) {
                stack.pop();
            }

            if (!stack.isEmpty()) {
                currentLength += stack.peek();
            }
            stack.push(currentLength);

            if (dir.contains(".")) {
                max = Math.max(max, stack.pop() - 1);
            }
        }

        return max;
    }

}
