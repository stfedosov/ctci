package Leetcode;

import java.util.Stack;

/**
 * @author sfedosov on 6/6/19.
 */
class MyQueue {

    private static Stack<Integer> stack;
    private static int peek;

    /**
     * Initialize your data structure here.
     */
    public MyQueue() {
        this.stack = new Stack<>();
    }

    /**
     * Push element x to the back of queue.
     */
    public static void push(int x) {
        if (stack.isEmpty()) {
            peek = x;
        }
        stack.push(x);
    }

    /**
     * Removes the element from in front of queue and returns that element.
     */
    public int pop() {
        return popHeadFromTheStack();
    }

    private static int popHeadFromTheStack() {
        if (stack.size() == 1) return stack.pop();
        int s = stack.pop();
        int result = popHeadFromTheStack();
        push(s);
        return result;
    }

    /**
     * Get the front element.
     */
    public int peek() {
        return peek;
    }

    /**
     * Returns whether the queue is empty.
     */
    public boolean empty() {
        return stack.isEmpty();
    }

    public static void main(String[] args) {
        MyQueue obj = new MyQueue();
        obj.push(1);
        obj.push(2);
        assert obj.peek() == 1;
        assert obj.pop() == 1;
        assert obj.peek() == 2;
    }
}