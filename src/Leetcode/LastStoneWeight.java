package Leetcode;

import java.util.Comparator;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @author sfedosov on 8/20/19.
 */
public class LastStoneWeight {

    public static void main(String[] args) {
        assert lastStoneWeight(new int[]{2, 7, 4, 1, 8, 1}) == 1;
        assert lastStoneWeight(new int[]{2, 2}) == 0;
    }

    public static int lastStoneWeight(int[] stones) {
        Queue<Integer> q = new PriorityQueue<>(Comparator.reverseOrder());
        for (int stone : stones) {
            q.offer(stone);
        }
        while (q.size() > 1) {
            Integer a = q.poll();
            Integer b = q.poll();
            if (!Objects.equals(a, b)) {
                q.add(Math.abs(a - b));
            }
        }
        return q.isEmpty() ? 0 : q.poll();
    }


}
