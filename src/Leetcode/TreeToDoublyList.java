package Leetcode;

public class TreeToDoublyList {

    public static void main(String[] args) {
        Node bst = new Node(4);
        Node left = new Node(2);
        bst.left = left;
        bst.right = new Node(5);
        left.left = new Node(1);
        left.right = new Node(3);

        //          4
        //        /   \
        //       2     5
        //      / \
        //     1   3

        Node result = treeToDoublyList(bst);

        // 1 -> 2 -> 3 -> 4 -> 5
        Node tmp1 = result;
        StringBuilder sb1 = new StringBuilder();
        while (tmp1 != null) {
            if (sb1.indexOf(String.valueOf(tmp1.val)) != -1) break;
            sb1.append(tmp1.val);
            tmp1 = tmp1.right;
        }

        // 5 <- 4 <- 3 <- 2 <- 1
        Node tmp2 = result.left;
        StringBuilder sb2 = new StringBuilder();
        while (tmp2 != null) {
            if (sb2.indexOf(String.valueOf(tmp2.val)) != -1) break;
            sb2.append(tmp2.val);
            tmp2 = tmp2.left;
        }

        assert sb1.toString().equals(sb2.reverse().toString());
    }

    static Node head = null;
    static Node tmp = null;

    public static Node treeToDoublyList(Node root) {
        convert(root);
        if (tmp == null) return null;
        tmp.right = head;
        head.left = tmp;
        return head;
    }

    private static void convert(Node root) {
        if (root == null) return;
        convert(root.left);
        if (head == null) {
            head = new Node(root.val);
            tmp = head;
        } else {
            Node n = new Node(root.val);
            n.left = tmp;
            tmp.right = n;
            tmp = tmp.right;
        }
        convert(root.right);
    }

    static class Node {
        public int val;
        public Node left;
        public Node right;

        public Node() {
        }

        public Node(int _val) {
            val = _val;
        }

        public Node(int _val, Node _left, Node _right) {
            val = _val;
            left = _left;
            right = _right;
        }
    }

}
