package Leetcode;

/**
 * @author sfedosov on 1/28/19.
 */
public class StringWithoutAAABBB {

    public static void main(String[] args) {
        assert !strWithout3a3b(4, 1).contains("aaa") && !strWithout3a3b(4, 1).contains("bbb");
        assert !strWithout3a3b(1, 3).contains("aaa") && !strWithout3a3b(1, 3).contains("bbb");
        assert !strWithout3a3b(3, 3).contains("aaa") && !strWithout3a3b(3, 3).contains("bbb");
        assert !strWithout3a3b(2, 5).contains("aaa") && !strWithout3a3b(2, 5).contains("bbb");
    }

    private static String strWithout3a3b(int A, int B) {
        StringBuilder sb = new StringBuilder();
        while (A + B > 0) {
            int length = sb.length();
            char last = length >= 2 ? sb.charAt(length - 1) : '\0';
            char penult = length >= 2 ? sb.charAt(length - 2) : '\0';
            if (last == penult && last == 'a') {
                sb.append('b');
                B--;
            } else if (last == penult && last == 'b') {
                sb.append('a');
                A--;
            } else if (A > B) {
                sb.append('a');
                A--;
            } else {
                sb.append('b');
                B--;
            }
        }
        return sb.toString();
    }

}
