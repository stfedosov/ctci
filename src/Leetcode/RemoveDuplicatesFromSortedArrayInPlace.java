package Leetcode;

import java.util.Arrays;

public class RemoveDuplicatesFromSortedArrayInPlace {

    public static void main(String[] args) {
        int[] nums = {1, 1, 2};
        int i = removeDuplicates(nums);
        assert i == 2;
        assert Arrays.equals(Arrays.copyOfRange(nums, 0, i), new int[]{1, 2});
    }

    public static int removeDuplicates(int[] nums) {
        if (nums.length == 0) return 0;
        int index = 1;

        for (int i = 1; i < nums.length; i++) {
            if (nums[i - 1] != nums[i]) {
                nums[index] = nums[i];
                index++;
            }
        }
        return index;
    }

}
