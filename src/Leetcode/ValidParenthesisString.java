package Leetcode;

/**
 * @author sfedosov on 7/30/19.
 */
public class ValidParenthesisString {

    public static void main(String[] args) {
        assert !checkValidString("(())((())()()(*)(*()(())())())()()((()())((()))(*");
        assert !checkValidString(")(");
        assert checkValidString("(*)");
    }

    private static boolean checkValidString(String s) {
        return checkValidString(s, 0, 0);
    }

    private static boolean checkValidString(String s, int index, int open) {
        if (index == s.length()) {
            return open == 0;
        }
        if (s.charAt(index) == '(') {
            return checkValidString(s, index + 1, open + 1);
        } else if (s.charAt(index) == ')') {
            return open != 0 && checkValidString(s, index + 1, open - 1);
        } else {
            return checkValidString(s, index + 1, open)
                    || checkValidString(s, index + 1, open + 1)
                    || open != 0 && checkValidString(s, index + 1, open - 1);
        }
    }

    public boolean checkValidStringMemo(String s) {
        Boolean[][] memo = new Boolean[s.length()][s.length()];
        return checkValidStringMemo(s, 0, 0, memo);
    }

    private boolean checkValidStringMemo(String s, int index, int open, Boolean[][] memo) {
        if (index == s.length()) {
            return open == 0;
        }
        if (memo[index][open] != null) return memo[index][open];
        if (s.charAt(index) == '(') {
            memo[index][open] = checkValidStringMemo(s, index + 1, open + 1, memo);
        } else if (s.charAt(index) == ')') {
            memo[index][open] = open != 0 && checkValidStringMemo(s, index + 1, open - 1, memo);
        } else {
            memo[index][open] = checkValidStringMemo(s, index + 1, open, memo)
                    || checkValidStringMemo(s, index + 1, open + 1, memo)
                    || open != 0 && checkValidStringMemo(s, index + 1, open - 1, memo);
        }
        return memo[index][open];
    }

}
