package Leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LetterCombinationOfAPhoneNumber {

    public static void main(String[] args) {
        System.out.println(letterCombinations("23"));
    }

    public static List<String> letterCombinations(String digits) {
        if (digits.isEmpty()) return Collections.emptyList();
        String[] str = new String[]{"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
        List<String> result = new ArrayList<>();
        letterCombinationsRecursive(str, "", result, digits, 0);
        return result;
    }

    // O(N*4^N) 4 is referring to the max length in the map
    // O(N) due to recursion stack
    private static void letterCombinationsRecursive(String[] str, String current, List<String> list, String digits, int index) {
        if (index == digits.length()) {
            list.add(current);
            return;
        }
        for (char c : str[digits.charAt(index) - '0'].toCharArray()) {
            letterCombinationsRecursive(str, current + c, list, digits, index + 1);
        }
    }

}
