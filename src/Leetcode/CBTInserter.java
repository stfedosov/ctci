package Leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author sfedosov on 8/12/20.
 */
public class CBTInserter {

    private List<TreeNode> list = new ArrayList<>();

    public CBTInserter(TreeNode root) {
        Queue<TreeNode> q = new LinkedList<>();
        q.offer(root);
        inorder(root, q);
    }

    private void inorder(TreeNode root, Queue<TreeNode> q) {
        while (!q.isEmpty()) {
            TreeNode tree = q.poll();
            list.add(tree);
            if (tree.left != null) q.offer(tree.left);
            if (tree.right != null) q.offer(tree.right);
        }
    }

    public int insert(int v) {
        TreeNode newTree = new TreeNode(v);
        TreeNode parent = list.get((list.size() - 1)/ 2);
        if (parent.left == null) parent.left = newTree;
        else parent.right = newTree;
        list.add(newTree);
        return parent.val;
    }

    public TreeNode get_root() {
        return list.get(0);
    }
}