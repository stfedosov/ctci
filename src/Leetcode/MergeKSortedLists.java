package Leetcode;

import Leetcode.RotateList.ListNode;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class MergeKSortedLists {

    public static void main(String[] args) {
        ListNode root = new ListNode(1);
        ListNode l4 = new ListNode(4);
        root.next = l4;
        l4.next = new ListNode(5); // 1 -> 4 -> 5

        ListNode root2 = new ListNode(1);
        ListNode l3 = new ListNode(3);
        root2.next = l3;
        l3.next = new ListNode(4); // 1 -> 3 -> 4

        ListNode root3 = new ListNode(2);
        root3.next = new ListNode(6); // 2 -> 6

        System.out.println(mergeKLists(new ListNode[]{root, root2, root3})); // 1 -> 1 -> 2 -> 3 -> 4 -> 4 -> 5 -> 6

        int[][] arrays = new int[][]{{99, 100}, {101, 102}, {103, 104}};
        System.out.println(Arrays.toString(merge(arrays)));
    }

    public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode h = new ListNode(0);
        ListNode ans = h;
        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                h.next = l1;
                l1 = l1.next;
            } else {
                h.next = l2;
                l2 = l2.next;
            }
            h = h.next;
        }
        if (l1 == null) {
            h.next = l2;
        }
        if (l2 == null) {
            h.next = l1;
        }
        return ans.next;
    }

    public static ListNode mergeKLists2(ListNode[] lists) {
        if (lists.length == 1) {
            return lists[0];
        }
        if (lists.length == 0) {
            return null;
        }
        ListNode head = mergeTwoLists(lists[0], lists[1]);
        for (int i = 2; i < lists.length; i++) {
            head = mergeTwoLists(head, lists[i]);
        }
        return head;
    }

    // ================================================

    public static ListNode mergeKLists(ListNode[] lists) {
        Queue<ListNode> heap = new PriorityQueue<>(Comparator.comparingInt(l -> l.val));
        ListNode head = new ListNode(0), tail = head;
        for (ListNode node : lists) {
            if (node != null)
                heap.offer(node);
        }

        while (!heap.isEmpty()) {
            tail.next = heap.poll();
            tail = tail.next;
            if (tail.next != null) heap.offer(tail.next);
        }
        return head.next;
    }

    static class QueueNode {
        int array, index, value;

        public QueueNode(int array, int index, int value) {
            this.array = array;
            this.index = index;
            this.value = value;
        }
    }

    public static int[] merge(int[][] arrays) {
        Queue<QueueNode> pq = new PriorityQueue<>(Comparator.comparingInt(a -> a.value));

        int size = 0;
        for (int i = 0; i < arrays.length; i++) {
            size += arrays[i].length;
            if (arrays[i].length > 0) {
                pq.add(new QueueNode(i, 0, arrays[i][0]));
            }
        }

        int[] result = new int[size];
        int i = 0;
        while (!pq.isEmpty()) {
            QueueNode n = pq.poll();
            result[i++] = n.value;
            int newIndex = n.index + 1;
            if (newIndex < arrays[n.array].length) {
                pq.add(new QueueNode(n.array, newIndex, arrays[n.array][newIndex]));
            }
        }

        return result;
    }


}
