package Leetcode;

public class ReorderList {

    public static void main(String[] args) {
        ListNode listNode = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode3 = new ListNode(3);
        listNode3.next = new ListNode(4);
        listNode2.next = listNode3;
        listNode.next = listNode2;
        System.out.println(listNode); // 1 -> 4 -> 2 -> 3
        reorderList(listNode);
        System.out.println(listNode); //1 -> 5 -> 2 -> 4 -> 3
    }

    public static void reorderList(ListNode head) {
        if (head == null || head.next == null) return;
        ListNode slow = head, fast = head;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        ListNode secondHalf = slow.next;
        slow.next = null; // ensure we reverse only a second part of the list, not the entire
        secondHalf = reverse(secondHalf);
        // Step 3: Merge the two halves alternately.
        ListNode firstHalf = head;
        while (secondHalf != null) {
            // Save next pointers.
            ListNode nextFirstHalf = firstHalf.next;
            ListNode nextSecondHalf = secondHalf.next;

            // Merge one node from second half after one node from first half.
            firstHalf.next = secondHalf;
            secondHalf.next = nextFirstHalf;

            // Move forward in both lists.
            firstHalf = nextFirstHalf;
            secondHalf = nextSecondHalf;
        }
    }

    private static ListNode reverse(ListNode head) {
        ListNode prev = null, current = head;
        while (current != null) {
            ListNode next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        return prev;
    }

    public static class ListNode {
        public int val;
        public ListNode next;

        public ListNode(int x) {
            val = x;
        }

        @Override
        public String toString() {
            return "ListNode{" +
                    "val=" + val +
                    ", next=" + next +
                    '}';
        }
    }

}
