package Leetcode;

/**
 * @author sfedosov on 7/24/19.
 */
public class BSTOperations {


    // Function to perform inorder traversal of the BST
    private static void inorder(TreeNode root) {
        if (root == null) {
            return;
        }

        inorder(root.left);
        System.out.print(root.val + " ");
        inorder(root.right);
    }

    // Helper function to find maximum value node in subtree rooted at ptr
    private static TreeNode maximumKey(TreeNode ptr) {
        while (ptr.right != null) {
            ptr = ptr.right;
        }
        return ptr;
    }

    // Recursive function to insert an key into BST
    public static TreeNode insert(TreeNode root, int key) {
        // if the root is null, create a new node an return it
        if (root == null) {
            return new TreeNode(key);
        }

        // if given key is less than the root node, recur for left subtree
        if (key < root.val) {
            root.left = insert(root.left, key);
        }

        // if given key is more than the root node, recur for right subtree
        else {
            root.right = insert(root.right, key);
        }

        return root;
    }

    // Function to delete node from a BST
    private static TreeNode deleteNode(TreeNode root, int key) {
        // base case: key not found in tree
        if (root == null) {
            return null;
        }

        // if given key is less than the root node, recur for left subtree
        if (key < root.val) {
            root.left = deleteNode(root.left, key);
        }

        // if given key is more than the root node, recur for right subtree
        else if (key > root.val) {
            root.right = deleteNode(root.right, key);
        }

        // key found
        else {
            // Case 1: node to be deleted has no children (it is a leaf node)
            if (root.left == null && root.right == null) {
                // update root to null
                return null;
            }

            // Case 2: node to be deleted has two children
            else if (root.left != null && root.right != null) {
                // find its in-order predecessor node
                TreeNode predecessor = maximumKey(root.left);

                // Copy the value of predecessor to current node
                root.val = predecessor.val;

                // recursively delete the predecessor. Note that the
                // predecessor will have at-most one child (left child)
                root.left = deleteNode(root.left, predecessor.val);
            }

            // Case 3: node to be deleted has only one child
            else {
                // find child node
                root = (root.left != null) ? root.left : root.right;
            }
        }

        return root;
    }

    // main function
    public static void main(String[] args) {
        TreeNode root = null;
        int[] keys = {15, 10, 20, 8, 12, 25};

        for (int key : keys) {
            root = insert(root, key);
        }

        root = deleteNode(root, 12);
        inorder(root);
    }


}
