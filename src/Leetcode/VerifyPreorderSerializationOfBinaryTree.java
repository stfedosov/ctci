package Leetcode;

/**
 * @author sfedosov on 2/18/18.
 */
public class VerifyPreorderSerializationOfBinaryTree {

    public static void main(String[] args) {
        System.out.println(isValidSerialization("9,#"));
        System.out.println(isValidSerialization("9,3,4,#,#,1,#,#,2,#,6,#,#"));
    }

    public static boolean isValidSerialization(String preorder) {
        String[] nodes = preorder.split(",");
        int diff = 1;
        for (String node: nodes) {
            if (--diff < 0) return false;
            if (!node.equals("#")) diff += 2;
        }
        return diff == 0;
    }

}
