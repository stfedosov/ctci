package Leetcode;


import java.util.LinkedList;
import java.util.Queue;

public class DecodeString {

    public static void main(String[] args) {
        assert decodeString("3[a2[c]]").equals("accaccacc");
        assert decodeString("3[a]2[bc]").equals("aaabcbc");
        assert decodeString("3[a]2[bc]").equals("aaabcbc");
        assert decodeString("2[abc]3[cd]ef").equals("abcabccdcdcdef");
        assert decodeString("3[a]2[b4[F]c]").equals("aaabFFFFcbFFFFc");
        assert decodeString("3[z]2[2[y]pq4[2[jk]e1[f]]]ef").equals("zzzyypqjkjkefjkjkefjkjkefjkjkefyypqjkjkefjkjkefjkjkefjkjkefef");
    }

    public static String decodeString(String s) {
        Queue<Character> q = new LinkedList<>();
        for (char c : s.toCharArray()) {
            q.offer(c);
        }
        return decodeRecur(q);
    }

    private static String decodeRecur(Queue<Character> q) {
        int num = 0;
        StringBuilder tmp = new StringBuilder();
        while (!q.isEmpty()) {
            char c = q.poll();
            if (Character.isLetter(c)) tmp.append(c);
            else if (Character.isDigit(c)) num = num * 10 + (c - '0');
            else if (c == ']') break;
            else {
                String next = decodeRecur(q);
                while (num > 0) {
                    tmp.append(next);
                    num--;
                }
            }
        }
        return tmp.toString();
    }

}