package Leetcode;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @author sfedosov on 8/16/19.
 */
public class RemoveKDigits {

    public static void main(String[] args) {
        assert removeKdigits("1432219", 3).equals("1219");
        assert removeKdigits("10200", 1).equals("200");
    }

    public static String removeKdigits(String num, int k) {
        if (num == null || num.length() == 0 || num.length() == k) return "0";
        Deque<Character> nums = new LinkedList<>();
        char[] digits = num.toCharArray();
        for (char digit : digits) {
            while (!nums.isEmpty() && k > 0 && digit < nums.peekLast()) {
                nums.pollLast();
                k--;
            }
            nums.offer(digit);
        }

        while (k > 0) {
            nums.pollLast();
            k--;
        }

        while (!nums.isEmpty() && nums.peek() == '0') {
            nums.poll();
        }

        StringBuilder sb = new StringBuilder();
        while (!nums.isEmpty()) {
            sb.append(nums.poll());
        }

        return sb.length() == 0 ? "0" : sb.toString();
    }
}
