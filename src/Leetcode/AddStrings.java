package Leetcode;

public class AddStrings {

    public static void main(String[] args) {
        assert addStrings("9", "9").equals("18");
        assert addStrings("1", "9").equals("10");
    }

    public static String addStrings(String num1, String num2) {
        int i = num1.length() - 1, j = num2.length() - 1, carry = 0;
        StringBuilder sb = new StringBuilder();
        while (i >= 0 || j >= 0) {
            int sum = carry;

            if (j >= 0) {
                sum += num2.charAt(j--) - '0';
            }
            if (i >= 0) {
                sum += num1.charAt(i--) - '0';
            }
            carry = sum / 10;
            sb.insert(0, sum % 10);
        }
        if (carry != 0) {
            sb.insert(0, carry);
        }
        return sb.toString();
    }

}
