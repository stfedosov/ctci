package Leetcode;

public class JumpGame2 {

    public static void main(String[] args) {
        assert jump(new int[]{2, 3, 0, 1, 4}) == 2;
        assert jump(new int[]{2, 3, 1, 1, 4}) == 2;
        assert jump(new int[]{1, 2}) == 1;
        assert jumpMemo(new int[]{2, 3, 0, 1, 4}) == 2;
        assert jumpMemo(new int[]{2, 3, 1, 1, 4}) == 2;
        assert jumpMemo(new int[]{1, 2}) == 1;
        assert jumpMemo(new int[]{5, 6, 4, 4, 6, 9, 4, 4, 7, 4, 4, 8, 2, 6, 8, 1, 5, 9, 6, 5, 2, 7, 9, 7, 9, 6, 9, 4, 1, 6, 8, 8, 4, 4, 2, 0, 3, 8, 5}) == 5;
        assert jump(new int[]{4, 1, 1, 3, 1, 1, 1}) == 2;
        assert jumpMemo(new int[]{4, 1, 1, 3, 1, 1, 1}) == 2;
    }

    public static int jump(int[] nums) {
        return jump(nums, 0);
    }

    // ----- Non-cached

    public static int jump(int[] nums, int index) {
        if (index >= nums.length - 1) return 0;
        int minSteps = nums.length;
        for (int i = 1; i <= nums[index]; i++) {
            minSteps = Math.min(minSteps, jump(nums, index + i));
        }
        return minSteps + 1;
    }

    // ----- Cached

    public static int jumpMemo(int[] nums) {
        return jumpMemo(nums, 0, new Integer[10000]);
    }

    public static int jumpMemo(int[] nums, int index, Integer[] memo) {
        if (memo[index] != null) return memo[index];
        if (index >= nums.length - 1) return 0;
        int minSteps = nums.length;
        for (int i = 1; i <= nums[index]; i++) {
            minSteps = Math.min(minSteps, jumpMemo(nums, index + i, memo));
        }
        memo[index] = minSteps + 1;
        return memo[index];
    }
}
