package Leetcode;

/**
 * @author sfedosov on 4/16/18.
 */
public class NumberOfIslands {

    public static void main(String[] args) {
        char[][] grid = new char[][]{
                {'1', '1', '0', '0', '0'},
                {'1', '1', '0', '0', '0'},
                {'0', '0', '1', '0', '0'},
                {'0', '0', '0', '1', '1'}
        };
        assert numIslands(grid) == 3;

        char[][] grid2 = new char[][]{
                {'1', '1', '1', '1', '0'},
                {'1', '1', '0', '1', '0'},
                {'1', '1', '0', '0', '0'},
                {'0', '0', '0', '0', '0'}
        };
        assert numIslands(grid2) == 1;

    }

    public static int numIslands(char[][] grid) {
        int count = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                count += dfs(grid, i, j);
            }
        }
        return count;
    }

    private static int dfs(char[][] binaryMatrix, int i, int j) {
        if (j < 0
                || i < 0
                || i >= binaryMatrix.length
                || j >= binaryMatrix[0].length
                || binaryMatrix[i][j] == '0') {
            return 0;
        } else {
            binaryMatrix[i][j] = '0';
            dfs(binaryMatrix, i, j - 1);
            dfs(binaryMatrix, i - 1, j);
            dfs(binaryMatrix, i + 1, j);
            dfs(binaryMatrix, i, j + 1);
            return 1;
        }
    }

}
