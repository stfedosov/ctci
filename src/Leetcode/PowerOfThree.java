package Leetcode;

/**
 * @author sfedosov on 10/21/19.
 */
public class PowerOfThree {

    public static void main(String[] args) {
        assert isPowerOfThreeBruteForce(27);
        assert isPowerOfThreeBruteForce(81);
        assert !isPowerOfThreeBruteForce(45);

        assert isPowerOfThreeOptimized(27);
        assert isPowerOfThreeOptimized(81);
        assert !isPowerOfThreeOptimized(45);
    }

    public static boolean isPowerOfThreeBruteForce(int n) {
        if (n != 1 && n % 3 != 0) return false;
        int x = 1;
        while (x < n) {
            x *= 3;
        }
        return x == n;
    }

    // log 10 (n) / log 10 (3) = log 3 (n)
    // 3 ^ log 3 (n) = n

    public static boolean isPowerOfThreeOptimized(int n) {
        int number = (int) (Math.log10(n) / Math.log10(3));
        int value = (int) Math.pow(3, number);
        return n > 0 && value == n;
    }

}
