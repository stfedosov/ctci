package Leetcode;

import Leetcode.RotateList.ListNode;

/**
 * @author sfedosov on 1/2/19.
 */
public class PartitionList {

    public static void main(String[] args) {
        ListNode listNode = new ListNode(1);
        ListNode listNode4 = new ListNode(4);
        ListNode listNode3 = new ListNode(3);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode5 = new ListNode(5);
        ListNode listNode22 = new ListNode(2);
        listNode.next = listNode4;
        listNode4.next = listNode3;
        listNode3.next = listNode2;
        listNode2.next = listNode5;
        listNode5.next = listNode22;
        partition2(listNode, 3);
        partition(listNode, 3);
        partition(new ListNode(1), 0);
        partition2(new ListNode(1), 0);
    }

    public static ListNode partition(ListNode head, int x) {
        ListNode minimums = null;
        ListNode maximums = null;
        ListNode current = head;
        while (current != null) {
            if (current.val >= x) {
                maximums = addTo(maximums, current);
            } else {
                minimums = addTo(minimums, current);
            }
            current = current.next;
        }
        if (minimums != null) {
            ListNode minCurrent = minimums;
            while (minCurrent.next != null) {
                minCurrent = minCurrent.next;
            }
            minCurrent.next = maximums;
            return minimums;
        } else {
            return maximums;
        }
    }

    private static ListNode addTo(ListNode head, ListNode toAdd) {
        ListNode current = head;
        if (current == null) {
            current = new ListNode(toAdd.val);
            return current;
        }
        while (current.next != null) {
            current = current.next;
        }
        current.next = new ListNode(toAdd.val);
        return head;
    }

    public static ListNode partition2(ListNode head, int x) {
        ListNode less = new ListNode(0);
        ListNode greater = new ListNode(0);
        ListNode curr1 = less, curr2 = greater;

        while (head != null) {
            if (head.val < x) {
                curr1.next = new ListNode(head.val);
                curr1 = curr1.next;
            } else {
                curr2.next = new ListNode(head.val);
                curr2 = curr2.next;
            }
            head = head.next;
        }
        curr1.next = greater.next;
        return less.next;
    }

}
