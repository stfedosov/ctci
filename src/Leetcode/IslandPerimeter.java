package Leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sfedosov on 8/27/19.
 */
public class IslandPerimeter {

    public static int islandPerimeter(int[][] grid) {
        boolean[][] visited = new boolean[grid.length][grid[0].length];
        List<Node> list = new ArrayList<>();
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (!visited[i][j] && grid[i][j] == 1) {
                    dfs(i, j, grid, list, visited);
                }
            }
        }
        int result = 0;
        for (Node node : list) {
            result += node.up;
            result += node.left;
            result += node.right;
            result += node.down;
        }
        return result;
    }

    private static void dfs(int i, int j, int[][] grid, List<Node> list, boolean[][] visited) {
        if (i < 0 || j < 0 || i >= grid.length || j >= grid[i].length || grid[i][j] == 0 || visited[i][j]) {
            return;
        }
        visited[i][j] = true;
        list.add(new Node(i == 0 || grid[i - 1][j] == 0 ? 1 : 0,
                j == 0 || grid[i][j - 1] == 0 ? 1 : 0,
                j == grid[i].length - 1 || j + 1 < grid[i].length && grid[i][j + 1] == 0 ? 1 : 0,
                i == grid.length - 1 || i + 1 < grid.length && grid[i + 1][j] == 0 ? 1 : 0));
        dfs(i + 1, j, grid, list, visited);
        dfs(i - 1, j, grid, list, visited);
        dfs(i, j + 1, grid, list, visited);
        dfs(i, j - 1, grid, list, visited);
    }

    static class Node {
        int up, left, right, down;

        Node(int up, int left, int right, int down) {
            this.up = up;
            this.left = left;
            this.right = right;
            this.down = down;
        }
    }

}
