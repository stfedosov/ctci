package Leetcode;

import Leetcode.RotateList.ListNode;

// Linked lsts are same size, but stored in reverse order
public class AddTwoNumbers {

    public static void main(String[] args) {
        ListNode two = new ListNode(2);
        ListNode four = new ListNode(4);
        two.next = four;
        four.next = new ListNode(3);

        ListNode five = new ListNode(5);
        ListNode six = new ListNode(6);
        five.next = six;
        six.next = new ListNode(4);

        assert addTwoNumbers(two, five).toString().equals("ListNode{val=7, next=ListNode{val=0, next=ListNode{val=8, next=null}}}");

        ListNode one = new ListNode(1);
        one.next = new ListNode(8);

        ListNode zero = new ListNode(0);

        assert addTwoNumbers(one, zero).toString().equals("ListNode{val=1, next=ListNode{val=8, next=null}}");
    }

    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        int carry = 0;
        ListNode tmp = new ListNode(0);
        ListNode p = tmp;
        while (l1 != null || l2 != null || carry != 0) {
            if (l1 != null) {
                carry += l1.val;
                l1 = l1.next;
            }
            if (l2 != null) {
                carry += l2.val;
                l2 = l2.next;
            }
            p.next = new ListNode(carry % 10);
            carry /= 10;
            p = p.next;
        }
        return tmp.next;
    }

}
