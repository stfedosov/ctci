package Leetcode;

/**
 * @author sfedosov on 12/18/19.
 */
public class KnightDialer {

    private static int max = 1_000_000_007;

    public static void main(String[] args) {
        assert knightDialer(1) == 10;
        assert knightDialerOptimized(4) ==104;
    }

    // -------------- Brute force recursive solution

    public static int knightDialer(int N) {
        long result = 0L;
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 3; j++)
                result += helper(i, j, N);
        return (int) result % max;
    }

    private static long helper(int i, int j, int N) {
        if (i < 0 || j < 0 || i >= 4 || j >= 3 || i == 3 && j != 1) return 0;
        if (N == 1) return 1;
        return helper(i + 1, j - 2, N - 1) +
                helper(i - 1, j + 2, N - 1) +
                helper(i - 1, j - 2, N - 1) +
                helper(i + 1, j + 2, N - 1) +
                helper(i + 2, j - 1, N - 1) +
                helper(i - 2, j + 1, N - 1) +
                helper(i - 2, j - 1, N - 1) +
                helper(i + 2, j + 1, N - 1);
    }

    // -------------- Optimized cached version

    public static int knightDialerOptimized(int N) {
        long result = 0L;
        long[][][] cache = new long[N + 1][4][3];
        for (int i = 0; i < 4; i++)
            for (int j = 0; j < 3; j++)
                result = (result + helperOptimized(i, j, N, cache)) % max;
        return (int) result % max;
    }

    private static long helperOptimized(int i, int j, int N, long[][][] cache) {
        if (i < 0 || j < 0 || i >= 4 || j >= 3 || i == 3 && j != 1) return 0;
        if (N == 1) return 1;
        if (cache[N][i][j] > 0) return cache[N][i][j];
        cache[N][i][j] = helperOptimized(i + 1, j - 2, N - 1, cache) % max +
                helperOptimized(i - 1, j + 2, N - 1, cache) % max +
                helperOptimized(i - 1, j - 2, N - 1, cache) % max +
                helperOptimized(i + 1, j + 2, N - 1, cache) % max +
                helperOptimized(i + 2, j - 1, N - 1, cache) % max +
                helperOptimized(i - 2, j + 1, N - 1, cache) % max +
                helperOptimized(i - 2, j - 1, N - 1, cache) % max +
                helperOptimized(i + 2, j + 1, N - 1, cache) % max;
        return cache[N][i][j];
    }


}
