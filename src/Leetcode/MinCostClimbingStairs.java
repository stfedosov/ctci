package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 11/2/19.
 */
public class MinCostClimbingStairs {

    public static void main(String[] args) {
        assert minCostClimbingStairs(new int[]{10, 15, 20}) == 15;
        assert minCostClimbingStairs(new int[]{1, 100, 1, 1, 1, 100, 1, 1, 100, 1}) == 6;
    }

    public static int minCostClimbingStairs(int[] cost) {
        int[] cache = new int[cost.length + 1];
        Arrays.fill(cache, -1);
        return Math.min(minCostClimbingStairs(cost, 0, cache), minCostClimbingStairs(cost, 1, cache));
    }

    private static int minCostClimbingStairs(int[] cost, int index, int[] cache) {
        if (index >= cost.length - 2) return cost[index];
        if (cache[index] != -1) return cache[index];
        cache[index] = cost[index] + Math.min(
                minCostClimbingStairs(cost, index + 1, cache),
                minCostClimbingStairs(cost, index + 2, cache));
        return cache[index];
    }

}
