package Leetcode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class LongestWord {

    public static void main(String[] args) {
        assert longestWordNonOptimal(new String[]{"w", "wo", "wor", "worl", "world"}).equals("world");
        assert longestWordNonOptimal(new String[]{"a", "banana", "app", "appl", "ap", "apply", "apple"}).equals("apple");

        assert longestWordOptimal(new String[]{"w", "wo", "wor", "worl", "world"}).equals("world");
        assert longestWordOptimal(new String[]{"a", "banana", "app", "appl", "ap", "apply", "apple"}).equals("apple");
    }

    static class TrieNode {
        TrieNode[] children;
        String word;

        public TrieNode() {
            children = new TrieNode[26];
        }
    }

    static class Trie {
        private TrieNode root;

        public Trie() {
            root = new TrieNode();
        }

        public void insert(String word) {
            TrieNode node = root;
            for (int i = 0; i < word.length(); i++) {
                int idx = word.charAt(i) - 'a';
                if (node.children[idx] == null) {
                    node.children[idx] = new TrieNode();
                }
                node = node.children[idx];
            }
            node.word = word;
        }

        // Time/space: O(sum of all words lengths)
        public String findLongestWord() {
            String result = null;
            Queue<TrieNode> queue = new LinkedList<>();
            queue.offer(root);
            while (!queue.isEmpty()) {
                int size = queue.size();
                for (int i = 0; i < size; i++) {
                    TrieNode node = queue.poll();
                    // to ensure smallest lexicographical order
                    for (int j = 25; j >= 0; j--) {
                        if (node.children[j] != null && node.children[j].word != null) {
                            result = node.children[j].word;
                            queue.offer(node.children[j]);
                        }
                    }
                }
            }
            return result == null ? "" : result;
        }
    }

    public static String longestWordOptimal(String[] words) {
        Trie trie = new Trie();
        for (String word : words) {
            trie.insert(word);
        }

        return trie.findLongestWord();
    }

    // ---------------------------------------------------------------------------------------------
    // =============================================================================================

    // Time: O(n*log(n)), space: O(n)
    public static String longestWordNonOptimal(String[] words) {
        Arrays.sort(words);
        Set<String> built = new HashSet<>();
        String res = "";
        for (String w : words) {
            if (w.length() == 1 || built.contains(w.substring(0, w.length() - 1))) {
                res = w.length() > res.length() ? w : res;
                built.add(w);
            }
        }
        return res;
    }

}
