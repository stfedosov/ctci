package Leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 4/27/20.
 */
public class MaximumPointsObtainFromCards {

    public static void main(String[] args) {
        assert maxScore(new int[]{1, 2, 3, 4, 5, 6, 1}, 3) == 12;
        assert maxScore(new int[]{9, 7, 7, 9, 7, 7, 9}, 7) == 55;
        assert maxScore(new int[]{1, 1000, 1}, 1) == 1;

        assert maxScore2(new int[]{1, 2, 3, 4, 5, 6, 1}, 3) == 12;
        assert maxScore2(new int[]{9, 7, 7, 9, 7, 7, 9}, 7) == 55;
        assert maxScore2(new int[]{1, 1000, 1}, 1) == 1;
    }

    public static int maxScore(int[] cardPoints, int k) {
        return helper(cardPoints, k, 0, 0, new HashMap<>());
    }

    private static int helper(int[] cardPoints, int k, int start, int end, Map<String, Integer> cache) {
        if (k == start + end) return 0;
        String key = start + "," + end;
        if (cache.containsKey(key)) return cache.get(key);
        cache.put(key, Math.max(
                cardPoints[start] + helper(cardPoints, k, start + 1, end, cache),
                cardPoints[cardPoints.length - 1 - end] + helper(cardPoints, k, start, end + 1, cache)));
        return cache.get(key);
    }

    // -- another solution: sliding windows

    public static int maxScore2(int[] cardPoints, int k) {
        int sum = 0;
        for (int i = 0; i < k; i++) sum += cardPoints[i];
        int max = sum, end = 0;
        while (end < k) {
            sum -= cardPoints[k - 1 - end];
            sum += cardPoints[cardPoints.length - 1 - end];
            max = Math.max(max, sum);
            end++;
        }
        return max;
    }

}
