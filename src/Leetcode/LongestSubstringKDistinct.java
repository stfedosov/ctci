package Leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 7/24/19.
 */
public class LongestSubstringKDistinct {

    // Given a string s and an integer k,
    // return the length of the longest substring of s that contains at most k distinct characters.
    public static void main(String[] args) {
        assert lengthOfLongestSubstringKDistinct("aba", 1) == 1;
        assert lengthOfLongestSubstringKDistinct("bacc", 2) == 3;
        assert lengthOfLongestSubstringKDistinct("eceba", 2) == 3;
        assert lengthOfLongestSubstringKDistinct("aa", 1) == 2;
        assert lengthOfLongestSubstringKDistinct("a", 1) == 1;
    }

    public static int lengthOfLongestSubstringKDistinct(String s, int k) {
        if (s.length() < k) return s.length();
        int start = 0, end = 0, max = 0;
        Map<Character, Integer> map = new HashMap<>();
        while (end < s.length()) {
            char c = s.charAt(end);
            map.put(c, map.getOrDefault(c, 0) + 1);
            if (map.size() > k) {
                char begin_c = s.charAt(start);
                Integer count = map.get(begin_c);
                if (count == 1) {
                    map.remove(begin_c);
                } else {
                    map.put(begin_c, count - 1);
                }
                start++;
            }
            max = Math.max(max, end - start + 1);
            end++;
        }
        return max;
    }

}
