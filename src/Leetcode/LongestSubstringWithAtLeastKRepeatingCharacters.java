package Leetcode;

/**
 * @author sfedosov on 4/18/19.
 */
public class LongestSubstringWithAtLeastKRepeatingCharacters {

    public static void main(String[] args) {
        System.out.println(longestSubstring("caaabb", 3));
        System.out.println(longestSubstring("ababbc", 2));
        System.out.println(longestSubstring("weitong", 2));
        System.out.println(longestSubstring("ababacb", 3));
    }

    // Given a string s and an integer k, return the length of the longest substring of s such that the frequency of
    // each character in this substring is greater than or equal to k.
    public static int longestSubstring(String s, int k) {
        int[] map = new int[26];
        for (char c : s.toCharArray()) map[c - 'a']++;
        for (int i = 0; i < s.length(); i++) {
            if (map[s.charAt(i) - 'a'] < k) {
                return Math.max(longestSubstring(s.substring(0, i), k), longestSubstring(s.substring(i + 1), k));
            }
        }
        return s.length();
    }
}
