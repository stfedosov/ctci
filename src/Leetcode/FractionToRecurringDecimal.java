package Leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 10/12/19.
 */
public class FractionToRecurringDecimal {

    public static void main(String[] args) {
        assert fractionToDecimal(1, 6).equals("0.1(6)");
        assert fractionToDecimal(1, 7).equals("0.(142857)");
        assert fractionToDecimal(1, 3).equals("0.(3)");
        assert fractionToDecimal(2, 3).equals("0.(6)");
        assert fractionToDecimal(1, 2).equals("0.5");
        assert fractionToDecimal(4, 333).equals("0.(012)");
        assert fractionToDecimal(1, 17).equals("0.(0588235294117647)");
        assert fractionToDecimal(22, 7).equals("3.(142857)");
        assert fractionToDecimal(-50, 8).equals("-6.25");
        assert fractionToDecimal(7, -12).equals("-0.58(3)");
        assert fractionToDecimal(89, 270).equals("0.3(296)");
        assert fractionToDecimal(7, 12).equals("0.58(3)");
        assert fractionToDecimal(1, 90).equals("0.0(1)");
        assert fractionToDecimal(-1, -2147483648).equals("0.0000000004656612873077392578125");
    }

    public static String fractionToDecimal(int numerator, int denominator) {
        if (numerator == 0) {
            return "0";
        }
        StringBuilder res = new StringBuilder();
        if (numerator < 0 && denominator > 0 || numerator > 0 && denominator < 0) res.append("-");
        long num = Math.abs((long) numerator);
        long den = Math.abs((long) denominator);

        res.append(num / den);
        num %= den;
        if (num == 0) {
            return res.toString();
        }
        res.append(".");
        Map<Long, Integer> map = new HashMap<>();
        map.put(num, res.length());
        while (num != 0) {
            num *= 10;
            res.append(num / den);
            num %= den;
            if (map.containsKey(num)) {
                res.insert(map.get(num), "(");
                res.append(")");
                break;
            } else {
                map.put(num, res.length());
            }
        }
        return res.toString();
    }

}
