package Leetcode;

public class MirrorReflection {

    public static void main(String[] args) {
        assert mirrorReflection(2, 1) == 2;
    }

    /*
    "remain" keeps track of the remaining distance from the north or south mirror.
    Every time that the laser ray touches the east or west mirror,
    it will be q (meters) closer to the north or south mirror,
    and because of that we deduct q every time it touches the east or west edge.
    When "remain" is less than zero, it means that it will touch the north or south mirror in the next step.
    Consequently, the new distance from the south or north mirror is now "remain" (which is a negative value) + p.
    At this point, we also change the direction ("up").
     */

    public static int mirrorReflection(int p, int q) {
        int remain = p;
        boolean up = true;
        boolean east = true;
        while (true) {
            remain -= q;
            if (remain == 0) {
                if (up) {
                    if (east) {
                        return 1;
                    } else {
                        return 2;
                    }
                } else {
                    return 0;
                }
            }
            if (remain < 0) {
                remain += p;
                up = !up;
            }
            east = !east;
        }
    }

}
