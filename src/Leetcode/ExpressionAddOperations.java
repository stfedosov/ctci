package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

/**
 * @author sfedosov on 4/5/20.
 */
public class ExpressionAddOperations {

    public static void main(String[] args) {
        assert addOperators("105", 5).containsAll(Arrays.asList("1*0+5", "10-5"));
        assert addOperators("123", 6).containsAll(Arrays.asList("1+2+3", "1*2*3"));
        assert addOperators("232", 8).containsAll(Arrays.asList("2*3+2", "2+3*2"));
    }

    public static List<String> addOperators(String num, int target) {
        List<String> powerSet = new ArrayList<>();
        generate(num, "", powerSet, 0);
        Set<String> result = new HashSet<>();
        for (List<Long> l : formIntegers(powerSet, target)) dfs(l, target, 0, "", result);
        return new ArrayList<>(result);
    }

    private static void dfs(List<Long> input, int target, int i, String answer, Set<String> result) {
        if (i == input.size()) {
            if (calculate(answer) == target) {
                result.add(answer);
            }
        } else {
            dfs(input, target, i + 1, answer + (i == 0 ? "" : "+") + input.get(i), result);
            dfs(input, target, i + 1, answer + (i == 0 ? "" : "*") + input.get(i), result);
            dfs(input, target, i + 1, answer + (i == 0 ? "" : "-") + input.get(i), result);
        }
    }

    private static int calculate(String input) {
        Queue<Character> q = new LinkedList<>();
        for (char c : input.toCharArray()) q.offer(c);
        q.offer('+');
        return calculate(q);
    }

    private static int calculate(Queue<Character> q) {
        char sign = '+';
        int num = 0;
        Stack<Integer> stack = new Stack<>();
        while (!q.isEmpty()) {
            char c = q.poll();
            if (Character.isDigit(c)) {
                num = num * 10 + (c - '0');
            } else {
                switch (sign) {
                    case '+':
                        stack.push(num);
                        break;
                    case '-':
                        stack.push(-num);
                        break;
                    case '*':
                        stack.push(stack.pop() * num);
                        break;
                }
                sign = c;
                num = 0;
            }
        }
        int sum = 0;
        while (!stack.isEmpty()) sum += stack.pop();
        return sum;
    }

    private static Set<List<Long>> formIntegers(List<String> input, int target) {
        Set<List<Long>> result = new HashSet<>();
        loop:
        for (String s : input) {
            List<Long> tmp = new ArrayList<>();
            for (String spl : s.split(" ")) {
                if (spl.length() > 1 && spl.startsWith("0")) continue loop;
                if (spl.isEmpty()) continue;
                tmp.add(Long.parseLong(spl));
            }
            if (tmp.size() == 1 && tmp.get(0) != target) continue;
            result.add(tmp);
        }
        return result;
    }

    private static void generate(String num, String current, List<String> powerSet, int i) {
        if (i == num.length()) {
            powerSet.add(current);
        } else {
            generate(num, current + " " + num.charAt(i), powerSet, i + 1);
            generate(num, current + num.charAt(i), powerSet, i + 1);
        }
    }

}
