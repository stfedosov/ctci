package Leetcode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WordBreak22 {

    public static void main(String[] args) {
        List<String> dict = new ArrayList<>();
        dict.add("cat");
        dict.add("cats");
        dict.add("and");
        dict.add("sand");
        dict.add("dogs");
        System.out.println(wordBreak("catsanddogs", dict));
        // output: cat sand dogs, cats and dogs
    }

    public static List<String> wordBreak(String s, List<String> wordDict) {
        List<String> result = new ArrayList<>();
        List<List<String>> results = new ArrayList<>();
        wordBreak(s, new HashSet<>(wordDict), result, results);
        return convert(results);
    }

    private static List<String> convert(List<List<String>> results) {
        List<String> toReturn = new ArrayList<>();
        for (List<String> l : results) {
            StringBuilder sb = new StringBuilder();
            for (String x : l) {
                sb.append(x).append(" ");
            }
            toReturn.add(sb.toString().trim());
        }
        return toReturn;
    }

    private static void wordBreak(String s,
                                  Set<String> wordDict,
                                  List<String> current,
                                  List<List<String>> results) {
        if (s.isEmpty()) {
            results.add(new ArrayList<>(current));
            return;
        }
        for (int i = 1; i <= s.length(); i++) {
            String subString = s.substring(0, i);
            if (wordDict.contains(subString)) {
                current.add(subString);
                wordBreak(s.substring(i), wordDict, current, results);
                current.remove(current.size() - 1);
            }
        }
    }

}
