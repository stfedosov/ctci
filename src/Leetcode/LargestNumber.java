package Leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sfedosov on 1/4/20.
 */
public class LargestNumber {

    public static void main(String[] args) {
        assert largestNumber(new int[]{3, 30, 34, 5, 9}).equals("9534330");
        assert largestNumber(new int[]{10, 2}).equals("210");
        assert largestNumber(new int[]{123, 321}).equals("321123");
        assert largestNumber(new int[]{121, 12}).equals("12121");
    }

    public static String largestNumber(int[] nums) {
        List<String> list = new ArrayList<>();
        for (int x : nums) list.add(String.valueOf(x));
        list.sort((a, b) -> (b + a).compareTo(a + b));
        // a = 30, b = 34
        // 3430.compareTo(3034) == 1

        // a = 3, b = 34
        // 343.compareTo(334) == 1

        // a = 30, b = 3
        // 330.compareTo(303) == 1

        StringBuilder result = new StringBuilder();
        for (String xx : list) {
            result.append(xx);
        }
        String res = result.toString();
        while (res.length() >= 2 && res.charAt(0) == '0') {
            res = res.substring(1);
        }
        return res;
    }

}
