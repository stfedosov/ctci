package Leetcode;

public class LongestPalindromeSequence {

    public static void main(String[] args) {
        System.out.println(longestPalindromeSubseq("ABCBA"));
    }

    public static int longestPalindromeSubseq(String s) {
        return find(s, new int[s.length() + 1][s.length() + 1], 0, s.length() - 1);
    }

    // O(n^2)
    private static int find(String word, int[][] memo, int i, int j) {
        if (memo[i][j] != 0) {
            return memo[i][j];
        }
        if (word.charAt(i) == word.charAt(j)) {
            if (i == j) {
                memo[i][j] = 1;
            } else if (i < j) {
                memo[i][j] = 2 + find(word, memo, i + 1, j - 1);
            }
        } else {
            memo[i][j] = Math.max(find(word, memo, i + 1, j), find(word, memo, i, j - 1));
        }
        return memo[i][j];
    }

}
