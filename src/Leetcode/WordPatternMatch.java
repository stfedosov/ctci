package Leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WordPatternMatch {

    /*
    Given a pattern and a string s, return true if s matches the pattern.

    A string s matches a pattern if there is some bijective mapping of single characters to strings
    such that if each character in pattern is replaced by the string it maps to, then the resulting string is s.
    A bijective mapping means that no two characters map to the same string,
    and no character maps to two different strings.
     */

    public static void main(String[] args) {
        assert wordPatternMatch("abab", "redblueredblue");
        assert wordPatternMatch("aaaa", "asdasdasdasd");
        assert !wordPatternMatch("aabb", "xyzabcxzyabc");
    }

    public static boolean wordPatternMatch(String pattern, String s) {
        return wordPatternMatch(s, new ArrayList<>(), pattern);
    }

    private static boolean wordPatternMatch(String s, List<String> tmp, String pattern) {
        if (tmp.size() > pattern.length()) return false;
        else if (s.isEmpty() && tmp.size() == pattern.length()) return sameSequences(tmp, pattern);
        else {
            for (int i = 1; i <= s.length(); i++) {
                tmp.add(s.substring(0, i));
                if (wordPatternMatch(s.substring(i), tmp, pattern)) return true;
                tmp.remove(tmp.size() - 1);
            }
        }
        return false;
    }

    private static boolean sameSequences(List<String> tmp, String pattern) {
        Map index = new HashMap();
        for (int i = 0; i < pattern.length(); i++)
            if (index.put(pattern.charAt(i), i) != index.put(tmp.get(i), i)) return false;
        return true;
    }

}
