package Leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sfedosov on 8/22/19.
 */
public class FileSystem {

    private Directory root;

    public FileSystem() {
        root = new Directory("");
    }

    public List<String> ls(String path) {
        Directory tmp = getDirectory(path);
        List<String> result = new ArrayList<>();
        if (tmp.isFile) {
            result.add(tmp.name);
        } else {
            for (Directory dir : tmp.child.values()) {
                result.add(dir.name);
            }
        }
        result.sort(String::compareTo);
        return result;
    }

    private Directory getDirectory(String paths) {
        Directory tmp = root;
        for (String p : paths.split("/")) {
            if (p.isEmpty()) continue;
            tmp = tmp.child.get(p);
        }
        return tmp;
    }


    public void mkdir(String path) {
        String[] paths = path.split("/");
        Directory tmp = root;
        for (String p : paths) {
            if (p.isEmpty()) continue;
            tmp.child.putIfAbsent(p, new Directory(p));
            tmp = tmp.child.get(p);
        }
    }

    public void addContentToFile(String filePath, String content) {
        String[] paths = filePath.split("/");
        String fileName = paths[paths.length - 1];
        Directory tmp = getDirectory(filePath.substring(0, filePath.length() - fileName.length() - 1));
        Directory directory = tmp.child.get(fileName);
        if (directory == null) {
            directory = new Directory(fileName);
            directory.isFile = true;
        }
        directory.content += content;
        tmp.child.put(fileName, directory);
    }

    public String readContentFromFile(String filePath) {
        return getDirectory(filePath).content;
    }

    static class Directory {
        Map<String, Directory> child;
        String name;
        String content = "";
        boolean isFile;

        Directory(String name) {
            this.name = name;
            this.child = new HashMap<>();
        }
    }

    public static void main(String[] args) {
        FileSystem fs = new FileSystem();
        System.out.println(fs.ls("/"));
        fs.mkdir("/a/b/c");
        fs.addContentToFile("/a/b/c/d", "hello");
        System.out.println(fs.ls("/"));
        System.out.println(fs.readContentFromFile("/a/b/c/d"));

        fs = new FileSystem();
        fs.mkdir("/zijzllb");
        System.out.println(fs.ls("/"));
        System.out.println(fs.ls("/zijzllb"));
        fs.mkdir("/r");
        System.out.println(fs.ls("/"));
        System.out.println(fs.ls("/r"));
        fs.addContentToFile("/zijzllb/hfktg", "d");
        System.out.println(fs.readContentFromFile("/zijzllb/hfktg"));
        System.out.println(fs.ls("/"));
        System.out.println(fs.readContentFromFile("/zijzllb/hfktg"));
        fs = new FileSystem();

        fs.mkdir("/goowmfn");
        System.out.println(fs.ls("/goowmfn"));
        System.out.println(fs.ls("/"));
        fs.mkdir("/z");
        System.out.println(fs.ls("/"));
        System.out.println(fs.ls("/"));
        fs.addContentToFile("/goowmfn/c", "shetopcy");
        System.out.println(fs.ls("/z"));
        System.out.println(fs.ls("/goowmfn/c"));
        System.out.println(fs.ls("/goowmfn"));
    }
}

