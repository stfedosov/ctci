package Leetcode;

public class AtMostOneTypo {

    private final TrieNode root = new TrieNode();

    private static class TrieNode {
        TrieNode[] next = new TrieNode[26];
        boolean isWord;
    }

    public AtMostOneTypo(String... words) {
        for (String word : words) {
            add(word);
        }
    }

    public void add(String word) {
        TrieNode curr = root;
        for (char c : word.toCharArray()) {
            int val = c - 'a';
            if (curr.next[val] == null) {
                curr.next[val] = new TrieNode();
            }
            curr = curr.next[val];
        }
        curr.isWord = true;
    }

    public boolean atMostOneTypo(String word) {
        if (word.isEmpty()) return root.isWord;
        return atMostOneTypo(root, word, 0, 0);
    }

    private boolean atMostOneTypo(TrieNode node, String word, int i, int typos) {
        if (typos > 1) return false;
        if (i == word.length()) return node.isWord;
        int val = word.charAt(i) - 'a';

        for (int ch = 0; ch < 26; ch++) {
            if (node.next[ch] == null) continue;
            if (ch == val && atMostOneTypo(node.next[ch], word, i + 1, typos)
                    || atMostOneTypo(node.next[ch], word, i + 1, typos + 1)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        test("banana", true);
        test("banena", true);
        test("banan", false);
        test("banxnn", false);
        test("", true);
        test("tanana", true);
        test("tinana", false);
        test("bpple", true);
        test("ban", false);
    }

    private static void test(String input, boolean expected) {
        AtMostOneTypo dict = new AtMostOneTypo("apple", "banana", "orange", "");
        boolean actual = dict.atMostOneTypo(input);
        if (actual == expected) {
            System.out.println(actual);
        } else {
            throw new AssertionError(String.format("Expected %b, but actual %b", expected, actual));
        }
    }


}
