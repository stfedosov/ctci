package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReorderLogFiles {

    public static void main(String[] args) {
        assert Arrays.equals(reorderLogFiles(new String[]{"a1 9 2 3 1", "g1 act car", "zo4 4 7", "ab1 off key dog", "a8 act zoo"}),
                new String[]{"g1 act car", "a8 act zoo", "ab1 off key dog", "a1 9 2 3 1", "zo4 4 7"});
        assert Arrays.equals(reorderLogFiles(new String[]{"a1 9 2 3 1","g1 act car","zo4 4 7","ab1 off key dog","a8 act zoo","a2 act car"}),
                new String[]{"a2 act car","g1 act car","a8 act zoo","ab1 off key dog","a1 9 2 3 1","zo4 4 7"});
    }

    public static String[] reorderLogFiles(String[] logs) {
        List<String> digitLogs = new ArrayList<>();
        List<String> letterLogs = new ArrayList<>();
        for (String log : logs) {
            String[] split = log.split(" ");
            if (Character.isDigit(split[1].toCharArray()[0]))
                digitLogs.add(log);
            else
                letterLogs.add(log);
        }
        letterLogs.sort((s1, s2) -> {
            int spacePosS1 = s1.indexOf(" ");
            int spacePosS2 = s2.indexOf(" ");
            int comparison = s1.substring(spacePosS1 + 1).compareTo(s2.substring(spacePosS2 + 1));
            if (comparison == 0)
                return s1.substring(0, spacePosS1 - 1).compareTo(s2.substring(0, spacePosS2 - 1));
            else
                return comparison;
        });
        letterLogs.addAll(digitLogs);
        return letterLogs.toArray(new String[0]);
    }

}
