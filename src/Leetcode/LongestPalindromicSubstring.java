package Leetcode;

public class LongestPalindromicSubstring {

    // Given a string s, return the longest palindromic substring in s.
    public static void main(String[] args) {
        assert longestPalindrome("cbbd").equals("bb");
        assert longestPalindrome("cbbd").equals("bb");
    }

    //-----------------------------------------
    // Expand around center - O(n^2)
    //-----------------------------------------

    public static String longestPalindrome(String s) {
        if (s.length() == 1) {
            return s;
        }
        String res = "";
        for (int i = 0; i < s.length() - 1; i++) {
            String odd = expand(s, i, i);
            String even = expand(s, i, i + 1);
            String longestTmp = "";
            if (odd.length() > even.length()) {
                longestTmp = odd;
            } else {
                longestTmp = even;
            }
            if (longestTmp.length() > res.length()) {
                res = longestTmp;
            }
        }
        return res;
    }

    private static String expand(String s, int start, int end) {
        while (start >= 0 && end < s.length() && s.charAt(start) == s.charAt(end)) {
            start--;
            end++;
        }
        // adjusting only 'start' -> 'start' + 1 because we went out of the loop
        // since 'end' is exclusive in the substring, no need to adjust it!
        return s.substring(start + 1, end);
    }

}
