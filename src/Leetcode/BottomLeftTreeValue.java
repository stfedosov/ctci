package Leetcode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author sfedosov on 3/22/18.
 */
public class BottomLeftTreeValue {

    public static void main(String[] args) {
        TreeNode treeNode = new TreeNode(2);
        treeNode.left = new TreeNode(1);
        treeNode.right = new TreeNode(3);
        assert findBottomLeftValue(treeNode) == 1;

        TreeNode root = new TreeNode(1);
        TreeNode treeNode2 = new TreeNode(2);
        treeNode2.left = new TreeNode(4);
        root.left = treeNode2;
        TreeNode treeNode3 = new TreeNode(3);
        TreeNode treeNode5 = new TreeNode(5);
        treeNode5.left = new TreeNode(7);
        treeNode3.left = treeNode5;
        treeNode3.right = new TreeNode(6);
        root.right = treeNode3;
        assert findBottomLeftValue(root) == 7;
    }

    public static int findBottomLeftValue(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            root = queue.poll();
            if (root.right != null)
                queue.add(root.right);
            if (root.left != null)
                queue.add(root.left);
        }
        return root.val;
    }

}
