package Leetcode;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class MinKnightMoves {

    public static void main(String[] args) {
        assert minKnightMoves(0, 1) == 3;
        assert minKnightMoves(5, 5) == 4;
        System.out.println(minKnightMoves(1, 1));
    }

    static class Node {
        int x, y;

        public Node(int x, int y) {
            this.x = x;
            this.y = y;
        }

        int getId() {
            return x * 1000 + y;
        }

    }

    public static int minKnightMoves(int x, int y) {

        Queue<Node> q = new LinkedList<>();

        Set<Integer> visited = new HashSet<>();
        Node e = new Node(0, 0);
        q.offer(e);
        int level = 0;

        while (!q.isEmpty()) {

            int sizeOfLevel = q.size();

            while (sizeOfLevel > 0) {

                Node node = q.poll();

                int current_x = node.x;
                int current_y = node.y;

                if (current_x == x && current_y == y) {
                    return level;
                }

                if (isValid(current_x + 1, current_y + 2, visited)) {
                    Node node1 = new Node(current_x + 1, current_y + 2);
                    visited.add(node1.getId());
                    q.offer(node1);
                }
                if (isValid(current_x + 2, current_y + 1, visited)) {
                    Node node1 = new Node(current_x + 2, current_y + 1);
                    visited.add(node1.getId());
                    q.offer(node1);
                }
                if (isValid(current_x - 1, current_y - 2, visited)) {
                    Node node1 = new Node(current_x - 1, current_y - 2);
                    visited.add(node1.getId());
                    q.offer(node1);
                }
                if (isValid(current_x - 2, current_y - 1, visited)) {
                    Node node1 = new Node(current_x - 2, current_y - 1);
                    visited.add(node1.getId());
                    q.offer(node1);
                }
                if (isValid(current_x + 1, current_y - 2, visited)) {
                    Node node1 = new Node(current_x + 1, current_y - 2);
                    visited.add(node1.getId());
                    q.offer(node1);
                }
                if (isValid(current_x + 2, current_y - 1, visited)) {
                    Node node1 = new Node(current_x - 2, current_y - 1);
                    visited.add(node1.getId());
                    q.offer(node1);
                }

                if (isValid(current_x - 2, current_y + 1, visited)) {
                    Node node1 = new Node(current_x - 2, current_y + 1);
                    visited.add(node1.getId());
                    q.offer(node1);
                }

                if (isValid(current_x - 1, current_y + 2, visited)) {
                    Node node1 = new Node(current_x - 1, current_y + 2);
                    visited.add(node1.getId());
                    q.offer(node1);
                }
                sizeOfLevel--;
            }
            level++;
        }
        return -1;
    }

    private static boolean isValid(int x, int y, Set<Integer> visited) {
        if ((Math.abs(x) + Math.abs(y) <= 300 && !visited.contains(new Node(x, y).getId()))) return true;
        else return false;
    }

}
