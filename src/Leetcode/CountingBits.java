package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 12/27/17.
 */
public class CountingBits {

    // Given an integer n, return an array of length n + 1 such that for each i (0 <= i <= n),
    // ans[i] is the number of 1's in the binary representation of i.
    public static void main(String[] args) {
        assert Arrays.equals(countBits(2), new int[]{0, 1, 1});
        assert Arrays.equals(countBits(5), new int[]{0, 1, 1, 2, 1, 2});
    }

    // recurrence for this problem is f[i] = f[i / 2] + i % 2.
    public static int[] countBits(int num) {
        int[] bits = new int[num + 1];
        for (int i = 1; i <= num; i++) {
            bits[i] = bits[i >> 1] + (i & 1);
        }
        return bits;
    }

}
