package Leetcode;

/**
 * @author sfedosov on 11/7/19.
 */
public class RectangleArea {

    public static void main(String[] args) {
        assert computeArea(-3, 0, 3, 4, 0, -1, 9, 2) == 45;
    }

    public static int computeArea(int A, int B, int C, int D, int E, int F, int G, int H) {
        long height = (long) Math.min(D, H) - Math.max(B, F),
                width = (long) Math.min(G, C) - Math.max(A, E);
        long w1 = (long) C - A,
                w2 = (long) G - E,
                h1 = (long) D - B,
                h2 = (long) H - F;
        return (int) (w1 * h1 + w2 * h2 - (height < 0 || width < 0 ? 0L : height * width));
    }

}
