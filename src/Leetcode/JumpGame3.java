package Leetcode;

import java.util.HashSet;
import java.util.Set;

public class JumpGame3 {

    public static void main(String[] args) {
        assert canReach(new int[]{4, 2, 3, 0, 3, 1, 2}, 5);
        assert canReach(new int[]{4, 2, 3, 0, 3, 1, 2}, 0);
        assert !canReach(new int[]{3, 0, 2, 1, 2}, 2);
    }

    public static boolean canReach(int[] arr, int start) {
        return canReach(arr, start, new HashSet<>());
    }

    private static boolean canReach(int[] arr, int start, Set<Integer> seen) {
        if (start < 0 || start >= arr.length) return false;
        if (arr[start] == 0) return true;
        if (!seen.add(start)) return false;
        return canReach(arr, start + arr[start], seen) || canReach(arr, start - arr[start], seen);
    }

}
