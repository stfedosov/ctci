package Leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sfedosov on 3/11/19.
 */
public class ClumsyFactorial {

    public static void main(String[] args) {
        System.out.println(clumsy(4));
        System.out.println(clumsy(10));
    }

    public static int clumsy(int N) {
        char[] operations = new char[]{'*', '/', '+', '-'};
        List<Integer> list = new ArrayList<>();
        int prev = N;
        for (int i = N - 1, j = 0; i > 0; i--, j++) {
            char c = operations[j % 4];
            if (c == '*') {
                prev *= i;
            } else if (c == '/') {
                prev /= i;
            } else if (c == '+') {
                list.add(prev);
                prev = i;
            } else {
                list.add(prev);
                prev = -i;
            }
        }
        for (int x : list) {
            prev += x;
        }
        return prev;
    }

}
