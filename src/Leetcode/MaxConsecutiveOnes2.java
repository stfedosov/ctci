package Leetcode;

public class MaxConsecutiveOnes2 {

    // Given a binary array nums,
    // return the maximum number of consecutive 1's in the array if you can flip at most one 0.
    public static void main(String[] args) {
        assert findMaxConsecutiveOnes(new int[]{1, 0, 1, 1, 0}) == 4;
        assert findMaxConsecutiveOnes(new int[]{1, 0, 1, 1, 0, 1}) == 4;
    }

    public static int findMaxConsecutiveOnes(int[] nums) {
        int zeroCount = 0;
        int start = 0;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) zeroCount++;
            while (zeroCount > 1) {
                if (nums[start] == 0) zeroCount--;
                start++;
            }
            max = Math.max(max, i - start + 1);
        }
        return max;
    }

}
