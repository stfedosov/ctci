package Leetcode;

import java.util.HashSet;
import java.util.Set;

/**
 * @author sfedosov on 12/24/19.
 */
public class RobotSimulation {

    public static void main(String[] args) {
        System.out.println(robotSim(new int[]{-2, -1, 8, 9, 6},
                new int[][]{{-1, 3}, {0, 1}, {-1, 5}, {-2, -4}, {5, 4}, {-2, -3}, {5, -1}, {1, -1}, {5, 5}, {5, 2}}));
    }

    /*
    North, direction = 0, directions[direction] = {0, 1}
    East,  direction = 1, directions[direction] = {1, 0}
    South, direction = 2, directions[direction] = {0, -1}
    West,  direction = 3, directions[direction] = {-1, 0}
     */

    private static int[][] directions = new int[][]{{-1, 0}, {0, 1}, {1, 0}, {0, -1}};

    public static int robotSim(int[] commands, int[][] obstacles) {
        Set<String> obstaclesSet = new HashSet<>();
        for (int[] obstacle : obstacles) obstaclesSet.add(obstacle[0] + ":" + obstacle[1]);
        int x = 0, y = 0, direction = 1, result = 0;
        for (int command : commands) {
            if (command == -1) {
                direction++;
                direction %= 4;
            } else if (command == -2) {
                direction--;
                if (direction < 0) direction += 4;
            } else {
                int step = 0;
                while (step < command && !obstaclesSet.contains((x + directions[direction][0]) + ":" + (y + directions[direction][1]))) {
                    x += directions[direction][0];
                    y += directions[direction][1];
                    step++;
                }
            }
            result = Math.max(result, x * x + y * y);
        }
        return result;
    }

}
