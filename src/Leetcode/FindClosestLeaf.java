package Leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

/**
 * @author sfedosov on 3/21/20.
 */
public class FindClosestLeaf {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.right = new TreeNode(3);
        TreeNode left = new TreeNode(2);
        root.left = left;
        TreeNode left1 = new TreeNode(4);
        left.left = left1;
        TreeNode left2 = new TreeNode(5);
        left1.left = left2;
        left2.left = new TreeNode(6);

        /*
        root = [1,2,3,4,null,null,null,5,null,6], k = 2
        Diagram of binary tree:
             1
            / \
           2   3
          /
         4
        /
       5
      /
     6
      Output: 3
      Explanation: The leaf node with value 3 (and not the leaf node with value 6) is nearest to the node with value 2.
         */
        assert findClosestLeaf(root, 2) == 3;

        root = new TreeNode(1);
        root.left = new TreeNode(3);
        root.right = new TreeNode(2);

        /*
        root = [1, 3, 2], k = 1
        Diagram of binary tree:
          1
         / \
        3   2

        Output: 2 (or 3)
        Explanation: Either 2 or 3 is the nearest leaf node to the target of 1.
         */
        assert findClosestLeaf(root, 1) == 3;
    }

    public static int findClosestLeaf(TreeNode root, int k) {
        Map<TreeNode, List<TreeNode>> graph = new HashMap<>();
        Queue<TreeNode> q = new LinkedList<>();
        Queue<TreeNode> q2 = new LinkedList<>();
        q.offer(root);
        while (!q.isEmpty()) {
            int size = q.size();
            while (size-- > 0) {
                TreeNode polled = q.poll();
                if (polled.val == k) q2.offer(polled);
                if (polled.left != null) {
                    graph.computeIfAbsent(polled, x -> new ArrayList<>()).add(polled.left);
                    graph.computeIfAbsent(polled.left, x -> new ArrayList<>()).add(polled);
                    q.offer(polled.left);
                }
                if (polled.right != null) {
                    graph.computeIfAbsent(polled, x -> new ArrayList<>()).add(polled.right);
                    graph.computeIfAbsent(polled.right, x -> new ArrayList<>()).add(polled);
                    q.offer(polled.right);
                }
            }
        }
        Set<Integer> seen = new HashSet<>();
        seen.add(q2.peek().val);
        while (!q2.isEmpty()) {
            TreeNode polled = q2.poll();
            if (polled.left == null && polled.right == null) {
                return polled.val;
            }
            for (TreeNode next : graph.get(polled)) {
                if (seen.add(next.val)) {
                    q2.offer(next);
                }
            }
        }
        return 0;
    }

}
