package Leetcode;

/**
 * @author sfedosov on 7/21/19.
 */
public class SudokuSolver {

    private static final char EMPTY_ENTRY = '.';

    public static boolean solveSudoku(char[][] board){
        return solveSudokuCell(0, 0, board);
    }

    private static boolean solveSudokuCell(int row, int col, char[][] board) {

        if (col == board[row].length){
            col = 0;
            row++;
            if (row == board.length){
                return true; // Entire board has been filled without conflict.
            }

        }

        // Skip non-empty entries. They already have a value in them.
        if (board[row][col] != EMPTY_ENTRY) {
            return solveSudokuCell(row, col + 1, board);
        }

        for (int value = 1; value <= board.length; value++) {

            char charToPlace = (char) (value + '0'); // convert int value to char
            if (canPlaceValue(board, row, col, charToPlace)) {
                board[row][col] = charToPlace;
                if (solveSudokuCell(row, col + 1, board)) { // recurse with our VALID placement
                    return true;
                }
            }

        }

        board[row][col] = EMPTY_ENTRY;
        return false; // No valid placement was found, this path is faulty, return false
    }

    /*
      Will the placement at (row, col) break the Sudoku properties?
    */
    private static boolean canPlaceValue(char[][] board, int row, int col, char charToPlace) {

        // Check column constraint. For each row, we do a check on column "col".
        for (char[] element : board) {
            if (charToPlace == element[col]){
                return false;
            }
        }

        // Check row constraint. For each column in row "row", we do a check.
        for (int i = 0; i < board.length; i++) {
            if (charToPlace == board[row][i]) {
                return false;
            }
        }

        int I = row / 3;
        int J = col / 3;
        int topLeftOfSubBoxRow = 3 * I; // the row of the top left of the block
        int topLeftOfSubBoxCol = 3 * J; // the column of the tol left of the block

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {

      /*
        i and j just define our offsets from topLeftOfBlockRow
        and topLeftOfBlockCol respectively
      */
                if (charToPlace == board[topLeftOfSubBoxRow + i][topLeftOfSubBoxCol + j]) {
                    return false;
                }

            }
        }

        return true; // placement is valid
    }

}
