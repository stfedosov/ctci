package Leetcode;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author sfedosov on 3/20/18.
 */
public class FindUniqueCharacterInString {

    public static void main(String[] args) {
        assert firstUniqChar("leetcode") == 0;
        assert firstUniqChar("loveleetcode") == 2;
        assert firstUniqChar("cc") == -1;
        assert firstUniqChar("") == -1;
        assert firstUniqChar2("leetcode") == 0;
        assert firstUniqChar2("loveleetcode") == 2;
        assert firstUniqChar2("cc") == -1;
        assert firstUniqChar2("") == -1;
    }

    public static int firstUniqChar(String s) {
        Map<Character, Integer> map = new LinkedHashMap<>();
        for (char c : s.toCharArray()) {
            map.put(c, map.get(c) == null ? 1 : map.get(c) + 1);
        }
        char found = '\0';
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if (entry.getValue() == 1) {
                found = entry.getKey();
                break;
            }
        }
        return found != '\0' ? s.indexOf(found) : -1;
    }

    public static int firstUniqChar2(String s) {
        int freq[] = new int[26];
        for (int i = 0; i < s.length(); i++) {
            freq[s.charAt(i) - 'a']++;
        }
        for (int i = 0; i < s.length(); i++) {
            if (freq[s.charAt(i) - 'a'] == 1)
                return i;
        }
        return -1;
    }

}
