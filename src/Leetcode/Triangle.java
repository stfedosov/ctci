package Leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sfedosov on 4/8/18.
 */
public class Triangle {

    public static void main(final String[] args) {
        final List<Integer> list11 = new ArrayList<Integer>() {{
            add(-1);
        }};
        final List<Integer> list112 = new ArrayList<Integer>() {{
            add(2);
            add(3);
        }};
        final List<Integer> list113 = new ArrayList<Integer>() {{
            add(1);
            add(-1);
            add(-3);
        }};
        List<List<Integer>> lists1 = new ArrayList<List<Integer>>() {{
            add(list11);
            add(list112);
            add(list113);
        }};
        System.out.println(minimumTotal(lists1));
    }

    public static int minimumTotal(List<List<Integer>> triangle) {
        int[][] memo = new int[triangle.size()][1000];
        return minimumTotalRecursive(triangle, 0, 0, memo);
    }

    private static int minimumTotalRecursive(List<List<Integer>> triangle, int row, int column, int[][] memo) {
        if (row >= triangle.size() || column >= triangle.get(row).size()) {
            return 0;
        }
        if (memo[row][column] != 0) {
            return memo[row][column];
        } else {
            memo[row][column] = triangle.get(row).get(column) + Math.min(minimumTotalRecursive(triangle, row + 1, column, memo),
                    minimumTotalRecursive(triangle, row + 1, column + 1, memo));
            return memo[row][column];
        }
    }

}
