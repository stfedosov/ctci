package Leetcode;

import Leetcode.RotateList.ListNode;

public class SortedListToBST {

    /*
    Given the head of a singly linked list where elements are sorted in ascending order, convert it to a height balanced BST.

    For this problem, a height-balanced binary tree is defined as a binary tree in which
    the depth of the two subtrees of every node never differ by more than 1.
     */
    public static void main(String[] args) {
        ListNode root = new ListNode(-10);
        ListNode next = new ListNode(-3);
        root.next = next;
        ListNode next1 = new ListNode(0);
        next.next = next1;
        ListNode next2 = new ListNode(5);
        next1.next = next2;
        next2.next = new ListNode(9);

        TreeNode actual = sortedListToBST(root);

        TreeNode expected = new TreeNode(0);
        TreeNode left = new TreeNode(-3);
        expected.left = left;
        TreeNode right = new TreeNode(9);
        expected.right = right;
        left.left = new TreeNode(-10);
        right.left = new TreeNode(5);

        assert actual.equals(expected);
    }

    // Time complexity: O(NlogN)
    // Space complexity: O(logN)
    public static TreeNode sortedListToBST(ListNode head) {
        if (head == null) return null;
        if (head.next == null) return new TreeNode(head.val);
        ListNode temp = head;
        ListNode slow = head;
        ListNode fast = head;
        while (fast != null && fast.next != null) {
            temp = slow;
            slow = slow.next;
            fast = fast.next.next;
        }
        temp.next = null;
        TreeNode root = new TreeNode(slow.val);
        root.left = sortedListToBST(head);
        root.right = sortedListToBST(slow.next);
        return root;
    }

}
