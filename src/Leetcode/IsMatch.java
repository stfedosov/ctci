package Leetcode;

public class IsMatch {

    public static void main(String[] args) {
        assert isMatch("ab", ".*");
        assert isMatch("aab", "c*a*b");
        assert !isMatch("mississippi", "mis*is*p*.");
        assert !isMatch("ab", "ab*a*c*a");
    }

    public static boolean isMatch(String s, String p) {
        if (p.isEmpty()) return s.isEmpty();
        boolean firstEqOrDot = !s.isEmpty() && (p.charAt(0) == s.charAt(0) || p.charAt(0) == '.');
        if (p.length() > 1 && p.charAt(1) == '*') {
            return isMatch(s, p.substring(2)) || firstEqOrDot && isMatch(s.substring(1), p);
        } else {
            return firstEqOrDot && isMatch(s.substring(1), p.substring(1));
        }
    }

}
