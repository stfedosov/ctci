package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author sfedosov on 12/3/19.
 */
public class InvertTree {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(4);
        TreeNode left2 = new TreeNode(2);
        root.left = left2;
        TreeNode right2 = new TreeNode(7);
        root.right = right2;
        left2.left = new TreeNode(1);
        left2.right = new TreeNode(3);
        right2.left = new TreeNode(6);
        right2.right = new TreeNode(9);
        List<Integer> l = new ArrayList<>();
        inorder(root, l);
        assert l.equals(Arrays.asList(4, 2, 1, 3, 7, 6, 9));
        l.clear();
        inorder(invertTree(root), l);
        assert l.equals(Arrays.asList(4, 7, 9, 6, 2, 3, 1));
    }

    public static TreeNode invertTree(TreeNode root) {
        if (root == null) return null;
        TreeNode left = invertTree(root.left);
        TreeNode right = invertTree(root.right);
        root.left = right;
        root.right = left;
        return root;
    }

    private static void inorder(TreeNode root, List<Integer> l) {
        if (root == null) {
            return;
        }
        l.add(root.val);
        inorder(root.left, l);
        inorder(root.right, l);
    }

}
