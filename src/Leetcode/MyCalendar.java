package Leetcode;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class MyCalendar {

    public static void main(String[] args) {
        MyCalendar obj = new MyCalendar();
        assert obj.book(37, 50);
        assert !obj.book(33, 50);
        assert obj.book(4, 17);
        assert !obj.book(35, 48);
        assert !obj.book(8, 25);
    }

    private final TreeMap<Integer, Integer> map;

    public MyCalendar() {
        map = new TreeMap<>();
    }

    // O(log n)
    public boolean book(int start, int end) {
        // Returns the entry for the greatest key less than the specified key - O(log n)
        Integer greatestStartFromTheEnd = map.lowerKey(end);

        if (greatestStartFromTheEnd == null || map.get(greatestStartFromTheEnd) <= start) {
            // insert O(log n)
            map.put(start, end);
            return true;
        }
        return false;
    }

    private class MyCalendar2 {
        private List<int[]> calendar2;

        MyCalendar2() {
            calendar2 = new ArrayList<>();
        }

        // O(n)
        public boolean book2(int start, int end) {
            for (int[] event : calendar2) {
                if (event[0] < end && start < event[1]) return false;
            }
            calendar2.add(new int[]{start, end});
            return true;
        }
    }
}