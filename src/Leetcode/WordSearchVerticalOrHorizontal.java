package Leetcode;

/**
 * @author sfedosov on 11/14/19.
 */
public class WordSearchVerticalOrHorizontal {

    private static char[][] board = {
            {'q', 'w', 'e', 'r', 't', 'y'},
            {'a', 's', 'd', 'f', 'g', 'h'},
            {'z', 'x', 'c', 'v', 'b', 'n'},
            {'y', 'u', 'a', 'd', 'p', 'r'},
            {'g', 'f', 't', 'b', 'a', 'g'},
            {'o', 'o', 'f', 't', 's', 'g'}
    };


    private static boolean wordFound(String word) {
        if (word == null || word.isEmpty()) return false;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (search(i, j, 0, word, true) || search(i, j, 0, word, false)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean search(int i, int j, int index, String word, boolean horizontalSearch) {
        if (index >= word.length()) return true;
        if (correctPlace(i, j, word.charAt(index))) return false;
        if (horizontalSearch) {
            return search(i, j + 1, index + 1, word, true) ||
                    search(i, j - 1, index + 1, word, true);
        } else {
            return search(i + 1, j, index + 1, word, false) ||
                    search(i - 1, j, index + 1, word, false);
        }
    }

    private static boolean correctPlace(int i, int j, char c) {
        return i < 0 || j < 0 || i >= board.length || j >= board[i].length || c != board[i][j];
    }

    public static void main(String[] args) {
        assert wordFound("cat");
        assert wordFound("bag");
        assert wordFound("foo");
        assert wordFound("dpr");
        assert wordFound("edc");
        assert wordFound("ooftsg");
        assert wordFound("cvbn");
        assert !wordFound("cad");
        assert !wordFound("qwertys");
        assert !wordFound("");
        assert !wordFound("bas");
        assert !wordFound("324");
        assert !wordFound("rpas");
    }
}
