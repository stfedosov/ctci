package Leetcode;

/**
 * @author sfedosov on 4/10/18.
 */
public class DeleteANodeInBST {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(5);
        TreeNode treeNode3 = new TreeNode(3);
        root.left = treeNode3;
        treeNode3.left = new TreeNode(2);
        treeNode3.right = new TreeNode(4);
        TreeNode treeNode6 = new TreeNode(6);
        root.right = treeNode6;
        treeNode6.right = new TreeNode(7);
        System.out.println(root);
        deleteNode(root, 3);
        System.out.println(root);
    }

    private static TreeNode deleteNode(TreeNode root, int key) {
        if (root == null) {
            return null;
        }
        if (key < root.val) {
            root.left = deleteNode(root.left, key);
        } else if (key > root.val) {
            root.right = deleteNode(root.right, key);
        } else {
            if (root.left == null) {
                return root.right;
            } else if (root.right == null) {
                return root.left;
            }

            TreeNode minNode = findMin(root.right);
            root.val = minNode.val;
            root.right = deleteNode(root.right, root.val);
        }
        return root;
    }

    private static TreeNode findMin(TreeNode node) {
        while (node.left != null) {
            node = node.left;
        }
        return node;
    }

}
