package Leetcode;

public class SubarrayProductLessThanK {

    public static void main(String[] args) {
        assert numSubarrayProductLessThanKOptimized(new int[]{10, 5, 2, 6}, 100) == 8;
    }

    // Sliding windows
    public static int numSubarrayProductLessThanKOptimized(int[] nums, int k) {
        if (k <= 1) return 0;
        int start = 0, tmp = 1, count = 0;
        for (int end = 0; end < nums.length; end++) {
            tmp *= nums[end];
            while (tmp >= k) {
                tmp /= nums[start++];
            }
            count += end - start + 1;
        }
        return count;
    }

}
