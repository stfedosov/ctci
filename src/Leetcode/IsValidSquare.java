package Leetcode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author sfedosov on 4/7/20.
 */
public class IsValidSquare {

    public static void main(String[] args) {

        // Idea: we need to find all distances for all sides including diagonals.
        // Then put all of them into set and see if we have at least four equal distances
        // If this is true then it means that we have a valid square
        assert validSquare(new int[]{0, 0}, new int[]{1, 1}, new int[]{1, 0}, new int[]{0, 1});
        assert !validSquare(new int[]{0, 0}, new int[]{1, 1}, new int[]{0, 0}, new int[]{0, 0});
        assert !validSquare(new int[]{0, 0}, new int[]{5, 0}, new int[]{5, 4}, new int[]{0, 4});
        assert validSquare(new int[]{1, 0}, new int[]{-1, 0}, new int[]{0, 1}, new int[]{0, -1});
    }

    public static boolean validSquare(int[] p1, int[] p2, int[] p3, int[] p4) {
        List<Double> doubles = Arrays.asList(
                Math.pow((p1[0] - p2[0]), 2) + Math.pow((p1[1] - p2[1]), 2),
                Math.pow((p2[0] - p3[0]), 2) + Math.pow((p2[1] - p3[1]), 2),
                Math.pow((p3[0] - p4[0]), 2) + Math.pow((p3[1] - p4[1]), 2),
                Math.pow((p4[0] - p1[0]), 2) + Math.pow((p4[1] - p1[1]), 2),
                Math.pow((p1[0] - p3[0]), 2) + Math.pow((p1[1] - p3[1]), 2),
                Math.pow((p2[0] - p4[0]), 2) + Math.pow((p2[1] - p4[1]), 2));
        Set<Double> set = new HashSet<>();
        int count = 0;
        for (Double d : doubles) {
            if (!d.equals(0.0d) && !set.add(d)) {
                count++;
            }
        }
        return count >= 4;
    }
}
