package Leetcode;

public class ConvertBSTTOGreaterTree {

    // Given the root of a Binary Search Tree (BST), convert it to a Greater Tree such that
    // every key of the original BST is changed to the original key plus sum of
    // all keys greater than the original key in BST.
    static int sum = 0;

    public static void main(String[] args) {
        TreeNode root = new TreeNode(5);
        root.right = new TreeNode(13);
        root.left = new TreeNode(2);
        convertBST(root);
    }

    public static TreeNode convertBST(TreeNode root) {
        if (root != null) {
            convertBST(root.right);
            sum += root.val;
            root.val = sum;
            convertBST(root.left);
        }
        return root;
    }

}
