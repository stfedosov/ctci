package Leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sfedosov on 4/4/18.
 */
public class MaxDepthNAryTree {

    public static void main(String[] args) {
        Node node5 = new Node(5, null);
        Node node6 = new Node(6, null);
        Node node2 = new Node(2, null);
        Node node4 = new Node(4, null);
        List<Node> list = new ArrayList<>();
        list.add(node5);
        list.add(node6);
        List<Node> list1 = new ArrayList<>();
        Node node3 = new Node(3, list);
        list1.add(node2);
        list1.add(node4);
        list1.add(node3);
        Node root = new Node(1, list1);
        assert maxDepth(root) == 3;
    }

    private static class Node {
        public int val;
        List<Node> children;

        Node(int val, List<Node> children) {
            this.val = val;
            this.children = children;
        }
    }

    public static int maxDepth(Node root) {
        if (root == null || root.children == null) {
            return 0;
        }
        int max = 0;
        for (Node child : root.children) {
            max = Math.max(max, maxDepth(child));
        }
        return max + 1;
    }

}
