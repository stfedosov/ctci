package Leetcode;

import java.util.HashMap;
import java.util.Map;

public class IsStroboGrammatic {

    public static void main(String[] args) {
        assert isStrobogrammatic("69");
        assert isStrobogrammatic("88");
        assert isStrobogrammatic("1");
        assert !isStrobogrammatic("6");
        assert !isStrobogrammatic("7");
        assert !isStrobogrammatic("962");
    }

    public static boolean isStrobogrammatic(String num) {
        Map<Character, Character> map = new HashMap<>();
        map.put('0', '0');
        map.put('1', '1');
        map.put('8', '8');
        map.put('6', '9');
        map.put('9', '6');

        int left = 0, right = num.length() - 1;

        while (left <= right) {
            if (!map.containsKey(num.charAt(left)) || map.get(num.charAt(left)) != num.charAt(right)) {
                return false;
            }
            left++;
            right--;
        }
        return true;
    }

}
