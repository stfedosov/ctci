package Leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class WordBreak {

    public static void main(String[] args) {
        assert wordBreak("leetcode", new ArrayList<>() {{
            add("leet");
            add("code");
        }});
        assert !wordBreak("facebook", new ArrayList<>() {{
            add("face");
            add("bouk");
            add("fac");
        }});
    }

    public static boolean wordBreak(String s, List<String> wordDict) {
        Set<String> set = new HashSet<>(wordDict);
        Map<String, Boolean> cache = new HashMap<>();
        cache.put("", true);
//        return findRecursivelyCached(s, set, cache);
        return findRecursivelyNonCached(s, set);
    }

    // O(n^2)
    private static boolean findRecursivelyCached(String s, Set<String> set, Map<String, Boolean> cache) {
        if (cache.containsKey(s)) return cache.get(s);
        for (int i = 1; i <= s.length(); i++) {
            boolean res = findRecursivelyCached(s.substring(i), set, cache);
            cache.put(s.substring(i), res);
            if (set.contains(s.substring(0, i)) && res) {
                return true;
            }
        }
        return false;
    }

    // O(2^n)
    private static boolean findRecursivelyNonCached(String s, Set<String> set) {
        if (s.isEmpty()) return true;
        for (int i = 1; i <= s.length(); i++) {
            if (set.contains(s.substring(0, i)) && findRecursivelyNonCached(s.substring(i), set)) {
                return true;
            }
        }
        return false;
    }

}
