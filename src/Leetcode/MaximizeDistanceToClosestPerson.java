package Leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sfedosov on 11/29/19.
 */
public class MaximizeDistanceToClosestPerson {

    public static void main(String[] args) {
        assert maxDistToClosest(new int[]{1, 0, 0, 0, 1, 0, 1}) == 2;
        assert maxDistToClosest(new int[]{1, 0, 0, 0}) == 3;
        assert maxDistToClosest(new int[]{0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0}) == 7;
        assert maxDistToClosest(new int[]{0, 1, 1, 1, 0, 0, 1, 0, 0}) == 2;
    }

    public static int maxDistToClosest(int[] seats) {
        List<Integer> positions = new ArrayList<>();
        for (int i = 0; i < seats.length; i++) {
            if (seats[i] > 0) positions.add(i);
        }
        int start = 0;
        if (positions.size() == 1) {
            return Math.max(seats.length - 1 - positions.get(start), positions.get(start));
        } else {
            int gap = 0;
            for (Integer position : positions) {
                gap = Math.max(gap, seats[start] == 0 ? position - start : (position - start) / 2);
                start = position;
            }
            return Math.max(seats.length - 1 - positions.get(positions.size() - 1), gap);
        }
    }

}
