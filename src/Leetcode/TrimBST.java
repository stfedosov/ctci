package Leetcode;

import java.util.Stack;

public class TrimBST {

    public static void main(String[] args) {
        //    1             1
        //   / \      =>     \
        //  0   2             2
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(0);
        root.right = new TreeNode(2);

        TreeNode expected = new TreeNode(1);
        expected.right = new TreeNode(2);
        assert trimBST(root, 1, 2).equals(expected);

        //    3                  3
        //   / \                /
        //  0   4     =>       2
        //   \                /
        //    2              1
        //   /
        //  1
        root = new TreeNode(3);
        TreeNode zero = new TreeNode(0);
        root.left = zero;
        zero.right = new TreeNode(2);
        zero.right.left = new TreeNode(1);
        root.right = new TreeNode(4);

        expected = new TreeNode(3);
        expected.left = new TreeNode(2, new TreeNode(1), null);
        assert trimBST(root, 1, 3).equals(expected);
    }

    public static TreeNode trimBST(TreeNode root, int low, int high) {
        if (root == null) return root;
        if (root.val > high) return trimBST(root.left, low, high);
        if (root.val < low) return trimBST(root.right, low, high);

        root.left = trimBST(root.left, low, high);
        root.right = trimBST(root.right, low, high);
        return root;
    }

    // iterative

    public TreeNode trimBSTIterative(TreeNode root, int low, int high) {
        // find root
        if (root == null) return root;
        while (root != null && (root.val < low || root.val > high)) {
            if (root.val < low) {
                root = root.right;
            }
            if (root.val > high) {
                root = root.left;
            }
        }
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode cur = stack.pop();
            TreeNode left = cur.left;
            // trim lower value
            while (left != null && left.val < low) left = left.right;
            TreeNode right = cur.right;
            // trim higher value
            while (right != null && right.val > high) right = right.left;
            cur.left = left;
            cur.right = right;
            if (cur.left != null)
                stack.push(cur.left);
            if (cur.right != null)
                stack.push(cur.right);
        }
        return root;
    }

}
