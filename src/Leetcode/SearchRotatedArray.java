package Leetcode;

/**
 * @author sfedosov on 4/19/20.
 */
public class SearchRotatedArray {

    public static void main(String[] args) {
        assert search(new int[]{4, 5, 6, 7, 0, 1, 2}, 0) == 4;
        assert search(new int[]{4, 5, 6, 7, 0, 1, 2}, 3) == -1;
    }

    public static int search(int[] nums, int target) {
        if (null == nums || 0 == nums.length)
            return -1;

        int lo = 0, hi = nums.length - 1;

        while (lo <= hi) {

            int mid = lo + (hi - lo) / 2;
            if (nums[mid] == target)
                return mid;

            if (nums[mid] >= nums[lo]) {
                // case 1: array is not rotated since middle element is bigger than first one
                if (target >= nums[lo] && target < nums[mid])
                    // target is somewhere in the non-rotated part
                    hi = mid - 1;
                else
                    lo = mid + 1;
            } else {
                // case 2: Middle element is smaller than the first element of the array,
                // i.e. the rotation index is somewhere between 0 and middle
                if (target <= nums[hi] && target > nums[mid])
                    // target is somewhere in the non-rotated part
                    lo = mid + 1;
                else
                    hi = mid - 1;
            }
        }
        return -1;
    }

}
