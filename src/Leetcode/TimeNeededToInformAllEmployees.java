package Leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TimeNeededToInformAllEmployees {

    public static void main(String[] args) {
        assert numOfMinutes(
                11,
                4,
                new int[]{5, 9, 6, 10, -1, 8, 9, 1, 9, 3, 4},
                new int[]{0, 213, 0, 253, 686, 170, 975, 0, 261, 309, 337}) == 2560;
    }

    public static int numOfMinutes(int n, int headID, int[] manager, int[] informTime) {
        Map<Integer, Node> map = new HashMap<>();
        int i = 0;
        while (i < n) {
            map.put(i, new Node(i, informTime[i]));
            i++;
        }
        for (int x = 0; x < manager.length; x++) {
            Node node = map.get(x);
            Node parent = manager[x] == -1 ? null : map.get(manager[x]);
            if (parent != null) parent.child.add(node);
        }
        return dfs(headID, map);
    }

    //  ---------------------------------------
    //        11
    //        4
    //        [5,9,6,10,-1,8,9,1,9,3,4]
    //         0 1 2  3  4 5 6 7 8 9 10
    //        [0,213,0,253,686,170,975,0,261,309,337]
    //         0  1  2  3   4   5   6  7  8   9   10
    //  ---------------------------------------
    //
    //                  /
    //                 / (686)
    //             (root = 4)
    //               / (337)
    //              10
    //              / (253)
    //             3
    //              \ (309)
    //               |  (213)     (0)
    //               9 ------- 1 ----- 7
    //        (261) / \ (975)
    //             /   \
    //            8     6
    //     (170) /       \ (0)
    //          5         2
    //     (0) /
    //        0
    //
    // 686 + 337 + 253 + 309 + 975 = 2560

    private static int dfs(int head, Map<Integer, Node> map) {
        int max = 0;
        for (Node n : map.get(head).child) {
            max = Math.max(max, dfs(n.val, map));
        }
        return max + map.get(head).informTime;
    }

    static class Node {
        int val;
        List<Node> child;
        int informTime;

        public Node(int val, int informTime) {
            this.val = val;
            this.informTime = informTime;
            this.child = new ArrayList<>();
        }

        public String toString() {
            return val + ", informTime = " + informTime + ", child = " + child;
        }
    }

}
