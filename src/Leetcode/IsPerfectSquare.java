package Leetcode;

/**
 * @author sfedosov on 5/9/20.
 */
public class IsPerfectSquare {

    public static void main(String[] args) {
        assert !isPerfectSquareBruteForce(5);
        assert isPerfectSquareBruteForce(4);
    }

    // brute-force, runtime is O(sqrt(n))
    public static boolean isPerfectSquareBruteForce(int num) {
        if (num <= 1) return true;
        for (int j = 1; j <= num / 2; j++) {
            if (j * j == num) return true;
        }
        return false;
    }

    //optimized, runtime is O(log(n))
    public static boolean isPerfectSquareOptimized(int num) {
        if (num <= 1) return true;
        int low = 1, high = num / 2;
        while (low <= high) {
            long mid = (low + high) / 2;
            if (mid * mid == num) return true;
            else if (mid * mid < num) {
                low = (int) mid + 1;
            } else {
                high = (int) mid - 1;
            }
        }
        return false;
    }

}
