package Leetcode;

import java.util.*;

public class MaxSlidingWindow {

    public static void main(String[] args) {
        assert Arrays.equals(maxSlidingWindowPQ(new int[]{1, 3, -1, -3, 5, 3, 6, 7}, 3), new int[]{3, 3, 5, 5, 6, 7});
        assert Arrays.equals(maxSlidingWindowPQ(new int[]{1, -1}, 1), new int[]{1, -1});
        assert Arrays.equals(maxSlidingWindowPQ(new int[]{1, 3, 1, 2, 0, 5}, 3), new int[]{3, 3, 2, 5});
        assert Arrays.equals(maxSlidingWindowPQ(new int[]{-7, -8, 7, 5, 7, 1, 6, 0}, 4), new int[]{7, 7, 7, 7, 7});

        assert Arrays.equals(maxSlidingWindow(new int[]{1, 3, -1, -3, 5, 3, 6, 7}, 3), new int[]{3, 3, 5, 5, 6, 7});
        assert Arrays.equals(maxSlidingWindow(new int[]{1, -1}, 1), new int[]{1, -1});
        assert Arrays.equals(maxSlidingWindow(new int[]{1, 3, 1, 2, 0, 5}, 3), new int[]{3, 3, 2, 5});
        assert Arrays.equals(maxSlidingWindow(new int[]{-7, -8, 7, 5, 7, 1, 6, 0}, 4), new int[]{7, 7, 7, 7, 7});
    }

    // n

    public static int[] maxSlidingWindow(int[] nums, int k) {
        if (nums == null || k <= 0) {
            return new int[0];
        }
        int[] res = new int[nums.length - k + 1];
        int x = 0;
        // store index
        Deque<Integer> q = new ArrayDeque<>();
        for (int i = 0; i < nums.length; i++) {
            // remove numbers out of range k
            if (!q.isEmpty() && q.peek() < i - k + 1) {
                q.pollFirst();
            }
            // remove smaller numbers in k range as they are useless
            while (!q.isEmpty() && nums[q.peekLast()] < nums[i]) {
                q.pollLast();
            }
            q.offer(i);
            if (i >= k - 1) {
                res[x++] = nums[q.peek()];
            }
        }
        return res;
    }

    // n * log(k)

    public static int[] maxSlidingWindowPQ(int[] nums, int k) {
        if (nums.length == 0) return new int[0];
        int[] result = new int[nums.length - k + 1];
        Queue<Integer> queue = new PriorityQueue<>(Collections.reverseOrder());

        for (int i = 0; i < k; i++) {
            queue.add(nums[i]);
        }
        result[0] = queue.peek();
        for (int i = k; i < nums.length; i++) {
            queue.remove(nums[i - k]);
            queue.add(nums[i]);
            result[i - k + 1] = queue.peek();
        }

        return result;
    }

}
