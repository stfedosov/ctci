package Leetcode;

public class CountAndSay {

    public static void main(String[] args) {
        System.out.println(countAndSay(1).equals("1"));
        System.out.println(countAndSay(2).equals("11"));
        System.out.println(countAndSay(3).equals("21"));
        System.out.println(countAndSay(4).equals("1211"));
        System.out.println(countAndSay(5).equals("111221"));
        System.out.println(countAndSay(6).equals("312211"));

    }

    //  It's O(2^n), because in each iteration the sequence at most doubles, and we go through each sequence once
    //  TC = O(1 + 2 + ... + 2^n) = O(2^n)
    //  SC = O(2^n)
    public static String countAndSay(int n) {
        if (n == 1) {
            return "1";
        } else if (n == 2) {
            return "11";
        }
        String current = countAndSay(n - 1);
        char prev = current.charAt(0);
        int count = 1;
        StringBuilder res = new StringBuilder();
        for (int i = 1; i < current.length(); i++) {
            if (prev == current.charAt(i)) {
                count++;
            } else {
                res.append(count);
                res.append(prev);
                count = 1;
                prev = current.charAt(i);
            }
            if (i == current.length() - 1) {
                res.append(count);
                res.append(prev);
            }
        }
        return res.toString();
    }

}
