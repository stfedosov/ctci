package Leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StreamChecker {

    public static void main(String[] args) {
        StreamChecker streamChecker = new StreamChecker(new String[]{"cd", "f", "kl"});
        assert !streamChecker.query('a');   // return false
        assert !streamChecker.query('b');   // return false
        assert !streamChecker.query('c');   // return false
        assert streamChecker.query('d');    // return true, because 'cd' is in the word list
        assert !streamChecker.query('e');   // return false
        assert streamChecker.query('f');    // return true, because 'f' is in the word list
        assert !streamChecker.query('g');   // return false
        assert !streamChecker.query('h');   // return false
        assert !streamChecker.query('i');   // return false
        assert !streamChecker.query('j');   // return false
        assert !streamChecker.query('k');   // return false
        assert streamChecker.query('l');    // return true, because 'kl' is in the word list

        // -------------
        streamChecker = new StreamChecker(new String[]{"ab", "ba", "aaab", "abab", "baa"});
        assert !streamChecker.query('a');
        assert !streamChecker.query('a');
        assert !streamChecker.query('a');
        assert !streamChecker.query('a');
        assert !streamChecker.query('a');
        assert streamChecker.query('b');
        assert streamChecker.query('a');
        assert streamChecker.query('b');
        assert streamChecker.query('a');
        assert streamChecker.query('b');
        assert !streamChecker.query('b');
        assert !streamChecker.query('b');
        assert streamChecker.query('a');
        assert streamChecker.query('b');
        assert streamChecker.query('a');
        assert streamChecker.query('b');
        assert !streamChecker.query('b');
        assert !streamChecker.query('b');
        assert !streamChecker.query('b');
        assert streamChecker.query('a');
        assert streamChecker.query('b');
        assert streamChecker.query('a');
        assert streamChecker.query('b');
        assert streamChecker.query('a');
        assert streamChecker.query('a');
        assert !streamChecker.query('a');
        assert streamChecker.query('b');
        assert streamChecker.query('a');
        assert streamChecker.query('a');
        assert !streamChecker.query('a');
    }

    private final TrieNode root = new TrieNode('*');
    private final List<Character> q = new ArrayList<>();

    public StreamChecker(String[] words) {
        for (String word : words) {
            TrieNode r = root;
            for (int i = word.length() - 1; i >= 0; i--) {
                TrieNode next = r.map.get(word.charAt(i));
                if (next == null) {
                    r.map.put(word.charAt(i), new TrieNode(word.charAt(i)));
                }
                r = r.map.get(word.charAt(i));
            }
            r.isWord = true;
        }
    }

    private static class TrieNode {
        char c;
        boolean isWord;
        Map<Character, TrieNode> map = new HashMap<>();

        public TrieNode(char c) {
            this.c = c;
        }
    }

    public boolean query(char letter) {
        q.add(letter);
        return search(q, q.size() - 1, root);
    }

    private boolean search(List<Character> q, int i, TrieNode root) {
        if (i < 0) return root.isWord;
        if (root.isWord) return true;
        if (root.map.containsKey(q.get(i))) {
            return search(q, i - 1, root.map.get(q.get(i)));
        }
        return false;
    }
}