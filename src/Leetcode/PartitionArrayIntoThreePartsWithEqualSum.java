package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 4/1/19.
 */
public class PartitionArrayIntoThreePartsWithEqualSum {

    public static void main(String[] args) {
        System.out.println(canThreePartsEqualSum(new int[]{0, 2, 1, -6, 6, -7, 9, 1, 2, 0, 1}));
    }

    private static boolean canThreePartsEqualSum(int[] A) {
        int total = Arrays.stream(A).reduce(0, Integer::sum);
        if (total % 3 != 0) return false;
        int count = 0, sum = 0;
        for (int element : A) {
            sum += element;
            if (sum == (total / 3)) {
                sum = 0;
                count++;
            }
        }
        return count >= 3;
    }

}
