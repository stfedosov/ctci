package Leetcode;

import Leetcode.RotateList.ListNode;

/**
 * @author sfedosov on 2/13/19.
 */
public class InsertionSortList {

    public static void main(String[] args) {
        ListNode root = new ListNode(5);
        ListNode listNode3 = new ListNode(3);
        root.next = listNode3;
        ListNode listNode2 = new ListNode(2);
        listNode3.next = listNode2;
        listNode2.next = new ListNode(6);
        insertionSortList(root);
    }

    public static ListNode sorted_insert(ListNode head, ListNode node) {
        if (node == null) {
            return head;
        }
        if (head == null || node.val <= head.val) {
            node.next = head;
            return node;
        }
        ListNode curr = head;
        while (curr.next != null && (curr.next.val < node.val)) {
            curr = curr.next;
        }
        node.next = curr.next;
        curr.next = node;
        return head;
    }

    public static ListNode insertionSortList(ListNode head) {
        ListNode sorted = null;
        ListNode curr = head;
        while (curr != null) {
            ListNode temp = curr.next;
            sorted = sorted_insert(sorted, curr);
            curr = temp;
        }
        return sorted;
    }

}
