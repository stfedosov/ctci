package Leetcode;

public class FindMinInRotatedArray {

    public static void main(String[] args) {
        assert findMin(new int[]{2, 2, 2, 0, 1}) == 0;
        assert findMin(new int[]{1, 3, 5}) == 1;
        assert findMin(new int[]{4, 5, 6, 7, 0, 1, 2}) == 0;
        assert findMin(new int[]{1, 1, 1}) == 1;
        assert findMin(new int[]{3, 3, 3, 1}) == 1;
    }

    public static int findMin(int[] nums) {
        int start = 0, end = nums.length - 1;
        while (start < end) {
            int mid = start + (end - start) / 2;
            if (nums[start] == nums[mid] && nums[mid] == nums[end]) {
                start++;
                end--;
            } else if (nums[mid] > nums[end]) {
                start = mid + 1;
            } else {
                end = mid;
            }
        }
        return nums[start];
    }

}
