package Leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SpiralMatrix {

    public static List<Integer> spiralOrder(int[][] matrix) {
        if (matrix.length == 0) return Collections.emptyList();
        int topBoundary = 0, rightBoundary = matrix[0].length - 1;
        int leftBoundary = 0, bottomBoundary = matrix.length - 1;
        List<Integer> result = new ArrayList<>();
        while (result.size() != matrix.length * matrix[0].length) {
            for (int column = leftBoundary; column <= rightBoundary; column++) {
                result.add(matrix[topBoundary][column]);
            }
            topBoundary++;

            for (int row = topBoundary; row <= bottomBoundary; row++) {
                result.add(matrix[row][rightBoundary]);
            }
            rightBoundary--;

            if (topBoundary <= bottomBoundary) {
                for (int column = rightBoundary; column >= leftBoundary; column--) {
                    result.add(matrix[bottomBoundary][column]);
                }
                bottomBoundary--;
            }

            if (leftBoundary <= rightBoundary) {
                for (int row = bottomBoundary; row >= topBoundary; row--) {
                    result.add(matrix[row][leftBoundary]);
                }
                leftBoundary++;
            }
        }
        return result;
    }

}
