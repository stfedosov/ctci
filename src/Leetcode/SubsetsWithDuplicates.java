package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SubsetsWithDuplicates {

    public static void main(String[] args) {
        List<List<Integer>> lists = subsetsWithDup(new int[]{1, 2, 2});
        assert lists.containsAll(Arrays.asList(
                Arrays.asList(),
                Arrays.asList(1, 2),
                Arrays.asList(1),
                Arrays.asList(1, 2, 2),
                Arrays.asList(2),
                Arrays.asList(2, 2)));
        assert lists.size() == 6;
    }

    public static List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        Arrays.sort(nums);
        helper(res, new ArrayList<>(), nums, 0, 0);
        return res;
    }

    private static void helper(List<List<Integer>> res, List<Integer> tmp, int[] nums, int start, int rec) {
        res.add(new ArrayList<>(tmp));
        Set<Integer> used = new HashSet<>();
        for (int i = start; i < nums.length; i++) {
            if (used.add(nums[i])) {
                tmp.add(nums[i]);
                helper(res, tmp, nums, i + 1, rec + 1);
                tmp.remove(tmp.size() - 1);
            }
        }
    }

}
