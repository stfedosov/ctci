package Leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sfedosov on 12/29/18.
 */
public class GrayCode {

    public static void main(String[] args) {
        System.out.println(grayCode(3));
    }

    public static List<Integer> grayCode(int n) {
        if (n == 0) {
            List<Integer> result = new ArrayList<>();
            result.add(0);
            return result;
        }

        List<Integer> result = grayCode(n - 1);
        int numToAdd = 1 << (n - 1);
        for (int i = result.size() - 1; i >= 0; i--) {
            result.add(numToAdd + result.get(i));
        }
        return result;
    }

}
