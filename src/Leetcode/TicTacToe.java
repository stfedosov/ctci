package Leetcode;

/**
 * @author sfedosov on 8/20/19.
 */
public class TicTacToe {

    private int[] rows;
    private int[] columns;
    private int diagonal, antidiagonal;

    /**
     * Initialize your data structure here.
     */
    public TicTacToe(int n) {
        this.rows = new int[n];
        this.columns = new int[n];
    }

    /**
     * Player {player} makes a move at ({row}, {col}).
     *
     * @param row    The row of the board.
     * @param col    The column of the board.
     * @param player The player, can be either 1 or 2.
     * @return The current winning condition, can be either:
     * 0: No one wins.
     * 1: Player 1 wins.
     * 2: Player 2 wins.
     */
    public int move(int row, int col, int player) {
        int current = player == 1 ? 1 : -1;
        rows[row] += current;
        columns[col] += current;
        if (row == col) diagonal += current;
        if (col == (columns.length - row - 1)) antidiagonal += current;
        int n = rows.length;
        if (Math.abs(rows[row]) == n || Math.abs(columns[col]) == n ||
                Math.abs(diagonal) == n || Math.abs(antidiagonal) == n) return player;
        return 0;
    }

//    BRUTE FORCE APPROACH ---->
//    private static int[][] matrix;
//
//    /**
//     * Initialize your data structure here.
//     */
//    public TicTacToe(int n) {
//        matrix = new int[n][n];
//    }
//
//    /**
//     * Player {player} makes a move at ({row}, {col}).
//     *
//     * @param row    The row of the board.
//     * @param col    The column of the board.
//     * @param player The player, can be either 1 or 2.
//     * @return The current winning condition, can be either:
//     * 0: No one wins.
//     * 1: Player 1 wins.
//     * 2: Player 2 wins.
//     */
//    public static int move(int row, int col, int player) {
//        matrix[row][col] = player;
//        return checkForWin(matrix, row, col, player) ? player : 0;
//    }
//
//    private static boolean checkForWin(int[][] matrix, int row, int col, int player) {
//        return checkForThisCol(matrix[row], player) ||
//                checkForThisRow(matrix, col, player) ||
//                checkForLeftDiagonal(matrix, matrix[row].length, player) ||
//                checkForRightDiagonal(matrix, matrix[row].length, player);
//    }
//
//    private static boolean checkForRightDiagonal(int[][] matrix, int columns, int player) {
//        for (int i = matrix.length - 1, j = 0; i >= 0 && j < columns; i--, j++) {
//            if (matrix[i][j] != player) return false;
//        }
//        return true;
//    }
//
//    private static boolean checkForLeftDiagonal(int[][] matrix, int columns, int player) {
//        for (int i = 0, j = 0; i < matrix.length && j < columns; i++, j++) {
//            if (matrix[i][j] != player) return false;
//        }
//        return true;
//    }
//
//    private static boolean checkForThisRow(int[][] matrix, int col, int player) {
//        for (int[] row : matrix) {
//            if (row[col] != player) return false;
//        }
//        return true;
//    }
//
//    private static boolean checkForThisCol(int[] matrix, int player) {
//        for (int col : matrix) {
//            if (col != player) return false;
//        }
//        return true;
//    }

    public static void main(String[] args) {
        TicTacToe toe = new TicTacToe(3);

        System.out.println(toe.move(0, 0, 1)); //-> Returns 0 (no one wins)
        //|X| | |
        //| | | |    // Player 1 makes a move at (0, 0).
        //| | | |

        System.out.println(toe.move(0, 2, 2)); //-> Returns 0 (no one wins)
        //|X| |O|
        //| | | |    // Player 2 makes a move at (0, 2).
        //| | | |

        System.out.println(toe.move(2, 2, 1)); //-> Returns 0 (no one wins)
        //|X| |O|
        //| | | |    // Player 1 makes a move at (2, 2).
        //| | |X|

        System.out.println(toe.move(1, 1, 2)); //-> Returns 0 (no one wins)
        //|X| |O|
        //| |O| |    // Player 2 makes a move at (1, 1).
        //| | |X|

        System.out.println(toe.move(2, 0, 1)); //-> Returns 0 (no one wins)
        //|X| |O|
        //| |O| |    // Player 1 makes a move at (2, 0).
        //|X| |X|

        System.out.println(toe.move(1, 0, 2)); //-> Returns 0 (no one wins)
        //|X| |O|
        //|O|O| |    // Player 2 makes a move at (1, 0).
        //|X| |X|

        System.out.println(toe.move(2, 1, 1)); //-> Returns 1 (player 1 wins)
        //|X| |O|
        //|O|O| |    // Player 1 makes a move at (2, 1).
        //|X|X|X|
    }
}