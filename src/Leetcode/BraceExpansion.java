package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BraceExpansion {

    public static void main(String[] args) {
        assert Arrays.equals(expand("{a,b}c{d,e}f"), new String[]{"acdf", "acef", "bcdf", "bcef"});
        assert Arrays.equals(expand("abcde"), new String[]{"abcde"});
    }

    public static String[] expand(String S) {
        Map<Integer, List<Character>> map = new HashMap<>();
        int i = 0;
        boolean hasSeen = false;
        List<Character> tmp = new ArrayList<>();
        for (char c : S.toCharArray()) {
            if (c == ',') continue;
            if (c == '{') {
                hasSeen = true;
                continue;
            }
            if (c == '}') {
                hasSeen = false;
                map.put(i++, new ArrayList<>(tmp));
                tmp = new ArrayList<>();
            } else if (!hasSeen) {
                map.put(i++, Collections.singletonList(c));
            }
            if (hasSeen) {
                tmp.add(c);
            }
        }
        List<String> list = new ArrayList<>();
        helper("", 0, map, list);
        list.sort(Comparator.naturalOrder());
        return list.toArray(new String[0]);
    }

    private static void helper(String tmp, int idx, Map<Integer, List<Character>> map, List<String> list) {
        if (idx >= map.size()) {
            list.add(tmp);
            return;
        }
        for (Character c : map.get(idx)) {
            helper(tmp + c, idx + 1, map, list);
        }
    }

}
