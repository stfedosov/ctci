package Leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sfedosov on 1/3/19.
 *         <p>
 *         As per question if length = 8. numbers would be 1,2,3,..,8
 *         need to find out which one is missing only if 1<=a[i]<=length
 *         - it will not work if any value is 0 or   greater than length
 *         thats why when doing 1st loop , if we decrement the number by 1
 *         we must get some index which is present in array, and if value on that index is
 *         greater than > 0 , we are making it negative as a visited one.
 *         <p>
 *         now whenever we are getting duplicate number we are moving to same index
 *         as a result some indexes will not be marked as negative. And in next loop
 *         we are getting those indexes and doing +1 with them to get the missing
 *         number
 */
public class FindDissappearedElements {

    public static void main(String[] args) {
        findDisappearedNumbers(new int[]{4, 3, 2, 7, 8, 2, 3, 1});
    }

    public static List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < nums.length; i++) {
            int index = Math.abs(nums[i]) - 1;
            if (nums[index] > 0) {
                nums[index] = -nums[index];
            }
        }
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > 0) {
                result.add(i + 1);
            }
        }
        return result;

    }

}
