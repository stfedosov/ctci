package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author sfedosov on 1/4/20.
 */
public class SummaryRanges {

    public static void main(String[] args) {
        assert summaryRanges(new int[]{0, 1, 2, 4, 5, 7}).equals(Arrays.asList("0->2", "4->5", "7"));
        assert summaryRanges(new int[]{0, 2, 3, 4, 6, 8, 9}).equals(Arrays.asList("0", "2->4", "6", "8->9"));
        assert summaryRanges(new int[]{}).equals(Collections.emptyList());
    }

    public static List<String> summaryRanges(int[] nums) {
        if (nums.length == 0) return Collections.emptyList();
        List<String> res = new ArrayList<>();
        int start = nums[0];
        int tmp = start;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] - tmp != 1) {
                if (start == tmp) res.add(start + "");
                else res.add(start + "->" + tmp);
                start = nums[i];
            }
            tmp = nums[i];
        }
        if (start == tmp) res.add(start + "");
        else res.add(start + "->" + tmp);
        return res;
    }

}
