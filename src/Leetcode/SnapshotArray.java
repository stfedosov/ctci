package Leetcode;

import java.util.TreeMap;

/**
 * @author sfedosov on 9/5/19.
 */
public class SnapshotArray {

    public static void main(String[] args) {
        SnapshotArray snapshotArray = new SnapshotArray(1);
        snapshotArray.set(0, 15);
        assert snapshotArray.snap() == 0;
        assert snapshotArray.snap() == 1;
        assert snapshotArray.snap() == 2;
        assert snapshotArray.get(0, 2) == 15;
        assert snapshotArray.snap() == 3;
        assert snapshotArray.snap() == 4;
        assert snapshotArray.get(0, 0) == 15;
    }

    TreeMap<Integer, Integer>[] maps;
    private int snap_id = 0;

    public SnapshotArray(int length) {
        this.maps = new TreeMap[length];
        for (int i = 0; i < length; i++) {
            maps[i] = new TreeMap<>();
            maps[i].put(0, 0);
        }
    }

    public void set(int index, int val) {
        maps[index].put(snap_id, val);
    }

    public int snap() {
        return snap_id++;
    }

    public int get(int index, int snap_id) {
        return maps[index].floorEntry(snap_id).getValue();
    }
}