package Leetcode;

import java.util.Stack;

/**
 * @author sfedosov on 4/20/18.
 */
public class ReverseStack {

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        System.out.println(popFromTheBeginning(stack));
        System.out.println(stack);
    }

    private static int popFromTheBeginning(Stack<Integer> stack) {
        if (stack.size() == 1) return stack.pop();
        int value = stack.pop();
        int result = popFromTheBeginning(stack);
        stack.push(value);
        return result;
    }

    private static Stack<Integer> reverseStackWithoutAdditionalMemory(Stack<Integer> stack) {
        if (stack.isEmpty()) {
            return stack;
        }
        int temp = stack.pop();
        reverseStackWithoutAdditionalMemory(stack);
        insertAtTheBottom(stack, temp);
        return stack;
    }

    private static void insertAtTheBottom(Stack<Integer> stack, int temp) {
        if (stack.isEmpty()) {
            stack.push(temp);
            return;
        }
        int temp2 = stack.pop();
        insertAtTheBottom(stack, temp);
        stack.push(temp2);
    }

}
