package Leetcode;

/**
 * @author sfedosov on 10/7/19.
 */
public class ReverseString2 {

    public static void main(String[] args) {
        assert reverseStr("abcdefg", 2).equals("bacdfeg");
        assert reverseStr("abcdefg", 8).equals("gfedcba");
        System.out.println(reverseRecur("abcde"));
    }

    public static String reverseStr(String s, int k) {
        if (s.length() < k) {
            return reverse(s);
        } else if (s.length() < 2 * k && s.length() >= k) {
            return reverse(s.substring(0, k)) + s.substring(k);
        } else {
            return reverse(s.substring(0, k)) + s.substring(k, 2 * k) + reverseStr(s.substring(2 * k), k);
        }
    }

    private static String reverse(String s) {
        char[] tmp = s.toCharArray();
        int start = 0, end = s.length() - 1;
        while (start < end) {
            char t = tmp[start];
            tmp[start] = tmp[end];
            tmp[end] = t;
            end--;
            start++;
        }
        return new String(tmp);
    }

    private static String reverseRecur(String s) {
        if (s.isEmpty()) return s;
        return reverseRecur(s.substring(1)) + s.charAt(0);
    }

}
