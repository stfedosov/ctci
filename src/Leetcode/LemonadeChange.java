package Leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 9/14/19.
 */
public class LemonadeChange {

    public static void main(String[] args) {
        assert lemonadeChange(new int[]{5, 5, 5, 5, 10, 5, 10, 10, 10, 20});
        assert lemonadeChange(new int[]{5, 5, 10, 20, 5, 5, 5, 5, 5, 5, 5, 5, 5, 10, 5, 5, 20, 5, 20, 5});
        assert !lemonadeChange(new int[]{5, 5, 10, 10, 20});
        assert !lemonadeChange(new int[]{5, 5, 5, 5, 20, 20, 5, 5, 20, 5});
    }

    // ------ My own

    public static boolean lemonadeChange(int[] bills) {
        Map<Integer, Integer> wallet = new HashMap<>();
        for (Integer bill : bills) {
            if (bill > 5) {
                int toChange = bill - 5;
                boolean hasFive = wallet.containsKey(5);
                if (toChange == 5 && hasFive) {
                    withdraw(wallet, toChange, 1);
                } else if (wallet.containsKey(10) && wallet.get(10) >= 1 && hasFive) {
                    withdraw(wallet, 5, 1);
                    withdraw(wallet, 10, 1);
                } else if (hasFive && wallet.get(5) >= 3) {
                    withdraw(wallet, 5, 3);
                } else {
                    return false;
                }
            }
            wallet.put(bill, wallet.getOrDefault(bill, 0) + 1);
        }
        return true;
    }

    private static void withdraw(Map<Integer, Integer> wallet, int amount, int count) {
        Integer currentCount = wallet.get(amount);
        currentCount -= count;
        if (currentCount == 0) wallet.remove(amount);
        else wallet.put(amount, currentCount);
    }


    // ------ More optimal and easy

    public boolean lemonadeChange2(int[] bills) {
        int five = 0, ten = 0;
        for (int i : bills) {
            if (i == 5) five++;
            else if (i == 10) {
                five--;
                ten++;
            } else if (ten > 0) {
                ten--;
                five--;
            } else five -= 3;
            if (five < 0) return false;
        }
        return true;
    }

}
