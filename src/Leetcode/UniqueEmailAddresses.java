package Leetcode;

import java.util.HashSet;
import java.util.Set;

/**
 * @author sfedosov on 5/7/19.
 */
public class UniqueEmailAddresses {

    public static void main(String[] args) {
        numUniqueEmails(new String[]{"test.email+alex@leetcode.com","test.email.leet+alex@code.com"});
    }

    public static int numUniqueEmails(String[] emails) {
        Set<String> result = new HashSet<>();
        for (String email : emails) {
            String[] splittedEmail = email.split("@");
            StringBuilder sb = new StringBuilder();
            for (char c : splittedEmail[0].toCharArray()) {
                if (c == '+') break;
                if (c == '.') continue;
                sb.append(c);
            }
            sb.append("@").append(splittedEmail[1]);
            result.add(sb.toString());
        }
        return result.size();
    }

}
