package Leetcode;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/*
   Min braces to remove to make valid
 */
public class MinimumRemoveToMakeValidParentheses {

    public static void main(String[] args) {
        System.out.println(minRemoveToMakeValid("))((").isEmpty());
        System.out.println(minRemoveToMakeValid("a)b(c)d").equals("ab(c)d"));
        System.out.println(minRemoveToMakeValid("(a(b(c)d)").equals("a(b(c)d)"));
        System.out.println(minRemoveToMakeValidNoStack("))((").isEmpty());
        System.out.println(minRemoveToMakeValidNoStack("a)b(c)d").equals("ab(c)d"));
        System.out.println(minRemoveToMakeValidNoStack("(a(b(c)d)").equals("(a(bc)d)"));
    }

    // remove the minimum number of parentheses ( '(' or ')', in any positions ) so that
    // the resulting parentheses string is valid and return any valid string.
    public static String minRemoveToMakeValid(String s) {
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            switch (s.charAt(i)) {
                case '(':
                    stack.push(i);
                    break;
                case ')':
                    if (!stack.isEmpty()) {
                        if (s.charAt(stack.peek()) == '(') {
                            stack.pop();
                            continue;
                        }
                    }
                    stack.push(i);
                    break;
            }
        }
        if (stack.size() == s.length()) return "";
        Set<Integer> set = new HashSet<>(stack);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (set.contains(i)) continue;
            sb.append(s.charAt(i));
        }
        return sb.toString();
    }

    // non stack solution:
    // 1. remove invalid close parenthesis
    // 2. remove invalid open parenthesis
    public static String minRemoveToMakeValidNoStack(String s) {
        int open = 0;
        char[] array = s.toCharArray();
        for (int i = 0; i < array.length; i++) {
            if (array[i] == '(') {
                open++;
            }
            if (array[i] == ')') {
                if (open == 0) {
                    array[i] = ' ';
                } else {
                    open--;
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        // we go from the end to cover this case: "())()(((" -> "()()"
        for (int i = array.length - 1; i >= 0; i--) {
            if (array[i] != ' ') {
                if (open > 0 && array[i] == '(') {
                    open--;
                    continue;
                }
                sb.insert(0, array[i]);
            }
        }
        return sb.toString();
    }

    private static StringBuilder removeInvalidClosing(CharSequence string, char open, char close) {
        StringBuilder sb = new StringBuilder();
        int balance = 0;
        for (int i = 0; i < string.length(); i++) {
            char c = string.charAt(i);
            if (c == open) {
                balance++;
            } if (c == close) {
                if (balance == 0) continue;
                balance--;
            }
            sb.append(c);
        }
        return sb;
    }

    public static String minRemoveToMakeValidNonStack2(String s) {
        StringBuilder result = removeInvalidClosing(s, '(', ')');
        result = removeInvalidClosing(result.reverse(), ')', '(');
        return result.reverse().toString();
    }

}
