package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 10/21/19.
 */
public class StringCompression {

    public static void main(String[] args) {
        char[] chars = {'a', 'a', 'a', 'b', 'b', 'a', 'a'};
        int compressedLength = compress(chars);
        assert Arrays.equals(Arrays.copyOf(chars, compressedLength), new char[]{'a', '3', 'b', '2', 'a', '2'});

        chars = new char[]{'a', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b'};
        compressedLength = compress(chars);
        assert Arrays.equals(Arrays.copyOf(chars, compressedLength), new char[]{'a', 'b', '1', '2'});

        chars = new char[]{'a', 'a', 'b', 'b', 'c', 'c', 'c'};
        compressedLength = compress(chars);
        assert Arrays.equals(Arrays.copyOf(chars, compressedLength), new char[]{'a', '2', 'b', '2', 'c', '3'});

        chars = new char[]{'a', 'a', 'a', 'a', 'a', 'a', 'c'};
        compressedLength = compress(chars);
        assert Arrays.equals(Arrays.copyOf(chars, compressedLength), new char[]{'a', '6', 'c', '1'});
    }

    public static int compress(char[] chars) {
        int start = 0, count = 0;
        for (int end = 0; end < chars.length; end++) {
            count++;
            if (end == chars.length - 1 || chars[end] != chars[end + 1]) {
                chars[start++] = chars[end];
                if (count != 1) {
                    String num = String.valueOf(count);
                    for (int i = 0; i < num.length(); i++) {
                        chars[start++] = num.charAt(i);
                    }
                }
                count = 0;
            }
        }
        return start;
    }

}
