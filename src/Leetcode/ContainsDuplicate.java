package Leetcode;

/**
 * @author sfedosov on 4/15/18.
 */
public class ContainsDuplicate {

    public static void main(String[] args) {
        System.out.println(containsNearbyAlmostDuplicate(new int[]{1, 3, 1}, 1, 1));
    }

    public static boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
        for (int i = 0, j = nums.length - 1; i < nums.length && j > 0; i++, j--) {
            if (Math.abs(i - j) <= k && Math.abs(nums[i] - nums[j]) <= t) {
                return true;
            }
        }
        return false;
    }

}
