package Leetcode;

import java.util.Arrays;
import java.util.Collections;
import java.util.PriorityQueue;

/**
 * @author sfedosov on 3/15/20.
 */
public class MedianSlidingWindow {

    public static void main(String[] args) {
        assert Arrays.equals(medianSlidingWindow(new int[]{1, 3, -1, -3, 5, 3, 6, 7}, 3),
                new double[]{1.0d, -1.0d, -1.0d, 3.0d, 5.0d, 6.0d});
    }

    public static double[] medianSlidingWindow(int[] nums, int k) {
        PriorityQueue<Long> minHeap = new PriorityQueue<>();
        PriorityQueue<Long> maxHeap = new PriorityQueue<>(Collections.reverseOrder());
        double[] result = new double[nums.length - k + 1];
        boolean even = k % 2 == 0;
        int i = 0;
        for (int x : nums) {
            if (i >= 1) {
                if (!maxHeap.remove((long) nums[i - 1])) minHeap.remove((long) nums[i - 1]);
            }
            maxHeap.offer((long) x);
            minHeap.offer(maxHeap.poll());
            if (maxHeap.size() < minHeap.size()) {
                maxHeap.offer(minHeap.poll());
            }
            if (maxHeap.size() + minHeap.size() == k) {
                result[i++] = even ? (double) (maxHeap.peek() + minHeap.peek()) / 2 : maxHeap.peek();
            }
        }
        return result;
    }

}
