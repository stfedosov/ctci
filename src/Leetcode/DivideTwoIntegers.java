package Leetcode;

public class DivideTwoIntegers {

    // divide two integers
    // Given two integers dividend and divisor -
    // divide two integers without using multiplication, division, and mod operator.

    public static void main(String[] args) {
        assert divide(-2147483647, -1) == 2147483647;
        assert divide(-2147483648, -3) == 715827882;
        assert divide(10, 3) == 3;
    }

    /*
    Time Complexity: O(log(n)).
    As we loop over the bits of our dividend, performing an O(1) operation each time,
    the time complexity is just the number of bits of the dividend: O(log(n)).
     */
    public static int divide(int dividend, int divisor) {
        if (divisor == -1 && dividend == Integer.MIN_VALUE) return Integer.MAX_VALUE;
        boolean isNeg = !(dividend < 0 && divisor < 0) && (dividend < 0 || divisor < 0);
        dividend = Math.abs(dividend);
        divisor = Math.abs(divisor);
        int count = 0;
        while (dividend - divisor >= 0) {
            int x = 0;
            while (dividend - (divisor << 1 << x) >= 0) {
                x++;
            }
            count += 1 << x;
            dividend -= divisor << x;
        }
        return isNeg ? (-1) * count : count;
    }
}
