package Leetcode;

import java.util.TreeMap;

/**
 * @author sfedosov on 1/2/20.
 */
public class TopVotedCandidate {

    private TreeMap<Integer, Integer> map;

    public TopVotedCandidate(int[] persons, int[] times) {
        map = new TreeMap<>();
        int[] count = new int[persons.length + 1];
        int max = 0;
        for (int i = 0; i < persons.length; i++) {
            ++count[persons[i]];
            if (max <= count[persons[i]]) {
                max = count[persons[i]];
                map.put(times[i], persons[i]);
            }
        }
    }

    public int q(int t) {
        return map.floorEntry(t).getValue();
    }

    public static void main(String[] args) {
        TopVotedCandidate topVotedCandidate = new TopVotedCandidate(
                new int[]{0, 1, 1, 0, 0, 1, 0},
                new int[]{0, 5, 10, 15, 20, 25, 30});
        assert topVotedCandidate.q(3) == 0;
        assert topVotedCandidate.q(12) == 1;
        assert topVotedCandidate.q(25) == 1;
        assert topVotedCandidate.q(15) == 0;
        assert topVotedCandidate.q(24) == 0;
        assert topVotedCandidate.q(8) == 1;

        topVotedCandidate = new TopVotedCandidate(new int[]{0, 0, 1, 1, 2}, new int[]{0, 67, 69, 74, 87});
        assert topVotedCandidate.q(4) == 0;
        assert topVotedCandidate.q(62) == 0;
        assert topVotedCandidate.q(100) == 1;
        assert topVotedCandidate.q(88) == 1;
        assert topVotedCandidate.q(70) == 0;
        assert topVotedCandidate.q(73) == 0;
        assert topVotedCandidate.q(22) == 0;
        assert topVotedCandidate.q(75) == 1;
        assert topVotedCandidate.q(29) == 0;
        assert topVotedCandidate.q(10) == 0;
    }


}
