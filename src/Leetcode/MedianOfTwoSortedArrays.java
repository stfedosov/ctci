package Leetcode;

public class MedianOfTwoSortedArrays {

    // Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.
    // The overall run time complexity should be O(log(m+n)).
    public static void main(String[] args) {
        assert findMedianSortedArrays(new int[]{1, 2}, new int[]{3, 4}) == 2.5d;
        assert findMedianSortedArrays(new int[]{1, 3}, new int[]{2}) == 2.0d;
        System.out.println(findMedianSortedArrays(new int[]{100000}, new int[]{100001}));
    }

    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
        // This ensures we always binary search the smaller array, optimizing the time complexity.
        if (nums1.length > nums2.length) {
            int[] temp = nums1;
            nums1 = nums2;
            nums2 = temp;
        }

        int m = nums1.length, n = nums2.length, low = 0, high = m;
        boolean evenTotalNumberOfElements = (m + n) % 2 == 0;

        while (low <= high) {
            int partitionForNums1 = (low + high) / 2;
            // when we deal with odd total number of elements, we have to add +1
            // to ensure the left partition has that one extra element, which is a median
            int totalElementsMedian = evenTotalNumberOfElements ? (m + n) / 2 : (m + n + 1) / 2;
            int partitionForNums2 = totalElementsMedian - partitionForNums1;

            int maxForNums1 = (partitionForNums1 == 0) ? Integer.MIN_VALUE : nums1[partitionForNums1 - 1];
            int maxForNums2 = (partitionForNums2 == 0) ? Integer.MIN_VALUE : nums2[partitionForNums2 - 1];

            int minForNums1 = (partitionForNums1 == m) ? Integer.MAX_VALUE : nums1[partitionForNums1];
            int minForNums2 = (partitionForNums2 == n) ? Integer.MAX_VALUE : nums2[partitionForNums2];

            // if current partitioning is correct and symmetrical
            //
            // ...__|__|__maxForNums1__|P|__minForNums1__|__|__...
            //
            // ...__|__|__maxForNums2__|P|__minForNums2__|__|__...
            if (maxForNums1 <= minForNums2 && maxForNums2 <= minForNums1) {
                if (evenTotalNumberOfElements) {
                    return (Math.max(maxForNums1, maxForNums2) + Math.min(minForNums1, minForNums2)) / 2.0;
                } else {
                    return Math.max(maxForNums1, maxForNums2);
                }
            } else if (maxForNums1 > minForNums2) {
                high = partitionForNums1 - 1;
            } else {
                low = partitionForNums1 + 1;
            }
        }

        throw new IllegalArgumentException("Input arrays are not sorted.");
    }

}
