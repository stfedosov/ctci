package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 11/2/19.
 */
public class ClimbStairs {

    public int climbStairs(int n) {
        int[] cache = new int[n + 1];
        Arrays.fill(cache, -1);
        return helper(n, cache);
    }

    private int helper(int n, int[] cache) {
        if (n < 0) return 0;
        else if (n == 0) return 1;
        else if (cache[n] != -1) return cache[n];
        cache[n] = helper(n - 1, cache) + helper(n - 2, cache);
        return cache[n];
    }

}
