package Leetcode;

/**
 * @author sfedosov on 4/19/19.
 */
public class BinaryGap {

    public static void main(String[] args) {
        assert binaryGap(22) == 2;
        assert binaryGap(5) == 2;
        assert binaryGap(6) == 1;
        assert binaryGap(8) == 0;
    }

    public static int binaryGap(int N) {
        int last = -1;
        int max = 0;
        for (int i = 0; i < 32; i++) {
            if (((N >> i) & 1) > 0) {
                if (last >= 0) max = Math.max(max, i - last);
                last = i;
            }
        }
        return max;
    }

}
