package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
Given a collection of candidate numbers (candidates) and a target number (target),
find all unique combinations in candidates where the candidate numbers sum to target.
 */
public class CombinationSum2 {

    public static void main(String[] args) {
        System.out.println(combinationSum2(new int[]{10, 1, 1, 7, 6, 2, 5}, 8));
    }

    public static List<List<Integer>> combinationSum2(int[] candidates, int target) {
        List<List<Integer>> list = new ArrayList<>();
        Arrays.sort(candidates);
        backtrack(list, new ArrayList<>(), candidates, target, 0);
        return list;
    }

    private static void backtrack(List<List<Integer>> list, List<Integer> tempList, int[] candidates, int remain, int start) {

        if (remain < 0) return;
        else if (remain == 0) list.add(new ArrayList<>(tempList));
        else {
            Set<Integer> set = new HashSet<>();
            for (int i = start; i < candidates.length; i++) {
                if (set.add(candidates[i])) { // skip duplicates
                    tempList.add(candidates[i]);
                    backtrack(list, tempList, candidates, remain - candidates[i], i + 1);
                    tempList.remove(tempList.size() - 1);
                }
            }
        }
    }
}
