package Leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sfedosov on 2/23/20.
 */
public class MagicDictionary {

    private Map<Integer, List<String>> map;

    public static void main(String[] args) {
        MagicDictionary dictionary = new MagicDictionary();
        dictionary.buildDict(new String[]{"hello", "leetcode"});
        assert !dictionary.search("hello");
        assert dictionary.search("hhllo");
        assert !dictionary.search("hell");
        assert !dictionary.search("leetcoded");
    }

    public MagicDictionary() {
        map = new HashMap<>();
    }

    public void buildDict(String[] dict) {
        for (String word : dict) {
            map.computeIfAbsent(word.length(), x -> new ArrayList<>()).add(word);
        }
    }

    /**
     * Returns if there is any word in the trie that equals to the given word after modifying exactly one character
     */
    public boolean search(String word) {
        if (!map.containsKey(word.length())) return false;

        for (String w : map.get(word.length())) {
            int count = 0;
            for (int i = 0; i < w.length(); i++) {
                if (w.charAt(i) != word.charAt(i)) {
                    count++;
                    if (count > 1) break;
                }
            }
            if (count == 1) return true;
        }
        return false;
    }
}