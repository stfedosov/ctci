package Leetcode;

/**
 * @author sfedosov on 3/19/18.
 */
public class MaxSubArray {

    public static void main(String[] args) {
        assert maxSubArray(new int[]{-2, 1, -3, 4, -1, 2, 1, -5, 4}) == 6;
    }

    public static int maxSubArray(int[] nums) {
        int maxSoFar = Integer.MIN_VALUE, tmp_sum = 0;
        for (int x : nums) {
            tmp_sum = Math.max(tmp_sum + x, x);
            maxSoFar = Math.max(maxSoFar, tmp_sum);
        }
        return maxSoFar;
    }
}
