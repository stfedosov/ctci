package Leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author sfedosov on 12/29/17.
 */
public class FindLargestValueInEachTreeRow {

    public static void main(String[] args) {
        TreeNode treeNode = new TreeNode(Integer.MIN_VALUE);
        TreeNode treeNode3 = new TreeNode(3);
        treeNode3.left = new TreeNode(5);
        treeNode3.right = new TreeNode(3);
        treeNode.left = treeNode3;
        TreeNode treeNode2 = new TreeNode(2);
        treeNode2.right = new TreeNode(9);
        treeNode.right = treeNode2;
        System.out.println(largestValues(treeNode));
    }


    public static List<Integer> largestValues(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<>();
        List<Integer> res = new ArrayList<>();
        queue.add(root);
        int queueSize = root == null ? 0 : 1;
        while (queueSize > 0) {
            int largestElement = Integer.MIN_VALUE;
            for (int i = 0; i < queueSize; i++) {
                TreeNode cur = queue.poll();
                largestElement = Math.max(cur.val, largestElement);
                if (cur.left != null) queue.add(cur.left);
                if (cur.right != null) queue.add(cur.right);
            }
            res.add(largestElement);
            queueSize = queue.size();
        }
        return res;
    }
}