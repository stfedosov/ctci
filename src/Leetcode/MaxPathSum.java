package Leetcode;

/**
 * @author sfedosov on 7/22/19.
 */
public class MaxPathSum {

    int max = Integer.MIN_VALUE;

    public int maxPathSum(TreeNode root) {
        maxPathSum2(root);
        return max;
    }

    private int maxPathSum2(TreeNode root) {
        if (root == null) return 0;
        int leftSum = Math.max(maxPathSum2(root.left), 0);
        int rightSum = Math.max(maxPathSum2(root.right), 0);
        max = Math.max(max, leftSum + rightSum + root.val);
        return root.val + Math.max(leftSum, rightSum);
    }

}
