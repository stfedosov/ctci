package Leetcode;

import Leetcode.RotateList.ListNode;

/**
 * @author sfedosov on 1/30/19.
 */
public class IntersectionNode {

    public static void main(String[] args) {
        ListNode root1 = new ListNode(4);
        ListNode listNode1 = new ListNode(1);
        root1.next = listNode1;
        ListNode listNode8 = new ListNode(8);
        listNode1.next = listNode8;
        ListNode listNode4 = new ListNode(4);
        listNode8.next = listNode4;
        listNode4.next = new ListNode(5);
        ListNode root2 = new ListNode(5);
        ListNode listNode5 = new ListNode(0);
        root2.next = listNode5;
        ListNode listNode0 = new ListNode(0);
        listNode5.next = listNode0;
        ListNode listNode11 = new ListNode(1);
        listNode0.next = listNode11;
        listNode11.next = listNode8;
        System.out.println(getIntersectionNode(root1, root2));
    }

    public static ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        ListNode iter1 = headA;
        ListNode iter2 = headB;
        while(iter1 != iter2) {
            iter1 = iter1 == null ? headA : iter1.next;
            iter2 = iter2 == null ? headB : iter2.next;
        }
        return iter1;
    }

}
