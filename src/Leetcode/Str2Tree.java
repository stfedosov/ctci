package Leetcode;

/**
 * @author sfedosov on 9/20/19.
 */
public class Str2Tree {

    public static void main(String[] args) {
        System.out.println(str2tree("4(2(3)(1))(6(5))"));
        System.out.println(str2tree("-51(232)(434)"));
    }

    private static int idx = 0;

    public static TreeNode str2tree(String s) {
        idx = 0;
        return buildTree(s);
    }

    private static TreeNode buildTree(String s) {
        //This is our condition to come out of recursion
        if (idx >= s.length()) return null;

        // If current character is closed bracket ')'
        // it means child of the root which called recursion is null
        // Also increment the index, to process the next character.
        if (s.charAt(idx) == ')') {
            idx++;
            return null;
        }

        var root = new TreeNode(getValue(s));

        //Check if we have not reached end of string,
        //If we have open bracket, means we need to add left child first.
        //So, call buildTree to add left child from current position.
        if (idx < s.length() && s.charAt(idx) == '(') {
            idx++;
            root.left = buildTree(s);
        }

        //Check if we have not reached end of string,
        //If we have open bracket, means we need to add right child.
        //So, call buildTree to add right child from current position.
        if (idx < s.length() && s.charAt(idx) == '(') {
            idx++;
            root.right = buildTree(s);
        }

        //Increase the index, to get pass the closed bracket for the current sub-tree.
        idx++;
        return root;

    }

    private static int getValue(String s) {
        var val = new StringBuilder();

        while (idx < s.length() && s.charAt(idx) != ')' && s.charAt(idx) != '(')
            val.append(s.charAt(idx++));

        return Integer.parseInt(val.toString());
    }

}
