package Leetcode;

import java.util.Arrays;

public class CandyCrush {

    // If three or more candies of the same type are adjacent vertically or horizontally,
    // "crush" them all at the same time - these positions become empty

    public static void main(String[] args) {
        assert Arrays.deepEquals(candyCrush(
                new int[][]{
                        {110, 5,   112, 113, 114},
                        {210, 211, 5,   213, 214},
                        {310, 311, 3,   313, 314},
                        {410, 411, 412, 5,   414},
                        {5,   1,   512, 3,   3},
                        {610, 4,   1,   613, 614},
                        {710, 1,   2,   713, 714},
                        {810, 1,   2,   1,   1},
                        {1,   1,   2,   2,   2},
                        {4,   1,   4,   4,   1014}}),
                new int[][]{
                        {0,   0,   0,   0,   0},
                        {0,   0,   0,   0,   0},
                        {0,   0,   0,   0,   0},
                        {110, 0,   0,   0,   114},
                        {210, 0,   0,   0,   214},
                        {310, 0,   0,   113, 314},
                        {410, 0,   0,   213, 414},
                        {610, 211, 112, 313, 614},
                        {710, 311, 412, 613, 714},
                        {810, 411, 512, 713, 1014}});
    }

    // idea is to mark candies as a negative (opposite) value, so that we don't have to maintain extra data structure

    public static int[][] candyCrush(int[][] board) {
        while (needToCrash(board)) {
            crash(board);
            shiftValues(board);
        }
        return board;
    }

    public static boolean needToCrash(int[][] board) {
        boolean shouldCrash = false;
        // check columns
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length - 2; j++) {
                int absVal = Math.abs(board[i][j]);
                if (absVal != 0 && absVal == Math.abs(board[i][j + 1]) && absVal == Math.abs(board[i][j + 2])) {
                    shouldCrash = true;
                    board[i][j] = board[i][j + 1] = board[i][j + 2] = -absVal;
                }
            }
        }
        // check rows
        for (int i = 0; i < board[0].length; i++) {
            for (int j = 0; j < board.length - 2; j++) {
                int absVal = Math.abs(board[j][i]);
                if (absVal != 0 && absVal == Math.abs(board[j + 1][i]) && absVal == Math.abs(board[j + 2][i])) {
                    shouldCrash = true;
                    board[j][i] = board[j + 1][i] = board[j + 2][i] = -absVal;
                }
            }
        }
        return shouldCrash;
    }

    public static void crash(int[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] < 0) board[i][j] = 0;
            }
        }
    }

    public static void shiftValues(int[][] board) {
        for (int i = 0; i < board[0].length; i++) {
            int count = board.length - 1;
            for (int j = board.length - 1; j >= 0; j--) {
                if (board[j][i] != 0) {
                    board[count][i] = board[j][i];
                    count--;
                }
            }
            while (count >= 0) board[count--][i] = 0;
        }
    }

}
