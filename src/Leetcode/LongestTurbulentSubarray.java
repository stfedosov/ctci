package Leetcode;

/**
 * @author sfedosov on 1/21/19.
 */
public class LongestTurbulentSubarray {

    public static void main(String[] args) {
        System.out.println(maxTurbulenceSize(new int[]{9, 4, 2, 10, 7, 8, 8, 1, 9}));
        System.out.println(maxTurbulenceSize(new int[]{4, 8, 12, 16}));
    }

    public static int maxTurbulenceSize(int[] A) {
        if (A.length == 0) return 0;
        int n = A.length, maxLen = 0;
        int[][] state = new int[n][2];
        for (int i = 1; i < n; i++) {
            if (A[i - 1] < A[i]) {
                state[i][0] = state[i - 1][1] + 1;
                maxLen = Math.max(maxLen, state[i][0]);
            } else if (A[i - 1] > A[i]) {
                state[i][1] = state[i - 1][0] + 1;
                maxLen = Math.max(maxLen, state[i][1]);
            }
        }
        return maxLen + 1;
    }

}
