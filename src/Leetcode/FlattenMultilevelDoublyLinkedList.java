package Leetcode;

/**
 * @author sfedosov on 7/6/19.
 */
public class FlattenMultilevelDoublyLinkedList {

    public static void main(String[] args) {

    }

    static class Node {
        public int val;
        public Node prev;
        public Node next;
        public Node child;

        public Node() {
        }

        public Node(int _val,
                    Node _prev,
                    Node _next,
                    Node _child) {
            val = _val;
            prev = _prev;
            next = _next;
            child = _child;
        }
    }

    private Node result = new Node(-1, null, null, null);
    private Node prev = null;

    public Node flatten(Node head) {
        Node resultIter = result;
        flattenHelper(head);
        return resultIter.next;
    }

    public void flattenHelper(Node head) {
        while (head != null) {
            Node tmpNode = new Node(head.val, prev, result.next, null);
            prev = tmpNode;
            result.next = tmpNode;
            result = result.next;

            if (head.child != null) {
                flattenHelper(head.child);
            }
            head = head.next;
        }
    }

}
