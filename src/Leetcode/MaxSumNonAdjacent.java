package Leetcode;

/**
 * @author sfedosov on 11/10/19.
 */
public class MaxSumNonAdjacent {

    public static void main(String[] args) {
        int[] v1 = new int[]{1, 6, 10, 14, 50, -20, -5, -10};
        System.out.println("Max non adjacent sum: " + find_max_sum_nonadjacent(v1));
    }

    private static int find_max_sum_nonadjacent(int[] a) {
        int[] dp = new int[a.length];
        dp[0] = a[0];
        dp[1] = Math.max(a[0], a[1]);
        for (int i = 2; i <= a.length - 1; i++) {
            dp[i] = Math.max(a[i] + dp[i - 2], dp[i - 1]);
        }
        return dp[a.length - 1];
    }

}
