package Leetcode;

import java.util.*;
import java.lang.*;
import java.util.List;

/**
 * @author sfedosov on 7/22/19.
 */
public class InterleaveLists {

    private static List<Integer> interleaveLists(List<List<Integer>> lists) {
        List<Integer> interleaveList = new ArrayList<>();

        int maxSize = 0;

        for (List<Integer> list : lists) {
            maxSize = Math.max(maxSize, list.size());
        }

        int index = 0;
        while (index < maxSize) {
            for (List<Integer> list : lists) {
                if (index < list.size()) {
                    interleaveList.add(list.get(index));
                }
            }
            index++;
        }
        return interleaveList;

    }

    public static List<List<Integer>> twoDArrayToList(int[][] twoDArray) {
        List<List<Integer>> list = new ArrayList<>();
        for (int[] array : twoDArray) {
            List<Integer> tmp = new ArrayList<>();
            for (int x : array) {
                tmp.add(x);
            }
            list.add(tmp);
        }
        return list;
    }

    public static void main(String[] args) throws java.lang.Exception {
        int[][] arr = {{1, 2, 3}, {9, 0}, {5}, {-4, -5, -2, -3, -1, 5, 2}};
        List<List<Integer>> ll = twoDArrayToList(arr);
        System.out.println(interleaveLists(ll));
    }
}