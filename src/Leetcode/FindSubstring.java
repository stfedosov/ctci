package Leetcode;

/**
 * @author sfedosov on 3/19/18.
 */
public class FindSubstring {

    public static void main(String[] args) {
        assert subStringIndex("aaS", "aS") == 1;
    }

    private static int subStringIndex(String str, String substr) {
        int subStrLen = substr.length();
        int strLen = str.length();
        int j = 0;

        if (subStrLen >= 1) {
            for (int i = 0; i < strLen; i++) {              // iterate through main string
                if (str.charAt(i) == substr.charAt(j)) {    // check substring
                    j++;                                    // iterate
                    if (j == subStrLen) {                   // when to stop
                        return i - (subStrLen - 1); //found substring. As i is currently at the end of our substr so sub subStrLen
                    }
                } else {
                    i -= j;
                    j = 0;
                }
            }
        }
        return -1;
    }

}
