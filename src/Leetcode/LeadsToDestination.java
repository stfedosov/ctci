package Leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LeadsToDestination {

    public static void main(String[] args) {
        assert !leadsToDestination(4, new int[][]{{0, 1}, {0, 3}, {1, 2}, {2, 1}}, 0, 3);
        assert leadsToDestination(4, new int[][]{{0, 1}, {0, 2}, {1, 3}, {2, 3}}, 0, 3);
    }

    public static boolean leadsToDestination(int n, int[][] edges, int source, int destination) {
        Map<Integer, List<Integer>> graph = new HashMap<>();
        for (int[] edge : edges) {
            graph.computeIfAbsent(edge[0], x -> new ArrayList<>()).add(edge[1]);
        }
        return isAllPathsToDest(graph, source, destination, new boolean[n], new boolean[n]);
    }

    private static boolean isAllPathsToDest(Map<Integer, List<Integer>> graph, int source, int destination, boolean[] visited, boolean[] visiting) {
        if (visiting[source]) return false;
        if (visited[source] || source == destination && graph.get(source) == null) return true;
        if (graph.get(source) == null) return false;
        visited[source] = true;
        visiting[source] = true;
        for (int neighbor : graph.get(source)) {
            if (!isAllPathsToDest(graph, neighbor, destination, visited, visiting)) return false;
        }
        visiting[source] = false;
        return true;
    }
}