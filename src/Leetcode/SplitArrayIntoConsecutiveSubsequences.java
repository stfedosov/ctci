package Leetcode;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @author sfedosov on 1/16/20.
 */
public class SplitArrayIntoConsecutiveSubsequences {

    public static void main(String[] args) {
        assert isPossible(new int[]{1, 2, 3, 3, 4, 5}); // [1, 2, 3] and [3, 4, 5]
        assert isPossible(new int[]{1, 2, 3, 3, 4, 4, 5, 5}); // [1, 2, 3, 4, 5] and [3, 4, 5]
        assert !isPossible(new int[]{1, 2, 3, 4, 4, 5}); // [1, 2, 3, 4] and [4, 5], the latter is less than 3 in size -> false
    }

    public static boolean isPossible(int[] nums) {
        Queue<Interval> q = new PriorityQueue<>(
                Comparator.comparingInt((Interval a) -> a.num).thenComparingInt(a -> a.length));
        for (int num : nums) {
            while (!q.isEmpty() && q.peek().num + 1 < num) {
                if (q.poll().length < 3) return false;
            }
            if (q.isEmpty() || q.peek().num == num) {
                q.offer(new Interval(num));
            } else {
                Interval polled = q.poll();
                polled.num = num;
                polled.length++;
                q.offer(polled);
            }
        }
        while (!q.isEmpty()) {
            Interval pol = q.poll();
            if (pol.length < 3) return false;
        }
        return true;
    }

    static class Interval {
        int num;
        int length;

        Interval(int num) {
            this.num = num;
            this.length++;
        }

        @Override
        public String toString() {
            return num + "@" + length;
        }
    }

}
