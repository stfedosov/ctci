package Leetcode;

public class FindPeakElement {

    public static void main(String[] args) {
        assert findPeakElement(new int[]{1, 2, 3, 1}) == 2;
        assert findPeakElement(new int[]{1, 2, 1, 3, 5, 6, 4}) == 1;
        assert findPeakElement(new int[]{1, 2, 3}) == 2;
        assert findPeakElement(new int[]{1, 2}) == 1;

        assert findPeakElementBinarySearch(new int[]{1, 2, 3, 1}) == 2;
        assert findPeakElementBinarySearch(new int[]{1, 2, 1, 3, 5, 6, 4}) == 1;
        assert findPeakElementBinarySearch(new int[]{1, 2, 3}) == 2;
        assert findPeakElementBinarySearch(new int[]{1, 2}) == 1;
    }

    public static int findPeakElement(int[] nums) {
        int start = 0, end = nums.length - 1;
        while (start < end) {
            int mid = start + (end - start) / 2;
            int prev = mid - 1 >= 0 ? nums[mid - 1] : Integer.MIN_VALUE;
            int next = mid + 1 < nums.length ? nums[mid + 1] : Integer.MIN_VALUE;
            if (prev < nums[mid] && nums[mid] > next) { // peak has been found
                return mid;
            } else if (nums[mid] < next) {
                // mid is definitely not a peak, so skipping it in the search space
                start = mid + 1;
            } else {
                // keep the mid in the search space since it might be the peak
                end = mid;
            }
        }
        return start;
    }

    // O(log(n))
    // 3 cases:
    // a. the array increases all the way up to the top - peak is at the end
    // b. the array decreases from the beginning - peak is in the beginning
    // c. the array increases for a while and then decreases
    public static int findPeakElementBinarySearch(int[] nums) {
        int N = nums.length;
        if (N == 1) {
            return 0;
        }

        int left = 0, right = N - 1;
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] < nums[mid + 1]) {
                // that means a peak element HAS to exist on the right half of that array as every number is unique
                // 1. the numbers keep increasing on the right side, and the peak will be the last element.
                // 2. the numbers stop increasing and there is a 'dip',
                // or there exists somewhere a number such that nums[y] < nums[y-1], which means num[y] is a peak element.
                left = mid + 1;
            } else {
                right = mid;
            }
        }

        return (left == N - 1 || nums[left] > nums[left + 1]) ? left : right;
    }

}
