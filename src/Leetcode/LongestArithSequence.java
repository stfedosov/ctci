package Leetcode;

import java.util.HashMap;
import java.util.Map;

public class LongestArithSequence {

    public static void main(String[] args) {
        System.out.println(longestArithSeqLength(new int[]{20, 1, 15, 3, 10, 5, 8}));
        System.out.println(longestArithSeqLength(new int[]{24, 13, 1, 100, 0, 94, 3, 0, 3}));
    }

    public static int longestArithSeqLength(int[] A) {
        Map<Integer, Map<Integer, Integer>> map = new HashMap<>();
        int max = 1;
        Map<Integer, Integer> s = new HashMap<>();
        s.put(A[0], 1);
        map.put(0, s);
        for (int i = 1; i < A.length; i++) {
            Map<Integer, Integer> tmp = new HashMap<>();
            for (int j = 0; j < i; j++) {
                int diff = A[i] - A[j];
                tmp.put(diff, 2);
                Map<Integer, Integer> previous = map.get(j);
                if (previous.containsKey(diff)) {
                    tmp.put(diff, previous.get(diff) + 1);
                }
                max = Math.max(max, tmp.get(diff));
            }
            map.put(i, tmp);
        }
        return max;
    }
}
