package Leetcode;

import java.util.Arrays;
import java.util.Comparator;

public class IsAlienSorted {

    public static void main(String[] args) {
        assert isAlienSorted(new String[]{"hello", "leetcode"}, "hlabcdefgijkmnopqrstuvwxyz");
        assert !isAlienSorted(new String[]{"word", "world", "row"}, "worldabcefghijkmnpqstuvxyz");
        assert !isAlienSorted(new String[]{"apple", "app"}, "abcdefghijklmnopqrstuvwxyz");
        System.out.println(sort("babbac", "bca"));
    }

    private static String sort(String input, String order) {
        Character[] chars = new Character[input.length()];
        int x = 0;
        for (char c : input.toCharArray()) chars[x++] = c;
        int[] ints = new int[26];
        for (int i = 0; i < order.length(); i++) {
            ints[order.charAt(i) - 'a'] = i;
        }
        Arrays.sort(chars, Comparator.comparingInt(c -> ints[c - 'a']));
        StringBuilder sb = new StringBuilder();
        for (Character c : chars) sb.append(c);
        return sb.toString();
    }

    private static final int[] alphabet_standard = new int[26];

    // Time complexity: O(total number of characters in words array)
    // Space complexity: O(1)
    public static boolean isAlienSorted(String[] words, String order) {
        for (int i = 0; i < order.length(); i++)
            alphabet_standard[order.charAt(i) - 'a'] = i;
        for (int i = 1; i < words.length; i++)
            if (compare(words[i - 1], words[i]) > 0)
                return false;
        return true;
    }

    static int compare(String s1, String s2) {
        int n = s1.length(), m = s2.length(), cmp = 0;
        int i = 0, j = 0;
        while (i < n && j < m && cmp == 0) {
            cmp = alphabet_standard[s1.charAt(i) - 'a'] - alphabet_standard[s2.charAt(j) - 'a'];
            i++;
            j++;
        }
        return cmp == 0 ? n - m : cmp;
    }
}
