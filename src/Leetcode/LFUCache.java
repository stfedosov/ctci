package Leetcode;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @author sfedosov on 8/23/19.
 */
public class LFUCache {

    public static void main(String[] args) {
        LFUCache lfuCache = new LFUCache(3);
        lfuCache.put(2, 2);
        lfuCache.put(1, 1);
        assert lfuCache.get(2) == 2;
        assert lfuCache.get(1) == 1;
        assert lfuCache.get(2) == 2;
        lfuCache.put(3, 3);
        lfuCache.put(4, 4);
        assert lfuCache.get(3) == -1;
        assert lfuCache.get(2) == 2;
        assert lfuCache.get(1) == 1;
        assert lfuCache.get(4) == 4;

        //---------------------------------

        lfuCache = new LFUCache(2);
        assert lfuCache.get(2) == -1;
        lfuCache.put(2, 6);
        assert lfuCache.get(1) == -1;
        lfuCache.put(1, 5);
        lfuCache.put(1, 2);

        assert lfuCache.get(1) == 2;
        assert lfuCache.get(2) == 6;

        //---------------------------------

        lfuCache = new LFUCache(3);
        lfuCache.put(1, 1);
        lfuCache.put(2, 2);
        lfuCache.put(3, 3);
        lfuCache.put(4, 4);
        assert lfuCache.get(4) == 4;
        assert lfuCache.get(3) == 3;
        assert lfuCache.get(2) == 2;
        assert lfuCache.get(1) == -1;
        lfuCache.put(5, 5);
        assert lfuCache.get(1) == -1;
        assert lfuCache.get(2) == 2;
        assert lfuCache.get(3) == 3;
        assert lfuCache.get(4) == -1;
        assert lfuCache.get(5) == 5;
    }

    private Map<Integer, Node> map;
    private int capacity;
    private int time = 0;
    private Queue<Node> minHeap;

    public LFUCache(int capacity) {
        this.capacity = capacity;
        this.map = new HashMap<>();
        this.minHeap = new PriorityQueue<>((o1, o2) -> {
            int freqComp = o1.freq - o2.freq;
            return freqComp == 0 ? o1.time - o2.time : freqComp;
        });
    }

    public int get(int key) {
        if (map.containsKey(key)) {
            Node old = map.get(key);
            minHeap.remove(old);
            Node newNode = new Node(key, old.value, old.freq + 1, time++);
            map.put(key, newNode);
            minHeap.offer(newNode);
            return newNode.value;
        }
        return -1;
    }

    public void put(int key, int value) {
        if (capacity == 0) {
            return;
        }
        Node newNode;
        if (map.containsKey(key)) {
            Node old = map.get(key);
            minHeap.remove(old);
            newNode = new Node(key, value, old.freq + 1, time++);
        } else {
            if (map.size() == capacity) {
                Node old = minHeap.poll();
                map.remove(old.key);
            }
            newNode = new Node(key, value, 1, time++);
        }
        map.put(key, newNode);
        minHeap.offer(newNode);
    }

    class Node {
        int freq, time, key, value;

        Node(int key, int value, int freq, int time) {
            this.key = key;
            this.value = value;
            this.time = time;
            this.freq = freq;
        }
    }

}
