package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * @author sfedosov on 12/19/19.
 */
public class FindDiagonalOrder {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(findDiagonalOrder(new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        })));
        System.out.println(Arrays.toString(findDiagonalOrder(new int[][]{{2, 3}})));
        System.out.println(Arrays.toString(findDiagonalOrder(new int[][]{{2}, {3}})));
    }

    public static int[] findDiagonalOrder(int[][] matrix) {
        int maxRows = matrix.length;
        if (maxRows == 0) return new int[0];
        int maxColumns = matrix[0].length;
        int[] result = new int[maxRows * maxColumns];
        List<Deque> deqs = new ArrayList<>();
        // traversing the left half of the matrix
        for (int i = 0; i < maxRows; i++) {
            int row = i;
            int column = 0;
            Deque<Integer> tmp = new LinkedList<>();
            while (row >= 0 && column < maxColumns) {
                tmp.add(matrix[row--][column++]);
            }
            deqs.add(tmp);
        }
        // traversing the right half of the matrix
        for (int j = 1; j < maxColumns; j++) {
            int row = maxRows - 1;
            int column = j;
            Deque<Integer> tmp = new LinkedList<>();
            while (row >= 0 && column < maxColumns) {
                tmp.add(matrix[row--][column++]);
            }
            deqs.add(tmp);
        }

        // when it comes to filling the result array we need to make sure that sometimes we go in different directions
        int x = 0;
        boolean moveUp = true;
        for (Deque deque : deqs) {
            while (!deque.isEmpty()) {
                result[x++] = (int) (moveUp ? deque.pollFirst() : deque.pollLast());
            }
            moveUp = !moveUp;
        }
        return result;
    }
}
