package Leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 12/5/19.
 */
public class WordDictionaryProgram {

    public static void main(String[] args) {
        WordDictionary wordDictionary = new WordDictionary();
        wordDictionary.addWord("bad");
        wordDictionary.addWord("dad");
        wordDictionary.addWord("mad");
        System.out.println(wordDictionary.search("pad"));
        System.out.println(wordDictionary.search("bad"));
        System.out.println(wordDictionary.search(".ad"));
        System.out.println(wordDictionary.search("b.."));
    }

    static class WordDictionary {

        private TrieNode root;

        /**
         * Initialize your data structure here.
         */
        public WordDictionary() {
            root = new TrieNode('*');
        }

        /**
         * Adds a word into the data structure.
         */
        public void addWord(String word) {
            TrieNode next = root;
            for (char c : word.toCharArray()) {
                TrieNode node = next.children.get(c);
                if (node == null) {
                    next.children.put(c, new TrieNode(c));
                }
                next = next.children.get(c);
            }
            next.isComplete = true;
        }

        /**
         * Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter.
         */
        public boolean search(String word) {
            return search(word, 0, root);
        }

        // Time: O(26^M) - we might have 26 branches if we have all dots
        // Space: O(M) where M is the length of the key
        private boolean search(String word, int i, TrieNode root) {
            if (i == word.length()) return root.isComplete;
            if (root.children.containsKey(word.charAt(i))) {
                return search(word, i + 1, root.children.get(word.charAt(i)));
            } else if (word.charAt(i) == '.') {
                for (char c : root.children.keySet()) {
                    if (search(word, i + 1, root.children.get(c))) return true;
                }
            }
            return false;
        }

        static class TrieNode {
            Map<Character, TrieNode> children = new HashMap<>();
            char c;
            boolean isComplete;

            TrieNode(char c) {
                this.c = c;
            }
        }
    }

}
