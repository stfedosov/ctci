package Leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 3/18/19.
 */
public class EncodeAndDecodeTinyUrl {

    private Map<String, String> map = new HashMap<>();

    // Encodes a URL to a shortened URL.
    public String encode(String longUrl) {
        String tinyUrl = "http://tinyurl.com/" + longUrl.hashCode();
        map.put(tinyUrl, longUrl);
        return tinyUrl;
    }

    // Decodes a shortened URL to its original URL.
    public String decode(String shortUrl) {
        return map.get(shortUrl);
    }

}
