package Leetcode;


/**
 * @author sfedosov on 7/20/19.
 */
public class NumMatrix {

    //          c1  c2
    //    | 1 | 2 | 3 |
    // r1 | 4 | 5 | 6 |
    // r2 | 7 | 8 | 9 |
    //
    // sumRegion(r1, c1, r2, c2) = sum[r2][c2] - sum[r2][c1 - 1] - sum[r1 - 1][c2] + sum[r1 - 1][c1 - 1]

    private int[][] dp;

    public NumMatrix(int[][] matrix) {
        if (matrix.length == 0 || matrix[0].length == 0) return;
        dp = new int[matrix.length + 1][matrix[0].length + 1];
        for (int r = 0; r < matrix.length; r++) {
            for (int c = 0; c < matrix[0].length; c++) {
                dp[r + 1][c + 1] = dp[r + 1][c] + dp[r][c + 1] + matrix[r][c] - dp[r][c];
            }
        }
    }

    public int sumRegion(int row1, int col1, int row2, int col2) {
        return dp[row2 + 1][col2 + 1] - dp[row1][col2 + 1] - dp[row2 + 1][col1] + dp[row1][col1];
    }

    public static void main(String[] args) {
        NumMatrix matrix = new NumMatrix(new int[][]{
                {3, 0, 1, 4, 2},
                {5, 6, 3, 2, 1},
                {1, 2, 0, 1, 5},
                {4, 1, 0, 1, 7},
                {1, 0, 3, 0, 5}
        });

        assert matrix.sumRegion(2, 1, 4, 3) == 8;
        assert matrix.sumRegion(1, 1, 2, 2) == 11;
        assert matrix.sumRegion(1, 2, 2, 4) == 12;
    }
}

