package Leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author sfedosov on 12/30/18.
 */
public class FruitsIntoBasket {

    public static void main(String[] args) {
        assert totalFruit(new int[]{0, 1, 2, 2}) == 3; // 1, 2, 2
        assert totalFruit(new int[]{1, 2, 1}) == 3; // 1, 2, 1
        assert totalFruit(new int[]{1, 2, 3, 2, 2}) == 4; // 2, 3, 2, 2
    }

    public static int totalFruit(int[] tree) {
        if (tree == null || tree.length == 0) return 0;
        Map<Integer, Integer> map = new HashMap<>();
        int i = 0;
        int j = 0;
        int max = 0;
        while (j < tree.length) {
            if (map.size() <= 2) {
                map.put(tree[j], j++);
            }
            if (map.size() > 2) {
                int min = Integer.MAX_VALUE;
                for (int value : map.values()) {
                    min = Math.min(min, value);
                }
                i = min + 1;
                map.remove(tree[min]);
            }
            max = Math.max(max, j - i);
        }
        return max;
    }
}
