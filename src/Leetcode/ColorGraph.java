package Leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author sfedosov on 7/23/19.
 */
public class ColorGraph {

    static Map<Integer, String> map = new HashMap<>();

    public static void main(String[] args) {
        String[] colors = {"red", "blue", "yellow", "orange", "pink"};
        colorGraph(new int[][]{{1, 3}, {0, 2}, {1, 3}, {0, 2}}, colors);
        //    0 ---- 1
        //    |      |
        //    |      |
        //    3 ---- 2
        System.out.println(map);
        map.clear();

        colorGraph(new int[][]{{1, 2, 3}, {0, 2}, {0, 1, 3}, {0, 2}}, colors);
        //    0 -- 1
        //    | \  |
        //    |  \ |
        //    3 -- 2

        System.out.println(map);
    }

    public static void colorGraph(int[][] graph, String[] colors) {
        for (int node = 0; node < graph.length; node++) {
            int[] neighbors = graph[node];
            for (int neighbor : neighbors) {
                if (neighbor == node) {
                    throw new IllegalArgumentException(String.format(
                            "Legal coloring impossible for node with loop: %s",
                            node));
                }
            }
            Set<String> illegalColors = new HashSet<>();
            for (int neighbor : neighbors) {
                if (map.containsKey(neighbor)) {
                    illegalColors.add(map.get(neighbor));
                }
            }
            for (String color : colors) {
                if (!illegalColors.contains(color)) {
                    map.put(node, color);
                    break;
                }
            }

        }

    }
}
