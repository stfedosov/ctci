package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IntervalListIntersections {

    /*
    You are given two lists of closed intervals, firstList and secondList,
    where firstList[i] = [starti, endi] and secondList[j] = [startj, endj].
    Each list of intervals is pairwise disjoint and in sorted order.

    Return the intersection of these two interval lists.

    A closed interval [a, b] (with a <= b) denotes the set of real numbers x with a <= x <= b.

    The intersection of two closed intervals is a set of real numbers that are either empty
    or represented as a closed interval. For example, the intersection of [1, 3] and [2, 4] is [2, 3].
     */

    public static void main(String[] args) {
        int[][] A = new int[][]{{0, 2}, {5, 10}, {13, 23}, {24, 25}};
        int[][] B = new int[][]{{1, 5}, {8, 12}, {15, 24}, {25, 26}};
        assert Arrays.deepToString(intervalIntersection(A, B)).equals("[[1, 2], [5, 5], [8, 10], [15, 23], [24, 24], [25, 25]]");

        A = new int[][]{{5, 10}};
        B = new int[][]{{3, 10}};
        assert Arrays.deepToString(intervalIntersection(A, B)).equals("[[5, 10]]");

        A = new int[][]{{8, 15}};
        B = new int[][]{{2, 6}, {8, 10}, {12, 20}};
        assert Arrays.deepToString(intervalIntersection(A, B)).equals("[[8, 10], [12, 15]]");

        int[][][] inputIntervalLista = {
                {{3, 11}, {14, 15}, {18, 22}, {23, 24}, {25, 26}},
        };

        int[][][] inputIntervalListb = {
                {{2, 8}, {13, 20}},
        };

        for (int i = 0; i < inputIntervalLista.length; i++) {
            System.out.println(i + 1 + ".\t Interval List A: " + Arrays.deepToString(inputIntervalLista[i]));
            System.out.println("\t Interval List B: " + Arrays.deepToString(inputIntervalListb[i]));
            System.out.println("\t Intersecting intervals in 'A' and 'B' are: " +
                    Arrays.deepToString(mergeIntervals(inputIntervalLista[i], inputIntervalListb[i])));
            System.out.println(new String(new char[100]).replace('\0', '-'));
        }

    }

    public static int[][] intervalIntersection(int[][] A, int[][] B) {
        int lengthA = A.length, lengthB = B.length, iA = 0, iB = 0;
        List<int[]> result = new ArrayList<>();
        while (iA < lengthA && iB < lengthB) {
            if (A[iA][0] > B[iB][1]) { // no intersection
                iB++;
            } else if (A[iA][1] < B[iB][0]) { // no intersection
                iA++;
            } else { // intersection
                result.add(new int[]{Math.max(B[iB][0], A[iA][0]), Math.min(B[iB][1], A[iA][1])});
                if (A[iA][1] >= B[iB][1]) {
                    iB++;
                } else {
                    iA++;
                }
            }
        }
        return prepareResult(result);
    }

    private static int[][] prepareResult(List<int[]> list) {
        int[][] toReturn = new int[list.size()][];
        for (int i = 0; i < list.size(); i++) {
            toReturn[i] = list.get(i);
        }
        return toReturn;
    }

    public static int[][] intervalsIntersection(int[][] intervalLista, int[][] intervalListb) {
        List<int[]> intersections = new ArrayList<>();
        int i = 0, j = 0;
        while (i < intervalLista.length && j < intervalListb.length) {
            int start = Math.max(intervalLista[i][0], intervalListb[j][0]);
            int end = Math.min(intervalLista[i][1], intervalListb[j][1]);
            if (start <= end)
                intersections.add(new int[]{start, end});
            if (intervalLista[i][1] < intervalListb[j][1]) {
                i++;
            } else {
                j++;
            }
        }
        return intersections.toArray(new int[0][]);
    }

    public static int[][] mergeIntervals(int[][] intervalA, int[][] intervalB) {
        List<int[]> intersections = new ArrayList<>();
        int i = 0, j = 0;
        while (i < intervalA.length && j < intervalB.length) {
            int[] currentInterval;
            if (intervalA[i][0] <= intervalB[j][0]) {
                currentInterval = intervalA[i++];
            } else {
                currentInterval = intervalB[j++];
            }
            merge(currentInterval, intersections);
        }
        while (i < intervalA.length) {
            merge(intervalA[i++], intersections);
        }
        while (j < intervalA.length) {
            merge(intervalA[j++], intersections);
        }
        return intersections.toArray(new int[0][]);
    }

    private static void merge(int[] currentInterval, List<int[]> intersections) {
        var last = !intersections.isEmpty() ? intersections.get(intersections.size() - 1) : null;
        if (intersections.isEmpty() || last[1] < currentInterval[0]) {
            intersections.add(currentInterval);
        } else {
            last[1] = Math.max(last[1], currentInterval[1]);
        }
    }

}
