package Leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BoundaryOfBinaryTree {

    public List<Integer> boundaryOfBinaryTree(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        if (root == null) return Collections.emptyList();
        if (root.left != null || root.right != null) {
            result.add(root.val);
        }
        leftBoundary(root.left, result);
        leaves(root, result);
        rightBoundary(root.right, result);
        return result;
    }

    private void leftBoundary(TreeNode root, List<Integer> result) {
        if (root == null || root.left == null && root.right == null) return;
        result.add(root.val);
        if (root.left == null) {
            leftBoundary(root.right, result);
        } else {
            leftBoundary(root.left, result);
        }
    }

    private void rightBoundary(TreeNode root, List<Integer> result) {
        if (root == null || root.left == null && root.right == null) return;
        if (root.right == null) {
            rightBoundary(root.left, result);
        } else {
            rightBoundary(root.right, result);
        }
        result.add(root.val);
    }

    private void leaves(TreeNode root, List<Integer> result) {
        if (root == null) return;
        if (root.left == null && root.right == null) {
            result.add(root.val);
        }
        leaves(root.left, result);
        leaves(root.right, result);
    }

}
