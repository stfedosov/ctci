package Leetcode;

class MyHashMap {

    public static void main(String[] args) {
        MyHashMap hashMap = new MyHashMap();
        hashMap.put(10, 14);
        hashMap.put(10, 15);
        System.out.println(hashMap.get(10));
        System.out.println(hashMap.get(11));
    }

    private static final int SIZE = 32;

    static class Entry<T> {
        T key, value;
        Entry next;

        Entry(T key, T value) {
            this.key = key;
            this.value = value;
            next = null;
        }
    }

    private final Entry[] table = new Entry[SIZE];

    public <T> void put(T key, T value) {
        Entry entry = new Entry(key, value);
        int code = key.hashCode() % SIZE;
        if (table[code] == null) {
            table[code] = entry;
        } else {
            addOrUpdateNode(table[code], entry);
        }
    }

    public <T> T get(T key) {
        int code = key.hashCode() % SIZE;
        Entry head = table[code];
        while (head != null) {
            if (head.key.equals(key)) return (T) head.value;
            head = head.next;
        }
        return null;
    }

    public <T> void remove(T key) {
        int code = key.hashCode() % SIZE;
        Entry head = table[code];
        Entry dummy = new Entry("0", "0");
        dummy.next = head;
        Entry prev = dummy;
        while (head != null) {
            if (head.key.equals(key)) prev.next = head.next;
            prev = head;
            head = head.next;
        }
        table[code] = dummy.next;
    }

    public <T> boolean containsKey(T key) {
        int code = key.hashCode() % SIZE;
        Entry head = table[code];
        while (head != null) {
            if (head.key.equals(key)) return true;
            head = head.next;
        }
        return false;
    }

    private void addOrUpdateNode(Entry head, Entry node) {
        Entry prev = head;
        while (head != null) {
            if (head.key.equals(node.key)) {
                head.value = node.value;
                return;
            }
            prev = head;
            head = head.next;
        }
        prev.next = node;
    }
}