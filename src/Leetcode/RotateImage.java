package Leetcode;

import java.util.Arrays;

public class RotateImage {

    public static void main(String[] args) {
        int[][] matrix = new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        rotate(matrix);
        int[][] rotated = new int[][]{{7, 4, 1}, {8, 5, 2}, {9, 6, 3}};
        for (int i = 0; i < matrix.length; i++) {
            assert Arrays.equals(matrix[i], rotated[i]);
        }
    }

    /*
    Initial matrix:
    [1, 2, 3]
    [4, 5, 6]
    [7, 8, 9]
    ---
    After transposing:
    [1, 4, 7]
    [2, 5, 8]
    [3, 6, 9]
    ---
    After final reverse of each row:
    [7, 4, 1]
    [8, 5, 2]
    [9, 6, 3]
     */
    // You are given an n x n 2D matrix representing an image, rotate the image by 90 degrees (clockwise).
    public static void rotate(int[][] matrix) {
        // transpose matrix
        for (int i = 0; i < matrix.length; i++) {
            for (int j = i; j < matrix[i].length; j++) {
                int tmp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = tmp;
            }
        }
        // reverse each row
        for (int[] m : matrix) {
            int start = 0;
            int end = m.length - 1;
            while (start < end) {
                int tmp = m[start];
                m[start] = m[end];
                m[end] = tmp;
                start++;
                end--;
            }
        }
    }

}
