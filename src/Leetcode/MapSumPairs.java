package Leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 1/5/18.
 */
public class MapSumPairs {

    /*
    Implement the MapSum class:

    MapSum() Initializes the MapSum object.
    void insert(String key, int val) Inserts the key-val pair into the map.
    If the key already existed, the original key-value pair will be overridden to the new one.
    int sum(string prefix)
    Returns the sum of all the pairs' value whose key starts with the prefix.
     */

    class MapSum {

        Map<String, Integer> map;
        Map<String, Integer> score;
        public MapSum() {
            map = new HashMap();
            score = new HashMap();
        }
        public void insert(String key, int val) {
            int delta = val - map.getOrDefault(key, 0);
            map.put(key, val);
            String prefix = "";
            for (char c: key.toCharArray()) {
                prefix += c;
                score.put(prefix, score.getOrDefault(prefix, 0) + delta);
            }
        }
        public int sum(String prefix) {
            return score.getOrDefault(prefix, 0);
        }
    }

/*
 * Your MapSum object will be instantiated and called as such:
 * MapSum obj = new MapSum();
 * obj.insert(key,val);
 * int param_2 = obj.sum(prefix);
 */

}
