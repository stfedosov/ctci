package Leetcode;

public class PalindromeNumber {

    public static void main(String[] args) {
        System.out.println(isPalindrome(121)); // true
        System.out.println(isPalindrome(-121)); // false
        System.out.println(isPalindrome(5665)); // true
    }

    public static boolean isPalindrome(int x) {
        if (x < 0 || x > 0 && x % 10 == 0) return false;
        int reverted = 0;
        // to avoid overflow, we only need to reverse half of the number
        while (x > reverted) {
            reverted = reverted * 10 + (x % 10);
            x /= 10;
        }
        // in one of the cases we divide by 10 to check the case where we have odd number of digits
        // i.e. 12321 -> reverted = 123 -> 123 / 10 which is equal to the first half 12 == 12
        return reverted == x || reverted / 10 == x;
    }

}
