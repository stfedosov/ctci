package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class FindTheCityWithSmallestNumberOfNeighborsAtThresholdDistance {

    public static void main(String[] args) {
        assert findTheCity(4, new int[][]{{0, 1, 3}, {1, 2, 1}, {1, 3, 4}, {2, 3, 1}}, 4) == 3;
    }

    public static int findTheCity(int n, int[][] edges, int distanceThreshold) {
        Graph g = new Graph(n);
        for (int[] edge : edges) {
            g.addEdge(edge[0], edge[1], edge[2]);
        }
        Map<Integer, Map<Integer, Integer>> vertexToClosestVertices = new HashMap<>();
        for (int i = 0; i < n; i++) {
            int j = 0;
            vertexToClosestVertices.put(i, new HashMap<>());
            for (int distance : g.dijkstra(i)) {
                if (distance > 0 && distance <= distanceThreshold) {
                    vertexToClosestVertices.get(i).put(j, distance);
                }
                j++;
            }
        }
        Queue<Map.Entry<Integer, Map<Integer, Integer>>> q = new PriorityQueue<>((a, b) -> {
            int cmp = Integer.compare(a.getValue().size(), b.getValue().size());
            if (cmp == 0) {
                cmp = Integer.compare(b.getKey(), a.getKey());
            }
            return cmp;
        });
        q.addAll(vertexToClosestVertices.entrySet());
        return q.poll().getKey();
    }


    static class Graph {
        int numOfVertices;
        ArrayList<Edge>[] adj;

        public Graph(int numOfVertices) {
            this.numOfVertices = numOfVertices;
            this.adj = new ArrayList[numOfVertices];
            for (int i = 0; i < numOfVertices; i++) {
                this.adj[i] = new ArrayList<>();
            }
        }

        public void addEdge(int source, int destination, int weight) {
            adj[source].add(new Edge(source, destination, weight));
            adj[destination].add(new Edge(destination, source, weight));
        }

        public int[] dijkstra(int source) {
            int[] distances = new int[numOfVertices];
            boolean[] visited = new boolean[numOfVertices];
            Arrays.fill(distances, Integer.MAX_VALUE);
            // sort according to distances: a[0] - source, a[1] - distance
            Queue<int[]> minHeap = new PriorityQueue<>(Comparator.comparingInt(a -> a[1]));
            distances[source] = 0;
            minHeap.offer(new int[]{source, 0});
            while (!minHeap.isEmpty()) {
                int[] polled = minHeap.poll();
                if (!visited[polled[0]]) {
                    visited[polled[0]] = true;
                    for (Edge e : adj[polled[0]]) {
                        int destination = e.dst;
                        if (!visited[destination]) {
                            int newDistance = distances[polled[0]] + e.distance;
                            if (newDistance < distances[destination]) {
                                distances[destination] = newDistance;
                                minHeap.offer(new int[]{destination, newDistance});
                            }
                        }
                    }
                }
            }
            return distances;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            int i = 0;
            for (List<Edge> e : adj) {
                sb.append(i++);
                sb.append(" ");
                for (Edge ee : e) {
                    sb.append(ee);
                    sb.append(" ");
                }
                sb.append("\n");
            }
            return sb.toString();
        }
    }

    static class Edge {
        int src, dst, distance;

        Edge(int src, int dst, int distance) {
            this.src = src;
            this.dst = dst;
            this.distance = distance;
        }

        @Override
        public String toString() {
            return src + " <-> " + dst + " = " + distance;
        }
    }

    public static int findTheCityFloydWarshall(int n, int[][] edges, int distanceThreshold) {
        int[][] distances = new int[n][n];
        for (int i = 0; i < n; i++) {
            Arrays.fill(distances[i], Integer.MAX_VALUE);
            distances[i][i] = 0;
        }
        for (int[] edge : edges) {
            distances[edge[0]][edge[1]] = edge[2];
            distances[edge[1]][edge[0]] = edge[2];
        }
        for (int k = 0; k < n; k++) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (distances[i][k] < Integer.MAX_VALUE && distances[k][j] < Integer.MAX_VALUE) {
                        distances[i][j] = Math.min(distances[i][j], distances[i][k] + distances[k][j]);
                    }
                }
            }
        }
        int count = Integer.MAX_VALUE;
        int answer = -1;
        for (int i = 0; i < n; i++) {
            int count2 = 0;
            for (int j = 0; j < n; j++) {
                if (distances[i][j] <= distanceThreshold) {
                    count2++;
                }
            }
            // we need to find the greatest one, so keep comparing if counts are the same
            if (count >= count2) {
                count = count2;
                answer = i;
            }
        }
        return answer;
    }

}
