package Leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 10/28/19.
 */
public class Stream {

    private final ListNode head = new ListNode(999);
    private final ListNode tail = new ListNode(999);
    private final Map<Integer, ListNode> map;
    private int nodesCount;

    public Stream() {
        this.map = new HashMap<>();
        head.next = tail;
        tail.previous = head;
        this.nodesCount = 0;
    }

    public void add(int num) {
        if (map.containsKey(num)) {
            ListNode node = map.get(num);
            deleteNode(node);
        } else {
            ListNode newNode = new ListNode(num);
            map.put(num, newNode);
            insertAtTheTail(newNode);
        }
    }

    private void deleteNode(ListNode node) {
        ListNode next = node.next;
        ListNode previous = node.previous;
        if (previous != null && next != null) {
            previous.next = next;
            next.previous = previous;
            node.next = null;
            node.previous = null;
            nodesCount--;
        }
    }

    private void insertAtTheTail(ListNode newNode) {
        ListNode previous = tail.previous;
        newNode.previous = previous;
        previous.next = newNode;
        newNode.next = tail;
        tail.previous = newNode;
        nodesCount++;
    }

    private boolean isEmpty() {
        return nodesCount == 0;
    }

    public Integer getFirstUnique() {
        if (isEmpty()) return null;
        return head.next.value;
    }

    static class ListNode {
        final int value;
        ListNode next;
        ListNode previous;

        ListNode(int value) {
            this.value = value;
        }
    }

    public static void main(String[] args) {
        Stream s = new Stream();
        s.add(2);
        assert s.getFirstUnique() == 2;
        s.add(2);
        assert s.getFirstUnique() == null;
        s.add(3);
        assert s.getFirstUnique() == 3;
        s.add(4);
        assert s.getFirstUnique() == 3;
        s.add(3);
        assert s.getFirstUnique() == 4;

        // New test ------

        s = new Stream();
        s.add(1);
        assert s.getFirstUnique() == 1;
        s.add(2);
        assert s.getFirstUnique() == 1;
        s.add(3);
        assert s.getFirstUnique() == 1;
        s.add(4);
        assert s.getFirstUnique() == 1;
        s.add(1);
        assert s.getFirstUnique() == 2;
        s.add(5);
        assert s.getFirstUnique() == 2;
        s.add(7);
        assert s.getFirstUnique() == 2;
        s.add(3);
        assert s.getFirstUnique() == 2;
        s.add(5);
        assert s.getFirstUnique() == 2;
        s.add(4);
        assert s.getFirstUnique() == 2;
        s.add(2);
        assert s.getFirstUnique() == 7;
    }

}
