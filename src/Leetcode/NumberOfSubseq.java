package Leetcode;

import java.util.Arrays;

public class NumberOfSubseq {

    public static void main(String[] args) {
        assert numSubseq(new int[]{3, 5, 6, 7}, 9) == 4;
        assert numSubseq(new int[]{3, 3, 6, 8}, 10) == 6;
        assert numSubseq(new int[]{2, 3, 3, 4, 6, 7}, 12) == 61;
    }

    /*
    Sort input nums first,
    For each nums[i], find out the maximum nums[j]
    that nums[i] + nums[j] <= target.

    For each elements in the subarray nums[i+1] ~ nums[j],
    we can pick or not pick,
    so there are 2 ^ (j - i) subsequences in total.
    So we can update count = (count + 2 ^ (j - i)) % mod.

    We don't care the original elements order,
    we only want to know the count of sub sequence.
    So we can sort the original nums, and the result won't change.
     */

    public static int numSubseq(int[] nums, int target) {
        int mod = 1000_000_007;
        int[] dp = new int[nums.length + 1];
        dp[0] = 1;
        for (int i = 1; i <= nums.length; i++) {
            dp[i] = dp[i - 1] * 2 % mod;
        }
        Arrays.sort(nums);
        int count = 0;
        int left = 0, right = nums.length - 1;
        while (left <= right) {
            if (nums[left] + nums[right] > target) {
                right--;
            } else {
                count += dp[right - left];
                count %= mod;
                left++;
            }
        }
        return count % mod;
    }

}
