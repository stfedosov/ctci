package Leetcode;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author sfedosov on 3/5/20.
 */
public class CarPooling {

    public static void main(String[] args) {
        assert !carPooling(new int[][]{{2, 1, 5}, {3, 3, 7}}, 4);
        assert carPooling(new int[][]{{2, 1, 5}, {3, 3, 7}}, 5);
        assert carPooling(new int[][]{{2, 1, 5}, {3, 5, 7}}, 3);
        assert carPooling(new int[][]{{3, 2, 7}, {3, 7, 9}, {8, 3, 9}}, 11);
    }

    public static boolean carPooling(int[][] trips, int capacity) {
        Map<Integer, Integer> map = new TreeMap<>();
        for (int[] trip : trips) {
            map.put(trip[1], map.getOrDefault(trip[1], 0) + trip[0]);
            map.put(trip[2], map.getOrDefault(trip[2], 0) - trip[0]);
        }

        int current = 0;

        for (int x : map.keySet()) {
            current += map.get(x);
            if (current > capacity) return false;
        }
        return true;
    }

}
