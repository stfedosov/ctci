package Leetcode;

/**
 * @author sfedosov on 5/9/20.
 */
public class DaysBetweenDates {

    public static void main(String[] args) {
        assert daysBetweenDates("2018-06-29", "2020-06-30") == 732;
        assert daysBetweenDates("2019-06-29", "2019-06-30") == 1;
        assert daysBetweenDates("2020-01-15", "2019-12-31") == 15;

        assert daysBetweenDatesOptimized("2018-06-29", "2020-06-30") == 732;
        assert daysBetweenDatesOptimized("2019-06-29", "2019-06-30") == 1;
        assert daysBetweenDatesOptimized("2020-01-15", "2019-12-31") == 15;
    }

    private static final int[] monthDays = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    // Brute force, very long approach ========================================================================
    public static int daysBetweenDates(String date1, String date2) {
        String[] split1 = date1.split("-"), split2 = date2.split("-");
        int year1 = Integer.parseInt(split1[0]), year2 = Integer.parseInt(split2[0]);
        int month1 = Integer.parseInt(split1[1]), month2 = Integer.parseInt(split2[1]);
        int day1 = Integer.parseInt(split1[2]), day2 = Integer.parseInt(split2[2]);
        int sum = 0;
        if (year1 == year2) {
            if (month1 == month2) {
                return Math.abs(day1 - day2);
            } else {
                int tmp = month1 > month2 ? daysBeforeNY(year1, month1, day1) + daysAfterNY(year2, month2, day2) :
                        daysBeforeNY(year2, month2, day2) + daysAfterNY(year1, month1, day1);
                sum = isLeapYear(year1) ? 366 - tmp : 365 - tmp;
            }
        } else {
            int bigger = Math.max(year1, year2), smaller = Math.min(year1, year2);
            if (bigger - smaller > 1) {
                int start = smaller + 1;
                int end = bigger - 1;
                while (start <= end) {
                    if (isLeapYear(start)) sum += 366;
                    else sum += 365;
                    start++;
                }
            }
            sum += year1 > year2 ? daysAfterNY(year1, month1, day1) + daysBeforeNY(year2, month2, day2) :
                    daysAfterNY(year2, month2, day2) + daysBeforeNY(year1, month1, day1);
        }
        return sum;
    }

    private static int daysBeforeNY(int year, int month, int day) {
        if (isLeapYear(year)) {
            return 366 - daysAfterNY(year, month, day);
        } else {
            return 365 - daysAfterNY(year, month, day);
        }
    }

    private static int daysAfterNY(int year, int month, int day) {
        int days = day;
        boolean isLeap = isLeapYear(year);
        for (int m = 0; m < month - 1; m++) {
            days += (m == 1 && isLeap ? 29 : monthDays[m]);
        }
        return days;
    }

    private static boolean isLeapYear(int year) {
        if (year % 400 == 0) return true;
        else if (year % 100 == 0) return false;
        else return year % 4 == 0;
    }

    // More optimized one =====================================================================================

    public static int daysBetweenDatesOptimized(String date1, String date2) {
        return Math.abs(countSince1971(date1) - countSince1971(date2));
    }

    public static int countSince1971(String date) {
        int[] monthDays = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        String[] data = date.split("-");

        int year = Integer.parseInt(data[0]);
        int month = Integer.parseInt(data[1]);
        int day = Integer.parseInt(data[2]);

        for (int i = 1971; i < year; i++) {
            day += isALeapYear(i) ? 366 : 365;
        }
        for (int i = 1; i < month; i++) {
            if (isALeapYear(year) && i == 2) {
                day += 1;
            }
            day += monthDays[i];
        }
        return day;
    }

    public static boolean isALeapYear(int year) {
        return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
    }

}
