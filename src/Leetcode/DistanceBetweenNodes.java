package Leetcode;

public class DistanceBetweenNodes {

    /*
    Given the root of a binary tree and two integers p and q,
    return the distance between the nodes of value p and value q in the tree.
     */
    public static int findDistance(TreeNode root, int p, int q) {
        if (p == q) return 0;
        TreeNode lca = findLca(root, p, q);
        int pNodeDist = findDist(lca, p);
        int qNodeDist = findDist(lca, q);
        return pNodeDist + qNodeDist;
    }

    private static TreeNode findLca(TreeNode root, int p, int q) {
        if (root == null) return null;
        if (root.val == p || root.val == q) return root;
        TreeNode left = findLca(root.left, p, q);
        TreeNode right = findLca(root.right, p, q);
        return (left != null && right != null) ? root : left != null ? left : right;
    }

    private static int findDist(TreeNode root, int node) {
        if (root == null) return -1;
        if (root.val == node) return 0;
        int resLeft = findDist(root.left, node);
        int resRight = findDist(root.right, node);
        if (resLeft >= 0) return 1 + resLeft;
        else if (resRight >= 0) return 1 + resRight;
        else return -1;
    }

    // Driver program to test above functions
    public static void main(String[] args) {

        // Let us create binary tree given in
        // the above example
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(7);
        root.right.left.right = new TreeNode(8);

        //                  1
        //               /     \
        //              2       3
        //            /   \    /  \
        //           4     5  6    7
        //                     \
        //                      8

        System.out.println("Dist(4, 5) = "
                + findDistance(root, 4, 5)); // 2

        System.out.println("Dist(4, 6) = "
                + findDistance(root, 4, 6)); // 4

        System.out.println("Dist(3, 4) = "
                + findDistance(root, 3, 4)); // 3

        System.out.println("Dist(2, 4) = "
                + findDistance(root, 2, 4)); // 1

        System.out.println("Dist(8, 5) = "
                + findDistance(root, 8, 5)); // 5

    }

}
