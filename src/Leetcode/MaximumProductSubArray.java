package Leetcode;

public class MaximumProductSubArray {

    public static void main(String[] args) {
        assert maxProduct(new int[]{2, 3, -2, -4}) == 48;
        assert maxProduct(new int[]{-2, 0, -1}) == 0;
        assert maxProduct(new int[]{2, 3, -2, 4}) == 6;
    }

    // find a contiguous non-empty subarray within the array that has the largest product, and return the product.
    public static int maxProduct(int[] nums) {
        int prev_min = nums[0];
        int prev_max = nums[0];
        int max = nums[0];
        int curr_max;
        int curr_min;
        for (int i = 1; i < nums.length; i++) {
            curr_min = Math.min(Math.min(prev_max * nums[i], prev_min * nums[i]), nums[i]);
            curr_max = Math.max(Math.max(prev_max * nums[i], prev_min * nums[i]), nums[i]);
            max = Math.max(max, curr_max);
            prev_max = curr_max;
            prev_min = curr_min;
        }
        return max;
    }

    public int maxProductBruteForce(int[] nums) {
        if (nums.length == 0) return 0;
        int max = nums[0];
        for (int i = 0; i < nums.length; i++) {
            int tmp = 1;
            for (int j = i; j < nums.length; j++) {
                tmp *= nums[j];
                max = Math.max(max, tmp);
            }
        }
        return max;
    }

    // Calculate prefix product in array.
    // Calculate suffix product in array.
    // Return the max.
    public int maxProduct2(int[] nums) {
        int max = Integer.MIN_VALUE, left = 0, right = 0;
        for (int i = 0; i < nums.length; i++) {
            left = (left == 0 ? 1 : left) * nums[i];
            right = (right == 0 ? 1 : right) * nums[nums.length - 1 - i];
            max = Math.max(max, Math.max(left, right));
        }
        return max;
    }

}
