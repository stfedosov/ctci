package Leetcode;

import java.util.Arrays;
import java.util.Comparator;

/*
 Minimum Number of Arrows to Burst Balloons
 */
public class FindMinArrowShots {

    public static void main(String[] args) {
        assert findMinArrowShots(new int[][]{{10, 16}, {2, 8}, {1, 6}, {7, 12}}) == 2;
        assert findMinArrowShots(new int[][]{{1, 2}, {2, 3}, {3, 4}, {4, 5}}) == 2;
    }

    public static int findMinArrowShots(int[][] points) {
        if (points.length == 0) return 0;
        Arrays.sort(points, Comparator.comparingInt(a -> a[0]));
        int arrows = 1;
        int[] interval = new int[]{points[0][0], points[0][1]};
        for (int i = 1; i < points.length; i++) {
            if (points[i][0] <= interval[1]) {
                interval[1] = Math.min(interval[1], points[i][1]);
            } else {
                arrows++;
                interval = new int[]{points[i][0], points[i][1]};
            }
        }
        return arrows;
    }
}
