package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class FindCheapestPrice {

    private static int findCheapestPrice(int n, int[][] flights, int src, int dst, int k) {
        Map<Integer, List<int[]>> adj = new HashMap<>();
        for (int[] i : flights)
            adj.computeIfAbsent(i[0], value -> new ArrayList<>()).add(new int[]{i[1], i[2]});

        int[] stops = new int[n];
        Arrays.fill(stops, Integer.MAX_VALUE);
        Queue<int[]> pq = new PriorityQueue<>();
        // {dist_from_src_node, node, number_of_stops_from_src_node}
        pq.offer(new int[]{0, src, 0});

        while (!pq.isEmpty()) {
            int[] temp = pq.poll();
            int dist = temp[0];
            int node = temp[1];
            int currentNumOfStops = temp[2];
            // We have already encountered a path with a lower cost and fewer stops,
            // or the number of stops exceeds the limit.
            if (currentNumOfStops > stops[node] || currentNumOfStops > k + 1)
                continue;
            stops[node] = currentNumOfStops;
            if (node == dst)
                return dist;
            for (int[] a : adj.getOrDefault(node, Collections.emptyList())) {
                pq.offer(new int[]{dist + a[1], a[0], currentNumOfStops + 1});
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        assert findCheapestPrice(3, new int[][]{{0, 1, 100}, {1, 2, 100}, {0, 2, 500}}, 0, 2, 1) == 200;
        assert findCheapestPrice(3, new int[][]{{0, 1, 100}, {1, 2, 100}, {0, 2, 500}}, 0, 2, 0) == 500;
    }

}
