package Leetcode;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author sfedosov on 2/21/20.
 */
public class AddBoldTagInString {

    public static void main(String[] args) {
        assert addBoldTag("aaabbcc", new String[]{"aaa", "aab", "bc"}).equals("<b>aaabbc</b>c");
        assert addBoldTag("abcxyz123", new String[]{"abc","123"}).equals("<b>abc</b>xyz<b>123</b>");
    }

    public static String addBoldTag(String s, String[] dict) {
        if (dict.length == 0) return s;
        List<int[]> intervals = new ArrayList<>();
        helper(s, intervals, dict);
        StringBuilder sb = new StringBuilder();
        String open = "<b>";
        String close = "</b>";
        int num = 0;
        List<String> list = new ArrayList<>();
        for (char c : s.toCharArray()) list.add(String.valueOf(c));
        for (int[] i : merge(intervals)) {
            list.add(i[0] + num, open);
            num++;
            list.add((i[1] + 1) + num, close);
            num++;
        }
        for (String ss : list) sb.append(ss);
        return sb.toString();
    }

    private static Set<int[]> merge(List<int[]> intervals) {
        Set<int[]> result = new LinkedHashSet<>();
        intervals.sort(Comparator.comparingInt(a -> a[0]));
        int[] prev = null;
        for (int[] interval : intervals) {
            if (prev == null || interval[0] > prev[1]) {
                if (prev != null && prev[1] + 1 == interval[0]) {
                    prev[1] = interval[1];
                } else {
                    result.add(interval);
                    prev = interval;
                }
            } else if (interval[1] > prev[1]) {
                prev[1] = interval[1];
            }
        }
        return result;
    }

    private static void helper(String s, List<int[]> intervals, String[] dict) {
        for (String d : dict) {
            for (int i = 0; i < s.length() - d.length() + 1; i++) {
                if (s.startsWith(d, i)) {
                    intervals.add(new int[]{i, i + d.length() - 1});
                }
            }
        }
    }
}
