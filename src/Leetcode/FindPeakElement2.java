package Leetcode;

public class FindPeakElement2 {

    public static void main(String[] args) {

    }

    public static int[] findPeakGrid(int[][] mat) {
        int n = mat.length, m = mat[0].length, start = 0, end = m - 1, mid;
        while (start <= end) {
            mid = start + (end - start) / 2;
            int row = 0;
            for (int i = 0; i < n; ++i) {
                if (mat[row][mid] < mat[i][mid]) row = i;
            }
            if ((mid == 0 || mat[row][mid] > mat[row][mid - 1]) &&
                    (mid == m - 1 || mat[row][mid] > mat[row][mid + 1]))
                return new int[]{row, mid};
            else if (mid > 0 && mat[row][mid - 1] > mat[row][mid])
                end = mid - 1;
            else
                start = mid + 1;
        }
        return new int[]{-1, -1};
    }


}
