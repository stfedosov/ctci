package Leetcode;

import java.util.HashSet;
import java.util.Set;

public class LongestSubstringWithoutRepeatingCharacters {

    public static void main(String[] args) {
        assert lengthOfLongestSubstring2("abcabcbb") == 3;
        assert lengthOfLongestSubstring2("bbbbb") == 1;
        assert lengthOfLongestSubstring2("pwwkew") == 3;
        assert lengthOfLongestSubstring2("dvdf") == 3;
    }

    public static int lengthOfLongestSubstring2(String s) {
        var set = new HashSet<>();
        int length = 0, start = 0;
        for (int end = 0; end < s.length(); end++) {
            if (set.contains(s.charAt(end))) {
                while (set.contains(s.charAt(end))) {
                    set.remove(s.charAt(start));
                    start++;
                }
            }
            set.add(s.charAt(end));
            length = Math.max(length, end - start + 1);
        }
        return length;
    }

}
