package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class CourseSchedule2 {

    public static void main(String[] args) {
        // 0 -> 1 -> 3
        //   \
        //    2 -> 3
        assert Arrays.toString(findOrder(4, new int[][]{{1, 0}, {2, 0}, {3, 1}, {3, 2}})).equals("[0, 2, 1, 3]");
        assert Arrays.toString(findOrder(4, new int[][]{{1, 0}, {0, 1}, {3, 1}, {3, 2}})).equals("[]");
        assert Arrays.toString(findOrder(2, new int[][]{{1, 0}})).equals("[0, 1]");
        assert Arrays.toString(findOrder(2, new int[][]{{0, 1}})).equals("[1, 0]");
    }

    // Time Complexity: O(V + E)
    public static int[] findOrder(int numCourses, int[][] prerequisites) {
        Map<Integer, List<Integer>> graph = new HashMap<>(numCourses);
        for (int[] prerequisite : prerequisites)
            graph.computeIfAbsent(prerequisite[1], x -> new ArrayList<>()).add(prerequisite[0]);
        boolean[] visited = new boolean[numCourses];
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < numCourses; i++) {
            if (hasCycle(graph, i, stack, visited, new boolean[numCourses])) return new int[0];
        }
        int i = 0;
        int[] result = new int[numCourses];
        while (!stack.isEmpty()) {
            result[i++] = stack.pop();
        }
        return result;
    }

    private static boolean hasCycle(Map<Integer, List<Integer>> graph,
                                    int v,
                                    Stack<Integer> stack,
                                    boolean[] visited,
                                    boolean[] visiting) {
        if (visited[v]) return false;
        if (visiting[v]) return true;
        visiting[v] = true;
        for (Integer u : graph.getOrDefault(v, Collections.emptyList())) {
            if (hasCycle(graph, u, stack, visited, visiting)) return true;
        }
        visited[v] = true;
        stack.push(v);
        return false;
    }

}