package Leetcode;

public class DiameterOfBinaryTree {

    static int max = 0;

    public static void main(String[] args) {
        TreeNode root = new TreeNode(2);
        TreeNode treeNode2 = new TreeNode(1);
        root.left = treeNode2;
        treeNode2.left = new TreeNode(4);
        treeNode2.right = new TreeNode(5);
        root.right = new TreeNode(3);
        assert diameterOfBinaryTree(root) == 3;
    }

    public static int diameterOfBinaryTree(TreeNode root) {
        getHeight(root);
        return max;
    }

    private static int getHeight(TreeNode root) {
        if (root == null) return 0;
        int left = getHeight(root.left);
        int right = getHeight(root.right);
        max = Math.max(max, left + right);
        return 1 + Math.max(left, right);
    }

}
