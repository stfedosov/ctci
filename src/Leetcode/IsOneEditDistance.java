package Leetcode;

public class IsOneEditDistance {

    public static void main(String[] args) {
        assert isOneEditDistance("abcdeft", "abcdef");
        assert !isOneEditDistance("abcdeft", "abcdmf");
    }

    public static boolean isOneEditDistance(String s, String t) {
        if (s == null || t == null)
            return false;

        int m = s.length();
        int n = t.length();

        if (Math.abs(m - n) > 1) {
            return false;
        }

        int i = 0;
        int j = 0;
        int count = 0;

        while (i < m && j < n) {
            if (s.charAt(i) == t.charAt(j)) {
                i++;
                j++;
            } else {
                count++;
                if (count > 1) return false;
                if (m > n) {
                    i++;
                } else if (m < n) {
                    j++;
                } else {
                    i++;
                    j++;
                }
            }
        }
        if (i < m || j < n) {
            count++;
        }
        return count == 1;
    }

    public boolean isOneEditDistanceRecursive(String s, String t) {
        if (s.equals(t)) return false;
        return isOneEdit(s, t, 0);
    }

    private static boolean isOneEdit(String s, String t, int count) {
        if (count > 1) return false;
        if (s.equals(t)) return true;
        boolean sEmpty = s.isEmpty(), tEmpty = t.isEmpty();
        if (!sEmpty && !tEmpty && s.charAt(0) == t.charAt(0)) {
            return isOneEdit(s.substring(1), t.substring(1), count);
        } else {
            return !sEmpty && isOneEdit(s.substring(1), t, count + 1)
                    || !tEmpty && isOneEdit(s, t.substring(1), count + 1)
                    || !sEmpty && !tEmpty && isOneEdit(s.substring(1), t.substring(1), count + 1);
        }
    }

}
