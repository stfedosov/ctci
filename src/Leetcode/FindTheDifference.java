package Leetcode;

/**
 * @author sfedosov on 9/13/19.
 */
public class FindTheDifference {

    public static void main(String[] args) {
       assert findTheDifference("abcd", "abcde") == 'e';
    }

    public static char findTheDifference(String s, String t) {
        int[] count = new int[26];
        for (char c : t.toCharArray()) {
            count[c - 'a']++;
        }
        for (char c : s.toCharArray()) {
            count[c - 'a']--;
        }
        for (int i = 0; i < count.length; i++) {
            if (count[i] == 1) return (char) (i + 'a');
        }
        return 'a';
    }

}
