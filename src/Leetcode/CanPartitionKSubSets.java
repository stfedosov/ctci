package Leetcode;

/**
 * @author sfedosov on 11/10/19.
 */
public class CanPartitionKSubSets {

    public static boolean canPartitionKSubsets(int[] nums, int k) {
        int sum = 0;
        for (int x : nums) {
            sum += x;
        }
        if (sum % k != 0) return false;
        boolean[] used = new boolean[nums.length];
        return canPartition(nums, 0, 0, sum / k, k, used);
    }

    private static boolean canPartition(int[] nums, int index, int sum, int total, int k, boolean[] used) {
        if (k == 1) return true;
        if (sum == total) return canPartition(nums, 0, 0, total, k - 1, used);
        for (int i = index; i < nums.length; i++) {
            if (!used[i] && sum + nums[i] <= total) {
                used[i] = true;
                if (canPartition(nums, i + 1, sum + nums[i], total, k, used)) {
                    return true;
                }
                used[i] = false;
            }
        }
        return false;
    }

}
