package Leetcode;

import java.util.HashMap;
import java.util.Map;

public class ExpressiveWords {

    public static void main(String[] args) {
        assert expressiveWords("heeellooo", new String[]{"hello", "hi", "helo"}) == 1;
        assert expressiveWords("heeelllooo", new String[]{"hellllo"}) == 0;
        assert expressiveWords("aaa", new String[]{"aaaa"}) == 0;
        assert expressiveWords("sass", new String[]{"sa"}) == 0;
        assert expressiveWords("", new String[]{"hello", "hi", "helo"}) == 0;
    }

    public static int expressiveWords(String S, String[] words) {
        if (S.isEmpty()) return 0;
        Map<Integer, Map<Character, Integer>> map = convertToMap(S);
        int count = 0;
        LOOP:
        for (String word : words) {
            Map<Integer, Map<Character, Integer>> new_map = convertToMap(word);
            if (new_map.size() != map.size()) continue;
            for (int i = 0; i < map.size(); i++) {
                for (Map.Entry<Character, Integer> entry : map.get(i).entrySet()) {
                    Integer integer = new_map.get(i).get(entry.getKey());
                    if (integer == null || entry.getValue() < integer || (entry.getValue() > integer && entry.getValue() < 3))
                        continue LOOP;
                }
            }
            count++;
        }
        return count;
    }

    private static Map<Integer, Map<Character, Integer>> convertToMap(String S) {
        char current = S.charAt(0);
        int index = 0;
        Map<Integer, Map<Character, Integer>> map = new HashMap<>();
        Map<Character, Integer> submap = new HashMap<>();
        submap.put(current, 1);
        map.put(0, submap);
        for (int i = 1; i < S.length(); i++) {
            char c = S.charAt(i);
            Map<Character, Integer> prev;
            if (current != c) {
                index++;
                current = c;
                prev = new HashMap<>();
                map.put(index, prev);
            } else {
                prev = map.get(index);
            }
            prev.put(current, prev.getOrDefault(current, 0) + 1);
        }
        return map;
    }

    // -----------

    public static int expressiveWords2(String S, String[] words) {
        if (S == null || words == null) {
            return 0;
        }
        int count = 0;
        for (String word : words) {
            if (stretchy(S, word)) {
                count++;
            }
        }
        return count;
    }

    public static boolean stretchy(String S, String word) {
        if (word == null) {
            return false;
        }
        int i = 0, j = 0;
        while (i < S.length() && j < word.length()) {
            if (S.charAt(i) == word.charAt(j)) {
                int len1 = getRepeatedLength(S, i);
                int len2 = getRepeatedLength(word, j);
                if (len1 < 3 && len1 != len2 || len1 < len2) {
                    return false;
                }
                i += len1;
                j += len2;
            } else {
                return false;
            }
        }
        return i == S.length() && j == word.length();
    }

    public static int getRepeatedLength(String str, int i) {
        int j = i;
        while (j < str.length() && str.charAt(j) == str.charAt(i)) {
            j++;
        }
        return j - i;
    }

}
