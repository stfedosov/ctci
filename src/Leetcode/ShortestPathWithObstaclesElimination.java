package Leetcode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author sfedosov on 12/31/19.
 */
public class ShortestPathWithObstaclesElimination {

    public static void main(String[] args) {
        shortestPath(new int[][]{
                        {0, 1, 0, 0, 0, 1, 0, 0},
                        {0, 1, 0, 1, 0, 1, 0, 1},
                        {0, 0, 0, 1, 0, 0, 1, 0}},
                1);
    }

    static int min = Integer.MAX_VALUE;

    public static int shortestPath(int[][] grid, int k) {
        bfs(grid, 0, 0, k);
        return min == Integer.MAX_VALUE ? -1 : min;
    }

    private static void bfs(int[][] grid, int i, int j, int k) {
        Queue<int[]> q = new LinkedList<>();
        q.offer(new int[]{0, 0, 0, k});
        boolean[][][] visited = new boolean[grid.length][grid[i].length][k + 1];
        visited[0][0][k] = true;
        while (!q.isEmpty()) {
            int[] polled = q.poll();
            int x = polled[0];
            int y = polled[1];
            int dist = polled[2];
            int kk = polled[3];
            if (x == grid.length - 1 && y == grid[i].length - 1) {
                min = Math.min(min, dist);
            }
            if (isSafe(x + 1, y, grid, visited, kk)) {
                if (grid[x + 1][y] == 1 && kk > 0 && isSafe(x + 1, y, grid, visited, kk - 1)) {
                    q.offer(new int[]{x + 1, y, dist + 1, kk - 1});
                    visited[x + 1][y][kk - 1] = true;
                } else if (grid[x + 1][y] == 0) {
                    q.offer(new int[]{x + 1, y, dist + 1, kk});
                    visited[x + 1][y][kk] = true;
                }
            }
            if (isSafe(x - 1, y, grid, visited, kk)) {
                if (grid[x - 1][y] == 1 && kk > 0 && isSafe(x - 1, y, grid, visited, kk - 1)) {
                    q.offer(new int[]{x - 1, y, dist + 1, kk - 1});
                    visited[x - 1][y][kk - 1] = true;
                } else if (grid[x - 1][y] == 0) {
                    q.offer(new int[]{x - 1, y, dist + 1, kk});
                    visited[x - 1][y][kk] = true;
                }
            }
            if (isSafe(x, y + 1, grid, visited, kk)) {
                if (grid[x][y + 1] == 1 && kk > 0 && isSafe(x, y + 1, grid, visited, kk - 1)) {
                    q.offer(new int[]{x, y + 1, dist + 1, kk - 1});
                    visited[x][y + 1][kk - 1] = true;
                } else if (grid[x][y + 1] == 0) {
                    q.offer(new int[]{x, y + 1, dist + 1, kk});
                    visited[x][y + 1][kk] = true;
                }
            }
            if (isSafe(x, y - 1, grid, visited, kk)) {
                if (grid[x][y - 1] == 1 && kk > 0 && isSafe(x, y - 1, grid, visited, kk - 1)) {
                    q.offer(new int[]{x, y - 1, dist + 1, kk - 1});
                    visited[x][y - 1][kk - 1] = true;
                } else if (grid[x][y - 1] == 0) {
                    q.offer(new int[]{x, y - 1, dist + 1, kk});
                    visited[x][y - 1][kk] = true;
                }
            }
        }
    }

    private static boolean isSafe(int x, int y, int[][] grid, boolean[][][] visited, int kk) {
        return x >= 0 && y >= 0 && kk >= 0 && x < grid.length && y < grid[0].length && !visited[x][y][kk];
    }

}
