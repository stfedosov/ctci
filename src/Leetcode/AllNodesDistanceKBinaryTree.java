package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author sfedosov on 11/29/18.
 */
public class AllNodesDistanceKBinaryTree {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(3);
        TreeNode five = new TreeNode(5);
        TreeNode two = new TreeNode(2);
        TreeNode six = new TreeNode(6);
        TreeNode seven = new TreeNode(7);
        TreeNode four = new TreeNode(4);
        TreeNode zero = new TreeNode(0);
        TreeNode eight = new TreeNode(8);
        TreeNode one = new TreeNode(1);
        five.left = six;
        five.right = two;
        two.left = seven;
        two.right = four;
        one.left = zero;
        one.right = eight;
        root.left = five;
        root.right = one;
        assert distanceK(root, five, 2).equals(Arrays.asList(1, 4, 7));
        //         3
        //       /   \
        //      5     1
        //     / \   / \
        //    6   2  0  8
        //       / \
        //       7  4

        TreeNode root2 = new TreeNode(0);
        root2.left = new TreeNode(2);
        TreeNode one2 = new TreeNode(1);
        root2.right = one2;
        TreeNode tree2 = new TreeNode(3);
        one2.left = tree2;
        //        0
        //       / \
        //      2   1
        //         /
        //        3
        assert distanceK(root2, tree2, 3).equals(Collections.singletonList(2));
    }

    public static List<Integer> distanceK(TreeNode root, TreeNode target, int K) {
        Map<TreeNode, TreeNode> graph = new HashMap<>();
        buildGraph(root, null, graph);
        List<Integer> result = new ArrayList<>();
        Set<TreeNode> visited = new HashSet<>();
        dfs(target, graph, K, result, visited);
        return result;
    }

    private static void buildGraph(TreeNode curr, TreeNode parent, Map<TreeNode, TreeNode> graph) {
        if (curr == null) return;
        graph.put(curr, parent);
        buildGraph(curr.left, curr, graph);
        buildGraph(curr.right, curr, graph);
    }

    private static void dfs(TreeNode target, Map<TreeNode, TreeNode> parent,
                            int level, List<Integer> res, Set<TreeNode> visited) {
        if (target == null || visited.contains(target)) return;
        if (level == 0) res.add(target.val);
        else {
            visited.add(target);
            dfs(target.left, parent, level - 1, res, visited);
            dfs(target.right, parent, level - 1, res, visited);
            dfs(parent.get(target), parent, level - 1, res, visited);
        }
    }
}
