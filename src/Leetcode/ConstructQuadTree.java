package Leetcode;

/**
 * @author sfedosov on 7/15/19.
 */
public class ConstructQuadTree {

    public static Node construct(int[][] grid) {
        return helper(grid, 0, 0, grid.length);
    }

    private static Node helper(int[][] grid, int x, int y, int length) {
        if (length == 1) {
            return new Node(grid[x][y] == 1, true, null, null, null, null);
        }
        Node result = new Node();
        Node topLeft = helper(grid, x, y, length / 2);
        Node topRight = helper(grid, x, y + length / 2, length / 2);
        Node bottomLeft = helper(grid, x + length / 2, y, length / 2);
        Node bottomRight = helper(grid, x + length / 2, y + length / 2, length / 2);
        if (topLeft.isLeaf && topRight.isLeaf
                && bottomLeft.isLeaf && bottomRight.isLeaf
                && topLeft.val == topRight.val
                && topRight.val == bottomLeft.val
                && bottomLeft.val == bottomRight.val) {
            result.isLeaf = true;
            result.val = topLeft.val;
        } else {
            result.topLeft = topLeft;
            result.topRight = topRight;
            result.bottomLeft = bottomLeft;
            result.bottomRight = bottomRight;
        }
        return result;
    }

    public static void main(String[] args) {
        construct(new int[][]{
                {1, 1, 1, 1, 0, 0, 0, 0},
                {1, 1, 1, 1, 0, 0, 0, 0},
                {1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 0, 0, 0, 0},
                {1, 1, 1, 1, 0, 0, 0, 0},
                {1, 1, 1, 1, 0, 0, 0, 0},
                {1, 1, 1, 1, 0, 0, 0, 0}
        });
    }

    // 1  1  1  1 | 0  0 | 0  0
    // 1  1  1  1 | 0  0 | 0  0
    //            | -----------
    // 1  1  1  1 | 1  1 | 1  1
    // 1  1  1  1 | 1  1 | 1  1
    // ---------- | -----------
    // 1  1  1  1 |  0  0  0  0
    // 1  1  1  1 |  0  0  0  0
    // 1  1  1  1 |  0  0  0  0
    // 1  1  1  1 |  0  0  0  0

    static class Node {
        public boolean val;
        public boolean isLeaf;
        public Node topLeft;
        public Node topRight;
        public Node bottomLeft;
        public Node bottomRight;

        public Node() {
        }

        public Node(boolean _val,
                    boolean _isLeaf,
                    Node _topLeft,
                    Node _topRight,
                    Node _bottomLeft,
                    Node _bottomRight) {
            val = _val;
            isLeaf = _isLeaf;
            topLeft = _topLeft;
            topRight = _topRight;
            bottomLeft = _bottomLeft;
            bottomRight = _bottomRight;
        }
    }
}
