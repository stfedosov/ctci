package Leetcode;

public class KthMissingElementInSortedArrayStaringFromTheLeft {

    public static void main(String[] args) {
        assert missingElementOptimal(new int[]{4, 7, 9, 10}, 1) == 5;
        assert missingElementOptimal(new int[]{4, 7, 9, 10}, 3) == 8;
        assert missingElementOptimal(new int[]{1, 2, 4}, 3) == 6;

        assert missingElementBruteForce(new int[]{4, 7, 9, 10}, 1) == 5;
        assert missingElementBruteForce(new int[]{4, 7, 9, 10}, 3) == 8;
        assert missingElementBruteForce(new int[]{1, 2, 4}, 3) == 6;
    }

    private static int missingElementOptimal(int[] arr, int k) {
        int left = 0, right = arr.length - 1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            int missed = arr[mid] - mid - arr[0];
            if (missed < k) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        // int missing = nums[mid] - nums[0] - mid
        // Here ^^ we simply calculate the amount of missing elements starting from the leftmost one.
        // Then we go either the left or right depending on the k value.

        // At the end of the loop, left > right, and the kth missing is in-between nums[right] and nums[left].
        // The number of integers missing before nums[right] is nums[right] - right - nums[0] -->
        // the number to return is nums[right] + k - (nums[right] - right - nums[0]) = k + right + nums[0]
        return arr[0] + k + right;
    }

    private static int missingElementBruteForce(int[] nums, int k) {
        int n = nums.length;

        for (int i = 1; i < n; i++) {
            int missedInGap = nums[i] - nums[i - 1] - 1;
            if (missedInGap >= k) {
                return nums[i - 1] + k;
            }
            k -= missedInGap;
        }

        return nums[n - 1] + k;
    }


}
