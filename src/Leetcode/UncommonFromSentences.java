package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sfedosov on 9/13/19.
 */
public class UncommonFromSentences {

    public static void main(String[] args) {
        assert Arrays.equals(uncommonFromSentences("this apple is sweet", "this apple is sour"),
                new String[]{"sweet", "sour"});
        assert Arrays.equals(uncommonFromSentences("apple apple", "banana"),
                new String[]{"banana"});
    }

    public static String[] uncommonFromSentences(String A, String B) {
        String[] sA = A.split(" ");
        String[] sB = B.split(" ");
        if (sA.length == 0 && sB.length == 0) return new String[0];
        else if (A.length() == 0) return sB;
        else if (B.length() == 0) return sA;

        Map<String, Integer> mapWithCounts = new HashMap<>();
        for (String word : sA) {
            mapWithCounts.put(word, mapWithCounts.getOrDefault(word, 0) + 1);
        }
        for (String word : sB) {
            mapWithCounts.put(word, mapWithCounts.getOrDefault(word, 0) + 1);
        }

        List<String> result = new ArrayList<>();
        for (String word : mapWithCounts.keySet()) {
            if (mapWithCounts.get(word) == 1) result.add(word);
        }

        return result.toArray(new String[0]);
    }

}
