package Leetcode;

/**
 * @author sfedosov on 11/7/19.
 */
public class RectangleOverlap {

    public static void main(String[] args) {
        assert isRectangleOverlap(new int[]{0, 0, 2, 2}, new int[]{1, 1, 3, 3});
        assert !isRectangleOverlap(new int[]{0, 0, 1, 1}, new int[]{1, 0, 2, 1});
        assert !isRectangleOverlap(new int[]{-5, 8, 0, 8}, new int[]{-5, 4, 5, 5});
    }

    public static boolean isRectangleOverlap(int[] rec1, int[] rec2) {
        int x1 = rec1[0], y1 = rec1[1], x2 = rec1[2], y2 = rec1[3];
        int x21 = rec2[0], y21 = rec2[1], x22 = rec2[2], y22 = rec2[3];
        int height = Math.min(y2, y22) - Math.max(y1, y21),
                width = Math.min(x22, x2) - Math.max(x1, x21);
        return height > 0 && width > 0;
    }

}
