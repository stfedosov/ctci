package Leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 3/19/19.
 */
public class DegreeOfAnArray {

    public static void main(String[] args) {
        System.out.println(findShortestSubArray(new int[]{2, 1, 1, 2, 1, 3, 3, 3, 1, 3, 1, 3, 2}));
    }

    public static int findShortestSubArray(int[] nums) {
        int freq = 0;
        Map<Integer, Integer> map = new HashMap<>();
        for (int x : nums) {
            int newValue = map.getOrDefault(x, 0) + 1;
            map.put(x, newValue);
            freq = Math.max(freq, newValue);
        }
        int min = nums.length;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() == freq) {
                int mostFrequentElement = entry.getKey();
                int start = 0;
                int end = nums.length - 1;
                while (start < end) {
                    if (nums[start] == mostFrequentElement && nums[end] == mostFrequentElement) break;
                    else if (nums[start] != mostFrequentElement) start++;
                    else if (nums[end] != mostFrequentElement) end--;
                }
                min = Math.min(min, end - start + 1);
            }
        }
        return min;
    }

    public int findShortestSubArray2(int[] nums) {
        if (nums == null || nums.length == 0) return 0;
        Map<Integer, Integer> countMap = new HashMap<>();
        Map<Integer, Integer> firstMap = new HashMap<>();
        Map<Integer, Integer> lastMap = new HashMap<>();
        int max = 0, res = Integer.MAX_VALUE;
        for (int i = 0; i < nums.length; i++) {
            if (!countMap.containsKey(nums[i])) {
                countMap.put(nums[i], 1);
                firstMap.put(nums[i], i);
                lastMap.put(nums[i], i);
            } else {
                countMap.put(nums[i], countMap.get(nums[i]) + 1);
                lastMap.put(nums[i], i);
            }
            max = Math.max(max, countMap.get(nums[i]));
        }

        for (Map.Entry<Integer, Integer> entry : countMap.entrySet()) {
            if (entry.getValue() == max) {
                res = Math.min(res, lastMap.get(entry.getKey()) - firstMap.get(entry.getKey()) + 1);
            }
        }

        return res;
    }

}
