package Leetcode;

public class LongestValidParenthesis {

    public static void main(String[] args) {
        System.out.println(longestValidParentheses("(()") == 2);
        System.out.println(longestValidParentheses("((((()))))") == 10);
        System.out.println(longestValidParentheses("()(()") == 2);
        System.out.println(longestValidParentheses("()(())") == 6);
        System.out.println(longestValidParentheses(")()())") == 4);
        System.out.println(longestValidParentheses(")()(((())))(") == 10);

        System.out.println(longestValidParentheses("(()") == 2);
        System.out.println(longestValidParentheses("((((()))))") == 10);
        System.out.println(longestValidParentheses("()(()") == 2);
        System.out.println(longestValidParentheses("()(())") == 6);
        System.out.println(longestValidParentheses(")()())") == 4);
        System.out.println(longestValidParentheses(")()(((())))(") == 10);
    }

    public static int longestValidParentheses(String s) {
        int left = 0, right = 0, maxLength = 0;

        // Left pass iteration
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                left++;
            } else {
                right++;
            }

            // Update the maximum length of valid parentheses
            if (left == right) {
                maxLength = Math.max(maxLength, 2 * right);
            } else if (right > left) { // Updating left and right to '0'
                left = right = 0;
            }
        }
        left = right = 0;

        // Right pass iteration
        for (int i = s.length() - 1; i >= 0; i--) {
            if (s.charAt(i) == '(') {
                left++;
            } else {
                right++;
            }

            // Update the maximum length of valid parentheses
            if (left == right) {
                maxLength = Math.max(maxLength, 2 * left);
            } else if (left > right) { // Updating left and right to '0'
                left = right = 0;
            }
        }
        return maxLength;
    }

}
