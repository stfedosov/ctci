package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 10/25/19.
 */
public class IncreasingTriplet {

    public static void main(String[] args) {
        assert increasingTriplet(new int[]{1, 2, 3, 4, 5});
        assert !increasingTriplet(new int[]{5, 4, 3, 2, 1});
    }

    public static boolean increasingTriplet(int[] nums) {
        int[] maxes = new int[nums.length];
        Arrays.fill(maxes, 1);
        int max = 1;
        for (int i = 1; i < nums.length; i++) {
            for (int j = 0; j < i; j++) {
                if (nums[i] > nums[j]) {
                    maxes[i] = Math.max(maxes[i], 1 + maxes[j]);
                }
            }
            max = Math.max(max, maxes[i]);
            if (max >= 3) return true;
        }
        return false;
    }

    // ---- good alternative

    public boolean increasingTripletAlternative(int[] nums) {
        // start with two largest values, as soon as we find a number bigger than both, while both have been updated, return true.
        int small = Integer.MAX_VALUE, big = Integer.MAX_VALUE;
        for (int n : nums) {
            if (n <= small) { // update small if n is smaller than both
                small = n;
            } else if (n <= big) { // update big only if greater than small but smaller than big
                big = n;
            } else { // return if you find a number bigger than both
                return true;
            }
        }
        return false;
    }

}
