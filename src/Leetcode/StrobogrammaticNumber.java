package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author sfedosov on 1/15/20.
 */
public class StrobogrammaticNumber {

    public static void main(String[] args) {
        assert findStrobogrammatic(2).equals(Arrays.asList("11", "69", "88", "96"));
        assert findStrobogrammatic(3).equals(Arrays.asList("101", "609", "808", "906", "111", "619", "818", "916", "181", "689", "888", "986"));
    }

    public static List<String> findStrobogrammatic(int n) {
        return findStrobogrammatic(n, n);
    }

    private static List<String> findStrobogrammatic(int initial, int n) {
        if (n == 0) return Arrays.asList("");
        if (n == 1) return Arrays.asList("1", "8", "0");
        List<String> nn = findStrobogrammatic(initial, n - 2);
        List<String> res = new ArrayList<>();
        for (String s : nn) {
            if (n != initial) res.add("0" + s + "0");
            res.add("1" + s + "1");
            res.add("6" + s + "9");
            res.add("8" + s + "8");
            res.add("9" + s + "6");
        }
        return res;
    }


}
