package Leetcode;

import Leetcode.RotateList.ListNode;

/**
 * @author sfedosov on 3/14/18.
 */
public class ReverseList {

    public static void main(String[] args) {
//        System.out.println(Arrays.toString(reverse(new int[]{1, 2, 3, 4, 5, 6})));
        ListNode listNode = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode3 = new ListNode(3);
        ListNode listNode4 = new ListNode(4);
        listNode4.next = new ListNode(5);
        listNode3.next = listNode4;
        listNode2.next = listNode3;
        listNode.next = listNode2;
        reverseRecursively(reverseRecursively(listNode));
        print(listNode);
        System.out.println(reverse(listNode));
    }

    private static int[] reverse(int[] ints) {
        int left = 0;
        int right = ints.length - 1;
        while (left < right) {
            int tmp = ints[left];
            ints[left] = ints[right];
            ints[right] = tmp;
            left++;
            right--;
        }
        return ints;
    }

    private static void print(ListNode node) {
        if (node == null) {
            return;
        }
        print(node.next);
        System.out.println(node.val);
    }

    private static ListNode reverse(ListNode head) {
        ListNode next;
        ListNode current = head;
        ListNode previous = null;
        while (current != null) {
            next = current.next;
            current.next = previous;
            previous = current;
            current = next;
        }
        return previous;
    }

    private static ListNode reverseRecursively(ListNode head) {
        if (head == null) {
            return null;
        } else if (head.next == null) {
            return head;
        } else {
            ListNode next = head.next;
            head.next = null;
            ListNode rest = reverseRecursively(next);
            next.next = head;
            return rest;
        }
    }

}
