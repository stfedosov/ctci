package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author sfedosov on 11/23/19.
 */
public class LetterCasePermutation {

    public static void main(String[] args) {
        assert letterCasePermutation("a1b2").equals(Arrays.asList("a1b2", "a1B2", "A1b2", "A1B2"));
    }

    public static List<String> letterCasePermutation(String S) {
        List<String> result = new ArrayList<>();
        letterCasePermutation(S, 0, result, "");
        return result;
    }

    private static void letterCasePermutation(String s, int idx, List<String> result, String current) {
        if (idx == s.length()) {
            result.add(current);
        } else {
            char c = s.charAt(idx);
            letterCasePermutation(s, idx + 1, result, current + c);
            if (Character.isLetter(c))
                letterCasePermutation(s, idx + 1, result,
                        current + (Character.isUpperCase(c) ? Character.toLowerCase(c) : Character.toUpperCase(c)));
        }
    }

}
