package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class TaskScheduler {

    /*
    Given a characters array tasks, representing the tasks a CPU needs to do,
    where each letter represents a different task.
    Tasks could be done in any order.
    Each task is done in one unit of time.
    For each unit of time, the CPU could complete either one task or just be idle.
     */
    public static void main(String[] args) {
//        System.out.println(leastIntervalPrimary(new char[]{'A', 'A', 'A', 'B', 'B', 'B'}, 0));
        System.out.println(leastIntervalSecondary(new char[]{'A', 'A', 'A', 'B', 'B', 'B'}, 2));
        System.out.println(leastIntervalPrimary(new char[]{'A', 'A', 'A', 'B', 'B', 'B'}, 2));
        System.out.println(leastIntervalPrimary(new char[]{'A', 'A', 'A', 'A', 'A', 'A', 'B', 'C', 'D', 'E', 'F', 'G'}, 2));
    }

    public static int leastIntervalPrimary(char[] tasks, int n) {
        if (tasks == null || tasks.length == 0) {
            return 0;
        }
        int[] count = new int[26];
        /* Build the count array with frequency of each task */
        for (char c : tasks) {
            count[c - 'A']++;
        }
        Arrays.sort(count);
        /* Maximum number of idle slots is defined by the frequency of the most frequent task */
        int maxFreq = count[25];
        int idleSlots = (maxFreq - 1) * n;

        /* Iterate over rest of the array and reduce the idle space count */
        for (int i = 24; i >= 0; i--) {
            idleSlots -= Math.min(maxFreq - 1, count[i]);
        }
        /* Handle cases when idleSlots become negative */
        idleSlots = Math.max(0, idleSlots);
        return tasks.length + idleSlots;
    }

    public static int leastIntervalSecondary(char[] tasks, int n) {
        Map<Character, Integer> map = new HashMap<>();
        for (char task : tasks) {
            map.put(task, map.getOrDefault(task, 0) + 1);
        }
        Queue<Integer> q = new PriorityQueue<>(Collections.reverseOrder());
        q.addAll(map.values());
        int count = 0;
        while (!q.isEmpty()) {
            List<Integer> tmpList = new ArrayList<>();
            int k = n + 1;
            while (k > 0 && !q.isEmpty()) {
                int polled = q.poll(); // poll the most frequent task
                if (polled > 1) {
                    tmpList.add(polled - 1); // decrease frequency, meaning it got executed and add back to queue later
                }
                k--;
                count++;
            }
            q.addAll(tmpList); // add valid tasks if any remains
            if (q.isEmpty()) {
                break;
            }
            count += k; // if k > 0, then it means we need to be idle
        }
        return count;
    }

}
