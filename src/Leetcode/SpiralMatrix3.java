package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 1/18/20.
 */
public class SpiralMatrix3 {

    public static void main(String[] args) {
        assert Arrays.deepEquals(spiralMatrixIII(1, 4, 0, 0),
                new int[][]{
                        {0, 0}, {0, 1},
                        {0, 2}, {0, 3}
                });
        assert Arrays.deepEquals(spiralMatrixIII(5, 6, 1, 4),
                new int[][]{
                        {1, 4}, {1, 5}, {2, 5}, {2, 4}, {2, 3},
                        {1, 3}, {0, 3}, {0, 4}, {0, 5}, {3, 5},
                        {3, 4}, {3, 3}, {3, 2}, {2, 2}, {1, 2},
                        {0, 2}, {4, 5}, {4, 4}, {4, 3}, {4, 2},
                        {4, 1}, {3, 1}, {2, 1}, {1, 1}, {0, 1},
                        {4, 0}, {3, 0}, {2, 0}, {1, 0}, {0, 0}
                });
    }

    public static int[][] spiralMatrixIII(int R, int C, int r0, int c0) {
        int i = 1, step = 0, z, max = R * C;
        int[][] result = new int[max][2];
        result[0] = new int[]{r0, c0};
        while (i < max) {
            step++;
            z = 0;
            while (step > z++) {
                c0++;
                if (r0 >= 0 && r0 < R && c0 >= 0 && c0 < C) result[i++] = new int[]{r0, c0};
            }
            z = 0;
            while (step > z++) {
                r0++;
                if (r0 >= 0 && r0 < R && c0 >= 0 && c0 < C) result[i++] = new int[]{r0, c0};
            }
            step++;
            z = 0;
            while (step > z++) {
                c0--;
                if (r0 >= 0 && r0 < R && c0 >= 0 && c0 < C) result[i++] = new int[]{r0, c0};
            }
            z = 0;
            while (step > z++) {
                r0--;
                if (r0 >= 0 && r0 < R && c0 >= 0 && c0 < C) result[i++] = new int[]{r0, c0};
            }
        }
        return result;
    }

    public static int[][] spiralMatrixIII_Shorter(int R, int C, int r0, int c0) {
        int[][] res = new int[R * C][2];
        int[][] dirs = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}}; //east, south, west, north
        int steps = 0, dir = 0, count = 0;
        res[0] = new int[]{r0, c0};
        count++;
        while (count < R * C) {
            if (dir == 0 || dir == 2)
                steps++;
            for (int i = 0; i < steps; i++) {
                r0 += dirs[dir][0];
                c0 += dirs[dir][1];
                if (r0 >= 0 && r0 < R && c0 >= 0 && c0 < C) {
                    res[count++] = new int[]{r0, c0};
                }
            }
            dir = (dir + 1) % 4;
        }
        return res;
    }

}
