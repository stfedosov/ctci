package Leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 2/12/18.
 */
public class NumberToWord {

    static Map<Integer, String> numberMap = new HashMap<Integer, String>() {
        {
            put(0, "Zero");
            put(1, "One");
            put(2, "Two");
            put(3, "Three");
            put(4, "Four");
            put(5, "Five");
            put(6, "Six");
            put(7, "Seven");
            put(8, "Eight");
            put(9, "Nine");
            put(10, "Ten");
            put(11, "Eleven");
            put(12, "Twelve");
            put(13, "Thirteen");
            put(14, "Fourteen");
            put(15, "Fifteen");
            put(16, "Sixteen");
            put(17, "Seventeen");
            put(18, "Eighteen");
            put(19, "Nineteen");
            put(20, "Twenty");
            put(30, "Thirty");
            put(40, "Forty");
            put(50, "Fifty");
            put(60, "Sixty");
            put(70, "Seventy");
            put(80, "Eighty");
            put(90, "Ninety");
            put(100, "Hundred");
            put(1000, "Thousand");
        }
    };

    public static String numberToWords(int num) {
        String result = "";
        if (num >= 1_000_000_000) {
            int quotient = num / 1_000_000_000;
            int remainder = num % 1_000_000_000;
            result += numberToWords(quotient) + " Billion ";
            if (remainder > 0) {
                result += " " + numberToWords(remainder);
            }
        } else if (num >= 1_000_000) {
            int quotient = num / 1_000_000;
            int remainder = num % 1_000_000;
            result += numberToWords(quotient) + " Million ";
            if (remainder > 0) {
                result += " " + numberToWords(remainder);
            }
        } else if (num >= 1_000) {
            int quotient = num / 1_000;
            int remainder = num % 1_000;
            result += numberToWords(quotient) + " Thousand ";
            if (remainder > 0) {
                result += " " + numberToWords(remainder);
            }
        } else if (num >= 100) {
            int quotient = num / 100;
            int remainder = num % 100;
            result += numberMap.get(quotient) + " Hundred " + (remainder > 0 ? " " + numberToWords(remainder) : " ");
        } else if (num > 20) {
            result += numberMap.get(num - (num % 10)) + (num % 10 == 0 ? " " : " " + numberMap.get(num % 10));
        } else {
            result += numberMap.get(num);
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(numberToWords(100000));
        System.out.println(numberToWords(999999));
        System.out.println(numberToWords(678900));
        System.out.println(numberToWords(0));
        System.out.println(numberToWords(100567));
        System.out.println(numberToWords(4589));
        System.out.println(numberToWords(3333));
        System.out.println(numberToWords(67500));
        System.out.println(numberToWords(72));
        System.out.println(numberToWords(172346));
        System.out.println(numberToWords(890000));
        System.out.println(numberToWords(600700));
        System.out.println(numberToWords(67));
        System.out.println(numberToWords(999999999));
    }
}