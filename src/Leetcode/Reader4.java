package Leetcode;

/**
 * @author sfedosov on 5/27/19.
 */
public class Reader4 {

    private static final String word = "abc";
    private static int i = 0;

    public static void main(String[] args) {
        char[] buf = new char[4];
        System.out.println(read(buf, 4));
        System.out.println(new String(buf, 0, 3));
        buf = new char[4];
        System.out.println(read(buf, 1));
        System.out.println(new String(buf, 0, 1));
    }

    private static char[] readed = new char[4];
    private static int readIndex = 0;
    private static int writeIndex = 0;

    public static int read(char[] buf, int n) {
        for (int i = 0; i < n; i++) {
            if (readIndex == writeIndex) {
                writeIndex = read4(readed);
                readIndex = 0;
                if (writeIndex == 0) {
                    return i;
                }
            }
            buf[i] = readed[readIndex++];
        }
        return n;
    }

    private static int read4(char[] buffer) {
        int limit = 4;
        int readed = 0;
        while (limit-- > 0) {
            if (i == word.length()) break;
            buffer[i] = word.charAt(i++);
            readed++;
        }
        return readed;
    }

}
