package Leetcode;

public class HammingDistance {

    public static void main(String[] args) {
        assert hammingDistance(1, 4) == 2;
        assert hammingDistance(3, 1) == 1;
    }

    public static int hammingDistance(int x, int y) {
        int dist = 0;
        int val = x ^ y;

        // Count the number of bits set
        while (val != 0) {
            // A bit is set, so increment the count and clear the bit
            dist += val & 1;
            val >>= 1;
        }

        // Return the number of differing bits
        return dist;
    }
}
