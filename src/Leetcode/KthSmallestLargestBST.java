package Leetcode;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * @author sfedosov on 8/6/19.
 */
public class KthSmallestLargestBST {

    // Given the root of a binary search tree, and an integer k, return the kth smallest element in the tree.
    public int kthSmallestStack(TreeNode root, int k) {
        Deque<TreeNode> stack = new ArrayDeque<>();

        while (true) {
            while (root != null) {
                stack.add(root);
                root = root.left;
            }
            root = stack.pollLast();
            if (--k == 0) return root.val;
            root = root.right;
        }
    }

}
