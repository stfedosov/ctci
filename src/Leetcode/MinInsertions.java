package Leetcode;

/**
 * @author sfedosov on 1/11/20.
 */
public class MinInsertions {

    public static void main(String[] args) {
        assert minInsertions("mbadm") == 2; // mbdadbm or mdbabdm -> 2
        assert minInsertions("zzazz") == 0; // no insertions needed
        assert minInsertions("leetcode") == 5; // leetcodocteel -> 5
    }

    public static int minInsertions(String s) {
        int[][] cache = new int[s.length() + 1][s.length() + 1];
        return minInsertions(s, 0, s.length() - 1, cache);
    }

    private static int minInsertions(String s, int start, int end, int[][] cache) {
        if (start >= end) return 0;
        if (cache[start][end] > 0) return cache[start][end];
        if (s.charAt(start) == s.charAt(end)) {
            cache[start][end] = minInsertions(s, start + 1, end - 1, cache);
        } else {
            cache[start][end] = Math.min(minInsertions(s, start + 1, end, cache), minInsertions(s, start, end - 1, cache)) + 1;
        }
        return cache[start][end];
    }

}
