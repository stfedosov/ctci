package Leetcode;

/**
 * @author sfedosov on 7/24/19.
 */
public class InorderSuccessor2 {

    class Node {
        public int val;
        public Node left;
        public Node right;
        public Node parent;
    }

    public static Node inorderSuccessor(Node x) {
        if (x.right == null) {
            Node ancestor = x.parent;
            while (ancestor != null && ancestor.val < x.val) {
                ancestor = ancestor.parent;
            }
            return ancestor;
        } else {
            return leftMostNode(x);
        }
    }

    private static Node leftMostNode(Node x) {
        Node result = x.right;
        while (result.left != null) {
            result = result.left;
        }
        return result;
    }

}
