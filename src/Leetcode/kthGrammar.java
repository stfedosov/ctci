package Leetcode;

public class kthGrammar {

    public static void main(String[] args) {
        assert kthGrammar(1, 1) == 0;
        // row 1: 0

        assert kthGrammar(2, 2) == 1;
        // row 1: 0
        // row 2: 01
    }

    //               K = 1       K = 2
    // Row N - 1       0           1
    //                 |           |
    //                 V           V
    // Row N        0     1     1     0
    //            K = 1 K = 2 K = 3 K = 4

    // Time complexity -> O(N)
    public static int dfs(int n, int k, int rootVal) {
        if (n == 1) {
            return rootVal;
        }

        int totalNodes = 1 << n - 1;

        // Target node will be present in the right half subtree of the current root node.
        if (k > (totalNodes / 2)) {
            int nextRootVal = (rootVal == 0) ? 1 : 0;
            return dfs(n - 1, k - (totalNodes / 2), nextRootVal);
        }
        // Otherwise, the target node is in the left subtree of the current root node.
        else {
            int nextRootVal = (rootVal == 0) ? 0 : 1;
            return dfs(n - 1, k, nextRootVal);
        }
    }

    public static int kthGrammar(int n, int k) {
        return dfs(n, k, 0);
    }

}
