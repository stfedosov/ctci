package Leetcode;

import java.util.TreeSet;

/**
 * @author sfedosov on 11/28/19.
 */
public class ExamRoom {

    public static void main(String[] args) {
        ExamRoom examRoom = new ExamRoom(10);
        System.out.println(examRoom.seat());
        System.out.println(examRoom.seat());
        System.out.println(examRoom.seat());
        System.out.println(examRoom.seat());
        examRoom.leave(4);
        System.out.println(examRoom.seat());

        ExamRoom examRoom1 = new ExamRoom(4);
        System.out.println(examRoom1.seat());
        System.out.println(examRoom1.seat());
        System.out.println(examRoom1.seat());
        System.out.println(examRoom1.seat());
        examRoom1.leave(1);
        examRoom1.leave(3);
        System.out.println(examRoom1.seat());

        ExamRoom examRoom2 = new ExamRoom(10);
        System.out.println(examRoom2.seat());
        System.out.println(examRoom2.seat());
        System.out.println(examRoom2.seat());
        examRoom2.leave(0);
        examRoom2.leave(4);
        System.out.println(examRoom2.seat());
        System.out.println(examRoom2.seat());
        System.out.println(examRoom2.seat());
        System.out.println(examRoom2.seat());
        System.out.println(examRoom2.seat());
        System.out.println(examRoom2.seat());
        System.out.println(examRoom2.seat());
        System.out.println(examRoom2.seat());
        System.out.println(examRoom2.seat());
        examRoom2.leave(0);
        examRoom2.leave(4);
        System.out.println(examRoom2.seat());
        System.out.println(examRoom2.seat());
        examRoom2.leave(7);
        System.out.println(examRoom2.seat());
        examRoom2.leave(3);
        System.out.println(examRoom2.seat());
        examRoom2.leave(3);
        System.out.println(examRoom2.seat());
        examRoom2.leave(9);
        System.out.println(examRoom2.seat());
        examRoom2.leave(0);
        examRoom2.leave(8);
        System.out.println(examRoom2.seat());
        System.out.println(examRoom2.seat());
        examRoom2.leave(0);
        examRoom2.leave(8);
        System.out.println(examRoom2.seat());
        System.out.println(examRoom2.seat());
        examRoom2.leave(2);
    }

    private TreeSet<Integer> set = new TreeSet<>();
    private int N;

    public ExamRoom(int N) {
        this.N = N;
    }

    public int seat() {
        if (set.size() == 0) {
            set.add(0);
            return 0;
        }
        int seat = 0;
        int max = set.first();
        Integer prev = null;
        for (Integer cur : set) {
            if (prev != null) {
                int distance = (cur - prev) / 2;
                if (distance > max) {
                    max = distance;
                    seat = prev + distance;
                }
            }
            prev = cur;
        }

        if (N - 1 - set.last() > max) {
            seat = N - 1;
        }
        set.add(seat);
        return seat;
    }

    public void leave(int p) {
        set.remove(p);
    }
}