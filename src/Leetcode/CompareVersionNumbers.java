package Leetcode;

/**
 * @author sfedosov on 8/23/19.
 */
public class CompareVersionNumbers {

    public static void main(String[] args) {
        assert compareVersion("0.1", "1.1") == -1;
        assert compareVersion("1.0.1", "1") == 1;
        assert compareVersion("7.5.2.4", "7.5.3") == -1;
        assert compareVersion("1.01", "1.001") == 0;
    }

    public static int compareVersion(String version1, String version2) {
        String[] split1 = version1.split("\\.");
        String[] split2 = version2.split("\\.");
        int p1 = 0, p2 = 0;
        while (p1 < split1.length || p2 < split2.length) {
            Integer i1 = null, i2 = null;
            if (p1 < split1.length) i1 = Integer.valueOf(split1[p1++]);
            if (p2 < split2.length) i2 = Integer.valueOf(split2[p2++]);
            if (i1 != null && i2 != null) {
                int compare = i1.compareTo(i2);
                if (compare != 0) return compare;
            } else if (i1 == null && i2 > 0) {
                return -1;
            } else if (i2 == null && i1 > 0) {
                return 1;
            }
        }
        return 0;
    }

}
