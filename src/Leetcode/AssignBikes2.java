package Leetcode;

/**
 * @author sfedosov on 1/13/20.
 */
public class AssignBikes2 {

    public static void main(String[] args) {
        System.out.println(assignBikes(new int[][]{{0, 0}, {2, 1}}, new int[][]{{1, 2}, {3, 3}}));
    }

    public static int assignBikes(int[][] workers, int[][] bikes) {
        return minDistance(workers, bikes, new boolean[bikes.length], 0, 0);
    }

    public static int minDistance(int[][] workers, int[][] bikes, boolean[] used, int idx, int currDistance) {
        if (idx == workers.length) {
            return currDistance;
        }
        int minDist = Integer.MAX_VALUE;
        for (int i = 0; i < bikes.length; i++) {
            if (used[i]) continue;
            used[i] = true;
            minDist = Math.min(minDist,
                    minDistance(workers,
                            bikes,
                            used,
                            idx + 1,
                            currDistance + Math.abs(workers[idx][0] - bikes[i][0]) + Math.abs(workers[idx][1] - bikes[i][1])));
            used[i] = false;
        }
        return minDist;
    }

}
