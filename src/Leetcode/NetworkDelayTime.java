package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

/**
 * @author sfedosov on 9/16/19.
 */
public class NetworkDelayTime {

    // You are given a network of n nodes, labeled from 1 to n.
    // You are also given times, a list of travel times as directed edges times[i] = (ui, vi, wi),
    // where ui is the source node, vi is the target node, and wi is the time it takes for a signal to travel from source to target.
    //
    // We will send a signal from a given node k.
    // Return the time it takes for all the n nodes to receive the signal.
    // If it is impossible for all the n nodes to receive the signal, return -1
    public static void main(String[] args) {
        assert networkDelayTime(new int[][]{{2, 1, 1}, {2, 3, 1}, {3, 4, 1}}, 4, 2) == 2;
        assert networkDelayTime(new int[][]{{1, 2, 1}, {2, 3, 2}, {1, 3, 2}}, 3, 1) == 2;
        assert networkDelayTime(new int[][]{{1, 2, 1}, {2, 3, 2}, {1, 3, 4}}, 3, 1) == 3;
        assert networkDelayTime(new int[][]{{1, 2, 1}}, 2, 2) == -1;
    }

    // dijkstra algorithm
    public static int networkDelayTime(int[][] times, int N, int K) {
        Graph2 g = new Graph2();
        for (int[] time : times) {
            g.addEdge(time[0], time[1], time[2]);
        }
        int[] distances = new int[N + 1];
        Arrays.fill(distances, Integer.MAX_VALUE);
        // sorted by weight of each node
        Queue<int[]> minHeap = new PriorityQueue<>(Comparator.comparing(ints -> ints[1]));
        minHeap.offer(new int[]{K, 0});
        distances[K] = 0;
        Set<Integer> visited = new HashSet<>();
        while (!minHeap.isEmpty()) {
            int u = minHeap.poll()[0];
            if (visited.add(u)) {
                for (Edge2 edge : g.vertexToEdges.getOrDefault(u, Collections.emptyList())) {
                    distances[edge.v] = Math.min(distances[edge.v], edge.weight + distances[u]);
                    minHeap.offer(new int[]{edge.v, distances[edge.v]});
                }
            }
        }
        int max = 0;
        for (int i = 1; i < distances.length; i++) {
            if (distances[i] == Integer.MAX_VALUE) return -1;
            max = Math.max(max, distances[i]);
        }
        return max;
    }
}

class Graph2 {

    Map<Integer, List<Edge2>> vertexToEdges = new HashMap<>();

    void addEdge(int u, int v, int w) {
        vertexToEdges.computeIfAbsent(u, x -> new ArrayList<>()).add(new Edge2(v, w));
    }

}

class Edge2 {

    int v, weight;

    Edge2(int v, int weight) {
        this.v = v;
        this.weight = weight;
    }
}
