package Leetcode;

import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @author sfedosov on 5/10/20.
 */
public class kthSmallestSumMatrixSortedRows {

    /*
    You are given an m x n matrix mat that has its rows sorted in non-decreasing order and an integer k.
    You are allowed to choose exactly one element from each row to form an array.

    Return the kth smallest array sum among all possible arrays.
     */
    public static void main(String[] args) {
        assert kthSmallest(new int[][]{
                {1, 3, 11},
                {2, 4, 6}}, 5) == 7;
        // Choosing one element from each row, the first k smallest sum are:
        // [1,2], [1,4], [3,2], [3,4], [1,6]. Where the 5th sum is 7.

        assert kthSmallest(new int[][]{
                {1, 10, 10},
                {1, 4, 5},
                {2, 3, 6}}, 7) == 9;
        // Choosing one element from each row, the first k smallest sum are:
        //[1,1,2], [1,1,3], [1,4,2], [1,4,3], [1,1,6], [1,5,2], [1,5,3].
        // Where the 7th sum is 9.

        assert kthSmallest(new int[][]{
                {1, 3, 11},
                {2, 4, 6}}, 9) == 17;
    }

    // Time: O(m * n * k * log(k))
    // Space: O(k)
    // k = 5
    //  [1, 3, 11]
    //  [2, 4, 6 ]
    //  -> sum each element with the next row and get sums with each element in the sum
    //  1 + [2, 4, 6], 3 + [2, 4, 6], 11 + [2, 4, 6] ->
    //  3, 5, 7, 5, 7, 9, 13, 15, 17 -> sort -> 3, 5, 5, 7, 7, 9, 13, 15, 17 -> prune array, remove irrelevant elements
    // take kth smallest ones -> 3, 5, 5, 7, 7 => return 7 as the answer since we're returning the top of a max heap


    // k = 7
    // [1, 10, 10]
    // [1, 4,  5 ]
    // [2, 3,  6 ]
    // -> sum each element with the next row and get sums with each element in the sum
    // 1 + [1, 4, 5], 10 + [1, 4, 5], 10 + [1, 4, 5] ->
    // 2, 5, 6, 11, 14, 15, 11, 14, 15 -> sort -> 2, 5, 6, 11, 11, 14, 14, 15, 15 -> prune array ->
    // -> 2, 5, 6, 11, 11, 14, 14 -> 2 + [2, 3, 6], 5 + [2, 3, 6], 6 + [2, 3, 6] .... ->
    // -> 4, 5, 8, 7, 8, 11, 8, 9, 12, 13, 14, 17, 13, 14, 17, 16, 17, 20, 16, 17, 20 -> sort
    // -> 4, 5, 7, 8, 8, 8, 9, 11, 12, 13, 13, 14, 14, 16, 16, 17, 17, 17, 20, 20 -> prune
    // 4, 5, 7, 8, 8, 8, 9 => return 9
    public static int kthSmallest(int[][] mat, int k) {
        Queue<Integer> q = new PriorityQueue<>(Collections.reverseOrder());
        q.offer(0);
        for (int[] row : mat) {
            Queue<Integer> next = new PriorityQueue<>(Collections.reverseOrder());
            for (int val : q) {
                for (int element : row) {
                    next.offer(val + element);
                    if (next.size() > k) next.poll();
                }
            }
            q = next;
        }
        return q.peek();
    }
}
