package Leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Factors {

    public static void main(String[] args) {
        System.out.println(getFactors(12)); // [[2, 2, 3], [2, 6], [3, 4]]
    }

    public static List<List<Integer>> getFactors(int n) {
        if (n <= 1) return Collections.emptyList();
        List<List<Integer>> factors = new ArrayList<>();
        getFactors(n, 2, new ArrayList<>(), factors);
        return factors;
    }

    private static void getFactors(int n, int idx, List<Integer> list, List<List<Integer>> factors) {
        if (n <= 1) {
            factors.add(new ArrayList<>(list));
            return;
        }
        for (int i = idx; i <= n; i++) {
            if (n % i != 0) continue;  // can not get divided evenly
            if (i == n && list.size() == 0) continue; // if no factor selected yet
            list.add(i);
            getFactors(n / i, i, list, factors);
            list.remove(list.size() - 1);
        }
    }

}
