package Leetcode;

/**
 * @author sfedosov on 11/2/19.
 */
public class Tribonacci {

    public static void main(String[] args) {
        // T(n + 3) = T(n) + T(n + 1) + T(n + 2)
        assert tribonacci(4) == 4;
        assert tribonacci(5) == 7;
    }

    public static int tribonacci(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        if (n == 2) return 1;
        int first = 0;
        int second = 1;
        int third = 1;
        int nth = first + second + third;
        for (int i = 4; i <= n; i++) {
            first = second;
            second = third;
            third = nth;
            nth = first + second + third;
        }
        return nth;
    }

}
