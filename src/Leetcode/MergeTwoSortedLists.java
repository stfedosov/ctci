package Leetcode;

import Leetcode.RotateList.ListNode;

/**
 * @author sfedosov on 3/23/18.
 */
public class MergeTwoSortedLists {

    public static void main(String[] args) {
        ListNode listNode = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        listNode.next = listNode2;
        listNode2.next = new ListNode(4);

        // 1 -> 2 -> 4

        ListNode listNode1 = new ListNode(1);
        ListNode listNode3 = new ListNode(3);
        listNode1.next = listNode3;
        listNode3.next = new ListNode(4);

        // 1 -> 3 -> 4

//        System.out.println(mergeTwoLists(listNode, listNode1));

        // 1 -> 1 -> 2 -> 3 -> 4 -> 4

        System.out.println(mergeTwoLists(new ListNode(2), new ListNode(1)));
        System.out.println(mergeTwoLists(new ListNode(1), new ListNode(2)));

//        System.out.println(mergeTwoLists(listNode, new ListNode(5)));
//        System.out.println(mergeTwoLists(new ListNode(5), listNode));

        System.out.println(mergeTwoListsIterative(listNode, listNode1));

        // 1 -> 1 -> 2 -> 3 -> 4 -> 4

        System.out.println(mergeTwoListsIterative(new ListNode(2), new ListNode(1)));
        System.out.println(mergeTwoListsIterative(new ListNode(1), new ListNode(2)));

        System.out.println(mergeTwoListsIterative(listNode, new ListNode(5)));
        System.out.println(mergeTwoListsIterative(new ListNode(5), listNode));
    }

    public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null) {
            return l2;
        }
        if (l2 == null) {
            return l1;
        }
        if (l1.val < l2.val) {
            l1.next = mergeTwoLists(l1.next, l2);
            return l1;
        } else {
            l2.next = mergeTwoLists(l1, l2.next);
            return l2;
        }
    }

    public static ListNode mergeTwoListsIterative(ListNode l1, ListNode l2) {
        ListNode dummy = new ListNode(0);
        ListNode head = dummy;
        while (l1 != null && l2 != null) {
            if (l1.val <= l2.val) {
                dummy.next = new ListNode(l1.val);
                l1 = l1.next;
            } else {
                dummy.next = new ListNode(l2.val);
                l2 = l2.next;
            }
            dummy = dummy.next;
        }
        if (l1 != null) {
            dummy.next = l1;
        } else {
            dummy.next = l2;
        }
        return head.next;
    }

}
