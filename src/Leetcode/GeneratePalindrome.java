package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author sfedosov on 12/21/19.
 */
public class GeneratePalindrome {

    public static void main(String[] args) {
        assert generatePalindromesOptimized("aabb").equals(Arrays.asList("baab", "abba"));
        assert generatePalindromesOptimized("caabb").equals(Arrays.asList("bacab", "abcba"));
    }

    // Optimized backtracking

    public static List<String> generatePalindromesOptimized(String s) {
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            map.put(s.charAt(i), map.getOrDefault(s.charAt(i), 0) + 1);
        }
        int cnt = 0;
        StringBuilder sb = new StringBuilder();
        for (char c : map.keySet()) {
            if (map.get(c) % 2 == 1) {
                sb.append(c);
                cnt++;
            }
        }
        if (cnt > 1) return Collections.emptyList();
        List<String> result = new ArrayList<>();
        backtrack(map, s, result, sb);
        return result;
    }

    private static void backtrack(Map<Character, Integer> map, String s, List<String> result, StringBuilder sb) {
        if (sb.length() == s.length()) {
            result.add(sb.toString());
            return;
        }
        for (Map.Entry<Character, Integer> entry : map.entrySet()) {
            if (entry.getValue() > 1) {
                int val = entry.getValue();
                map.put(entry.getKey(), val - 2);
                sb.insert(0, entry.getKey());
                sb.append(entry.getKey());
                backtrack(map, s, result, sb);
                sb.deleteCharAt(0);
                sb.deleteCharAt(sb.length() - 1);
                map.put(entry.getKey(), val);
            }
        }
    }

    // Brute force (TLE) -----------------

    public static List<String> generatePalindromesBruteForce(String s) {
        Set<String> set = new HashSet<>();
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            map.put(s.charAt(i), map.getOrDefault(s.charAt(i), 0) + 1);
        }
        int cnt = 0;
        for (char c : map.keySet()) {
            if (map.get(c) % 2 == 1) cnt++;
        }
        if (cnt > 1) return Collections.emptyList();
        dfs(s.toCharArray(), 0, set);
        return new ArrayList<>(set);
    }

    private static void dfs(char[] array, int i, Set<String> result) {
        if (i == array.length) {
            if (isPalindrome(array)) result.add(new String(array));
            return;
        }
        for (int j = i; j < array.length; j++) {
            swap(array, i, j);
            dfs(array, i + 1, result);
            swap(array, i, j);
        }
    }

    private static boolean isPalindrome(char[] array) {
        int start = 0, end = array.length - 1;
        while (start < end) {
            if (array[start] != array[end]) return false;
            start++;
            end--;
        }
        return true;
    }

    private static void swap(char[] arr, int i, int j) {
        char tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }


}
