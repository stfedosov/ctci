package Leetcode;

import Leetcode.RotateList.ListNode;

public class IsPalindrome {

    public static void main(String[] args) {
        ListNode listNode = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        listNode.next = listNode2;
        ListNode listNode22 = new ListNode(2);
        listNode2.next = listNode22;
        listNode22.next = new ListNode(1);
        // 1 -> 2 -> 2 -> 1
        System.out.println(isPalindrome(listNode));

        ListNode listNode99 = new ListNode(1);
        ListNode listNode992 = new ListNode(2);
        listNode99.next = listNode992;
        ListNode listNode993 = new ListNode(3);
        listNode992.next = listNode993;
        ListNode listNode9922 = new ListNode(2);
        listNode993.next = listNode9922;
        listNode9922.next = new ListNode(1);
        // 1 -> 2 -> 3 -> 2 -> 1
        System.out.println(isPalindrome(listNode99));

        ListNode listNode1 = new ListNode(1);
        ListNode listNode0 = new ListNode(0);
        listNode1.next = listNode0;
        listNode0.next = new ListNode(0);
        // 1 -> 0 -> 0
        System.out.println(isPalindrome(listNode1));

        ListNode listNode4 = new ListNode(4);
        ListNode listNode5 = new ListNode(5);
        listNode4.next = listNode5;
        listNode5.next = new ListNode(3);
        // 4 -> 5 -> 3
        System.out.println(isPalindrome(listNode1));
    }

    public static boolean isPalindrome(ListNode head) {
        ListNode fast = head, slow = head;
        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        if (fast != null) { // odd nodes: let right half smaller
            slow = slow.next;
        }
        slow = reverse(slow);
        fast = head;

        while (slow != null && fast != null) {
            if (fast.val != slow.val) {
                return false;
            }
            fast = fast.next;
            slow = slow.next;
        }
        return true;
    }

    private static ListNode reverse(ListNode head) {
        ListNode prev = null, current = head;
        while (current != null) {
            ListNode next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        return prev;
    }

}
