package Leetcode;

/**
 * @author sfedosov on 1/31/19.
 */
public class Search2DMatrix {

    public static void main(String[] args) {
        int[][] matrix = {
                {1, 3, 5, 7},
                {10, 11, 16, 20},
                {23, 30, 34, 50}};
        assert searchMatrix(matrix, 3);
        assert !searchMatrix(matrix, 13);
    }

    public static boolean searchMatrix(int[][] matrix, int target) {
        int m = matrix.length;
        if (m == 0) return false;
        int n = matrix[0].length;

        // binary search
        int left = 0, right = m * n - 1;
        int mid, midValue;
        while (left <= right) {
            mid = (left + right) / 2;
            midValue = matrix[mid / n][mid % n];
            if (target == midValue) return true;
            else {
                if (target < midValue) right = mid - 1;
                else left = mid + 1;
            }
        }
        return false;
    }

}
