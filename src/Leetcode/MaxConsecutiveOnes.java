package Leetcode;

public class MaxConsecutiveOnes {

    // Given a binary array nums, return the maximum number of consecutive 1's in the array.
    public static void main(String[] args) {
        assert findMaxConsecutiveOnes(new int[]{1, 1, 0, 0, 0, 1, 1, 1}) == 3;
        assert findMaxConsecutiveOnes(new int[]{1, 1, 1, 1, 1, 0, 1, 1, 1, 1}) == 5;
    }

    public static int findMaxConsecutiveOnes(int[] nums) {
        int start = 0, max = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 1) {
                max = Math.max(max, i - start + 1);
            } else {
                start = i + 1;
            }
        }
        return max;
    }

}
