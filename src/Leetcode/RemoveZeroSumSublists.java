package Leetcode;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author sfedosov on 5/2/20.
 */
public class RemoveZeroSumSublists {

    public static void main(String[] args) {
        ListNode root = new ListNode(1, new ListNode(2, new ListNode(-3, new ListNode(3, new ListNode(1)))));
        // 1,2,-3,3,1 => 3,1
        assert removeZeroSumSublists(root).equals(new ListNode(3, new ListNode(1)));

        root = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(-3, new ListNode(-2)))));
        // 1,2,3,-3,-2 => 1
        assert removeZeroSumSublists(root).equals(new ListNode(1));
    }

    public static ListNode removeZeroSumSublists(ListNode head) {
        Map<Integer, ListNode> sumToFarthestNodeMap = new HashMap<>();

        // Need the dummy node to track the new head if changed.
        ListNode preHead = new ListNode(0);
        preHead.next = head;

        // First iteration to compute the map.
        int sum = 0;
        ListNode tmp = preHead;
        while (tmp != null) {
            sum += tmp.val;
            sumToFarthestNodeMap.put(sum, tmp);
            tmp = tmp.next;
        }

        // Second iteration to re-connect the nodes to the farthest node where the sum stays unchanged
        sum = 0;
        tmp = preHead;
        while (tmp != null) {
            sum += tmp.val;
            tmp.next = sumToFarthestNodeMap.get(sum).next;
            tmp = tmp.next;
        }

        // Done, return the head from preHead
        return preHead.next;
    }

    static class ListNode {
        int val;
        ListNode next;

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ListNode listNode = (ListNode) o;
            return val == listNode.val &&
                    Objects.equals(next, listNode.next);
        }

        @Override
        public int hashCode() {
            return Objects.hash(val, next);
        }
    }

}
