package Leetcode;

public class MinSizeSubarraySum {

    // Given an array of positive integers nums and a positive integer target,
    // return the minimal length of a contiguous subarray [numsl, numsl+1, ..., numsr-1, numsr]
    // of which the sum is greater than or equal to target.
    // If there is no such subarray, return 0 instead.
    public static void main(String[] args) {
        assert minSubArrayLen(7, new int[]{2, 3, 1, 2, 4, 3}) == 2;
        assert minSubArrayLen(3, new int[]{1, 1}) == 0;
    }

    public static int minSubArrayLen(int s, int[] nums) {
        int sum = 0, start = 0;
        int length = Integer.MAX_VALUE;
        for (int end = 0; end < nums.length; end++) {
            sum += nums[end];
            while (sum >= s) {
                length = Math.min(length, end - start + 1);
                sum -= nums[start++];
            }
        }
        return length == Integer.MAX_VALUE ? 0 : length;
    }

}
