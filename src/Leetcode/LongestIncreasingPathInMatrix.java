package Leetcode;

/**
 * @author sfedosov on 4/2/20.
 */
public class LongestIncreasingPathInMatrix {

    public static void main(String[] args) {
        assert longestIncreasingPath(new int[][]{
                {9, 9, 4},
                {6, 6, 8},
                {2, 1, 1}}) == 4;
        // The longest increasing path is [1, 2, 6, 9]

        assert longestIncreasingPath(new int[][]{{1}}) == 1;

        // The longest increasing path is [1]

        assert longestIncreasingPath(new int[][]{
                {3, 4, 5},
                {3, 2, 6},
                {2, 2, 1}}) == 4;
        // The longest increasing path is [3, 4, 5, 6]
    }

    private static int[][] directions = new int[][]{{-1, 0}, {1, 0}, {0, 1}, {0, -1}};

    public static int longestIncreasingPath(int[][] matrix) {
        int max = 1;
        if (matrix.length == 0) return 0;
        int[][] cache = new int[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                max = Math.max(max, dfs(matrix, i, j, cache));
            }
        }
        return max;
    }

    private static int dfs(int[][] matrix, int i, int j, int[][] cache) {
        int max = 1;
        if (cache[i][j] > 0) return cache[i][j];
        for (int[] dir : directions) {
            if (isValid(matrix, i + dir[0], j + dir[1], matrix[i][j])) {
                max = Math.max(max, 1 + dfs(matrix, i + dir[0], j + dir[1], cache));
            }
        }
        cache[i][j] = max;
        return cache[i][j];
    }

    private static boolean isValid(int[][] matrix, int i, int j, int prev) {
        return i >= 0 && j >= 0 && i < matrix.length && j < matrix[i].length && matrix[i][j] > prev;
    }
}
