package Leetcode;

import java.util.HashSet;
import java.util.Set;

public class LongestRepeatingSubstring {

    public static void main(String[] args) {
        System.out.println(longestRepeatingSubstring("abbaba"));
        System.out.println(longestRepeatingSubstring("abcd"));
        System.out.println(longestRepeatingSubstring("aabcaabdaab"));
        System.out.println(longestRepeatingSubstring("aaaaa"));
    }

    // Time complexity -> average is N * log N, N^2 in the worst case
    public static int longestRepeatingSubstring(String s) {
        int left = 1, right = s.length(), max = 0;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (search(mid, s.length(), s)) {
                left = mid + 1;
                max = mid;
            } else {
                right = mid - 1;
            }
        }
        return max;
    }

    // multiple optimizations can be applied:
    // Since storing the whole string can be space intensive, storing only hash integer might be the one such tweak.
    // But it might not work in case of hash collisions. The other approach is to use Rabin Karp algorithm which scales linearly.
    private static boolean search(int mid, int end, String s) {
        Set<String> seen = new HashSet<>();
        for (int start = 0; start < end - mid + 1; start++) {
            String tmp = s.substring(start, start + mid);
            if (seen.contains(tmp)) return true;
            seen.add(tmp);
        }
        return false;
    }

}
