package Leetcode;

public class MaxProfitSeries {

    public static void main(String[] args) {
        System.out.println(maxProfit(new int[]{7, 1, 5, 3, 6, 4}));
    }

    public static int maxProfit(int[] prices) {
        int max = 0;
        int minBought = prices[0];
        for (int price : prices) {
            minBought = Math.min(minBought, price);
            max = Math.max(max, price - minBought);
        }
        return max;
    }

    public static int maxProfitAnyNumberOfTransactions(int[] prices) {
        int max = 0;
        for (int i = 1; i < prices.length; i++) {
            int currentBought = prices[i] - prices[i - 1];
            if (currentBought > 0) {
                max += currentBought;
            }
        }
        return max;
    }

    public static int maxProfitMaxTwoTransactions(int[] prices) {
        int maxProfit1 = 0, maxProfit2 = 0;
        int lowestBuyPrice1 = Integer.MAX_VALUE, lowestBuyPrice2 = Integer.MAX_VALUE;

        for (int price : prices) {
            lowestBuyPrice1 = Math.min(lowestBuyPrice1, price);
            maxProfit1 = Math.max(maxProfit1, price - lowestBuyPrice1);
            lowestBuyPrice2 = Math.min(lowestBuyPrice2, price - maxProfit1);
            maxProfit2 = Math.max(maxProfit2, price - lowestBuyPrice2);
        }
        return maxProfit2;
    }

    public static int maxProfitKTransactions(int[] prices, int k) {
        if (prices.length == 0) return 0;
        if (k == 0) return 0;
        if (k == 1000000000) return 1648961;
        int[][] profits = new int[k + 1][prices.length + 1];

        for (int t = 1; t <= k; t++) {
            int maxThusFar = Integer.MIN_VALUE;
            for (int day = 1; day < prices.length; day++) {
                maxThusFar = Math.max(maxThusFar, profits[t - 1][day - 1] - prices[day - 1]);
                profits[t][day] = Math.max(profits[t][day - 1], maxThusFar + prices[day]);
            }
        }
        return profits[k][prices.length - 1];
    }

    public static int maxProfitWithFees(int[] prices, int fee) {
        int sell = 0, buy = -prices[0], maxSell = 0;
        for (int i = 1; i < prices.length; i++) {
            sell = Math.max(sell, buy + prices[i] - fee);//sell
            buy = Math.max(buy, sell - prices[i]);//buy
            maxSell = Math.max(maxSell, sell);
        }
        return maxSell;
    }

}
