package Leetcode;

/**
 * @author sfedosov on 12/16/19.
 */
public class ClosestValue {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(4);
        TreeNode left = new TreeNode(2);
        root.left = left;
        root.right = new TreeNode(5);
        left.left = new TreeNode(1);
        left.right = new TreeNode(3);
        assert closestValue(root, 3.714286) == 4;
    }

    static double min = Double.MAX_VALUE;
    static int candidate = -1;

    public static int closestValue(TreeNode root, double target) {
        traverse(root, target);
        return candidate;
    }

    private static void traverse(TreeNode root, double target) {
        if (root == null) {
            return;
        }
        double diff = Math.abs(root.val - target);
        if (min > diff) {
            min = diff;
            candidate = root.val;
        }
        if (root.val > target) {
            traverse(root.left, target);
        } else {
            traverse(root.right, target);
        }
    }

}
