package Leetcode;

public class MaximumSwap {

    public static void main(String[] args) {
        System.out.println(maximumSwap(98368));
        System.out.println(maximumSwap(2736));
        System.out.println(maximumSwap(1993));
    }

    // O(n)
    public static int maximumSwap(int num) {
        char[] numArray = String.valueOf(num).toCharArray();
        int[] maxRightIndexes = new int[numArray.length];
        maxRightIndexes[numArray.length - 1] = numArray.length - 1;
        for (int i = numArray.length - 2; i >= 0; i--) {
            if (numArray[i] > numArray[maxRightIndexes[i + 1]]) {
                maxRightIndexes[i] = i;
            } else {
                maxRightIndexes[i] = maxRightIndexes[i + 1];
            }
        }

        for (int i = 0; i < numArray.length; i++) {
            if (numArray[i] < numArray[maxRightIndexes[i]]) {
                swap(numArray, i, maxRightIndexes);
                return Integer.parseInt(new String(numArray));
            }
        }
        return num;
    }

    private static void swap(char[] numArray, int i, int[] maxRightIndexes) {
        char tmp = numArray[i];
        numArray[i] = numArray[maxRightIndexes[i]];
        numArray[maxRightIndexes[i]] = tmp;
    }
}
