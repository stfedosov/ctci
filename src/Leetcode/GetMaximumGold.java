package Leetcode;

/**
 * @author sfedosov on 3/15/20.
 */
public class GetMaximumGold {

    public static void main(String[] args) {
        assert getMaximumGold(new int[][]{
                {0, 6, 0},
                {5, 8, 7},
                {0, 9, 0}
        }) == 24;

        assert getMaximumGold(new int[][]{
                {1, 0, 7},
                {2, 0, 6},
                {3, 4, 5},
                {0, 3, 0},
                {9, 0, 20}
        }) == 28;

        assert getMaximumGold(new int[][]{
                {1, 0, 7, 0, 0, 0},
                {2, 0, 6, 0, 1, 0},
                {3, 5, 6, 7, 4, 2},
                {4, 3, 1, 0, 2, 0},
                {3, 0, 5, 0, 20, 0}
        }) == 60;
    }

    public static int getMaximumGold(int[][] grid) {
        int max = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] != 0) {
                    max = Math.max(max, dfs(grid, i, j));
                }
            }
        }
        return max;
    }

    private static int[][] dirs = new int[][]{{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

    // Time: O(4^m*n)
    // Space: O(m*n)
    private static int dfs(int[][] grid, int i, int j) {
        if (i < 0 || j < 0 || i >= grid.length || j >= grid[i].length || grid[i][j] == 0) return 0;
        int origin = grid[i][j];
        grid[i][j] = 0;
        int max = 0;
        for (int[] dir : dirs) {
            max = Math.max(max, dfs(grid, i + dir[0], j + dir[1]));
        }
        grid[i][j] = origin;
        return max + origin;
    }

}
