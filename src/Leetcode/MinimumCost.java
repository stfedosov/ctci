package Leetcode;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @author sfedosov on 8/15/19.
 */
public class MinimumCost {

    public static void main(String[] args) {
        assert minimumCost(4, new int[][]{{1, 2, 3}, {3, 4, 4}}) == -1;
        assert minimumCost(3, new int[][]{{1, 2, 5}, {1, 3, 6}, {2, 3, 1}}) == 6;
        assert minimumCost(5, new int[][]{{2, 1, 3267}, {3, 2, 25910}, {4, 1, 30518}}) == -1;
        assert minimumCost(4, new int[][]{{2, 1, 3267}, {3, 2, 25910}, {4, 1, 30518}}) == 59695;
    }

    public static int minimumCost(int N, int[][] connections) {
        List<List<int[]>> graph = createGraph(connections, N).adj;
        boolean[] visited = new boolean[N + 1];
        Queue<int[]> queue = new PriorityQueue<>(Comparator.comparingInt(o -> o[1]));
        int cost = 0, numberOfCitiesVisited = 0;

        queue.offer(new int[]{1, 0});
        while (!queue.isEmpty()) {
            int[] u = queue.poll();
            if (visited[u[0]]) continue;
            visited[u[0]] = true;
            cost += u[1]; // first time visit so accumulate the cost
            numberOfCitiesVisited++;

            for (int[] v : graph.get(u[0])) {
                if (!visited[v[0]]) queue.offer(new int[]{v[0], v[1]});
            }
        }

        return numberOfCitiesVisited == N ? cost : -1;
    }

    private static Graph createGraph(int[][] connections, int N) {
        Graph graph = new Graph(N);
        for (int[] connection : connections) {
            graph.addEdge(connection[0], connection[1], connection[2]);
        }
        return graph;
    }

    static class Graph {
        List<List<int[]>> adj;
        int N;

        Graph(int N) {
            this.N = N + 1;
            this.adj = new ArrayList<>();
            for (int i = 0; i < N + 1; i++) {
                adj.add(i, new ArrayList<>());
            }
        }

        void addEdge(int start, int end, int cost) {
            adj.get(start).add(new int[]{end, cost});
            adj.get(end).add(new int[]{start, cost});
        }

        int numOfConnectedComponents() {
            int count = 0;
            boolean[] visited = new boolean[N];
            for (int v = 0; v < N; v++) {
                if (!visited[v] && !adj.get(v).isEmpty()) {
                    dfs(v, visited);
                    count++;
                }
            }
            return count;
        }

        private void dfs(int v, boolean[] visited) {
            visited[v] = true;
            for (int[] adj : adj.get(v)) {
                if (!visited[adj[0]]) {
                    dfs(adj[0], visited);
                }
            }
        }
    }

}
