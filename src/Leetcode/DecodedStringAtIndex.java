package Leetcode;

public class DecodedStringAtIndex {

    public static void main(String[] args) {
        assert decodeAtIndex2("ha22", 5).equals("h");
        assert decodeAtIndex2("leet2code3", 10).equals("o");
        assert decodeAtIndex2("a2345678999999999999999", 1).equals("a");
        assert decodeAtIndex2("a2b3c4d5e6f7g8h9", 10).equals("c");
        assert decodeAtIndex2("y959q969u3hb22odq595", 222280369).equals("y");
    }

    private static String decodeAtIndex2(String S, int K) {
        long size = 0;
        for (char c : S.toCharArray()) {
            if (Character.isDigit(c)) {
                size *= c - '0';
            } else {
                size++;
            }
        }
        for (int i = S.length() - 1; i >= 0; i--) {
            char c = S.charAt(i);
            K %= size;
            if (K == 0 && Character.isLetter(c)) {
                return Character.toString(c);
            }
            if (Character.isDigit(c)) {
                size /= c - '0';
            } else {
                size--;
            }
        }
        return "";
    }

}
