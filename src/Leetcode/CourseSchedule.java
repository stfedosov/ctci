package Leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CourseSchedule {

    public static void main(String[] args) {
        assert canFinish(2, new int[][]{{1, 0}});
        assert !canFinish(2, new int[][]{{1, 0}, {0, 1}});
    }

    public static boolean canFinish(int numCourses, int[][] prerequisites) {
        Map<Integer, List<Integer>> graph = new HashMap<>();
        for (int[] prereq : prerequisites) {
            graph.computeIfAbsent(prereq[1], x -> new ArrayList<>()).add(prereq[0]);
        }
        Set<Integer> visited = new HashSet<>();
        for (int course = 0; course < numCourses; course++) {
            if (hasCycle(course, graph, visited, new HashSet<>())) {
                return false;
            }
        }
        return true;
    }

    // Time/Space: O(E + V), E - number of edges/dependencies, V - number of vertices/courses.
    private static boolean hasCycle(int course, Map<Integer, List<Integer>> graph, Set<Integer> visited, Set<Integer> visiting) {
        if (visited.contains(course)) {
            return false;
        }
        if (visiting.contains(course)) {
            return true;
        }
        visiting.add(course);
        for (int prereq : graph.getOrDefault(course, Collections.emptyList())) {
            if (hasCycle(prereq, graph, visited, visiting)) {
                return true;
            }
        }
        visited.add(course);
        return false;
    }

}
