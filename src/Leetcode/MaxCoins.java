package Leetcode;

import java.util.HashMap;
import java.util.Map;

public class MaxCoins {

    public static void main(String[] args) {
        // nums = [3,1,5,8] --> [3,5,8] -->   [3,8]   -->  [8]  --> []
        // coins =  3*1*5      +  3*5*8    +  1*3*8      + 1*8*1   = 167
        assert maxCoins(new int[]{3, 1, 5, 8}) == 167;
    }

    public static int maxCoins(int[] nums) {
        return maxCoins(nums, 0, nums.length - 1, new HashMap<>());
    }

    private static int maxCoins(int[] nums, int start, int end, Map<String, Integer> map) {
        String key = start + "," + end;
        if (map.containsKey(key)) return map.get(key);
        if (start > end) {
            return 0;
        } else {
            int max = 0;
            int left = start > 0 ? nums[start - 1] : 1;
            int right = end < nums.length - 1 ? nums[end + 1] : 1;
            for (int idx = start; idx <= end; idx++) {
                max = Math.max(max, maxCoins(nums, start, idx - 1, map) +
                        left * nums[idx] * right + maxCoins(nums, idx + 1, end, map));
            }
            map.put(key, max);
            return max;
        }
    }

}
