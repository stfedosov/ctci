package Leetcode;

/**
 * @author sfedosov on 10/17/19.
 */
public class ExcelSheetColumnTitle {

    public static void main(String[] args) {
        assert convertToTitle(701).equals("ZY");
        assert convertToTitle(702).equals("ZZ");
        assert convertToTitle(703).equals("AAA");
        assert convertToTitle(28).equals("AB");
        assert convertToTitle(14).equals("N");
        assert convertToTitle(1048).equals("ANH");
    }

    public static String convertToTitle(int n) {
        String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder res = new StringBuilder();
        while (n != 0) {
            int remainder = n % 26;
            if (remainder != 0) {
                res.insert(0, str.charAt(remainder - 1));
                n -= remainder;
            } else {
                res.insert(0, "Z");
                n -= 26;
            }
            n /= 26;
        }
        return res.toString();
    }

}
