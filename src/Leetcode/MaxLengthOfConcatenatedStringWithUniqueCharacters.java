package Leetcode;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author sfedosov on 3/24/20.
 */
public class MaxLengthOfConcatenatedStringWithUniqueCharacters {

    private static String maxString = "";

    public static void main(String[] args) {
        assert maxLength(Arrays.asList("cha", "r", "act", "ers")) == 6;
        maxString = "";
        assert maxLength(Arrays.asList("un", "iq", "ue")) == 4;
        maxString = "";
        assert maxLength(Collections.singletonList("abcdefghijklmnopqrstuvwxyz")) == 26;
    }

    public static int maxLength(List<String> arr) {
        dfs(arr, "", 0);
        return maxString.length();
    }

    private static void dfs(List<String> arr, String current, int i) {
        if (i == arr.size()) {
            if (current.length() > maxString.length() && isUnique(current)) maxString = current;
        } else {
            dfs(arr, current + arr.get(i), i + 1);
            dfs(arr, current, i + 1);
        }
    }

    private static boolean isUnique(String s) {
        int[] cnt = new int[26];
        for (char c : s.toCharArray()) {
            if (++cnt[c - 'a'] > 1) return false;
        }
        return true;
    }

}
