package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 * @author sfedosov on 12/8/19.
 */
public class GroupThePeople {

    public static void main(String[] args) {
        assert groupThePeople(new int[]{3, 3, 3, 3, 3, 1, 3}).equals(
                Arrays.asList(Arrays.asList(5), Arrays.asList(0, 1, 2), Arrays.asList(3, 4, 6)));
        assert groupThePeople(new int[]{2, 1, 3, 3, 3, 2}).equals(
                Arrays.asList(Arrays.asList(1), Arrays.asList(0, 5), Arrays.asList(2, 3, 4)));
    }

    public static List<List<Integer>> groupThePeople(int[] groupSizes) {
        Map<Integer, Queue<Integer>> map = new HashMap<>();
        for (int i = 0; i < groupSizes.length; i++) {
            map.putIfAbsent(groupSizes[i], new LinkedList<>());
            map.get(groupSizes[i]).offer(i);
        }
        List<List<Integer>> result = new ArrayList<>();
        for (Map.Entry<Integer, Queue<Integer>> entry : map.entrySet()) {
            List<Integer> tmp = new ArrayList<>();
            Queue<Integer> q = entry.getValue();
            while (!q.isEmpty()) {
                int size = entry.getKey();
                while (size-- > 0) {
                    tmp.add(q.poll());
                }
                result.add(new ArrayList<>(tmp));
                tmp.clear();
            }
        }
        return result;
    }

}
