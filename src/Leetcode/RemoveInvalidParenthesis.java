package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public class RemoveInvalidParenthesis {

    // Given a string s that contains parentheses and letters,
    // remove the minimum number of invalid parentheses to make the input string valid.
    public static void main(String[] args) {
        assert removeInvalidParentheses("()())()").containsAll(Arrays.asList("(())()", "()()()"));
        assert removeInvalidParentheses("(a)())()").containsAll(Arrays.asList("(a())()", "(a)()()"));
        assert removeInvalidParentheses(")(").contains("");
    }

    // O(n * 2^n)
    public static List<String> removeInvalidParentheses(String s) {
        List<String> res = new ArrayList<>();

        if (s == null) {
            return res;
        }

        Set<String> visited = new HashSet<>();
        Queue<String> queue = new LinkedList<>();

        queue.add(s);
        visited.add(s);

        boolean found = false;

        while (!queue.isEmpty()) {
            s = queue.poll();

            if (isValid(s)) {
                // found an answer, add to the result
                res.add(s);
                found = true;
            }

            // Do not process further as it'll give you only smaller strings.
            // Instead, process items in the queue to get strings of the same length.
            if (found) continue;

            // generate all possible states
            for (int i = 0; i < s.length(); i++) {
                // we only try to remove left or right paren
                if (s.charAt(i) != '(' && s.charAt(i) != ')') continue;

                String t = s.substring(0, i) + s.substring(i + 1);

                if (!visited.contains(t)) {
                    // for each state, if it's not visited, add it to the queue
                    queue.add(t);
                    visited.add(t);
                }
            }
        }

        return res;
    }

    private static boolean isValid(String str) {
        int count = 0;
        for (char c : str.toCharArray()) {
            if (c == '(') {
                count++;
            } else if (c == ')') {
                count--;
                if (count < 0) {
                    return false;
                }
            }
        }
        return count == 0;
    }

    // For case of - return any string after modification

    public String balanceParens(String str) {
        int n = str.length();
        StringBuilder sb = new StringBuilder(n);
        boolean[] remove = new boolean[n];
        int open = 0;
        for (int i = 0; i < n; i++) {
            if (str.charAt(i) == '(') {
                open++;
            } else if (str.charAt(i) == ')') {
                if (open > 0) {
                    open--;
                } else {
                    remove[i] = true;
                }
            }
        }

        int close = 0;
        for (int i = n - 1; i >= 0; i--) {
            if (str.charAt(i) == ')') {
                close++;
            } else if (str.charAt(i) == '(') {
                if (close > 0) {
                    close--;
                } else {
                    remove[i] = true;
                }
            }
        }

        for (int i = 0; i < n; i++) {
            if (!remove[i]) sb.append(str.charAt(i));
        }

        return sb.toString();
    }

}
