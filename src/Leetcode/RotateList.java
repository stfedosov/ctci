package Leetcode;

public class RotateList {

    /**
     * Definition for singly-linked list.
     * public class ListNode {
     * int val;
     * ListNode next;
     * ListNode(int x) { val = x; }
     * }
     */
    public static void main(String[] args) {
        ListNode listNode = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode3 = new ListNode(3);
        ListNode listNode4 = new ListNode(4);
        listNode4.next = new ListNode(5);
        listNode3.next = listNode4;
        listNode2.next = listNode3;
        listNode.next = listNode2;
        System.out.println(rotateRight(listNode, 2));
    }

    public static ListNode rotateRight(ListNode head, int k) {
        if (head == null || head.next == null || k == 0) return head;

        //make it a cricle, break from k postion far from the head
        ListNode index = head;
        int len = 1;// int len to record the length of list
        while (index.next != null) {
            index = index.next;
            len++;
        }
        index.next = head;
        for (int i = 0; i < len - k % len; i++) {
            index = index.next;
        }
        ListNode result = index.next;
        index.next = null;
        return result;
    }

    public static class ListNode {
        public int val;
        public ListNode next;

        public ListNode(int x) {
            val = x;
        }

        public ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public String toString() {
            return "ListNode{" +
                    "val=" + val +
                    ", next=" + next +
                    '}';
        }
    }

}
