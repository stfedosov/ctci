package Leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author sfedosov on 12/7/18.
 */
public class PathSum2 {


    /*
              5
             / \
            4   8
           /   / \
          11  13  4
         /  \    / \
        7    2  5   1

        sum = 22

        [
          [5,4,11,2],
          [5,8,4,5]
        ]
    */

    public List<List<Integer>> pathSum(TreeNode root, int sum) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> tmp = new ArrayList<>();
        pathSumRec(root, sum, result, tmp);
        return result;
    }

    private void pathSumRec(TreeNode root, int sum, List<List<Integer>> result, List<Integer> tmp) {
        if (root == null) return;
        tmp.add(root.val);
        if (root.left == null && root.right == null && root.val == sum) {
            result.add(new ArrayList<>(tmp));
        } else {
            pathSumRec(root.left, sum - root.val, result, tmp);
            pathSumRec(root.right, sum - root.val, result, tmp);
        }
        tmp.remove(tmp.size() - 1);
    }

}
