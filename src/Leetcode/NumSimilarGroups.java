package Leetcode;

import java.util.HashSet;
import java.util.Set;

public class NumSimilarGroups {

    // Two strings X and Y are similar if we can swap two letters (in different positions) of X, so that it equals Y.
    // Also two strings X and Y are similar if they are equal.
    // We are given a list strs of strings where every string in strs is an anagram of every other string in strs.
    // How many groups are there?
    public static void main(String[] args) {
        assert numSimilarGroups(new String[]{"tars", "rats", "arts", "star"}) == 2;
        assert numSimilarGroups(new String[]{"omv", "ovm"}) == 1;
    }

    // O(n * k), n stands for the length of the array, k stands for the avg. length of every string
    public static int numSimilarGroups(String[] strs) {
        Set<String> set = new HashSet<>();
        int count = 0;
        for (String s : strs) {
            if (!set.contains(s)) {
                dfs(s, set, strs);
                count++;
            }
        }
        return count;
    }

    private static void dfs(String s, Set<String> set, String[] strs) {
        set.add(s);
        for (String new_s : strs) {
            if (!set.contains(new_s) && diffIsTwo(s, new_s)) {
                dfs(new_s, set, strs);
            }
        }
    }

    private static boolean diffIsTwo(String s1, String s2) {
        int i = 0, j = 0;
        int diff = 0;
        while (i < s1.length()) {
            if (s1.charAt(i) != s2.charAt(j)) diff++;
            i++;
            j++;
        }
        return diff == 2 || diff == 0;
    }

}
