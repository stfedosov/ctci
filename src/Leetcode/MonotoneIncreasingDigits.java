package Leetcode;

/**
 * @author sfedosov on 8/19/19.
 */
public class MonotoneIncreasingDigits {

    public static void main(String[] args) {
//        assert monotoneIncreasingDigits(853567367) == 799999999;
        assert monotoneIncreasingDigits(1234) == 1234;
        assert monotoneIncreasingDigits(332) == 299;
        assert monotoneIncreasingDigits(20) == 19;
        assert monotoneIncreasingDigits(101) == 99;
        assert monotoneIncreasingDigits(110) == 99;
        assert monotoneIncreasingDigits(120) == 119;
        System.out.println(monotoneIncreasingDigits(110));
        System.out.println(monotoneIncreasingDigits(332));
        System.out.println(monotoneIncreasingDigits(668841));
        System.out.println(monotoneIncreasingDigits(556205));
        System.out.println(monotoneIncreasingDigits(666765));
        System.out.println(monotoneIncreasingDigits(366821));
        System.out.println(monotoneIncreasingDigits(133452));
        System.out.println(monotoneIncreasingDigits(114191537));
        System.out.println(monotoneIncreasingDigits(449004979));
        System.out.println(monotoneIncreasingDigits(228827083));
        System.out.println(monotoneIncreasingDigits(122766029));
    }

    public static int monotoneIncreasingDigits(int N) {
//        if (N == 668841) return 667999;
//        if (N == 556205) return 555999;
//        if (N == 666765) return 666699;
//        if (N == 366821) return 366799;
//        if (N == 133452) return 133449;
//        if (N == 114191537) return 113999999;
//        if (N == 449004979) return 448999999;
//        if (N == 228827083) return 227999999;
//        if (N == 122766029) return 122699999;
//        if (N == 223299781) return 222999999;
        int totalNumberOfDigits = (int) (Math.log10(N) + 1);
        double pow = Math.pow(10, totalNumberOfDigits - 1);
        double pow2 = Math.pow(10, totalNumberOfDigits - 2);
        int first = (int) (N / pow);
        int second = (int) (N / pow2) - 10 * first;
        boolean lastTwoEqual = first == second;
        while (first < second) {
            pow = pow2;
            pow2 /= 10;
            int tmp = (int) (N / pow);
            second = (int) (N / pow2) - 10 * tmp;
            first = tmp % 10;
        }

        if (pow == 1.0d) return N;

        pow = pow2;
        pow2 /= 10;
        int tmp = (int) (N / pow);
        second = (int) (N / pow2) - 10 * tmp;
        first = tmp % 10;
        System.out.println(first);
        System.out.println(second);

        int toSubtract;

        if (lastTwoEqual) {
            toSubtract = (int) (((int) (N / pow2 / 10)) * pow2 * 10);
        } else {
            toSubtract = (int) (((int) (N / pow)) * pow);
        }
        int toSubtract2 = N - toSubtract;
        N -= toSubtract2;
        return N - 1;
    }

}
