package Leetcode;

public class SplitArray {

    /*
    Given an array nums which consists of non-negative integers and an integer m,
    you can split the array into m non-empty continuous subarrays.

    Write an algorithm to minimize the largest sum among these m subarrays.
     */
    public static int splitArray(int[] nums, int m) {
        int maxNum = 0;
        long totalSum = 0;
        for (int num : nums) {
            maxNum = Math.max(num, maxNum);
            totalSum += num;
        }
        if (m == 1) return (int) totalSum;
        // The answer is between maximum value of input array numbers and total sum of those numbers
        long start = maxNum, end = totalSum;
        while (start <= end) {
            long mid = (start + end) / 2;
            // if the number of subarrays is not larger than m, mid could be smaller
            if (notLargerThanM(mid, nums, m)) {
                end = mid - 1;
            } else {
                // if the number of subarrays is larger than m, mid should be larger.
                start = mid + 1;
            }
        }
        return (int) start;
    }

    // Time complexity: O(Length of the array N * log(sum of integers in the array))
    // To check if we can divide nums into m subarrays where sum of each subarray <= largest sum
    public static boolean notLargerThanM(long midValue, int[] nums, int m) {
        int count = 1;
        long totalSum = 0;
        for (int num : nums) {
            totalSum += num;
            if (totalSum > midValue) {
                totalSum = num;
                count++;
                if (count > m) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        assert splitArray(new int[]{7, 2, 5, 10, 8}, 3) == 18;
    }

}
