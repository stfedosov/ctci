package Leetcode;

/**
 * @author sfedosov on 7/24/19.
 */
public class FirstBadVersion {

    public static void main(String[] args) {
        FirstBadVersion firstBadVersion = new FirstBadVersion();
        assert firstBadVersion.firstBadVersion(5) == 4;
    }

    public int firstBadVersion(int n) {
        int low = 1, high = n;
        while (low <= high) {                     //exit loop when low > high
            int mid = low + (high - low) / 2;
            if (isBadVersion(mid)) {
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }
        return low;
    }

    // n = 5, bad = 4
    private boolean isBadVersion(int version) {
        return version != 3;
    }

}
