package Leetcode;

public class ValidateBST {

    public static void main(String[] args) {
        TreeNode treeNode = new TreeNode(0);
        TreeNode left1 = new TreeNode(-1);
        treeNode.left = left1;
        left1.left = new TreeNode(-2);
        left1.right = new TreeNode(-3);
        treeNode.right = new TreeNode(1);
        System.out.println(isValidBST(treeNode));
    }

    public static boolean isValidBST(TreeNode root) {
        return isValidBST(root, Long.MIN_VALUE, Long.MAX_VALUE);
    }

    public static boolean isValidBST(TreeNode root, long minVal, long maxVal) {
        if (root == null) return true;
        if (root.val >= maxVal || root.val <= minVal) return false;
        return isValidBST(root.left, minVal, root.val) && isValidBST(root.right, root.val, maxVal);
    }

}
