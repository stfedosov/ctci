package Leetcode;

/**
 * @author sfedosov on 4/5/20.
 */
public class NumSteps {

    public static void main(String[] args) {
        /*
        Given a number s in their binary representation.
        Return the number of steps to reduce it to 1 under the following rules:

        If the current number is even, you have to divide it by 2.
        If the current number is odd, you have to add 1 to it.
         */
        assert numSteps("1101") == 6;
        assert numSteps("10") == 1;
        assert numSteps("1") == 0;
        assert numSteps("1111110011101010110011100100101110010100101110111010111110110010") == 89;
    }

    public static int numSteps(String s) {
        int steps = 0;
        while (!isOne(s)) {
            if (isEven(s)) s = s.substring(0, s.length() - 1);
            else s = addOne(s);
            steps++;
        }
        return steps;
    }

    private static String addOne(String s) {
        StringBuilder sb = new StringBuilder();
        int carry = 1, i = s.length() - 1;
        while (carry != 0 && i >= 0) {
            int sum = (s.charAt(i) - '0') + carry;
            carry = sum / 2;
            sum %= 2;
            sb.insert(0, sum);
            i--;
        }
        if (carry != 0) sb.insert(0, carry);
        else sb.insert(0, s.substring(0, i + 1));
        return sb.toString();
    }

    private static boolean isOne(String s) {
        for (int i = 0; i < s.length() - 1; i++) if (s.charAt(i) != '0') return false;
        return s.charAt(s.length() - 1) == '1';
    }

    private static boolean isEven(String s) {
        return s.charAt(s.length() - 1) == '0';
    }
}
