package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 1/2/19.
 */
public class GraphColoring {

    public static void main(String[] args) {
        //    0 ------- 4
        //    |        /|
        //    |       / |
        //    |      /  |
        //    |     /   |
        //    |    3    |
        //    |   /     |
        //    |  /      |
        //    | /       |
        //    1 -------- 2
        // 0 - Red, 1 - White, 2 - Red, 3 - Red, 4 - White
        // adjacency matrix:
        //
        //   |0 1 2 3 4
        // ------------
        //  0|1 1 0 0 1
        //  1|1 1 1 1 0
        //  2|0 1 1 0 1
        //  3|0 1 0 1 1
        //  4|1 0 1 1 1
        graphColor();
    }

    private static Colors[] graphColor() {
        int[][] G = new int[][]{
                {1, 1, 0, 0, 1},
                {1, 1, 1, 1, 0},
                {0, 1, 1, 0, 1},
                {0, 1, 0, 1, 1},
                {1, 0, 1, 1, 1}
        };
        int numOfNodes = 5;
        Colors[] result = new Colors[5];
        graphColor(G, Colors.values(), numOfNodes, 0, result);
        return result;
    }

    enum Colors {
        Red, White, Green, Blue
    }

    private static void graphColor(int[][] G, Colors[] values, int numOfNodes, int k, Colors[] result) {
        for (Colors color : values) {
            if (isSafe(k, G, color, result)) {
                result[k] = color;
                if (k + 1 < numOfNodes) {
                    graphColor(G, values, numOfNodes, k + 1, result);
                } else {
                    System.out.println(Arrays.toString(result));
                    return;
                }
            }
        }
    }

    private static boolean isSafe(int k, int[][] G, Colors color, Colors[] result) {
        for (int i = 0; i < G[k].length; i++) {
            if (G[k][i] == 1 && result[i] == color) {
                return false;
            }
        }
        return true;
    }


}
