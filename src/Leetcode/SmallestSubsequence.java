package Leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

/**
 * @author sfedosov on 3/15/20.
 */
public class SmallestSubsequence {

    public static void main(String[] args) {
        assert smallestSubsequence("cdadabcc").equals("adbc");
        assert smallestSubsequence("abcd").equals("abcd");
        assert smallestSubsequence("ecbacba").equals("eacb");
        assert smallestSubsequence("leetcode").equals("letcod");
    }

    public static String smallestSubsequence(String text) {
        Map<Character, Integer> map = new HashMap<>();
        for (char c : text.toCharArray()) map.put(c, map.getOrDefault(c, 0) + 1);
        Stack<Character> stack = new Stack<>();
        Set<Character> seen = new HashSet<>();
        for (char c : text.toCharArray()) {
            if (seen.contains(c)) {
                map.put(c, map.get(c) - 1);
                continue;
            }
            while (!stack.isEmpty() && stack.peek() > c && map.get(stack.peek()) > 1) {
                char tmp = stack.pop();
                map.put(tmp, map.get(tmp) - 1);
                seen.remove(tmp);
            }
            stack.push(c);
            seen.add(c);
        }
        StringBuilder sb = new StringBuilder();
        while (!stack.isEmpty()) sb.append(stack.pop());
        return sb.reverse().toString();
    }
}
