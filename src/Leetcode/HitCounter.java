package Leetcode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author sfedosov on 6/9/19.
 */
public class HitCounter {

    private Queue<Integer> queue;

    /**
     * Initialize your data structure here.
     */
    public HitCounter() {
        queue = new LinkedList<>();
    }

    /**
     * Record a hit.
     *
     * @param timestamp - The current timestamp (in seconds granularity).
     */
    public void hit(int timestamp) {
        queue.add(timestamp);
    }

    /**
     * Return the number of hits in the past 5 minutes.
     *
     * @param timestamp - The current timestamp (in seconds granularity).
     */
    public int getHits(int timestamp) {
        expire(timestamp);
        return queue.size();
    }

    private void expire(int timestamp) {
        while (!queue.isEmpty() && timestamp - queue.peek() >= 300) {
            queue.poll();
        }
    }

    public static void main(String[] args) {
        testCase1();
        testCase2();
    }

    private static void testCase1() {
        HitCounter obj = new HitCounter();
        obj.hit(1);
        obj.hit(1);
        obj.hit(1);
        obj.hit(300);
        assert obj.getHits(300) == 4;
        obj.hit(300);
        assert obj.getHits(300) == 5;
        obj.hit(301);
        assert obj.getHits(301) == 3;
    }

    private static void testCase2() {
        HitCounter obj = new HitCounter();
        obj.hit(1);
        obj.hit(2);
        obj.hit(3);
        assert obj.getHits(4) == 3;
        obj.hit(300);
        assert obj.getHits(300) == 4;
        assert obj.getHits(301) == 3;
    }
}