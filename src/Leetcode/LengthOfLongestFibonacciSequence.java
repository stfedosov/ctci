package Leetcode;

import java.util.HashSet;
import java.util.Set;

/**
 * @author sfedosov on 4/19/19.
 */
public class LengthOfLongestFibonacciSequence {

    public static void main(String[] args) {
        lenLongestFibSubseq(new int[]{1, 3, 7, 11, 12, 14, 18});
    }

    public static int lenLongestFibSubseq(int[] A) {
        int N = A.length;
        Set<Integer> S = new HashSet<>();
        for (int x : A) S.add(x);

        int ans = 0;
        for (int i = 0; i < N; ++i)
            for (int j = i + 1; j < N; ++j) {
                int x = A[j], y = A[i] + A[j];
                int length = 2;
                while (S.contains(y)) {
                    int tmp = y;
                    y += x;
                    x = tmp;
                    ans = Math.max(ans, ++length);
                }
            }

        return ans;
    }

}
