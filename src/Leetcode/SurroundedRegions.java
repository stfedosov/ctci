package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 7/20/19.
 */
public class SurroundedRegions {

    public static void main(String[] args) {
        char[][] board = {
                {'O', 'O'},
                {'O', 'O'}};
        System.out.println(Arrays.deepToString(board));
        solve(board);
        System.out.println(Arrays.deepToString(board));

        board = new char[][]{
                {'X', 'X', 'X', 'X'},
                {'X', 'O', 'O', 'X'},
                {'X', 'X', 'O', 'X'},
                {'X', 'O', 'X', 'X'}};
        System.out.println(Arrays.deepToString(board));
        solve(board);
        System.out.println(Arrays.deepToString(board));
    }

    public static void solve(char[][] board) {
        int rows = board.length;
        if (rows == 0) return;
        int colons = board[0].length;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < colons; j++) {
                if (i == 0 || j == colons - 1 || i == rows - 1 || j == 0) {
                    markBorderOsAsOnes(board, i, j, rows, colons);
                }
            }
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < colons; j++) {
                if (board[i][j] == 'O') {
                    board[i][j] = 'X';
                }
                if (board[i][j] == '1') {
                    board[i][j] = 'O';
                }
            }
        }
    }

    private static void markBorderOsAsOnes(char[][] board, int i, int j, int rows, int colons) {
        if (i < 0 || j < 0 || i >= rows || j >= colons || board[i][j] != 'O') {
            return;
        }
        board[i][j] = '1';
        markBorderOsAsOnes(board, i - 1, j, rows, colons);
        markBorderOsAsOnes(board, i, j - 1, rows, colons);
        markBorderOsAsOnes(board, i + 1, j, rows, colons);
        markBorderOsAsOnes(board, i, j + 1, rows, colons);
    }

}
