package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AccountsMerge {

    public static void main(String[] args) {
        List<List<String>> accounts = Arrays.asList(
                Arrays.asList("David", "David0@m.co", "David1@m.co"),
                Arrays.asList("David", "David3@m.co", "David4@m.co"),
                Arrays.asList("David", "David4@m.co", "David5@m.co"),
                Arrays.asList("David", "David2@m.co", "David3@m.co"),
                Arrays.asList("David", "David1@m.co", "David2@m.co"));
        System.out.println(accountsMerge(accounts));

        accounts = Arrays.asList(
                Arrays.asList("John", "johnsmith@mail.com", "john_newyork@mail.com"),
                Arrays.asList("John", "johnsmith@mail.com", "john00@mail.com"),
                Arrays.asList("Mary", "mary@mail.com"),
                Arrays.asList("John", "johnnybravo@mail.com")
        );
        System.out.println(accountsMerge(accounts));
    }

    /*
    N is the number of accounts and K is the max length of an account

    Time complexity: O(N*K*log(NK)), Space: O(N*K)

    In the worst case, all the emails will end up belonging to a single person.
    The total number of emails will be N*K, and we need to sort these emails.
    DFS traversal will take N*K operations as no email will be traversed more than once.
     */
    public static List<List<String>> accountsMerge(List<List<String>> accounts) {
        Map<String, List<String>> graph = buildGraph(accounts);
        List<List<String>> result = new ArrayList<>();
        Set<String> seen = new HashSet<>();
        for (List<String> acc : accounts) {
            if (seen.contains(acc.get(1))) continue;
            List<String> emails = new ArrayList<>();
            dfs(acc.get(1), emails, seen, graph);
            Collections.sort(emails);
            emails.add(0, acc.get(0));
            result.add(emails);
        }
        return result;
    }

    private static Map<String, List<String>> buildGraph(List<List<String>> accounts) {
        Map<String, List<String>> graph = new HashMap<>();
        for (List<String> acc : accounts) {
            if (acc.size() == 2) {
                graph.computeIfAbsent(acc.get(1), x -> new ArrayList<>()).add(acc.get(1));
            } else {
                for (int i = 1; i + 1 < acc.size(); i++) {
                    graph.computeIfAbsent(acc.get(i), x -> new ArrayList<>()).add(acc.get(i + 1));
                    graph.computeIfAbsent(acc.get(i + 1), x -> new ArrayList<>()).add(acc.get(i));
                }
            }
        }
        return graph;
    }

    private static void dfs(String start, List<String> emails, Set<String> seen, Map<String, List<String>> graph) {
        seen.add(start);
        emails.add(start);
        for (String edge : graph.get(start)) {
            if (!seen.contains(edge))
                dfs(edge, emails, seen, graph);
        }
    }

}
