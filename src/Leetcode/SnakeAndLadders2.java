package Leetcode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author sfedosov on 9/29/19.
 */
public class SnakeAndLadders2 {

    public static void main(String[] args) {
        System.out.println(snakesAndLadders(new int[][]{
                {-1, -1, -1, -1, -1, -1},
                {-1, -1, -1, -1, -1, -1},
                {-1, -1, -1, -1, -1, -1},
                {-1, 35, -1, -1, 13, -1},
                {-1, -1, -1, -1, -1, -1},
                {-1, 15, -1, -1, -1, -1}
        }));

        System.out.println(snakesAndLadders(new int[][]{
                {-1, -1},
                {-1, 3}
        }));
    }

    public static int snakesAndLadders(int[][] board) {
        int[] array = new int[board.length * board.length];
        int i = 0;
        boolean leftToRight = true;
        for (int row = board.length - 1; row >= 0; row--) {
            if (leftToRight) {
                for (int column = 0; column < board[row].length; column++) {
                    array[i++] = board[row][column];
                }
            } else {
                for (int column = board[row].length - 1; column >= 0; column--) {
                    array[i++] = board[row][column];
                }
            }
            leftToRight = !leftToRight;
        }
        Queue<Integer> q = new LinkedList<>();
        q.offer(0);
        int steps = 0;
        boolean[] visited = new boolean[array.length];
        visited[0] = true;
        while (!q.isEmpty()) {
            int size = q.size();
            while (size-- > 0) {
                int current = q.poll();
                if (current >= array.length - 1) return steps;
                for (int next = current + 1; next <= current + 6 && next < array.length; next++) {
                    if (!visited[next]) {
                        visited[next] = true;
                        int position = array[next] == -1 ? next : array[next] - 1;
                        q.offer(position);
                    }
                }
            }
            steps++;
        }
        return -1;
    }

}
