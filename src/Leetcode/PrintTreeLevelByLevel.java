package Leetcode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author sfedosov on 3/11/18.
 */
public class PrintTreeLevelByLevel {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(2);
        TreeNode node3 = new TreeNode(3);
        node3.left = new TreeNode(1);
        root.left = node3;
        TreeNode node4 = new TreeNode(4);
        root.right = node4;
        node4.left = new TreeNode(2);
        node4.right = new TreeNode(5);
        printInLevelOrder(root);
        printInLevelOrder2(root);
        //     2
        //    / \
        //   3   4
        //  /   / \
        // 1   2   5
        // ---------------
        // 2
        // 34
        // 125
    }

    // 1st solution, with temp queue, not efficient
    private static void printInLevelOrder(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        findItRecursively(queue);
    }

    private static void findItRecursively(Queue<TreeNode> queue) {
        if (queue.isEmpty()) {
            return;
        }
        Queue<TreeNode> temp = new LinkedList<>();
        while (!queue.isEmpty()) {
            TreeNode treeNode = queue.poll();
            System.out.print(treeNode.val);
            temp.add(treeNode);
        }
        System.out.println();
        while (!temp.isEmpty()) {
            TreeNode node = temp.poll();
            if (node != null) {
                if (node.left != null) {
                    queue.add(node.left);
                }
                if (node.right != null) {
                    queue.add(node.right);
                }
            }
        }
        findItRecursively(queue);
    }

    // 2nd solution, more efficient
    private static void printInLevelOrder2(TreeNode root) {
        // Base Case
        if (root == null)
            return;

        // Create an empty queue for level order traversal
        Queue<TreeNode> q = new LinkedList<>();

        // Enqueue Root and initialize height
        q.add(root);


        while (!q.isEmpty()) {

            // nodeCount (queue size) indicates number of nodes
            // at current level.
            int nodeCount = q.size();

            // Dequeue all nodes of current level and Enqueue all
            // nodes of next level
            for (int i = 0; i < nodeCount; i++) {
                TreeNode node = q.poll();
                System.out.print(node.val);
                if (node.left != null) {
                    q.add(node.left);
                }
                if (node.right != null) {
                    q.add(node.right);
                }
            }
            System.out.println();
        }
    }


}
