package Leetcode;

/**
 * @author sfedosov on 2/11/18.
 */
public class MinimumDistanceBST {

    public static void main(String[] args) {
        // test case 1
        TreeNode head = new TreeNode(4);
        TreeNode left2 = new TreeNode(2);
        head.left = left2;
        TreeNode right6 = new TreeNode(6);
        head.right = right6;
        left2.left = new TreeNode(1);
        left2.right = new TreeNode(3);
        assert minDiffInBST(head) == 1;

        // test case 2
        TreeNode head2 = new TreeNode(1);
        head2.left = new TreeNode(0);
        TreeNode treeNode48 = new TreeNode(48);
        treeNode48.left = new TreeNode(12);
        treeNode48.right = new TreeNode(49);
        head2.right = treeNode48;
        assert minDiffInBST(head2) == 1;

        // test case 3
        TreeNode head3 = new TreeNode(27);
        TreeNode treeNode34 = new TreeNode(34);
        head3.right = treeNode34;
        TreeNode treeNode58 = new TreeNode(58);
        treeNode34.right = treeNode58;
        TreeNode treeNode50 = new TreeNode(50);
        treeNode58.left = treeNode50;
        treeNode50.left = new TreeNode(44);
        assert minDiffInBST(head3) == 6;

        //test case 4
        TreeNode head4 = new TreeNode(90);
        TreeNode treeNode69 = new TreeNode(69);
        head4.left = treeNode69;
        TreeNode treeNode49 = new TreeNode(49);
        treeNode69.left = treeNode49;
        treeNode69.right = new TreeNode(89);
        treeNode49.right = new TreeNode(52);
        assert minDiffInBST(head4) == 1;
    }

    static int minDiff = Integer.MAX_VALUE;
    static TreeNode prev;

    public static int minDiffInBST(TreeNode root) {
        inorder(root);
        return minDiff;
    }

    public static void inorder(TreeNode root) {
        if (root == null) return;
        inorder(root.left);
        if (prev != null) minDiff = Math.min(minDiff, root.val - prev.val);
        prev = root;
        inorder(root.right);
    }

}
