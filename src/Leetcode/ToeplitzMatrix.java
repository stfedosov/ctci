package Leetcode;

public class ToeplitzMatrix {

    public static void main(String[] args) {
        assert isToeplitzMatrix(new int[][]{
                {1, 2, 3, 4},
                {5, 1, 2, 3},
                {9, 5, 1, 2}
        });
        assert !isToeplitzMatrix(new int[][]{
                {1, 2},
                {2, 2}
        });
    }

    public static boolean isToeplitzMatrix(int[][] matrix) {
        for (int i = 1; i < matrix.length; i++)
            for (int j = 1; j < matrix[0].length; j++) {
                if (matrix[i - 1][j - 1] != matrix[i][j])
                    return false;
            }

        return true;
    }

    public static boolean isToeplitzMatrix2(int[][] matrix) {
        int rowLength = matrix.length, colLength = matrix[0].length;
        for (int row = rowLength - 2; row >= 0; row--) {
            int tmpRow = row, tmpCol = 0, seen = matrix[row][tmpCol];
            while (tmpRow < rowLength && tmpCol < colLength) {
                if (seen != matrix[tmpRow][tmpCol]) {
                    return false;
                }
                tmpRow++;
                tmpCol++;
            }
        }
        for (int col = 1; col < colLength; col++) {
            int tmpRow = 0, tmpCol = col, seen = matrix[tmpRow][col];
            while (tmpCol < colLength && tmpRow < rowLength) {
                if (seen != matrix[tmpRow][tmpCol]) {
                    return false;
                }
                tmpRow++;
                tmpCol++;
            }
        }
        return true;
    }

    /*
    ------------
    Follow ups:
    ------------
    Question: What if the matrix is stored on disk, and the memory is limited
    such that you can only load at most one row of the matrix into the memory at once?

    Answer: load one row, drop its last element and load the second element of the next row, compare them.
    If good, drop both and keep reading the next 2 element of next row. compare 2 pairs of them, if good, remove 4 of them and load the next 4 items.

    ------------
    Question: What if the matrix is so large that you can only load up a partial row into the memory at once?

    Answer: when memory is the limit, using more computing power to save the situation.
    As long as you can at least load two items in a time, you can make it work.

    Try to resolve the mapping pairs and load them as many as possible into the memory, do the compare, if good,
    drop all of them and move the cursor forward (recalculate the mapping pair may be required).

    Think out of the box, could you simply not load the original data?
    Instead, calculate a representation? a hash code? for each of the element? for a bunch of elements? or even for a single row? a blocks? a matrix overall?
    Then we compare its representation rather than its original data. Of course, the representation method or hash function requires detailed design
     */

}
