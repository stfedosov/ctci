package Leetcode;

import Leetcode.ReorderList.ListNode;

import java.util.ArrayDeque;
import java.util.Deque;

// Linked lists are different sizes but stored direct order
public class AddTwoNumbers2 {

    public static void main(String[] args) {
        ListNode root = new ListNode(7);
        ListNode listNode2 = new ListNode(2);
        root.next = listNode2;
        ListNode listNode4 = new ListNode(4);
        listNode2.next = listNode4;
        listNode4.next = new ListNode(3);

        ListNode root2 = new ListNode(5);
        ListNode listNode6 = new ListNode(6);
        root2.next = listNode6;
        listNode6.next = new ListNode(4);

//        System.out.println(addTwoNumbers(root, root2));
        System.out.println(addTwoNumbersStack(root, root2));

        ListNode root3 = new ListNode(1);

        ListNode root4 = new ListNode(9);
        root4.next = new ListNode(9);

        System.out.println(addTwoNumbers(root3, root4));

        ListNode root5 = new ListNode(5);
        ListNode root6 = new ListNode(5);

        System.out.println(addTwoNumbers(root5, root6));

        ListNode root7 = new ListNode(8);
        ListNode listNode9 = new ListNode(9);
        root7.next = listNode9;
        listNode9.next = new ListNode(9);

        ListNode root8 = new ListNode(2);

        System.out.println(addTwoNumbers(root7, root8));
    }

    // ----- O(1) space
    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        // reverse lists
        l1 = reverse(l1);
        l2 = reverse(l2);

        ListNode head = null;
        int carry = 0;
        while (l1 != null || l2 != null) {
            int x1 = l1 != null ? l1.val : 0;
            int x2 = l2 != null ? l2.val : 0;

            int val = (carry + x1 + x2) % 10;
            carry = (carry + x1 + x2) / 10;

            // update the result: add to front
            ListNode curr = new ListNode(val);
            curr.next = head;
            head = curr;

            l1 = l1 != null ? l1.next : null;
            l2 = l2 != null ? l2.next : null;
        }

        if (carry != 0) {
            ListNode curr = new ListNode(carry);
            curr.next = head;
            head = curr;
        }

        return head;
    }

    private static ListNode reverse(ListNode node) {
        ListNode prev = null, current = node;
        while (current != null) {
            ListNode next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        return prev;
    }

    // ----- O(n) space with Stack

    public static ListNode addTwoNumbersStack(ListNode L1, ListNode L2) {
        Deque<Integer> stack1 = new ArrayDeque<>(), stack2 = new ArrayDeque<>();

        ListNode current = L1;
        while (current != null) {
            stack1.push(current.val);
            current = current.next;
        }

        ListNode current2 = L2;
        while (current2 != null) {
            stack2.push(current2.val);
            current2 = current2.next;
        }

        ListNode head = null;
        int carry = 0;

        while (!stack1.isEmpty() || !stack2.isEmpty() || carry > 0) {
            int sum = carry + (!stack1.isEmpty() ? stack1.pop() : 0) + (!stack2.isEmpty() ? stack2.pop() : 0);
            carry = sum / 10;

            // Insert NodeCopy at front
            ListNode newNode = new ListNode(sum % 10);
            newNode.next = head;
            head = newNode;
        }
        return head;
    }

}
