package Leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 5/17/19.
 */
public class SubArraySumEqualsK {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(subarraySumIndexes(0, new int[]{5, -3, -4, 1, 6, -2, -5})));
        System.out.println(Arrays.toString(subarraySumIndexes(3, new int[]{1, 2, 3})));
        System.out.println(Arrays.toString(subarraySumIndexes(2, new int[]{1, 1, 1})));
    }

    public static int subarraySum(int[] nums, int k) {
        int[] cumulativeSum = new int[nums.length + 1];
        int count = 0;
        for (int i = 1; i <= nums.length; i++) {
            cumulativeSum[i] = cumulativeSum[i - 1] + nums[i - 1];
        }
        for (int start = 0; start < nums.length; start++) {
            for (int end = start + 1; end <= nums.length; end++) {
                if (cumulativeSum[end] - cumulativeSum[start] == k) {
                    count++;
                }
            }
        }
        return count;
    }

    public static int subarraySum2(int[] nums, int k) {
        int count = 0, sum = 0;
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0, 1);
        for (int num : nums) {
            sum += num;
            count += map.getOrDefault(sum - k, 0);
            map.put(sum, map.getOrDefault(sum, 0) + 1);
        }
        return count;
    }

    public static int[] subarraySumIndexes(int x, int[] array) {
        Map<Integer, Integer> map = new HashMap<>();
        int sum = 0, i = 0;
        for (int a : array) {
            sum += a;
            if (map.containsKey(sum - x)) {
                return new int[]{map.get(sum - x), i};
            }
            if (sum - x == 0) return new int[]{0, i};
            map.put(sum, i);
            i++;
        }
        return new int[]{-1, -1};
    }

    /**
     * MergeIntervals 2: prefix sum with hashmap
     * The hashmap stores the sum of all elements before index i as key, and i as value
     * Time complexity: O(n)
     * Space complexity: O(n)
     */
    public static int maxSubArrayLenII(int[] nums, int k) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int sum = 0, max = 0;
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            sum = sum + nums[i];
            if (sum == k) {
                max = i + 1;
            } else if (map.containsKey(sum - k)) {
                max = Math.max(max, i - map.get(sum - k));
            }
            map.putIfAbsent(sum, i);
        }
        return max;
    }

}
