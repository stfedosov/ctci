package Leetcode;


import java.util.LinkedList;
import java.util.Queue;

/**
 * @author sfedosov on 1/23/19.
 */
public class FlattenBinaryTreeToLinkedList {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode treeNode2 = new TreeNode(2);
        root.left = treeNode2;
        treeNode2.left = new TreeNode(3);
        treeNode2.right = new TreeNode(4);
        TreeNode treeNode5 = new TreeNode(5);
        treeNode5.right = new TreeNode(6);
        root.right = treeNode5;
        System.out.println(root);
        flatten(root);
        System.out.println(root);
    }

    public static void flatten(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<>();
        flattenHelper(root, queue);
        TreeNode current = root;
        queue.poll();
        while (!queue.isEmpty()) {
            current.left = null;
            current.right = queue.poll();
            current = current.right;
        }
    }

    private static void flattenHelper(TreeNode root, Queue<TreeNode> queue) {
        if (root == null) return;
        queue.offer(new TreeNode(root.val));
        flattenHelper(root.left, queue);
        flattenHelper(root.right, queue);
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }

        @Override
        public String toString() {
            return "TreeNode{" +
                    "val=" + val +
                    ", left=" + left +
                    ", right=" + right +
                    '}';
        }
    }

}
