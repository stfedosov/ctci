package Leetcode;

/**
 * @author sfedosov on 3/6/19.
 */
public class CheckIfWordValidAfterSubstitutions {

    public static void main(String[] args) {
        isValid("aabcbc");
        isValid("abcabcababcc");
    }

    public static boolean isValid(String S) {
        String abc = "abc";
        while(S.contains(abc)) {
            S = S.replace(abc, "");
        }
        return S.isEmpty();
    }

}
