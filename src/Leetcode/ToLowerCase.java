package Leetcode;

/**
 * @author sfedosov on 12/30/19.
 */
public class ToLowerCase {

    public static void main(String[] args) {
        assert toLowerCase("LoVeLy").equals("lovely");
        assert toLowerCase("Hello").equals("hello");
    }

    public static String toLowerCase(String str) {
        StringBuilder sb = new StringBuilder();
        for (char c : str.toCharArray()) {
            if (c >= 65 && c <= 90) {
                sb.append((char) (c + 32));
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

}
