package Leetcode;

import java.util.stream.IntStream;

/**
 * @author sfedosov on 1/3/19.
 */
public class PivotIndex {

    public static void main(String[] args) {
        System.out.println(pivotIndex(new int[]{-1, -1, -1, -1, -1, -1}));
        System.out.println(pivotIndex(new int[]{-1, 0, 0, 0, 0, 1}));
    }

    public static int pivotIndex(int[] nums) {
        int index = -1;
        if (nums.length == 0) return -1;
        int left = 0;
        int right = IntStream.of(nums).sum();
        for (int i = 0; i < nums.length; i++) {
            right -= nums[i];
            if (left == right) {
                index = i;
                break;
            }
            left += nums[i];
        }
        return index;
    }

}
