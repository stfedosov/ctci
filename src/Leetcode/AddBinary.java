package Leetcode;

/**
 * @author sfedosov on 11/28/18.
 */
public class AddBinary {

    public static void main(String[] args) {
        assert addBinary2("100", "110010").equals("110110");
        assert addBinary2("1010", "1011").equals("10101");
        assert addBinary2("111", "111").equals("1110");
        assert addBinary2("11", "11").equals("110");
        assert addBinary2("0", "0").equals("0");
        assert addBinary2("11", "1").equals("100");
        assert addBinary2("1", "111").equals("1000");
    }

    private static String addBinary2(String a, String b) {
        StringBuilder sb = new StringBuilder();
        int i = a.length() - 1, j = b.length() - 1, carry = 0;
        while (i >= 0 || j >= 0) {
            int sum = (i >= 0 ? (a.charAt(i) - '0') : 0) + (j >= 0 ? (b.charAt(j) - '0') : 0) + carry;
            carry = sum / 2;
            sum %= 2;
            sb.insert(0, sum);
            i--;
            j--;
        }
        if (carry != 0) {
            sb.insert(0, carry);
        }
        return sb.toString();
    }

}
