package Leetcode;

import java.util.Arrays;
import java.util.Stack;

/**
 * @author sfedosov on 3/30/20.
 */
public class AsteroidCollision {

    public static void main(String[] args) {
        assert Arrays.equals(asteroidCollision(new int[]{-2, -2, 2, -2}), new int[]{-2, -2});
        assert Arrays.equals(asteroidCollision(new int[]{-2, 1, -2, -1}), new int[]{-2, -2, -1});
        assert Arrays.equals(asteroidCollision(new int[]{-2, 1, 1, -1}), new int[]{-2, 1});
        assert Arrays.equals(asteroidCollision(new int[]{-2, 2, 1, -2}), new int[]{-2});
        assert Arrays.equals(asteroidCollision(new int[]{-2, -1, 1, 2}), new int[]{-2, -1, 1, 2});
        assert Arrays.equals(asteroidCollision(new int[]{-2, 1, 1, -2}), new int[]{-2, -2});
        assert Arrays.equals(asteroidCollision(new int[]{5, 10, -5}), new int[]{5, 10});
        assert Arrays.equals(asteroidCollision(new int[]{-2, -2, 1, -2}), new int[]{-2, -2, -2});
        assert Arrays.equals(asteroidCollision(new int[]{2, -2}), new int[]{});
        assert Arrays.equals(asteroidCollision(new int[]{10, 2, -5}), new int[]{10});
    }

    public static int[] asteroidCollision(int[] asteroids) {
        Stack<Integer> s = new Stack<>();
        int toAdd = Integer.MAX_VALUE;
        for (int x : asteroids) {
            if (x < 0 && !s.isEmpty() && s.peek() > 0) {
                while (!s.isEmpty() && s.peek() > 0) {
                    if (s.peek() == -x) {
                        s.pop();
                        toAdd = Integer.MAX_VALUE;
                        break;
                    } else if (s.peek() < -x) {
                        s.pop();
                        toAdd = x;
                    } else {
                        toAdd = Integer.MAX_VALUE;
                        break;
                    }
                }
                if (toAdd != Integer.MAX_VALUE) s.push(toAdd);
                toAdd = Integer.MAX_VALUE;
            } else {
                s.push(x);
            }
        }
        int i = s.size();
        int[] result = new int[s.size()];
        while (!s.isEmpty()) {
            result[--i] = s.pop();
        }
        return result;
    }

}
