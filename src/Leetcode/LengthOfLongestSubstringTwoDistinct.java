package Leetcode;

import java.util.HashMap;
import java.util.Map;

public class LengthOfLongestSubstringTwoDistinct {

    public static void main(String[] args) {
        assert lengthOfLongestSubstringTwoDistinct("eceba") == 3;
        assert lengthOfLongestSubstringTwoDistinct("ccaabbb") == 5;
        assert lengthOfLongestSubstringTwoDistinct("abc") == 2;
        assert lengthOfLongestSubstringTwoDistinct("abaccc") == 4;
    }

    public static int lengthOfLongestSubstringTwoDistinct(String s) {
        if (s.length() <= 2) return s.length();
        int start = 0, end = 0, max = 0;
        Map<Character, Integer> map = new HashMap<>();
        while (end < s.length()) {
            char new_c = s.charAt(end);
            map.put(new_c, map.getOrDefault(new_c, 0) + 1);
            if (map.size() <= 2) {
                max = Math.max(max, end - start + 1);
            }
            if (map.size() > 2) {
                char start_char = s.charAt(start);
                int res = map.get(start_char);
                map.put(start_char, res - 1);
                if (map.get(start_char) == 0) map.remove(start_char);
                start++;
            }
            end++;
        }
        return max;
    }

}
