package Leetcode;

public class DeleteAndEarn {

    /*
    You are given an integer array nums.
    You want to maximize the number of points you get by performing the following operation
    any number of times:
     - Pick any nums[i] and delete it to earn nums[i] points.
     - Afterwards, you must delete every element equal to nums[i] - 1 and every element equal to nums[i] + 1.

    Return the maximum number of points you can earn by applying the above operation some number of times.
     */

    public static void main(String[] args) {
        deleteAndEarn(new int[]{3, 4, 2}); // 6
        deleteAndEarn(new int[]{2, 2, 3, 3, 3, 4}); // 9
    }

    // Recurrence Relation
    // f(i) = max(f(i-2) + money(i), f(i-1))
    public static int deleteAndEarn(int[] nums) {
        int n = 500;
        int[] buckets = new int[n];

        for (int num : nums) {
            buckets[num] += num;
        }
        int prev_1 = buckets[1];
        int prev_2 = buckets[0];
        int curr = Math.max(prev_2, prev_1);
        for (int i = 2; i < n; i++) {
            curr = Math.max(prev_2 + buckets[i], prev_1);
            prev_2 = prev_1;
            prev_1 = curr;
        }
        return curr;
    }

}
