package Leetcode;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @author sfedosov on 12/18/19.
 */
public class MinimumCostToConnectSticks {

    public static void main(String[] args) {
        assert connectSticks(new int[]{2, 4, 3}) == 14;
        assert connectSticks(new int[]{1, 8, 3, 5}) == 30;
    }

    public static int connectSticks(int[] sticks) {
        Queue<Integer> q = new PriorityQueue<>();
        for (int stick : sticks) q.offer(stick);
        int sum = 0;
        while (q.size() > 1) {
            int one = q.poll();
            int two = q.poll();
            int tmp = one + two;
            sum += tmp;
            q.offer(tmp);
        }
        return sum;
    }

}
