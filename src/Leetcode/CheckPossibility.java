package Leetcode;

/**
 * @author sfedosov on 12/6/19.
 */
public class CheckPossibility {

    // Non-decreasing Array
    // Make array increasing by modifying only one element
    public static void main(String[] args) {
        checkPossibility(new int[]{3, 4, 2, 3});
    }

    public static boolean checkPossibility(int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] > arr[i + 1]) {
                if (i > 0) {
                    if (arr[i - 1] <= arr[i + 1]) {
                        arr[i] = arr[i - 1];
                    } else {
                        arr[i + 1] = arr[i];
                    }
                }
                count++;
                if (count > 1) return false;
            }
        }
        return true;
    }

}
