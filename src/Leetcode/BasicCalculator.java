package Leetcode;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class BasicCalculator {

    public static void main(String[] args) {
        System.out.println(calculate("3*2"));
        System.out.println(calculate("3+5/2"));
        System.out.println(calculateConstantSpace("3+6 * 2"));
    }

    public static int calculate(String s) {
        s = s.replaceAll(" ", "");
        Queue<Character> q = new LinkedList<>();
        for (char c : s.toCharArray()) q.offer(c);
        q.offer('+');
        return calculate(q);
    }

    private static int calculate(Queue<Character> q) {
        char sign = '+';
        Stack<Integer> stack = new Stack<>();
        int num = 0;
        while (!q.isEmpty()) {
            char c = q.poll();
            if (Character.isDigit(c)) {
                num = num * 10 + (c - '0');
            } else {
                if (sign == '+') {
                    stack.push(num);
                } else if (sign == '-') {
                    stack.push(-num);
                } else if (sign == '*') {
                    stack.push(stack.pop() * num);
                } else {
                    stack.push(stack.pop() / num);
                }
                num = 0;
                sign = c;
            }
        }
        int sum = 0;
        while (!stack.isEmpty()) sum += stack.pop();
        return sum;
    }

    public static int calculateConstantSpace(String s) {
        int finalResult = 0, tempResult = 0, num = 0;
        char operator = '+';

        for (int idx = 0; idx < s.length(); idx++) {
            char c = s.charAt(idx);
            if (Character.isDigit(c)) {
                num = num * 10 + (c - '0');
            }
            if (idx == s.length() - 1
                    || c == '+' || c == '-' || c == '*' || c == '/') {
                switch (operator) {
                    case '+':
                        finalResult += tempResult;
                        tempResult = num;
                        break;
                    case '-':
                        finalResult += tempResult;
                        tempResult = -num;
                        break;
                    case '*':
                        tempResult *= num;
                        break;
                    case '/':
                        tempResult /= num;
                        break;
                }
                num = 0;
                operator = c;
            }
        }
        finalResult += tempResult;
        return finalResult;
    }

}
