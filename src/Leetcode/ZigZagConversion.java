package Leetcode;

public class ZigZagConversion {

    public static void main(String[] args) {
        System.out.println(convert("PAYPALISHIRING", 3));
    }

    // zigzag conversion
    public static String convert(String s, int numRows) {
        if (numRows == 1) return s;

        StringBuilder[] array = new StringBuilder[Math.min(numRows, s.length())];

        int index = 0;
        boolean forward = false;

        for (char c : s.toCharArray()) {
            if (array[index] == null) array[index] = new StringBuilder();
            array[index].append(c);
            if (index == 0 || index == numRows - 1) forward = !forward;
            index = forward ? index + 1 : index - 1;
        }
        return String.join("", array);
    }

}
