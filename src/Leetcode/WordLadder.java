package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

/**
 * @author sfedosov on 6/21/19.
 */
public class WordLadder {

    public static void main(String[] args) {
        // "hit" -> "hot" -> "dot" -> "dog" -> "cog"
        assert ladderLength("hit", "cog", Arrays.asList("hot", "dot", "dog", "lot", "log", "cog")) == 5;
        ladderLength("red", "tax", Arrays.asList("ted", "tex", "red", "tax", "tad", "den", "rex", "pee"));
    }

    // Time/Space Complexity: O(M^2 * N), where M is the length of words and N is the total number of words in the input word list
    public static int ladderLength(String beginWord, String endWord, List<String> wordList) {
        Map<String, List<String>> combinations = generateCombinations(wordList);
        Queue<Pair<String, Integer>> q = new LinkedList<>();
        q.add(new Pair<>(beginWord, 1));
        Set<String> seen = new HashSet<>();
        seen.add(beginWord);
        while (!q.isEmpty()) {
            Pair<String, Integer> pair = q.poll();
            String startWord = pair.getKey();
            Integer level = pair.getValue();
            for (int i = 0; i < startWord.length(); i++) {
                String toCheck = startWord.substring(0, i) + '*' + startWord.substring(i + 1);
                for (String combination : combinations.getOrDefault(toCheck, new ArrayList<>())) {
                    if (combination.equals(endWord)) {
                        return level + 1;
                    }
                    if (!seen.contains(combination)) {
                        seen.add(combination);
                        q.add(new Pair<>(combination, level + 1));
                    }
                }
            }
        }
        return 0;
    }

    private static Map<String, List<String>> generateCombinations(List<String> wordList) {
        Map<String, List<String>> combinations = new HashMap<>();
        for (String word : wordList) {
            for (int i = 0; i < word.length(); i++) {
                String newWord = word.substring(0, i) + '*' + word.substring(i + 1);
                combinations.computeIfAbsent(newWord, x -> new ArrayList<>()).add(word);
            }
        }
        return combinations;
    }

    private static class Pair<K, V> {
        K key;
        V value;
        public Pair(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }
    }

}
