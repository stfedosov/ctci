package Leetcode;

import java.util.*;

public class OpenTheLock {

    public static void main(String[] args) {
        // no way to form 8888 from 0000 with the following dead ends...
        assert openLock(new String[]{"8887", "8889", "8878", "8898", "8788", "8988", "7888", "9888"}, "8888") == -1;
        // for instance, "0000" -> "1000" -> "1100" -> "1200" -> "1201" -> "1202" -> "0202"
        assert openLock(new String[]{"0201", "0101", "0102", "1212", "2002"}, "0202") == 6;
    }

    public static int openLock(String[] deadends, String target) {
        Set<String> dead = new HashSet<>();
        Collections.addAll(dead, deadends);
        if (dead.contains("0000")) return -1;
        Queue<Pair> q = new LinkedList<>();
        q.offer(new Pair("0000", 0));
        Set<String> seen = new HashSet<>();
        seen.add("0000");
        while (!q.isEmpty()) {
            Pair next = q.poll();
            int dist = next.dist;
            if (next.val.equals(target)) {
                return dist;
            }
            for (int i = 0; i < 4; i++) {
                String candidate1 = changeState(next.val, i, true);
                if (!dead.contains(candidate1) && !seen.contains(candidate1)) {
                    q.offer(new Pair(candidate1, dist + 1));
                    seen.add(candidate1);
                }
                String candidate2 = changeState(next.val, i, false);
                if (!dead.contains(candidate2) && !seen.contains(candidate2)) {
                    q.offer(new Pair(candidate2, dist + 1));
                    seen.add(candidate2);
                }
            }
        }
        return -1;
    }

    static class Pair {
        String val;
        int dist;

        Pair(String val, int dist) {
            this.val = val;
            this.dist = dist;
        }
    }

    private static String changeState(String num, int idx, boolean increase) {
        char[] array = num.toCharArray();
        int n = array[idx] - '0';
        if (increase) {
            array[idx] = n == 9 ? (char) (9 + '0') : (char) (++n + '0');
        } else {
            array[idx] = n == 0 ? (char) (9 + '0') : (char) (--n + '0');
        }
        return new String(array);
    }
}
