package Leetcode;

import java.util.ArrayList;
import java.util.List;

public class FindReplaceString {

    public static void main(String[] args) {
        assert findReplaceString("wreorttvosuidhrxvmvo",
                new int[]{14, 12, 10, 5, 0, 18},
                new String[]{"rxv", "dh", "ui", "ttv", "wreor", "vo"},
                new String[]{"frs", "c", "ql", "qpir", "gwbeve", "n"}).equals("gwbeveqpirosqlcfrsmn");
        assert findReplaceString("fvokzonyhukpwbnkomdianhirsvdulhsfseaqzktupyeverfsd",
                new int[]{26, 30, 38, 2, 41, 10, 8, 44, 19, 4, 13, 28, 21, 35, 23, 16},
                new String[]{"vd", "hsfs", "ktu", "ok", "pye", "kp", "hu", "verfs", "ia", "zon", "bnk", "ul", "nh", "aqz", "irs", "om"},
                new String[]{"h", "gdlf", "nl", "sr", "xhn", "ax", "arf", "ifuax", "a", "mk", "vwqe", "fdl", "n", "miyr", "ibh", "den"})
                .equals("fvsrmkyarfaxwvwqedendanibhhfdlgdlfemiyrnlxhnifuaxd");
        assert findReplaceString("abcd", new int[]{0, 2}, new String[]{"a", "cd"}, new String[]{"eee", "ffff"}).equals("eeebffff");
        assert findReplaceString("abcd", new int[]{0, 2}, new String[]{"ab", "ec"}, new String[]{"eee", "ffff"}).equals("eeecd");
        assert findReplaceString("wreorttvosuidhrxvmvo",
                new int[]{14, 12, 10, 5, 0, 18},
                new String[]{"rxv", "dh", "ui", "ttv", "wreor", "vo"},
                new String[]{"frs", "c", "ql", "qpir", "gwbeve", "n"}).equals("gwbeveqpirosqlcfrsmn");
    }

    public static String findReplaceString(String S, int[] indexes, String[] sources, String[] targets) {
        String[] map = new String[S.length()];
        for (int i = 0; i < S.length(); i++) {
            map[i] = S.charAt(i) + "";
        }
        int c;
        for (int i = 0; i < indexes.length; i++) {
            c = indexes[i];
            StringBuilder current = new StringBuilder();
            while (c < (indexes[i] + sources[i].length())) {
                current.append(map[c]);
                c++;
            }
            if (current.toString().equals(sources[i])) {
                while (c > indexes[i]) {
                    map[--c] = "";
                }
                map[c] = targets[i];
            }
        }
        StringBuilder res = new StringBuilder();
        for (String val : map) {
            res.append(val);
        }
        return res.toString();
    }

    public static String findReplaceStringAlternative(String S, int[] indexes, String[] sources, String[] targets) {
        if (S == null || S.length() == 0) return "";

        List<int[]> list = new ArrayList<>(indexes.length);
        for (int i = 0; i < indexes.length; i++) {
            list.add(new int[]{indexes[i], i});
        }
        //Sort indexes with frequency in reverse order, we will replace from last
        //If we will replace from first or random order, indices of the source will change in result string for the given index.
        list.sort((x, y) -> y[0] - x[0]);

        for (int[] e : list) {
            int index = e[0];
            String s = sources[e[1]];
            String t = targets[e[1]];
            int pos = S.indexOf(s, index);
            //Replace only first occurrence
            if (pos == index) {
                S = S.substring(0, pos) + S.substring(pos).replaceFirst(s, t);
            }
        }
        return S;
    }
}
