package Leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SortCharactersByFrequency {

    public static void main(String[] args) {
        assert frequencySort("tree").equals("eert");
        assert frequencySort("Aabb").equals("bbAa");
    }

    public static String frequencySort(String s) {
        Map<Character, Integer> map = new HashMap<>();
        int max = 0;
        for (char c : s.toCharArray()) {
            int num = map.getOrDefault(c, 0) + 1;
            map.put(c, num);
            max = Math.max(max, num);
        }
        String[] chars = new String[max + 1];
        StringBuilder res = new StringBuilder();
        for (Character ch : map.keySet()) {
            int counter = map.get(ch);
            char[] cc = new char[counter];
            Arrays.fill(cc, ch);
            String m = new String(cc);
            chars[counter] = chars[counter] == null ? m : chars[counter] + m;
        }
        for (int i = chars.length - 1; i >= 0; i--) {
            if (chars[i] != null) {
                res.append(chars[i]);
            }
        }
        return res.toString();
    }

}
