package Leetcode;

/**
 * @author sfedosov on 5/8/20.
 */
public class CheckStraightLine {

    public static void main(String[] args) {
        assert checkStraightLine(new int[][]{{0, 1}, {1, 3}, {-4, -7}, {5, 11}});
        assert !checkStraightLine(new int[][]{{1, 1}, {2, 2}, {3, 4}, {4, 5}, {5, 6}, {7, 7}});
        assert checkStraightLine(new int[][]{{1, 2}, {2, 3}, {3, 4}, {4, 5}, {5, 6}, {6, 7}});
    }

    public static boolean checkStraightLine(int[][] coordinates) {
        if (coordinates.length == 2) return true;
        double slope = -999;
        for (int i = 1; i < coordinates.length - 1; i++) {
            // calculate the slope  = (y2 - y1) / (x2 - x1)
            float tmp = (float) (coordinates[i][1] - coordinates[0][1]) / (coordinates[i][0] - coordinates[0][0]);
            if (slope == -999) {
                slope = tmp;
            } else if (slope != tmp) {
                return false;
            }
        }
        return true;
    }

}
