package Leetcode;

import Leetcode.RotateList.ListNode;

public class ReverseLinkedList2 {

    public static void main(String[] args) {
        ListNode listNode = new ListNode(1);
        ListNode listNode4 = new ListNode(4);
        ListNode listNode5 = new ListNode(5);
        ListNode listNode3 = new ListNode(3);
        ListNode listNode2 = new ListNode(2);
        listNode.next = listNode2;
        listNode2.next = listNode3;
        listNode3.next = listNode4;
        listNode4.next = listNode5;
        reverseBetween(listNode, 2, 4);
    }

    public static ListNode reverseBetween(ListNode head, int left, int right) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode nodeBeforeReverse = dummy;
        for (int i = 0; i < left - 1; i++) {
            nodeBeforeReverse = nodeBeforeReverse.next;
        }
        ListNode startOfTheReversal = nodeBeforeReverse.next;
        ListNode prev = null, current = startOfTheReversal;
        for (int i = 0; i < right - left + 1; i++) {
            ListNode next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        nodeBeforeReverse.next = prev; // connect start of the reversal with the reversed list
        startOfTheReversal.next = current; // connect new end of the reversal (after the reversal) with the remaining list
        return dummy.next;
    }

}
