package Leetcode;

/**
 * @author sfedosov on 8/21/18.
 */
public class CanReachBot {

    public static void main(String[] args) {
        assert canReach(1, 4, 5, 9);
        assert !canReach(1, 2, 2, 1);
    }

    private static boolean canReach(int x1, int y1, int x2, int y2) {
        if (x1 == x2 && y1 == y2) return true;
        if (x1 > x2 || y1 > y2) return false;
        return canReach(x1 + y1, y1, x2, y2) || canReach(x1, x1 + y1, x2, y2);
    }

}
