package Leetcode;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author sfedosov on 7/20/19.
 */
public class WallsAndGates {

    public static void main(String[] args) {
        int[][] rooms = new int[][]{
                {0, -1, -2, 0},
                {0, 0, 0, -1},
                {0, -1, 0, -1},
                {-2, -1, 0, 0}
        };
        wallsAndGates(rooms);
        for (int[] r : rooms) {
            System.out.println(Arrays.toString(r));
        }

        /*

          0  -1  -2   0
          0   0   0  -1
          0  -1   0  -1
         -2  -1   0   0


        ||  ||  ||
        \/  \/  \/


        3  -1   0   1
        2   2   1  -1
        1  -1   2  -1
        0  -1   3   4

         */
    }

    private static final int EMPTY = 0;
    private static final int GATE = -2;
    private static final int[][] DIRECTIONS = new int[][]{{1, 0}, {-1, 0}, {0, 1}, {0, -1}};

    // Time complexity : O(mn)
    // Once we set a room's distance, we are basically marking it as visited, which means each room is visited at most once.
    // Therefore, the time complexity does not depend on the number of gates and is O(mn)O(mn).
    //
    public static void wallsAndGates(int[][] rooms) {
        int m = rooms.length;
        if (m == 0) return;
        int n = rooms[0].length;
        Queue<int[]> q = new LinkedList<>();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (rooms[i][j] == GATE) {
                    q.add(new int[]{i, j});
                }
            }
        }
        while (!q.isEmpty()) {
            int[] point = q.poll();
            int i = point[0];
            int j = point[1];
            for (int[] direction : DIRECTIONS) {
                int new_i = i + direction[0];
                int new_j = j + direction[1];
                if (new_i < 0 || new_j < 0 || new_i >= m || new_j >= n || rooms[new_i][new_j] != EMPTY) {
                    continue;
                }
                rooms[new_i][new_j] = (rooms[i][j] == -2 ? 0 : rooms[i][j]) + 1;
                q.add(new int[]{new_i, new_j});
            }
        }
    }

}
