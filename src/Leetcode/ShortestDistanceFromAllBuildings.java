package Leetcode;

import java.util.LinkedList;
import java.util.Queue;

public class ShortestDistanceFromAllBuildings {

    // Shortest Distance from All Buildings
    public static void main(String[] args) {
        System.out.println(shortestDistance(new int[][]{
                {1, 0, 2, 0, 1},
                {0, 0, 0, 0, 0},
                {0, 0, 1, 0, 0}
        }));

        System.out.println(shortestDistance(new int[][]{
                {1, 1},
                {0, 1}
        }));

        System.out.println(shortestDistance(new int[][]{{1, 0}}));

        System.out.println(shortestDistance(new int[][]{
                {1, 1, 1, 1, 1, 0},
                {0, 0, 0, 0, 0, 1},
                {0, 1, 1, 0, 0, 1},
                {1, 0, 0, 1, 0, 1},
                {1, 0, 1, 0, 0, 1},
                {1, 0, 0, 0, 0, 1},
                {0, 1, 1, 1, 1, 0}}));
    }

    private static int[][] directions = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};

    // O(N^2 * M^2)
    public static int shortestDistance(int[][] grid) {
        int rows = grid.length, columns = grid[0].length, numberOfBuildings = 0;
        int[][] distances = new int[rows][columns], reachedBuildings = new int[rows][columns];

        for (int i = 0; i < rows; i++)
            for (int j = 0; j < columns; j++) {
                if (grid[i][j] == 1) {
                    numberOfBuildings++;
                    bfs(grid, rows, columns, distances, reachedBuildings, i, j);
                }
            }
        int shortest = Integer.MAX_VALUE;
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < columns; j++)
                if (distances[i][j] < shortest && reachedBuildings[i][j] == numberOfBuildings)
                    shortest = distances[i][j];
        return shortest == Integer.MAX_VALUE ? -1 : shortest;
    }

    private static void bfs(int[][] grid, int rows, int columns, int[][] distances, int[][] reachedBuildings, int i, int j) {
        int level = 1;
        Queue<int[]> q = new LinkedList<>();
        q.offer(new int[]{i, j});
        boolean[][] visited = new boolean[rows][columns];
        while (!q.isEmpty()) {
            int size = q.size();
            while (size-- > 0) {
                int[] polled = q.poll();
                for (int[] direction : directions) {
                    int new_x = polled[0] + direction[0], new_y = polled[1] + direction[1];
                    if (new_x >= 0 && new_x < rows
                            && new_y >= 0 && new_y < columns
                            && grid[new_x][new_y] == 0 && !visited[new_x][new_y]) {
                        distances[new_x][new_y] += level;
                        reachedBuildings[new_x][new_y]++;
                        q.offer(new int[]{new_x, new_y});
                        visited[new_x][new_y] = true;
                    }
                }
            }
            level++;
        }
    }
}

