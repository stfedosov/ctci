package Leetcode;

public class EditDistance {

    public int minDistance(String word1, String word2) {
        int[][] dp = new int[word1.length() + 1][word2.length() + 1];
        return editDistance(word1, word2, 0, 0, dp);
    }

    private int editDistance(String s1, String s2, int i, int j, int[][] dp) {
        if (i == s1.length() && j == s2.length())
            return 0;
        if (j == s2.length()) {
            return s1.length() - i;
        }
        if (i == s1.length()) {
            return s2.length() - j;
        }
        if (dp[i][j] != 0) return dp[i][j];

        if (s1.charAt(i) == s2.charAt(j)) {
            dp[i][j] = editDistance(s1, s2, i + 1, j + 1, dp);
            return dp[i][j];
        } else {
            int replace = editDistance(s1, s2, i + 1, j + 1, dp) + 1;
            int delete = editDistance(s1, s2, i, j + 1, dp) + 1;
            int insert = editDistance(s1, s2, i + 1, j, dp) + 1;
            dp[i][j] = Math.min(delete, Math.min(insert, replace));
            return dp[i][j];
        }

    }
}
