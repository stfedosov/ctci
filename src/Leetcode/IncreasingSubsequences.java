package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author sfedosov on 4/11/20.
 */
public class IncreasingSubsequences {

    public static void main(String[] args) {
        assert findSubsequences(new int[]{4, 6, 7, 7}).equals(
                Arrays.asList(Arrays.asList(7, 7), Arrays.asList(6, 7), Arrays.asList(4, 6), Arrays.asList(4, 6, 7, 7),
                Arrays.asList(4, 7, 7), Arrays.asList(4, 6, 7), Arrays.asList(4, 7), Arrays.asList(6, 7, 7)));
    }

    public static List<List<Integer>> findSubsequences(int[] nums) {
        Set<List<Integer>> res = new HashSet<>();
        generate(res, nums, new ArrayList<>(), 0);
        return new ArrayList<>(res);
    }

    private static void generate(Set<List<Integer>> res, int[] nums, List<Integer> cur, int idx) {
        if (cur.size() > 1) {
            res.add(new ArrayList<>(cur));
        }
        for (int i = idx; i < nums.length; i++) {
            if (cur.isEmpty() || cur.get(cur.size() - 1) <= nums[i]) {
                cur.add(nums[i]);
                generate(res, nums, cur, i + 1);
                cur.remove(cur.size() - 1);
            }
        }
    }

}
