package Leetcode;

import java.util.Arrays;

public class TwoSumLessThanK {

    public static void main(String[] args) {
        System.out.println(twoSumLessThanK(new int[]{34, 23, 1, 24, 75, 33, 54, 8}, 60));
        System.out.println(twoSumLessThanK(new int[]{10, 20, 30}, 15));
    }

    public static int twoSumLessThanK(int[] A, int K) {
        Arrays.sort(A);
        int diff = Integer.MAX_VALUE;
        int i = 0, j = A.length - 1;
        int candidate = -1;
        while (i < j) {
            int tmp = A[i] + A[j];
            if (tmp < K && diff > K - tmp) {
                diff = K - tmp;
                candidate = tmp;
            } else if (tmp > K) {
                j--;
            } else {
                i++;
            }

        }
        return candidate;
    }

}
