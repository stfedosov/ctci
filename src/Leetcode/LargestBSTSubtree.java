package Leetcode;

/**
 * @author sfedosov on 12/5/19.
 */
public class LargestBSTSubtree {

    int max = 0;

    public int largestBSTSubtree(TreeNode root) {
        find(root);
        return max;
    }

    private void find(TreeNode root) {
        if (root == null) return;
        if (isBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE)) {
            max = Math.max(max, findNumOfNodes(root));
        }
        find(root.left);
        find(root.right);
    }

    private boolean isBST(TreeNode root, int min, int max) {
        if (root == null) return true;
        if (root.val <= min || root.val >= max) return false;
        return isBST(root.left, min, root.val) && isBST(root.right, root.val, max);
    }

    private int findNumOfNodes(TreeNode root) {
        if (root == null) return 0;
        return 1 + findNumOfNodes(root.left) + findNumOfNodes(root.right);
    }

    class Result {  // (size, rangeLower, rangeUpper) -- size of current tree, range of current tree [rangeLower, rangeUpper]
        int size;
        int lower;
        int upper;

        Result(int size, int lower, int upper) {
            this.size = size;
            this.lower = lower;
            this.upper = upper;
        }
    }

    // Optimized solution ------

    int max2 = 0;

    public int largestBSTSubtree2(TreeNode root) {
        if (root == null) {
            return 0;
        }
        traverse(root);
        return max2;
    }

    private Result traverse(TreeNode root) {
        if (root == null) {
            return new Result(0, Integer.MAX_VALUE, Integer.MIN_VALUE);
        }
        Result left = traverse(root.left);
        Result right = traverse(root.right);
        if (left.size == -1 || right.size == -1 || root.val <= left.upper || root.val >= right.lower) {
            return new Result(-1, 0, 0);
        }
        int size = left.size + 1 + right.size;
        max2 = Math.max(size, max2);
        return new Result(size, Math.min(left.lower, root.val), Math.max(right.upper, root.val));
    }

}
