package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * @author sfedosov on 4/12/20.
 */
public class NextLargerNodes {

    public static void main(String[] args) {
        ListNode root = new ListNode(2);
        ListNode next = new ListNode(1);
        root.next = next;
        next.next = new ListNode(5);

        assert Arrays.equals(nextLargerNodes(root), new int[]{5, 5, 0}); // 2 -> 1 -> 5 => 5 -> 5 -> 0

        root = new ListNode(2);
        ListNode next1 = new ListNode(7);
        root.next = next1;
        ListNode next2 = new ListNode(4);
        next1.next = next2;
        ListNode next3 = new ListNode(3);
        next2.next = next3;
        next3.next = new ListNode(5);

        assert Arrays.equals(nextLargerNodes(root), new int[]{7, 0, 5, 5, 0}); // 2 -> 7 -> 4 -> 3 -> 5 => 7 -> 0 -> 5 -> 5 -> 0
    }

    public static int[] nextLargerNodes(ListNode head) {
        List<Integer> list = new ArrayList<>();
        ListNode tmp = head;
        while (tmp != null) {
            list.add(tmp.val);
            tmp = tmp.next;
        }
        int[] res = new int[list.size()];
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < res.length; i++) {
            while (!stack.isEmpty() && list.get(stack.peek()) < list.get(i)) {
                res[stack.pop()] = list.get(i);
            }
            stack.push(i);
        }
        return res;
    }

    private static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }
}
