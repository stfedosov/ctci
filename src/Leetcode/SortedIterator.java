package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @author sfedosov on 7/24/19.
 */
public class SortedIterator implements Iterator<Integer> {
    private Queue<QueueNode> minHeap;
    private List<List<Integer>> lists;

    public SortedIterator(List<List<Integer>> lists) {
        this.minHeap = new PriorityQueue<>();
        this.lists = lists;
        for (int i = 0; i < lists.size(); i++) {
            minHeap.add(new QueueNode(i, 0, lists.get(i).get(0)));
        }
    }

    @Override
    public boolean hasNext() {
        return minHeap.size() > 0;
    }

    @Override
    public Integer next() {
        QueueNode pollNode = minHeap.poll();
        int toReturn = pollNode.value;
        addNextNodeToQueue(pollNode.listNumber, pollNode.index);
        return toReturn;
    }

    private void addNextNodeToQueue(int listNumber, int index) {
        List<Integer> list = lists.get(listNumber);
        if (list.size() > index + 1) {
            minHeap.offer(new QueueNode(listNumber, index + 1, list.get(index + 1)));
        }
    }

    public static void main(String[] args) {
        List<List<Integer>> lists = new ArrayList<List<Integer>>() {{
            add(Arrays.asList(1, 4, 5, 8, 9));
            add(Arrays.asList(3, 4, 4, 6));
            add(Arrays.asList(0, 2, 8));
        }};
        SortedIterator it = new SortedIterator(lists);
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }
}

class QueueNode implements Comparable<QueueNode> {
    int listNumber, index, value;

    public QueueNode(int array, int index, int value) {
        this.listNumber = array;
        this.index = index;
        this.value = value;
    }

    public int compareTo(QueueNode n) {
        return value - n.value;
    }
}