package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author sfedosov on 11/21/19.
 */
public class SumTo100 {

    public static void main(String[] args) {
        String s = "123456789";
        int target = 100;
        List<String> list = new ArrayList<>();
        generate(s, 0, list, "");
        List<String> result = new ArrayList<>();
        convert(list).forEach(l -> dfs(target, result, l, 0, 0, ""));
        for (String res : result) System.out.println(res);
    }

    private static void dfs(int target, List<String> result, List<Integer> array, int sum, int idx, String tmp) {
        if (idx == array.size()) {
            if (sum == target) {
                result.add(tmp.startsWith("+") ? tmp.substring(1) : tmp);
            }
        } else {
            dfs(target, result, array, sum + array.get(idx), idx + 1, tmp + "+" + array.get(idx));
            dfs(target, result, array, sum - array.get(idx), idx + 1, tmp + "-" + array.get(idx));
        }
    }

    private static void generate(String s, int idx, List<String> list, String num) {
        if (idx == s.length()) {
            list.add(num);
        } else {
            generate(s, idx + 1, list, num + s.charAt(idx));
            generate(s, idx + 1, list, num + " " + s.charAt(idx));
        }
    }

    private static List<List<Integer>> convert(List<String> input) {
        List<List<Integer>> result = new ArrayList<>();
        input.forEach(s -> result.add(
                Arrays.stream(s.split(" "))
                        .filter(x -> !x.isEmpty())
                        .map(Integer::parseInt)
                        .collect(Collectors.toList())));
        return result;
    }

}
