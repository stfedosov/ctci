package Leetcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

/**
 * @author sfedosov on 11/17/19.
 */
public class Shift2DGrid {

    public static void main(String[] args) {
        System.out.println(shiftGrid(new int[][]{{3,8,1,9},{19,7,2,5},{4,6,11,10},{12,0,21,13}}, 4));
        System.out.println(shiftGrid(new int[][]{{1,2,3},{4,5,6},{7,8,9}}, 9));
        System.out.println(shiftGrid(new int[][]{{1},{2},{3},{4},{7},{6},{5}}, 23));
    }

    public static List<List<Integer>> shiftGrid(int[][] grid, int k) {
        Deque<Integer> deque = new ArrayDeque<>();
        for (int[] g : grid) {
            for (int x : g) {
                deque.offer(x);
            }
        }
        while (k-- > 0) {
            deque.offerFirst(deque.pollLast());
        }
        List<List<Integer>> result = new ArrayList<>();
        while (!deque.isEmpty()) {
            int length = grid[0].length;
            List<Integer> tmp = new ArrayList<>();
            while (length-- > 0) {
                tmp.add(deque.pollFirst());
            }
            result.add(tmp);
        }
        return result;
    }

}
