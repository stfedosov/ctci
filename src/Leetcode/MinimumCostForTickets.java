package Leetcode;

/*
 * In a country popular for train travel, you have planned some train travelling one year in advance.
 * The days of the year that you will travel is given as an array days.  Each day is an integer from 1 to 365.
 * <p>
 * Train tickets are sold in 3 different ways:
 * <p>
 * a 1-day pass is sold for costs[0] dollars;
 * a 7-day pass is sold for costs[1] dollars;
 * a 30-day pass is sold for costs[2] dollars.
 * The passes allow that many days of consecutive travel.  For example, if we get a 7-day pass on day 2,
 * then we can travel for 7 days: day 2, 3, 4, 5, 6, 7, and 8.
 * <p>
 * Return the minimum number of dollars you need to travel every day in the given list of days.
 * <p>
 * Example 1:
 * <p>
 * Input: days = [1,4,6,7,8,20], costs = [2,7,15]
 * Output: 11
 * Explanation:
 * For example, here is one way to buy passes that lets you travel your travel plan:
 * On day 1, you bought a 1-day pass for costs[0] = $2, which covered day 1.
 * On day 3, you bought a 7-day pass for costs[1] = $7, which covered days 3, 4, ..., 9.
 * On day 20, you bought a 1-day pass for costs[0] = $2, which covered day 20.
 * In total, you spent $11 and covered all the days of your travel.
 * <p>
 * Example 2:
 * <p>
 * Input: days = [1,2,3,4,5,6,7,8,9,10,30,31], costs = [2,7,15]
 * Output: 17
 * Explanation:
 * For example, here is one way to buy passes that lets you travel your travel plan:
 * On day 1, you bought a 30-day pass for costs[2] = $15 which covered days 1, 2, ..., 30.
 * On day 31, you bought a 1-day pass for costs[0] = $2 which covered day 31.
 * In total, you spent $17 and covered all the days of your travel.
 */
public class MinimumCostForTickets {

    public static void main(String[] args) {
        assert mincostTickets(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 30, 31}, new int[]{2, 7, 15}) == 17;
        assert mincostTickets(new int[]{1, 4, 6, 7, 8, 20}, new int[]{2, 7, 15}) == 11;
    }

    public static int mincostTickets(int[] days, int[] costs) {
        return dfs(days, costs, 0, 0);
    }

    private static int dfs(int[] days, int[] costs, int idx, int endDay) {
        if (idx == days.length) {
            return 0;
        } else if (days[idx] <= endDay) { // Current day is already covered, move to the next day
            return dfs(days, costs, idx + 1, endDay);
        } else { // Compute costs for each pass
            int minCostIfTaken1DayPass = costs[0] + dfs(days, costs, idx + 1, endDay);
            int minCostIfTaken7DayPass = costs[1] + dfs(days, costs, idx + 1, days[idx] + 6);
            int minCostIfTaken30DayPass = costs[2] + dfs(days, costs, idx + 1, days[idx] + 29);
            return Math.min(minCostIfTaken1DayPass, Math.min(minCostIfTaken7DayPass, minCostIfTaken30DayPass));
        }
    }

    /*
    ------------------
      Cached version
    ------------------

    public static int mincostTickets(int[] days, int[] costs) {
        Map<String, Integer> memo = new HashMap<>();
        return dfs(days, costs, 0, 0, memo);
    }

    private static int dfs(int[] days, int[] costs, int idx, int endDay, Map<String, Integer> memo) {
        if (idx == days.length) {
            return 0;
        }

        String key = idx + "," + endDay;
        if (memo.containsKey(key)) {
            return memo.get(key); // Return cached result
        } else if (days[idx] <= endDay) {
            return dfs(days, costs, idx + 1, endDay, memo);
        } else {
            // Compute costs for each pass
            int minCostIfTaken1DayPass = costs[0] + dfs(days, costs, idx + 1, endDay, memo);
            int minCostIfTaken7DayPass = costs[1] + dfs(days, costs, idx + 1, days[idx] + 6, memo);
            int minCostIfTaken30DayPass = costs[2] + dfs(days, costs, idx + 1, days[idx] + 29, memo);

            int result = Math.min(minCostIfTaken1DayPass, Math.min(minCostIfTaken7DayPass, minCostIfTaken30DayPass));
            memo.put(key, result);
            return result;
        }
    }
     */

}
