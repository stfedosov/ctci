package Leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ValidateBinaryTree {

    public static void main(String[] args) {
        //      0
        //     / \
        //    1   2
        //       /
        //      3
        assert validateBinaryTreeNodes(4, new int[]{1, -1, 3, -1}, new int[]{2, -1, -1, -1});
        //      0
        //     / \
        //    1   2
        //     \ /
        //      3
        assert !validateBinaryTreeNodes(4, new int[]{1, -1, 3, -1}, new int[]{2, 3, -1, -1});
        //      0
        //     /
        //    1
        assert !validateBinaryTreeNodes(2, new int[]{1, 0}, new int[]{-1, -1});
        //      0            3
        //     / \          / \
        //    1   2        4   5
        assert !validateBinaryTreeNodes(6, new int[]{1, -1, -1, 4, -1, -1}, new int[]{2, -1, -1, 5, -1, -1});
    }

    public static boolean validateBinaryTreeNodes(int n, int[] leftChild, int[] rightChild) {
        Map<Integer, Node> map = new HashMap<>();
        for (int node = 0; node < n; node++) {
            map.putIfAbsent(node, new Node(node));
            Node current = map.get(node);
            int left = leftChild[node], right = rightChild[node];
            if (left != -1) {
                Node nodeL = map.getOrDefault(left, new Node(left));
                if (nodeL.parent != -1) return false;
                map.put(left, current.addLeft(nodeL));
            }
            if (right != -1) {
                Node nodeR = map.getOrDefault(right, new Node(right));
                if (nodeR.parent != -1) return false;
                map.put(right, current.addRight(nodeR));
            }
        }
        Set<Integer> seen = new HashSet<>();
        for (Map.Entry<Integer, Node> entry : map.entrySet()) {
            if (entry.getValue().parent == -1 && seen.isEmpty()) {
                dfs(entry.getValue(), seen);
            }
        }
        return seen.size() == n;
    }

    private static void dfs(Node root, Set<Integer> seen) {
        if (root == null) return;
        dfs(root.left, seen);
        seen.add(root.val);
        dfs(root.right, seen);
    }

    private static class Node {
        int parent = -1;
        Node left;
        Node right;
        int val;

        public Node(int val) {
            this.val = val;
        }

        public Node addLeft(Node newLeft) {
            newLeft.parent = this.val;
            this.left = newLeft;
            return newLeft;
        }

        public Node addRight(Node newRight) {
            newRight.parent = this.val;
            this.right = newRight;
            return newRight;
        }
    }

}
