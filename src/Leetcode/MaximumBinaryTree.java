package Leetcode;

public class MaximumBinaryTree {

    public static void main(String[] args) {
        System.out.println(constructMaximumBinaryTree(new int[]{3, 2, 1, 6, 0, 5}));
    }

    public static TreeNode constructMaximumBinaryTree(int[] nums) {
        return build(nums, 0, nums.length);
    }

    private static TreeNode build(int[] nums, int s, int e) {
        if (s == e) return null;
        int max = nums[s], idx = s;
        for (int i = s; i < e; i++) {
            if (max < nums[i]) {
                max = nums[i];
                idx = i;
            }
        }
        TreeNode root = new TreeNode(max);
        root.left = build(nums, s, idx);
        root.right = build(nums, idx + 1, e);
        return root;
    }

}
