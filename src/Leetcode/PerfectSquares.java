package Leetcode;

import java.util.Arrays;

public class PerfectSquares {

    public static void main(String[] args) {
        assert numSquares(12) == 3;
        assert numSquares(13) == 2;
    }

    public static int numSquares(int n) {
        int[] cache = new int[n + 1];
        Arrays.fill(cache, -1);
        return numSquares(n, cache);
    }

    public static int numSquares(int n, int[] cache) {
        if (n < 4) return n;
        if (cache[n] != -1) return cache[n];
        int min = Integer.MAX_VALUE;
        for (int i = 1; i <= Math.sqrt(n); i++) {
            min = Math.min(min, 1 + numSquares(n - (i * i), cache));
        }
        return cache[n] = min;
    }

}
