package Leetcode;

public class NumFriendRequests {

    public static void main(String[] args) {
        assert numFriendRequests(new int[]{16, 16}) == 2;
        assert numFriendRequests(new int[]{16, 17, 18}) == 2;
        assert numFriendRequests(new int[]{20, 30, 100, 110, 120}) == 3;
    }

    // Person A will friend request B iif
    // 0.5 * age[A] + 7 < age[B] <= age[A]
    public static int numFriendRequests(int[] ages) {
        int[] count = new int[121];
        for (int age : ages) {
            count[age]++;
        }
        int frq = 0;
        for (int a = 0; a <= 120; a++) {
            for (int b = (int) (0.5 * a + 8); b <= a; b++) {
                frq += count[a] * count[b];
                if (a == b) frq -= count[a];
            }
        }
        return frq;
    }
}
