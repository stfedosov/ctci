package Leetcode;

/**
 * @author sfedosov on 3/11/19.
 */
public class WaterAndJugProblem {

    public static void main(String[] args) {
        assert canMeasureWater(3, 5, 4);
        assert !canMeasureWater(2, 6, 5);
        assert !canMeasureWater(0, 0, 1);
    }

    public static boolean canMeasureWater(int x, int y, int z) {
        if (x + y == z) return true;
        if (x + y < z) return false;
        return z % gcd(x, y) == 0;
    }

    private static int gcd(int x, int y) {
        while (y > 0) {
            int temp = x % y;
            x = y;
            y = temp;
        }
        return x;
    }

    public static int gcd2(int num1, int num2) {
        // Base case
        if (num1 == num2) {
            return num1;
        }
        // Recursive case
        if (num1 > num2) {
            return gcd2(num1 - num2, num2);
        } else {
            return gcd2(num1, num2 - num1);
        }
    }

}
