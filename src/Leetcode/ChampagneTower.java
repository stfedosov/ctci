package Leetcode;

/**
 * @author sfedosov on 1/31/19.
 */
public class ChampagneTower {

    public static void main(String[] args) {
        champagneTower(3, 1, 1);
    }

    public static double champagneTower(int poured, int query_row, int query_glass) {
        double[][] pyramid = new double[101][101];
        pyramid[0][0] = poured;
        for (int row = 0; row <= query_row; ++row) {
            for (int column = 0; column <= row; ++column) {
                double quantity = (pyramid[row][column] - 1.0) / 2.0;
                if (quantity < 0) continue;
                pyramid[row + 1][column] += quantity;
                pyramid[row + 1][column + 1] += quantity;
            }
        }
        return Math.min(1, pyramid[query_row][query_glass]);
    }

}
