package Leetcode;

/**
 * @author sfedosov on 2/20/20.
 */
public class MaximumDifferenceBetweenNodeAndAncestor {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(8);
        TreeNode left = new TreeNode(3);
        root.left = left;
        left.left = new TreeNode(1);
        TreeNode right1 = new TreeNode(6);
        left.right = right1;
        right1.left = new TreeNode(4);
        right1.right = new TreeNode(7);
        TreeNode right = new TreeNode(10);
        TreeNode right2 = new TreeNode(14);
        right.right = right2;
        right2.left = new TreeNode(13);
        root.right = right;

        /*
            We have various ancestor-node differences, some of which are given below :
            |8 - 3| = 5
            |3 - 7| = 4
            |8 - 1| = 7
            |10 - 13| = 3
            Among all possible differences, the maximum value of 7 is obtained by |8 - 1| = 7.
         */

        assert maxAncestorDiff(root) == 7;
    }

    static int max = 0;

    public static int maxAncestorDiff(TreeNode root) {
        helper(root, root.val, root.val);
        return max;
    }

    private static void helper(TreeNode root, int currMax, int currMin) {
        if (root == null) return;
        max = Math.max(max, Math.max(Math.abs(currMax - root.val), Math.abs(currMin - root.val)));
        int newMax = Math.max(currMax, root.val);
        int newMin = Math.min(currMin, root.val);
        helper(root.left, newMax, newMin);
        helper(root.right, newMax, newMin);
    }

}
