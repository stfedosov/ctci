package Leetcode;

import java.util.Arrays;
import java.util.Comparator;

/**
 * @author sfedosov on 3/7/19.
 */
public class EraseOverlapIntervals {

    public int eraseOverlapIntervals(Interval[] intervals) {
        if (intervals == null || intervals.length == 0) return 0;
        Arrays.sort(intervals, Comparator.comparingInt(o -> o.end));
        int count = 1;
        int last = 0;
        for (int i = 1; i < intervals.length; i++) {
            if (intervals[last].end <= intervals[i].start) {
                count++;
                last = i;
            }
        }
        return intervals.length - count;
    }

}
