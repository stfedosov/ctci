package Leetcode;

import java.util.ArrayList;
import java.util.List;

public class Subsets {

    public static List<List<Integer>> subsetsBackTracking(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        helper(nums, result, new ArrayList<>(), 0);
        return result;
    }

    private static void helper(int[] nums, List<List<Integer>> result, List<Integer> current, int i) {
        if (i == nums.length) {
            result.add(new ArrayList<>(current));
        } else {
            current.add(nums[i]);
            helper(nums, result, current, i + 1);
            current.remove(current.size() - 1);
            helper(nums, result, current, i + 1);
        }
    }

    // Print all subsets of given set[]
    public static List<List<Integer>> subsetsWithBinary(int[] nums) {
        int n = nums.length;
        List<List<Integer>> result = new ArrayList<>();
        // Run a loop for printing all 2^n
        // subsets one by obe
        for (int i = 0; i < (1 << n); i++) {
            List<Integer> tmp = new ArrayList<>();

            // Print current subset
            for (int j = 0; j < n; j++) {

                // (1<<j) is a number with jth bit 1
                // so when we 'and' them with the
                // subset number we get which numbers
                // are present in the subset and which
                // are not
                if ((i & (1 << j)) > 0) {
                    tmp.add(nums[j]);
                }
            }
            result.add(tmp);
        }
        return result;
    }

    // Driver code
    public static void main(String[] args) {
        int set[] = {1, 2, 3};
        System.out.println(subsetsWithBinary(set));
        System.out.println(subsetsBackTracking(set));
    }

}
