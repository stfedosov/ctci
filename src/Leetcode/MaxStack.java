package Leetcode;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class MaxStack {

    public static void main(String[] args) {
        MaxStack maxStack = new MaxStack();
        maxStack.push(5);
        maxStack.push(1);
        maxStack.push(5);
        assert maxStack.top() == 5;
        assert maxStack.popMax() == 5;
        assert maxStack.top() == 1;
        assert maxStack.peekMax() == 5;
        assert maxStack.pop() == 1;
        assert maxStack.top() == 5;

        maxStack = new MaxStack();
        maxStack.push(5);
        maxStack.push(1);
        assert maxStack.popMax() == 5;
        assert maxStack.peekMax() == 1;
    }

    Node head, tail;
    TreeMap<Integer, List<Node>> map;

    public MaxStack() {
        head = new Node(0);
        tail = new Node(0);
        head.next = tail;
        tail.pre = head;
        map = new TreeMap<>();
    }

    public void push(int x) {
        Node newNode = new Node(x);
        newNode.pre = tail.pre;
        newNode.next = tail;
        tail.pre.next = newNode;
        tail.pre = newNode;
        map.computeIfAbsent(x, list -> new ArrayList<>()).add(newNode);
    }

    public int pop() {
        int value = tail.pre.val;
        removeNode(tail.pre);
        int listSize = map.get(value).size();
        map.get(value).remove(listSize - 1);
        if (listSize == 1) map.remove(value);
        return value;
    }

    public int top() {
        return tail.pre.val;
    }

    public int peekMax() {
        return map.lastKey();
    }

    public int popMax() {
        int maxVal = map.lastKey(), listSize = map.get(maxVal).size();
        Node node = map.get(maxVal).remove(listSize - 1);
        removeNode(node);
        if (listSize == 1) map.remove(maxVal);
        return maxVal;
    }

    private void removeNode(Node n) {
        Node preNode = n.pre, nextNode = n.next;
        preNode.next = nextNode;
        nextNode.pre = preNode;
    }

    static class Node {
        Node pre, next;
        int val;

        public Node(int x) {
            this.val = x;
            this.pre = null;
            this.next = null;
        }
    }
}
