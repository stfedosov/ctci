package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Partition {

    // Given a string s, partition s such that every substring of the partition is a palindrome.
    // Return all possible palindrome partitioning of s.
    // A palindrome string is a string that reads the same backward as forward.
    public static void main(String[] args) {
        assert partition("ccdd").equals(Arrays.asList(
                Arrays.asList("c", "c", "d", "d"), Arrays.asList("c", "c", "dd"),
                Arrays.asList("cc", "d", "d"), Arrays.asList("cc", "dd")));
        assert partition("aab").equals(Arrays.asList(
                Arrays.asList("a", "a", "b"), Arrays.asList("aa", "b")
        ));
        assert partition("a").equals(Collections.singletonList(Collections.singletonList("a")));
    }

    public static List<List<String>> partition(String s) {
        List<List<String>> result = new ArrayList<>();
        evaluate(s, new ArrayList<>(), result);
        return result;
    }

    private static void evaluate(String s, List<String> curr, List<List<String>> result) {
        if (s.isEmpty()) {
            result.add(new ArrayList<>(curr));
            return;
        }
        for (int i = 1; i <= s.length(); i++) {
            String substr = s.substring(0, i);
            if (isPal(substr)) {
                curr.add(substr);
                evaluate(s.substring(i), curr, result);
                curr.remove(curr.size() - 1);
            }
        }
    }

    private static boolean isPal(String s) {
        if (s.isEmpty()) return false;
        if (s.length() == 1) return true;
        int start = 0, end = s.length() - 1;
        while (start < end) {
            if (s.charAt(start) != s.charAt(end)) return false;
            start++;
            end--;
        }
        return true;
    }

}
