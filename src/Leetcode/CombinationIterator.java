package Leetcode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author sfedosov on 2/15/20.
 */
public class CombinationIterator {

    public static void main(String[] args) {
        CombinationIterator iterator = new CombinationIterator("abc", 2);
        assert iterator.next().equals("ab");
        assert iterator.hasNext();
        assert iterator.next().equals("ac");
        assert iterator.hasNext();
        assert iterator.next().equals("bc");
        assert !iterator.hasNext();
    }

    private Iterator<String> it;

    public CombinationIterator(String characters, int combinationLength) {
        List<String> list = new ArrayList<>();
        generate(list, characters, "", 0, combinationLength);
        it = list.iterator();
    }

    public String next() {
        return it.next();
    }

    public boolean hasNext() {
        return it.hasNext();
    }

    private void generate(List<String> list, String characters, String current, int start, int limit) {
        if (current.length() == limit) {
            list.add(current);
        } else {
            for (int i = start; i < characters.length(); i++) {
                generate(list, characters, current + characters.charAt(i), i + 1, limit);
            }
        }
    }

}
