package Leetcode;

public class HappyNumber {

    public static void main(String[] args) {

        /*

        Starting with any positive integer, replace the number by the sum of the squares of its digits,
        and repeat the process until the number equals 1 (where it will stay),
        or it loops endlessly in a cycle which does not include 1.
        Those numbers for which this process ends in 1 are happy numbers.

            Explanation:
            1^2 + 9^2 = 82
            8^2 + 2^2 = 68
            6^2 + 8^2 = 100
            1^2 + 0^2 + 0^2 = 1
         */

        assert !isHappy(20);
        assert isHappy(19);
        assert !isHappy(18);
    }

    // Time O(log(n)) Space O(1)
    public static boolean isHappy(int n) {
        int slowPointer = n, fastPointer = getSum(n);
        while (slowPointer != 1 && slowPointer != fastPointer) {
            slowPointer = getSum(slowPointer);
            fastPointer = getSum(getSum(fastPointer));
        }
        return slowPointer == 1;
    }

    private static int getSum(int n) {
        int sum = 0;
        while (n > 0) {
            sum += (n % 10) * (n % 10);
            n /= 10;
        }
        return sum;
    }

}
