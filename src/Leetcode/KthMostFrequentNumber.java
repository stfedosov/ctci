package Leetcode;

import Hackerrank.MaxIntHeap;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 1/23/18.
 */
public class KthMostFrequentNumber {

    public static void main(String[] args) {
        printKthMostFreq(new int[]{1, 2, 3, 2, 1, 1, 2, 2, 2, 3}, 2);
    }

    private static void printKthMostFreq(int[] ints, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i : ints) {
            map.put(i, 1 + (map.getOrDefault(i, 0)));
        }
        MaxIntHeap heap = new MaxIntHeap();
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            heap.add(entry.getValue());
        }
        int kth = 0;
        while (k > 0) {
            kth = heap.poll();
            k--;
        }
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() == kth) {
                System.out.println(entry.getKey());
                break;
            }
        }
    }

}
