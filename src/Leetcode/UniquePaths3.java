package Leetcode;

public class UniquePaths3 {

    public static void main(String[] args) {
        // Return the number of 4-directional walks from the starting square to the ending square,
        // that walk over every non-obstacle square exactly once.

        assert uniquePathsIII(new int[][]{{1, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 2, -1}}) == 2;
        // Explanation: We have the following two paths:
        //1. (0,0),(0,1),(0,2),(0,3),(1,3),(1,2),(1,1),(1,0),(2,0),(2,1),(2,2)
        //2. (0,0),(1,0),(2,0),(2,1),(1,1),(0,1),(0,2),(0,3),(1,3),(1,2),(2,2)

        assert uniquePathsIII(new int[][]{{1, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 2}}) == 4;
        // Explanation: We have the following four paths:
        //1. (0,0),(0,1),(0,2),(0,3),(1,3),(1,2),(1,1),(1,0),(2,0),(2,1),(2,2),(2,3)
        //2. (0,0),(0,1),(1,1),(1,0),(2,0),(2,1),(2,2),(1,2),(0,2),(0,3),(1,3),(2,3)
        //3. (0,0),(1,0),(2,0),(2,1),(2,2),(1,2),(1,1),(0,1),(0,2),(0,3),(1,3),(2,3)
        //4. (0,0),(1,0),(2,0),(2,1),(1,1),(0,1),(0,2),(0,3),(1,3),(1,2),(2,2),(2,3)
    }

    private static int answer;
    private static int[][] directions = new int[][]{{-1, 0}, {0, -1}, {0, 1}, {1, 0}};

    public static int uniquePathsIII(int[][] grid) {
        int numOfCellsToWalk = 0;
        int sr = 0, sc = 0;
        for (int r = 0; r < grid.length; ++r)
            for (int c = 0; c < grid[0].length; ++c) {
                if (grid[r][c] != -1) {
                    numOfCellsToWalk++;
                }
                if (grid[r][c] == 1) {
                    sr = r;
                    sc = c;
                }
            }

        answer = 0;
        dfs(grid, sr, sc, numOfCellsToWalk);
        return answer;
    }

    public static void dfs(int[][] grid, int i, int j, int numOfCellsToWalk) {
        numOfCellsToWalk--;
        if (numOfCellsToWalk < 0) return;
        if (grid[i][j] == 2) {
            if (numOfCellsToWalk == 0) answer++;
            return;
        }
        grid[i][j] = 3;
        for (int[] direction : directions) {
            int new_i = i + direction[0];
            int new_j = j + direction[1];
            if (0 <= new_i
                    && new_i < grid.length
                    && 0 <= new_j
                    && new_j < grid[0].length
                    && grid[new_i][new_j] % 2 == 0) {
                dfs(grid, new_i, new_j, numOfCellsToWalk);
            }
        }
        grid[i][j] = 0;
    }

}
