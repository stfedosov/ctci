package Leetcode;

public class PalindromicSubstrings {

    public static void main(String[] args) {
        assert countSubstrings("abc") == 3;
        assert countSubstrings("aaa") == 6;
        assert countSubstrings("a") == 1;
        assert countSubstrings("") == 0;
    }

    public static int countSubstrings(String s) {
        int result = 0;
        if (s == null || s.length() == 0) {
            return result;
        }
        for (int i = 0; i < s.length(); i++) {
            result += isPalindromeRange(s, i, i);   //odd length substrings
            result += isPalindromeRange(s, i, i + 1); // even length substrings
        }
        return result;
    }

    /**
     * check if string withing given range is palindrome and return count of palindrome substrings
     * withing this range
     **/
    public static int isPalindromeRange(String string, int start, int end) {
        int count = 0;
        while (start >= 0 && end < string.length() && string.charAt(start) == string.charAt(end)) {
            count++;
            start--;
            end++;
        }
        return count;
    }
}
