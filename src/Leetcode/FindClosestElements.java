package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author sfedosov on 1/19/20.
 */
public class FindClosestElements {

    /*

    assume A[mid] ~ A[mid + k] is sliding window

    case 1: x - A[mid] < A[mid + k] - x, need to move window go left
    -------x----A[mid]-----------------A[mid + k]----------

    case 2: x - A[mid] < A[mid + k] - x, need to move window go left again
    -------A[mid]----x-----------------A[mid + k]----------

    case 3: x - A[mid] > A[mid + k] - x, need to move window go right
    -------A[mid]------------------x---A[mid + k]----------

    case 4: x - A[mid] > A[mid + k] - x, need to move window go right
    -------A[mid]---------------------A[mid + k]----x------

     */

    public static void main(String[] args) {
        assert findClosestElements(new int[]{1, 2, 3, 4, 5}, 3, 3).equals(Arrays.asList(2, 3, 4));
        assert findClosestElements(new int[]{1, 1, 1, 10, 10, 10}, 1, 9).equals(Collections.singletonList(10));
        assert findClosestElements(new int[]{1, 1, 2, 3, 3, 3, 4, 6, 8, 8}, 6, 1).equals(Arrays.asList(1, 1, 2, 3, 3, 3));
    }

    public static List<Integer> findClosestElements(int[] arr, int k, int x) {
        int start = 0, end = arr.length - k - 1;

        while (start <= end) {
            int mid = start + (end - start) / 2;
            // adjusting the sliding window: (x - left_bound_element) compare to (right_bound_element - x)
            if (x - arr[mid] > arr[mid + k] - x)
                start = mid + 1; // right bound is closer, since the difference is bigger
            else
                end = mid - 1; // left bound is closer
        }

        List<Integer> results = new ArrayList<>();
        for (int i = start; i < start + k; i++) {
            results.add(arr[i]);
        }
        return results;

    }

}
