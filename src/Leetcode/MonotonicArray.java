package Leetcode;

public class MonotonicArray {

    public static void main(String[] args) {
        assert isMonotonic(new int[]{6, 5, 4, 4});
        assert isMonotonic(new int[]{4, 4, 4});
        assert !isMonotonic(new int[]{1, 3, 2});
    }

    public static boolean isMonotonic(int[] nums) {
        if (nums.length == 1) return true;
        boolean isIncreasing = false, isDecreasing = false;
        int prev = nums[0];
        for (int i = 1; i < nums.length; i++) {
            if (prev < nums[i]) {
                if (isDecreasing) return false;
                isIncreasing = true;
            } else if (prev > nums[i]) {
                if (isIncreasing) return false;
                isDecreasing = true;
            }
            prev = nums[i];
        }
        return true;
    }

}
