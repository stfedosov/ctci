package Leetcode;

import java.util.Arrays;

/**
 * @author sfedosov on 1/3/19.
 */
public class GenerateMatrix {

    public static void main(String[] args) {
        int[][] expected = {
                {1, 2, 3},
                {8, 9, 4},
                {7, 6, 5}};
        int[][] actual = generateMatrix(3);
        int i = 0;
        for (int[] exp : expected) {
            assert Arrays.equals(actual[i], exp);
            i++;
        }
    }

    public static int[][] generateMatrix(int n) {
        int[][] ret = new int[n][n];
        int left = 0, top = 0;
        int right = n - 1, down = n - 1;
        int count = 1;
        while (left <= right) {
            for (int j = left; j <= right; j++) {
                ret[top][j] = count++;
            }
            top++;
            for (int i = top; i <= down; i++) {
                ret[i][right] = count++;
            }
            right--;
            for (int j = right; j >= left; j--) {
                ret[down][j] = count++;
            }
            down--;
            for (int i = down; i >= top; i--) {
                ret[i][left] = count++;
            }
            left++;
        }
        return ret;
    }

}
