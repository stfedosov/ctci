package Leetcode;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * @author sfedosov on 12/14/19.
 */
public class ExclusiveTime {

    public static void main(String[] args) {
        assert Arrays.toString(exclusiveTime(2, Arrays.asList("0:start:0", "1:start:2", "1:end:5", "0:end:6"))).equals("[3, 4]");
        assert Arrays.toString(exclusiveTime(1, Arrays.asList("0:start:0", "0:start:2", "0:end:5", "0:start:6", "0:end:6", "0:end:7"))).equals("[8]");
    }

    public static int[] exclusiveTime(int n, List<String> logs) {
        Stack<Integer> stack = new Stack<>();
        int prev = 0;
        int[] result = new int[n];
        for (String log : logs) {
            String[] s = log.split(":");
            int key = Integer.parseInt(s[0]);
            boolean isStart = s[1].equals("start");
            int value = Integer.parseInt(s[2]);
            if (!stack.isEmpty()) {
                result[stack.peek()] += value - prev;
            }
            prev = value;
            if (isStart) {
                stack.push(key);
            } else {
                result[stack.pop()]++;
                prev++;
            }
        }
        return result;
    }


}
