package Leetcode;

import java.util.Arrays;

/**
 * Given an unsorted array nums, reorder it in-place such that nums[0] <= nums[1] >= nums[2] <= nums[3]....
 * Allows consecutive duplicates
 */
public class WiggleSort {

    public static void main(String[] args) {
        int[] nums = new int[]{3, 5, 2, 1, 6, 4};
        wiggleSort(nums);
        assert Arrays.equals(new int[]{3, 5, 1, 6, 2, 4}, nums);
    }

    public static void wiggleSort(int[] nums) {
        for (int i = 1; i < nums.length; i++) {
            if (i % 2 == 0) {
                if (nums[i - 1] < nums[i]) swap(nums, i - 1, i);
            } else {
                if (nums[i - 1] > nums[i]) swap(nums, i - 1, i);
            }
        }
    }

    private static void swap(int[] nums, int i1, int i2) {
        int tmp = nums[i1];
        nums[i1] = nums[i2];
        nums[i2] = tmp;
    }

}
