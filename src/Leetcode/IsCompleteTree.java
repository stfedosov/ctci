package Leetcode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author sfedosov on 12/20/18.
 */
public class IsCompleteTree {

    public boolean isCompleteTree(TreeNode root) {
        int N = countNodes(root);
        int index = 0;
        return isCompleteTree(root, index, N);
    }

    private int countNodes(TreeNode root) {
        if (root == null) return 0;
        return 1 + countNodes(root.left) + countNodes(root.right);
    }

    private boolean isCompleteTree(TreeNode root, int index, int N) {
        if (root == null) return true;
        if (index >= N) return false;
        return isCompleteTree(root.left, 2 * index + 1, N) && isCompleteTree(root.right, 2 * index + 2, N);
    }

    public boolean isCompleteTree2(TreeNode root) {
        boolean end = false;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            TreeNode cur = queue.poll();
            if (cur == null) end = true;
            else {
                if (end) return false;
                queue.add(cur.left);
                queue.add(cur.right);
            }
        }
        return true;
    }

}
