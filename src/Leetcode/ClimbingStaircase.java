package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author sfedosov on 8/14/19.
 */
public class ClimbingStaircase {

    public static void main(String[] args) {
        assert Arrays.deepToString(climbingStaircase(4, 2)).equals("[[1, 1, 1, 1], [1, 1, 2], [1, 2, 1], [2, 1, 1], [2, 2]]");
    }

    static int[][] climbingStaircase(int n, int k) {
        if (n == 0 && k == 0) return new int[1][0];
        List<String> result = new ArrayList<>();
        climbingStaircase(n, k, "", result);
        return convert(result);
    }

    private static void climbingStaircase(int n, int k, String tmp, List<String> result) {
        if (n < 0) {
            return;
        }
        if (n == 0) {
            result.add(tmp.trim());
            return;
        }
        for (int i = 1; i <= k; i++) {
            climbingStaircase(n - i, k, tmp + " " + i, result);
        }
    }

    private static int[][] convert(List<String> result) {
        int[][] toReturn = new int[result.size()][];
        int i = 0;
        for (String list : result) {
            int j = 0;
            String[] split = list.split(" ");
            toReturn[i] = new int[split.length];
            for (String x : split) {
                toReturn[i][j++] = Integer.parseInt(x);
            }
            i++;
        }
        return toReturn;
    }

}
