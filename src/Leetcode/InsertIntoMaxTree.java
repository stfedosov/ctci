package Leetcode;

public class InsertIntoMaxTree {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(4);
        root.left = new TreeNode(1);
        TreeNode right = new TreeNode(3);
        root.right = right;
        right.left = new TreeNode(2);
        // Initially we had
        //             4
        //           /   \
        //          1     3
        //               /
        //              2
        System.out.println(insertIntoMaxTree(root, 5));
        // After
        //               5
        //              /
        //             4
        //           /   \
        //          1     3
        //               /
        //              2
    }

    // The idea is to insert node to the right parent or right sub-tree of current node.
    // If inserted value is greater than current node, the inserted goes to right parent
    // If inserted value is smaller than current node, we recursively re-calculate right subtree
    public static TreeNode insertIntoMaxTree(TreeNode root, int val) {
        if (root == null) return new TreeNode(val);
        if (root.val < val) {
            TreeNode node = new TreeNode(val);
            node.left = root;
            return node;
        }
        root.right = insertIntoMaxTree(root.right, val);
        return root;
    }

}
