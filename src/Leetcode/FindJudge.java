package Leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author sfedosov on 9/3/19.
 */
public class FindJudge {

    public int findJudge(int N, int[][] trust) {
        if (trust.length == 0) return N;
        Map<Integer, List<Integer>> map = new HashMap<>();
        Set<Integer> values = new HashSet<>();
        for (int[] tr : trust) {
            map.putIfAbsent(tr[1], new ArrayList<>());
            map.get(tr[1]).add(tr[0]);
            values.add(tr[0]);
        }
        for (Map.Entry<Integer, List<Integer>> entry : map.entrySet()) {
            if (entry.getValue().size() == N - 1 && !values.contains(entry.getKey())) {
                return entry.getKey();
            }
        }
        return -1;
    }

}
