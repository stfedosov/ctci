package Leetcode;

public class SearchRotatedArrayWithDuplicates {

    public static void main(String[] args) {
        assert search(new int[]{2, 5, 6, 0, 0, 1, 2}, 0);
        assert !search(new int[]{2, 5, 6, 0, 0, 1, 2}, 3);
    }

    public static boolean search(int[] nums, int target) {
        if (null == nums || 0 == nums.length)
            return false;

        int lo = 0, hi = nums.length - 1;

        while (lo <= hi) {
            // to avoid duplicates
            while (lo < hi && nums[lo] == nums[lo + 1])
                lo++;
            while (lo < hi && nums[hi] == nums[hi - 1])
                hi--;

            int mid = lo + (hi - lo) / 2;
            if (nums[mid] == target)
                return true;

            if (nums[mid] >= nums[lo]) {
                // case 1: array is not rotated since middle element is bigger than first one
                if (target >= nums[lo] && target < nums[mid])
                    // target is somewhere in the non-rotated part
                    hi = mid - 1;
                else
                    lo = mid + 1;
            } else {
                // case 2: Middle element is smaller than the first element of the array,
                // i.e. the rotation index is somewhere between 0 and middle
                if (target <= nums[hi] && target > nums[mid])
                    // target is somewhere in the non-rotated part
                    lo = mid + 1;
                else
                    hi = mid - 1;
            }
        }

        return false;
    }

}
