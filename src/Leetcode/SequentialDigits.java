package Leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author sfedosov on 12/27/19.
 */
public class SequentialDigits {

    public static void main(String[] args) {
        assert sequentialDigits(100, 300).equals(Arrays.asList(123, 234));
        assert sequentialDigits(1000, 13000).equals(Arrays.asList(1234, 2345, 3456, 4567, 5678, 6789, 12345));
    }

    public static List<Integer> sequentialDigits(int low, int high) {
        int totalNumberOfDigits = (int) Math.log10(high) + 1;
        Set<Integer> set = new TreeSet<>();
        int tmp;
        int tt = 1;
        while (tt <= 9) {
            int k = totalNumberOfDigits;
            int i = tt;
            tmp = 0;
            while (k-- >= 0 && i < 10) {
                tmp = tmp * 10 + i;
                if (tmp >= low && tmp < high) {
                    set.add(tmp);
                }
                i++;
            }
            tt++;
        }
        return new ArrayList<>(set);
    }


}
