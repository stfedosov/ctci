package Leetcode;

/**
 * @author sfedosov on 4/21/20.
 */
public class LeftMostColumnWithOne {

    public static void main(String[] args) {
        assert leftMostColumnWithOne(
                new int[][]{
                        {0, 0, 1, 1, 1, 1, 1},
                        {0, 1, 1, 1, 1, 1, 1},
                        {0, 0, 0, 1, 1, 1, 1},
                        {0, 0, 0, 1, 1, 1, 1},
                        {0, 0, 0, 0, 0, 0, 0},
                        {0, 0, 0, 0, 0, 1, 1},
                        {0, 0, 0, 1, 1, 1, 1}}) == 1;

        assert leftMostColumnWithOne(new int[][]{
                {0, 0},
                {0, 1}}) == 1;

        assert leftMostColumnWithOne(new int[][]{
                {0, 0},
                {1, 1}
        }) == 0;

        assert leftMostColumnWithOne(new int[][]{
                {0, 0},
                {0, 0}
        }) == -1;

        assert leftMostColumnWithOne(new int[][]{
                {0, 0, 0, 1},
                {0, 0, 1, 1},
                {0, 1, 1, 1}
        }) == 1;
    }

    public static int leftMostColumnWithOne(int[][] matrix) {
        if (matrix == null || matrix.length == 0 || matrix[0] == null || matrix[0].length == 0) return -1;

        int row = matrix.length;
        int col = matrix[0].length;

        int m = 0;
        int n = col - 1;

        while (m < row && n >= 0) {
            if (matrix[m][n] == 1) {
                n--;
            } else {
                m++;
            }
        }
        return n < col - 1 ? n + 1 : -1;
    }

}
