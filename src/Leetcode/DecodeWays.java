package Leetcode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class DecodeWays {

    public static void main(String[] args) {
        System.out.println(decode("226"));
    }

    public static Set<String> decode(String code) {
        Set<String> result = new HashSet<>();
        helper("", code, 0, result);
        return result;
    }

    public static void helper(String prefix, String code, int decodePointer, Set<String> result) {
        if (decodePointer >= code.length()) {
            result.add(prefix);
            return;
        }
        if (code.charAt(0) == '0')
            return;

        helper(prefix + (char) ((code.charAt(decodePointer) - '1') + 'a'), code, decodePointer + 1, result);

        if (decodePointer + 2 <= code.length()) {
            int value = Integer.parseInt(code.substring(decodePointer, decodePointer + 2));
            if (value <= 26) {
                helper(prefix + (char) ((value - 1) + 'a'), code, decodePointer + 2, result);
            }
        }
    }


    // ----------------------------------
    // Total number of ways
    // ----------------------------------

    public int numDecodings(String s) {
        int[] memo = new int[s.length()];
        Arrays.fill(memo, -1);
        return numDecodings(s, 0, memo);
    }

    public int numDecodings(String s, int idx, int[] memo) {
        if (idx >= s.length()) {
            return 1;
        }

        if (memo[idx] != -1) {
            return memo[idx];
        }

        int numWaysFromHere = 0;

        if (idx + 1 <= s.length()) {
            String firstWay = s.substring(idx, idx + 1); // single character decoding
            if (isValid(firstWay)) {
                numWaysFromHere += numDecodings(s, idx + 1, memo);
            }
        }

        if (idx + 2 <= s.length()) {
            String secondWay = s.substring(idx, idx + 2); // 2 character decoding
            if (isValid(secondWay)) {
                numWaysFromHere += numDecodings(s, idx + 2, memo);
            }
        }
        memo[idx] = numWaysFromHere;
        return memo[idx];
    }

    private boolean isValid(String s) {
        if (s.isEmpty()) {
            return false;
        }

        if (s.charAt(0) == '0') {
            return false;
        }
        int value = Integer.parseInt(s);
        return value <= 26 && value >= 1;
    }

}
