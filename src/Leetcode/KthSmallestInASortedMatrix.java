package Leetcode;

import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Queue;

public class KthSmallestInASortedMatrix {

    /*
    You are given an m x n matrix mat that has its rows sorted in non-decreasing order and an integer k.
    You are allowed to choose exactly one element from each row to form an array.
    Return the kth smallest array sum among all possible arrays.
     */
    public static void main(String[] args) {
        assert kthSmallest(new int[][]{{1, 5, 9}, {10, 11, 13}, {12, 13, 15}}, 8) == 13;
        assert kthSmallest(new int[][]{{1, 2}, {1, 3}}, 1) == 1;
        assert kthSmallest(new int[][]{{-5}}, 1) == -5;
    }

    // Time: O(m * n * k * log(k))
    // Space: O(k)
    public static int kthSmallest(int[][] matrix, int k) {
        int col = Math.min(matrix[0].length, k);

        Queue<Integer> maxHeap = new PriorityQueue<>(Collections.reverseOrder());
        maxHeap.add(0);
        for (int[] row : matrix) {
            // max priority queue for the i-th row
            PriorityQueue<Integer> nextMaxHeap = new PriorityQueue<>(Collections.reverseOrder());
            for (int i : maxHeap) {
                for (int c = 0; c < col; c++) {
                    nextMaxHeap.add(i + row[c]);
                    if (nextMaxHeap.size() > k) {
                        nextMaxHeap.poll();
                    }
                }
            }
            maxHeap = nextMaxHeap;
        }
        return maxHeap.poll();
    }

}
