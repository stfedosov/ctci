package Leetcode;

public class SolveSudoku {

    public static void main(String[] args) {

    }

    public static void solveSudoku(char[][] board) {
        helper(board, 0, 0);
    }

    private static boolean helper(char[][] board, int row, int col) {
        if (!isValid(board, row, col, board[row][col])) return false;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == '.') {
                    for (char c = '1'; c <= '9'; c++) {
                        board[i][j] = c;
                        if (helper(board, row, col)) return true;
                        board[i][j] = '.';
                    }
                }
            }
        }
        return false;
    }

    private static boolean isValid(char[][] board, int row, int col, char charToPlace) {
        for (char[] element : board) {
            if (charToPlace == element[col]){
                return false;
            }
        }

        for (int i = 0; i < board.length; i++) {
            if (charToPlace == board[row][i]) {
                return false;
            }
        }

        int I = row / 3;
        int J = col / 3;
        int topLeftOfSubBoxRow = 3 * I;
        int topLeftOfSubBoxCol = 3 * J;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (charToPlace == board[topLeftOfSubBoxRow + i][topLeftOfSubBoxCol + j]) {
                    return false;
                }

            }
        }
        return true;
    }

}
