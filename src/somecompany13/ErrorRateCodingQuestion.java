package somecompany13;

import java.util.ArrayDeque;
import java.util.Deque;

public class ErrorRateCodingQuestion {

    public static void main(String[] args) {
//        int[] errorRates = new int[]{10, 2, 3, 5, 2, 7, 4, 1};
//        assert !existsErrorRateAtLeastRWithLengthOfL(errorRates, 3, 5);
//        assert existsErrorRateAtLeastRWithLengthOfL(errorRates, 3, 2);
//        assert existsErrorRateAtLeastRWithLengthOfL(errorRates, 3, 1);
//        assert existsErrorRateAtLeastRWithLengthOfL(errorRates, 1, 10);
//        assert existsErrorRateAtLeastRWithLengthOfL(errorRates, 2, 3);
//        assert !existsErrorRateAtLeastRWithLengthOfL(errorRates, 3, 3);
//        assert !existsErrorRateAtLeastRWithLengthOfL(errorRates, 10, 2);
//        assert !existsErrorRateAtLeastRWithLengthOfL(errorRates, -4, 2);
//        assert existsErrorRateAtLeastRWithLengthOfL(errorRates, 8, 1);
//        assert existsErrorRateAtLeastRWithLengthOfL(errorRates, 5, 1);
//
//        assert maxUnhealthyServerTime(new int[]{10, 2, 3, 5, 1, 7, 4, 8}, 5) == 12;
//        assert maxUnhealthyServerTime(new int[]{10, 2, 3, 5, 1, 7, 4, 8}, 4) == 8;
//        assert maxUnhealthyServerTime(new int[]{5, 4, 2, 10}, 2) == 8;
//        assert maxUnhealthyServerTime(new int[]{1, 2, 3}, 2) == 4;
//        assert maxUnhealthyServerTime(new int[]{1, 2, 3, 4, 5, 6}, 3) == 12;
        System.out.println(maxUnhealthyServerTime(new int[]{1,4,3,7,4,5}, 3));
    }

    private static boolean existsErrorRateAtLeastRWithLengthOfL(int[] errorRates, int L, int R) {
        if (L <= 0 || L > errorRates.length) {
            return false;
        }
        int count = 0;
        for (int error : errorRates) {
            if (error >= R) {
                count++;
                if (count == L) return true;
            } else {
                count = 0;
            }
        }
        return false;
    }

    public static int maxUnhealthyServerTime(int[] errors, int pos) {
        int n = errors.length;
        Deque<Integer> leftMinIndices = new ArrayDeque<>(), rightMinIndices = new ArrayDeque<>();
        for (int i = 0; i <= pos; i++) {
            while (!leftMinIndices.isEmpty() && errors[leftMinIndices.peekLast()] > errors[i]) {
                leftMinIndices.pollLast();
            }
            leftMinIndices.addLast(i);
        }
        for (int i = n - 1; i >= pos; i--) {
            while (!rightMinIndices.isEmpty() && errors[rightMinIndices.peekLast()] > errors[i]) {
                rightMinIndices.pollLast();
            }
            rightMinIndices.addLast(i);
        }
        int windowsLeft = 0, windowsRight = n - 1, maxValue = -1;
        while (!leftMinIndices.isEmpty() && !rightMinIndices.isEmpty()) {
            int minLeft = leftMinIndices.peekFirst(), minRight = rightMinIndices.peekFirst();
            if (errors[minLeft] < errors[minRight]) {
                maxValue = Math.max(maxValue, (windowsRight - windowsLeft + 1) * errors[leftMinIndices.pollFirst()]);
                windowsLeft = minLeft + 1;
            } else {
                maxValue = Math.max(maxValue, (windowsRight - windowsLeft + 1) * errors[rightMinIndices.pollFirst()]);
                windowsRight = minRight - 1;
            }
        }
        return maxValue;
    }

}
