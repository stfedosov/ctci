package Pramp;

import java.util.Arrays;

/**
 * @author sfedosov on 12/20/18.
 */
public class AwardBudgetCuts {

    public static void main(String[] args) {
        assert Double.compare(findGrantsCap(new double[]{2, 100, 50, 120, 1000}, 190), 47) == 0;
        assert Double.compare(findGrantsCap(new double[]{21, 100, 50, 120, 130, 110}, 140), 23.8) == 0;
        assert Double.compare(findGrantsCap(new double[]{2, 4}, 3), 1.5) == 0;
    }

    static double findGrantsCap(double[] grantsArray, double newBudget) {
        double valuePerGrant = newBudget / grantsArray.length;
        double sumOfSkipped = 0;
        int numberOfSkipped = 0;
        Arrays.sort(grantsArray);
        for (double grant : grantsArray) {
            if (Double.compare(grant, valuePerGrant) < 0) {
                sumOfSkipped += grant;
                numberOfSkipped++;
                valuePerGrant = (newBudget - sumOfSkipped) / (grantsArray.length - numberOfSkipped);
            }
        }
        return valuePerGrant;
    }

}
