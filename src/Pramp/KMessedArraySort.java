package Pramp;

import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @author sfedosov on 12/17/18.
 */
public class KMessedArraySort {

    public static void main(String[] args) {
        assert Arrays.toString(sortKMessedArray(new int[]{1, 3, 2}, 2)).equals("[1, 2, 3]");
        assert Arrays.toString(sortKMessedArray(new int[]{1}, 0)).equals("[1]");
        assert Arrays.toString(sortKMessedArray(new int[]{1, 0}, 1)).equals("[0, 1]");
    }

    private static int[] sortKMessedArray(int[] arr, int k) {
        Queue<Integer> queue = new PriorityQueue<>();
        int position = 0;
        for (int i = 0; i < arr.length; i++) {
            queue.add(arr[i]);
            if (queue.size() > k) {
                arr[position] = queue.poll();
                position++;
            }
        }
        while (!queue.isEmpty()) {
            arr[position] = queue.poll();
            position++;
        }
        return arr;
    }


}
