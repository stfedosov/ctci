package Pramp;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class MeetingPlanner {

    public static void main(String[] args) {
        int[][] slotsA = new int[][]{{10, 50}, {60, 120}, {140, 210}};
        int[][] slotsB = new int[][]{{0, 15}, {60, 70}};
        assert Objects.equals(minAvailableDuration(slotsA, slotsB, 8), List.of(60, 68));
        assert Objects.equals(minAvailableDuration(slotsA, slotsB, 12), Collections.emptyList());
    }

    public static List<Integer> minAvailableDuration(int[][] slots1, int[][] slots2, int duration) {
        int i1 = 0, i2 = 0;
        Arrays.sort(slots1, Comparator.comparingInt(a -> a[0]));
        Arrays.sort(slots2, Comparator.comparingInt(a -> a[0]));
        while (i1 < slots1.length && i2 < slots2.length) {
            int max = Math.max(slots1[i1][0], slots2[i2][0]);
            int min = Math.min(slots1[i1][1], slots2[i2][1]);
            if (max + duration <= min) {
                return List.of(max, max + duration);
            }
            if (slots1[i1][1] < slots2[i2][1])
                i1++;
            else
                i2++;
        }
        return Collections.emptyList();
    }

}
