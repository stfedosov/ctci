package Pramp;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author sfedosov on 12/3/19.
 */
public class FlattenDictionary {

    private static Map<String, String> flattenDictionary(Map<String, Object> dict) {
        // your code goes here
        // I can't see you, but I think we can start implementation? ok?
        Map<String, Object> map = new HashMap<>();
        flatten(null, map, dict);
        Map<String, String> result = new HashMap<>();
        for (String key : map.keySet()) result.put(key, String.valueOf(map.get(key)));
        return result;
    }

    private static void flatten(String initialKey,
                                Map<String, Object> map,
                                Map<String, Object> dictionary) {
        for (String currentKey : dictionary.keySet()) {
            if (dictionary.get(currentKey) instanceof Map) {
                if (initialKey == null || initialKey.isEmpty()) {
                    flatten(currentKey, map, ((Map) dictionary.get(currentKey)));
                } else {
                    flatten(initialKey + "." + currentKey, map, ((Map) dictionary.get(currentKey)));
                }
            } else {
                if (initialKey == null || initialKey.isEmpty()) {
                    map.put(currentKey, dictionary.get(currentKey));
                } else {
                    map.put(initialKey + "." + currentKey, dictionary.get(currentKey));
                }
            }
        }
    }

    public static void main(String[] args) {
        Map<String, Object> map = new HashMap<>();
        map.put("Key", "{" + "a" + ":" + "2" + "," + "b" + ":" + "3" + "}");
        System.out.println(flattenDictionary(map));
        Map<String, Object> dictionary = new LinkedHashMap<>();
        dictionary.put("key1", 1);
        dictionary.put("key2", new LinkedHashMap<>());
        ((Map) dictionary.get("key2")).put("a", 2);
        ((Map) dictionary.get("key2")).put("b", 3);
        ((Map) dictionary.get("key2")).put("c", new LinkedHashMap<>());
        ((Map) ((Map) dictionary.get("key2")).get("c")).put("d", 3);
        ((Map) ((Map) dictionary.get("key2")).get("c")).put("e", 1);
        System.out.println(flattenDictionary(dictionary));
    }

}
