package Pramp;

/**
 * @author sfedosov on 11/13/18.
 */
public class DronFlightPlanner {

    public static void main(String[] args) {
        assert calcDroneMinEnergy(new int[][]{{0, 1, 19}}) == 0;
        assert calcDroneMinEnergy(new int[][]{{0, 2, 10}, {10, 10, 8}}) == 0;
        assert calcDroneMinEnergy(new int[][]{{0, 2, 6}, {10, 10, 20}}) == 14;
    }

    private static int calcDroneMinEnergy(int[][] route) {
        int largestEnergyDeficit = 0;
        int energyBalance = 0;
        for (int i = 1; i < route.length; i++) {
            energyBalance = energyBalance + (route[i - 1][2] - route[i][2]);
            if (energyBalance < largestEnergyDeficit) {
                largestEnergyDeficit = energyBalance;
            }
        }
        return -1 * (largestEnergyDeficit);
    }

}
