package Pramp;

/**
 * @author sfedosov on 10/26/18.
 */
public class FindRoot {

    private static double EPS = 0.001;

    public static void main(String[] args) {
        assert findApproximateRoot(4, 2) - 2 <= EPS;
        assert findApproximateRoot(27, 3) - 3 <= EPS;
        assert findApproximateRoot(7, 3) - 1.913 <= EPS;
        assert findApproximateRoot(9, 2) - 3 <= EPS;
        assert findApproximateRoot(160, 3) - 5.429 <= EPS;
    }

    public static int mySqrt(int x) {
        if (x < 1) return 0;
        int low = 1;
        int high = x;
        while (low <= high) {
            int mid = (high - low) / 2 + low;
            if (x / mid == mid) return mid;
            else if (x / mid > mid) low = mid + 1;
            else high = mid - 1;
        }
        return high;
    }

    private static double findApproximateRoot(double x, int n) {
        double low = 0;
        double high = x;
        double approximateRoot;
        do {
            approximateRoot = (high + low) / 2;
            int compare = Double.compare(Math.pow(approximateRoot, n), x);
            if (compare < 0) {
                low = approximateRoot;
            } else if (compare > 0) {
                high = approximateRoot;
            } else {
                break;
            }
        } while (Double.compare(Math.abs(low - approximateRoot), EPS) > 0);
        return approximateRoot;
    }

}
