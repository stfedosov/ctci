package Pramp;

import java.util.Arrays;
import java.util.StringJoiner;

/**
 * @author sfedosov on 6/11/19.
 */
public class DiffBetweenTwoStrings {

    private static final String DELIMETER = "|";

    public static String[] diffBetweenTwoStrings(String source, String target) {
        return recurMethod(source, target).split("\\|");
    }

    private static String recurMethod(String source, String target) {
        if (source.isEmpty() || target.isEmpty()) return source.isEmpty() ? decorate(target, "+") : decorate(source, "-");
        char source0 = source.charAt(0);
        char target0 = target.charAt(0);
        if (source0 == target0) {
            return source0 + DELIMETER + recurMethod(source.substring(1), target.substring(1));
        }
        String result1 = recurMethod(source.substring(1), target);
        String result2 = recurMethod(source, target.substring(1));
        if (result1.split("\\|").length <= result2.split("\\|").length) {
            return "-" + source0 + DELIMETER + result1;
        } else {
            return "+" + target0 + DELIMETER + result2;
        }
    }

    private static String decorate(String source, String toAdd) {
        StringJoiner joiner = new StringJoiner(DELIMETER);
        for (char cs : source.toCharArray()) {
            joiner.add(toAdd + cs);
        }
        return joiner.toString();
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(diffBetweenTwoStrings("ABCDEFG", "ABDFFGH")));
        System.out.println(Arrays.toString(diffBetweenTwoStrings("HMXPHHUM", "HLZPLUPH")));
        System.out.println(Arrays.toString(diffBetweenTwoStrings("CCBC", "CCBC")));
    }

}
