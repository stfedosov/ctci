package Pramp;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author sfedosov on 6/28/19.
 */
public class ShortestCellPath {

    static int shortestCellPath(int[][] grid, int sr, int sc, int tr, int tc) {
        Queue<Node> q = new LinkedList<>();
        q.add(new Node(sr, sc, 0));
        boolean[][] visited = new boolean[grid.length][grid[0].length];
        int minDistance = Integer.MAX_VALUE;
        while (!q.isEmpty()) {
            Node polled = q.poll();
            int distance = polled.distance;
            int x = polled.x;
            int y = polled.y;
            if (x == tr && y == tc) {
                minDistance = Math.min(minDistance, distance);
            } else {
                if (isValid(x + 1, y, grid, visited)) {
                    visited[x + 1][y] = true;
                    q.add(new Node(x + 1, y, distance + 1));
                }
                if (isValid(x - 1, y, grid, visited)) {
                    visited[x - 1][y] = true;
                    q.add(new Node(x - 1, y, distance + 1));
                }
                if (isValid(x, y + 1, grid, visited)) {
                    visited[x][y + 1] = true;
                    q.add(new Node(x, y + 1, distance + 1));
                }
                if (isValid(x, y - 1, grid, visited)) {
                    visited[x][y - 1] = true;
                    q.add(new Node(x, y - 1, distance + 1));
                }
            }
        }
        return minDistance == Integer.MAX_VALUE ? -1 : minDistance;
    }

    private static boolean isValid(int x, int y, int[][] grid, boolean[][] visited) {
        return x >= 0 && y >= 0 && x < grid.length && y < grid[0].length && !visited[x][y] && grid[x][y] != 0;
    }

    public static class Node {
        public int x;
        public int y;
        public int distance;

        public Node(int x, int y, int distance) {
            this.x = x;
            this.y = y;
            this.distance = distance;
        }
    }

    public static void main(String[] args) {
        assert shortestCellPath(new int[][]{{1, 1, 1, 1}, {0, 0, 0, 1}, {1, 1, 1, 1}}, 0, 0, 2, 0) == 8;
        assert shortestCellPath(new int[][]{{1, 0, 1, 1}, {1, 0, 1, 1}, {0, 0, 1, 0}, {0, 0, 0, 0}}, 1, 3, 0, 0) == -1;
    }

}
