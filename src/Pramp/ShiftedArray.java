package Pramp;

public class ShiftedArray {

    private static int shiftedArrSearch(int[] shiftArr, int num) {
        int pivot = findPivotPoint(shiftArr);
        if (pivot == 0 || num < shiftArr[0]) {
            return binarySearch(shiftArr, pivot, shiftArr.length - 1, num);
        }
        return binarySearch(shiftArr, 0, pivot - 1, num);
    }


    private static int findPivotPoint(int[] arr) {
        int begin = 0;
        int end = arr.length - 1;

        while (begin <= end) {
            int mid = (end + begin) / 2;
            if (mid == 0 || arr[mid] < arr[mid - 1]) {
                return mid;
            }
            if (arr[mid] > arr[0]) {
                begin = mid + 1;
            } else {
                end = mid - 1;
            }
        }
        return 0;
    }


    private static int binarySearch(int[] arr, int begin, int end, int num) {
        while (begin <= end) {
            int mid = (end + begin) / 2;
            if (arr[mid] < num) {
                begin = mid + 1;
            } else if (arr[mid] == num) {
                return mid;
            } else {
                end = mid - 1;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        assert shiftedArrSearch(new int[]{3, 4, 5, 1, 2}, 2) == 4;
        assert shiftedArrSearch(new int[]{9, 12, 17, 2, 4, 5}, 2) == 3;
        assert shiftedArrSearch(new int[]{9, 12, 17, 18, 19, 20, 2, 4, 5}, 2) == 6;
        assert shiftedArrSearch(new int[]{2}, 2) == 0;
        assert shiftedArrSearch(new int[]{1, 2}, 2) == 1;
        assert shiftedArrSearch(new int[]{1, 2, 3, 4, 5, 0}, 0) == 5;
    }


}
