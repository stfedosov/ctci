package Pramp;


/**
 * @author sfedosov on 11/7/18.
 */
public class DeletionDistance {

    private static int deletionDistance(String str1, String str2) {
        int str1Len = str1.length();
        int str2Len = str2.length();

        int[][] memo = new int[str1Len + 1][str2Len + 1];

        for (int i = 0; i < str1Len + 1; i++) {
            for (int j = 0; j < str2Len + 1; j++) {
                if (i == 0) {
                    memo[i][j] = j;
                } else if (j == 0) {
                    memo[i][j] = i;
                } else if (str1.charAt(i - 1) == str2.charAt(j - 1)) {
                    memo[i][j] = memo[i - 1][j - 1];
                } else {
                    memo[i][j] = 1 + Math.min(memo[i - 1][j], memo[i][j - 1]);
                }
            }
        }
        return memo[str1Len][str2Len];
    }

    private static int deletionDistanceRecursive(String str1, String str2) {
        if (str1.equals(str2)) {
            return 0;
        } else if (str1.length() == 0 || str2.length() == 0) {
            return Math.abs(str1.length() - str2.length());
        }
        if (str1.charAt(0) != str2.charAt(0)) {
            return 1 + Math.min(deletionDistanceRecursive(str1.substring(1), str2),
                    deletionDistanceRecursive(str1, str2.substring(1)));
        } else {
            return deletionDistanceRecursive(str1.substring(1), str2.substring(1));
        }
    }

    public static void main(String[] args) {
        assert deletionDistance("", "some") == 4;
        assert deletionDistance("some", "some") == 0;
        assert deletionDistance("some", "thing") == 9;
        assert deletionDistanceRecursive("", "some") == 4;
        assert deletionDistanceRecursive("", "jj") == 2;
        assert deletionDistanceRecursive("jj", "jj") == 0;
        assert deletionDistanceRecursive("some", "thing") == 9;
        assert deletionDistanceRecursive("a", "b") == 2;
    }

}
