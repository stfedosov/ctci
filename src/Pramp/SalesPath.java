package Pramp;


import java.util.ArrayList;
import java.util.List;

/**
 * @author sfedosov on 10/21/18.
 */
public class SalesPath {

    public static void main(String[] args) {
        Node root = new Node(0);
        Node node5 = new Node(5);
        node5.addChildren(new Node(4));
        root.addChildren(node5);
        Node node33 = new Node(3);
        root.addChildren(node33);
        Node node22 = new Node(2);
        Node node00 = new Node(0);
        node33.addChildren(node22);
        node33.addChildren(node00);
        node00.addChildren(new Node(10));
        Node node1 = new Node(1);
        node1.addChildren(new Node(1));
        node22.addChildren(node1);
        Node node6 = new Node(6);
        node6.addChildren(new Node(1));
        node6.addChildren(new Node(5));
        root.addChildren(node6);
        //               0
        //            ___|____
        //           /   |    \
        //          5    3     6
        //         /    / \   / \
        //        4    2   0 1   5
        //            /    |
        //           1    10
        //          /
        //         1
        assert findMinimumSalesPathCost(root) == 7;
    }

    private static int findMinimumSalesPathCost(Node root) {
        if (root.getChildrens().size() == 0) {
            return root.getCost();
        }
        int min = Integer.MAX_VALUE;
        for (Node node : root.getChildrens()) {
            min = Math.min(findMinimumSalesPathCost(node), min);
        }
        return root.getCost() + min;
    }

    private static class Node {
        private int cost;
        private final List<Node> childrens = new ArrayList<>();

        public Node(int cost) {
            this.cost = cost;
        }

        public int getCost() {
            return cost;
        }

        public void addChildren(Node toBeAdded) {
            childrens.add(toBeAdded);
        }

        public List<Node> getChildrens() {
            return childrens;
        }
    }

}
