package Pramp;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 12/19/18.
 */
public class Merging2Packages {

    private static int[] getIndicesOfItemWeights(int[] arr, int limit) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int j = 0; j < arr.length; j++) {
            int w = arr[j];
            int comp = limit - w;
            if (map.containsKey(comp)) {
                return new int[]{j, map.get(comp)};
            } else {
                map.put(w, j);
            }
        }
        return new int[0];
    }

    public static void main(String[] args) {
        assert Arrays.toString(getIndicesOfItemWeights(new int[]{4, 4, 1}, 5)).equals("[2, 1]");
        assert Arrays.toString(getIndicesOfItemWeights(new int[]{4, 6, 10, 15, 16}, 20)).equals("[4, 0]");
    }

}
