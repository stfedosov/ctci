package Pramp;

/**
 * @author sfedosov on 11/30/18.
 */
public class BusiestTimeInTheMall {

    private static int findBusiestPeriod(int[][] data) {
        int max = Integer.MIN_VALUE;
        int currentNumberOfVisitors = 0;
        int timestamp = 0;
        for (int i = 0; i < data.length; i++) {
            if (data[i][2] == 1) {
                currentNumberOfVisitors += data[i][1];
            } else {
                currentNumberOfVisitors -= data[i][1];
            }
            if (i < data.length - 1 && data[i][0] == data[i + 1][0]) continue;
            if (max < currentNumberOfVisitors) {
                max = currentNumberOfVisitors;
                timestamp = data[i][0];
            }
        }
        return timestamp;
    }

    public static void main(String[] args) {
        assert findBusiestPeriod(new int[][]{
                {1487799425, 14, 1},
                {1487799425, 4, 0},
                {1487799425, 2, 0},
                {1487800378, 10, 1},
                {1487801478, 18, 0},
                {1487801478, 19, 1},
                {1487801478, 1, 0},
                {1487801478, 1, 1},
                {1487901013, 1, 0},
                {1487901211, 7, 1},
                {1487901211, 8, 0}
        }) == 1487801478;
    }


}
