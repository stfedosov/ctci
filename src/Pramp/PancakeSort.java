package Pramp;

import java.util.Arrays;

/**
 * @author sfedosov on 10/23/18.
 */
public class PancakeSort {

    public static void main(String[] args) {
        int[] ints = new int[]{2, 3, 1, 5, 4};
        sortWithUsingReverseMethod(ints);
        assert Arrays.equals(ints, new int[]{1, 2, 3, 4, 5});
        ints = new int[]{1, 5, 4, 3, 2};
        sortWithUsingReverseMethod(ints);
        assert Arrays.equals(ints, new int[]{1, 2, 3, 4, 5});
    }

    private static void sortWithUsingReverseMethod(int[] input) {
        for (int i = input.length - 1; i >= 0; i--) {
            reverse(input, findIndexOfTheMaxFromRemainingArray(input, i));
            reverse(input, i);
        }
    }

    private static void reverse(int[] input, int k) {
        for (int i = 0; i < (k + 1) / 2; i++) {
            swap(i, k - i, input);
        }
    }

    private static void swap(int first, int second, int[] input) {
        int tmp = input[first];
        input[first] = input[second];
        input[second] = tmp;
    }

    private static int findIndexOfTheMaxFromRemainingArray(int[] input, int k) {
        int max = Integer.MIN_VALUE;
        int max_index = 0;
        for (int i = 0; i <= k; i++) {
            if (max < input[i]) {
                max_index = i;
                max = input[i];
            }
        }
        return max_index;
    }

}
