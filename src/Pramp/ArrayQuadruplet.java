package Pramp;

import java.util.*;

/**
 * @author sfedosov on 12/4/18.
 */
public class ArrayQuadruplet {

    static int[] findArrayQuadruplet(int[] arr, int s) {
        Arrays.sort(arr);
        int l, r;
        int n = arr.length;
        for (int i = 0; i <= n - 4; i++) {
            for (int j = i + 1; j <= n - 3; j++) {
                l = j + 1;
                r = n - 1;
                while (l < r) {
                    if (arr[i] + arr[j] + arr[l] + arr[r] == s) {
                        return new int[]{arr[i], arr[j], arr[l], arr[r]};
                    } else if (arr[i] + arr[j] + arr[l] + arr[r] < s)
                        l++;
                    else {
                        r--;
                    }
                }
            }
        }
        return new int[0];
    }

    public static void main(String[] args) {
        assert Arrays.equals(findArrayQuadruplet(new int[]{2, 7, 4, 0, 9, 5, 1, 3}, 20), new int[]{0, 4, 7, 9});
        assert Arrays.equals(findArrayQuadruplet(new int[]{4, 4, 4, 2}, 16), new int[0]);
        assert Arrays.equals(findArrayQuadruplet(new int[0], 16), new int[0]);
        assert Arrays.equals(findArrayQuadruplet(new int[]{1, 2, 3, 4, 5, 9, 19, 12, 12, 19}, 40), new int[]{4, 5, 12, 19});
        assert Arrays.equals(findArrayQuadruplet(new int[]{4, 4, 4, 4}, 16), new int[]{4, 4, 4, 4});
    }


}
