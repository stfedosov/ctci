package Pramp;

import Leetcode.TreeNode;

/**
 * @author sfedosov on 10/21/18.
 */
public class LargestSmallerBST {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(20);
        TreeNode treeNode9 = new TreeNode(9);
        root.left = treeNode9;
        treeNode9.left = new TreeNode(5);
        TreeNode treeNode12 = new TreeNode(12);
        treeNode9.right = treeNode12;
        treeNode12.left = new TreeNode(11);
        treeNode12.right = new TreeNode(14);
        root.right = new TreeNode(25);
        assert findLargestSmallerBST(root, 17) == 14;
    }

    private static int findLargestSmallerBST(TreeNode root, int num) {
        int result = -1;
        while (root != null) {
            if (root.val >= num) {
                root = root.left;
            } else {
                result = root.val;
                root = root.right;
            }
        }
        return result;
    }

}
