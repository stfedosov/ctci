package somecompany17;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Result {

    /*
     * Complete the 'maxValue' function below.
     *
     * The function is expected to return a LONG_INTEGER.
     * The function accepts following parameters:
     *  1. INTEGER n
     *  2. 2D_INTEGER_ARRAY rounds
     */
    public static long maxValueSlow(int n, List<List<Integer>> rounds) {
        long[] instruments = new long[n];
        long max = 0L;
        for (List<Integer> round : rounds) {
            for (int i = round.get(0) - 1; i <= round.get(1) - 1; i++) {
                instruments[i] += round.get(2);
            }
        }
        for (long instr : instruments) {
            max = Math.max(max, instr);
        }
        return max;
    }

    public static long maxValueFast(int n, List<List<Integer>> rounds) {
        // line sweep algorithm
        long max = 0L;
        List<List<Integer>> events = new ArrayList<>();
        for (List<Integer> round : rounds) {
            events.add(List.of(round.get(0) - 1, round.get(2)));
            events.add(List.of(round.get(1), -round.get(2)));
        }
        events.sort(Comparator.comparingInt(a -> a.get(0)));
        long cumulativeSum = 0L;
        for (int i = 0; i < n; i++) {
            cumulativeSum += events.get(i).get(1);
            max = Math.max(max, cumulativeSum);
        }
        return max;
    }

    public static void main(String[] args) throws IOException {
        System.out.println(maxValueSlow(5, List.of(List.of(1, 2, 10), List.of(2, 4, 5), List.of(3, 5, 12))));
        System.out.println(maxValueSlow(5, List.of(List.of(1, 2, 100), List.of(2, 5, 100), List.of(3, 4, 100))));
        System.out.println(maxValueFast(5, List.of(List.of(1, 2, 10), List.of(2, 4, 5), List.of(3, 5, 12))));
        System.out.println(maxValueFast(5, List.of(List.of(1, 2, 100), List.of(2, 5, 100), List.of(3, 4, 100))));
    }
}

