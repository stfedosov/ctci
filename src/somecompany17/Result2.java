package somecompany17;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Result2 {

    /*
#       Summary of the task:
#
#  In this task, you are given a set of words (wordSet)
# and a set of sentences (sentences). Each sentence is composed of words from the wordSet.
# Your goal is to determine how many new sentences can be created by rearranging
# the letters of each word in the input sentences,
# but only if the resulting word is also in the input wordSet.
#
# In other words, you need to count the number of sentences that
# can be formed by replacing words in the original sentences with their
# anagrams from the wordSet. The function should return a list of integers, where each integer denotes the number of
# sentences that can be formed from each input sentence.
#
#
# Import Counter class from collections module, used to count the occurrence of elements
from collections import Counter
     */

    public static void main(String[] args) {
        System.out.println(countSentences(List.of("listen", "silent", "it", "is"), List.of("listen it is silent")));

        // "silent it is silent"
        // "silent it is listen"
        // "listen it is listen"
        // + original (counted as an option)

        System.out.println(countSentences(List.of("the", "bats", "tabs", "in", "cat", "act"), List.of("cat the bats", "in the act", "act tabs in")));

        // "cat the bats" => "act the bats"
        // "cat the bats" => "cat the tabs"
        // "cat the bats" => "act the tabs"
        // + original (counted as an option)
    }

    private static Map<String, List<String>> countAnagrams(List<String> wordSet) {
        Map<String, List<String>> anagramGroups = new HashMap<>();
        for (String word : wordSet) {
            char[] charArray = word.toCharArray(); // Convert the word to a char array
            Arrays.sort(charArray);
            String key = new String(charArray);
            anagramGroups.computeIfAbsent(key, x -> new ArrayList<>()).add(word);
        }
        return anagramGroups;
    }

    private static int countSentencesHelper(String sentence, Map<String, List<String>> anagramGroups) {
        int count = 1; // no rearrangements of words is also counted as a valid choice
        var words = sentence.split("\\s+");
        for (String word : words) {
            char[] charArray = word.toCharArray();
            Arrays.sort(charArray);
            String key = new String(charArray);
            // Multiply the count by the number of anagrams for the key (if it exists)
            // because we need to calculate the total number of possible rearrangements of words with independent choices:
            // Example:
            // - If the first word has 3 anagrams, you have 3 options for that word.
            // - If the second word has 2 anagrams, you have 2 independent options for that word.
            // Together, you have 3 × 2 = 6 ways to form valid rearrangement
            if (anagramGroups.containsKey(key)) {
                count *= anagramGroups.get(key).size();
            }
        }
        return count;
    }

    public static List<Integer> countSentences(List<String> wordSet, List<String> sentences) {
        var anagramGroups = countAnagrams(wordSet); // Group anagrams in wordSet
        List<Integer> result = new ArrayList<>(); // Initialize a list to store the results
        for (String sentence : sentences) {
            result.add(countSentencesHelper(sentence, anagramGroups));
        }
        return result;
    }
}

