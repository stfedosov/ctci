package somecompany5;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Meals {

    static class Meal {
        String name;
        List<String> ingredients;

        public Meal(String name, List<String> ingredients) {
            this.name = name;
            this.ingredients = ingredients;
        }

        public String getName() {
            return this.name;
        }

        public List<String> getIngredients() {
            return this.ingredients;
        }
    }

    public static int getUniqueMealCount(List<Meal> meals) {
        if (meals == null) return 0;
        Set<Integer> set = new HashSet<>();
        for (Meal meal : meals) {
            set.add(Objects.hash(meal.ingredients));
        }
        return set.size();
    }

}
