package random;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author sfedosov on 11/27/18.
 */
public class CE {

    public static void main(String[] args) {
        // NOTE: The following input values are used for testing your solution.

        int[] array1A = {1, 3, 4, 6, 7, 9};
        int[] array2A = {1, 2, 4, 5, 9, 10};
        assert Arrays.toString(commonElements(array1A, array2A)).equals("[1, 4, 9]");

        int[] array1B = {1, 2, 9, 10, 11, 12};
        int[] array2B = {0, 1, 2, 3, 4, 5, 8, 9, 10, 12, 14, 15};
        assert Arrays.toString(commonElements(array1B, array2B)).equals("[1, 2, 9, 10, 12]");

        int[] array1C = {0, 1, 2, 3, 4, 5};
        int[] array2C = {6, 7, 8, 9, 10, 11};
        assert Arrays.toString(commonElements(array1C, array2C)).equals("[]");
    }

    // Implement your solution below.
    // NOTE: Remember to return an Integer array, not an int array.
    public static Integer[] commonElements(int[] array1, int[] array2) {
        int pointer1 = 0;
        int pointer2 = 0;
        List<Integer> list = new ArrayList<>();
        while (pointer1 < array1.length && pointer2 < array2.length) {
            if (array1[pointer1] == array2[pointer2]) {
                list.add(array1[pointer1]);
                pointer1++;
                pointer2++;
            } else if (array1[pointer1] > array2[pointer2]) {
                pointer2++;
            } else {
                pointer1++;
            }
        }
        return list.toArray(new Integer[0]);
    }

}
