package random;

/**
 * @author sfedosov on 12/12/18.
 */
public class SumTree {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(4);
        TreeNode left = new TreeNode(3);
        root.left = left;
        left.left = new TreeNode(1);
        left.right = new TreeNode(9);
        TreeNode two = new TreeNode(2);
        two.left = new TreeNode(6);
        two.right = new TreeNode(7);
        root.right = two;
        TreeNode result = getSumTree(root);
        //           4
        //         /   \
        //        3     2
        //       / \   / \
        //      1   9 6   7
        System.out.println(result);
    }

    private static TreeNode getSumTree(TreeNode root) {
        if (root == null) return null;
        TreeNode left = getSumTree(root.left);
        TreeNode right = getSumTree(root.right);
        if (left != null && right != null) {
            root.value += left.value + right.value;
        } else if (left != null) {
            root.value += left.value;
        } else if (right != null) {
            root.value += right.value;
        }
        return root;
    }


}
