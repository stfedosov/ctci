package random;

import java.util.Arrays;

public class PrintDiagonalElements {

    public static void main(String[] args) {
        int[][] matrix;
        matrix = new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        System.out.println(Arrays.toString(findDiagonalOrder(matrix)));
    }

    public static int[] findDiagonalOrder(int[][] matrix) {
        if (matrix.length == 0) return new int[0];

        int rows = matrix.length, columns = matrix[0].length;
        int[] nums = new int[rows * columns];

        int currentRow = 0, currentColumn = 0;
        boolean goUp = true;

        for (int i = 0; i < nums.length; i++) {
            if (goUp) {
                nums[i] = matrix[currentRow--][currentColumn++];

                // exceed the boundary
                if (currentRow < 0 || currentColumn > columns - 1) {
                    // return to the previous valid position
                    currentRow++;
                    currentColumn--;

                    // Going to the element right to it (same row, next column)
                    if (currentColumn < columns - 1) currentColumn++;
                    else currentRow++;

                    goUp = false;
                }
            } else {
                nums[i] = matrix[currentRow++][currentColumn--];

                // exceed the boundary
                if (currentRow > rows - 1 || currentColumn < 0) {
                    // return to the previous valid position
                    currentRow--;
                    currentColumn++;

                    // Going to the element below it (same column, next row)
                    if (currentRow < rows - 1) currentRow++;
                    else currentColumn++;

                    goUp = true;
                }
            }
        }
        return nums;
    }

}
