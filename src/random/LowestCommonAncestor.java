package random;

import java.util.HashMap;
import java.util.Map;

public class LowestCommonAncestor {
    public static void main(String[] args) {
        // NOTE: The following input values will be used for testing your solution.
        // The mapping we're going to use for constructing a tree.
        // For example, {0: [1, 2]} means that 0's left child is 1, and its right
        // child is 2.
        Map<Integer, int[]> mapping1 = new HashMap<>();
        int[] childrenA = {2, 8};
        int[] childrenB = {0, 4};
        int[] childrenC = {7, 9};
        int[] childrenD = {3, 5};
        mapping1.put(6, childrenA);
        mapping1.put(2, childrenB);
        mapping1.put(4, childrenD);
        mapping1.put(8, childrenC);

        TreeNode head1 = LCAHelper.createTree(mapping1, 6);
        // This tree is:
        // head1 = 0
        //        / \
        //       1   2
        //      /\   /\
        //     3  4 5  6

        System.out.println(lowestCommonAncestorBST(head1, new TreeNode(2), new TreeNode(8)));
//        System.out.println(lowestCommonAncestor(head1, new TreeNode(3), new TreeNode(1)).val);
//        System.out.println(lowestCommonAncestor(head1, new TreeNode(1), new TreeNode(4)).val);
//        System.out.println(lowestCommonAncestor(head1, new TreeNode(0), new TreeNode(5)).val);
    }

    // ------------------------------------------- LCA FOR BST ------------------------------------------

    // O(log N) on average
    public static TreeNode lowestCommonAncestorBST(TreeNode root, TreeNode p, TreeNode q) {
        // Value of p
        int pVal = p.value;
        // Value of q;
        int qVal = q.value;
        // Start from the root node of the tree
        TreeNode node = root;
        // Traverse the tree
        while (node != null) {
            // Value of ancestor/parent node.
            int parentVal = node.value;
            if (pVal > parentVal && qVal > parentVal) {
                // If both p and q are greater than parent
                node = node.right;
            } else if (pVal < parentVal && qVal < parentVal) {
                // If both p and q are lesser than parent
                node = node.left;
            } else {
                // We have found the split point, i.e. the LCA node.
                return node;
            }
        }
        return null;
    }

    // ---------------------------------------------- LCA REGULAR TREE ---------------------------------------------

    // Time complexity: O(N), space : O(H = log(N) on average)
    public static TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null || root.value == p.value || root.value == q.value) return root;
        TreeNode left = lowestCommonAncestor(root.left, p, q);
        TreeNode right = lowestCommonAncestor(root.right, p, q);
        if (left != null && right != null) return root;
        return left != null ? left : right;
    }


    // ------------------------------- LCA WHEN ROOT IS UNKNOWN BUT HAS PARENT REFERENCE ---------------------------

    static class Node {
        public int val;
        public Node left;
        public Node right;
        public Node parent;
    }

    public static Node lowestCommonAncestor(Node p, Node q) {
        Node a = p, b = q;
        while (a != b) {
            // when the fastest pointer becomes 'null', we reset it to start
            // from where the slower one began, which gives the faster pointer a chance to catch up
            // by traveling the same distance the slower one did
            a = a == null ? q : a.parent;
            b = b == null ? p : b.parent;
        }
        return a;
    }

    // ------------------------------------------------ LCA FOR DEEPEST LEAVES -------------------------------------
    static class Node2 {
        int depth;
        TreeNode lca;

        Node2(TreeNode lca, int d) {
            this.depth = d;
            this.lca = lca;
        }
    }

    public TreeNode lcaDeepestLeaves(TreeNode root) {
        Node2 rst = helper(root, 0);
        return rst.lca;
    }

    private Node2 helper(TreeNode node, int depth) {
        if (node == null) {
            return null;
        }
        if (node.left == null && node.right == null) {
            return new Node2(node, depth);
        }

        Node2 left = helper(node.left, depth + 1), right = helper(node.right, depth + 1);

        if (left == null) {
            return right;
        } else if (right == null || left.depth > right.depth) {
            return left;
        } else if (left.depth == right.depth) {
            return new Node2(node, left.depth);
        }
        return right;
    }


}
