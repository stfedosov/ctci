package random;

import java.util.HashMap;
import java.util.Map;

public class BuildPreorderInorderBinaryTree extends BuildBinaryTreeMain {


    public static TreeNode buildTree(int[] preorder, int[] inorder) {
        Map<Integer, Integer> inorderMap = new HashMap<>();
        for (int i = 0; i < inorder.length; i++) inorderMap.put(inorder[i], i);
        return helper(0, 0, inorder.length - 1, preorder, inorderMap);
    }

    public static TreeNode helper(int preStart, int inStart, int inEnd, int[] preorder, Map<Integer, Integer> inorderMap) {
        if (preStart >= preorder.length || inStart > inEnd) {
            return null;
        }
        TreeNode root = new TreeNode(preorder[preStart]);
        int inIndex = inorderMap.get(preorder[preStart]);
        int numOfNodesLeft = 1;
        int numOfNodesRight = inIndex - inStart + 1;
        root.left = helper(preStart + numOfNodesLeft, inStart, inIndex - 1, preorder, inorderMap);
        root.right = helper(preStart + numOfNodesRight, inIndex + 1, inEnd, preorder, inorderMap);
        return root;
    }

}
