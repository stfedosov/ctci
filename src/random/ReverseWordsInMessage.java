package random;

public class ReverseWordsInMessage {

    public static void main(String[] args) {
        String str = "I want to work at Amazon";
        String result = reverseWords("Amazon at work to want I");
        assert str.equals(result);
    }

    private static String reverseWords(String str) {
        char[] toReturn = reverseCharacters(str.toCharArray(), 0, str.length() - 1);
        int currentWordIndex = 0;
        for (int i = 0; i < str.length() + 1; i++) {
            if (i == str.length() || toReturn[i] == ' ') {
                reverseCharacters(toReturn, currentWordIndex, i - 1);
                currentWordIndex = i + 1;
            }
        }
        return new String(toReturn);
    }

    private static char[] reverseCharacters(char[] str, int start, int end) {
        while (start < end) {
            char tmp = str[start];
            str[start] = str[end];
            str[end] = tmp;
            start++;
            end--;
        }
        return str;
    }

}
