package random;

public class CalculatorSimplePlusAndMultiplier {

    /*
    Solve a mathematics expression using only * and + operators no parenthesis.
    For example:
    String input = "42+10*2+4*3+4";
    Output: 78
     */
    public static void main(String[] args) {
        System.out.println(solve("40+45*3+106"));
        System.out.println(solve("40+45*3*2+106"));
        System.out.println(solve("40+45*3*2+106*3"));
        System.out.println(solve("40*2*3+45*3*2+106*3"));
        System.out.println(solve("42+10*2+4*3+4"));
        System.out.println(solve("40*2"));
        System.out.println(solve("40*2*3+1"));
        System.out.println(solve("5+40*2"));
        System.out.println(solve("5+5"));
    }

    private static int solve(String input) {
        int num = 0, sum = 0, product = 1;
        char sign = '+';
        for (char c : input.toCharArray()) {
            if (Character.isDigit(c)) {
                num = num * 10 + (c - '0');
            } else if (c == '+') {
                if (sign == '*') {
                    product *= num;
                    sum += product;
                    product = 1;
                } else {
                    sum += num;
                }
                sign = '+';
                num = 0;
            } else if (c == '*') {
                sign = '*';
                product *= num;
                num = 0;
            }
        }
        return sign == '+' ? sum + num : sum + product * num;
    }

}
