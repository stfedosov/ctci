package random;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sfedosov on 10/30/19.
 */
public class MajorityElement2 {

    public static void main(String[] args) {
        System.out.println(majorityElementBruteForce(new int[]{1, 1, 1, 3, 3, 2, 2, 2}));
        System.out.println(majorityElementBruteForce(new int[]{3, 2, 3}));
    }

    public static List<Integer> majorityElementBruteForce(int[] nums) {
        List<Integer> list = new ArrayList<>();
        Map<Integer, Integer> map = new HashMap<>();
        for(int n : nums) {
            map.put(n, map.getOrDefault(n, 0) + 1);
            if (map.get(n) > nums.length / 3 && !list.contains(n)) list.add(n);
        }
        return list;
    }

}
