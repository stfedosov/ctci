package random;

import java.util.Arrays;

/**
 * @author sfedosov on 11/29/18.
 */
public class MS {

    public static void main(String[] args) {
        // NOTE: The following input values will be used for testing your solution.
        int[][] bombs1 = {{0, 2}, {2, 0}};
        assert Arrays.deepToString(mineSweeper(bombs1, 3, 3)).equals("[[0, 1, -1], [1, 2, 1], [-1, 1, 0]]");

        int[][] bombs2 = {{0, 0}, {0, 1}, {1, 2}};
        assert Arrays.deepToString(mineSweeper(bombs2, 3, 4)).equals("[[-1, -1, 2, 1], [2, 3, -1, 1], [0, 1, 1, 1]]");

        int[][] bombs3 = {{1, 1}, {1, 2}, {2, 2}, {4, 3}};
        assert Arrays.deepToString(mineSweeper(bombs3, 5, 5)).equals("[[1, 2, 2, 1, 0], [1, -1, -1, 2, 0], [1, 3, -1, 2, 0], [0, 1, 2, 2, 1], [0, 0, 1, -1, 1]]");
    }

    // Implement your solution below.
    public static int[][] mineSweeper(int[][] bombs, int numRows, int numCols) {
        int[][] field = new int[numRows][numCols];
        for (int[] bomb : bombs) {
            field[bomb[0]][bomb[1]] = -1;
        }
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                if (field[i][j] < 0) continue;
                field[i][j] += calc(field, i - 1, j);
                field[i][j] += calc(field, i - 1, j - 1);
                field[i][j] += calc(field, i, j - 1);
                field[i][j] += calc(field, i + 1, j - 1);
                field[i][j] += calc(field, i - 1, j + 1);
                field[i][j] += calc(field, i + 1, j);
                field[i][j] += calc(field, i + 1, j + 1);
                field[i][j] += calc(field, i, j + 1);
            }
        }
        return field;
    }

    private static int calc(int[][] field, int i, int j) {
        if (i < 0 || j < 0) return 0;
        if (i + 1 > field.length || j + 1 > field[0].length) return 0;
        return field[i][j] < 0 ? 1 : 0;
    }

}
