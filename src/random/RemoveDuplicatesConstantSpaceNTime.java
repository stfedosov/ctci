package random;

import java.util.Arrays;

/**
 * @author sfedosov on 12/10/18.
 */
public class RemoveDuplicatesConstantSpaceNTime {

    public static void main(String[] args) {
        assert doWork("fooolllooowup").equals("folwup");
    }

    private static String doWork(String string) {
        boolean[] seen = new boolean[26];
        char[] array = string.toCharArray();
        int readerPointer = 0;
        int writerPointer = 0;
        while (readerPointer != array.length) {
            char c = string.charAt(readerPointer);
            if (!seen[c - 'a']) {
                seen[c - 'a'] = true;
                array[writerPointer] = c;
                writerPointer++;
            }
            readerPointer++;
        }
        return new String(Arrays.copyOfRange(array, 0, writerPointer));
    }

}
