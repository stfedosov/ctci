package random;

/**
 * @author sfedosov on 11/15/18.
 */
public class GetLastNodeFromLinkedList {

    public static void main(String[] args) {
        LinkedListNode node1 = new LinkedListNode(1);
        LinkedListNode node2 = new LinkedListNode(2);
        LinkedListNode node3 = new LinkedListNode(3);
        LinkedListNode node4 = new LinkedListNode(4);
        LinkedListNode node5 = new LinkedListNode(5);
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        assert getLastNode(node1, 2) == 4;
        assert getLastNode(node1, 3) == 3;
        assert getLastNode(node1, 1) == 5;
    }

    public static int getLastNode(LinkedListNode head, int n) {
        LinkedListNode fast = head;
        LinkedListNode slow = head;
        int start = 1;
        while (fast.next != null) {
            fast = fast.next;
            start++;
            if (start > n) {
                slow = slow.next;
            }
        }
        return slow.value;
    }

}
