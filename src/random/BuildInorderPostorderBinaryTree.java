package random;

import java.util.HashMap;
import java.util.Map;

public class BuildInorderPostorderBinaryTree extends BuildBinaryTreeMain {

    /*
    - Find the root from postorder.
    - Find the index of root from inorder map
    - Find the number of elements in the left and right subtree using the index from the inorder map
    - Use this information to get the correct indices in the postorder array
                1
               / \
              2   3

              inorder -> left - root - right
              2 - 1 - 3

              postorder -> left - right - root
              2 - 3 - 1

        inorderStart = 0, inorderEnd = 2, postStart = 0, postEnd = 2
        [1] - root
        [1]'s index in inorder array is 1
        [1]'s left node
        inorderStart = 0, inorderEnd = 1 - 1 = 0, postStart = 0, postEnd = 0 + 1 - 0 - 1 = 0
        postorder[0] = 2
        [1]'s right node
        inorderStart = 1 + 1 = 2, inorderEnd = 2, postStart = 0 + 1 - 0 = 1, postEnd = 2 - 1 = 1
        postorder[1] = 3
     */
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        Map<Integer, Integer> inorderMap = new HashMap<>();
        for (int i = 0; i < inorder.length; i++) inorderMap.put(inorder[i], i);
        return helper(0, inorder.length - 1, postorder, 0, postorder.length - 1, inorderMap);
    }

    private TreeNode helper(int inorderStart,
                            int inorderEnd,
                            int[] postorder,
                            int postStart,
                            int postEnd,
                            Map<Integer, Integer> inorderMap) {
        if (postStart > postEnd || inorderStart > inorderEnd) {
            return null;
        }
        TreeNode root = new TreeNode(postorder[postEnd]);
        int postIndex = inorderMap.get(postorder[postEnd]);
        int numOfNodesLeft = postIndex - inorderStart - 1;
        int numOfNodesRight = postIndex - inorderStart;
        root.left = helper(inorderStart, postIndex - 1, postorder, postStart, postStart + numOfNodesLeft, inorderMap);
        root.right = helper(postIndex + 1, inorderEnd, postorder, postStart + numOfNodesRight, postEnd - 1, inorderMap);
        return root;
    }

}
