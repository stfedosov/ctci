package random;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @author sfedosov on 12/6/18.
 */
public class KthMostFrequentCharacter {

    public static void main(String[] args) {
        assert Objects.equals(kthMostFrequent(new String[]{"a", "b", "a", "c", "d"}, 0), "a");
        assert Objects.equals(kthMostFrequent(new String[]{"a", "b", "c", "a", "b", "a"}, 1), "b");
        assert Objects.equals(kthMostFrequent(new String[]{"a", "b", "c", "a", "b", "a"}, 2), "c");
        assert Objects.equals(kthMostFrequent(new String[]{"a", "b", "c", "a", "b", "a"}, 3), null);
    }

    public static String kthMostFrequent(String[] strings, int k) {
        HashMap<String, Integer> map = new HashMap<>();
        // O(n)
        for (String s : strings) {
            Integer x = map.get(s);
            if (x == null) x = 0;
            map.put(s, ++x);
        }

        Queue<Map.Entry<String, Integer>> queue = new PriorityQueue<>((o1, o2) -> {
            Integer v1 = (o1).getValue();
            Integer v2 = (o2).getValue();
            return v2.compareTo(v1);
        });
        int x = 0;
        // O(k)
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (x <= k) {
                queue.add(entry);
            } else {
                break;
            }
            x++;
        }
        // O(n * logk)
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (queue.peek().getValue() < entry.getValue()) {
                queue.poll();
                queue.add(entry);
            }
        }
        String toReturn = null;
        // O(k * logk)
        while (k >= 0) {
            Map.Entry<String, Integer> poll = queue.poll();
            if (poll == null) return null;
            toReturn = poll.getKey();
            k--;
        }
        return toReturn;
    }

    // O(n) + O(k) + O(n*logk) + O(k*logk) = O(n*logk)

}
