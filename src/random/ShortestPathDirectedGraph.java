package random;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 * @author sfedosov on 10/26/18.
 */
public class ShortestPathDirectedGraph {

    public static void main(String[] args) {
        Node node1 = new Node(1);
        Node node2 = new Node(2);
        node1.addChild(node2);
        Node node5 = new Node(5);
        Node node7 = new Node(7);
        Node node4 = new Node(4);
        node5.addChild(node7);
        node7.addChild(node4);
        node5.addChild(node4);
        node2.addChild(node5);
        Node node3 = new Node(3);
        node1.addChild(node3);
        node4.addChild(node3);
        node4.addChild(node1);
        List<Node> nodes = shortestPath(node5, node3);
        //  1 - 2 - 5 - 7
        //            \ |
        //             4
        //            / \
        //           1 - 3
        System.out.println(nodes); // 5 - 4 - 3
    }

    public static class Node {
        int value;
        public List<Node> children;

        // Basic constructor
        public Node(int value) {
            this.value = value;
        }

        // Lazily instantiate children list
        void addChild(Node n) {
            if (this.children == null) this.children = new LinkedList<>();
            this.children.add(n);
        }

        @Override
        public String toString() {
            return "NodeCopy{" +
                    "value=" + value +
                    '}';
        }
    }

    // Find the shortest path between two nodes using BFS
    private static List<Node> shortestPath(Node a, Node b) {
        // Return null if either node is null or if they're the same node
        if (a == null || b == null) return null;
        if (a == b) return null;

        // Using a queue for our BFS
        Queue<Node> toVisit = new LinkedList<>();

        // Track the parents so that we can reconstruct our path
        Map<Node, Node> parents = new HashMap<>();

        // Initialize the BFS
        toVisit.add(a);
        parents.put(a, null);

        // Keep going until we run out of nodes or reach our destination
        while (!toVisit.isEmpty()) {
            Node curr = toVisit.poll();

            // If we find the node we're looking for then we're done
            if (curr == b) break;

            // If the current node doesn't have children, skip it
            if (curr.children == null) continue;

            // Add all the children to the queue
            for (Node n : curr.children) {
                if (!parents.containsKey(n)) {
                    toVisit.add(n);
                    parents.put(n, curr);
                }
            }
        }

        // If we couldn't find a path, the destination node won't have been
        // added to our parents set
        if (parents.get(b) == null) return null;

        // Create the output list and add the path to the list
        List<Node> out = new LinkedList<>();
        Node n = b;
        while (n != null) {
            out.add(0, n);
            n = parents.get(n);
        }

        return out;
    }

}
