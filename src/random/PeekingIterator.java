package random;

import java.util.Iterator;

/**
 * @author sfedosov on 12/6/18.
 */

class PeekingIterator implements Iterator<Integer> {
    private final Iterator<Integer> iterator;
    private Integer temp;

    public PeekingIterator(Iterator<Integer> iterator) {
        this.iterator = iterator;
    }

    public Integer peek() {
        //if there is no peek, advance the iterator and store its value, return the peek otherwise
        if (temp == null) {
            temp = iterator.next();
        }
        return temp;
    }

    @Override
    public Integer next() {
        //if we already have a peek,return it and nullify it, otherwise do normal next()
        if (temp != null) {
            Integer result = temp;
            temp = null;
            return result;
        } else {
            return iterator.next();
        }
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext() || temp != null;
    }
}

// -----
// peeking iterator from Guava library
// -----
class PeekingImpl implements Iterator<Integer> {

    private final Iterator<Integer> iterator;
    private boolean hasPeeked;
    private Integer temp;

    public PeekingImpl(Iterator<Integer> iterator) {
        this.iterator = iterator;
    }

    @Override
    public boolean hasNext() {
        return hasPeeked || iterator.hasNext();
    }

    @Override
    public Integer next() {
        if (hasPeeked) {
            Integer result = temp;
            hasPeeked = false;
            temp = null;
            return result;
        } else {
            return iterator.next();
        }
    }

    @Override
    public void remove() {
        if (hasPeeked) throw new IllegalStateException("Can't remove after you've peeked at next");
        iterator.remove();
    }

    public Integer peek() {
        if (!hasPeeked) {
            temp = iterator.next();
            hasPeeked = true;
        }
        return temp;
    }
}
