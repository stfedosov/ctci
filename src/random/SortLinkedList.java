package random;

import Leetcode.RotateList.ListNode;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * @author sfedosov on 11/19/18.
 */
public class SortLinkedList {

    public static void main(String[] args) {
        ListNode listNode = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode3 = new ListNode(3);
        ListNode listNode4 = new ListNode(4);
        listNode4.next = listNode2;
        listNode2.next = listNode;
        listNode.next = listNode3;
        // 4->2->1->3
        System.out.println(sortList(listNode4)); // 1->2->3->4
    }

    public static ListNode sortList(ListNode head) {
        PriorityQueue<ListNode> queue = new PriorityQueue<>(Comparator.comparingInt(o -> o.val));
        ListNode current = head;
        while(current != null) {
            queue.add(current);
            current = current.next;
        }
        ListNode tmp = new ListNode(0);
        ListNode result = tmp;
        while (!queue.isEmpty()) {
            ListNode polled = queue.poll();
            polled.next = null;
            result.next = polled;
            result = result.next;
        }
        return tmp.next;
    }

}
