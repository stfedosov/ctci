package random;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Deque;

public class CanAttendMeetings {

    public static void main(String[] args) {
        assert !canAttendMeetings(new int[][]{{0, 30}, {5, 10}, {15, 20}});
        assert canAttendMeetings(new int[][]{{7, 10}, {2, 4}});
    }

    public static boolean canAttendMeetings(int[][] intervals) {
        Arrays.sort(intervals, Comparator.comparingInt(a -> a[0]));
        Deque<int[]> deque = new ArrayDeque<>();
        for (int[] interval : intervals) {
            if (deque.isEmpty() || deque.peek()[1] <= interval[0]) {
                deque.push(interval);
            } else {
                deque.pop();
                deque.push(interval);
            }
        }
        return deque.size() == intervals.length;
    }

}
