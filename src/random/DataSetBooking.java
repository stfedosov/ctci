package random;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DataSetBooking {

    public static final String CHECKIN = "checkin";
    public static final String CHECKOUT = "checkout";
    public static final String ROOMS = "rooms";
    public static final String FEATURES = "features";
    public static final String AVAILABILITY = "availability";
    public static final String PRICE = "price";
    private static final Map<Integer, List<Map<String, Object>>> dataSet = Map.of(
            176, List.of(
                    Map.of(
                            PRICE, 120,
                            FEATURES, List.of("breakfast", "refundable"),
                            AVAILABILITY, 5),
                    Map.of(
                            PRICE, 178,
                            FEATURES, List.of("wifi", "sauna"),
                            AVAILABILITY, 2)),
            177, List.of(
                    Map.of(
                            PRICE, 130,
                            FEATURES, List.of("breakfast"),
                            AVAILABILITY, 1),
                    Map.of(
                            PRICE, 140,
                            FEATURES, List.of("breakfast", "refundable", "wifi"),
                            AVAILABILITY, 3
                    )),
            178, List.of(
                    Map.of(
                            PRICE, 130,
                            FEATURES, List.of("breakfast"),
                            AVAILABILITY, 1),
                    Map.of(
                            PRICE, 140,
                            FEATURES, List.of("breakfast", "refundable", "wifi"),
                            AVAILABILITY, 3
                    ))

    );

    public static List<Map<String, Object>> getAllAvailableOptions(Map<String, Object> input) {
        checkInput(input);
        int checkin = Integer.parseInt(String.valueOf(input.get(CHECKIN))),
                checkout = Integer.parseInt(String.valueOf(input.get(CHECKOUT))),
                roomsRequired = Integer.parseInt(String.valueOf(input.get(ROOMS)));
        List<String> preferences = (List<String>) input.get(FEATURES);
        List<Map<String, Object>> result = new ArrayList<>();
        IntStream.range(checkin, checkout)
                .forEach(day -> pushToTheResult(filterOutProperties(roomsRequired, preferences, day), result));
        return result;
    }

    private static void pushToTheResult(List<Map<String, Object>> filtered, List<Map<String, Object>> result) {
        if (result.isEmpty()) {
            result.addAll(filtered);
        } else {
            List<Map<String, Object>> copy = new ArrayList<>(result);
            result.clear();
            filtered.forEach(newOption -> copy.forEach(prevOption -> {
                Map<String, Object> finalOption = new HashMap<>();
                prevOption.forEach((key, value) -> {
                    switch (key) {
                        case PRICE:
                            finalOption.put(PRICE, getTotalPriceValue(value, newOption.get(PRICE)));
                            break;
                        case FEATURES:
                            finalOption.put(FEATURES, getIntersectionOfFeatures(value, newOption.get(FEATURES)));
                            break;
                        case AVAILABILITY:
                            finalOption.put(AVAILABILITY, getLowestAvailability(value, newOption.get(AVAILABILITY)));
                            break;
                    }
                });
                result.add(finalOption);
            }));
        }
    }

    private static Object getLowestAvailability(Object o1, Object o2) {
        return Math.min(Integer.parseInt(String.valueOf(o1)), Integer.parseInt(String.valueOf(o2)));
    }

    private static Object getIntersectionOfFeatures(Object o1, Object o2) {
        List<String> existing = (List<String>) o1;
        List<String> newExisting = (List<String>) o2;
        return existing.stream().filter(newExisting::contains).collect(Collectors.toList());
    }

    static int getTotalPriceValue(Object o1, Object o2) {
        return Integer.parseInt(String.valueOf(o1)) + Integer.parseInt(String.valueOf(o2));
    }

    private static List<Map<String, Object>> filterOutProperties(int roomsRequired, List<String> preferences, int i) {
        return dataSet.get(i)
                .stream()
                .filter(entry -> Integer.parseInt(String.valueOf(entry.get(AVAILABILITY))) >= roomsRequired)
                .filter(entry -> !preferences.isEmpty() && ((List<String>) entry.get(FEATURES)).containsAll(preferences))
                .collect(Collectors.toList());
    }

    private static void checkInput(Map<String, Object> input) {
        if (!input.containsKey(CHECKIN) || !input.containsKey(CHECKOUT)) {
            throw new IllegalArgumentException("Please enter checkin and checkout");
        }
        if (!input.containsKey(ROOMS) || Integer.parseInt(String.valueOf(input.get(ROOMS))) == 0) {
            throw new IllegalArgumentException("Incorrect amount of rooms");
        }
    }

    public static void main(String[] args) {
        System.out.println(getAllAvailableOptions(
                Map.of(CHECKIN, 176,
                        CHECKOUT, 178,
                        FEATURES, List.of("breakfast"),
                        ROOMS, 1)));

        System.out.println(getAllAvailableOptions(
                Map.of(CHECKIN, 176,
                        CHECKOUT, 177,
                        FEATURES, List.of("wifi"),
                        ROOMS, 1)));
    }
}
