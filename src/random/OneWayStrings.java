package random;

/**
 * @author sfedosov on 11/27/18.
 */
public class OneWayStrings {

    public static void main(String[] args) {
        // NOTE: The following input values will be used for testing your solution.
        assert isOneAway("abcde", "abcd");  // should return true
        assert isOneAway("abde", "abcde");  // should return true
        assert isOneAway("a", "a");  // should return true
        assert isOneAway("abcdef", "abqdef");  // should return true
        assert isOneAway("abcdef", "abccef");  // should return true
        assert isOneAway("abcdef", "abcde");  // should return true
        assert !isOneAway("aaa", "abc");  // should return false
        assert !isOneAway("abcde", "abc");  // should return false
        assert !isOneAway("abc", "abcde");  // should return false
        assert !isOneAway("abc", "bcc");  // should return false
    }

    // Implement your solution below.
    public static Boolean isOneAway(String s1, String s2) {
        if (Math.abs(s1.length() - s2.length()) >= 2) {
            return false;
        }
        int i1 = 0;
        int i2 = 0;
        int count = 0;
        while (i1 < s1.length() && i2 < s2.length()) {
            if (s1.charAt(i1) != s2.charAt(i2)) {
                if (count == 1) {
                    return false;
                }
                if (s1.length() > s2.length()) {
                    i1++;
                } else if (s2.length() > s1.length()) {
                    i2++;
                } else {
                    i1++;
                    i2++;
                }
                count++;
            }
            i1++;
            i2++;
        }
        return count == 1;
    }

}
