package random;

import Leetcode.RotateList.ListNode;

import java.util.HashSet;
import java.util.Set;

/**
 * @author sfedosov on 1/22/19.
 */
public class FindALengthOfACircularList {

    public static void main(String[] args) {
        ListNode root = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        root.next = listNode2;
        ListNode listNode3 = new ListNode(3);
        listNode2.next = listNode3;
        ListNode listNode4 = new ListNode(4);
        listNode3.next = listNode4;
        ListNode listNode5 = new ListNode(5);
        listNode4.next = listNode5;
        ListNode listNode6 = new ListNode(6);
        listNode5.next = listNode6;
        ListNode listNode7 = new ListNode(7);
        listNode6.next = listNode7;
        listNode7.next = listNode3;
        // 1 -> 2 -> 3 --- 4
        //          /       \
        //         7 <- 6 <- 5
        System.out.println(findLength(root));
    }

    private static int findLength(ListNode root) {
        Set<Integer> set = new HashSet<>();
        ListNode current = root;
        while (true) {
            if (!set.contains(current.val)) {
                set.add(current.val);
            } else {
                break;
            }
            current = current.next;
        }
        return set.size();
    }

}
