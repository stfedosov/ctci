package random;

public class MathPow {

    public static void main(String[] args) {
        System.out.println(myPowIterative(-111, 3));
        System.out.println(myPowIterative(2, 4));
        System.out.println(myPowIterative(2, -2));
        System.out.println(myPowIterative(8.84372, -5));
    }

    static double myPowRecursive(double x, int n) {
        if (n == 0) return 1;
        if (n == 1) return x;
        if (n == -1) return 1 / x;
        double temp = myPowRecursive(x, n / 2);
        temp = n % 2 == 0 ? temp * temp : temp * temp * (n > 0 ? x : 1 / x);
        return temp;
    }

    public static double myPowIterative(double x, int n) {
        if (n == 0) return 1;
        long nLong = n; // to avoid overflow if n = Integer.MAX_VALUE
        if (nLong < 0) {
            nLong = -nLong;
            x = 1 / x;
        }
        double ans = 1;
        while (nLong > 0) {
            if (nLong % 2 != 0) ans *= x;
            x *= x;
            nLong >>= 1;
        }
        return ans;
    }

}
