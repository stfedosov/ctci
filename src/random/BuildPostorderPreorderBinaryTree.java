package random;

import java.util.HashMap;
import java.util.Map;

public class BuildPostorderPreorderBinaryTree extends BuildBinaryTreeMain {

    public static void main(String[] args) {

    }

    public TreeNode buildTree(int[] preorder, int[] postorder) {
        Map<Integer, Integer> postorderMap = new HashMap<>();
        for (int i = 0; i < postorder.length; i++) postorderMap.put(postorder[i], i);
        return helper(preorder, 0, preorder.length - 1, 0, postorder.length - 1, postorderMap);
    }

    //    1
    //   / \
    //  2   3
    //
    //  preorder -> root - left - right
    //  1 - 2 - 3
    //
    //  postorder -> left - right - root
    //  2 - 3 - 1

    private TreeNode helper(int[] preorder, int preStart, int preEnd, int postStart, int postEnd, Map<Integer, Integer> postorderMap) {
        if (preStart > preEnd) {
            return null;
        }

        // when we reach to array size of 1, we return that node since no child is present after that node
        if (preStart == preEnd) {
            return new TreeNode(preorder[preStart]);
        }

        TreeNode root = new TreeNode(preorder[preStart]);

        // postorder traverse first Left then Right and then Root
        // so we are taking advantage of that we are getting the index of root next element in postorder
        // (as it will be the upperbound for left subtree)
        int postIndex = postorderMap.get(preorder[preStart + 1]);
        int leftSubtreeSize = postIndex - postStart + 1;

        root.left = helper(preorder, preStart + 1, preStart + leftSubtreeSize, postStart, postIndex, postorderMap);
        root.right = helper(preorder, preStart + leftSubtreeSize + 1, preEnd, postIndex + 1, postEnd - 1, postorderMap);
        return root;
    }

}
