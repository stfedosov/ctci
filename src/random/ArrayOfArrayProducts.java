package random;

import java.util.Arrays;

/**
 * @author sfedosov on 11/15/18.
 */
public class ArrayOfArrayProducts {

    public static void main(String[] args) {
        assert Arrays.equals(findArrayOfProducts(new int[]{8, 2, 3, 4}), new int[]{24, 96, 64, 48});
    }

    private static int[] findArrayOfProducts(int[] input) {
        int product = 1;
        int[] result = new int[input.length];
        for (int i = 0; i < input.length; i++) {
            result[i] = product;
            product *= input[i];
        }
        product = 1;
        for (int j = input.length - 1; j >= 0; j--) {
            result[j] *= product;
            product *= input[j];
        }
        return result;
    }


}
