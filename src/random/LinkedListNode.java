package random;

/**
 * @author sfedosov on 11/15/18.
 */
class LinkedListNode {
    LinkedListNode next;
    LinkedListNode previous;
    int value;

    public LinkedListNode(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "{" +
                "next=" + next +
                ", previous=" + previous +
                ", value=" + value +
                '}';
    }
}
