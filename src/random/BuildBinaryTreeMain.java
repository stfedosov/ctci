package random;

public class BuildBinaryTreeMain {

    static String getPostorderString(TreeNode root) {
        if (root == null) return "";
        String res = getInorderString(root.left);
        res += getInorderString(root.right);
        res += root.value;
        return res;
    }

    static int[] convert(String input) {
        int[] res = new int[input.length()];
        for (int i = 0; i < input.length(); i++) {
            res[i] = Integer.parseInt("" + input.charAt(i));
        }
        return res;
    }

    static String getInorderString(TreeNode root) {
        if (root == null) return "";
        String res = getInorderString(root.left);
        res += root.value;
        res += getInorderString(root.right);
        return res;
    }

    static String getPreorderString(TreeNode root) {
        if (root == null) return "";
        String res = "" + root.value;
        res += getPreorderString(root.left);
        res += getPreorderString(root.right);
        return res;
    }

}
