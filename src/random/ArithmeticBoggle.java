package random;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 10/23/18.
 */
public class ArithmeticBoggle {

    public static void main(String[] args) {
        assert !arithmeticBoggle(1, new ArrayList<>(Arrays.asList(6, 3)));
        assert arithmeticBoggle(19, new ArrayList<>(Arrays.asList(1, 3, 6, 9, 5, 5)));
        assert arithmeticBoggle(0, new ArrayList<>());
        assert !arithmeticBoggle(43, new ArrayList<>());
        assert arithmeticBoggle(42, new ArrayList<>(Collections.singletonList(42)));
        assert !arithmeticBoggle(0, new ArrayList<>(Collections.singletonList(99)));

        assert !arithmeticBoggle2(1, new ArrayList<>(Arrays.asList(6, 3)));
        assert arithmeticBoggle2(19, new ArrayList<>(Arrays.asList(1, 3, 6, 9, 5, 5)));
        assert arithmeticBoggle2(0, new ArrayList<>());
        assert !arithmeticBoggle2(43, new ArrayList<>());
        assert arithmeticBoggle2(42, new ArrayList<>(Collections.singletonList(42)));
        assert !arithmeticBoggle2(0, new ArrayList<>(Collections.singletonList(99)));

        assert !arithmeticBoggle2Cached(1, new ArrayList<>(Arrays.asList(6, 3)));
        assert arithmeticBoggle2Cached(19, new ArrayList<>(Arrays.asList(1, 3, 6, 9, 5, 5)));
        assert arithmeticBoggle2Cached(0, new ArrayList<>());
        assert !arithmeticBoggle2Cached(43, new ArrayList<>());
        assert arithmeticBoggle2Cached(42, new ArrayList<>(Collections.singletonList(42)));
        assert !arithmeticBoggle2Cached(0, new ArrayList<>(Collections.singletonList(99)));
    }

    public static boolean arithmeticBoggle(int magicNumber, ArrayList<Integer> numbers) {
        if (numbers == null || numbers.size() == 0) {
            return magicNumber == 0;
        } else if (numbers.size() == 1) {
            return magicNumber == numbers.get(0);
        }
        boolean found = false;
        int i = 0;
        while (!found && i < (1 << numbers.size())) {
            int tmpResult = 0;
            for (int j = 0; j < numbers.size(); j++) {
                int num = numbers.get(j);
                if ((i & (1 << j)) > 0) {
                    num = -num;
                }
                tmpResult += num;
            }
            found = tmpResult == magicNumber;
            i++;
        }
        return found;
    }

    public static boolean arithmeticBoggle2(int T, ArrayList<Integer> nums) {
        return arithmeticBoggleRec(nums, T, 0, 0);
    }

    private static boolean arithmeticBoggleRec(ArrayList<Integer> nums, int T, int i, int sum) {
        if (i == nums.size()) {
            return sum == T;
        }
        return arithmeticBoggleRec(nums, T, i + 1, sum + nums.get(i)) || arithmeticBoggleRec(nums, T, i + 1, sum - nums.get(i));
    }

    public static boolean arithmeticBoggle2Cached(int T, ArrayList<Integer> nums) {
        Map<Integer, Map<Integer, Boolean>> cache = new HashMap<>();
        return arithmeticBoggleDP(nums, T, 0, 0, cache);
    }

    private static boolean arithmeticBoggleDP(ArrayList<Integer> nums, int T, int i, int sum, Map<Integer, Map<Integer, Boolean>> cache) {
        if (i == nums.size()) {
            return sum == T;
        }
        if (!cache.containsKey(i)) cache.put(i, new HashMap<>());
        Boolean cached = cache.get(i).get(sum);
        if (cached != null) return cached;
        boolean result =
                arithmeticBoggleDP(nums, T, i + 1, sum + nums.get(i), cache)
                        || arithmeticBoggleDP(nums, T, i + 1, sum - nums.get(i), cache);
        cache.get(i).put(sum, result);
        return result;
    }

}
