package random;

/**
 * @author sfedosov on 11/27/18.
 */
public class MajorityElement {

    public static void main(String[] args) {
        assert findElementWithConstantSpace(new int[]{1, 1, 1, 1, 2, 3}) == 1;
        assert findElementWithConstantSpace(new int[]{3, 2, 3}) == 3;
        assert findElementWithConstantSpace(new int[]{2, 1, 2, 3, 4, 2, 1, 2, 2}) == 2;
    }

    private static int findElementWithConstantSpace(int[] nums) {
        int major = nums[0];
        int count = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == major) count++;
            else count--;
            if (count == 0) {
                major = nums[i];
                count = 1;
            }
        }
        return major;
    }

}
