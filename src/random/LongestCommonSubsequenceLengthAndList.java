package random;

import java.util.Arrays;

public class LongestCommonSubsequenceLengthAndList {

    public static void main(String[] args) {
        System.out.println(findLength("ABAZDC", "BACBAD"));
        System.out.println(findLength("AGGTAB", "GXTXAYB"));
        System.out.println(findLength("aaaa", "aa"));
    }

    private static int findLength(String s1, String s2) {
        int[][] memo = new int[s1.length()][s2.length()];
        for (int[] ints : memo) {
            Arrays.fill(ints, -1);
        }
        return find(s1, s2, 0, 0, memo);
    }

    private static int find(String s1, String s2, int i, int j, int[][] memo) {
        if (i >= s1.length() || j >= s2.length()) return 0;
        if (memo[i][j] != -1) return memo[i][j];
        if (s1.charAt(i) == s2.charAt(j)) {
            memo[i][j] = 1 + find(s1, s2, i + 1, j + 1, memo);
        } else {
            memo[i][j] = Math.max(find(s1, s2, i + 1, j, memo), find(s1, s2, i, j + 1, memo));
        }
        return memo[i][j];
    }
}
