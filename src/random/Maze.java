package random;

import java.util.LinkedList;
import java.util.List;

/**
 * @author sfedosov on 11/19/18.
 */
public class Maze {

    public static void main(String[] args) {

        int[][] maze = {
                        {1, 0, 0, 0, 0},
                        {1, 0, 1, 1, 1},
                        {1, 1, 1, 0, 1},
                        {0, 0, 0, 0, 1}
                };


        boolean[][] visited = new boolean[maze.length][maze[0].length];

        List<int[]> path = new LinkedList<>();

        boolean found = pathFound(maze, visited, 0, 0, path);

        System.out.println("found?: " + found);
        if (found) {
            for (int[] elem : path) {
                System.out.println(elem[1] + "," + elem[0]);
            }
            System.out.println("0,0");
        }
    }

    private static boolean pathFound(int[][] maze, boolean[][] visited, int x, int y, List<int[]> path) {
        if (x < 0 || y < 0 || x >= maze.length || y >= maze[0].length || maze[x][y] != 1 || visited[x][y]) {
            return false;
        }
        if (x == maze.length - 1 && y == maze[0].length - 1) {
            return true;
        }
        visited[x][y] = true;
        if (pathFound(maze, visited, x + 1, y, path)) {
            path.add(new int[]{x + 1, y});
            return true;
        } else if (pathFound(maze, visited, x, y + 1, path)) {
            path.add(new int[]{x, y + 1});
            return true;
        } else if (pathFound(maze, visited, x - 1, y, path)) {
            path.add(new int[]{x - 1, y});
            return true;
        } else if (pathFound(maze, visited, x, y - 1, path)) {
            path.add(new int[]{x, y - 1});
            return true;
        }
        return false;
    }
}
