package random;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * @author sfedosov on 11/8/18.
 */
public class IntersectionWithoutDuplicates {

    public static void main(String[] args) {
        assert intersectTwoSortedArrays(Arrays.asList(2, 3, 3, 5, 7, 11), Arrays.asList(3, 3, 7, 15, 31)).equals(Arrays.asList(3, 7));
        assert intersectTwoNonSortedArrays(Arrays.asList(3, 2, 3, 7, 5, 11), Arrays.asList(3, 7, 3, 31, 15)).equals(Arrays.asList(3, 7));

        assert intersectTwoSortedArrays(null, Arrays.asList(3, 3, 7, 15, 31)).equals(Arrays.asList(3, 3, 7, 15, 31));
        assert intersectTwoNonSortedArrays(null, Arrays.asList(3, 3, 7, 15, 31)).equals(Arrays.asList(3, 3, 7, 15, 31));

        assert intersectTwoSortedArrays(Arrays.asList(3, 3, 7, 15, 31), null).equals(Arrays.asList(3, 3, 7, 15, 31));
        assert intersectTwoNonSortedArrays(Arrays.asList(3, 3, 7, 15, 31), null).equals(Arrays.asList(3, 3, 7, 15, 31));

        assert intersectTwoSortedArrays(null, null).equals(Collections.emptyList());
        assert intersectTwoNonSortedArrays(null, null).equals(Collections.emptyList());

        assert intersectTwoSortedArrays(Collections.emptyList(), Collections.emptyList()).equals(Collections.emptyList());
        assert intersectTwoNonSortedArrays(Collections.emptyList(), Collections.emptyList()).equals(Collections.emptyList());

        assert intersectTwoSortedArrays(Arrays.asList(0, 0, 0, 0), Arrays.asList(0, 0, 0)).equals(Collections.singletonList(0));
        assert intersectTwoNonSortedArrays(Arrays.asList(0, 0, 0, 0), Arrays.asList(0, 0, 0)).equals(Collections.singletonList(0));
    }

    public static List<Integer> intersectTwoSortedArrays(List<Integer> A, List<Integer> B) {
        if (A == null && B == null) {
            return Collections.emptyList();
        } else if (A == null || B == null) {
            return A == null ? B : A;
        }
        List<Integer> intersection = new ArrayList<>();
        int i = 0, j = 0;
        while (i < A.size() && j < B.size()) {
            if (Objects.equals(A.get(i), B.get(j)) && (i == 0 || !Objects.equals(A.get(i), A.get(i - 1)))) {
                intersection.add(A.get(i));
                i++;
                j++;
            } else if (A.get(i) < B.get(j)) {
                i++;
            } else {
                j++;
            }
        }
        return intersection;
    }

    public static List<Integer> intersectTwoNonSortedArrays(List<Integer> A, List<Integer> B) {
        if (A == null && B == null) {
            return Collections.emptyList();
        } else if (A == null || B == null) {
            return A == null ? B : A;
        }
        Set<Integer> set = new HashSet<>(A);
        set.retainAll(B);
        return new ArrayList<>(set);
    }

}
