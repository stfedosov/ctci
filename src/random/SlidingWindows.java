package random;

/**
 * @author sfedosov on 1/29/19.
 */
public class SlidingWindows {

    public static void main(String[] args) {
        System.out.println(sumOfK(new int[]{100, 200, 300, 400}, 2));
    }

    private static int sumOfK(int[] ints, int K) {
        int tmp_sum = 0;
        for (int i = 0; i < K; i++) {
            tmp_sum += ints[i];
        }
        int max = 0;
        for (int i = K; i < ints.length; i++) {
            tmp_sum += ints[i] - ints[i - K];
            max = Math.max(max, tmp_sum);
        }
        return max;
    }

}
