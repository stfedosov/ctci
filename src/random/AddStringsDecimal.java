package random;

public class AddStringsDecimal {

    /*
    Given 2 strings str1 and str2, which represent numbers in string,
    perform addition of the numbers and return the result as a string.

    Example 1: str1 = "123.52" and str2 = "11.2", output should be "134.72"
    Example 2: str1 = "110.75" and str2 = "9", output should be "119.75"
     */
    public static void main(String[] args) {
        //Example 1:
        String str1 = "123.52";
        String str2 = "11.2";
        String ans = addStringsDecimal(str1, str2);
        System.out.println(ans);

        //Example 2:
        str1 = "110.75";
        str2 = "999";
        ans = addStringsDecimal(str1, str2);
        System.out.println(ans);

        str1 = "1.23";
        str2 = "1.456661213123";
        ans = addStringsDecimal(str1, str2);
        System.out.println(ans);
    }

    // Time: O(Max (N, M)); N = str1 length, M = str2 length
    // Space: O(N + M)
    public static String addStringsDecimal(String str1, String str2) {

        String[] s1 = str1.split("\\."), s2 = str2.split("\\.");

        StringBuilder sb = new StringBuilder();

        // step 1. calculate decimal points after .
        StringBuilder dec1 = new StringBuilder(s1.length > 1 ? s1[1] : "0"),
                      dec2 = new StringBuilder(s2.length > 1 ? s2[1] : "0");
        while (dec1.length() != dec2.length()) {
            if (dec1.length() < dec2.length()) {
                dec1.append("0");
            } else {
                dec2.append("0");
            }
        }
        int carry = performAddition(dec1.toString(), dec2.toString(), sb, 0);

        sb.append(".");

        // Step 2. Calculate Number before decimal point.
        carry = performAddition(s1[0], s2[0], sb, carry);
        if (carry != 0) {
            sb.append(carry);
        }
        return sb.reverse().toString();
    }

    private static int performAddition(String s1, String s2, StringBuilder sb, int carry) {
        int i = s1.length() - 1, j = s2.length() - 1;
        while (i >= 0 || j >= 0) {
            int sum = carry;

            if (j >= 0) {
                sum += s2.charAt(j--) - '0';
            }
            if (i >= 0) {
                sum += s1.charAt(i--) - '0';
            }
            carry = sum / 10;
            sb.append(sum % 10);
        }
        return carry;
    }

}
