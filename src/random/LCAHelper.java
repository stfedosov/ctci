package random;

import java.util.HashMap;
import java.util.Map;

public class LCAHelper {
    // A function for creating a tree.
    // Input:
    // - mapping: a node-to-node mapping that shows how the tree should be constructed
    // - headValue: the value that will be used for the head node
    // Output:
    // - The head node of the resulting tree
    public static TreeNode createTree(Map<Integer, int[]> mapping, int headValue) {
        TreeNode head = new TreeNode(headValue, null, null);
        HashMap<Integer, TreeNode> nodes = new HashMap<>();
        nodes.put(headValue, head);
        for (Integer key : mapping.keySet()) {
            int[] value = mapping.get(key);
            TreeNode leftChild = new TreeNode(value[0], null, null);
            TreeNode rightChild = new TreeNode(value[1], null, null);
            nodes.put(value[0], leftChild);
            nodes.put(value[1], rightChild);
        }
        for (Integer key : mapping.keySet()) {
            int[] value = mapping.get(key);
            if (nodes.get(key) != null) {
                nodes.get(key).left = nodes.get(value[0]);
                nodes.get(key).right = nodes.get(value[1]);
            }
        }
        return head;
    }
}