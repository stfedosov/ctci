package Chapter7;

import java.util.Iterator;

/**
 * @author sfedosov on 6/12/17.
 */
class CircullarArray<T> implements Iterable<T> {
    private T[] field;
    private int head = 0;
    private int size;
    private boolean rotate = false;

    private CircullarArray() {
        this.field = null;
        this.size = 0;
    }

    @SuppressWarnings("unchecked")
    public CircullarArray(int size) {
        if (field == null) {
            this.size = size;
            this.field = (T[]) new Object[size];
        }
    }

    public void add(T object) {
        if (field == null) {
            throw new IndexOutOfBoundsException("Array is empty! Please call constructor with specified size of the array!");
        }
        if (object == null) {
            throw new NullPointerException("Array will not accept null objects!");
        }
        if (head < size) {
            field[head++] = object;
        } else {
            throw new IndexOutOfBoundsException("Too much elements!");
        }
    }

    void rotate() {
        this.rotate = !rotate;
    }

    private int getIndex(int i) {
        if (rotate) {
            return size - 1 - i;
        } else {
            return i;
        }
    }

    T get(int i) {
        if (i < 0 || field == null || i >= size) {
            throw new IndexOutOfBoundsException("Incorrect number or array haven't been initialized!");
        }
        return field[getIndex(i)];
    }

    public int length() {
        return field.length;
    }

    void remove() {
        field[getIndex(0)] = null;
    }

    @Override
    public Iterator<T> iterator() {
        return new CircullarArrayIterator<>(this);
    }

    private class CircullarArrayIterator<T> implements Iterator<T> {

        private CircullarArray<T> circullarArray;
        private int i;

        CircullarArrayIterator(CircullarArray<T> circullarArray) {
            this.circullarArray = circullarArray;
        }

        @Override
        public boolean hasNext() {
            return i < circullarArray.length();
        }

        @Override
        public T next() {
            T t = circullarArray.get(i);
            i++;
            return t;
        }

        @Override
        public void remove() {
            circullarArray.remove();
        }
    }
}
