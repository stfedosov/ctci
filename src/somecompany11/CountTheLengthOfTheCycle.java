package somecompany11;

import java.util.HashMap;
import java.util.Map;

public class CountTheLengthOfTheCycle {

    /**
     * lengthOfCycle(arr, startIndex)
     * <p>
     * You are given an integer array of size N.
     * Every element of the array is greater than or equal to 0.
     * Starting from arr[startIndex], follow each element to the index it points to.
     * Continue to do this until you find a cycle.
     * Return the length of the cycle. If no cycle is found return -1
     * <p>
     * Examples:
     * lengthOfCycle([1, 0], 0) == 2
     * lengthOfCycle([1, 2, 0], 0) == 3
     */
    public static void main(String[] args) {
        assert lengthOfCycle(new int[]{1, 2, 0}, 0) == 3;
        assert lengthOfCycle(new int[]{1, 0}, 0) == 2;
        assert lengthOfCycle(new int[]{1, 2, 3, 1}, 0) == 3;
        assert lengthOfCycle(new int[]{1, 2, 3, 0}, 0) == 4;
        assert lengthOfCycle(new int[]{2, 3, 0}, 0) == 2;


        assert findCircle(new int[]{1, 2, 0}, 0) == 3;
        assert findCircle(new int[]{1, 0}, 0) == 2;
        assert findCircle(new int[]{1, 2, 3, 1}, 0) == 3;
        assert findCircle(new int[]{1, 2, 3, 0}, 0) == 4;
        assert findCircle(new int[]{2, 3, 0}, 0) == 2;

        assert lengthOfCycleFloydTortoiseAndHare(new int[]{1, 2, 0}, 0) == 3;
        assert lengthOfCycleFloydTortoiseAndHare(new int[]{1, 0}, 0) == 2;
        assert lengthOfCycleFloydTortoiseAndHare(new int[]{1, 2, 3, 1}, 0) == 3;
        assert lengthOfCycleFloydTortoiseAndHare(new int[]{1, 2, 3, 0}, 0) == 4;
        assert lengthOfCycleFloydTortoiseAndHare(new int[]{2, 3, 0}, 0) == 2;

        System.out.println(lengthOfCycleFloydTortoiseAndHare(new int[]{2, 5, 8, 6, 8, 3, 9, 8, 1, 7}, 0));
        System.out.println(lengthOfCycle(new int[]{2, 5, 8, 6, 8, 3, 9, 8, 1, 7}, 0));
        System.out.println(findCircle(new int[]{2, 5, 8, 6, 8, 3, 9, 8, 1, 7}, 0));
    }

    public static int lengthOfCycle(int[] arr, int startInd) {
        if (startInd < 0 || startInd >= arr.length) {
            return -1;
        }
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0, -1);
        int currentInd = startInd, idx = 0;
        while (currentInd < arr.length) {
            if (map.containsKey(arr[currentInd])) {
                return idx - map.get(arr[currentInd]);
            }
            map.put(arr[currentInd], idx++);
            currentInd = arr[currentInd];
        }
        return -1;
    }

    private static int findCircle(int[] nums, int start) {
        int s = nums[start];
        int f = nums[nums[start]];
        int sSoFar = 1;
        if (s == f)
            return -1;
        while (s != f) {
            s = nums[s];
            sSoFar++;
            f = nums[nums[f]];
        }
        return sSoFar;
    }

    // ---- another implementation using Floyd's Tortoise and Hare
    public static int lengthOfCycleFloydTortoiseAndHare(int[] arr, int startInd) {
        if (startInd < 0 || startInd >= arr.length) {
            return -1;
        }
        int slow = arr[startInd];
        int fast = arr[arr[startInd]];
        // trying to detect a cycle at first
        while (slow != fast) {
            // out of bounds - no cycle
            if (fast >= arr.length) {
                return -1;
            }
            slow = arr[slow];
            fast = arr[arr[fast]];
        }
        // yes, there is a cycle for sure, we need to find the starting point
        fast = 0;
        while (fast != slow) {
            fast = arr[fast];
            slow = arr[slow];
        }
        // okay, we've got to the starting point, so move away from it and count the length of the cycle
        int length = 1;
        slow = arr[slow];
        while (fast != slow) {
            length++;
            slow = arr[slow];
        }
        return length;
    }

}
