package somecompany4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Find3ElementsWithSumEqualTo {

    public static void main(String[] args) {
        assert findElements(
                new int[]{1, 2, 3, 4, 5},
                new int[]{2, 3, 6, 1, 2},
                new int[]{3, 2, 4, 5, 6},
                9).equals(Arrays.asList(
                "300",
                "401",
                "202",
                "103",
                "004",
                "210",
                "311",
                "112",
                "013",
                "021",
                "430",
                "332",
                "233",
                "134",
                "340",
                "441",
                "242",
                "143",
                "044"));
    }

    private static List<String> findElements(int[] a, int[] b, int[] c, int num) {
        List<String> result = new ArrayList<>();
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < a.length; i++) {
            map.put(a[i], i);
        }
        for (int j = 0; j < b.length; j++) {
            for (int k = 0; k < c.length; k++) {
                Integer position = map.get(num - b[j] - c[k]);
                if (position != null) {
                    result.add("" + position + j + k);
                }
            }
        }
        return result;
    }


}
