package somecompany4;

import Chapter3.Stack;

public class ReversePolishNotation {

    public static void main(String[] args) {
        assert evalRPN(new String[]{"2", "1", "+", "3", "*"}) == 9;
        assert evalRPN(new String[]{"4", "13", "5", "/", "+"}) == 6;
        assert evalRPN(new String[]{"10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"}) == 22;
    }

    private static int evalRPN(String[] tokens) {
        Stack<Integer> stack = new Stack<>();
        for (String token : tokens) {
            if (isOperation(token)) {
                stack.push(handle(stack, token));
            } else {
                stack.push(Integer.parseInt(token));
            }
        }
        return stack.pop();
    }

    private static boolean isOperation(String token) {
        return token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/");
    }

    private static int handle(Stack<Integer> stack, String operation) {
        int first = stack.pop();
        int second = stack.pop();
        int result = 0;
        switch (operation) {
            case "+":
                result = first + second;
                break;
            case "-":
                result = second - first;
                break;
            case "*":
                result = first * second;
                break;
            case "/":
                result = second / first;
                break;
        }
        return result;
    }

}
