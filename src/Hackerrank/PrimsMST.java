package Hackerrank;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

/**
 * @author sfedosov on 8/17/17.
 *         <p>
 *         Minimum spanning tree problem
 */
public class PrimsMST {

    public static void main(String[] args) {
        final Scanner in = new Scanner(System.in);
        in.nextInt();
        final int M = in.nextInt();
        final Edge[] edges = new Edge[M];
        for (int i = 0; i < edges.length; i++) {
            edges[i] = new Edge(
                    in.nextInt(),  // from
                    in.nextInt(),  // to
                    in.nextInt()); // weight
        }
        final int root = in.nextInt();
        final Graph graph = new Graph(edges);
        prim(graph, root);
    }

    private static void prim(final Graph graph, final int root) {

        final Map<Integer, Integer> cheapestCost = new HashMap<>();

        initialize(graph, cheapestCost);
        cheapestCost.put(root, 0);

        final Set<Integer> verticesQueue = new HashSet<>(graph.graph.keySet());
        int min = 0;
        while (!verticesQueue.isEmpty()) {
            final Pair minimumWeight = findVertexWithMinWeight(verticesQueue, cheapestCost);
            final int u = minimumWeight.getKey();
            min += minimumWeight.getValue();
            verticesQueue.remove(u);

            final Vertex vertex = graph.graph.get(u);

            for (final Entry<Vertex, Integer> entry : vertex.neighbours.entrySet()) {
                if (verticesQueue.contains(entry.getKey().name)
                        && entry.getValue() < cheapestCost.get(entry.getKey().name)) {
                    cheapestCost.put(entry.getKey().name, entry.getValue());
                }
            }

        }
        System.out.println(min);
    }

    private static void initialize(final Graph graph,
                                   final Map<Integer, Integer> cheapestCost) {
        for (final int c : graph.graph.keySet()) {
            cheapestCost.put(c, Integer.MAX_VALUE);
        }
    }

    private static Pair findVertexWithMinWeight(final Set<Integer> verticesQueue,
                                                final Map<Integer, Integer> cheapestCost) {
        int minimum = Integer.MAX_VALUE;
        int minimumVertex = 0;
        for (final Entry<Integer, Integer> entry : cheapestCost.entrySet()) {
            if (verticesQueue.contains(entry.getKey()) && entry.getValue() < minimum) {
                minimum = entry.getValue();
                minimumVertex = entry.getKey();
            }
        }
        return new Pair(minimumVertex, minimum);
    }

    private final static class Pair {
        private final int n1;
        private final int n2;

        Pair(int n1, int n2) {
            this.n1 = n1;
            this.n2 = n2;
        }

        int getKey() {
            return n1;
        }

        int getValue() {
            return n2;
        }
    }

    final static class Vertex {
        final int name;
        final Map<Vertex, Integer> neighbours = new HashMap<>();

        Vertex(final int name) {
            this.name = name;
        }
    }

    final static class Edge {
        final int source, destination;
        int weight = Integer.MAX_VALUE;

        Edge(final int source,
             final int destination,
             final int weight) {
            this.source = source;
            this.destination = destination;
            this.weight = weight;
        }
    }

    final static class Graph {
        final Map<Integer, Vertex> graph;

        Graph(final Edge[] edges) {
            this.graph = new HashMap<>(edges.length);

            for (final Edge e : edges) {
                if (!this.graph.containsKey(e.source)) {
                    this.graph.put(e.source, new Vertex(e.source));
                }
                if (!this.graph.containsKey(e.destination)) {
                    this.graph.put(e.destination, new Vertex(e.destination));
                }
            }

            for (final Edge e : edges) {
                this.graph.get(e.source).neighbours.put(this.graph.get(e.destination), e.weight);
                this.graph.get(e.destination).neighbours.put(this.graph.get(e.source), e.weight);
            }
        }
    }

}
