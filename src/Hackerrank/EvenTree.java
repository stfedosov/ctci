package Hackerrank;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * @author sfedosov on 8/13/17.
 */
public class EvenTree {

    public static void main(String[] args) {
        final Map<Integer, Node> map = readInputAndReturnTree();
        System.out.println(findBFS(getRoot(map)));
    }

    private static int findBFS(Node node) {
        final LinkedList<Node> nextToVisit = new LinkedList<>();
        final Set<Integer> visited = new HashSet<>();
        nextToVisit.add(node);
        int k = 0;
        while (!nextToVisit.isEmpty()) {
            Node node1 = nextToVisit.pop();
            if (visited.contains(node1.name)) {
                continue;
            }
            visited.add(node1.name);
            List<Node> adjacents = node1.getAdjacents();
            if (adjacents != null) {
                for (Node n : adjacents) {
                    if ((numNodes(n) + 1) % 2 == 0) {
                        k++;
                    }
                }
                nextToVisit.addAll(adjacents);
            }
        }
        return k;
    }

    private static Map<Integer, Node> readInputAndReturnTree() {
        Scanner in = new Scanner(System.in);

        int N, M;
        N = in.nextInt();
        M = in.nextInt();

        final Map<Integer, Node> map = new HashMap<>();

        for (int i = 0; i < M; i++) {
            int a = in.nextInt();
            int b = in.nextInt();
            if (!map.containsKey(a)) {
                map.put(a, new Node(a));
            }
            if (!map.containsKey(b)) {
                map.put(b, new Node(b));
            }
            map.get(b).addAdjacent(map.get(a));
        }
        return map;
    }

    private static int numNodes(Node root) {
        final LinkedList<Node> nextToVisit = new LinkedList<>();
        final Set<Integer> visited = new HashSet<>();
        nextToVisit.add(root);
        int k = 0;
        while (!nextToVisit.isEmpty()) {
            Node node1 = nextToVisit.pop();
            if (visited.contains(node1.name)) {
                continue;
            }
            visited.add(node1.name);
            List<Node> adjacents = node1.getAdjacents();
            if (adjacents != null) {
                k += adjacents.size();
                nextToVisit.addAll(adjacents);
            }
        }
        return k;
    }

    private static Node getRoot(final Map<Integer, Node> map) {
        Integer root = null;
        int maxAdjacent = 0;
        for (Map.Entry<Integer, Node> entry : map.entrySet()) {
            List<Node> adjacents = entry.getValue().getAdjacents();
            int tmp = adjacents == null ? 0 : adjacents.size();
            if (tmp > maxAdjacent) {
                maxAdjacent = tmp;
                root = entry.getKey();
            }
        }
        return map.get(root);
    }

    static class Node {
        private int name;
        private List<Node> adjacents;

        Node(int name) {
            this.name = name;
        }

        List<Node> getAdjacents() {
            return adjacents;
        }

        void addAdjacent(Node node) {
            if (adjacents == null) {
                adjacents = new ArrayList<>();
            }
            adjacents.add(node);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Node node = (Node) o;

            return name == node.name;
        }

        @Override
        public int hashCode() {
            return name;
        }

        @Override
        public String toString() {
            final int[] nodes;
            if (adjacents != null) {
                nodes = new int[adjacents.size()];
                int i = 0;
                for (Node node : adjacents) {
                    nodes[i] = node.name;
                    i++;
                }
            } else {
                nodes = new int[0];
            }
            return "NodeCopy{" +
                    "name=" + name +
                    ", adjacents=" + Arrays.toString(nodes) +
                    "}\n";
        }
    }

}
