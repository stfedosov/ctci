package Hackerrank;

import java.util.Arrays;

/**
 * @author sfedosov on 9/13/18.
 */
public class IntegerComplementInDecimalRepresentation {

    public static void main(String[] args) {
        assert getIntegerComplement(50) == 13;
        assert getIntegerComplement(100) == 27;
    }

    private static int getIntegerComplement(int n) {
        int bin[] = new int[32];
        int i = 0;
        int highestOrderBitPosition = 0;
        while (n > 0) {
            int remainder = n % 2;
            if (remainder == 1) {
                highestOrderBitPosition = Math.max(highestOrderBitPosition, i);
            }
            bin[i++] = remainder;
            n = n / 2;
        }
        bin = Arrays.copyOfRange(bin, 0, highestOrderBitPosition + 1);
        int result = 0;
        for (int k = 0; k < bin.length; k++) {
            result += (bin[k] == 1 ? 0 : 1) * Math.pow(2, k);
        }
        return result;
    }

}
