package Hackerrank;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author sfedosov on 9/19/17.
 */
public class ClosestNumbers {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int m = in.nextInt();
        long[] longs = new long[m];
        for (int i = 0; i < longs.length; i++) {
            longs[i] = in.nextLong();
        }
        Arrays.sort(longs);
        long min = Long.MAX_VALUE;
        for (int i = 1; i < longs.length; i++) {
            long l = longs[i] - longs[i - 1];
            if (min > l) {
                min = l;
            }
        }
        long[] longs2 = new long[longs.length];
        for (int i = 1; i < longs.length; i++) {
            long l = longs[i] - longs[i - 1];
            if (min == l) {
                longs2[i] = longs[i];
                longs2[i - 1] = longs[i - 1];
            }
        }
        Arrays.sort(longs2);
        for (long l : longs2) {
            if (l != 0) {
                System.out.print(l + " ");
            }
        }
    }

}
