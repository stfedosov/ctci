package Hackerrank;

import java.util.Scanner;

/**
 * @author sfedosov on 8/23/17.
 */
public class CoinChange {

    private static long getWays(long n, long[] c) {
        return count(n, c, c.length, new long[(int) (n + 1)][c.length + 1]);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        long[] c = new long[m];
        for (int c_i = 0; c_i < m; c_i++) {
            c[c_i] = in.nextLong();
        }
        // Print the number of ways of making change for 'n' units using coins having the values given by 'c'
        long ways = getWays(n, c);
        System.out.println(ways);
    }

    private static long count(long sum,
                              long[] coins,
                              int numberOfCoins,
                              long[][] memo) {
        if (sum == 0) {
            return 1;
        } else if (sum < 0) {
            return 0;
        } else if (numberOfCoins <= 0) {
            return 0;
        } else {
            memo[(int) sum][numberOfCoins] = count(sum, coins, numberOfCoins - 1, memo)
                    + count(sum - coins[numberOfCoins - 1], coins, numberOfCoins, memo);
            return memo[(int) sum][numberOfCoins];
        }
    }

    public int change(int amount, int[] coins) {
        return countWays(amount, coins.length, coins);
    }

    private int countWays(int amount, int numberOfCoins, int[] coins) {
        if (amount == 0) {
            return 1;
        } else if (amount < 0 || numberOfCoins <= 0) {
            return 0;
        } else {
            return countWays(amount, numberOfCoins - 1, coins) +
                    countWays(amount - coins[numberOfCoins - 1], numberOfCoins, coins);
        }
    }

}
