package Hackerrank;

import java.util.Arrays;

/**
 * @author sfedosov on 10/13/17.
 */
public class MinIntHeap {

    private int capacity = 100;
    protected int size = 0;

    protected int[] items = new int[capacity];

    protected void swap(int fpos, int spos) {
        int tmp = items[fpos];
        items[fpos] = items[spos];
        items[spos] = tmp;
    }

    protected int getLeftChildIndex(int parentIndex) {
        return 2 * parentIndex + 1;
    }

    protected int getRightChildIndex(int parentIndex) {
        return 2 * parentIndex + 2;
    }

    protected int getParentIndex(int childIndex) {
        return (childIndex - 1) / 2;
    }

    protected boolean hasLeftChild(int index) {
        return getLeftChildIndex(index) < size;
    }

    protected boolean hasRightChild(int index) {
        return getRightChildIndex(index) < size;
    }

    protected boolean hasParent(int index) {
        return getParentIndex(index) >= 0;
    }

    protected int parent(int index) {
        return items[getParentIndex(index)];
    }

    protected int rightChild(int index) {
        return items[getRightChildIndex(index)];
    }

    protected int leftChild(int index) {
        return items[getLeftChildIndex(index)];
    }

    public int peek() {
        if (size == 0) {
            throw new IllegalStateException();
        }
        return items[0];
    }

    public int poll() {
        if (size == 0) {
            throw new IllegalStateException();
        }
        int item = items[0];
        items[0] = items[size - 1];
        size--;
        heapifyDown();
        return item;
    }

    public void add(int element) {
        ensureCapacity();
        items[size] = element;
        size++;
        heapifyUp();
    }

    protected void heapifyUp() {
        int index = size - 1;
        while (hasParent(index) && parent(index) > items[index]) {
            swap(getParentIndex(index), index);
            index = getParentIndex(index);
        }
    }

    protected void heapifyDown() {
        int index = 0;
        while (hasLeftChild(index)) {
            int smallerChildIndex = getLeftChildIndex(index);
            if (hasRightChild(index) && rightChild(index) < leftChild(index)) {
                smallerChildIndex = getRightChildIndex(index);
            }
            if (items[index] < items[smallerChildIndex]) {
                break;
            } else {
                swap(index, smallerChildIndex);
            }
            index = smallerChildIndex;
        }
    }

    private void ensureCapacity() {
        if (size == capacity) {
            capacity = capacity * 2;
            items = Arrays.copyOf(items, capacity);
        }
    }

}
