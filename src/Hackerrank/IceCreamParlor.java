package Hackerrank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * @author sfedosov on 9/16/17.
 */
public class IceCreamParlor {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int queries = scanner.nextInt();
        int[][] answers = new int[queries][];
        int i = 0;
        while (i != queries) {
            int amountOfMoney = scanner.nextInt();
            int lengthOfFlavors = scanner.nextInt();
            int[] flavors = new int[lengthOfFlavors];
            for (int m = 0; m < flavors.length; m++) {
                flavors[m] = scanner.nextInt();
            }
            count(amountOfMoney, flavors, answers, i);
            i++;
        }
        for (int[] answer : answers) {
            for (int anAnswer : answer) {
                System.out.print(anAnswer + " ");
            }
            System.out.println();
        }
    }

    private static void count(int amountOfMoney, int[] flavors, int[][] answers, int i) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        for (int j = 0; j < flavors.length; j++) {
            if (map.containsKey(flavors[j])) {
                map.get(flavors[j]).add(j);
                break;
            }
            List<Integer> list = new ArrayList<>();
            list.add(j);
            map.put(amountOfMoney - flavors[j], list);
        }
        for (Map.Entry<Integer, List<Integer>> entry : map.entrySet()) {
            if (entry.getValue().size() == 2) {
                answers[i] = new int[2];
                for (int x = 0; x < 2; x++) {
                    answers[i][x] = entry.getValue().get(x) + 1;
                }
                break;
            }
        }
    }

}
