package Hackerrank;

import java.util.Scanner;

import static java.lang.Math.max;

public class CommonChild {

    private static int commonChild(String s1, String s2) {
        return lcs(s1.toCharArray(), s2.toCharArray(), s1.length(), s2.length());
    }

    private static int lcs(char[] X, char[] Y, int m, int n) {
        int[][] L = new int[m + 1][n + 1];

        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (X[i - 1] == Y[j - 1])
                    L[i][j] = L[i - 1][j - 1] + 1;
                else
                    L[i][j] = max(L[i - 1][j], L[i][j - 1]);
            }
        }
        return L[m][n];
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s1 = in.next();
        String s2 = in.next();
        int result = commonChild(s1, s2);
        System.out.println(result);
    }


}
