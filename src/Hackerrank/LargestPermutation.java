package Hackerrank;

import java.math.BigInteger;
import java.util.Scanner;

public class LargestPermutation {

    private static BigInteger max = BigInteger.ZERO;

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int k = input.nextInt();
        int[] a = new int[n];
        int[] index = new int[n + 1];
        for (int i = 0; i < n; i++) {
            a[i] = input.nextInt();
            index[a[i]] = i;
        }
        for (int i = 0; i < n && k > 0; i++) {
            if (a[i] == n - i) {
                continue;
            }
            a[index[n - i]] = a[i];
            index[a[i]] = index[n - i];
            a[i] = n - i;
            index[n - i] = i;
            k--;
        }
        for (int i = 0; i < n; i++) {
            System.out.print(a[i] + " ");
        }
    }

    private static void permute(int[] arr, int i, int n, int k) {
        if (i == k) {
            BigInteger tmp = toInteger(arr);
            if (tmp.compareTo(max) > 0) {
                max = tmp;
            }
            return;
        }
        for (int j = i; j < n; j++) {
            swap(arr, i, j);
            permute(arr, i + 1, n, k);
            swap(arr, i, j);
        }
    }

    private static BigInteger toInteger(int[] arr) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int m : arr) {
            stringBuilder.append(m);
        }
        return new BigInteger(stringBuilder.toString());
    }

    private static void swap(int[] a, int i, int j) {
        int c = a[i];
        a[i] = a[j];
        a[j] = c;
    }

}
