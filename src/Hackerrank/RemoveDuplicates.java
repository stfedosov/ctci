package Hackerrank;

import java.util.Stack;

public class RemoveDuplicates {

    public static void main(String[] args) {
        assert removeDuplicates("aaaaaaaa").equals("");
        assert removeDuplicates("abbaca").equals("ca");
    }

    public static String removeDuplicates(String S) {
        Stack<Character> stack = new Stack<>();
        for (char s : S.toCharArray()) {
            if (!stack.isEmpty() && stack.peek() == s) {
                stack.pop();
            } else {
                stack.push(s);
            }
        }
        StringBuilder sb = new StringBuilder();
        for (char s : stack) {
            sb.append(s);
        }
        return sb.toString();
    }

    public static String removeDuplicates2(String S) {
        StringBuilder sb = new StringBuilder();
        for (char c : S.toCharArray()) {
            int size = sb.length();
            if (size > 0 && sb.charAt(size - 1) == c) {
                sb.deleteCharAt(size - 1);
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

}
