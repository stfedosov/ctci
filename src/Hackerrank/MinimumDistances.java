package Hackerrank;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MinimumDistances {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int A[] = new int[n];
        for (int A_i = 0; A_i < n; A_i++) {
            A[A_i] = in.nextInt();
        }
        Map<Integer, Integer> map = new HashMap<>();
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < A.length; i++) {
            Integer integer = map.get(A[i]);
            if (integer != null) {
                int abs = Math.abs(integer - i);
                if (abs < min) {
                    min = abs;
                }
                map.put(A[i], abs);
            } else {
                map.put(A[i], i);
            }
        }
        System.out.println(min != Integer.MAX_VALUE ? min : -1);
    }

}
