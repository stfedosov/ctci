package Hackerrank;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DegreeOfArray {

    public static void main(String[] args) {
        assert degreeOfArray(Arrays.asList(1, 2, 2, 3, 1)) == 2;
    }

    private static int degreeOfArray(List<Integer> arr) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        int freq = 0;
        for (int i = 0; i < arr.size(); i++) {
            if (!map.containsKey(arr.get(i))) {
                map.put(arr.get(i), new ArrayList<Integer>());
            }
            map.get(arr.get(i)).add(i);
            freq = Math.max(freq, map.get(arr.get(i)).size());
        }
        int min = Integer.MAX_VALUE;
        for (List<Integer> list : map.values()) {
            if (list.size() == freq) {
                min = Math.min(min, list.get(list.size() - 1) - list.get(0) + 1);
            }
        }
        return min;
    }

}
