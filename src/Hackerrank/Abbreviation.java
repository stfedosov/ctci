package Hackerrank;

import java.util.Scanner;

/**
 * @author sfedosov on 9/8/17.
 */
public class Abbreviation {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        for (int i = 0; i < num; i++) {
            handle(scanner.next(), scanner.next());
        }
    }

    private static void handle(String a, String b) {
        if (handleRecursively(a, b)) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }

    private static boolean containsUpperCaseWords(String string) {
        for (char c : string.toCharArray()) {
            if (Character.isUpperCase(c)) {
                return true;
            }
        }
        return false;
    }

    private static boolean handleRecursively(String a, String b) {
        if (b.isEmpty()) {
            return !containsUpperCaseWords(a);
        }
        if (a.isEmpty() && !b.isEmpty()) {
            return false;
        }
        final char firstAChar = a.charAt(0);
        final char firstBChar = b.charAt(0);
        char secondAChar = 0;
        if (a.length() > 1) {
            secondAChar = a.charAt(1);
        }
        if ((firstAChar == firstBChar)
                || ((Character.isUpperCase(firstAChar) || Character.isUpperCase(firstBChar))
                && Character.toLowerCase(firstAChar) == Character.toLowerCase(firstBChar)
                && Character.toLowerCase(firstAChar) != Character.toLowerCase(secondAChar))) {
            return handleRecursively(a.substring(1), b.substring(1));
        } else {
            return !Character.isUpperCase(firstAChar) && handleRecursively(a.substring(1), b);
        }
    }

}
