package Hackerrank;

import java.util.Scanner;

/**
 * @author sfedosov on 9/23/17.
 */
public class Cipher {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int K = scanner.nextInt();
        String s = scanner.next();
        /*System.err.println(recursive(s, N, new StringBuilder(), -1));*/
        // xxxxxx0
        //  xxxxxx0
        //   xxxxxx0
        //    xxxxxx0
        // __________
        // 1110100110
        //
        // xxx1010
        //  xxx1010
        //   xxx1010
        //    xxx1010
        // __________
        // 1110100110
        StringBuilder sb = new StringBuilder();
        int k = 0;
        boolean hasBeenInit = false;
        for (int i = s.length() - 1; i >= 0; i--) {
            int m = s.charAt(i) == '1' ? 1 : 0;
            if (!hasBeenInit) {
                sb.append(m);
                hasBeenInit = true;
                continue;
            }
            if (sb.length() == N) {
                break;
            }
            k++;
            char[] chars;
            if (k >= K) {
                chars = sb.substring((Math.abs(K - k) + 1), sb.length()).toCharArray();
            } else {
                chars = sb.toString().toCharArray();
            }
            int tmp = -1;
            for (char c : chars) {
                int x = c == '0' ? 0 : 1;
                if (tmp < 0) {
                    tmp = x;
                    continue;
                }
                tmp ^= x;
            }
            sb.append(m ^ tmp);
        }
        System.out.println(sb.reverse().toString());
    }

    /*private static String recursive(String input, int N, StringBuilder result, int tmp) {
        if (result.length() == 0) {
            result.append(input.charAt(input.length() - 1));
        }
        if (result.length() == N) {
            return new String(result);
        }
        if (tmp < 0) {
            if (result.charAt(0) == '1') {
                tmp = 1;
            } else {
                tmp = 0;
            }
        }
        tmp ^= input.charAt(input.length() - result.length()) == '1' ? 1 : 0;
        result.append(tmp);
        return recursive(input, N, result, tmp);
    }*/

}
