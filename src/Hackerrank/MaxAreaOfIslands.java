package Hackerrank;

/**
 * @author sfedosov on 11/29/18.
 */
public class MaxAreaOfIslands {

    public static void main(String[] args) {
        assert maxAreaOfIsland(new int[][]{
                {0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0}
        }) == 6;
    }

    public static int maxAreaOfIsland(int[][] grid) {
        int maxSize = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == 1) {
                    maxSize = Math.max(maxSize, dfs(grid, i, j));
                }
            }
        }
        return maxSize;
    }

    private static int dfs(int[][] grid, int i, int j) {
        if (i < 0
                || j < 0
                || i >= grid.length
                || j >= grid[i].length
                || grid[i][j] == 0) {
            return 0;
        }
        grid[i][j] = 0;
        return 1 + dfs(grid, i - 1, j)
                + dfs(grid, i, j - 1)
                + dfs(grid, i + 1, j)
                + dfs(grid, i, j + 1);
    }

}
