package Hackerrank;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * @author sfedosov on 9/5/17.
 */
public class BFSShortestReachInGraph {

    public static class Graph {

        int size;
        private final Map<Integer, Node> nodes = new HashMap<>();

        public Graph(int size) {
            this.size = size;
        }

        public void addEdge(int first, int second) {
            Node nodeFirst;
            Node nodeSecond;
            if (nodes.get(first) == null) {
                nodeFirst = new Node(first);
                nodes.put(first, nodeFirst);
            } else {
                nodeFirst = nodes.get(first);
            }
            if (nodes.get(second) == null) {
                nodeSecond = new Node(second);
                nodes.put(second, nodeSecond);
            } else {
                nodeSecond = nodes.get(second);
            }
            nodeFirst.addAdjacent(nodeSecond);
            nodeSecond.addAdjacent(nodeFirst);
        }

        public int[] shortestReach(int startId) { // 0 indexed
            int[] distances = new int[size];
            Arrays.fill(distances, -1);
            LinkedList<Node> queue = new LinkedList<>();
            queue.add(nodes.get(startId));
            Set<Integer> visited = new HashSet<>();
            visited.add(startId);
            distances[startId] = 0;
            while (!queue.isEmpty()) {
                Node current = queue.poll();
                Set<Node> adjacents = current.getAdjacents();
                if (adjacents != null) {
                    for (Node childNode : adjacents) {
                        if (!visited.contains(childNode.id)) {
                            visited.add(childNode.id);
                            distances[childNode.id] = distances[current.id] + 6;
                            queue.add(childNode);
                        }
                    }
                }
            }
            return distances;
        }
    }

    private static class Node {
        int id;
        private final Set<Node> adjacents = new HashSet<>();

        public Node(int id) {
            this.id = id;
        }

        public void addAdjacent(Node e) {
            adjacents.add(e);
        }

        public Set<Node> getAdjacents() {
            return adjacents;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Node node = (Node) o;

            return id == node.id;
        }

        @Override
        public int hashCode() {
            return id;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int queries = scanner.nextInt();

        for (int t = 0; t < queries; t++) {

            // Create a graph of size n where each edge weight is 6:
            Graph graph = new Graph(scanner.nextInt());
            int m = scanner.nextInt();

            // read and set edges
            for (int i = 0; i < m; i++) {
                int u = scanner.nextInt() - 1;
                int v = scanner.nextInt() - 1;

                // add each edge to the graph
                graph.addEdge(u, v);
            }

            // Find shortest reach from node s
            int startId = scanner.nextInt() - 1;
            int[] distances = graph.shortestReach(startId);

            for (int i = 0; i < distances.length; i++) {
                if (i != startId) {
                    System.out.print(distances[i]);
                    System.out.print(" ");
                }
            }
            System.out.println();
        }

        scanner.close();
    }

}
