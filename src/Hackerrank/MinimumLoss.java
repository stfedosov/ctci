package Hackerrank;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MinimumLoss {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        long[] v = new long[n];

        Map<Long, Integer> map = new HashMap<>(); // <value, position>
        for (int i = 0; i < n; i++) {
            v[i] = in.nextLong();
            map.put(v[i], i);
        }

        Arrays.sort(v);
        long min = Long.MAX_VALUE;
        for (int i = 1; i < n; i++) {
            int pos1 = map.get(v[i]);
            int pos2 = map.get(v[i - 1]);
            if (pos1 < pos2)
                min = Math.min(min, v[i] - v[i - 1]);
        }

        System.out.println(min);
    }

}
