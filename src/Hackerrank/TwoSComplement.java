package Hackerrank;


import java.util.Scanner;

public class TwoSComplement {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        long[] result = new long[n];
        for (int i = 0; i < n; i++) {
            result[i] = findNumberOfOnes(scanner.nextInt(), scanner.nextInt());
        }
        for (long l : result) {
            System.out.println(l);
        }
    }

    private static long findNumberOfOnes(int i, int i1) {
        long res = 0L;
        for (int x = i; x <= i1; x++) {
            res += Integer.bitCount(x);
        }
        return res;
    }

}
