package Hackerrank;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

/**
 * @author sfedosov on 8/21/18.
 */
public class ArrayReduction {

    public static void main(String[] args) throws IOException {
        assert reductionCost(Arrays.asList(4,2,3,1)) == 19;
        assert reductionCost(Arrays.asList(2,3,1)) == 9;
    }

    static int reductionCost(List<Integer> num) {
        List<Integer> totalCosts = new ArrayList<>();
        PriorityQueue<Integer> q = new PriorityQueue<>(num.size());
        q.addAll(num);
        reductionCostRecursive(q, totalCosts);
        int result = 0;
        for (int cost : totalCosts) {
            result += cost;
        }
        return result;
    }

    private static void reductionCostRecursive(final PriorityQueue<Integer> q,
                                               final List<Integer> costs) {
        if (q.size() == 1) {
            return;
        }
        int cost = q.poll() + q.poll();
        costs.add(cost);
        q.add(cost);
        reductionCostRecursive(q, costs);
    }

    /*static int reductionCost(List<Integer> num) {
        List<Integer> totalCosts = new ArrayList<>();
        reductionCostRecursive(num, totalCosts);
        int result = 0;
        for (int cost : totalCosts) {
            result += cost;
        }
        return result;
    }

    private static void reductionCostRecursive(final List<Integer> num,
                                               final List<Integer> costs) {
        if (num.size() == 1) {
            return;
        }
        Collections.sort(num);
        final List<Integer> newList;
        if (num.size() > 2) {
            newList = new ArrayList<>(num.subList(2, num.size()));
        } else {
            newList = new ArrayList<>();
        }
        int cost = num.get(0) + num.get(1);
        newList.add(cost);
        costs.add(cost);
        reductionCostRecursive(newList, costs);
    }*/




}
