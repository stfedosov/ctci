package Hackerrank;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * @author sfedosov on 8/25/17.
 */
public class RecursiveDigitSum {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        BigInteger num = scanner.nextBigInteger();
        int k = scanner.nextInt();
        StringBuilder s = new StringBuilder(String.valueOf(num));
        for (int i = 1; i < k; i++) {
            s.append(String.valueOf(num));
        }
        System.out.println(recursiveSuperDigit(s.toString()));
    }

    private static char recursiveSuperDigit(String s) {
        char[] chars = s.toCharArray();
        if (chars.length == 1) {
            return chars[0];
        }
        BigInteger result = BigInteger.ZERO;
        for (char aChar : chars) {
            result = result.add(BigInteger.valueOf((long) (aChar - '0')));
        }
        return recursiveSuperDigit(String.valueOf(result));
    }

}
