package Hackerrank;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author sfedosov on 8/16/17.
 */
public class RansomNote {

    private Map<String, Integer> magazineMap;
    private Map<String, Integer> noteMap;

    private RansomNote(String magazine, String note) {
        magazineMap = new HashMap<>();
        convertStringIntoMap(magazineMap, magazine);
        noteMap = new HashMap<>();
        convertStringIntoMap(noteMap, note);
    }

    private void convertStringIntoMap(Map<String, Integer> map, String string) {
        for (String s : string.split(" ")) {
            if (!map.containsKey(s)) {
                map.put(s, 1);
            } else {
                map.put(s, map.get(s) + 1);
            }
        }
    }

    private boolean solve() {
        for (Map.Entry<String, Integer> entry : noteMap.entrySet()) {
            if (!magazineMap.containsKey(entry.getKey())
                    || magazineMap.get(entry.getKey()) < entry.getValue()) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();

        // Eat whitespace to beginning of next line
        scanner.nextLine();

        RansomNote s = new RansomNote(scanner.nextLine(), scanner.nextLine());
        scanner.close();

        boolean answer = s.solve();
        if (answer)
            System.out.println("Yes");
        else System.out.println("No");

    }

}
