package Hackerrank;

public class KSubsequences {

    public static void main(String[] args) {
        assert getKSub(new int[]{1, 2, 3, 4, 1}, 3) == 3;
    }

    private static long getKSub(int[] array, int k) {
        int[] accumulative_sum = new int[array.length];
        long count = 0;
        accumulative_sum[0] = array[0];

        for (int i = 1; i < array.length; i++) {
            accumulative_sum[i] = accumulative_sum[i - 1] + array[i];
        }

        int[] kVal = new int[k];

        for (int num : accumulative_sum) {
            int mod = num % k;

            if (mod == 0) {
                count++;
            }

            count += kVal[mod];
            kVal[mod] += 1;

        }
        return count;
    }

}
