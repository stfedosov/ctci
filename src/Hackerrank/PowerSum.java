package Hackerrank;

import java.util.Scanner;

/**
 * @author sfedosov on 8/28/17.
 */
public class PowerSum {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long total = in.nextLong();
        int power = in.nextInt();
        System.out.println(findPowerSum(total, power, 1));
    }

    private static int findPowerSum(long total, int power, int i) {
        long value = (long) (total - Math.pow(i, power));
        if (total == 0) {
            return 1;
        }
        if (value < 0 || total < 0) {
            return 0;
        }
        return findPowerSum(total, power, i + 1) + findPowerSum(value, power, i + 1);
    }

}
