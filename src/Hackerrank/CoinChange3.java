package Hackerrank;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 11/8/19.
 */
public class CoinChange3 {

    public static void main(String[] args) {
        assert change(5, new int[]{1, 2, 2}) == 6;
        assert change(5, new int[]{1, 2, 5}) == 4;
    }

    public static int change(int amount, int[] coins) {
        Map<Integer, Map<Integer, Integer>> map = new HashMap<>();
        return countWays(amount, 0, coins, map);
    }

    private static int countWays(int amount, int idx, int[] coins, Map<Integer, Map<Integer, Integer>> map) {
        if (amount == 0) {
            return 1;
        } else if (map.containsKey(idx) && map.get(idx).containsKey(amount)) {
            return map.get(idx).get(amount);
        } else {
            int result = 0;
            for (int i = idx; i < coins.length; i++) {
                if (amount - coins[i] >= 0) {
                    result += countWays(amount - coins[i], i, coins, map);
                }
            }
            map.putIfAbsent(idx, new HashMap<>());
            map.get(idx).put(amount, result);
            return result;
        }
    }

}
