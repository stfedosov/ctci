package Hackerrank;

import java.util.Scanner;

/**
 * @author sfedosov on 9/7/17.
 */
public class SansaAndXor {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        for (int i = 0; i < num; i++) {
            int i1 = scanner.nextInt();
            int[] newArray = new int[i1];
            for (int j = 0; j < newArray.length; j++) {
                newArray[j] = scanner.nextInt();
            }
            countXoR(newArray);
        }
    }

    private static void countXoR(int[] newArray) {
        int length = newArray.length;
        if (length % 2 == 0) {
            System.out.println(0);
        } else {
            long sum = 0;
            for (int i = 0; i < newArray.length; i = i + 2) {
                sum ^= newArray[i];
            }
            System.out.println(sum);
        }
    }

    private static void countXoR2(int[] newArray) {
        int length = newArray.length;
        if (length % 2 == 0) {
            System.out.println(0);
        } else {
            long sum = 0L;
            int k = 0;
            for (int i = 0; i < length; i++) {
                int index = i + k + 1;
                if (index < length) {
                    sum ^= xorSubarray(newArray, i, index);
                } else if (index == length) {
                    sum ^= xorSubarray(newArray, length - k - 1, length);
                }
                if (i == length - 1 && k < length) {
                    i = -1;
                    k++;
                }
            }
            System.out.println(sum);
        }
    }

    private static long xorSubarray(int[] newArray, int i, int j) {
        long res = 0L;
        for (int x = i; x < j; x++) {
            res ^= newArray[x];
        }
        return res;
    }

}
