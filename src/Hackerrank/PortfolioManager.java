package Hackerrank;


import java.util.LinkedList;

/**
 * @author sfedosov on 9/24/18.
 */
public class PortfolioManager {

    public static void main(String[] args) {
        assert findMax(6, "3 4 5 1 3 # 1") == 9;
        assert findMax(5, "3 2 3 # 3 # 1") == 7;
        assert findMax(0, "") == 0;
    }

    static long findMax(int n, String tree) {
        TreeNode root = buildABinaryTree(tree);
        long[] memo = new long[n];
        return Math.max(findMaxDP(root, memo, true), findMaxDP(root, memo, false));
    }

    private static TreeNode buildABinaryTree(String tree) {
        if (tree == null || tree.trim().isEmpty()) {
            return null;
        }
        String[] parsed = tree.split(" ");
        TreeNode root = new TreeNode(Integer.parseInt(parsed[0]));
        LinkedList<TreeNode> queue = new LinkedList<>();
        queue.add(root);

        int i = 1;
        while (!queue.isEmpty() && i < parsed.length) {
            TreeNode node = queue.poll();
            if (!parsed[i].equals("#")) {
                node.left = new TreeNode(Integer.parseInt(parsed[i]));
                queue.offer(node.left);
            }
            i++;
            if (!parsed[i].equals("#")) {
                node.right = new TreeNode(Integer.parseInt(parsed[i]));
                queue.offer(node.right);
            }
            i++;
        }
        return root;
    }

    private static long findMaxDP(TreeNode root, long[] memo, boolean isChosen) {
        if (root == null) {
            return 0L;
        }
        int rootValue = root.value;
        if (isChosen) {
            memo[rootValue] = findMaxDP(root.left, memo, false) + findMaxDP(root.right, memo, false) + root.value;
        } else {
            memo[rootValue] = findMaxDP(root.left, memo, true) + findMaxDP(root.right, memo, true);
        }
        return memo[rootValue];
    }

    private static final class TreeNode {
        private TreeNode left;
        private TreeNode right;
        private int value;

        public TreeNode(int value) {
            this.value = value;
        }
    }

}
