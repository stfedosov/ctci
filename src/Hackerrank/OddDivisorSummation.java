package Hackerrank;

import java.util.Arrays;
import java.util.List;

/**
 * @author sfedosov on 8/28/18.
 */
public class OddDivisorSummation {

    public static void main(String[] args) {
        assert countSum(Arrays.asList(1, 6, 10)) == 11;
        assert countSum(Arrays.asList(21, 11, 7)) == 52;
    }

    static long countSum(List<Integer> numbers) {
        long result = 0L;
        for (int num : numbers) {
            for (int i = 1; i <= num / 2; i++) {
                if (num % i == 0 && i % 2 != 0) {
                    result += i;
                }
            }
            if (num % 2 != 0) {
                result += num;
            }
        }
        return result;
    }

}
