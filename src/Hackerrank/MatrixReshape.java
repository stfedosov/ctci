package Hackerrank;

/**
 * @author sfedosov on 8/31/18.
 */
public class MatrixReshape {

    public static void main(String[] args) {
        matrixReshape(new int[][]{{1, 2}, {3, 4}}, 4, 1);
    }

    public static int[][] matrixReshape(int[][] nums, int r, int c) {
        if (nums.length == r) {
            return nums;
        }
        int[][] result = new int[r][c];
        int col = nums[0].length;
        int j1 = 0;
        int i1 = 0;
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                result[i][j] = nums[i1][j1];
                j1++;
                if (j1 >= col) {
                    j1 = 0;
                    i1++;
                }
            }
        }
        return result;
    }

}
