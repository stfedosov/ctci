package Hackerrank;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sfedosov on 6/27/18.
 */
public class Cities {

    public static void main(String[] args) {
        assert Arrays.deepEquals(groupCitiesWithRotatedWords(new String[]{"Tokyo", "London", "Rome", "Donlon", "Kyoto", "Paris"}),
                new String[][]{
                        {"Rome"},
                        {"Tokyo", "Kyoto"},
                        {"London", "Donlon"},
                        {"Paris"}
                });
    }

    private static String[][] groupCitiesWithRotatedWords(String[] strings) {
        Map<String, List<Integer>> map = new HashMap<>();
        int i = 0;
        for (String string : strings) {
            String[] chars = string.toLowerCase().split("");
            Arrays.sort(chars);
            StringBuilder tmp = new StringBuilder();
            for (String c : chars) {
                tmp.append(c);
            }
            String key = tmp.toString();
            List<Integer> list = map.computeIfAbsent(key, k -> new ArrayList<>());
            list.add(i);
            i++;
        }
        String[][] res = new String[map.size()][];
        int k = 0;
        for (Map.Entry<String, List<Integer>> entry : map.entrySet()) {
            String[] tmp = new String[entry.getValue().size()];
            int x = 0;
            for (int integer : entry.getValue()) {
                tmp[x] = strings[integer];
                x++;
            }
            res[k] = tmp;
            k++;
        }
        return res;
    }

}
