package Hackerrank;

import java.util.Arrays;

public class WiggleSort2 {

    /**
     * Given an integer array nums, reorder it such that nums[0] < nums[1] > nums[2] < nums[3]....
     * Doesn't allow consecutive duplicates
     */
    public static void main(String[] args) {
        int[] nums = new int[]{1, 5, 1, 1, 6, 4};
        wiggleSort(nums);
        assert Arrays.equals(nums, new int[]{1, 6, 1, 5, 1, 4});
        nums = new int[]{1, 3, 2, 2, 3, 1};
        wiggleSort(nums);
        assert Arrays.equals(nums, new int[]{2, 3, 1, 3, 1, 2});
    }

    public static void wiggleSort(int[] nums) {
        if (nums == null || nums.length < 2) {
            return;
        }
        int[] copy = Arrays.copyOf(nums, nums.length);

        Arrays.sort(copy);

        int endOf1stHalf = (nums.length - 1) / 2;
        int endOf2ndHalf = nums.length - 1;

        for (int i = 0; i < nums.length; i++) {
            if (i % 2 == 0) { // i = 0, 2, 4, 6...
                nums[i] = copy[endOf1stHalf--];
            } else {
                nums[i] = copy[endOf2ndHalf--];
            }
        }
    }

}
