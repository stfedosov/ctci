package Hackerrank;

import java.util.Scanner;

/**
 * @author sfedosov on 8/14/17.
 */
public class FlipBits {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int N;
        N = in.nextInt();
        long[] ints = new long[N];
        for (int i = 0; i < ints.length; i++) {
            ints[i] = in.nextLong();
            long maxInt = (long) Math.pow(2, 32) - 1;
            ints[i] = ints[i] ^ maxInt;
        }

        for (long anInt : ints) {
            System.out.println(anInt);
        }

    }

}
