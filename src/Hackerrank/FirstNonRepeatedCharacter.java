package Hackerrank;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sfedosov on 9/12/18.
 */
public class FirstNonRepeatedCharacter {

    private static final int NO_OF_CHARS = 256;
    private static final String STRING = "mnonmpsms";
    static char count[] = new char[NO_OF_CHARS];

    public static void main(String[] args) {
        assert firstNonRepeating1(STRING).equals("o");
        assert STRING.charAt(firstNonRepeating2(STRING)) == 'o';
    }

    private static String firstNonRepeating1(String s) {
        Map<Character, Integer> scoreboard = new HashMap<>();
        // build table [char -> count]
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (scoreboard.containsKey(c)) {
                scoreboard.put(c, scoreboard.get(c) + 1);
            } else {
                scoreboard.put(c, 1);
            }
        } // since HashMap doesn't maintain order, going through string again
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (scoreboard.get(c) == 1) {
                return String.valueOf(c);
            }
        }
        return "";
    }

    /////////////////////////////////////////////////////////////

    private static void getCharCountArray(String str) {
        for (int i = 0; i < str.length(); i++)
            count[str.charAt(i)]++;
    }

    /* The method returns index of first non-repeating
       character in a string. If all characters are repeating
       then returns -1 */
    private static int firstNonRepeating2(String str) {
        getCharCountArray(str);
        int index = -1, i;

        for (i = 0; i < str.length(); i++) {
            if (count[str.charAt(i)] == 1) {
                index = i;
                break;
            }
        }

        return index;
    }

}
