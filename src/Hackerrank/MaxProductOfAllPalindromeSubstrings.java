package Hackerrank;

import java.util.ArrayList;
import java.util.List;

public class MaxProductOfAllPalindromeSubstrings {

    public static void main(String[] args) {
        System.out.println(findMaxProduct(getAllPalindromeSubstrings("attach")));
    }

    private static List<String> getAllPalindromeSubstrings(String s) {
        List<String> res = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            for (int j = i + 1; j <= s.length(); j++) {
                String x = s.substring(i, j);
                if (new StringBuilder(x).reverse().toString().equals(x)) {
                    res.add(x);
                }
            }
        }
        return res;
    }

    private static int findMaxProduct(List<String> list) {
        int[] mass = new int[list.size()];
        for (int i = 0; i < mass.length; i++) {
            for (char c : list.get(i).toCharArray()) {
                mass[i] |= 1 << c - 'a';
            }
        }
        int maxProduct = 0;
        for (int i = 0; i < mass.length; i++) {
            for (int j = i + 1; j < mass.length; j++) {
                int potentialMax = list.get(i).length() * list.get(j).length();
                if ((mass[i] & mass[j]) == 0 && (potentialMax > maxProduct))
                    maxProduct = potentialMax;
            }
        }
        return maxProduct;
    }
}
