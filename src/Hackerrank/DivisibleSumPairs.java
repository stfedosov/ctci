package Hackerrank;

import java.util.Scanner;

/**
 * @author sfedosov on 9/26/17.
 */
public class DivisibleSumPairs {

    static int divisibleSumPairs(int k, int[] ar) {
        int num = 0;
        for (int i = 0; i < ar.length; i++) {
            for (int j = i + 1; j < ar.length; j++) {
                if ((ar[i] + ar[j]) % k == 0) {
                    num++;
                }
            }
        }
        return num;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int[] ar = new int[n];
        for(int ar_i = 0; ar_i < n; ar_i++){
            ar[ar_i] = in.nextInt();
        }
        int result = divisibleSumPairs(k, ar);
        System.out.println(result);
    }

}
