package Hackerrank;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * @author sfedosov on 8/16/17.
 */
public class FibonacciModified {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] arr = new int[3];
        for (int arr_i = 0; arr_i < 3; arr_i++) {
            arr[arr_i] = in.nextInt();
        }
        System.out.println(getFibonacci(arr));
    }

    private static BigInteger getFibonacci(int[] arr) {
        BigInteger first = BigInteger.valueOf(arr[0]);
        BigInteger second = BigInteger.valueOf(arr[1]);
        BigInteger nth = BigInteger.valueOf(0);
        for (int i = 2; i < arr[2]; i++) {
            nth = first.add(second.multiply(second));
            first = second;
            second = nth;
        }
        return nth;
    }

}
