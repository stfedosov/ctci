package Hackerrank;

import java.util.Scanner;

public class BubbleSort {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] a = new int[n];
        for (int a_i = 0; a_i < n; a_i++) {
            a[a_i] = in.nextInt();
        }
        sort(a);
    }

    private static void sort(int[] a) {
        int numberOfSwapsTotal = 0;
        while (true) {
            // Track number of elements swapped during a single array traversal
            int numberOfSwaps = 0;

            for (int j = 0; j < a.length - 1; j++) {
                // Swap adjacent elements if they are in decreasing order
                if (a[j] > a[j + 1]) {
                    swap(a, j, j + 1);
                    numberOfSwaps++;
                }
            }

            // If no elements were swapped during a traversal, array is sorted
            if (numberOfSwaps == 0) {
                break;
            } else {
                numberOfSwapsTotal += numberOfSwaps;
            }
        }
        System.out.println("Array is sorted in " + numberOfSwapsTotal + " swaps.\n" +
                "First Element: " + a[0] + "\n" +
                "Last Element: " + a[a.length - 1]);
    }

    private static void swap(int[] a, int j, int i) {
        int tmp = a[j];
        a[j] = a[i];
        a[i] = tmp;
    }

}
