package Hackerrank;

public class IsSubsequence {

    public static void main(String[] args) {
        assert !isSubsequence2("acb", "ahbgdc");
        assert isSubsequence2("abc", "ahbgdc");
        assert !isSubsequence2("b", "c");
        assert isSubsequence2("", "c");
        assert isSubsequence2("twn", "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxtxxxxxxxxxxxxxxxxxxxxwxxxxxxxxxxxxxxxxxxxxxxxxxn");
    }

    // 35ms
    public static boolean isSubsequence2(String s, String t) {
        int sidx = 0;
        int tidx = 0;
        while (sidx < s.length() && tidx < t.length()) {
            if (s.charAt(sidx) == t.charAt(tidx)) {
                sidx++;
            }
            tidx++;
        }
        return s.length() == sidx;
    }

}
