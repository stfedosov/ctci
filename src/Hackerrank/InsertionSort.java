package Hackerrank;

/**
 * @author sfedosov on 9/8/17.
 */
public class InsertionSort {

    public static void insertionSort(int[] A) {
        for (int i = 1; i < A.length; i++) {
            int value = A[i];
            int j = i - 1;
            while (j >= 0 && A[j] > value) {
                A[j + 1] = A[j];
                j--;
            }
            A[j + 1] = value;
        }

        printArray(A);
    }


    static void printArray(int[] ar) {
        for (int n : ar) {
            System.out.print(n + " ");
        }
    }

    public static void main(String[] args) {
        int[] ar = new int[]{4, 3, 2, 10, 12, 1, 5, 6};
        insertionSort(ar);
    }

}
