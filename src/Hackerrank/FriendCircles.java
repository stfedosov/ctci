package Hackerrank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sfedosov on 8/29/18.
 */
public class FriendCircles {

    public static void main(String[] args) {
        assert findCircleNum(new int[][]{
                {1, 1, 0, 0},
                {1, 1, 0, 0},
                {0, 0, 0, 1}
        }) == 2;
    }

    // idea: transform adjacency matrix graph representation into adjacency list and count connected components

    public static int findCircleNum(int[][] M) {
        Map<Integer, List<Integer>> graph = new HashMap<>();
        int count = 0;
        for (int i = 0; i < M.length; i++) {
            for (int j = 0; j < M.length; j++) {
                if (M[i][j] == 1) {
                    graph.putIfAbsent(i, new ArrayList<>());
                    if (i != j) {
                        graph.get(i).add(j);
                    }
                }
            }
        }
        boolean[] visited = new boolean[graph.size()];
        for (int i : graph.keySet()) {
            if (!visited[i]) {
                markConnectedComponent(i, graph, visited);
                count++;
            }
        }
        return count;
    }

    private static void markConnectedComponent(int start, Map<Integer, List<Integer>> graph, boolean[] visited) {
        visited[start] = true;
        for (int x : graph.get(start)) {
            if (!visited[x]) {
                markConnectedComponent(x, graph, visited);
            }
        }
    }

}
