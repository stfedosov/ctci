package Hackerrank;

import java.io.IOException;
import java.util.Scanner;
import java.util.Stack;

/**
 * @author sfedosov on 9/8/18.
 */
public class CheckIfGivenSequenceIsAPreOrderOfBST {

    public static void main(String[] args) throws IOException {
//        Scanner sc = new Scanner(System.in);
//        int q = sc.nextInt();
//        for (int t = 0; t < q; t++) {
//            int n = sc.nextInt();
//            int[] array = new int[n];
//            for (int i = 0; i < n; i++) {
//                array[i] = sc.nextInt();
//            }
        int[] array = new int[]{10, 5, 1, 7, 40, 50};
        if (checkIsBST(array)) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
//        }
    }

    private static boolean checkIsBST(int array[]) {
        Stack<Integer> stack = new Stack<>();
        int root = -1;

        for (int x : array) {
            if (x < root) {
                return false;
            }

            while (!stack.empty() && stack.peek() < x) {
                root = stack.pop();
            }

            stack.push(x);
        }
        return true;
    }


}
