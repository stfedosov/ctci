package Hackerrank;

import java.util.Scanner;

/**
 * @author sfedosov on 8/31/17.
 */
public class LeftRotation {

    private static int[] arrayLeftRotation(int[] a, int n, int k) {
       int[] newInts = new int[n - k];
       System.arraycopy(a, k, newInts, 0, n - k);
       System.arraycopy(a, 0, a, n - k, k);
       System.arraycopy(newInts, 0, a, 0, n - k);
       return a;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int a[] = new int[n];
        for (int a_i = 0; a_i < n; a_i++) {
            a[a_i] = in.nextInt();
        }

        int[] output = arrayLeftRotation(a, n, k);
        for (int i = 0; i < n; i++)
            System.out.print(output[i] + " ");

        System.out.println();

    }

}
