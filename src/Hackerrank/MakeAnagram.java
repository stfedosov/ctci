package Hackerrank;

import java.util.Scanner;

/**
 * @author sfedosov on 8/15/17.
 */
public class MakeAnagram {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        String a = in.next();
        String b = in.next();

        int[] mass = new int[26];
        int[] mass1 = new int[26];

        for (char ca : a.toCharArray()) {
            mass[ca - 'a']++;
        }

        for (char cb : b.toCharArray()) {
            mass1[cb - 'a']++;
        }

        int k = 0;
        for (int i = 0; i < mass.length; i++) {
            k += Math.abs(mass[i] - mass1[i]);
        }

        System.out.println(k);

    }

}
