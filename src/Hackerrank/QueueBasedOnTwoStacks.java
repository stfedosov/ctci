package Hackerrank;

import java.util.Scanner;
import java.util.Stack;

/**
 * @author sfedosov on 9/21/17.
 */
public class QueueBasedOnTwoStacks {

    public static void main(String[] args) {
        MyQueue<Integer> queue = new MyQueue<>();

        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        for (int i = 0; i < n; i++) {
            int operation = scan.nextInt();
            if (operation == 1) { // enqueue
                queue.enqueue(scan.nextInt());
            } else if (operation == 2) { // dequeue
                queue.dequeue();
            } else if (operation == 3) { // print/peek
                System.out.println(queue.peek());
            }
        }
        scan.close();
    }

    private static class MyQueue<T> {
        Stack<T> stackNewestOnTop = new Stack<>();
        Stack<T> stackOldestOnTop = new Stack<>();

        public void enqueue(T value) { // Push onto newest stack
            stackNewestOnTop.push(value);
            if (stackOldestOnTop.isEmpty()) {
                stackOldestOnTop.push(value);
            }
        }

        public T peek() {
            return stackOldestOnTop.peek();
        }

        public T dequeue() {
            T obj = stackOldestOnTop.pop();
            stackNewestOnTop.remove(0);
            if (!stackNewestOnTop.isEmpty()) {
                stackOldestOnTop.push(stackNewestOnTop.elementAt(0));
            }
            return obj;
        }
    }

}
