package Leetcode2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class MinTaps {

    public static void main(String[] args) {

    }

    // Greedy solution, Time O(n * log(n)), Space O(n)
    public static int minTaps(int n, int[] ranges) {
        List<int[]> intervals = new ArrayList<>();
        for (int i = 0; i < ranges.length; i++)
            intervals.add(new int[]{i - ranges[i], i + ranges[i]});
        intervals.sort(Comparator.comparingInt(a -> a[0]));

        int count = 0, start = 0, maxEnd = 0, i = 0;
        while (true) {
            int temp = maxEnd; // for case when we don't have enough intervals to cover the whole length
            while (i < intervals.size() && intervals.get(i)[0] <= start) {
                maxEnd = Math.max(maxEnd, intervals.get(i)[1]);
                i++;
            }
            count++;
            if (maxEnd >= n) return count;
            if ((i < intervals.size() && intervals.get(i)[0] > maxEnd) || maxEnd == temp) return -1;
            start = maxEnd;
        }
    }

    // --------------------------

    // DP, top down, slow solution, doesn't pass all tests, Time O(n * long(n) + n^2), Space O(n)
    private static final int MAX = Integer.MAX_VALUE - 1;

    public static int minTapsBruteForce(int n, int[] ranges) {
        List<int[]> intervals = new ArrayList<>();
        for (int i = 0; i < ranges.length; i++)
            intervals.add(new int[]{i - ranges[i], i + ranges[i]});

        intervals.sort(Comparator.comparingInt(a -> a[0]));
        int[][] cache = new int[3000][3000];
        for (int[] c : cache) Arrays.fill(c, -1);
        int res = helper(intervals, n, 0, 0, cache);
        return res == MAX ? -1 : res;
    }

    private static int helper(List<int[]> clips, int time, int idx, int end, int[][] cache) {
        if (end >= time) return 0;
        if (idx >= clips.size()) return MAX;
        if (cache[idx][end] != -1) return cache[idx][end];
        int min = MAX;
        for (int i = idx; i < clips.size(); i++) {
            if (clips.get(i)[0] <= end) {
                min = Math.min(min, 1 + helper(clips, time, idx + 1, clips.get(i)[1], cache));
            }
        }
        cache[idx][end] = min;
        return min;
    }

}
