package Leetcode2;

public class GenerateRandomPointInACircle {

    public static void main(String[] args) {
        // The area of the square is (2R)^2
        // and the area of the circle is pi*R ≈3.14R
        GenerateRandomPointInACircle generator = new GenerateRandomPointInACircle(1.0, 0.0, 0.0);
    }

    private double x_center, y_center, radius;

    public GenerateRandomPointInACircle(double radius, double x_center, double y_center) {
        this.x_center = x_center;
        this.y_center = y_center;
        this.radius = radius;
    }

    public double[] randPoint() {
        double x0 = x_center - radius;
        double y0 = y_center - radius;
        while (true) {
            double x_new = x0 + Math.random() * radius * 2;
            double y_new = y0 + Math.random() * radius * 2;
            if (Math.sqrt(Math.pow((x_center - x_new), 2) + Math.pow((y_center - y_new), 2)) <= radius) {
                return new double[]{x_new, y_new};
            }
        }
    }

}
