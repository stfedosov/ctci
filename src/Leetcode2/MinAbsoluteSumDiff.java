package Leetcode2;

import java.util.TreeSet;

public class MinAbsoluteSumDiff {

    public static void main(String[] args) {
        assert minAbsoluteSumDiff(new int[]{1, 10, 4, 4, 2, 7}, new int[]{9, 3, 5, 1, 7, 4}) == 20;
        assert minAbsoluteSumDiff(new int[]{1, 28, 21}, new int[]{9, 21, 20}) == 9;
    }

    static int mod = (int) 1e9 + 7;

    public static int minAbsoluteSumDiff(int[] num1, int[] num2) {
        TreeSet<Integer> ts = new TreeSet();
        for (int n : num1) ts.add(n);
        long sum = 0;
        long maxSave = 0;
        for (int i = 0; i < num1.length; ++i) {
            long originalDiff = Math.abs(num1[i] - num2[i]);
            sum += originalDiff;
            sum %= mod;
            long diffIfChosen = originalDiff;
            if (ts.contains(num2[i]))
                diffIfChosen = 0;
            else {
                Integer higher = ts.higher(num2[i]);
                if (higher != null) diffIfChosen = higher - num2[i];

                Integer lower = ts.lower(num2[i]);
                if (lower != null) diffIfChosen = Math.min(diffIfChosen, num2[i] - lower);
            }
            long save = originalDiff - diffIfChosen;
            maxSave = Math.max(maxSave, save);
        }

        return (int) (sum - maxSave) % mod;
    }

}
