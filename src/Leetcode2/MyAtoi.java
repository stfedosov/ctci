package Leetcode2;

public class MyAtoi {

    public static void main(String[] args) {
        assert myAtoi("    -42") == -42;
        assert myAtoi("4193 with words") == 4193;
        assert myAtoi("-91283472332") == -2147483648;
    }

    public static int myAtoi(String s) {
        int i = 0;
        int result = 0;
        int sign = 1;
        if (s.length() == 0) return 0;

        //discard white space
        while (i < s.length() && s.charAt(i) == ' ') i++;

        //check the sign
        if (i < s.length() && (s.charAt(i) == '+' || s.charAt(i) == '-'))
            sign = (s.charAt(i++) == '-') ? -1 : 1;

        // proceed only if the char is digits
        while (i < s.length() && s.charAt(i) >= '0' && s.charAt(i) <= '9') {
            // Since we are doing r =  r * 10 + digit formula
            // when r > max / 10 if u do * 10 it will overflow
            // if r == max / 10 then any number + 7 will overflow
            if (result > Integer.MAX_VALUE / 10 ||
                    (result == Integer.MAX_VALUE / 10 && s.charAt(i) - '0' > Integer.MAX_VALUE % 10))
                return (sign == 1) ? Integer.MAX_VALUE : Integer.MIN_VALUE;

            result = result * 10 + (s.charAt(i++) - '0');
        }
        return result * sign;
    }

}
