package Leetcode2;

import java.util.Arrays;

public class HighestStackOfBoxes {

    public static void main(String[] args) {
        // L, W, H
        int[][] nums = {{3, 9, 9}, {1, 4, 10}, {5, 10, 11}, {3, 9, 3}, {1, 5, 3}, {7, 12, 1}};
        assert howManyBoxesAtMaxWeMightHave(nums) == 4;
        nums = new int[][]{{2, 3, 2}, {1, 5, 4}, {1, 2, 2}, {2, 4, 1}, {3, 6, 2}, {4, 5, 3}};
        assert howManyBoxesAtMaxWeMightHave(nums) == 3;
        assert maxHeightOfTheBoxesStack(nums) == 7;
    }

    private static int howManyBoxesAtMaxWeMightHave(int[][] nums) {
        // compare L and W
        Arrays.sort(nums, (a, b) -> a[0] == b[0] ? a[1] == b[1] ? a[2] - b[2] : a[1] - b[1] : a[0] - b[0]);
        int res = 0;
        int[] dp = new int[nums.length];
        Arrays.fill(dp, 1);
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < i; j++) {
                if (nums[j][0] < nums[i][0] && nums[j][1] < nums[i][1]) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
            }
            res = Math.max(dp[i], res);
        }
        return res;
    }

    private static int maxHeightOfTheBoxesStack(int[][] nums) {
        // compare L and W
        Arrays.sort(nums, (a, b) -> a[0] == b[0] ? a[1] == b[1] ? a[2] - b[2] : a[1] - b[1] : a[0] - b[0]);
        int res = 0;
        int[] dp = new int[nums.length];
        int[] height = new int[nums.length];
        Arrays.fill(dp, 1);
        for (int i = 0; i < nums.length; i++) {
            height[i] = nums[i][2];
            for (int j = 0; j < i; j++) {
                if (nums[j][0] < nums[i][0] && nums[j][1] < nums[i][1]) {
                    if (dp[i] == dp[j] + 1) {
                        height[i] += height[j];
                    } else if (dp[i] < dp[j] + 1) {
                        dp[i] = dp[j] + 1;
                    }
                }
            }
            res = Math.max(height[i], res);
        }
        return res;
    }

}
