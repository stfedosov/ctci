package Leetcode2;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class MinStickers {

    public static void main(String[] args) {
        assert minStickers(new String[]{"with", "example", "science"}, "thehat") == 3;
        assert minStickers(new String[]{"notice", "possible"}, "basicbasic") == -1;
        assert minStickers(new String[]{"these", "guess", "about", "garden", "him"}, "atomher") == 3;
    }

    // BFS approach
    public static int minStickers(String[] stickers, String target) {
        int[][] stickerMaps = new int[stickers.length][26];
        int i = 0;
        for (String sticker : stickers) {
            stickerMaps[i] = new int[26];
            for (char c : sticker.toCharArray()) {
                stickerMaps[i][c - 'a']++;
            }
            i++;
        }
        int[] targetMap = new int[26];
        for (char c : target.toCharArray()) {
            targetMap[c - 'a']++;
        }
        Queue<int[]> q = new LinkedList<>();
        q.offer(targetMap);
        int numStickers = 0;
        Set<String> seen = new HashSet<>();
        while (!q.isEmpty()) {
            numStickers++;
            int size = q.size();
            while (size-- > 0) {
                int[] next = q.poll();
                if (seen.add(toString(next))) {
                    for (int[] stickerMap : stickerMaps) {
                        int[] temp = next.clone();
                        for (int a = 0; a < 26; a++) {
                            temp[a] = temp[a] > stickerMap[a] ? temp[a] - stickerMap[a] : 0;
                        }
                        if (isSpelled(temp)) {
                            return numStickers;
                        }
                        q.offer(temp);
                    }
                }
            }
        }
        return -1;
    }

    private static boolean isSpelled(int[] temp) {
        for (int x : temp) {
            if (x != 0) return false;
        }
        return true;
    }

    private static String toString(int[] next) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < next.length; i++) {
            int x = next[i];
            while (x-- > 0) {
                sb.append((char) (i + 'a'));
            }
        }
        return sb.toString();
    }

}
