package Leetcode2;

public class BrokenCalculator {

    public static void main(String[] args) {
        assert brokenCalc(3, 10) == 3;
        assert brokenCalc(2, 3) == 2;
        assert brokenCalc(1024, 1) == 1023;
        assert brokenCalc(5, 8) == 2;
        assert brokenCalc(1, 1000000000) == 39;
    }

    // Work backwards: we should add 1 to Y in case when we want to make X - 1 and divide by 2 in case we want to X * 2
    public static int brokenCalc(int X, int Y) {
        int result = 0;
        while (X < Y) {
            result++;
            if (Y % 2 == 1) {
                Y++;
            } else {
                Y /= 2;
            }
        }
        return result + (X - Y);
    }

}
