package Leetcode2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KillProcess {

    public static void main(String[] args) {
        assert killProcess(Arrays.asList(1, 3, 10, 5), Arrays.asList(3, 0, 5, 3), 5).equals(Arrays.asList(5, 10));
    }

    public static List<Integer> killProcess(List<Integer> pid, List<Integer> ppid, int kill) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        for (int i = 0; i < pid.size(); i++) {
            map.computeIfAbsent(ppid.get(i), x -> new ArrayList<>()).add(pid.get(i));
        }
        List<Integer> result = new ArrayList<>();
        dfs(map, kill, result);
        return result;
    }

    private static void dfs(Map<Integer, List<Integer>> map, int kill, List<Integer> result) {
        result.add(kill);
        for (int child : map.getOrDefault(kill, Collections.emptyList())) {
            dfs(map, child, result);
        }
    }

}
