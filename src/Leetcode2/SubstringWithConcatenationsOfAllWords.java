package Leetcode2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class SubstringWithConcatenationsOfAllWords {

    /*
    You are given a string s and an array of strings words of the same length.
    Return all starting indices of substring(s) in s that is a concatenation of each word in words exactly once,
    in any order, and without any intervening characters.
     */
    public static void main(String[] args) {
        System.out.println(findSubstringOptimal("barfoothefoobarman", new String[]{"foo", "bar"})); // [0,9]
        System.out.println(findSubstringOptimal("wordgoodgoodgoodbestword", new String[]{"word", "good", "best", "good"})); // [8]
    }

    public static List<Integer> findSubstringOptimal(String s, String[] words) {
        Map<String, Integer> wordFreq = new HashMap<>();

        for (String word : words)
            wordFreq.put(word, wordFreq.getOrDefault(word, 0) + 1);

        List<Integer> answer = new ArrayList<>();
        for (int i = 0; i < words[0].length(); i++) {
            slidingWindow(i, s, answer, wordFreq, words.length, words[0].length());
        }

        return answer;
    }

    private static void slidingWindow(int left,
                                      String s,
                                      List<Integer> answer,
                                      Map<String, Integer> wordFreq,
                                      int wordsLength,
                                      int wordLength) {
        Map<String, Integer> wordsFound = new HashMap<>();
        int wordsUsed = 0;
        boolean excessWord = false;

        // Do the same iteration pattern as the previous approach - iterate
        // word_length at a time, and at each iteration we focus on one word
        for (int right = left; right <= s.length() - wordLength; right += wordLength) {

            String substring = s.substring(right, right + wordLength);
            if (!wordFreq.containsKey(substring)) {
                // Mismatched word - reset the window
                wordsFound.clear();
                wordsUsed = 0;
                excessWord = false;
                left = right + wordLength;
            } else {
                // If we reached max window size or have an excess word
                while (right - left == wordsLength * wordLength || excessWord) {
                    String leftmostWord = s.substring(left, left + wordLength);
                    left += wordLength;
                    wordsFound.put(leftmostWord, wordsFound.get(leftmostWord) - 1);

                    if (wordsFound.get(leftmostWord) >= wordFreq.get(leftmostWord)) {
                        // This word was an excess word
                        excessWord = false;
                    } else {
                        // Otherwise we actually needed it
                        wordsUsed--;
                    }
                }

                // Keep track of how many times this word occurs in the window
                wordsFound.put(substring, wordsFound.getOrDefault(substring, 0) + 1);
                if (wordsFound.get(substring) <= wordFreq.get(substring)) {
                    wordsUsed++;
                } else {
                    // Found too many instances
                    excessWord = true;
                }

                if (wordsUsed == wordsLength && !excessWord) {
                    // Found a valid substring
                    answer.add(left);
                }
            }
        }
    }

    // -------------------------------------- ANOTHER APPROACH ----------------------------

    public static List<Integer> findSubstring(String s, String[] words) {
        // count sum lengths of substring
        Map<String, Integer> allWords = new HashMap<>();
        int subSize = 0;
        for (String word : words) {
            subSize += word.length();
            allWords.put(word, allWords.getOrDefault(word, 0) + 1);
        }
        List<Integer> indices = new ArrayList<>();
        int start = 0;
        // iterate over each char of original string up to end - subLength
        while (start <= s.length() - subSize) {
            findSubstring(start, s, new HashMap<>(), allWords, indices, subSize);
            start += 1;
        }

        return indices;
    }

    // in method search for new entry starting from index and counting in subMap
    private static void findSubstring(int index,
                                      String s,
                                      Map<String, Integer> subMap,
                                      Map<String, Integer> allWords,
                                      List<Integer> indices,
                                      int subSize) {
        // if maps are equal - all words are used
        if (allWords.equals(subMap)) {
            indices.add(index - subSize);
            return;
        }
        // go over each entry in original map
        for (Map.Entry<String, Integer> entry : allWords.entrySet()) {
            String word = entry.getKey();
            // if count is reached - move to next word
            if (Objects.equals(entry.getValue(), subMap.getOrDefault(word, 0))) continue;

            // if count is not reached and string start with word, starting from index
            if (s.startsWith(word, index)) {
                // increment words count in subMap and call recursive method
                subMap.put(word, subMap.getOrDefault(word, 0) + 1);
                findSubstring(index + word.length(), s, subMap, allWords, indices, subSize);
                // decrease count back for this word
                subMap.put(word, subMap.getOrDefault(word, 0) - 1);
            }
        }
    }

}
