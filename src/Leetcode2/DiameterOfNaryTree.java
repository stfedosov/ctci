package Leetcode2;

import java.util.ArrayList;
import java.util.List;

public class DiameterOfNaryTree {

    public static void main(String[] args) {
        Node n = new Node(1);
        Node n2 = new Node(2);
        Node n3 = new Node(3);
        Node n4 = new Node(4);
        Node n5 = new Node(5);
        Node n6 = new Node(6);
        n.children.add(n2);
        n.children.add(n3);
        n.children.add(n4);
        n3.children.add(n5);
        n3.children.add(n6);
        assert diameter(n) == 3;
    }

    static int m = 0;

    // select the top two longest sub-paths connected by each non-leaf node
    public static int diameter(Node root) {
        dfs(root);
        return m;
    }

    private static int dfs(Node root) {
        if (root == null) return 0;
        int max1 = 0, max2 = 0;
        for (Node chld : root.children) {
            int max = 1 + dfs(chld);
            if (max > max1) {
                max2 = max1;
                max1 = max;
            } else if (max > max2) {
                max2 = max;
            }
        }
        m = Math.max(m, max1 + max2);
        return max1;
    }

}

class Node {
    public int val;
    public List<Node> children;

    public Node(int _val) {
        val = _val;
        children = new ArrayList<>();
    }

    public Node(int _val, ArrayList<Node> _children) {
        val = _val;
        children = _children;
    }
}
