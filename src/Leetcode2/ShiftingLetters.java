package Leetcode2;

public class ShiftingLetters {

    public static void main(String[] args) {
        assert shiftingLetters("abc", new int[]{3, 5, 9}).equals("rpl");
        assert shiftingLetters("abc", new int[0]).equals("");
        assert shiftingLetters("thklop", new int[]{4, 5, 6, 7, 8, 8}).equals("fpniex");
    }

    public static String shiftingLetters(String s, int[] shifts) {
        if (shifts.length == 0) return "";
        StringBuilder sb = new StringBuilder();
        int i = 0;
        long[] cumulativeShifts = new long[shifts.length];
        cumulativeShifts[shifts.length - 1] = shifts[shifts.length - 1];
        for (int j = shifts.length - 2; j >= 0; j--) {
            cumulativeShifts[j] += cumulativeShifts[j + 1] + shifts[j];
        }
        for (char c : s.toCharArray()) {
            int cInt = c - 'a';
            sb.append((char) (((cInt + cumulativeShifts[i++]) % 26) + 'a'));
        }
        return sb.toString();
    }

}
