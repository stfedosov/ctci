package Leetcode2;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class MaxFreqStack {

    /*
        Design a stack-like data structure to push elements to the stack and pop the most frequent element from the stack.
     */
    public static void main(String[] args) {
        MaxFreqStack stack = new MaxFreqStack();
        stack.push(5);
        stack.push(7);
        stack.push(5);
        stack.push(7);
        stack.push(4);
        stack.push(5);
        System.out.println(stack.pop()); // 5
        System.out.println(stack.pop()); // 7
        System.out.println(stack.pop()); // 5
        System.out.println(stack.pop()); // 4
        System.out.println(stack.pop()); // 7
    }

    private final Queue<Node> q = new PriorityQueue<>((a, b) ->
            b.freq == a.freq ? b.order - a.order : b.freq - a.freq);
    private final Map<Integer, Integer> mapWithFreq = new HashMap<>();
    private int order = 0;

    public MaxFreqStack() {

    }

    // O(log(n))
    public void push(int val) {
        mapWithFreq.put(val, mapWithFreq.getOrDefault(val, 0) + 1);
        q.offer(new Node(val, mapWithFreq.get(val), order++));
    }

    // O(log(n))
    public int pop() {
        Node next = q.poll();
        int toReturn = next.val;
        mapWithFreq.put(toReturn, mapWithFreq.get(toReturn) - 1);
        if (mapWithFreq.get(toReturn) == 0) mapWithFreq.remove(toReturn);
        return toReturn;
    }

    private static class Node {
        private final int val, order, freq;
        public Node(int val, int freq, int order) {
            this.val = val;
            this.freq = freq;
            this.order = order;
        }
    }
}