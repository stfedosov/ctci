package Leetcode2;

import java.util.Arrays;

public class MinOperationToMakeUniValuedGrid {

    /*
    ===========================
    You are given a 2D integer grid of size m x n and an integer x.
    In one operation, you can add x to or subtract x from any element in the grid.

    Return the minimum number of operations to make the grid uni-value (all values are equal).
    If it is not possible, return -1.
    ===========================

    Approach: transform matrix to array and sort it out.
    then find the median and use it as a optimal value for all others to get to.

    Another approach is brute force DFS, but it's quite slow and inefficient.
     */
    public static void main(String[] args) {
        assert minOperations(new int[][]{{2, 4}, {6, 8}}, 2) == 4;
    }

    public static int minOperations(int[][] grid, int x) {
        int[] array = new int[grid.length * grid[0].length];
        int index = 0;

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                array[index++] = grid[i][j];
            }
        }

        Arrays.sort(array);
        int mid = array[(array.length - 1) / 2];
        int count = 0;

        for (int num : array) {
            if (num == mid) {
                continue;
            }

            if (Math.abs(num - mid) % x != 0) {
                return -1;
            }

            count += (Math.abs(num - mid) / x);
        }

        return count;
    }

}
