package Leetcode2;

public class LeftRightBoundariesBinarySearch {

    public static void main(String[] args) {
        int[] arr = new int[]{2, 3, 4, 5, 6, 7, 7, 7, 7, 8, 9};
        System.out.println(find_left(arr, 7)); // = 5
        System.out.println(find_right(arr, 7)); // = 8
    }

    private static int find_left(int[] nums, int target) {
        int left = 0, right = nums.length - 1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] >= target) {
                // in case of equal we shrink the right boundary
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        // Check Outbound
        if (left >= nums.length || nums[left] != target)
            return -1;
        return left;
    }

    private static int find_right(int[] nums, int target) {
        int left = 0, right = nums.length - 1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] <= target) {
                // in case of equal we increase the left boundary
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        // Instead, check for right is crossing the border, for case when the target is smaller than all the elements
        if (right < 0 || nums[right] != target)
            return -1;
        return right;
    }

}
