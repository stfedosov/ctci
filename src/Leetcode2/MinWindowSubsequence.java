package Leetcode2;

public class MinWindowSubsequence {

    // Find a minimum window containing all characters of T with THE SAME ORDER of chars
    public static void main(String[] args) {
        assert minWindow("abcdebdde", "bde").equals("bcde");
        // "bcde" is the answer because it occurs before "bdde" which has the same length.
        // "deb" is not a smaller window because the elements of T in the window must occur in order.
    }

    // O(S * T)
    // The idea is to scan forward until you’ve matched T (in order)
    // and then scan backward to exclude any extra characters
    public static String minWindow(String S, String T) {
        int length = Integer.MAX_VALUE, idxS = 0, idxT = 0, start, end;
        String minSubsequence = "";
        while (idxS < S.length()) {
            if (S.charAt(idxS) == T.charAt(idxT)) {
                idxT++;
                if (idxT == T.length()) { // matched T
                    start = idxS;
                    end = idxS;
                    idxT--;
                    // scan backward to exclude extra chars not needed in the answer
                    while (idxT >= 0) {
                        if (S.charAt(start) == T.charAt(idxT)) {
                            idxT--;
                        }
                        start--;
                    }
                    start++;
                    if (end - start + 1 < length) {
                        length = end - start + 1;
                        minSubsequence = S.substring(start, end + 1);
                    }
                    idxS = start;
                    idxT = 0;
                }
            }
            // if chars do not match we keep scanning S. even if they do match, we still need to continue
            idxS++;
        }
        return minSubsequence;
    }

}
