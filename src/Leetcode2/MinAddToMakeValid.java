package Leetcode2;

public class MinAddToMakeValid {

    // "()" - valid
    public static void main(String[] args) {
        assert minAddToMakeValid("())") == 1;
        assert minAddToMakeValid("(((") == 3;
        assert minAddToMakeValid("()") == 0;
    }

    public static int minAddToMakeValid(String S) {
        int open = 0, closed = 0;
        for (char c : S.toCharArray()) {
            if (c == '(') {
                open++;
            } else {
                if (open != 0) open--;
                else closed++;
            }
        }
        return open + closed;
    }

}
