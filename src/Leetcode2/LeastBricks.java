package Leetcode2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LeastBricks {

    public static void main(String[] args) {
        assert leastBricks(Arrays.asList(Arrays.asList(1, 2, 2, 1),
                Arrays.asList(3, 1, 2),
                Arrays.asList(1, 3, 2),
                Arrays.asList(2, 4),
                Arrays.asList(3, 1, 2),
                Arrays.asList(1, 3, 1, 1))) == 2;
    }

    // We need to find similar sums for all rows - it means that it is possible to draw the line between them without crossing brick
    // One row won't have similar sum, as it has only positive numbers
    public static int leastBricks(List<List<Integer>> wall) {
        Map<Integer, Integer> map = new HashMap<>();
        int max = 0;
        for (List<Integer> row : wall) {
            int sum = 0;
            for (int j = 0; j < row.size() - 1; j++) {
                sum += row.get(j);
                map.put(sum, map.getOrDefault(sum, 0) + 1);
                max = Math.max(max, map.get(sum));
            }
        }
        return wall.size() - max;
    }

}
