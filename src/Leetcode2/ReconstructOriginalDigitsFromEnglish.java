package Leetcode2;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class ReconstructOriginalDigitsFromEnglish {

    public static void main(String[] args) {
       assert originalDigitsBacktracking("owoztneoer").equals("012");
       assert originalDigitsBacktracking("fviefuro").equals("45");
       assert originalDigitsBacktracking("zeroonetwothreefourfivesixseveneightnine").equals("0123456789");
       assert originalDigits("owoztneoer").equals("012");
       assert originalDigits("fviefuro").equals("45");
       assert originalDigits("zeroonetwothreefourfivesixseveneightnine").equals("0123456789");
    }

    // count unique letters for each number and then calculate the result
    public static String originalDigits(String s) {
        Map<Character, Integer> map = new HashMap<>();
        for (char c : s.toCharArray()) map.put(c, map.getOrDefault(c, 0) + 1);
        int[] result = new int[10];
        result[0] = map.getOrDefault('z', 0); // zero - 'z' unique
        result[2] = map.getOrDefault('w', 0); // two - 'w' unique
        result[4] = map.getOrDefault('u', 0); // four - 'u' unique
        result[6] = map.getOrDefault('x', 0); // six - 'x' unique
        // letter "g" is present only in "eight"
        result[8] = map.getOrDefault('g', 0);
        // letter "h" is present only in "three" and "eight"
        result[3] = map.getOrDefault('h', 0) - result[8];
        // letter "f" is present only in "five" and "four"
        result[5] = map.getOrDefault('f', 0) - result[4];
        // letter "s" is present only in "seven" and "six"
        result[7] = map.getOrDefault('s', 0) - result[6];
        // letter "i" is present in "nine", "five", "six", and "eight"
        result[9] = map.getOrDefault('i', 0) - result[5] - result[6] - result[8];
        // letter "n" is present in "one", "nine", and "seven"
        result[1] = map.getOrDefault('n', 0) - result[7] - 2 * result[9];
        StringBuilder toReturn = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            toReturn.append(String.valueOf(i).repeat(result[i]));
        }
        return toReturn.toString();
    }

    // ===================
    // Backtracking - TLE
    // ===================
    public static String originalDigitsBacktracking(String s) {
        Map<String, String> nums = new LinkedHashMap<>() {{
            put("zero", "0");
            put("one", "1");
            put("two", "2");
            put("three", "3");
            put("four", "4");
            put("five", "5");
            put("six", "6");
            put("seven", "7");
            put("eight", "8");
            put("nine", "9");
        }};
        Map<Character, Integer> map = new HashMap<>();
        for (char c : s.toCharArray()) {
            map.put(c, map.getOrDefault(c, 0) + 1);
        }
        return dfs(map, nums, "");
    }

    private static String dfs(Map<Character, Integer> map, Map<String, String> nums, String res) {
        if (map.isEmpty()) {
            return res;
        } else {
            Loop:
            for (String number : nums.keySet()) {
                Map<Character, Integer> map2 = new HashMap<>();
                for (char c : number.toCharArray()) map2.put(c, map2.getOrDefault(c, 0) + 1);
                for (Map.Entry<Character, Integer> e : map2.entrySet())
                    if (map.getOrDefault(e.getKey(), 0) < e.getValue()) continue Loop;
                Map<Character, Integer> tmp = new HashMap<>(map2);
                for (Map.Entry<Character, Integer> e : map2.entrySet()) {
                    Integer val = map.get(e.getKey());
                    if (val - e.getValue() == 0) {
                        map.remove(e.getKey());
                    } else {
                        map.put(e.getKey(), val - e.getValue());
                    }
                }
                String dfs = dfs(map, nums, res + nums.get(number));
                if (!dfs.isEmpty()) return dfs;
                for (Map.Entry<Character, Integer> e : tmp.entrySet()) {
                    if (!map.containsKey(e.getKey())) map.put(e.getKey(), e.getValue());
                    else map.put(e.getKey(), map.get(e.getKey()) + e.getValue());
                }
            }
            return "";
        }
    }

}
