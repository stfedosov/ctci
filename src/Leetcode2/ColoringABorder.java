package Leetcode2;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class ColoringABorder {

    public static void main(String[] args) {
        int[][] answer = colorBorder(new int[][]{
                {1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1}}, 3, 3, 2);
               /*
                [2, 2, 2, 2, 2, 2]
                [2, 1, 1, 1, 1, 2]
                [2, 1, 1, 1, 1, 2]
                [2, 1, 1, 1, 1, 2]
                [2, 1, 1, 1, 1, 2]
                [2, 2, 2, 2, 2, 2]
               */
    }

    private static final int[][] directions = new int[][]{{0, 1}, {1, 0}, {-1, 0}, {0, -1}}; // neighbors' relative displacements.

    public static int[][] colorBorder(int[][] grid, int r0, int c0, int color) {
        int initial = grid[r0][c0];
        Set<String> connectedComponent = new HashSet<>(); // put the cell number into Set to avoid visiting again.
        Queue<int[]> q = new LinkedList<>();
        q.offer(new int[]{r0, c0}); // add initial cell.
        connectedComponent.add(r0 + "," + c0); // add initial cell number.
        while (!q.isEmpty()) { // BFS starts.
            int[] next = q.poll();
            int r = next[0], c = next[1];
            if (isBorderLine(grid, r, c)) {
                grid[r][c] = color;
            } // on grid boundary.
            for (int[] direction : directions) { // travers its 4 neighbors.
                int i = r + direction[0], j = c + direction[1]; // neighbor coordinates.
                if (isValid(grid, connectedComponent, i, j)) { // not visited before.
                    if (grid[i][j] == initial) { // its neighbor is of same color, put it into Queue.
                        q.offer(new int[]{i, j}); // put it into Queue.
                        connectedComponent.add(i + "," + j);
                    } else { // its neighbor is of different color, hence it is on component boundary.
                        grid[r][c] = color;
                    }
                }
            }
        }
        return grid;
    }

    private static boolean isBorderLine(int[][] grid, int r, int c) {
        return r == 0 || r == grid.length - 1 || c == 0 || c == grid[r].length - 1;
    }

    private static boolean isValid(int[][] grid, Set<String> connectedComponent, int i, int j) {
        return i >= 0 && i < grid.length && j >= 0 && j < grid[i].length && !connectedComponent.contains(i + "," + j);
    }

}
