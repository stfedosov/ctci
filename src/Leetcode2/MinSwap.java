package Leetcode2;

import java.util.Arrays;

public class MinSwap {

    public static void main(String[] args) {
        assert minSwap(new int[]{1, 3, 5, 4}, new int[]{1, 2, 3, 7}) == 1;
    }

    public static int minSwap(int[] A, int[] B) {
        int[][] cache = new int[A.length][2];
        for (int[] c : cache) Arrays.fill(c, -1);
        return minSwap(A, B, 0, -1, -1, 0, cache);
    }

    private static int minSwap(int[] A, int[] B, int idx, int prevA, int prevB, int swap, int[][] cache) {
        if (idx >= A.length) return 0;
        if (cache[idx][swap] != -1) return cache[idx][swap];
        int min = 99999;
        if (idx == 0 || A[idx] > prevA && B[idx] > prevB) {
            min = Math.min(min, minSwap(A, B, idx + 1, A[idx], B[idx], 0, cache));
        }
        if (idx == 0 || A[idx] > prevB && B[idx] > prevA) {
            min = Math.min(min, 1 + minSwap(A, B, idx + 1, B[idx], A[idx], 1, cache));
        }
        cache[idx][swap] = min;
        return min;
    }

}
