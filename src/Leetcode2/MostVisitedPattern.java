package Leetcode2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MostVisitedPattern {

    public List<String> mostVisitedPattern(String[] username, int[] timestamp, String[] website) {
        //group all username, timestamp and website in one object
        List<Data> dataList = new ArrayList<>();
        for (int i = 0; i < username.length; i++) {
            dataList.add(new Data(username[i], timestamp[i], website[i]));
        }
        //sort the dataList by timestamp
        dataList.sort(Comparator.comparingInt(d -> d.timestamp));
        //key: username, value: website list, sorted by timestamp
        Map<String, List<String>> nameToWebsiteVisit = new HashMap<>();
        for (Data data : dataList) {
            nameToWebsiteVisit.computeIfAbsent(data.username, x -> new ArrayList<>()).add(data.website);
        }
        //backtracking, found all possible patterns
        Map<List<String>, Integer> freqMap = new HashMap<>();
        for (List<String> list : nameToWebsiteVisit.values()) {
            backtracking(0, list, new ArrayList<>(), freqMap, new HashSet<>());
        }
        //find the max freq pattern and return it
        int maxFreq = 0;
        List<String> result = null;
        for (List<String> key : freqMap.keySet()) {
            int freq = freqMap.get(key);
            if (freq > maxFreq) {
                maxFreq = freq;
                result = key;
            } else if (freq == maxFreq && isLexicographicallySmaller(key, result)) {
                result = key;
            }
        }
        return result;
    }

    private boolean isLexicographicallySmaller(List<String> key, List<String> result) {
        for (int i = 0; i < key.size(); i++) {
            if (!key.get(i).equals(result.get(i))) {
                return key.get(i).compareTo(result.get(i)) < 0;
            }
        }
        return false;
    }

    private void backtracking(int currIdx,
                              List<String> websites,
                              List<String> currList,
                              Map<List<String>, Integer> freqMap,
                              Set<List<String>> visited) {
        if (currList.size() == 3) {
            if (visited.contains(currList)) return;
            List<String> copy = new ArrayList<>(currList);
            visited.add(copy);
            freqMap.put(copy, freqMap.getOrDefault(copy, 0) + 1);
        } else {
            for (int i = currIdx; i < websites.size(); i++) {
                currList.add(websites.get(i));
                backtracking(i + 1, websites, currList, freqMap, visited);
                currList.remove(currList.size() - 1);
            }
        }
    }

    private static class Data {
        public String username;
        public int timestamp;
        public String website;

        public Data(String username, int timestamp, String website) {
            this.username = username;
            this.timestamp = timestamp;
            this.website = website;
        }
    }

}
