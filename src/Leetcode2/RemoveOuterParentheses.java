package Leetcode2;

public class RemoveOuterParentheses {

    public static void main(String[] args) {
        assert removeOuterParentheses("(()())(())").equals("()()()");
        assert removeOuterParentheses("(()())(())(()(()))").equals("()()()()(())");
        assert removeOuterParentheses("()()").equals("");
        assert removeOuterParentheses("(()())(())(()(()))").equals("()()()()(())");
    }

    public static String removeOuterParentheses(String S) {
        StringBuilder sb = new StringBuilder();
        int open = 0, closed = 0, i = 0, prev = 0;
        for (char c : S.toCharArray()) {
            if (c == '(') open++;
            else if (c == ')') closed++;
            if (open == closed) {
                String s = S.substring(prev, i + 1);
                if (s.length() > 2) {
                    sb.append(s, 1, s.length() - 1);
                }
                prev = i + 1;
                open = 0;
                closed = 0;
            }
            i++;
        }
        return sb.toString();
    }

}
