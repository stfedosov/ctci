package Leetcode2;

import java.util.Map;
import java.util.TreeMap;

public class RangeModule {

    private final TreeMap<Integer, Integer> map;

    public static void main(String[] args) {
        RangeModule rangeModule = new RangeModule();
        rangeModule.addRange(10, 20);
        rangeModule.removeRange(14, 16);
        assert rangeModule.queryRange(10, 14);
        assert !rangeModule.queryRange(13, 15);
        assert rangeModule.queryRange(16, 17);
    }

    public RangeModule() {
        this.map = new TreeMap<>();
    }

    public void addRange(int left, int right) {
        while (true) {
            // Merge with existing ones
            Map.Entry<Integer, Integer> foundEntry = map.floorEntry(right);
            if (foundEntry == null || foundEntry.getValue() < left) break;
            left = Math.min(left, foundEntry.getKey());
            right = Math.max(right, foundEntry.getValue());
            map.remove(foundEntry.getKey());
        }
        map.put(left, right);
    }

    public boolean queryRange(int left, int right) {
        Map.Entry<Integer, Integer> foundEntry = map.floorEntry(left);
        return foundEntry != null && foundEntry.getValue() >= right;
    }

    public void removeRange(int left, int right) {
        // Add the range first, to make the code simpler
        addRange(left, right);
        Map.Entry<Integer, Integer> foundEntry = map.floorEntry(left);
        map.remove(foundEntry.getKey());
        if (foundEntry.getKey() < left) {
            map.put(foundEntry.getKey(), left);
        }
        if (foundEntry.getValue() > right) {
            map.put(right, foundEntry.getValue());
        }
    }


}
