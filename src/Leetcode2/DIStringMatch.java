package Leetcode2;

import java.util.Arrays;

public class DIStringMatch {

    public static void main(String[] args) {
        assert Arrays.equals(diStringMatch("IDID"), new int[]{0, 4, 1, 3, 2});
        assert Arrays.equals(diStringMatch("III"), new int[]{0, 1, 2, 3});
        assert Arrays.equals(diStringMatch("DDI"), new int[]{3, 2, 0, 1});
    }

    public static int[] diStringMatch(String S) {
        int[] res = new int[S.length() + 1];
        int x = 0, start = 0, end = S.length();
        for (int i = 0; i < S.length(); i++) {
            res[x++] = S.charAt(i) == 'I' ? start++ : end--;
        }
        res[x] = S.charAt(S.length() - 1) == 'I' ? start++ : end--;
        return res;
    }

}
