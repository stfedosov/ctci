package Leetcode2;

import java.util.Arrays;

public class NumRescueBoats {

    public static void main(String[] args) {
        assert numRescueBoats(new int[]{1, 9, 9, 8}, 9) == 3;
        assert numRescueBoats(new int[]{2, 2}, 6) == 1;
        assert numRescueBoats(new int[]{3, 2, 2, 1}, 3) == 3;
        assert numRescueBoats(new int[]{1, 2}, 3) == 1;
    }

    public static int numRescueBoats(int[] people, int limit) {
        Arrays.sort(people);
        int i = 0, j = people.length - 1, count = 0;
        while (i <= j) {
            count++;
            if (people[i] + people[j] <= limit) {
                i++;
            }
            j--;
        }
        return count;
    }

}
