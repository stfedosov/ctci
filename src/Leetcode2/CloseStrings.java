package Leetcode2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CloseStrings {

    public static void main(String[] args) {
        assert closeStrings("abc", "bca");
        assert !closeStrings("cabbba", "aabbss");
        assert closeStrings("cabbba", "abbccc");
        assert !closeStrings("hmhhhhfhmhhmmfhhfmmhhmhmhhhh", "mfmmmmmmmmmmmfmfmmmmmmmmmmmm");
    }

    public static boolean closeStrings(String word1, String word2) {
        if (word1.length() != word2.length()) return false;
        Map<Character, Integer> map1 = new HashMap<>(), map2 = new HashMap<>();
        for (char c : word1.toCharArray()) {
            map1.put(c, map1.getOrDefault(c, 0) + 1);
        }
        for (char c : word2.toCharArray()) {
            map2.put(c, map2.getOrDefault(c, 0) + 1);
        }
        for (char c : word1.toCharArray()) {
            if (!map2.containsKey(c)) return false;
        }
        for (char c : word2.toCharArray()) {
            if (!map1.containsKey(c)) return false;
        }
        List<Integer> list1, list2;
        list1 = new ArrayList<>(map1.values());
        list2 = new ArrayList<>(map2.values());
        list1.sort(Comparator.naturalOrder());
        list2.sort(Comparator.naturalOrder());
        return list1.equals(list2);
    }

}
