package Leetcode2;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class StockPrice {

    // Stock Price Fluctuation
    /*
    You are given a stream of records about a particular stock.
    Each record contains a timestamp and the corresponding price of the stock at that timestamp.

    Unfortunately due to the volatile nature of the stock market, the records do not come in order.
    Even worse, some records may be incorrect.
    Another record with the same timestamp may appear later in the stream correcting the price of the previous wrong record.

    Design an algorithm that:

    Updates the price of the stock at a particular timestamp, correcting the price from any previous records at the timestamp.
    Finds the latest price of the stock based on the current records.
    The latest price is the price at the latest timestamp recorded.
    Finds the maximum price the stock has been based on the current records.
    Finds the minimum price the stock has been based on the current records.
     */
    public static void main(String[] args) {

    }

    private int latestTime = Integer.MIN_VALUE;
    private final Map<Integer, Integer> current;
    private final TreeMap<Integer, Integer> maxesAndMins;

    public StockPrice() {
        this.current = new HashMap<>();
        this.maxesAndMins = new TreeMap<>();
    }

    public void update(int timestamp, int price) {
        latestTime = Math.max(latestTime, timestamp);
        var prev = current.put(timestamp, price);
        if (prev != null) {
            maxesAndMins.computeIfPresent(prev, (key, cnt) -> cnt == 1 ? null : cnt - 1);
        }
        maxesAndMins.put(price, maxesAndMins.getOrDefault(price, 0) + 1);
    }

    public int current() {
        return current.get(latestTime);
    }

    public int maximum() {
        return maxesAndMins.lastKey();
    }

    public int minimum() {
        return maxesAndMins.firstKey();
    }

}
