package Leetcode2;

import java.util.PriorityQueue;
import java.util.Queue;

public class MaximumMinimumPath {

    // find a path with maximal minimum number

    public static void main(String[] args) {
        assert maximumMinimumPath(new int[][]{
                {5, 4, 5},
                {1, 2, 6},
                {7, 4, 6}}) == 4;
        assert maximumMinimumPath(new int[][]{
                {3, 4, 6, 3, 4},
                {0, 2, 1, 1, 7},
                {8, 8, 3, 2, 7},
                {3, 2, 4, 9, 8},
                {4, 1, 2, 0, 0},
                {4, 6, 5, 4, 3}
        }) == 3;
    }

    private static int[][] directions = new int[][]{{-1, 0}, {1, 0}, {0, 1}, {0, -1}};

    public static int maximumMinimumPath(int[][] A) {
        Queue<int[]> maxHeap = new PriorityQueue<>((a, b) -> b[0] - a[0]);
        maxHeap.offer(new int[]{A[0][0], 0, 0});
        int maxScore = A[0][0];
        A[0][0] = -1; // visited;
        while (!maxHeap.isEmpty()) {
            int[] next = maxHeap.poll();
            maxScore = Math.min(maxScore, next[0]);
            if (next[1] == A.length - 1 && next[2] == A[0].length - 1) break;
            for (int[] dir : directions) {
                int newDirX = next[1] + dir[0];
                int newDirY = next[2] + dir[1];
                if (newDirX >= 0 && newDirX < A.length && newDirY >= 0 && newDirY < A[0].length && A[newDirX][newDirY] >= 0) {
                    maxHeap.offer(new int[]{A[newDirX][newDirY], newDirX, newDirY});
                    A[newDirX][newDirY] = -1;
                }
            }
        }
        return maxScore;
    }

}
