package Leetcode2;

import java.util.HashSet;
import java.util.Set;

public class SplitArray {

    // Given an integer array nums of length n, return true if there is a triplet (i, j, k)
    public static void main(String[] args) {
        assert splitArray(new int[]{1, 2, 1, 2, 1, 2, 1});
        assert !splitArray(new int[]{1, 2, 1, 2, 1, 2, 1, 2});
    }

    public static boolean splitArray(int[] nums) {
        int[] accum = new int[nums.length];
        accum[0] = nums[0];
        for (int i = 1; i < accum.length; i++) {
            accum[i] = accum[i - 1] + nums[i];
        }
        for (int j = 3; j < accum.length - 3; j++) {
            Set<Integer> set = new HashSet<>();

            for (int i = 1; i < j - 1; i++) {
                int a = accum[i - 1];
                int b = accum[j - 1] - accum[i];

                if (a == b)
                    set.add(a);
            }

            for (int k = j + 2; k < accum.length - 1; k++) {
                int a = accum[accum.length - 1] - accum[k];
                int b = accum[k - 1] - accum[j];

                if (a == b && set.contains(a)) return true;
            }
        }
        return false;
    }

}
