package Leetcode2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DifferByOneCharacter {

    public static void main(String[] args) {
        assert differByOneOptimal(new String[]{"abcd","acbd", "aacd"});
        assert !differByOneOptimal(new String[]{"ab","cd","yz"});
        assert differByOneOptimal(new String[]{"abcd","cccc","abyd","abab"});

        assert differByOneBruteForce(new String[]{"abcd","acbd", "aacd"});
        assert !differByOneBruteForce(new String[]{"ab","cd","yz"});
        assert differByOneBruteForce(new String[]{"abcd","cccc","abyd","abab"});
    }

    // ---- OPTIMAL
    public static boolean differByOneOptimal(String[] dict) {
        Set<String> set = new HashSet<>();
        for (String word : dict) {
            for (String generated : generate(word)) {
                if (!set.add(generated)) return true;
            }
        }
        return false;
    }

    private static List<String> generate(String word) {
        List<String> res = new ArrayList<>();
        for (int i = 0; i < word.length(); i++) {
            res.add(word.substring(0, i) + '*' + word.substring(i + 1));
        }
        return res;
    }


    // ---- BRUTE FORCE
    public static boolean differByOneBruteForce(String[] dict) {
        for (int i = 0; i < dict.length; i++) {
            for (int j = i + 1; j < dict.length; j++) {
                if (compare(dict[i], dict[j])) return true;
            }
        }
        return false;
    }

    private static boolean compare(String s1, String s2) {
        boolean hasSeenDiff = false;
        for (int idx = 0; idx < s1.length(); idx++) {
            if (s1.charAt(idx) != s2.charAt(idx)) {
                if (hasSeenDiff) return false;
                hasSeenDiff = true;
            }
        }
        return true;
    }

}
