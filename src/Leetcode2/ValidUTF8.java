package Leetcode2;

public class ValidUTF8 {

    public static void main(String[] args) {
        assert validUtf8(new int[]{197, 130, 1});
        assert !validUtf8(new int[]{235, 140, 4});
        assert !validUtf8(new int[]{145});
    }

    public static boolean validUtf8(int[] data) {
        int numOfBytes = 0;
        for (int datum : data) {
            String strBinary = String.format("%8s", Integer.toBinaryString(datum)).replaceAll(" ", "0");
            if (numOfBytes == 0) {
                // Get the number of 1s in the beginning of the string.
                for (int j = 0; j < strBinary.length(); j++) {
                    if (strBinary.charAt(j) == '0') {
                        break;
                    }
                    numOfBytes++;
                }
                // 1 byte characters
                if (numOfBytes == 0) {
                    continue;
                }
                // Invalid scenarios according to the rules of the problem.
                if (numOfBytes > 4 || numOfBytes == 1) {
                    return false;
                }
            } else {
                // Else, we are processing integers which represent bytes which are a part of
                // a UTF-8 character. So, they must adhere to the pattern `10xxxxxx`.
                if (strBinary.charAt(0) != '1' || strBinary.charAt(1) != '0') {
                    return false;
                }
            }
            // We reduce the number of bytes to process by 1 after each integer.
            numOfBytes--;
        }
        // This is for the case where we might not have the complete data for
        // a particular UTF-8 character.
        return numOfBytes == 0;
    }


}
