package Leetcode2;

import java.util.Stack;

public class StockSpanner {

    public static void main(String[] args) {
        StockSpanner stockSpanner = new StockSpanner();
        assert stockSpanner.next(100) == 1;
        assert stockSpanner.next(80) == 1;
        assert stockSpanner.next(60) == 1;
        assert stockSpanner.next(70) == 2;
        assert stockSpanner.next(60) == 1;
        assert stockSpanner.next(75) == 4;
        assert stockSpanner.next(85) == 6;

        stockSpanner = new StockSpanner();
        assert stockSpanner.next(29) == 1;
        assert stockSpanner.next(91) == 2;
        assert stockSpanner.next(62) == 1;
        assert stockSpanner.next(76) == 2;
        assert stockSpanner.next(51) == 1;
    }

    private static class Pair {
        int val;
        int count;

        public Pair(int val, int count) {
            this.val = val;
            this.count = count;
        }
    }

    private Stack<Pair> s;

    public StockSpanner() {
        s = new Stack<>();
    }

    // worst case Time complexity - O(N)
    public int next(int price) {
        int count = 1;
        while (!s.isEmpty() && s.peek().val <= price) {
            count += s.peek().count;
            s.pop();
        }
        s.push(new Pair(price, count));
        return count;
    }

}
