package Leetcode2;

public class TotalNQueens {

    public static void main(String[] args) {
        assert totalNQueens(4) == 2;
        assert totalNQueens(8) == 92;
    }

    static int res = 0;

    public static int totalNQueens(int n) {
        res = 0;
        solve(new int[n][n], 0, 0);
        return res;
    }

    private static void solve(int[][] board, int row, int col) {
        if (col >= board[row].length) {
            res++;
        } else {
            for (int i = 0; i < board.length; i++) {
                if (isSafe(board, i, col)) {
                    board[i][col] = 1;
                    solve(board, i, col + 1);
                    board[i][col] = 0;
                }
            }
        }
    }

    private static boolean isSafe(int[][] board, int row, int col) {
        for (int i = 0; i < col; i++) {
            if (board[row][i] == 1) return false;
        }
        for (int i = row, j = col; i >= 0 && j >= 0; i--, j--) {
            if (board[i][j] == 1) return false;
        }
        for (int i = row, j = col; j >= 0 && i < board.length; i++, j--) {
            if (board[i][j] == 1) return false;
        }
        return true;
    }

}
