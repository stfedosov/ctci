package Leetcode2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BuildingsWithAnOceanView {

    // There are n buildings in a line.
    // You are given an integer array heights of size n that represents
    // the heights of the buildings in the line.
    //
    // The ocean is to the right of the buildings.
    // A building has an ocean view if the building can see the ocean without obstructions.
    // Formally, a building has an ocean view if all the buildings to its right have a smaller height.
    //
    // Return a list of indices (0-indexed) of buildings that have an ocean view, sorted in increasing order.
    public static void main(String[] args) {
        assert Arrays.equals(findBuildings(new int[]{4, 2, 3, 1}), new int[]{0, 2, 3});
        assert Arrays.equals(findBuildings(new int[]{4, 3, 2, 1}), new int[]{0, 1, 2, 3});
        assert Arrays.equals(findBuildings(new int[]{1, 3, 2, 4}), new int[]{3});
        assert Arrays.equals(findBuildings(new int[]{2, 2, 2, 2}), new int[]{3});
    }

    public static int[] findBuildings(int[] heights) {
        List<Integer> res = new ArrayList<>();
        int max = Integer.MIN_VALUE;
        for (int i = heights.length - 1; i >= 0; i--) {
            if (max < heights[i]) {
                max = heights[i];
                res.add(0, i);
            }
        }
        return res.stream().mapToInt(Integer::valueOf).toArray();
    }

}
