package Leetcode2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

public class FindAllPeople {

    /*
    You are given an integer n indicating there are n people numbered from 0 to n - 1.
    You are also given a 0-indexed 2D integer array meetings where meetings[i] = [xi, yi, timei]
    indicates that person xi and person yi have a meeting at timei.

    A person may attend multiple meetings at the same time.
    Finally, you are given an integer firstPerson.

    Person 0 has a secret and initially shares the secret with a person firstPerson at time 0.
    This secret is then shared every time a meeting takes place with a person that has the secret.
    More formally, for every meeting, if a person xi has the secret at timei,
    then they will share the secret with person yi, and vice versa.

    The secrets are shared instantaneously.
    That is, a person may receive the secret and share it with people
    in other meetings within the same time frame.

    Return a list of all the people that have the secret after all the meetings have taken place.
     */

    public static void main(String[] args) {
        System.out.println(findAllPeople(6, new int[][]{{1, 2, 5}, {2, 3, 8}, {1, 5, 10}}, 1)); // [0, 1, 2, 3, 5]
        System.out.println(findAllPeople(4, new int[][]{{3, 1, 3}, {1, 2, 2}, {0, 3, 3}}, 3)); // [0, 1, 3]
    }

    public static List<Integer> findAllPeople(int n, int[][] meetings, int firstPerson) {
        Map<Integer, Map<Integer, List<Integer>>> graph = new HashMap<>(); // <person, <time, <next_persons...>>>
        for (int[] meeting : meetings) {
            graph.putIfAbsent(meeting[0], new HashMap());
            graph.putIfAbsent(meeting[1], new HashMap());
            graph.get(meeting[0]).computeIfAbsent(meeting[2], x -> new ArrayList<>()).add(meeting[1]);
            graph.get(meeting[1]).computeIfAbsent(meeting[2], x -> new ArrayList<>()).add(meeting[0]);
        }
        Set<Integer> visited = new HashSet<>();
        Queue<int[]> pq = new PriorityQueue<>(Comparator.comparingInt(a -> a[1]));
        pq.offer(new int[]{0, 0});
        pq.offer(new int[]{firstPerson, 0});
        while (!pq.isEmpty()) {
            int[] cur = pq.poll();
            int currentPerson = cur[0], currentTime = cur[1];
            if (!visited.contains(currentPerson)) {
                visited.add(currentPerson);
                for (int otherTime : graph.getOrDefault(currentPerson, new HashMap<>()).keySet()) {
                    if (otherTime >= currentTime) {
                        for (int nextPerson : graph.get(currentPerson).get(otherTime))
                            pq.offer(new int[]{nextPerson, otherTime});
                    }
                }
            }
        }
        return new ArrayList<>(visited);
    }

}
