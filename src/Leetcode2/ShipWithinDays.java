package Leetcode2;

public class ShipWithinDays {

    public static void main(String[] args) {
        assert shipWithinDays(new int[]{3, 2, 2, 4, 1, 4}, 3) == 6;
        assert shipWithinDays(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 5) == 15;
    }

    public static int shipWithinDays(int[] weights, int days) {
        int low = 0, high = 0;
        for (int weight : weights) {
            low = Math.max(low, weight);
            high += weight;
        }
        while (low < high) {
            int mid = low + (high - low) / 2;
            if (!isShipPossible(weights, days, mid)) {
                low = mid + 1;
            } else {
                high = mid;
            }
        }
        return low;
    }

    private static boolean isShipPossible(int[] weights, int days, int mid) {
        int curSum = 0, daysNeeded = 0;
        for (int weight : weights) {
            if (curSum + weight > mid) {
                daysNeeded++;
                curSum = 0;
            }
            curSum += weight;
        }
        return daysNeeded < days;
    }

}
