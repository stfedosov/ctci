package Leetcode2;

public class ReorderPowerOfTwo {

    public static void main(String[] args) {
        assert reorderedPowerOf2(1);
        assert reorderedPowerOf2(46);
        assert !reorderedPowerOf2(24);
    }

    public static boolean reorderedPowerOf2(int N) {
        if (isPowerOfTwo(N)) return true;
        return permute(N + "", 0);
    }

    private static boolean permute(String number, int reordered) {
        if (number.length() == 0) {
            return isPowerOfTwo(reordered);
        } else {
            for (int i = 0; i < number.length(); i++) {
                char symbol = number.charAt(i);
                if (((reordered == 0 && symbol != '0') || (reordered > 0)) &&
                        permute(number.substring(0, i) + number.substring(i + 1), 10 * reordered + (symbol - '0')))
                    return true;
            }
            return false;
        }
    }

    private static boolean isPowerOfTwo(int N) {
        return N == 0 || (N & (N - 1)) == 0;
    }

}
