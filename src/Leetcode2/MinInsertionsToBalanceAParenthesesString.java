package Leetcode2;

public class MinInsertionsToBalanceAParenthesesString {

    // "())" - valid
    public static void main(String[] args) {
        assert minInsertions("(()))") == 1;
        assert minInsertions("())") == 0;
        assert minInsertions("))())(") == 3;
        assert minInsertions("((((((") == 12;
    }

    public static int minInsertions(String s) {
        int count = 0;
        int open = 0;  // open brackets not matched till now
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                open++; // add open brackets to not matched count
            } else {
                // look if only 1 closing bracket encountered, we need 1 more closing bracket
                if (i + 1 == s.length() || s.charAt(i + 1) != ')') {
                    count++;
                } else {
                    i++;  // skip the next closing bracket, valid case "())"
                }
                if (open > 0) {
                    open--; // in either way here we can close available open bracket
                } else {
                    count++;   // if there is no opened bracket we increment count for the open brackets required to be inserted
                }
            }
        }
        return count + open * 2; // open * 2 is the final amount of open brackets remaining which needs twice closing braces.
    }

}
