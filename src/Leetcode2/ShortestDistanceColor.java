package Leetcode2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShortestDistanceColor {

    public static void main(String[] args) {
        assert shortestDistanceColor(new int[]{1, 1, 2, 1, 3, 2, 2, 3, 3}, new int[][]{{1, 3}, {2, 2}, {6, 1}})
                .equals(Arrays.asList(3, 0, 3));

        assert shortestDistanceColor(new int[]{3, 2, 2, 1, 3, 1, 1, 1, 3, 1}, new int[][]
                {{4, 1}, {9, 2}, {4, 2}, {8, 1}, {0, 3}, {2, 1}, {2, 3}, {6, 3}, {4, 1}, {1, 2}})
                .equals(Arrays.asList(1, 7, 2, 1, 0, 1, 2, 2, 1, 0));

        assert shortestDistanceColor(new int[]{1, 2}, new int[][]{{0, 3}})
                .equals(Collections.singletonList(-1));
    }

    public static List<Integer> shortestDistanceColor(int[] colors, int[][] queries) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        for (int i = 0; i < colors.length; i++) {
            map.computeIfAbsent(colors[i], x -> new ArrayList<>()).add(i);
        }
        List<Integer> ans = new ArrayList<>();
        for (int[] query : queries) {
            int index = query[0];
            int color = query[1];
            if (!map.containsKey(color)) {
                ans.add(-1);
            } else {
                ans.add(binarySearch(index, map.get(color)));
            }
        }
        return ans;
    }

    public static int binarySearch(int index, List<Integer> list) {
        int left = 0, right = list.size() - 1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (list.get(mid) >= index) {
                // in case of equal we shrink the right boundary
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        int res = left == list.size() ? --left : left;
        if (left - 1 >= 0 && index - list.get(left - 1) < list.get(left) - index) {
            res = left - 1;
        }
        return Math.abs(list.get(res) - index);
    }

}
