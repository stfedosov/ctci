package Leetcode2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class NQueens {

    public static void main(String[] args) {
        assert solveNQueens(4).equals(Arrays.asList(
                Arrays.asList(".Q..", "...Q", "Q...", "..Q."),
                Arrays.asList("..Q.", "Q...", "...Q", ".Q..")));
        assert solveNQueens(1).equals(Collections.singletonList(Collections.singletonList("Q")));
    }

    public static List<List<String>> solveNQueens(int n) {
        List<List<String>> res = new ArrayList<>();
        solve(new int[n][n], 0, 0, new ArrayList<>());
        return res;
    }

    private static void solve(int[][] board, int row, int col, List<List<String>> res) {
        if (col >= board[row].length) {
            List<String> tmp = new ArrayList<>();
            for (int[] rows : board) {
                StringBuilder sb = new StringBuilder();
                for (int column : rows) {
                    sb.append(column == 1 ? "Q" : ".");
                }
                tmp.add(sb.toString());
            }
            res.add(tmp);
        } else {
            for (int i = 0; i < board.length; i++) {
                if (isSafe(board, i, col)) {
                    board[i][col] = 1;
                    solve(board, i, col + 1, res);
                    board[i][col] = 0;
                }
            }
        }
    }

    private static boolean isSafe(int[][] board, int row, int col) {
        for (int i = 0; i < col; i++) {
            if (board[row][i] == 1) return false;
        }
        for (int i = row, j = col; i >= 0 && j >= 0; i--, j--) {
            if (board[i][j] == 1) return false;
        }
        for (int i = row, j = col; j >= 0 && i < board.length; i++, j--) {
            if (board[i][j] == 1) return false;
        }
        return true;
    }

}
