package Leetcode2;

import java.util.HashMap;
import java.util.Map;

public class NumOfWaysToPaintFence {

    /*
    You are painting a fence of n posts with k different colors.
    You must paint the posts following these rules:

    Every post must be painted exactly one color.
    There cannot be three or more consecutive posts with the same color.
    Given the two integers n and k, return the number of ways you can paint the fence.
     */
    public static void main(String[] args) {
        assert numWays(3, 2) == 6;
    }

    private static final Map<Integer, Integer> map = new HashMap<>();

    public static int numWays(int n, int k) {
        if (n == 1) return k;
        if (n == 2) return k * k;
        if (map.containsKey(n)) return map.get(n);
        map.put(n, (k - 1) * (numWays(n - 2, k) + numWays(n - 1, k)));
        return map.get(n);
    }

}
