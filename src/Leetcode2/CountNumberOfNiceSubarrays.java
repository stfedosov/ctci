package Leetcode2;

import java.util.HashMap;
import java.util.Map;

/*
Given an array of integers nums and an integer k.
A continuous subarray is called nice if there are k odd numbers on it.

Return the number of nice sub-arrays.
 */

public class CountNumberOfNiceSubarrays {

    public static void main(String[] args) {
        assert numberOfSubarrays(new int[]{1, 1, 2, 1, 1}, 3) == 2;
        assert numberOfSubarraysAlternative(new int[]{1, 1, 2, 1, 1}, 3) == 2;
        assert numberOfSubarrays(new int[]{2, 2, 2, 1, 2, 2, 1, 2, 2, 2}, 2) == 16;
        assert numberOfSubarraysAlternative(new int[]{2, 2, 2, 1, 2, 2, 1, 2, 2, 2}, 2) == 16;
    }

    public static int numberOfSubarrays(int[] A, int k) {
        return atMost(A, k) - atMost(A, k - 1);
    }

    public static int atMost(int[] arr, int k) {
        int res = 0, start = 0;
        int tmp = k;
        int end = 0;
        while (end < arr.length) {
            tmp -= arr[end] % 2;
            while (tmp < 0)
                tmp += arr[start++] % 2;
            res += end - start + 1;
            end++;
        }
        return res;
    }

    // ----- ALTERNATIVE WAY

    public static int numberOfSubarraysAlternative(int[] nums, int k) {
        //Replace all odd by 1 and even by 0
        for (int i = 0; i < nums.length; i++) {
            nums[i] = (nums[i] % 2 == 0) ? 0 : 1;
        }

        //problem becomes number of subarrays with sum = k
        Map<Integer, Integer> map = new HashMap<>();
        int res = 0;
        int sum = 0;//cumulative sum
        map.put(0, 1);
        for (int num : nums) {
            sum += num;
            if (map.containsKey(sum - k)) {
                res += map.get(sum - k);
            }
            map.put(sum, map.getOrDefault(sum, 0) + 1);
        }
        return res;
    }

}
