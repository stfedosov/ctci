package Leetcode2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class WebCrawler {

    public List<String> crawl(String startUrl, HtmlParser htmlParser) {
        ExecutorService executorService = Executors.newFixedThreadPool(20);
        Set<String> seen = new HashSet<>();
        Queue<String> q = new LinkedList<>();
        String hostname = getHostname(startUrl);

        q.offer(startUrl);
        seen.add(startUrl);

        try {
            while (!q.isEmpty()) {
                List<Task> tasks = new ArrayList<>();
                int size = q.size();
                while (size-- > 0) {
                    tasks.add(new Task(q.poll(), htmlParser));
                }

                List<Future<List<String>>> futures = executorService.invokeAll(tasks);

                for (Future<List<String>> future : futures) {
                    List<String> nextUrls = future.get();
                    for (String nextUrl : nextUrls) {
                        if (nextUrl.contains(hostname) && seen.add(nextUrl)) {
                            q.offer(nextUrl);
                        }
                    }
                }
            }
        } catch (InterruptedException | ExecutionException ignored) {
        }
        shutdownAndAwaitTermination(executorService);
        return new ArrayList<>(seen);
    }

    private void shutdownAndAwaitTermination(ExecutorService executorService) {
        try {
            while (!executorService.awaitTermination(10, TimeUnit.MILLISECONDS)) {
                executorService.shutdownNow(); // Cancel currently executing tasks
            }
        } catch (InterruptedException e) {
            // (Re-)Cancel if current thread also interrupted
            executorService.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }

    private String getHostname(String url) {
        String[] arr1 = url.split("/");
        return arr1[0] + "//" + arr1[2];
    }

    private static class Task implements Callable<List<String>> {

        String url;
        HtmlParser htmlParser;

        Task(String url, HtmlParser htmlParser) {
            this.url = url;
            this.htmlParser = htmlParser;
        }

        @Override
        public List<String> call() {
            return htmlParser.getUrls(url);

        }
    }

    interface HtmlParser {
        List<String> getUrls(String url);
    }

    private class HtmlParserImpl implements HtmlParser {

        @Override
        public List<String> getUrls(String url) {
            return null;
        }
    }

}
