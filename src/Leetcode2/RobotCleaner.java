package Leetcode2;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class RobotCleaner {

    public static void main(String[] args) {
        Robot robot = new Robot(new int[][]{
                {1, 1, 1, 1, 1, 0, 1, 1},
                {1, 1, 1, 1, 1, 0, 1, 1},
                {1, 0, 1, 1, 1, 1, 1, 1},
                {0, 0, 0, 1, 0, 0, 0, 0},
                {1, 1, 1, 1, 1, 1, 1, 1}
        }, 1, 3);
        cleanRoom(robot);
        for (int[] row : robot.matrix) {
            System.out.println(Arrays.toString(row));
        }
    }

    static int[][] dirs = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

    public static void cleanRoom(Robot robot) {
        cleanRoom(robot, 0, 0, 0, new HashSet<>());
    }

    private static void cleanRoom(Robot robot, int row, int col, int direction, Set<String> visited) {
        String key = row + "," + col;
        visited.add(key);
        robot.clean();
        for (int i = 0; i < dirs.length; i++) {
            int newDirection = (direction + i) % 4;
            int newRow = row + dirs[newDirection][0];
            int newCol = col + dirs[newDirection][1];
            if (!visited.contains(newRow + "," + newCol) && robot.move()) {
                cleanRoom(robot, newRow, newCol, newDirection, visited);
                // go back to previous cell
                robot.turnRight();
                robot.turnRight();
                robot.move();
                robot.turnRight();
                robot.turnRight();
            }
            // turn right, to rotate clockwise
            robot.turnRight();
        }
    }

    private static class Robot {

        private final int[][] matrix;
        private int row, col;
        private boolean right = true, down = false, left = false, up = false;

        public Robot(int[][] matrix, int row, int col) {
            this.matrix = matrix;
            this.row = row;
            this.col = col;
        }

        // returns true if next cell is open and robot moves into the cell.
        // returns false if next cell is obstacle and robot stays on the current cell.
        boolean move() {
            if (right) {
                if (++col < matrix[row].length) {
                    return matrix[row][col] != 0;
                } else {
                    --col;
                    return false;
                }
            } else if (up) {
                if (--row >= 0) {
                    return matrix[row][col] != 0;
                } else {
                    ++row;
                    return false;
                }
            } else if (left) {
                if (--col >= 0) {
                    return matrix[row][col] != 0;
                } else {
                    ++col;
                    return false;
                }
            } else {
                if (++row < matrix.length) {
                    return matrix[row][col] != 0;
                } else {
                    --row;
                    return false;
                }
            }
        }

        // Robot will stay on the same cell after calling turnLeft/turnRight.
        // Each turn will be 90 degrees.
        void turnLeft() {
            if (right) {
                right = false;
                up = true;
            } else if (up) {
                up = false;
                left = true;
            } else if (left) {
                left = false;
                down = true;
            } else {
                down = false;
                right = true;
            }
        }

        void turnRight() {
            if (right) {
                right = false;
                down = true;
            } else if (down) {
                down = false;
                left = true;
            } else if (left) {
                left = false;
                up = true;
            } else {
                up = false;
                right = true;
            }
        }

        // Clean the current cell.
        void clean() {
            matrix[row][col] = 2;
        }
    }
}
