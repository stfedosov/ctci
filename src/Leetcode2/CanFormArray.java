package Leetcode2;

import java.util.HashMap;
import java.util.Map;

public class CanFormArray {

    public static void main(String[] args) {
        assert canFormArray(new int[]{75, 1, 60, 91, 84, 55, 5, 39, 19, 52, 38, 66, 14, 17, 49},
                new int[][]{
                        {60}, {52, 38}, {66}, {39}, {19}, {1}, {84, 55}, {17}, {14}, {49}, {91}, {5}, {75}
                });
        assert canFormArray(new int[]{91, 4, 64, 78},
                new int[][]{{78}, {4, 64}, {91}});
        assert !canFormArray(new int[]{49, 18, 16}, new int[][]{{16, 18, 49}});
    }

    public static boolean canFormArray(int[] arr, int[][] pieces) {
        Map<Integer, int[]> map = new HashMap<>();
        for (int[] piece : pieces) {
            map.put(piece[0], piece);
        }
        for (int i = 0; i < arr.length; i++) {
            if (map.containsKey(arr[i])) {
                int[] next = map.get(arr[i]);
                int j = i;
                for (int x = 0; j < arr.length && x < next.length; x++, j++) {
                    if (arr[j] != next[x]) {
                        return false;
                    }
                }
                i = j - 1;
            } else {
                return false;
            }
        }
        return true;
    }

}
