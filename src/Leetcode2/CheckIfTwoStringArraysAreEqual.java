package Leetcode2;

public class CheckIfTwoStringArraysAreEqual {

    public static void main(String[] args) {
        assert arrayStringsAreEqual(new String[]{"ab", "c"}, new String[]{"a", "bc"});
        assert !arrayStringsAreEqual(new String[]{"a", "cb"}, new String[]{"ab", "c"});
        assert arrayStringsAreEqual(new String[]{"abc", "d", "defg"}, new String[]{"abcddefg"});
    }

    public static boolean arrayStringsAreEqual(String[] word1, String[] word2) {
        int idx1 = 0, idx2 = 0, i1 = 0, i2 = 0;
        while (idx1 < word1.length && idx2 < word2.length) {
            if (word1[idx1].charAt(i1++) != word2[idx2].charAt(i2++)) return false;
            if (i1 >= word1[idx1].length()) {
                i1 = 0;
                idx1++;
            }
            if (i2 >= word2[idx2].length()) {
                i2 = 0;
                idx2++;
            }
        }
        return idx1 == word1.length && idx2 == word2.length;
    }

}
