package Leetcode2;

import java.util.Arrays;

public class MaxSumOfThreeSubarrays {

    public static void main(String[] args) {
        assert Arrays.equals(maxSumOfThreeSubarrays(new int[]{1, 2, 1, 2, 6, 7, 5, 1}, 2), new int[]{0, 3, 5});
    }

    public static int[] maxSumOfThreeSubarrays(int[] nums, int k) {
        int sum = 0;
        for (int i = 0; i < k; i++) {
            sum += nums[i];
        }

        // calculate sums of windows. Sums array size is less than actual array.
        int[] sums = new int[nums.length - k + 1];
        sums[0] = sum;

        for (int i = 1; i < sums.length; i++) {
            sum -= nums[i - 1];
            sum += nums[i - 1 + k];
            sums[i] = sum;
        }

        int[] leftBest = new int[sums.length];
        int best = 0;
        for (int i = 0; i < sums.length; i++) {
            if (sums[best] < sums[i]) {
                best = i;
            }
            leftBest[i] = best;
        }

        int[] rightBest = new int[sums.length];
        best = sums.length - 1;
        for (int i = sums.length - 1; i >= 0; i--) {
            if (sums[best] <= sums[i]) {
                best = i;
            }
            rightBest[i] = best;
        }

        // move m from k to (sum.length-k).
        // l becomes m-k, r becomes m+k
        // check is s[l]+s[m]+s[r] is max
        int m = k;
        int[] res = {leftBest[m - k], m, rightBest[m + k]};

        for (m = k; m < sums.length - k; m++) {
            if ((sums[res[0]] + sums[res[1]] + sums[res[2]]) < (sums[leftBest[m - k]] + sums[m] + sums[rightBest[m + k]])) {
                res = new int[]{leftBest[m - k], m, rightBest[m + k]};
            }
        }
        return res;
    }
}
