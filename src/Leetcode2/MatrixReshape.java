package Leetcode2;

import java.util.Arrays;

public class MatrixReshape {

    public static void main(String[] args) {
        assert Arrays.deepEquals(matrixReshape(new int[][]{{1, 2}, {3, 4}}, 1, 4), new int[][]{{1, 2, 3, 4}});
        assert Arrays.deepEquals(matrixReshape(new int[][]{{1, 2}, {3, 4}}, 2, 4), new int[][]{{1, 2}, {3, 4}});
    }

    public static int[][] matrixReshape(int[][] matrix, int r, int c) {
        int rows = matrix.length;
        int[][] res = new int[r][c];
        int cols = matrix[0].length;
        if (r * c != matrix.length * matrix[0].length) return matrix;
        int rows_new = 0, cols_new = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                res[rows_new][cols_new] = matrix[i][j];
                cols_new++;
                if (cols_new == c) {
                    rows_new++;
                    cols_new = 0;
                }
            }
        }
        return res;
    }

}
