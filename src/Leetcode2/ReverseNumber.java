package Leetcode2;

public class ReverseNumber {

    public static void main(String[] args) {
        assert reverse(123) == 321;
        assert reverse(-123) == -321;
    }

    public static int reverse(int x) {
        long result = 0;
        while (x != 0) {
            int mod = x % 10;
            x = x / 10;
            result = result * 10 + mod;
            if (result > Integer.MAX_VALUE || result < Integer.MIN_VALUE) return 0;
        }
        return (int) result;
    }

}
