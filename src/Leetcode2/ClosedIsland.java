package Leetcode2;

public class ClosedIsland {

    public static void main(String[] args) {
        assert closedIsland(new int[][]{
                {1, 1, 1, 1, 1, 1, 1, 0},
                {1, 0, 0, 0, 0, 1, 1, 0},
                {1, 0, 1, 0, 1, 1, 1, 0},
                {1, 0, 0, 0, 0, 1, 0, 1},
                {1, 1, 1, 1, 1, 1, 1, 0}}) == 2;
    }

    public static int closedIsland(int[][] grid) {
        int rows = grid.length, cols = grid[0].length;
        //sink left col and right col
        for (int i = 0; i < rows; i++) {
            if (grid[i][0] == 0) sink(grid, i, 0);
            if (grid[i][cols - 1] == 0) sink(grid, i, cols - 1);
        }
        //sink top row and bottom row
        for (int j = 0; j < cols; j++) {
            if (grid[0][j] == 0) sink(grid, 0, j);
            if (grid[rows - 1][j] == 0) sink(grid, rows - 1, j);
        }
        //count islands not at edge
        int islands = 0;
        for (int i = 1; i < rows - 1; i++) {
            for (int j = 1; j < cols - 1; j++) {
                if (grid[i][j] == 0) {
                    islands++;
                    sink(grid, i, j);
                }
            }
        }
        return islands;
    }

    private static void sink(int[][] grid, int x, int y) {
        if (x < 0 || y < 0 || x == grid.length || y == grid[0].length || grid[x][y] == 1) return;
        grid[x][y] = 1;
        sink(grid, x + 1, y);
        sink(grid, x - 1, y);
        sink(grid, x, y + 1);
        sink(grid, x, y - 1);
    }

}
