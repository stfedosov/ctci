package Leetcode2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SuggestedProducts {

    public static void main(String[] args) {
        System.out.println(suggestedProducts(new String[]{"mobile", "mouse", "moneypot", "monitor", "mousepad"}, "mouse"));
        System.out.println(suggestedProductsBinarySearch(new String[]{"mobile", "mouse", "moneypot", "monitor", "mousepad"}, "mouse"));
    }

    // Time complexity : O(nlog(n)) + O(mlog(n))
    // Where n is the length of products and m is the length of the search word.
    public static List<List<String>> suggestedProductsBinarySearch(String[] products, String searchWord) {
        List<List<String>> res = new ArrayList<>();
        Arrays.sort(products);
        String tmp = "";
        for (char c : searchWord.toCharArray()) {
            List<String> suggestions = new ArrayList<>();
            tmp += c;
            int insertionPoint = Arrays.binarySearch(products, tmp);
            if (insertionPoint < 0) insertionPoint = -insertionPoint - 1;
            for (int i = insertionPoint; i < products.length; i++) {
                if (suggestions.size() == 3) break;
                if (products[i].startsWith(tmp)) {
                    suggestions.add(products[i]);
                }
            }
            res.add(suggestions);
        }
        return res;
    }

    // Time complexity: O(len(prefix))
    // Space complexity: O(26*N), where N is the number of nodes in the tree

    private static final TrieNode root = new TrieNode();

    public static List<List<String>> suggestedProducts(String[] products,
                                                       String searchWord) {

        List<List<String>> result = new ArrayList<>();
        for (String w : products) insert(w);
        String prefix = "";
        for (char c : searchWord.toCharArray()) {
            prefix += c;
            result.add(startsWith(prefix));
        }
        return result;
    }


    // NodeCopy definition of a trie
    private static class TrieNode {
        boolean isComplete = false;
        Map<Character, TrieNode> children = new HashMap<>();
    }

    private static List<String> startsWith(String prefix) {
        TrieNode curr = root;
        List<String> list = new ArrayList<>();
        for (char c : prefix.toCharArray()) {
            if (!curr.children.containsKey(c))
                return Collections.emptyList();
            curr = curr.children.get(c);
        }
        search(curr, prefix, list);
        return list;
    }

    private static void search(TrieNode curr, String word, List<String> list) {
        if (list.size() == 3)
            return;
        if (curr.isComplete)
            list.add(word);

        // Run DFS on all possible paths preserving the alphabet order
        for (char c = 'a'; c <= 'z'; c++)
            if (curr.children.containsKey(c)) search(curr.children.get(c), word + c, list);
    }

    private static void insert(String s) {
        TrieNode curr = root;
        for (char c : s.toCharArray()) {
            if (curr.children.get(c) == null)
                curr.children.put(c, new TrieNode());
            curr = curr.children.get(c);
        }
        curr.isComplete = true;
    }
}

