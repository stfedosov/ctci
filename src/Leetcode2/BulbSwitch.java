package Leetcode2;

public class BulbSwitch {

    public int bulbSwitch(int n) {
        //find all perfect squares less than or equal to n
        int count = 0;
        for (long i = 1; i * i <= n; i++) {
            count++;
        }
        return count;
    }

}
