package Leetcode2;

import java.util.PriorityQueue;
import java.util.Queue;

public class JumpGame {

    // You are initially standing at index 0.
    // In one move, you can jump at most k steps forward without going outside the boundaries of the array.
    // That is, you can jump from index i to any index in the range [i + 1, min(n - 1, i + k)] inclusive.
    public static void main(String[] args) {
        assert maxResult(new int[]{1, -1, -2, 4, -7, 3}, 2) == 7;
        assert maxResult(new int[]{10, -5, -2, 4, 0, 3}, 3) == 17;
        assert maxResult(new int[]{1, -5, -20, 4, -1, 3, -6, -3}, 2) == 0;
    }

    // O(n*log(n))

    public static int maxResult(int[] nums, int k) {
        Queue<int[]> maxHeap = new PriorityQueue<>((a, b) -> b[0] - a[0]);
        maxHeap.offer(new int[]{nums[0], 0});
        int max = nums[0];
        for (int i = 1; i < nums.length; i++) {
            while (maxHeap.peek()[1] < i - k) {
                maxHeap.poll();
            }
            max = nums[i] + maxHeap.peek()[0];
            maxHeap.offer(new int[]{max, i});
        }
        return max;
    }

    // Brute force ----- O(k^n);
    // Even with cache this method will be slow as we need to choose k new elements for each number

    public int maxResultBruteForce(int[] nums, int k) {
        return nums[0] + maxResult(nums, 0, k);
    }

    private int maxResult(int[] nums, int idx, int k) {
        if (idx == nums.length) return 0;
        int max = Integer.MIN_VALUE;
        for (int i = idx + 1; i <= Math.min(nums.length - 1, idx + k); i++) {
            max = Math.max(max, nums[i] + maxResult(nums, i, k));
        }
        return max == Integer.MIN_VALUE ? 0 : max;
    }

}
