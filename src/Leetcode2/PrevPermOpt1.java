package Leetcode2;

import java.util.Arrays;

public class PrevPermOpt1 {

    // Given an array of positive integers arr (not necessarily distinct),
    // return the lexicographically largest permutation that is smaller than arr,
    // that can be made with exactly one swap
    public static void main(String[] args) {
        assert Arrays.equals(prevPermOpt1(new int[]{3, 2, 1}), new int[]{3, 1, 2});
        assert Arrays.equals(prevPermOpt1(new int[]{1, 1, 5}), new int[]{1, 1, 5});
        assert Arrays.equals(prevPermOpt1(new int[]{1, 9, 4, 6, 7}), new int[]{1, 7, 4, 6, 9});
        assert Arrays.equals(prevPermOpt1(new int[]{3, 1, 1, 3}), new int[]{1, 3, 1, 3});
    }

    // idea is to go from the end of the array and find such transition point
    // where array starts increasing, then just swap it's position with the max from that suffix
    // 1, 2, 3
    // 1, 3, 2
    // 2, 1, 3
    // 2, 3, 1
    // 3, 1, 2 <-- previous permutation
    // 3, 2, 1 <-- we're here

    // edge case: what if we have only one element?
    public static int[] prevPermOpt1(int[] A) {
        if (A.length < 2) return A;
        int transitionPos = -1;
        for (int i = A.length - 1; i > 0; i--) {
            if (A[i - 1] > A[i]) {
                transitionPos = i - 1;
                break;
            }
        }
        // decreasing order -> no need to do anything else
        if (transitionPos == -1) return A;
        int maxPos = transitionPos + 1;
        for (int i = transitionPos + 1; i < A.length; i++) {
            if (A[i] >= A[transitionPos]) break;
            if (A[maxPos] < A[i]) {
                maxPos = i;
            }
        }
        swap(A, transitionPos, maxPos);
        return A;
    }

    private static void swap(int[] A, int x, int y) {
        int tmp = A[x];
        A[x] = A[y];
        A[y] = tmp;
    }

}
