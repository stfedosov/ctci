package Leetcode2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class OnesAndZeroes {

    public static void main(String[] args) {
        assert findMaxFormTopDown(new String[]{"10", "0001", "111001", "1", "0"}, 5, 3) == 4;
        assert findMaxFormTopDown(new String[]{"10", "0", "1"}, 1, 1) == 2;

        assert findMaxFormBottomUp(new String[]{"10", "0001", "111001", "1", "0"}, 5, 3) == 4;
        assert findMaxFormBottomUp(new String[]{"10", "0", "1"}, 1, 1) == 2;
    }

    // --------- Bottom-up approach

    public static int findMaxFormBottomUp(String[] strs, int m, int n) {
        int[][] dp = new int[m + 1][n + 1];
        for (String s : strs) {
            int[] count = countOnesAndZeroes(s);
            for (int zero = m; zero >= count[0]; zero--) {
                for (int one = n; one >= count[1]; one--) {
                    dp[zero][one] = Math.max(dp[zero][one], 1 + dp[zero - count[0]][one - count[1]]);
                }
            }
        }
        return dp[m][n];
    }

    private static int[] countOnesAndZeroes(String s) {
        int[] res = new int[2];
        for (char c : s.toCharArray()) res[c - '0']++;
        return res;
    }

    // --------- Top-down approach

    public static int findMaxFormTopDown(String[] strs, int m, int n) {
        Map<String, int[]> map = new HashMap<>();
        for (String s : strs) {
            map.put(s, count(s));
        }
        int[][][] cache = new int[600][105][105];
        for (int[][] c : cache) {
            for (int[] cc : c) {
                Arrays.fill(cc, -1);
            }
        }
        return dfs(strs, map, 0, 0, m, n, cache);
    }

    private static int dfs(String[] strs, Map<String, int[]> map, int i, int c, int m, int n, int[][][] cache) {
        if (m == 0 && n == 0) {
            return 0;
        }
        if (i == strs.length) return 0;
        if (cache[i][m][n] != -1) return cache[i][m][n];

        int[] mn = map.get(strs[i]);
        if (mn[0] > m || mn[1] > n) return dfs(strs, map, i + 1, c, m, n, cache);
        cache[i][m][n] = Math.max(1 + dfs(strs, map, i + 1, c + 1, m - mn[0], n - mn[1], cache),
                dfs(strs, map, i + 1, c, m, n, cache));
        return cache[i][m][n];

    }

    private static int[] count(String s) {
        int m = 0, n = 0;
        for (char c : s.toCharArray()) {
            if (c == '0') m++;
            else n++;
        }
        return new int[]{m, n};
    }

}
