package Leetcode2;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class MinimumAreaRectangle {

    // If we have two points x1, y1 and x2, y2, seeing this as diagonal, a rectangle
    // with sides parallel to x and y axis, will have other two points as x1, y2 and x2, y1.
    public static void main(String[] args) {
        assert minAreaRect(new int[][]{{1, 1}, {1, 3}, {3, 1}, {3, 3}, {2, 2}}) == 4;
        assert minAreaRect(new int[][]{{1, 1}, {1, 3}, {3, 1}, {3, 3}, {4, 1}, {4, 3}}) == 2;
    }

    public static int minAreaRect(int[][] points) {
        Set<Node> actualNodes = new HashSet<>();
        for (int[] point : points) {
            actualNodes.add(new Node(point[0], point[1]));
        }
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < points.length; i++) {
            for (int j = i + 1; j < points.length; j++) {
                int x1 = points[i][0], y1 = points[i][1], x2 = points[j][0], y2 = points[j][1];
                Node potential1 = new Node(x1, y2), potential2 = new Node(x2, y1);
                if (actualNodes.contains(potential1) && actualNodes.contains(potential2)) {
                    min = Math.min(min, Math.abs(y2 - y1) * Math.abs(x2 - x1));
                }
            }
        }
        return min == Integer.MAX_VALUE ? 0 : min;
    }

    static class Node {
        int x, y;

        public Node(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object obj) {
            return (((Node) obj).x == this.x) && (((Node) obj).y == this.y);
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }
    }

}
