package Leetcode2;

public class NumOfSubmatricesSumToTarget {

    public static void main(String[] args) {
        assert numSubmatrixSumTarget(new int[][]{{-1, 1}, {1, -1}}, 0) == 5;
        assert numSubmatrixSumTarget(new int[][]{{0, 1, 0}, {1, 1, 1}, {0, 1, 0}}, 0) == 4;
    }

    public static int numSubmatrixSumTarget(int[][] matrix, int target) {
        int[][] prefixSum = new int[matrix.length + 1][matrix[0].length + 1];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                prefixSum[i + 1][j + 1] = prefixSum[i + 1][j] + prefixSum[i][j + 1] + matrix[i][j] - prefixSum[i][j];
            }
        }
        int count = 0;
        for (int i = 0; i < prefixSum.length; i++) {
            for (int j = 0; j < prefixSum[i].length; j++) {
                for (int k = i; k < prefixSum.length - 1; k++) {
                    for (int z = j; z < prefixSum[i].length - 1; z++) {
                        if (sumRegion(prefixSum, i, j, k, z) == target) count++;
                    }
                }
            }
        }
        return count;
    }

    private static int sumRegion(int[][] prefixSum, int row1, int col1, int row2, int col2) {
        return prefixSum[row2 + 1][col2 + 1] - prefixSum[row1][col2 + 1] - prefixSum[row2 + 1][col1] + prefixSum[row1][col1];
    }

}
