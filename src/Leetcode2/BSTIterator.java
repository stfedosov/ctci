package Leetcode2;

import Leetcode.TreeNode;

import java.util.ArrayDeque;
import java.util.Deque;

// average O(1) time
// O(H) memory, where H is the height of the tree
public class BSTIterator {

    private Deque<TreeNode> stack;

    public BSTIterator(TreeNode root) {
        this.stack = new ArrayDeque<>();
        fillLeft(root);
    }

    private void fillLeft(TreeNode root) {
        if (root == null) return;
        stack.push(root);
        fillLeft(root.left);
    }

    // O(1) amortized time complexity, with O(N) in a worst case
    public int next() {
        TreeNode popped = stack.pop();
        if (popped.right != null) fillLeft(popped.right);
        return popped.val;
    }

    public boolean hasNext() {
        return stack.size() > 0;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(7);
        root.left = new TreeNode(3);
        TreeNode right15 = new TreeNode(15);
        root.right = right15;
        right15.left = new TreeNode(9);
        right15.right = new TreeNode(20);
        BSTIterator iterator = new BSTIterator(root);
        assert iterator.next() == 3;
        assert iterator.next() == 7;
        assert iterator.hasNext();
        assert iterator.next() == 9;
        assert iterator.hasNext();
        assert iterator.next() == 15;
        assert iterator.hasNext();
        assert iterator.next() == 20;
        assert !iterator.hasNext();
    }

}
