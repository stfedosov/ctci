package Leetcode2;

import java.util.Arrays;

public class SortColorsInPlace {

    public static void main(String[] args) {
        int[] colors = {0, 1, 0, 2, 0, 1, 2, 2};
        System.out.println(Arrays.toString(sortColors(colors)));

        colors = new int[]{2, 0, 2, 1, 1, 0};
        System.out.println(Arrays.toString(sortColors(colors)));

        colors = new int[]{1, 0, 2};
        System.out.println(Arrays.toString(sortColors(colors)));

        colors = new int[]{1, 0, 0};
        System.out.println(Arrays.toString(sortColors(colors)));

        colors = new int[]{0};
        System.out.println(Arrays.toString(sortColors(colors)));

        colors = new int[]{0, 1, 2, 2, 2, 1, 1, 0, 0, 0, 0, 1, 2, 2};
        System.out.println(Arrays.toString(sortColors(colors)));

        colors = new int[]{2, 1, 1, 1, 1, 1, 1, 1, 2, 0, 0, 0, 0, 2};
        System.out.println(Arrays.toString(sortColors(colors)));

        colors = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
        System.out.println(Arrays.toString(sortColors(colors)));

        colors = new int[]{1, 1, 1};
        System.out.println(Arrays.toString(sortColors(colors)));
    }

    public static int[] sortColors(int[] colors) {
        int start = 0;
        int current = 0;
        int end = colors.length - 1;
        while (current <= end) {

            if (colors[current] == 0) {
                swap(colors, start, current);
                current++;
                start++;
            } else if (colors[current] == 1) {
                current++;
            } else {
                swap(colors, current, end);
                end--;
            }
        }
        return colors;
    }

    private static void swap(int[] colors, int a, int b) {
        int tmp = colors[a];
        colors[a] = colors[b];
        colors[b] = tmp;
    }

    /*

    public static void sortColors(int[] nums) {
        int countOfZeros = 0;
        int countOfOnes = 0;
        for (int x : nums) {
            if (x == 0) countOfZeros++;
            if (x == 1) countOfOnes++;
        }
        for (int i = 0; i < nums.length; i++) {
            if (i < countOfZeros) {
                nums[i] = 0;
            } else if (i < countOfZeros + countOfOnes) {
                nums[i] = 1;
            } else {
                nums[i] = 2;
            }
        }
    }

     */

}
