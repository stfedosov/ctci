package Leetcode2;

public class IsMajorityElement {

    public static void main(String[] args) {
        assert isMajorityElement(new int[]{2, 4, 5, 5, 5, 5, 5, 6, 6}, 5);
        assert !isMajorityElement(new int[]{10, 100, 101, 101}, 101);
    }

    public static boolean isMajorityElement(int[] nums, int target) {
        int left = search(nums, target, true);
        if (left == -1) return false;
        int right = search(nums, target, false);
        return right - left >= nums.length / 2;
    }

    public static int search(int[] nums, int target, boolean isFirst) {
        int l = 0, h = nums.length - 1, idx = -1;
        while (l <= h) {
            int mid = l + (h - l) / 2;
            if (nums[mid] == target) {
                idx = mid;
                if (isFirst) h = mid - 1;
                else l = mid + 1;
            } else if (nums[mid] < target) {
                l = mid + 1;
            } else {
                h = mid - 1;
            }
        }
        return idx;
    }

}
