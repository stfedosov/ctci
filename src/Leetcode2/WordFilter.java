package Leetcode2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WordFilter {

    public static void main(String[] args) {
        WordFilter wordFilter = new WordFilter(new String[]{"apple", "apod", "mac", "iphone"});
        assert wordFilter.f("a", "e") == 0;
        assert wordFilter.f("a", "m") == -1;
        assert wordFilter.f("m", "c") == 2;
        assert wordFilter.f("ma", "c") == 2;

        wordFilter = new WordFilter(new String[]{"cabaabaaaa", "ccbcababac", "bacaabccba", "bcbbcbacaa", "abcaccbcaa", "accabaccaa", "cabcbbbcca", "ababccabcb", "caccbbcbab", "bccbacbcba"});
        assert wordFilter.f("ab", "abcaccbcaa") == 4;
    }

    private Map<String, List<Integer>> prefixes = new HashMap<>();
    private Map<String, List<Integer>> suffixes = new HashMap<>();

    public WordFilter(String[] words) {
        for (int i = 0; i < words.length; i++) {
            for (int j = 0; j <= words[i].length(); j++) {
                prefixes.computeIfAbsent(words[i].substring(0, j), x -> new ArrayList<>()).add(i);
            }
            for (int j = 0; j <= words[i].length(); j++) {
                suffixes.computeIfAbsent(words[i].substring(words[i].length() - j), x -> new ArrayList<>()).add(i);
            }
        }
    }

    public int f(String prefix, String suffix) {
        if (!prefixes.containsKey(prefix) || !suffixes.containsKey(suffix)) return -1;
        List<Integer> p = prefixes.get(prefix);
        List<Integer> s = suffixes.get(suffix);
        int i = p.size() - 1, j = s.size() - 1;
        while (i >= 0 && j >= 0) {
            if (p.get(i) < s.get(j)) j--;
            else if (p.get(i) > s.get(j)) i--;
            else return p.get(i);
        }
        return -1;
    }
}
