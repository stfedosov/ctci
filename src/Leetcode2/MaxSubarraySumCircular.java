package Leetcode2;

public class MaxSubarraySumCircular {

    public static void main(String[] args) {
        assert maxSubarraySumCircular(new int[]{-1, -2, -3}) == -1;
        assert maxSubarraySumCircular(new int[]{1, -2, 3, -2}) == 3; // 3
        assert maxSubarraySumCircular(new int[]{5, -3, 5}) == 10; // 5 + 5
        assert maxSubarraySumCircular(new int[]{3, -1, 2, -1}) == 4; // 2 + (-1) + 3 = 4
    }

    // Case 1. The first is that the subarray take only a middle part, and we know how to find the max subarray sum.
    // Case2. The second is that the subarray take a part of head array and a part of tail array.
    // In other words (for the 2nd case), maximum result = total sum - minimum subarray sum.

    // Edge case: if all numbers are negative, then totalSum == minSoFar, so we can return maxSoFar

    public static int maxSubarraySumCircular(int[] nums) {
        int maxSoFar = Integer.MIN_VALUE,
                tmp_sumMax = 0,
                minSoFar = Integer.MAX_VALUE,
                tmp_sumMin = 0,
                totalSum = 0;
        for (int x : nums) {
            tmp_sumMax = Math.max(x, tmp_sumMax + x);
            maxSoFar = Math.max(maxSoFar, tmp_sumMax);
            tmp_sumMin = Math.min(x, tmp_sumMin + x);
            minSoFar = Math.min(minSoFar, tmp_sumMin);
            totalSum += x;
        }
        return maxSoFar > 0 ? Math.max(maxSoFar, totalSum - minSoFar) : maxSoFar;
    }

}
