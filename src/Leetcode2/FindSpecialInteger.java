package Leetcode2;

public class FindSpecialInteger {

    // Given an integer array sorted in non-decreasing order, there is exactly one integer
    // in the array that occurs more than 25% of the time, return that integer.
    public static void main(String[] args) {
        assert findSpecialInteger(new int[]{1, 2, 2, 6, 6, 6, 6, 7, 10}) == 6;
    }

    public static int findSpecialInteger(int[] arr) {
        int targetCount = arr.length / 4;
        for (int i = 0; i < arr.length; i++) {
            int borderEnd = findBorderEnd(arr, arr[i]);
            if (borderEnd > 0 && borderEnd - i + 1 > targetCount) return arr[i];
        }
        return 1;
    }

    private static int findBorderEnd(int[] arr, int target) {
        int left = 0, right = arr.length - 1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (arr[mid] == target) {
                if (mid + 1 < arr.length && arr[mid + 1] == arr[mid]) {
                    left = mid + 1;
                } else {
                    return mid;
                }
            } else if (arr[mid] < target) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return 0;
    }

}
