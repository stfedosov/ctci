package Leetcode2;

import java.util.HashSet;
import java.util.Set;

public class RemoveStones {

    public static void main(String[] args) {
        assert removeStones(new int[][]{{0, 1}, {1, 0}, {1, 1}}) == 2;
        assert removeStones(new int[][]{{0, 0}, {0, 1}, {1, 0}, {1, 2}, {2, 1}, {2, 2}}) == 5;
    }

    // ultimately, we just need to count the number of islands we have and that connected component can be removed entirely
    // but since we can't remove everything, we have to leave at least one or several stones.
    public static int removeStones(int[][] stones) {
        Set<String> seen = new HashSet<>();
        int islands = 0;
        for (int[] stone : stones) {
            String key = stone[0] + "," + stone[1];
            if (!seen.contains(key)) {
                dfs(stones, stone[0], stone[1], seen);
                islands++;
            }
        }
        return stones.length - islands;
    }

    private static void dfs(int[][] stones, int x, int y, Set<String> seen) {
        seen.add(x + "," + y);
        for (int[] stone : stones) {
            String key = stone[0] + "," + stone[1];
            if (!seen.contains(key) && (stone[0] == x || stone[1] == y)) {
                dfs(stones, stone[0], stone[1], seen);
            }
        }
    }

}
