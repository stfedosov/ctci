package Leetcode2;

import java.util.Arrays;

public class Minesweeper {

    public static void main(String[] args) {
        char[][] output = updateBoard(new char[][]{
                {'E', 'E', 'E', 'E', 'E'},
                {'E', 'E', 'M', 'E', 'E'},
                {'E', 'E', 'E', 'E', 'E'},
                {'E', 'E', 'E', 'E', 'E'}
        }, new int[]{3, 0});
        for (char[] o : output) {
            System.out.println(Arrays.toString(o));
        }
    }

    public static char[][] updateBoard(char[][] board, int[] click) {
        if (board[click[0]][click[1]] == 'M') {
            board[click[0]][click[1]] = 'X';
            return board;
        }
        dfs(board, click[0], click[1]);
        return board;
    }

    private static void dfs(char[][] board, int row, int column) {
        if (row < 0 || column < 0 || row >= board.length || column >= board[row].length || board[row][column] != 'E') {
            return;
        }
        int mines = collectMines(board, row, column);
        if (mines == 0) {
            board[row][column] = 'B';
            for (int i = row - 1; i <= row + 1; i++) {
                for (int j = column - 1; j <= column + 1; j++) {
                    dfs(board, i, j);
                }
            }
        } else {
            board[row][column] = (char) ('0' + mines);
        }
    }

    private static int collectMines(char[][] board, int row, int column) {
        int count = 0;
        for (int i = row - 1; i <= row + 1; i++) {
            for (int j = column - 1; j <= column + 1; j++) {
                count += isMine(board, i, j);
            }
        }
        return count;
    }

    private static int isMine(char[][] board, int row, int column) {
        if (row < 0 || column < 0 || row >= board.length || column >= board[row].length) {
            return 0;
        }
        return board[row][column] == 'M' ? 1 : 0;
    }

}
