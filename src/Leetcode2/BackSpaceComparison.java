package Leetcode2;

import java.util.Stack;

public class BackSpaceComparison {

    public static void main(String[] args) {
        assert backspaceCompare("ab#c", "ad#c");
        assert !backspaceCompare("a#c", "b");

        assert backspaceCompareConstantSpace("ab#c", "ad#c");
        assert !backspaceCompareConstantSpace("a#c", "b");
    }

    public static boolean backspaceCompare(String S, String T) {
        return evaluate(S).equals(evaluate(T));
    }

    private static String evaluate(String s) {
        Stack<Character> s1 = new Stack<>();
        for (char c : s.toCharArray()) {
            if (c == '#') {
                if (s1.isEmpty()) continue;
                s1.pop();
            } else {
                s1.push(c);
            }
        }
        StringBuilder sb = new StringBuilder();
        while (!s1.isEmpty()) sb.append(s1.pop());
        return sb.toString();
    }

    public static boolean backspaceCompareConstantSpace(String S, String T) {
        int i = S.length() - 1;
        int j = T.length() - 1;
        while (i >= 0 || j >= 0) {
            i = backspace(S, i);
            j = backspace(T, j);
            if (i < 0 && j < 0) return true;
            if (i < 0 || j < 0 || S.charAt(i) != T.charAt(j)) return false;
            i--;
            j--;
        }
        return true;
    }

    private static int backspace(String S, int i) { // back to the remaining character
        int skip = 0;
        while (i >= 0 && (skip > 0 || S.charAt(i) == '#')) {
            if (S.charAt(i) == '#') skip++;
            else skip--;
            i--;
        }
        return i;
    }

}
