package Leetcode2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Intersection {

    public static void main(String[] args) {
        assert Arrays.equals(intersect(new int[]{1, 2, 2, 1}, new int[]{2, 2}), new int[]{2, 2});
        assert Arrays.equals(intersect(new int[]{4, 9, 5}, new int[]{9, 4, 9, 8, 4}), new int[]{4, 9});
        assert Arrays.equals(intersect(new int[]{3, 1, 2}, new int[]{1, 1}), new int[]{1});
    }

    public static int[] intersect(int[] nums1, int[] nums2) {
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        List<Integer> res = new ArrayList<>();
        int i = 0, j = 0;
        while (i < nums1.length && j < nums2.length) {
            if (nums1[i] == nums2[j]) {
                res.add(nums1[i]);
                i++;
                j++;
            } else if (nums1[i] < nums2[j]) {
                i++;
            } else {
                j++;
            }
        }
        i = 0;
        int[] result = new int[res.size()];
        for (Integer num : res) {
            result[i++] = num;
        }
        return result;
    }

    public static int[] intersect2(int[] nums1, int[] nums2) {
        if (nums1 == null || nums2 == null || nums1.length == 0 || nums2.length == 0) return new int[0];
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums1) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }

        List<Integer> res = new ArrayList<>();
        for (int num : nums2) {
            if (map.containsKey(num) && map.get(num) > 0) {
                res.add(num);
                int freq = map.get(num);
                map.put(num, freq - 1);
            }
        }

        int i = 0;
        int[] result = new int[res.size()];
        for (Integer num : res) {
            result[i++] = num;
        }
        return result;
    }

}
