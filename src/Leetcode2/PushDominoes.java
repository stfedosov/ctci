package Leetcode2;

public class PushDominoes {

    public static void main(String[] args) {
        System.out.println(pushDominoes("R..L"));
        System.out.println(pushDominoes("R...L"));
        System.out.println(pushDominoes("L...R"));
        System.out.println(pushDominoes("..L...R.."));
        System.out.println(pushDominoes(".L.R...LR..L.."));
        System.out.println(pushDominoes("R..L..R..LR.R.R....."));
    }

    public static String pushDominoes(String dominoes) {
        int i = 0, j = 1;
        char[] chars = dominoes.toCharArray();
        while (j < chars.length) {
            if (chars[i] == 'R' && chars[j] == '.') {
                while (j < chars.length - 1 && chars[j] == '.') j++;
                if (chars[j] == 'L') {
                    i = evaluateDominoes(i, j, chars);
                } else {
                    while (i < j) chars[i++] = 'R';
                    if (chars[j] == '.') chars[j] = 'R';
                }
            } else if (chars[i] == '.' && chars[j] == 'L') {
                while (i > 0 && chars[i] == '.') i--;
                if (chars[i] == 'R') {
                    i = evaluateDominoes(i, j, chars);
                } else {
                    while (i < j) chars[i++] = 'L';
                }
            } else {
                i++;
            }
            j++;
        }
        return new String(chars);
    }

    private static int evaluateDominoes(int i, int j, char[] chars) {
        int mid = i + (j - i) / 2;
        if ((j - i - 1) % 2 == 0) {
            while (i <= mid) chars[i++] = 'R';
        } else {
            while (i < mid) chars[i++] = 'R';
            chars[mid] = '.';
            i++;
        }
        while (i < j) chars[i++] = 'L';
        return i;
    }

}
