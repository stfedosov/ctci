package Leetcode2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MinimumSemesters {

    // Our task is to find the length of longest path in the graph
    // If DAG contains a loop, return -1.
    public static void main(String[] args) {
        assert minimumSemesters(3, new int[][]{{1, 3}, {2, 3}}) == 2;
        max = 0;
        assert minimumSemesters(3, new int[][]{{1, 2}, {2, 3}, {3, 1}}) == -1;
    }

    static int max = 0;

    public static int minimumSemesters(int n, int[][] relations) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        for (int[] relation : relations) {
            map.computeIfAbsent(relation[0], x -> new ArrayList<>()).add(relation[1]);
        }
        int[] visited = new int[n + 1];
        int[] distance = new int[n + 1];
        for (int i = 1; i <= n; i++) {
            if (visited[i] == 0 && !canFinish(map, i, visited, distance)) return -1;
        }
        return max + 1;
    }

    private static boolean canFinish(Map<Integer, List<Integer>> map, int i, int[] visited, int[] distance) {
        if (visited[i] == 1) return false;
        if (visited[i] == 2) return true;
        visited[i] = 1;
        for (int child : map.getOrDefault(i, Collections.emptyList())) {
            if (!canFinish(map, child, visited, distance)) return false;
            else distance[i] = Math.max(distance[i], distance[child] + 1);
        }
        max = Math.max(max, distance[i]);
        visited[i] = 2;
        return true;
    }

}
