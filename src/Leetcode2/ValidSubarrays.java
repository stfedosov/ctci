package Leetcode2;

import java.util.Stack;

public class ValidSubarrays {

    /*
    Given an integer array nums.
    Return the number of non-empty subarrays with the leftmost element not larger than other elements in the subarray.
     */
    public static void main(String[] args) {
        System.out.println(validSubarrays(new int[]{1, 4, 2, 5, 3}));
    }

    public static int validSubarrays(int[] A) {
        Stack<Integer> stack = new Stack<>();
        int count = 0;
        for (int x : A) {
            while (!stack.isEmpty() && x < stack.peek()) stack.pop();
            stack.push(x);
            count += stack.size();
        }
        return count;
    }

}
