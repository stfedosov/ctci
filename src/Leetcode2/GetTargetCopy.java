package Leetcode2;

import Leetcode.TreeNode;

public class GetTargetCopy {

    public static void main(String[] args) {

    }

    public static TreeNode getTargetCopy(final TreeNode original,
                                         final TreeNode cloned,
                                         final TreeNode target) {
        if (original == null || original == target)
            return cloned;
        TreeNode res = getTargetCopy(original.left, cloned.left, target);
        if (res != null)
            return res;
        return getTargetCopy(original.right, cloned.right, target);
    }

}
