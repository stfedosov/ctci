package Leetcode2;

public class ScoreOfParentheses {

    public static void main(String[] args) {
        assert scoreOfParentheses("(())") == 2;
        assert scoreOfParentheses("()()") == 2;
        assert scoreOfParentheses("(()(()))") == 6;
        assert scoreOfParentheses("(((())))") == 8;
    }

    public static int scoreOfParentheses(String S) {
        int score = 0, depth = 0;
        for (int i = 0; i < S.length(); i++) {
            if (S.charAt(i) == '(') {
                depth++;
            } else {
                depth--;
            }
            if (S.charAt(i) == ')' && S.charAt(i - 1) == '(') {
                score += Math.pow(2, depth);
            }
        }
        return score;
    }

}
