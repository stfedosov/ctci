package Leetcode2;

import java.util.Stack;

public class ValidateStackSequences {

    public static void main(String[] args) {
        assert validateStackSequences(new int[]{1, 2, 3, 4, 5}, new int[]{4, 5, 3, 2, 1});
        assert !validateStackSequences(new int[]{1, 2, 3, 4, 5}, new int[]{4, 3, 5, 1, 2});
    }

    public static boolean validateStackSequences(int[] pushed, int[] popped) {
        Stack<Integer> s = new Stack<>();
        int popped_i = 0;
        for (int x : pushed) {
            s.push(x);
            while (!s.isEmpty() && s.peek() == popped[popped_i]) {
                s.pop();
                popped_i++;
            }
        }
        return popped_i == popped.length;
    }

}
