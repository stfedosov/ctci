package Leetcode2;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AreSentencesSimilar {

    public static void main(String[] args) {
        System.out.println(areSentencesSimilar(
                new String[]{"I", "love", "leetcode"},
                new String[]{"I", "love", "onepiece"},
                List.of(
                        List.of("manga", "onepiece"), List.of("platform", "anime"),
                        List.of("leetcode", "platform"), List.of("anime", "manga")
                )
        )); // true

        System.out.println(areSentencesSimilar(
                new String[]{"I", "love", "leetcode"},
                new String[]{"I", "love", "onepiece"},
                List.of(
                        List.of("manga", "hunterXhunter"), List.of("platform", "anime"),
                        List.of("leetcode", "platform"), List.of("anime", "manga")
                )
        )); // false
    }

    public static boolean areSentencesSimilar(String[] sentence1, String[] sentence2, List<List<String>> similarPairs) {
        if (sentence1.length != sentence2.length) {
            return false;
        }
        Map<String, Set<String>> map = new HashMap<>();
        for (List<String> pair : similarPairs) {
            map.computeIfAbsent(pair.get(0), x -> new HashSet<>()).add(pair.get(1));
            map.computeIfAbsent(pair.get(1), x -> new HashSet<>()).add(pair.get(0));
        }
        for (int i = 0; i < sentence1.length; i++) {
            if (!dfs(sentence1[i], sentence2[i], map, new HashSet<>())) {
                return false;
            }
        }
        return true;
    }

    private static boolean dfs(String a, String b, Map<String, Set<String>> map, Set<String> set) {
        if (a.equals(b) || map.containsKey(a) && map.get(a).contains(b)) {
            return true;
        }
        set.add(a);
        for (String value : map.getOrDefault(a, Collections.emptySet())) {
            if (!set.contains(value) && dfs(value, b, map, set)) {
                return true;
            }
        }
        return false;
    }

}
