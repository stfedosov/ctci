package Leetcode2;

import Leetcode.TreeNode;

public class SubtreeWithAllDeepest {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(3);
        TreeNode left5 = new TreeNode(5);
        TreeNode right1 = new TreeNode(1);
        root.right = right1;
        root.left = left5;
        right1.left = new TreeNode(0);
        right1.right = new TreeNode(8);
        left5.left = new TreeNode(6);
        TreeNode right2 = new TreeNode(2);
        left5.right = right2;
        right2.left = new TreeNode(7);
        right2.right = new TreeNode(4);

        //          3
        //        /   \
        //       5     1
        //      / \   / \
        //     6  2   0  8
        //       / \
        //      7  4

        System.out.println(subtreeWithAllDeepest(root));

        // should be
        //     2
        //    / \
        //   7   4
    }

    static int deepestLevel = 0;
    static TreeNode res = null;

    public static TreeNode subtreeWithAllDeepest(TreeNode root) {
        dfs(root, 0);
        return res;
    }

    private static int dfs(TreeNode root, int level) {
        if (root == null) return level;

        int leftLevel = dfs(root.left, level + 1), rightLevel = dfs(root.right, level + 1);

        int curLevel = Math.max(leftLevel, rightLevel);
        deepestLevel = Math.max(deepestLevel, curLevel);
        if (leftLevel == deepestLevel && rightLevel == deepestLevel)
            res = root;
        return curLevel;
    }

    /*
    -----------------------
    No global variable
    -----------------------

    public TreeNode subtreeWithAllDeepest(TreeNode root) {
        return deep(root).getValue();
    }

    public Pair<Integer, TreeNode> deep(TreeNode root) {
        if (root == null) return new Pair(0, null);
        Pair<Integer, TreeNode> l = deep(root.left), r = deep(root.right);

        int d1 = l.getKey(), d2 = r.getKey();
        return new Pair(Math.max(d1, d2) + 1, d1 == d2 ? root : d1 > d2 ? l.getValue() : r.getValue());
    }
     */

}
