package Leetcode2;

public class CuttingRibbons {

    // Your goal is to obtain k ribbons of all the same positive integer length.
    // You are allowed to throw away any excess ribbon as a result of cutting.
    public static void main(String[] args) {
        assert maxLength(new int[]{9, 7, 5}, 3) == 5;
        assert maxLength(new int[]{7, 5, 9}, 4) == 4;
        assert maxLength(new int[]{5, 7, 9}, 22) == 0;
    }

    public static int maxLength(int[] ribbons, int k) {
        int low = 1, high = Integer.MIN_VALUE, max = Integer.MIN_VALUE;
        for (int ribbon : ribbons) {
            high = Math.max(high, ribbon);
        }
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (isCutPossible(ribbons, k, mid)) {
                max = Math.max(max, mid);
                // since we're aiming for maximum ribbon length we need to check
                // if there's a bigger answer that satisfies our requirement, move to the right
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        return max == Integer.MIN_VALUE ? 0 : max;
    }

    private static boolean isCutPossible(int[] ribbons, int k, int length) {
        int count = 0;
        for (int ribbon : ribbons) {
            count += ribbon / length;
        }
        return count >= k;
    }

}
