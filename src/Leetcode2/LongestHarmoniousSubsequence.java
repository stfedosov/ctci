package Leetcode2;

import java.util.HashMap;
import java.util.Map;

public class LongestHarmoniousSubsequence {

    public static void main(String[] args) {
        assert findLHS(new int[]{1, 3, 2, 2, 5, 2, 3, 7}) == 5;
        assert findLHS(new int[]{1, 2, 3, 4}) == 2;
        assert findLHS(new int[]{1, 1, 1, 1}) == 0;
    }

    public static int findLHS(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) map.put(num, map.getOrDefault(num, 0) + 1);
        int max = 0;
        for (int num : nums) {
            int howManyAbove = map.getOrDefault(num + 1, 0);
            if (howManyAbove != 0) {
                howManyAbove += map.get(num);
            }
            max = Math.max(max, howManyAbove);
        }
        return max;
    }

}
