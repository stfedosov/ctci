package Leetcode2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RankTeam {

    public static void main(String[] args) {
        assert rankTeams(new String[]{"ABC", "ACB", "ABC", "ACB", "ACB"}).equals("ACB");
        assert rankTeams(new String[]{"WXYZ", "XYZW"}).equals("XWYZ");
    }

    public static String rankTeams(String[] votes) {
        if (votes.length == 1) return votes[0];
        Map<Character, int[]> rank = new HashMap<>();
        for (String vote : votes) {
            for (int i = 0; i < vote.length(); i++) {
                rank.computeIfAbsent(vote.charAt(i), x -> new int[vote.length()])[i]++;
            }
        }
        List<Map.Entry<Character, int[]>> l = new ArrayList<>(rank.entrySet());
        l.sort((a, b) -> {
                    int[] la = a.getValue();
                    int[] lb = b.getValue();
                    for (int i = 0; i < la.length; i++) {
                        if (la[i] != lb[i]) return lb[i] - la[i];
                    }
                    return Character.compare(a.getKey(), b.getKey());
                });
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<Character, int[]> x : l) {
            sb.append(x.getKey());
        }
        return sb.toString();
    }

}
