package Leetcode2;

import java.util.Random;

public class RandomPickWithWeight {

    public static void main(String[] args) {
        int[] w = new int[]{2, 5, 3, 4};
        RandomPickWithWeight randomPickWithWeight = new RandomPickWithWeight(w);
        System.out.println(randomPickWithWeight.pickIndex());
        System.out.println(randomPickWithWeight.pickIndex());
        System.out.println(randomPickWithWeight.pickIndex());
        System.out.println(randomPickWithWeight.pickIndex());
    }

    /*
    Given a 0-indexed array of positive integers w where w[i] describes the weight of the ith index.

    Implement the function pickIndex(), which randomly picks an index in the range [0, w.length - 1] (inclusive) and returns it.
    The probability of picking an index i is w[i] / sum(w).
     */
    private int[] accumulator;
    Random random;

    // we are using the width of each range as the weight of the indexes (accumulated sum increases more with a higher weight)
    // for index 0 the range is [1, 2],
    // for index 1 the range is [3, 7]
    // for index 2 the range is [8, 10]
    // for index 3 the range is [11, 14]
    // in case of index 1 the range is longer than other ranges (since it has higher weight, 5),
    // and therefore the random pick falls into that range with higher probability
    public RandomPickWithWeight(int[] w) {
        accumulator = new int[w.length];
        this.random = new Random();
        accumulator[0] = w[0];
        for (int i = 1; i < w.length; i++) {
            accumulator[i] = w[i] + accumulator[i - 1];
        }
    }

    public int pickIndex() {
        // add 1 ensures that the range of random numbers generated corresponds to the 1-based indexing
        int target = random.nextInt(accumulator[accumulator.length - 1]) + 1;
        // run a binary search to find the target zone as accumulated array is sorted naturally
        int low = 0, high = accumulator.length;
        while (low < high) {
            int mid = low + (high - low) / 2;
            if (target > accumulator[mid])
                low = mid + 1;
            else
                high = mid;
        }
        return low;
    }

}
