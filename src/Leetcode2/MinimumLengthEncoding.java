package Leetcode2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MinimumLengthEncoding {

    public static void main(String[] args) {
        assert minimumLengthEncodingBruteForce(new String[]{"time", "me", "bell"}) == 10; // time#bell#
        assert minimumLengthEncodingBruteForce(new String[]{"t"}) == 2; // t#
        assert minimumLengthEncodingBruteForce(new String[]{"t", "tt"}) == 3; // tt#

        assert minimumLengthEncodingOptimal(new String[]{"time", "me", "bell"}) == 10; // time#bell#
        assert minimumLengthEncodingOptimal(new String[]{"t"}) == 2; // t#
        assert minimumLengthEncodingOptimal(new String[]{"t", "tt"}) == 3; // tt#
    }

    // ------------------- ! OPTIMAL ! -------------------------------------

    private static class Trie {
        char c;
        int depth;
        Map<Character, Trie> child = new HashMap<>();
    }

    public static int minimumLengthEncodingOptimal(String[] words) {
        Trie root = new Trie();
        for (String word : words) {
            Trie next = root;
            for (int i = word.length() - 1; i >= 0; i--) {
                char n = word.charAt(i);
                if (!next.child.containsKey(n)) next.child.put(n, new Trie());
                next = next.child.get(n);
            }
            next.depth = word.length() + 1;
        }
        return dfs(root);
    }

    private static int dfs(Trie root) {
        if (root.child.isEmpty()) return root.depth;
        int length = 0;
        for (Trie trie : root.child.values()) {
            length += dfs(trie);
        }
        return length;
    }

    // ------------------- ! BRUTE-FORCE ! -------------------------------------

    public static int minimumLengthEncodingBruteForce(String[] words) {
        Set<String> set = new HashSet<>(Arrays.asList(words));
        for (String word : words) {
            for (int x = 1; x < word.length(); x++) {
                set.remove(word.substring(x));
            }
        }
        int length = 0;
        for (String s : set) {
            length += s.length() + 1;
        }
        return length;
    }


}
