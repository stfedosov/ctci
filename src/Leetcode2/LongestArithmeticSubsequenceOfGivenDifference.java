package Leetcode2;

import java.util.HashMap;
import java.util.Map;

public class LongestArithmeticSubsequenceOfGivenDifference {

    // Given an integer array arr and an integer difference, return the length of the longest subsequence in arr
    // which is an arithmetic sequence such that the difference between adjacent
    // elements in the subsequence equals difference.
    public static void main(String[] args) {
        assert longestSubsequence(new int[]{1, 2, 3, 4}, 1) == 4;
        assert longestSubsequence(new int[]{1, 3, 5, 7}, 1) == 1;
        assert longestSubsequence(new int[]{1, 5, 7, 8, 5, 3, 4, 2, 1}, -2) == 4;
        // {1=1}
        // {1=1, 5=1}
        // {1=1, 5=1, 7=1}
        // {1=1, 5=1, 7=1, 8=1}
        // {1=1, 5=2, 7=1, 8=1}
        // {1=1, 3=3, 5=2, 7=1, 8=1}
        // {1=1, 3=3, 4=1, 5=2, 7=1, 8=1}
        // {1=1, 2=2, 3=3, 4=1, 5=2, 7=1, 8=1}
        // {[1=4], 2=2, 3=3, 4=1, 5=2, 7=1, 8=1}
    }

    public static int longestSubsequence(int[] arr, int difference) {
        Map<Integer, Integer> map = new HashMap<>();
        int max = 0;
        for (int num : arr) {
            map.put(num, map.getOrDefault(num - difference, 0) + 1);
            max = Math.max(max, map.getOrDefault(num, 0));
        }
        return max;
    }

}
