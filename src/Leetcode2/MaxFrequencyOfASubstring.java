package Leetcode2;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MaxFrequencyOfASubstring {

    // Maximum Number of Occurrences of a Substring
    public static void main(String[] args) {
        assert maxFreq("aababcaab", 2, 3, 4) == 2;
        assert maxFreq("aaaa", 1, 3, 3) == 2;
        assert maxFreq("aabcabcab", 2, 2, 3) == 3;
    }

    public static int maxFreq(String s, int maxLetters, int minSize, int maxSize) {
        Map<String, Integer> candidates = new HashMap<>();
        int max = 0;
        for (int i = 0; i < s.length(); i++) {
            Set<Character> set = new HashSet<>();
            int j = i;
            while (j < s.length() && j <= i + maxSize) {
                set.add(s.charAt(j));
                if (set.size() > maxLetters) break;
                if (j - i + 1 >= minSize) {
                    String ss = s.substring(i, j + 1);
                    candidates.put(ss, candidates.getOrDefault(ss, 0) + 1);
                    max = Math.max(max, candidates.get(ss));
                }
                j++;
            }
        }
        return max;
    }

}
