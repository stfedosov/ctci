package Leetcode2;

public class MissingNumber {

    public static void main(String[] args) {
        assert missingNumber(new int[]{5, 3, 2, 1}) == 4;
        assert missingNumber(new int[]{100, 95, 90, 80, 75, 70}) == 85;
        assert missingNumber(new int[]{100, 300, 400}) == 200;
    }

    public static int missingNumber(int[] arr) {
        int prevDiff = -1;
        int number = 0;
        boolean isIncreasing = arr[0] < arr[arr.length - 1];
        for (int i = 0; i + 1 < arr.length; i++) {
            int currDiff = Math.abs(arr[i + 1] - arr[i]);
            if (prevDiff == -1) {
                prevDiff = currDiff;
            } else {
                if (currDiff < prevDiff) {
                    return arr[i] + (isIncreasing ? -1 : 1) * currDiff;
                } else if (currDiff > prevDiff) {
                    return arr[i] + (isIncreasing ? 1 : -1) * prevDiff;
                }
            }
        }
        return number;
    }

}
