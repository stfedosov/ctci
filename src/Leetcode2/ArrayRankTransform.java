package Leetcode2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ArrayRankTransform {

    public static void main(String[] args) {
        assert Arrays.equals(arrayRankTransform(new int[]{40, 10, 20, 30}), new int[]{4, 1, 2, 3});
        assert Arrays.equals(arrayRankTransform(new int[]{100, 100, 100}), new int[]{1, 1, 1});
    }

    public static int[] arrayRankTransform(int[] arr) {
        int[] arr_copy = arr.clone();
        Arrays.sort(arr);
        Map<Integer, Integer> rank = new HashMap<>();
        for (int x : arr) {
            if (!rank.containsKey(x)) rank.put(x, rank.size() + 1);
        }
        int[] res = new int[arr.length];
        int i = 0;
        for (int x : arr_copy) {
            res[i++] = rank.get(x);
        }
        return res;
    }

}
