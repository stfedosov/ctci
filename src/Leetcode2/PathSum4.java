package Leetcode2;

import java.util.HashMap;
import java.util.Map;

public class PathSum4 {

    // If the depth of a tree is smaller than 5, then this tree can be represented by an array of three-digit integers.
    public static void main(String[] args) {
        // The tree that the list represents is shown.
        // The path sum is (3 + 5) + (3 + 1) = 12.
        assert pathSum(new int[]{113, 215, 221}) == 12;
        sum = 0;
        assert pathSum(new int[]{113, 221}) == 4;
    }

    private static int sum = 0;

    public static int pathSum(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            map.put(num / 10, num % 10);
        }
        traverse(nums[0] / 10, map, 0);
        return sum;
    }

    private static void traverse(int root, Map<Integer, Integer> map, int preSum) {
        if (!map.containsKey(root)) return;
        preSum += map.get(root);
        int level = root / 10;
        int position = root % 10;
        int left = (level + 1) * 10 + 2 * position - 1;
        int right = (level + 1) * 10 + 2 * position;
        if (!map.containsKey(left) && !map.containsKey(right)) {
            sum += preSum;
        } else {
            traverse(left, map, preSum);
            traverse(right, map, preSum);
        }
    }

}
