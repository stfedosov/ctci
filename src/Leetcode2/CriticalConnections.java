package Leetcode2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CriticalConnections {

    public static void main(String[] args) {
        //        (1) ------ (2)
        //         |  \     /
        //         |   \   /
        //         |    \ /
        //        (3)   (0)
        assert criticalConnections(4,
                Arrays.asList(
                        Arrays.asList(0, 1),
                        Arrays.asList(1, 2),
                        Arrays.asList(2, 0),
                        Arrays.asList(1, 3)))
                .equals(
                        Collections.singletonList(Arrays.asList(1, 3)));
    }


    // We need to record time for each node.
    // For each node, we check every neighbor except its parent and return a smallest timestamp in all its neighbors.
    // If this timestamp is strictly less than the node's timestamp, we know that this node is somehow in a cycle.
    // If this timestamp is bigger, this edge from the parent to this node is a critical connection
    // (not in this cycle or in the other cycle).

    private static int TIME = 1;

    public static List<List<Integer>> criticalConnections(int n, List<List<Integer>> connections) {
        List[] graph = new List[n];
        for (int i = 0; i < n; i++) graph[i] = new ArrayList<>();
        for (List<Integer> l : connections) {
            graph[l.get(0)].add(l.get(1));
            graph[l.get(1)].add(l.get(0));
        }
        int[] timestamp = new int[n];
        List<List<Integer>> criticalConn = new ArrayList<>();
        dfs(-1, 0, graph, criticalConn, timestamp);
        return criticalConn;
    }

    private static int dfs(int parent, int node, List[] graph, List<List<Integer>> criticalConn, int[] timestamp) {
        if (timestamp[node] != 0) return timestamp[node];
        timestamp[node] = TIME;
        TIME++;
        int min = Integer.MAX_VALUE;
        for (int child : (List<Integer>) graph[node]) {
            if (child == parent) continue;
            int childTimestamp = dfs(node, child, graph, criticalConn, timestamp);
            min = Math.min(min, childTimestamp);
        }
        if (min >= timestamp[node]) {
            if (parent >= 0) criticalConn.add(Arrays.asList(parent, node));
        }
        return Math.min(min, timestamp[node]);
    }

}
