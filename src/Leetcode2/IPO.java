package Leetcode2;

import java.util.PriorityQueue;
import java.util.Queue;

public class IPO {

    /*
    You are given n projects where the ith project has a pure profit profits[i]
    and a minimum capital of capital[i] is needed to start it.

    Initially, you have w capital.
    When you finish a project, you will obtain its pure profit
    and the profit will be added to your total capital.

    Pick a list of at most k distinct projects from given projects
    to maximize your final capital, and return the final maximized capital.
     */
    public static void main(String[] args) {
        findMaximizedCapital(2, 0, new int[]{1, 2, 3}, new int[]{0, 1, 1}); // 4
        findMaximizedCapital(3, 0, new int[]{1, 2, 3}, new int[]{0, 1, 2}); // 6
    }

    public static int findMaximizedCapital(int k, int w, int[] profits, int[] capital) {
        Queue<int[]> capitalQueue = new PriorityQueue<>();
        Queue<int[]> profitQueue = new PriorityQueue<>((a, b) -> b[1] - a[1]);
        for (int i = 0; i < capital.length; i++) {
            capitalQueue.offer(new int[]{capital[i], profits[i]});
        }
        for (int i = 0; i < k; i++) {
            while (!capitalQueue.isEmpty() && capitalQueue.peek()[0] <= w) {
                profitQueue.offer(capitalQueue.poll());
            }
            if (profitQueue.isEmpty()) break;
            w += profitQueue.poll()[1];
        }
        return w;
    }

}
