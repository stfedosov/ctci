package Leetcode2;

public class RepeatedStringMatch {

    // Given two strings a and b, return the minimum number of times you should
    // repeat string a so that string b is a substring of it.
    // If it is impossible for b to be a substring of a after repeating it, return -1.
    public static void main(String[] args) {
        System.out.println(repeatedStringMatch("abcd", "cdabcdab"));
        System.out.println(repeatedStringMatch("a", "aa"));
    }

    public static int repeatedStringMatch(String A, String B) {
        int count = 1; // Minimum number of times A has to be repeated.
        StringBuilder res = new StringBuilder(A);

        while (res.length() < B.length()) {
            res.append(A);
            count++;
        }
        if (res.indexOf(B) != -1) return count;

        res.append(A);
        return (res.indexOf(B) == -1) ? -1 : count + 1;
    }

}
