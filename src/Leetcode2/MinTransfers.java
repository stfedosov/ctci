package Leetcode2;

import java.util.HashMap;
import java.util.Map;

public class MinTransfers {

    /*
    You are given an array of transactions transactions where transactions[i] = [fromi, toi, amounti]
    indicates that the person with ID = fromi gave amounti $ to the person with ID = toi.

    Return the minimum number of transactions required to settle the debt.
     */
    public static void main(String[] args) {
        System.out.println(minTransfers(new int[][]{{0, 1, 10}, {2, 0, 5}})); // 2
        System.out.println(minTransfers(new int[][]{{0, 1, 10}, {1, 0, 1}, {1, 2, 5}, {2, 0, 5}})); // 1
    }

    public static int minTransfers(int[][] transactions) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int[] tr : transactions) {
            map.put(tr[0], map.getOrDefault(tr[0], 0) - tr[2]);
            map.put(tr[1], map.getOrDefault(tr[1], 0) + tr[2]);
        }
        int[] finalBalance = new int[map.size()];
        int i = 0;
        for (int key : map.keySet()) finalBalance[i++] = map.get(key);
        return dfs(0, finalBalance);
    }

    private static int dfs(int idx, int[] finalBalance) {
        if (idx == finalBalance.length) return 0;
        if (finalBalance[idx] == 0) return dfs(idx + 1, finalBalance);
        int min = Integer.MAX_VALUE;
        for (int i = idx + 1; i < finalBalance.length; i++) {
            // both negative or positive -> skip
            if (finalBalance[i] > 0 && finalBalance[idx] > 0 || finalBalance[i] < 0 && finalBalance[idx] < 0) continue;
            finalBalance[i] += finalBalance[idx];
            min = Math.min(min, 1 + dfs(idx + 1, finalBalance));
            finalBalance[i] -= finalBalance[idx];
        }
        return min;
    }

}
