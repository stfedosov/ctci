package Leetcode2;

import Leetcode.TreeNode;

public class AddOneRow {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(4);
        TreeNode left2 = new TreeNode(2);
        TreeNode right6 = new TreeNode(6);
        TreeNode leftleft3 = new TreeNode(3);
        TreeNode leftright1 = new TreeNode(1);
        TreeNode rightleft5 = new TreeNode(5);
        root.left = left2;
        root.right = right6;
        left2.left = leftleft3;
        left2.right = leftright1;
        rightleft5.left = rightleft5;
//        System.out.println(addOneRow(root, 1, 2));

        root = new TreeNode(4);
        root.left = new TreeNode(2);
        root.left.left = new TreeNode(3);
        root.left.right = new TreeNode(1);
        System.out.println(addOneRow(root, 1, 3));
    }

    public static TreeNode addOneRow(TreeNode root, int v, int d) {
        if (d == 1) {
            TreeNode newNode = new TreeNode(v);
            newNode.left = root;
            return newNode;
        }
        addOneRow(root, null, v, d, 1, true);
        return root;
    }

    private static void addOneRow(TreeNode root, TreeNode prev, int v, int d, int currentd, boolean isLeft) {
        if (currentd == d) {
            if (isLeft) {
                TreeNode newLeft = new TreeNode(v);
                newLeft.left = root;
                prev.left = newLeft;
            } else {
                TreeNode newRight = new TreeNode(v);
                newRight.right = root;
                prev.right = newRight;
            }
        } else if (root != null) {
            addOneRow(root.left, root, v, d, currentd + 1, true);
            addOneRow(root.right, root, v, d, currentd + 1, false);
        }
    }

}
