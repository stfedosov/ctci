package Leetcode2;

import Leetcode.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VerticalTraversal {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode node2 = new TreeNode(2);
        TreeNode node3 = new TreeNode(3);
        root.left = node2;
        root.right = node3;
        node2.left = new TreeNode(4);
        node2.right = new TreeNode(5);
        node3.left = new TreeNode(6);
        node3.right = new TreeNode(7);
        assert verticalTraversal(root).equals(
                Arrays.asList(
                        Arrays.asList(4),
                        Arrays.asList(2),
                        Arrays.asList(1, 5, 6),
                        Arrays.asList(3),
                        Arrays.asList(7)));
    }

    private static final Map<Integer, Map<Integer, List<Integer>>> columnRowMap = new HashMap<>();
    static int minRow = 0, maxRow = 0, minCol = 0, maxCol = 0;

    // Time complexity: O(k columns * N/klog(N/k) time to spend in sorting rows = N * log(N/k)) -  where k is the number of columns
    public static List<List<Integer>> verticalTraversal(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null) return result;

        dfs(root, 0, 0);

        for (int col = minCol; col <= maxCol; col++) {
            var rowLists = columnRowMap.get(col);
            List<Integer> sortedColumn = new ArrayList<>();
            for (int row = minRow; row <= maxRow; row++) {
                if (!rowLists.containsKey(row)) {
                    continue;
                }
                var valueList = rowLists.get(row);
                valueList.sort(Comparator.naturalOrder());
                sortedColumn.addAll(valueList);
            }
            result.add(sortedColumn);
        }
        return result;
    }

    private static void dfs(TreeNode root, int row, int col) {
        if (root == null) return;

        dfs(root.left, row + 1, col - 1);

        minRow = Math.min(minRow, row);
        maxRow = Math.max(maxRow, row);
        minCol = Math.min(minCol, col);
        maxCol = Math.max(maxCol, col);

        columnRowMap.computeIfAbsent(col, x -> new HashMap<>())
                .computeIfAbsent(row, y -> new ArrayList<>())
                .add(root.val);

        dfs(root.right, row + 1, col + 1);
    }
}
