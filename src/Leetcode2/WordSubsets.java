package Leetcode2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class WordSubsets {

    public static void main(String[] args) {
        assert wordSubsets(new String[]{"amazon", "apple", "facebook", "google", "leetcode"}, new String[]{"e", "o"}).equals(Arrays.asList("facebook", "google", "leetcode"));
        assert wordSubsets(new String[]{"amazon", "apple", "facebook", "google", "leetcode"}, new String[]{"eo", "lo"}).equals(Arrays.asList("google", "leetcode"));
    }

    // O(N + M)
    public static List<String> wordSubsets(String[] words1, String[] words2) {
        Map<Character, Long> maxes = new HashMap<>();
        for (String w : words2) {
            var map = compute(w);
            for (Character key : map.keySet()) {
                maxes.put(key, Math.max(maxes.getOrDefault(key, 0L), map.get(key)));
            }
        }
        List<String> res = new ArrayList<>();
        MAIN_LOOP:
        for (String w : words1) {
            Map<Character, Long> map = compute(w);
            for (char c = 'a'; c <= 'z'; c++) {
                if (maxes.getOrDefault(c, 0L) > map.getOrDefault(c, 0L)) {
                    continue MAIN_LOOP;
                }
            }
            res.add(w);
        }
        return res;
    }

    private static Map<Character, Long> compute(String w) {
        return w.chars()
                .mapToObj(c -> (char) c)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    }

}
