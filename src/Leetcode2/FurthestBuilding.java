package Leetcode2;

import java.util.PriorityQueue;
import java.util.Queue;

public class FurthestBuilding {

    public static void main(String[] args) {
        assert furthestBuilding(new int[]{1, 5, 1, 2, 3, 4, 10000}, 4, 1) == 5;
        assert furthestBuilding(new int[]{4, 2, 7, 6, 9, 14, 12}, 5, 1) == 4;
    }

    public static int furthestBuilding(int[] heights, int bricks, int ladders) {
        Queue<Integer> maxHeap = new PriorityQueue<>((a, b) -> b - a);
        for (int i = 0; i < heights.length - 1; i++) {
            if (heights[i + 1] > heights[i]) {
                int diff = heights[i + 1] - heights[i];
                maxHeap.offer(diff);
                bricks -= diff;
                // If we've used all the bricks, and have no ladders remaining, then
                // we can't go any further.
                if (bricks < 0 && ladders == 0) {
                    return i;
                }
                // Otherwise, if we've run out of bricks, we should replace the largest
                // brick allocation with a ladder.
                else if (bricks < 0) {
                    bricks += maxHeap.poll();
                    ladders--;
                }
            }
        }
        // If we got to here, this means we had enough materials to cover every climb.
        return heights.length - 1;
    }

}
