package Leetcode2;

import Leetcode.TreeNode;

public class DistributeCoins {

    // 1. Each node demands Math.abs(node.val - 1) number of moves inorder to turn itself into one
    // e.g.: if a node is 0 then => we should borrow one or Math.abs(0 - 1)
    //       if a node is 3 then => give out 2 or Math.abs(3 - 1)
    // 2. Apart from trying to balance out myself I must also check whether my left and right subtrees are also balanced.

    public static void main(String[] args) {
        TreeNode root = new TreeNode(3);
        root.left = new TreeNode(0);
        root.right = new TreeNode(0);
        assert distributeCoins(root) == 2;
    }

    static int moves = 0;

    public static int distributeCoins(TreeNode root) {
        dfs(root);
        return moves;
    }

    private static int dfs(TreeNode root) {
        if (root == null) return 0;
        int left = dfs(root.left);
        int right = dfs(root.right);
        int total_coins = left + right + root.val;
        moves += Math.abs(total_coins - 1);
        return total_coins - 1;
    }

}
