package Leetcode2;

import Leetcode.TreeNode;

public class MinDepth {

    public static void main(String[] args) {

    }

    static int min = Integer.MAX_VALUE;

    public static int minDepth(TreeNode root) {
        dfs(root, 0);
        return min == Integer.MAX_VALUE ? 0 : min + 1;
    }

    private static void dfs(TreeNode root, int depth) {
        if (root == null) return;
        else if (root.left == null && root.right == null) {
            min = Math.min(min, depth);
        } else {
            dfs(root.left, depth + 1);
            dfs(root.right, depth + 1);
        }
    }

}
