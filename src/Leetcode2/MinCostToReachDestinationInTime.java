package Leetcode2;

import java.util.ArrayList;
import java.util.List;

public class MinCostToReachDestinationInTime {

    public static void main(String[] args) {
        System.out.println(minCost(29,
                new int[][]{{0, 1, 10}, {1, 2, 10}, {2, 5, 10}, {0, 3, 1}, {3, 4, 10}, {4, 5, 15}},
                new int[]{5, 1, 2, 20, 20, 3}));
    }

    static class Edge {
        int to, weight;

        public Edge(int to, int weight) {
            this.to = to;
            this.weight = weight;
        }
    }

    public static int minCost(int maxTime, int[][] edges, int[] passingFees) {
        List<Edge>[] graph = new ArrayList[passingFees.length];
        for (int[] edge : edges) {
            if (graph[edge[0]] == null) graph[edge[0]] = new ArrayList<>();
            graph[edge[0]].add(new Edge(edge[1], edge[2]));
            if (graph[edge[1]] == null) graph[edge[1]] = new ArrayList<>();
            graph[edge[1]].add(new Edge(edge[0], edge[2]));
        }
        int res = findMin(graph, 0, maxTime, passingFees, new Integer[1001][maxTime + 1]);
        return res == Integer.MAX_VALUE ? -1 : res;
    }

    private static int findMin(List<Edge>[] graph, int src, int remainingTime, int[] passingFees, Integer[][] dp) {
        if (remainingTime < 0) return Integer.MAX_VALUE;
        if (dp[src][remainingTime] != null) return dp[src][remainingTime];
        if (src == passingFees.length - 1) return (dp[src][remainingTime] = passingFees[src]);
        int min = Integer.MAX_VALUE;
        for (Edge e : graph[src]) {
            min = Math.min(min, findMin(graph, e.to, remainingTime - e.weight, passingFees, dp));
        }
        return (dp[src][remainingTime] = (min == Integer.MAX_VALUE ? min : min + passingFees[src]));
    }

}
