package Leetcode2;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SubarraysWithKDistinct {

    public static void main(String[] args) {
        assert subarraysWithKDistinctBruteForce(new int[]{1, 2, 1, 2, 3}, 2) == 7;
        assert subarraysWithKDistinctBruteForce(new int[]{1, 2, 1, 3, 4}, 3) == 3;
        assert subarraysWithKDistinctBruteForce(new int[]{1, 2}, 1) == 2;

        assert subarraysWithKDistinct(new int[]{1, 2, 1, 2, 3}, 2) == 7;
        assert subarraysWithKDistinct(new int[]{1, 2, 1, 3, 4}, 3) == 3;
        assert subarraysWithKDistinct(new int[]{1, 2}, 1) == 2;
    }

    public static int subarraysWithKDistinct(int[] A, int K) {
        // Number of subarrays with "exactly" K distinct numbers =
        // number of subarrays with "atmost" K distinct numbers - number of subarrays with "atmost" K-1 distinct numbers
        return atMost(A, K) - atMost(A, K - 1);
    }

    private static int atMost(int[] A, int K) {
        Map<Integer, Integer> map = new HashMap<>();
        int left = 0;
        int answer = 0;
        for (int right = 0; right < A.length; right++) {
            map.put(A[right], map.getOrDefault(A[right], 0) + 1);
            while (map.size() > K) {
                Integer leftCount = map.get(A[left]);
                if (leftCount == 1) map.remove(A[left]);
                else map.put(A[left], leftCount - 1);
                left++;
            }
            answer += right - left;
        }
        return answer;
    }

    // ----------------------------- Brute force...
    public static int subarraysWithKDistinctBruteForce(int[] A, int K) {
        int count = 0;
        for (int i = 0; i < A.length; i++) {
            Set<Integer> set = new HashSet<>();
            set.add(A[i]);
            for (int j = i; j < A.length; j++) {
                set.add(A[j]);
                if (set.size() == K) {
                    count++;
                } else if (set.size() > K) {
                    break;
                }
            }
        }
        return count;
    }

}
