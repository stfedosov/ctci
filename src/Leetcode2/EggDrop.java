package Leetcode2;

public class EggDrop {

    public static void main(String[] args) {
        assert twoEggDropGeneral(10) == 4;
        assert twoEggDropGeneral(100) == 14;
        assert twoEggDropGeneral(2) == 2;

        assert superEggDrop(2, 10) == 4;
        assert superEggDrop(2, 100) == 14;
        assert superEggDrop(2, 2) == 2;
    }

    public static int twoEggDrop(int n) {
        if (n <= 1) return n;
        int min = Integer.MAX_VALUE;
        for (int i = 1; i <= n; i++) {
            min = Math.min(min, 1 + Math.max(i - 1, twoEggDrop(n - i)));
        }
        return min;
    }

    // General case
    // Let's consider a more general problem when we have x eggs and n floors.
    // If you drop an egg from i floor (1 <= i <= n), then
    //
    // If the egg breaks, the problem is reduced to x - 1 eggs and i - 1 floors
    // If the eggs does not break, the problem is reduced to x eggs and n - i floors
    public static int twoEggDropGeneral(int N) {
        int K = 2;
        int[][] memo = new int[K + 1][N + 1];
        return drop(K, N, memo);
    }

    private static int drop(int k, int n, int[][] memo) {
        if (n <= 1 || k == 1) {
            return n;
        }
        if (memo[k][n] > 0) {
            return memo[k][n];
        }
        int min = Integer.MAX_VALUE;
        for (int floor = 1; floor <= n; floor++) {
            int left = drop(k - 1, floor - 1, memo); // egg breaks
            int right = drop(k, n - floor, memo); // egg does not break
            min = Math.min(min, 1 + Math.max(left, right));
        }
        memo[k][n] = min;
        return min;
    }

    // Improvement for the general case - binary search
    // O(KNlogN)
    // when floor goes up, left goes up and right goes down.
    // We can use Binary Search here to get the minimum Math.max(left, right) + 1, when left and right are as close as possible
    public static int superEggDrop(int K, int N) {
        int[][] memo = new int[K + 1][N + 1];
        return helper(K, N, memo);
    }

    private static int helper(int K, int N, int[][] memo) {
        if (N <= 1 || K == 1) {
            return N;
        }
        if (memo[K][N] > 0) {
            return memo[K][N];
        }

        int low = 1, high = N, result = Integer.MAX_VALUE;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            int left = helper(K - 1, mid - 1, memo);
            int right = helper(K, N - mid, memo);
            result = Math.min(result, 1 + Math.max(left, right));
            if (left == right) {
                break;
            } else if (left < right) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        memo[K][N] = result;
        return result;
    }

}
