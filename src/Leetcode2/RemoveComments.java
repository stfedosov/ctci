package Leetcode2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RemoveComments {

    public static void main(String[] args) {
        assert removeComments(new String[]{"/*LeftRightBoundariesBinarySearch program */",
                "int main()", "{ ",
                "  // variable declaration ",
                "int a, b, c;", "/* This is a test",
                "   multiline  ",
                "   comment for ",
                "   testing */", "a = b + c;", "}"})
                .equals(Arrays.asList(
                        "int main()",
                        "{ ",
                        "  ",
                        "int a, b, c;",
                        "a = b + c;",
                        "}"));
    }

    public static List<String> removeComments(String[] source) {
        List<String> res = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        boolean opened = false;
        for (String s : source) {
            for (int i = 0; i < s.length(); i++) {
                if (!opened) {
                    if (s.charAt(i) == '/' && i < s.length() - 1 && s.charAt(i + 1) == '/') {
                        break;      //ignore remaining characters on line s
                    } else if (s.charAt(i) == '/' && i < s.length() - 1 && s.charAt(i + 1) == '*') {
                        opened = true;
                        i++;           //skip '*' on next iteration of i
                    } else sb.append(s.charAt(i));     //not a comment
                } else {
                    if (s.charAt(i) == '*' && i < s.length() - 1 && s.charAt(i + 1) == '/') {
                        opened = false;
                        i++;        //skip '/' on next iteration of i
                    }
                }
            }
            if (!opened && sb.length() > 0) {
                res.add(sb.toString());
                sb = new StringBuilder();   //reset for next line of source code
            }
        }
        return res;
    }

}
