package Leetcode2;

import java.util.Stack;

public class MaxNestingDepthOfParentheses {

    public static void main(String[] args) {
        assert maxDepth("(1+(2*3)+((8)/4))+1") == 3;
        assert maxDepth("(1)+((2))+(((3)))") == 3;
        assert maxDepth("1+(2*3)/(2-1)") == 1;
    }

    public static int maxDepth(String s) {
        Stack<Character> stack = new Stack<>();
        int maxDepth = 0;
        for (char c : s.toCharArray()) {
            if (c == '(') {
                stack.push(c);
                maxDepth = Math.max(maxDepth, stack.size());
            }
            if (c == ')') {
                stack.pop();
            }
        }
        return maxDepth;
    }

}
