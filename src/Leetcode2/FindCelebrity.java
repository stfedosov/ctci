package Leetcode2;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class FindCelebrity extends Relation {

    public FindCelebrity(int[][] ints) {
        super(ints);
    }

    public static void main(String[] args) {
        FindCelebrity findCelebrity = new FindCelebrity(new int[][]{{1, 1, 0}, {0, 1, 0}, {1, 1, 1}});
        assert findCelebrity.findCelebrity(3) == 1;

        findCelebrity = new FindCelebrity(new int[][]{{1, 0, 1}, {1, 1, 0}, {0, 1, 1}});
        assert findCelebrity.findCelebrity(3) == -1;
    }

    public int findCelebrity(int n) {
        int candidate = 0;
        for (int i = 1; i < n; i++) {
            if (knows(candidate, i)) candidate = i;
        }
        for (int i = 0; i < n; i++) {
            if (!knows(i, candidate) || i != candidate && knows(candidate, i)) {
                return -1;
            }
        }
        return candidate;
    }

}

abstract class Relation {

    private final Map<Integer, Set<Integer>> map;

    public Relation(int[][] ints) {
        map = new HashMap<>();
        int i = 0;
        for (int[] in : ints) {
            map.put(i, new HashSet<>());
            for (int j = 0; j < in.length; j++) {
                if (in[j] == 1) map.get(i).add(j);
            }
            i++;
        }
    }

    abstract int findCelebrity(int n);

    // determine whether A knows B
    boolean knows(int a, int b) {
        return map.get(a).contains(b);
    }
}
