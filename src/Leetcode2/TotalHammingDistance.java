package Leetcode2;

public class TotalHammingDistance {

    public static void main(String[] args) {
        assert totalHammingDistance(new int[]{4, 14, 2}) == 6;
        assert totalHammingDistance(new int[]{4, 14, 4}) == 4;
    }

    // For example, look at the rightmost bit of all the numbers in nums.
    // Suppose that i numbers have a rightmost 0-bit, and j numbers have a 1-bit.
    // Then out of the pairs, i * j of them will have 1 in the rightmost bit of the XOR.
    // This is because there are i * j ways to choose one number that has a 0-bit and one that has a 1-bit.
    // These bits will therefore contribute i * j towards the total of all the XORs.
    public static int totalHammingDistance(int[] nums) {
        int total = 0;
        int[] countOfOnes = new int[31];
        for (int num : nums) {
            for (int i = 0; i < 31; i++) {
                countOfOnes[i] += (num >> i) & 1;
            }
        }

        for (int x : countOfOnes) {
            total += x * (nums.length - x);
        }
        return total;
    }

}
