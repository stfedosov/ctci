package Leetcode2;

import java.util.Arrays;
import java.util.Comparator;

public class VideoStitching {

    /*
    You are given a series of video clips from a sporting event that lasted time seconds.
    These video clips can be overlapping with each other and have varying lengths.

    Each video clip is described by an array clips where clips[i] = [starti, endi] indicates that
    the ith clip started at starti and ended at endi.

    Return the minimum number of clips needed so that we can cut the clips into segments
    that cover the entire sporting event [0, time]. If the task is impossible, return -1.
     */

    public static void main(String[] args) {
        assert videoStitchingGreedy(new int[][]{{0, 2}, {4, 6}, {8, 10}, {1, 9}, {1, 5}, {5, 9}}, 10) == 3;
        assert videoStitchingGreedy(new int[][]{{0, 1}, {1, 2}}, 5) == -1;
    }

    public static int videoStitchingGreedy(int[][] clips, int time) {
        Arrays.sort(clips, Comparator.comparingInt(a -> a[0]));
        int count = 0, start = 0, maxEnd = 0, i = 0;
        while (true) {
            int temp = maxEnd; // for case when we don't have enough intervals to cover the whole length
            while (i < clips.length && clips[i][0] <= start) {
                maxEnd = Math.max(maxEnd, clips[i][1]);
                i++;
            }
            count++;
            if (maxEnd >= time) return count;
            if ((i < clips.length && clips[i][0] > maxEnd) || maxEnd == temp) return -1;
            start = maxEnd;
        }
    }

    private static final int MAX = Integer.MAX_VALUE - 1;

    public static int videoStitchingBruteForce(int[][] clips, int time) {
        Arrays.sort(clips, Comparator.comparingInt(a -> a[0]));
        int[][] cache = new int[100][100];
        for (int[] c : cache) Arrays.fill(c, -1);
        int res = helper(clips, time, 0, 0, cache);
        return res == MAX ? -1 : res;
    }

    private static int helper(int[][] clips, int time, int idx, int prevEnd, int[][] cache) {
        if (prevEnd >= time) return 0;
        if (idx >= clips.length) return MAX;
        if (cache[idx][prevEnd] != -1) return cache[idx][prevEnd];
        int min = MAX;
        for (int i = idx; i < clips.length; i++) {
            if (clips[i][0] <= prevEnd) {
                min = Math.min(min, 1 + helper(clips, time, idx + 1, clips[i][1], cache));
            }
        }
        cache[idx][prevEnd] = min;
        return min;
    }

}
