package Leetcode2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PancakeSorting {

    public static void main(String[] args) {
        int[] arr = new int[]{3, 2, 4, 1};
        pancakeSort(arr);
        assert Arrays.equals(arr, new int[]{1, 2, 3, 4});

        // [3, 2, 4, 1]
        //        ^  ^
        //        j  i
        // [4, 2, 3, 1] <- flipped from 0 to j
        // [1, 3, 2, 4] <- flipped from 0 to i
        // [1, 3, 2, 4]
        //     ^  ^
        //     j  i
        // [3, 1, 2, 4] <- flipped from 0 to j
        // [2, 1, 3, 4] <- flipped from 0 to i
        //  ^  ^
        //  j  i
        // [2, 1, 3, 4] <- flipped from 0 to j
        // [1, 2, 3, 4] <- flipped from 0 to i
        // Done!
    }

    public static List<Integer> pancakeSort(int[] arr) {
        List<Integer> res = new ArrayList<>();
        for (int i = arr.length - 1; i >= 0; i--) {
            int max = i;
            for (int j = i; j >= 0; j--) {
                if (arr[j] > arr[max]) {
                    max = j;
                }
            }
            if (max != i) {
                flip(arr, max);
                flip(arr, i);
                res.add(max + 1);
                res.add(i + 1);
            }
        }
        return res;
    }

    private static void flip(int[] arr, int end) {
        int start = 0;
        while (start < end) {
            int tmp = arr[start];
            arr[start] = arr[end];
            arr[end] = tmp;
            start++;
            end--;
        }
    }

}
