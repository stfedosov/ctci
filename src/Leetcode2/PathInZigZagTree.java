package Leetcode2;

import java.util.ArrayList;
import java.util.List;

public class PathInZigZagTree {

    public static void main(String[] args) {
        assert pathInZigZagTree(14).equals(List.of(1, 3, 4, 14));
        assert pathInZigZagTree(26).equals(List.of(1, 2, 6, 10, 26));
    }

    // in a normally indexed tree, we can find the parent of a node by node.index/2
    // but in this case, some levels node indexes are reversed
    // original: 8 9 10 11 12 13 14 15
    // current:  15 14 12 12 11 10 9 8
    // we want to find current 14's parent which is original 9's parent  9/2 = 4
    // how do we know 14's corresponding reverse node is 9?
    // we can see that every original index + current index are the same
    // so we can get min and max of every level  -> min + max - 14 is the corresponding reversed index
    // step1: find target label's level
    // step2: all the way up to root, every level, we get min, max, find reversed index -> parent add to result;
    public static List<Integer> pathInZigZagTree(int label) {
        int level = 0;
        int count = 0;
        while (count < label) {
            count += Math.pow(2, level);
            level++;
        }
        level--;
        List<Integer> res = new ArrayList<>();
        while (level >= 0) {
            res.add(0, label);
            int min = (int) Math.pow(2, level);
            int max = (int) Math.pow(2, level + 1) - 1;
            int reversedLabel = min + max - label;
            label = reversedLabel / 2;
            level--;
        }
        return res;
    }

}
