package Leetcode2;

import Leetcode.Interval;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeeFreeTime {

    public static void main(String[] args) {
        assert employeeFreeTime(Arrays.asList(
                Arrays.asList(new Interval(1, 2), new Interval(5, 6)),
                Arrays.asList(new Interval(1, 3)),
                Arrays.asList(new Interval(4, 10)))).equals(Arrays.asList(new Interval(3, 4)));

        assert employeeFreeTime(Arrays.asList(
                Arrays.asList(new Interval(1, 3), new Interval(6, 7)),
                Arrays.asList(new Interval(2, 4)),
                Arrays.asList(new Interval(2, 5), new Interval(9, 12)))).equals(Arrays.asList(new Interval(5, 6), new Interval(7, 9)));
    }

    public static List<Interval> employeeFreeTime(List<List<Interval>> schedule) {
        List<Interval> newList = schedule.stream()
                .flatMap(List::stream)
                .sorted(Comparator.comparingInt(a -> a.start))
                .collect(Collectors.toList());
        List<Interval> result = new ArrayList<>();
        // merge intervals logic
        Interval prev = null;
        for (Interval i : newList) {
            if (prev == null || i.start > prev.end) {
                // if intervals are not overlapping we can add the gap between them to the result
                if (prev != null) result.add(new Interval(prev.end, i.start));
                prev = i;
            } else if (i.end > prev.end) {
                prev.end = i.end;
            }
        }
        return result;
    }

}
