package Leetcode2;

import Leetcode.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class BSTIteratorExtension {

    static class BSTIterator1 {

        private final List<TreeNode> list;
        private int idx = -1;

        public BSTIterator1(TreeNode root) {
            this.list = new ArrayList<>();
            inorder(root, list);
        }

        public boolean hasNext() {
            return idx < list.size() - 1;
        }

        public int next() {
            return list.get(++idx).val;
        }

        public boolean hasPrev() {
            return idx > 0;
        }

        public int prev() {
            return list.get(--idx).val;
        }

        private void inorder(TreeNode root, List<TreeNode> list) {
            if (root == null) return;
            inorder(root.left, list);
            list.add(root);
            inorder(root.right, list);
        }
    }

    /*
    Time complexity:
    O(1) for the constructor.
    O(1) for hasPrev.
    O(1) for prev.
    O(1) for hasNext.
    O(N) for next. In the worst-case of the skewed tree one has to parse the entire tree, all N nodes.

    Space complexity: O(N)
    The space is taken by stack and list.
    Stack contains up to H elements, where H is the tree height, and list is up to N elements.
     */
    static class BSTIterator2 {

        private final Stack<TreeNode> stack;
        private final List<Integer> list;
        private TreeNode last;
        private int idx;

        public BSTIterator2(TreeNode root) {
            last = root;
            stack = new Stack<>();
            list = new ArrayList<>();
            idx = -1;
        }

        public boolean hasNext() {
            return !stack.isEmpty() || last != null || idx < list.size() - 1;
        }

        public int next() {
            idx++;
            // if the pointer is out of precomputed range
            if (idx == list.size()) {
                // process all predecessors of the last node:
                // go left till you can and then one step right
                while (last != null) {
                    stack.push(last);
                    last = last.left;
                }
                TreeNode curr = stack.pop();
                last = curr.right;
                list.add(curr.val);
            }

            return list.get(idx);
        }

        public boolean hasPrev() {
            return idx > 0;
        }

        public int prev() {
            idx--;
            return list.get(idx);
        }
    }

    public static void main(String[] args) {
        //          7
        //         /  \
        //        3   15
        //           /  \
        //          9    20
        TreeNode root = new TreeNode(7);
        root.left = new TreeNode(3);
        TreeNode right15 = new TreeNode(15);
        root.right = right15;
        right15.left = new TreeNode(9);
        right15.right = new TreeNode(20);
        BSTIteratorExtension.BSTIterator1 iterator = new BSTIteratorExtension.BSTIterator1(root);
        System.out.println(iterator.hasNext());
        System.out.println(iterator.next());
        System.out.println(iterator.next());
        System.out.println(iterator.prev());
        System.out.println(iterator.next());
    }


}
