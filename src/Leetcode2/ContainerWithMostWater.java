package Leetcode2;

public class ContainerWithMostWater {

    public static void main(String[] args) {
        assert maxArea(new int[]{1, 8, 6, 2, 5, 4, 8, 3, 7}) == 49;
        assert maxArea(new int[]{1, 1}) == 1;
        assert maxArea(new int[]{4, 3, 2, 1, 4}) == 16;
        assert maxArea(new int[]{1, 2, 1}) == 2;
    }

    public static int maxArea(int[] height) {
        int max = Integer.MIN_VALUE, i = 0, j = height.length - 1;
        while (i < j) {
            int min = Math.min(height[i], height[j]);
            max = Math.max(max, min * (j - i));
            if (height[i] >= height[j]) {
                j--;
            } else {
                i++;
            }
        }
        return max;
    }

}
