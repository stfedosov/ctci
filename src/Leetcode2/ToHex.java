package Leetcode2;

public class ToHex {

    public static void main(String[] args) {
        assert toHex(26).equals("1a");
        assert toHex(0).equals("0");
        assert toHex(-1).equals("ffffffff");
    }

    public static String toHex(int num) {
        if (num == 0) return "0";
        final StringBuilder stringBuilder = new StringBuilder();

        // convert signed integer to unsigned long
        long numLong = num & 0x00000000ffffffffL;

        while (numLong > 0) {
            stringBuilder.insert(0, getChar(numLong % 16));
            numLong /= 16;
        }

        return stringBuilder.toString();
    }

    private static char getChar(long num) {
        if (num <= 9) {
            return (char) (num + '0');
        } else {
            return (char) (num - 10 + 'a');
        }
    }
}
