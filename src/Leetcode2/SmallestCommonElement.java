package Leetcode2;

import java.util.Arrays;

public class SmallestCommonElement {

    public static void main(String[] args) {
        assert smallestCommonElement(new int[][]{
                {1, 2, 3, 4, 5},
                {2, 4, 5, 8, 10},
                {3, 5, 7, 9, 11},
                {1, 3, 5, 7, 9}}) == 5;
        assert smallestCommonElement(new int[][]{
                {1, 2, 3, 4, 5, 6},
                {2, 4, 5, 6, 8, 10},
                {3, 5, 6, 7, 9, 11},
                {1, 3, 5, 6, 7, 9}}) == 5;

        assert smallestCommonElementBruteForceCount(new int[][]{
                {1, 2, 3, 4, 5},
                {2, 4, 5, 8, 10},
                {3, 5, 7, 9, 11},
                {1, 3, 5, 7, 9}}) == 5;
        assert smallestCommonElementBruteForceCount(new int[][]{
                {1, 2, 3, 4, 5, 6},
                {2, 4, 5, 6, 8, 10},
                {3, 5, 6, 7, 9, 11},
                {1, 3, 5, 6, 7, 9}}) == 5;
    }

    public static int smallestCommonElement(int[][] mat) {
        int[] first = mat[0];
        for (int num : first) {
            boolean passed = true;
            for (int i = 1; i < mat.length; i++) {
                if (Arrays.binarySearch(mat[i], num) < 0) {
                    passed = false;
                    break;
                }
            }
            if (passed) return num;
        }
        return -1;
    }

    // Alternative - count numbers

    public static int smallestCommonElementBruteForceCount(int[][] A) {
        int[] count = new int[10001];
        int n = A.length, m = A[0].length;
        for (int j = 0; j < m; ++j)
            for (int[] ints : A)
                if (++count[ints[j]] == n)
                    return ints[j];
        return -1;
    }

}
