package Leetcode2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FindRLEArray {

    public static void main(String[] args) {
        assert findRLEArray(
                new int[][]{{1, 3}, {2, 3}},
                new int[][]{{6, 3}, {3, 3}}).equals(
                        Collections.singletonList(Arrays.asList(6, 6)));
        assert findRLEArray(new int[][]{{1, 3}, {2, 1}, {3, 2}}, new int[][]{{}}).equals(
                Arrays.asList(Arrays.asList(2, 3), Arrays.asList(6, 1), Arrays.asList(9, 2)));
    }

    public static List<List<Integer>> findRLEArray(int[][] encoded1, int[][] encoded2) {
        int i1 = 0, i2 = 0;
        int f1, f2;
        int product;
        List<int[]> list = new ArrayList<>();
        while (i1 < encoded1.length && i2 < encoded2.length) {
            f1 = encoded1[i1][1];
            f2 = encoded2[i2][1];
            product = encoded1[i1][0] * encoded2[i2][0];
            if (f1 == f2) {
                list.add(new int[]{product, f1});
                i1++;
                i2++;
            } else if (f1 < f2) {
                list.add(new int[]{product, f1});
                encoded2[i2][1] = f2 - f1;
                i1++;
            } else {
                list.add(new int[]{product, f2});
                encoded1[i1][1] = f1 - f2;
                i2++;
            }
        }
        List<List<Integer>> res = new ArrayList<>();

        int[] cur = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            if (list.get(i)[0] != cur[0]) {
                res.add(Arrays.asList(cur[0], cur[1])); // if diff from prev, add to result.
                cur = list.get(i);
            } else {
                cur[1] += list.get(i)[1]; // if same as prev, then add freq.
            }
        }
        res.add(Arrays.asList(cur[0], cur[1])); // last one
        return res;
    }

}
