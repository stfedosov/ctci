package Leetcode2;

public class CherryPickup {

    public static void main(String[] args) {
        assert cherryPickup(new int[][]{{0, 1, -1}, {1, 0, -1}, {1, 1, 1}}) == 5;
    }

    static boolean passedRoute = false;

    public static int cherryPickup(int[][] grid) {
        int result = dfs(grid, 0, 0, 0, 0, new Integer[grid.length][grid.length][grid[0].length][grid[0].length]);
        if (!passedRoute) return 0;
        return result;
    }

    private static int dfs(int[][] grid, int x1, int y1, int x2, int y2, Integer[][][][] cache) {
        if (x1 >= grid.length || y1 >= grid[x1].length || grid[x1][y1] == -1
                || x2 >= grid.length || y2 >= grid[x2].length || grid[x2][y2] == -1) {
            return Integer.MIN_VALUE;
        }
        if (x1 == grid.length - 1 && y1 == grid[x1].length - 1 && x2 == grid.length - 1 && y2 == grid[x2].length - 1) {
            passedRoute = true;
            return grid[x1][y1];
        }
        if (cache[x1][y1][x2][y2] != null) return cache[x1][y1][x2][y2];
        int res = Integer.MIN_VALUE;
        // since each person of the 2 person can move only to the bottom or to the right, then the total number of cherries
        // equals the max of the following possibilities:
        //    P1     |      P2
        //   DOWN    |     DOWN
        //   DOWN    |     RIGHT
        //   RIGHT   |     DOWN
        //   RIGHT   |     RIGHT
        res = Math.max(res, Math.max(dfs(grid, x1 + 1, y1, x2 + 1, y2, cache), dfs(grid, x1 + 1, y1, x2, y2 + 1, cache)));
        res = Math.max(res, Math.max(dfs(grid, x1, y1 + 1, x2 + 1, y2, cache), dfs(grid, x1, y1 + 1, x2, y2 + 1, cache)));
        int current;
        if (x1 == x2 && y1 == y2) {
            // if both persons standing on the same cell, don't double count and return what's in this cell (could be 1 or 0)
            current = grid[x1][y1];
        } else {
            // otherwise, number of cherries collected by both of them equals the sum of what's on their cells
            current = grid[x1][y1] + grid[x2][y2];
        }
        cache[x1][y1][x2][y2] = current + res;
        return cache[x1][y1][x2][y2];
    }


}
