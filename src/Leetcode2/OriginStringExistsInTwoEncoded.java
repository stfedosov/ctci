package Leetcode2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class OriginStringExistsInTwoEncoded {

    public static void main(String[] args) {
        assert possiblyEqualsBruteForce("i18n", "internationalization");
        assert possiblyEqualsBruteForce("l123e", "44");
        assert !possiblyEqualsBruteForce("a5b", "c5b");

        assert possiblyEquals("i18n", "internationalization");
        assert possiblyEquals("l123e", "44");
        assert !possiblyEquals("a5b", "c5b");

        assert !possiblyEqualsBruteForce("64g97q959g531q54g576g491q611g362g", "9g157q83q57q9g465q92q554g23g41q47");
    }

    // To match the strings, we need to track indices i and j, as well as the number of "wildcard" characters.
    // We just need to track the difference (diff) in wildcards.
    //
    // diff > 0 meaning we need to pick more chars in s1
    // diff < 0 meaning we need to pick more chars in s2
    //
    // 1. if both i and j are at the same location and chars are same then simply increment both pointers
    // 2. if s1[i] is not digit and diff > 0  then increment i + 1, diff - 1
    // 3. if s2[j] is not digit and diff < 0 then increment j + 1, diff + 1
    // 4. if s1[i] is digit - extract value and decrement diff val cause we covered such chars in the s1
    // 5. if s2[j] is digit - extract value and increment diff val cause we must cover such chars in the s2
    public static boolean possiblyEquals(String s1, String s2) {
        return helper(s1.toCharArray(), s2.toCharArray(), 0, 0, 0);
    }

    private static boolean helper(char[] s1, char[] s2, int i, int j, int diff) {
        if (i == s1.length && j == s2.length) {
            return diff == 0;
        }

        if (i < s1.length && j < s2.length && diff == 0 && s1[i] == s2[j]) {
            if (helper(s1, s2, i + 1, j + 1, diff)) {
                return true;
            }
        }

        if (i < s1.length && !Character.isDigit(s1[i]) && diff > 0 && helper(s1, s2, i + 1, j, diff - 1)) {
            return true;
        }

        if (j < s2.length && !Character.isDigit(s2[j]) && diff < 0 && helper(s1, s2, i, j + 1, diff + 1)) {
            return true;
        }

        // wildcard matching in s1
        int val = 0;
        for (int x = i; x < s1.length; x++) {
            if (Character.isDigit(s1[x])) {
                val = val * 10 + (s1[x] - '0');
                if (helper(s1, s2, x + 1, j, diff - val)) {
                    return true;
                }
            }
        }

        // wildcard matching in s2
        val = 0;
        for (int x = j; x < s2.length; x++) {
            if (Character.isDigit(s2[x])) {
                val = val * 10 + (s2[x] - '0');
                if (helper(s1, s2, i, x + 1, diff + val)) {
                    return true;
                }
            }
        }

        return false;
    }


    // -------===BRUTE FORCE===-------

    public static boolean possiblyEqualsBruteForce(String s1, String s2) {
        Set<String> options1 = new HashSet<>(), options2 = new HashSet<>();
        generate(s1, "", 0, options1);
        generate(s2, "", 0, options2);
        return compare(options1, options2);
    }

    private static boolean compare(Set<String> options1, Set<String> options2) {
        for (String val1 : options1) {
            for (String val2 : options2) {
                if (val1.length() != val2.length()) continue;
                if (equal(val1, val2, 0)) return true;
            }
        }
        return false;
    }

    private static boolean equal(String v1, String v2, int idx) {
        if (idx >= v1.length()) return true;
        if (v1.charAt(idx) != '*' && v2.charAt(idx) != '*' && v1.charAt(idx) != v2.charAt(idx)) return false;
        return equal(v1, v2, idx + 1);
    }

    private static void generate(String s, String sb, int idx, Set<String> res) {
        if (idx >= s.length()) {
            res.add(sb);
        } else {
            if (Character.isDigit(s.charAt(idx))) {
                StringBuilder num = new StringBuilder();
                int start = idx;
                while (start < s.length() && Character.isDigit(s.charAt(start))) {
                    num.append(s.charAt(start));
                    start++;
                }
                String tmp = sb;
                for (String ss : permutation(num.toString())) {
                    String[] split = ss.split(" ");
                    int length = 0;
                    for (String sss : split) {
                        StringBuilder m = new StringBuilder();
                        if (!sss.isEmpty()) {
                            length += sss.length();
                            int x = Integer.parseInt(sss);
                            while (x-- > 0) m.append("*");
                        }
                        sb += m.toString();
                    }
                    generate(s, sb, idx + length, res);
                    sb = tmp;
                }
            } else {
                sb += s.charAt(idx);
                generate(s, sb, idx + 1, res);
            }
        }
    }

    // for instance for 123: [123, 1 23, 12 3, 1 2 3]
    private static List<String> permutation(String str) {
        List<String> res = new ArrayList<>();
        res.add(str);
        for (int i = 1; i < str.length(); i++) {
            String first = str.substring(0, i), rest = str.substring(i);
            for (String s : permutation(rest)) {
                res.add(first + " " + s);
            }
        }
        return res;
    }

}
