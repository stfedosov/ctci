package Leetcode2;

public class ArithmeticSlices {

    public static void main(String[] args) {
        // return: 3, for 3 arithmetic slices in A: [1, 2, 3], [2, 3, 4] and [1, 2, 3, 4] itself.
        System.out.println(numberOfArithmeticSlices(new int[]{1, 2, 3, 4}));

        assert numberOfArithmeticSlices(new int[]{1, 3, 5, 7}) == 3;
    }

    public static int numberOfArithmeticSlices(int[] A) {
        int[] dp = new int[A.length]; // number of arithmetic slices ending at i
        int count = 0;
        for (int i = 2; i < A.length; i++) {
            if (A[i] - A[i - 1] == A[i - 1] - A[i - 2]) {
                dp[i] = 1 + dp[i - 1];
                count += dp[i];
            }
        }
        return count;
    }

}
