package Leetcode2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PalindromePairs {

    public static void main(String[] args) {
        assert palindromePairs(new String[]{"abcd", "dcba", "lls", "s", "sssll"}).equals(
                Arrays.asList(Arrays.asList(0, 1), Arrays.asList(1, 0), Arrays.asList(3, 2), Arrays.asList(2, 4)));
    }

    // There are several cases to be considered that isPalindrome(s1 + s2):
    //
    // Case1: If s1 is a blank string, then for any string that is palindrome s2, s1+s2 and s2+s1 are palindrome.
    //
    // Case 2: If s2 is the reversing string of s1, then s1+s2 and s2+s1 are palindrome.
    //
    // Case 3: If s1[0:cut] is palindrome and there exists s2 is the reversing string of s1[cut+1:] , then s2+s1 is palindrome.
    //
    // Case 4: Similar to case3.
    // If s1[cut+1: ] is palindrome and there exists s2 is the reversing string of s1[0:cut] , then s1+s2 is palindrome.

    public static List<List<Integer>> palindromePairs(String[] words) {
        List<List<Integer>> result = new ArrayList<>();
        Map<String, Integer> m = new HashMap<>();
        for (int i = 0; i < words.length; i++) {
            m.put(words[i], i);
        }
        if (m.containsKey("")) {
            int ind = m.get("");
            for (int i = 0; i < words.length; i++) {
                if (isPal(words[i]) && ind != i) {
                    result.add(Arrays.asList(ind, i));
                    result.add(Arrays.asList(i, ind));
                }
            }
        }
        for (int i = 0; i < words.length; i++) {
            String reversed = reverse(words[i]);
            if (m.containsKey(reversed) && m.get(reversed) != i) {
                result.add(Arrays.asList(i, m.get(reversed)));
            }
        }
        for (int i = 0; i < words.length; i++) {
            String current = words[i];
            for (int cur = 1; cur < current.length(); cur++) {
                if (isPal(current.substring(0, cur))) {
                    String rev = reverse(current.substring(cur));
                    if (m.containsKey(rev) && m.get(rev) != i) {
                        result.add(Arrays.asList(m.get(rev), i));
                    }
                }
                if (isPal(current.substring(cur))) {
                    String rev = reverse(current.substring(0, cur));
                    if (m.containsKey(rev) && m.get(rev) != i) {
                        result.add(Arrays.asList(i, m.get(rev)));
                    }
                }
            }
        }
        return result;
    }

    private static boolean isPal(String s) {
        int start = 0, end = s.length() - 1;
        while (start < end) {
            if (s.charAt(start) != s.charAt(end)) {
                return false;
            }
            start++;
            end--;
        }
        return true;
    }

    private static String reverse(String s) {
        char[] c = s.toCharArray();
        int start = 0, end = s.length() - 1;
        while (start < end) {
            char t = s.charAt(start);
            c[start] = c[end];
            c[end] = t;
            start++;
            end--;
        }
        return new String(c);
    }

}
