package Leetcode2;

public class PartitionArrayIntoDisjointIntervals {

    /*
    Given an array nums, partition it into two (contiguous) subarrays left and right so that:

    Every element in left is less than or equal to every element in right.
        left and right are non-empty.
        left has the smallest possible size.

    Return the length of left after such a partitioning.  It is guaranteed that such a partitioning exists.

    Idea: we build left maxes and right mins (e.g. array [5,0,3,8,6]):
    [5,5,5,8,8]      ====>    [5,5,[5],8,8]
    [0,0,3,6,6]               [0,0,3,[6],6]

    and then look for the first situation when leftMaxes is less than rightMins - that would be our answer
     */
    public static void main(String[] args) {
        assert partitionDisjoint(new int[]{5, 0, 3, 8, 6}) == 3;
    }

    public static int partitionDisjoint(int[] nums) {
        int[] leftMaxes = new int[nums.length];
        int[] rightMins = new int[nums.length];
        leftMaxes[0] = nums[0];
        for (int i = 1; i < nums.length; i++) {
            leftMaxes[i] = Math.max(leftMaxes[i - 1], nums[i]);
        }
        rightMins[nums.length - 1] = nums[nums.length - 1];
        for (int i = nums.length - 2; i >= 0; i--) {
            rightMins[i] = Math.min(rightMins[i + 1], nums[i]);
        }

        for (int i = 1; i < nums.length; i++) {
            if (leftMaxes[i - 1] <= rightMins[i]) return i;
        }
        return -1;
    }

}
