package Leetcode2;

public class GrumpyBookStoreOwner {

    public static void main(String[] args) {
        assert maxSatisfied(new int[]{1, 0, 1, 2, 1, 1, 7, 5}, new int[]{0, 1, 0, 1, 0, 1, 0, 1}, 3) == 16;
        assert maxSatisfied(new int[]{1}, new int[]{0}, 1) == 1;
    }

    public static int maxSatisfied(int[] customers, int[] grumpy, int minutes) {
        int max = 0, sum = 0, potential = 0;
        for (int i = 0; i < customers.length; i++) {
            if (grumpy[i] == 0)
                sum += customers[i]; // take sum when owner isn't grumpy
            else
                potential += customers[i]; // main window
            // if we exceed the window size, check if we can remove something
            if (i >= minutes && grumpy[i - minutes] == 1) {
                potential -= customers[i - minutes];
            }
            max = Math.max(max, potential);
        }
        return sum + max;
    }

}
