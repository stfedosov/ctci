package Leetcode2;

public class LongestLineInMatrix {

    public static int longestLineOptimal(int[][] mat) {
        int max = 0;
        int[][][] dp = new int[mat.length][mat[0].length][4];
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[0].length; j++) {
                if (mat[i][j] == 1) {
                    dp[i][j][0] = j > 0 ? dp[i][j - 1][0] + 1 : 1; // horizontal
                    dp[i][j][1] = i > 0 ? dp[i - 1][j][1] + 1 : 1; // vertical
                    dp[i][j][2] = (i > 0 && j > 0) ? dp[i - 1][j - 1][2] + 1 : 1; // diagonal
                    dp[i][j][3] = (i > 0 && j < mat[0].length - 1) ? dp[i - 1][j + 1][3] + 1 : 1; // anti-diagonal
                    max = Math.max(max,
                            Math.max(Math.max(dp[i][j][0], dp[i][j][1]),
                            Math.max(dp[i][j][2], dp[i][j][3])));
                }
            }
        }
        return max;
    }

    // Brute-force, time - O(m * n), space - O(1)
    public static int longestLineBruteForce(int[][] mat) {
        int max = 0;
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat[0].length; j++) {
                if (mat[i][j] == 1) {
                    max = Math.max(max,
                            Math.max(Math.max(findLength(mat, i, j, 'V'), findLength(mat, i, j, 'H')),
                                    Math.max(findLength(mat, i, j, 'D'), findLength(mat, i, j, 'A'))));
                }
            }
        }
        return max;
    }

    private static int findLength(int[][] mat, int i, int j, char dir) {
        int count = 0, i_start = i, j_start = j;
        switch (dir) {
            case 'V':
                while (i >= 0) {
                    if (mat[i][j] == 1) count++;
                    else break;
                    i--;
                }
                i = i_start + 1;
                while (i < mat.length) {
                    if (mat[i][j] == 1) count++;
                    else break;
                    i++;
                }
                break;
            case 'H':
                while (j >= 0) {
                    if (mat[i][j] == 1) count++;
                    else break;
                    j--;
                }
                j = j_start + 1;
                while (j < mat[i].length) {
                    if (mat[i][j] == 1) count++;
                    else break;
                    j++;
                }
                break;
            case 'D':
                while (j >= 0 && i >= 0) {
                    if (mat[i][j] == 1) count++;
                    else break;
                    j--;
                    i--;
                }
                j = j_start + 1;
                i = i_start + 1;
                while (i < mat.length && j < mat[i].length) {
                    if (mat[i][j] == 1) count++;
                    else break;
                    j++;
                    i++;
                }
                break;
            case 'A':
                while (i >= 0 && j < mat[i].length) {
                    if (mat[i][j] == 1) count++;
                    else break;
                    j++;
                    i--;
                }
                j = j_start - 1;
                i = i_start + 1;
                while (i < mat.length && j >= 0) {
                    if (mat[i][j] == 1) count++;
                    else break;
                    j--;
                    i++;
                }
                break;
        }

        return count;
    }

}
