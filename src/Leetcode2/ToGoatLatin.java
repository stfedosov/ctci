package Leetcode2;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ToGoatLatin {

    public static void main(String[] args) {
        assert toGoatLatin("I speak Goat Latin").equals("Imaa peaksmaaa oatGmaaaa atinLmaaaaa");
    }

    public static String toGoatLatin(String S) {
        Set<Character> vowels = new HashSet<>(Arrays.asList('a', 'e', 'i', 'o', 'u'));
        String[] split = S.split(" ");
        String a = "a";
        StringBuilder sb = new StringBuilder();
        for (String s : split) {
            if (vowels.contains(Character.toLowerCase(s.charAt(0)))) {
                sb.append(s);
            } else {
                sb.append(s.substring(1));
                sb.append(s.charAt(0));
            }
            sb.append("ma");
            sb.append(a);
            a += "a";
            sb.append(" ");
        }
        return sb.toString().trim();
    }

}
