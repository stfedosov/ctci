package Leetcode2;

public class ShortestWayToFormAString {

    public static void main(String[] args) {
        assert shortestWay("abc", "abcbc") == 2; // "abc" + "bc"
        assert shortestWay("abc", "acdbc") == -1; // impossible due to character "d" in target
        assert shortestWay("xyz", "xzyxz") == 3; // "xz" + "y" + "xz"
    }

    public static int shortestWay(String source, String target) {
        for (char c : target.toCharArray()) if (source.indexOf(c) == -1) return -1;
        int j = 0, count = 0;
        while (j < target.length()) {
            for (char c : source.toCharArray()) {
                if (j < target.length() && c == target.charAt(j)) j++;
            }
            count++;
        }
        return count;
    }

}
