package Leetcode2;

public class NumSubarrayBoundedMax {

    public static void main(String[] args) {
        // Return the number of (contiguous, non-empty) subarrays such that the value of
        // the maximum array element in that subarray is at least left and at most right.
        assert numSubarrayBoundedMax(new int[]{2, 1, 4, 3}, 2, 3) == 3; // [2], [2, 1], [3]
    }

    public static int numSubarrayBoundedMax(int[] nums, int left, int right) {
        int i = 0, j = 0, tmp = 0, count = 0;
        while (j < nums.length) {
            if (nums[j] >= left && nums[j] <= right) {
                tmp = j - i + 1;
            } else if (nums[j] > right) {
                tmp = 0;
                i = j + 1;
            }
            count += tmp;
            j++;
        }
        return count;
    }

}
