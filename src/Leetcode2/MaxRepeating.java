package Leetcode2;

public class MaxRepeating {

    public static void main(String[] args) {
        assert maxRepeating("aaabaaaabaaabaaaabaaaabaaaabaaaaba", "aaaba") == 5;
        assert maxRepeating("aaa", "a") == 3;
        assert maxRepeating("ababc", "ab") == 2;
        assert maxRepeating("ababc", "ba") == 1;
        assert maxRepeating("ababc", "ac") == 0;
    }

    public static int maxRepeating(String sequence, String word) {
        StringBuilder res = new StringBuilder();
        int count = 0;
        while (sequence.contains(res.toString())) {
            res.append(word);
            count++;
        }
        return count - 1;
    }

}
