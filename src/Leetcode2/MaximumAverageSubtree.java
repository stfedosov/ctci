package Leetcode2;

import Leetcode.TreeNode;

public class MaximumAverageSubtree {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(2);
        root.right = new TreeNode(1);
        assert Double.compare(maximumAverageSubtree(root), 1.5d) == 0;
        avg = 0.0d;
        root = new TreeNode(5);
        root.left = new TreeNode(6);
        root.right = new TreeNode(1);
        assert Double.compare(maximumAverageSubtree(root), 6.0d) == 0;
    }

    private static double avg = 0.0d;

    public static double maximumAverageSubtree(TreeNode root) {
        traverse(root);
        return avg;
    }

    private static Node traverse(TreeNode root) {
        if (root == null) return null;
        if (root.left == null && root.right == null) return new Node(root);
        Node left = traverse(root.left);
        if (left != null) {
            avg = Math.max(avg, (double) left.root.val / left.count);
        }
        Node right = traverse(root.right);
        if (right != null) {
            avg = Math.max(avg, (double) right.root.val / right.count);
        }
        Node newNode = new Node(new TreeNode(
                (left != null ? left.root.val : 0) +
                        (right != null ? right.root.val : 0) + root.val));
        newNode.count = (left == null ? 0 : left.count) + (right == null ? 0 : right.count) + 1;
        avg = Math.max(avg, (double) newNode.root.val / newNode.count);
        return newNode;
    }

    private static class Node {
        private final TreeNode root;
        private int count = 1;

        public Node(TreeNode root) {
            this.root = root;
        }
    }

}
