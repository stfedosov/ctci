package Leetcode2;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.TreeMap;

public class LeaderboardSolutions {

    class Leaderboard {

        private final Map<Integer, Integer> scores;
        private final Queue<Integer> q;

        public Leaderboard() {
            this.scores = new HashMap<>();
            this.q = new PriorityQueue<>();
        }

        // O(1)
        public void addScore(int playerId, int score) {
            this.scores.put(playerId, this.scores.getOrDefault(playerId, 0) + score);
        }

        // N * logK
        public int top(int K) {
            for (int value : this.scores.values()) {
                this.q.offer(value);
                if (this.q.size() > K) this.q.poll();
            }
            int sum = 0;
            while (!this.q.isEmpty()) sum += this.q.poll();
            return sum;
        }

        // O(1)
        public void reset(int playerId) {
            this.scores.remove(playerId);
        }
    }

    // Tradeoff for other methods to improve overall performance ---

    class Leaderboard2 {

        private final Map<Integer, Integer> map;
        private final TreeMap<Integer, Integer> sorted;

        public Leaderboard2() {
            map = new HashMap<>();
            sorted = new TreeMap<>(Collections.reverseOrder());
        }

        // logN
        public void addScore(int playerId, int score) {
            if (!map.containsKey(playerId)) {
                map.put(playerId, score);
                sorted.put(score, sorted.getOrDefault(score, 0) + 1);
            } else {
                int preScore = map.get(playerId);
                sorted.put(preScore, sorted.get(preScore) - 1);
                if (sorted.get(preScore) == 0) {
                    sorted.remove(preScore);
                }
                int newScore = preScore + score;
                map.put(playerId, newScore);
                sorted.put(newScore, sorted.getOrDefault(newScore, 0) + 1);
            }
        }

        // K * logN
        public int top(int K) {
            int count = 0;
            int sum = 0;
            MAIN_LOOP:
            for (int key : sorted.keySet()) {
                int times = sorted.get(key);
                for (int i = 0; i < times; i++) {
                    sum += key;
                    count++;
                    if (count == K) {
                        break MAIN_LOOP;
                    }
                }
            }
            return sum;
        }

        // logN
        public void reset(int playerId) {
            int preScore = map.get(playerId);
            sorted.put(preScore, sorted.get(preScore) - 1);
            if (sorted.get(preScore) == 0) {
                sorted.remove(preScore);
            }
            map.remove(playerId);
        }
    }

}
