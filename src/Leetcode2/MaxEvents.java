package Leetcode2;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class MaxEvents {

    public static void main(String[] args) {

    }

    public static int maxEventsBruteForce(int[][] events) {
        Arrays.sort(events, (a, b) -> a[1] != b[1] ? a[1] - b[1] : a[0] - b[0]);
        Set<Integer> set = new HashSet<>();
        for (int[] e : events) {
            // if there are many days can be used grab the earliest day
            for (int i = e[0]; i <= e[1]; i++) {
                if (!set.contains(i)) {
                    set.add(i);
                    break;
                }
            }
        }
        return set.size();
    }

    // Most people probably would choose to go to the one that is going to end soon.
    // And after that meeting, pick the next meeting from those that are still available.

    public static int maxEventsOptimal(int[][] events) {
        return 1;
    }

}
