package Leetcode2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RemoveSubFolders {

    public static void main(String[] args) {
        assert removeSubfolders(new String[]{"/a", "/a/b", "/c/d", "/c/d/e", "/c/f"})
                .containsAll(Arrays.asList("/a", "/c/d", "/c/f"));
        assert removeSubfolders(new String[]{"/a", "/a/b/c", "/a/b/d"})
                .containsAll(Collections.singletonList("/a"));
        assert removeSubfolders(new String[]{"/a/b/c", "/a/b/ca", "/a/b/d"})
                .containsAll(Arrays.asList("/a/b/c", "/a/b/ca", "/a/b/d"));
    }

    static class TrieNode {
        boolean isEndOfFolder;
        Map<String, TrieNode> children = new HashMap<>();

        TrieNode() {
            this.isEndOfFolder = false;
        }
    }

    static TrieNode root = new TrieNode();

    // O(N * L) where L is the longest folder in the list
    public static List<String> removeSubfolders(String[] folder) {
        // Build Trie from folder paths
        for (String path : folder) {
            TrieNode currentNode = root;
            String[] folderNames = path.split("/");

            for (String folderName : folderNames) {
                if (folderName.isEmpty()) continue;
                if (!currentNode.children.containsKey(folderName)) {
                    currentNode.children.put(folderName, new TrieNode());
                }
                currentNode = currentNode.children.get(folderName);
            }
            // Mark the end of the folder path
            currentNode.isEndOfFolder = true;
        }

        // Check each path for subfolders
        List<String> result = new ArrayList<>();
        for (String path : folder) {
            TrieNode currentNode = root;
            if (!isSubFolder(path, currentNode)) {
                result.add(path);
            }
        }

        return result;
    }

    private static boolean isSubFolder(String path, TrieNode currentNode) {
        String[] folderNames = path.split("/");

        for (int i = 0; i < folderNames.length; i++) {
            // Skip empty folder names
            if (folderNames[i].isEmpty()) continue;

            TrieNode nextNode = currentNode.children.get(folderNames[i]);
            // Check if the current folder path is a subfolder of an
            // existing folder
            if (nextNode.isEndOfFolder && i != folderNames.length - 1) {
                return true;
            }
            currentNode = nextNode;
        }
        // If not a sub-folder, add to the result
        return false;
    }

}
