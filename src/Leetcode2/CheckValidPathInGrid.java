package Leetcode2;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

public class CheckValidPathInGrid {

    /*
    Given a m x n grid. Each cell of the grid represents a street. The street of grid[i][j] can be:
    1 which means a street connecting the left cell and the right cell.
    2 which means a street connecting the upper cell and the lower cell.
    3 which means a street connecting the left cell and the lower cell.
    4 which means a street connecting the right cell and the lower cell.
    5 which means a street connecting the left cell and the upper cell.
    6 which means a street connecting the right cell and the upper cell.
    You will initially start at the street of the upper-left cell (0,0).
    A valid path in the grid is a path which starts from the upper left cell (0,0)
    and ends at the bottom-right cell (m - 1, n - 1).
    The path should only follow the streets.

    Return true if there is a valid path in the grid or false otherwise.
     */
    public static void main(String[] args) {
        assert hasValidPath(new int[][]{{2, 4, 3}, {6, 5, 2}});
        assert !hasValidPath(new int[][]{{1, 2, 1}, {1, 2, 1}});
    }

    private static final Map<Character, Set<Integer>>[] allowedPaths =
            new Map[]{
                    Map.of(
                            'L', Set.of(1, 4),
                            'R', Set.of(1, 3)),
                    Map.of(
                            'U', Set.of(2, 3, 4),
                            'D', Set.of(2, 5, 6)),
                    Map.of(
                            'L', Set.of(1, 4),
                            'D', Set.of(2, 6, 5)),
                    Map.of(
                            'R', Set.of(1, 3, 5),
                            'D', Set.of(2, 6, 5)),
                    Map.of(
                            'U', Set.of(2, 4, 3),
                            'L', Set.of(1, 4)),
                    Map.of(
                            'U', Set.of(2, 4, 3),
                            'R', Set.of(1, 3, 5))};

    private static final Map<Character, int[]> directions = Map.of(
            'L', new int[]{0, -1},
            'R', new int[]{0, 1},
            'U', new int[]{-1, 0},
            'D', new int[]{1, 0}
    );

    public static boolean hasValidPath(int[][] grid) {
        return dfs(grid, 0, 0, new boolean[grid.length][grid[0].length]);
    }

    private static boolean dfs(int[][] grid, int x, int y, boolean[][] visited) {
        if (x == grid.length - 1 && y == grid[x].length - 1) return true;
        visited[x][y] = true;
        Map<Character, Set<Integer>> allowedPathsFromHere = allowedPaths[grid[x][y] - 1];
        for (Map.Entry<Character, int[]> dir : directions.entrySet()) {
            int newX = x + dir.getValue()[0], newY = y + dir.getValue()[1];
            if (isValid(grid, allowedPathsFromHere, newX, newY, dir.getKey(), visited) && dfs(grid, newX, newY, visited))
                return true;
        }
        return false;
    }

    private static boolean isValid(int[][] grid, Map<Character, Set<Integer>> allowedPathsFromHere, int newX, int newY, char dir, boolean[][] visited) {
        if (newX < 0 || newX >= grid.length || newY < 0 || newY >= grid[newX].length || visited[newX][newY])
            return false;
        return allowedPathsFromHere.getOrDefault(dir, Collections.emptySet()).contains(grid[newX][newY]);
    }
}
