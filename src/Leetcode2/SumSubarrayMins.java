package Leetcode2;

import java.util.Arrays;
import java.util.Stack;

public class SumSubarrayMins {

    public static void main(String[] args) {
        sumSubarrayMins(new int[]{3, 1, 2, 4});
    }

    /*
                [2, 9, 7, 8, 3, 4, 6, 1]
			     |                    |
	             the previous less       the next less
	                element of 3          element of 3

	     How many subarrays with 3 being its minimum value?
         The answer is 4*3.

         9 7 8 3
         9 7 8 3 4
         9 7 8 3 4 6
         7 8 3
         7 8 3 4
         7 8 3 4 6
         8 3
         8 3 4
         8 3 4 6
         3
         3 4
         3 4 6

         How much the element 3 contributes to the final answer? It is 3*(4*3).

         What is the final answer?
         Denote by left[i] the distance between element A[i] and its PLE.
         Denote by right[i] the distance between element A[i] and its NLE.

         The final answer is, sum(A[i]*left[i]*right[i] )
     */
    private static final int mod = (int) Math.pow(10, 9) + 7;

    public static int sumSubarrayMins(int[] A) {
        int[] lastSmaller = indexOfPreviousSmallerValue(A);
         System.out.println(Arrays.toString(lastSmaller));
        int[] nextSmaller = indexOfNextSmallerValue(A);
         System.out.println(Arrays.toString(nextSmaller));
        int sum = 0;
        for (int i = 0; i < A.length; i++) {
            int cntSubarrays = (i - lastSmaller[i]) * (nextSmaller[i] - i); // Counts subarrays with A[i] as minimum value
            sum = sum % mod + A[i] * cntSubarrays;
        }
        return sum;
    }

    private static int[] indexOfPreviousSmallerValue(int[] A) {
        Stack<Integer> stack = new Stack<>();
        int[] arr = new int[A.length];
        for (int i = 0; i < A.length; i++) {
            while (!stack.isEmpty() && A[stack.peek()] > A[i]) {
                stack.pop();
            }
            arr[i] = stack.isEmpty() ? -1 : stack.peek();
            stack.push(i);
        }
        return arr;
    }

    private static int[] indexOfNextSmallerValue(int[] A) {
        Stack<Integer> stack = new Stack<>();
        int[] arr = new int[A.length];
        for (int i = A.length - 1; i >= 0; i--) {
            while (!stack.isEmpty() && A[stack.peek()] >= A[i]) {
                stack.pop();
            }
            arr[i] = stack.isEmpty() ? arr.length : stack.peek();
            stack.push(i);
        }
        return arr;
    }
}
