package Leetcode2;

import java.util.Arrays;

public class FindLongestChain {

    public static void main(String[] args) {
        assert findLongestChain(new int[][]{{1, 2}, {2, 3}, {3, 4}}) == 2;
        assert findLongestChain(new int[][]{{3, 4}, {2, 3}, {1, 2}}) == 2;
        assert findLongestChain(new int[][]{{7, 9}, {4, 5}, {7, 9}, {-7, -1}, {0, 10}, {3, 10}, {3, 6}, {2, 3}}) == 4;
    }

    private static int findLongestChain(int[][] pairs) {
        Arrays.sort(pairs, (a, b) -> a[1] - b[1]);
        int count = 1;
        int[] first = pairs[0];
        for (int[] pair : pairs) {
            if (first[1] < pair[0]) {
                first = pair;
                count++;
            }
        }
        return count;
    }

}
