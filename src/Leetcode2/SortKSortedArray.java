package Leetcode2;

import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class SortKSortedArray {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 4, 5, 2, 3, 7, 8, 6, 10, 9);
        int k = 2;

        sortKSortedArray(list, k);
        System.out.println(list);
    }

    // Function to sort a k–sorted array
    public static void sortKSortedArray(List<Integer> list, int k) {
        // create an empty min-heap and insert the first `k+1` elements into it
        Queue<Integer> pq = new PriorityQueue<>(list.subList(0, k + 1));

        int index = 0;

        // do for remaining elements in the array
        for (int i = k + 1; i < list.size(); i++) {
            // pop the top element from the min-heap and assign them to the
            // next available array index
            list.set(index++, pq.poll());

            // push the next array element into min-heap
            pq.add(list.get(i));
        }

        // pop all remaining elements from the min-heap and assign them to the
        // next available array index
        while (!pq.isEmpty()) {
            list.set(index++, pq.poll());
        }
    }

}
