package Leetcode2;

import java.util.LinkedList;
import java.util.Queue;

public class DetectCyclesIn2DGrid {

    public static void main(String[] args) {
        assert containsCycle(new char[][]{
                {'c', 'c', 'c', 'a'},
                {'c', 'd', 'c', 'c'},
                {'c', 'c', 'e', 'c'},
                {'f', 'c', 'c', 'c'}});

        assert !containsCycle(new char[][]{
                {'a', 'b', 'b'},
                {'b', 'z', 'b'},
                {'b', 'b', 'a'}});

        assert containsCycle(new char[][]{
                {'d', 'd', 'a'},
                {'d', 'd', 'c'},
                {'d', 'c', 'c'},
                {'d', 'd', 'c'},
                {'d', 'a', 'b'}});
    }

    public static boolean containsCycle(char[][] grid) {
        boolean[][] visited = new boolean[grid.length][grid[0].length];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (!visited[i][j] && bfs(grid, i, j, visited)) return true;
            }
        }
        return false;
    }

    private static final int[][] directions = new int[][]{{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

    private static boolean bfs(char[][] grid,
                               int i,
                               int j,
                               boolean[][] visited) {
        Queue<int[]> queue = new LinkedList<>();
        queue.add(new int[]{i, j});
        while (!queue.isEmpty()) {
            int[] next = queue.poll();
            if (visited[next[0]][next[1]]) return true;
            visited[next[0]][next[1]] = true;
            for (int[] dir : directions) {
                int newI = next[0] + dir[0], newJ = next[1] + dir[1];
                if (isValid(grid, newI, newJ, visited, grid[i][j])) queue.offer(new int[]{newI, newJ});
            }
        }
        return false;
    }

    private static boolean isValid(char[][] grid,
                                   int i,
                                   int j,
                                   boolean[][] visited,
                                   char expectedChar) {
        return i >= 0
                && i < grid.length
                && j >= 0
                && j < grid[i].length
                && !visited[i][j]
                && grid[i][j] == expectedChar;
    }

}
