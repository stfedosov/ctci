package Leetcode2;

import java.util.ArrayList;
import java.util.List;

public class DifferentWaysToAddParentheses {

    public static void main(String[] args) {
        System.out.println(diffWaysToCompute("2-1-1")); // [0,2]
        // ((2-1)-1) = 0
        // (2-(1-1)) = 2
        System.out.println(diffWaysToCompute("2*3-4*5")); // [-34,-14,-10,-10,10]
        // (2*(3-(4*5))) = -34
        // ((2*3)-(4*5)) = -14
        // ((2*(3-4))*5) = -10
        // (2*((3-4)*5)) = -10
        // (((2*3)-4)*5) = 10
    }

    // Time complexity: this problem is a Catalan number problem, it's approximately O(n!)
    public static List<Integer> diffWaysToCompute(String input) {
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c == '+' || c == '-' || c == '*') {
                List<Integer> list1 = diffWaysToCompute(input.substring(0, i));
                List<Integer> list2 = diffWaysToCompute(input.substring(i + 1));
                for (int v1 : list1) {
                    for (int v2 : list2) {
                        if (c == '+')
                            res.add(v1 + v2);
                        if (c == '-')
                            res.add(v1 - v2);
                        if (c == '*')
                            res.add(v1 * v2);
                    }
                }
            }
        }
        if (res.isEmpty()) res.add(Integer.parseInt(input));
        return res;
    }

}
