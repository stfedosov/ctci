package Leetcode2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FindDuplicates {

    public static void main(String[] args) {
        assert findDuplicate(
                new String[]{"root/a 1.txt(abcd) 2.txt(efgh)", "root/c 3.txt(abcd)", "root/c/d 4.txt(efgh)", "root 4.txt(efgh)"})
                .equals(Arrays.asList(Arrays.asList("root/a/2.txt", "root/c/d/4.txt", "root/4.txt"),
                        Arrays.asList("root/a/1.txt", "root/c/3.txt")));
    }

    public static List<List<String>> findDuplicate(String[] paths) {
        Map<String, List<String>> map = new HashMap<>();
        for (String path : paths) {
            String[] pathSplit = path.split(" ");
            String root = pathSplit[0];
            for (int i = 1; i < pathSplit.length; i++) {
                String[] contentAndFile = pathSplit[i].split("\\(");
                map.computeIfAbsent(contentAndFile[1], x -> new ArrayList<>()).add(root + "/" + contentAndFile[0]);
            }
        }
        // filter out lists containing only 1 path (not a duplicate)
        return map.values().stream().filter(list -> list.size() > 1).collect(Collectors.toList());
    }

    // Imagine you are given a real file system, how will you search files? DFS or BFS?
    // ---------------------------------------
    // DFS. In this case the directory path could be large.
    // DFS can reuse the shared the parent directory before leaving that directory.
    // But BFS cannot.
    //
    // If the file content is very large (GB level), how will you modify your solution?
    // ---------------------------------------
    // In this case, not realistic to match the whole string of the content.
    // So we use file footprint to understand if two files are identical.
    // Footprint can include file size, as well as sampled contents on the same positions.
    // They could have different file names and time stamps though.
    // Hashmaps are necessary to store the previous scanned file info. S = O(|keys| + |list(directory)|).
    // The key and the directory strings are the main space consumption.
    //
    //a. Sample to obtain the sliced indices in the strings stored in the RAM only once and used for all the scanned files.
    //   Accessing the strings is on-the-fly.
    //   But transforming them to hashcode used to look up in hashmap and storing the keys and the directories in the hashmap can be time consuming.
    //   The directory string can be compressed to directory id.
    //   The keys are hard to compress.
    //b. Use fast hashing algorithm e.g. MD5 or use SHA256 (no collisions found yet).
    //   If no worry about the collision, meaning the hashcode is 1-1.
    //   Thus in the hashmap, the storage consumption on key string can be replaced by key_hashcode, space usage compressed.
    //
    //If you can only read the file by 1kb each time, how will you modify your solution?
    // ---------------------------------------
    // That is the file cannot fit the whole ram.
    // Use a buffer to read controlled by a loop; read until not needed or to the end.
    // The sampled slices are offset by the times the buffer is called.
    //
    // What is the time complexity of your modified solution? What is the most time-consuming part and memory consuming part of it?
    // How to optimize?
    // ---------------------------------------
    // T = O(|num_files||sample||directory_depth|) + O(|hashmap.keys()|)
    //
    //How to make sure the duplicated files you find are not false positive?
    // ---------------------------------------
    //Add a round of final check which checks the whole string of the content.
    // T = O(|num_output_list||max_list_size||file_size|).

}
