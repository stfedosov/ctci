package Leetcode2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class RandomizedSetWithDuplicates {

    public static void main(String[] args) {
        RandomizedSetWithDuplicates randomizedSetWithDuplicates = new RandomizedSetWithDuplicates();
        randomizedSetWithDuplicates.insert(1);
        randomizedSetWithDuplicates.insert(1);
        randomizedSetWithDuplicates.remove(1);
        randomizedSetWithDuplicates.getRandom();

    }

    private final Map<Integer, Set<Integer>> dict;
    private final List<Integer> list;
    private final Random rand = new Random();

    /**
     * Initialize your data structure here.
     */
    public RandomizedSetWithDuplicates() {
        dict = new HashMap<>();
        list = new ArrayList<>();
    }

    /**
     * Inserts a value to the set. Returns true if the set did not already contain the specified element.
     */
    public boolean insert(int val) {
        dict.computeIfAbsent(val, x -> new LinkedHashSet<>()).add(list.size());
        list.add(val);
        return dict.get(val).size() == 1;
    }

    /**
     * Removes a value from the set. Returns true if the set contained the specified element.
     */
    public boolean remove(int val) {
        if (!dict.containsKey(val) || dict.get(val).size() == 0) return false;
        int idx_to_delete = dict.get(val).iterator().next();
        dict.get(val).remove(idx_to_delete);
        int last = list.get(list.size() - 1);
        list.set(idx_to_delete, last);
        dict.get(last).add(idx_to_delete);
        dict.get(last).remove(list.size() - 1);
        list.remove(list.size() - 1);
        return true;
    }

    /**
     * Get a random element from the set.
     */
    public int getRandom() {
        return list.get(rand.nextInt(list.size()));
    }

}
