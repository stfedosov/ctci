package Leetcode2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class TreeDiameter {

    public static void main(String[] args) {
        assert treeDiameter(new int[][]{{0, 1}, {0, 2}}) == 2;
        assert treeDiameter(new int[][]{{0, 1}, {1, 2}, {2, 3}, {1, 4}, {4, 5}}) == 4;
    }

    public static int treeDiameter(int[][] edges) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        for (int[] edge : edges) {
            map.computeIfAbsent(edge[0], x -> new ArrayList<>()).add(edge[1]);
            map.computeIfAbsent(edge[1], x -> new ArrayList<>()).add(edge[0]);
        }
        int[] res = bfs(0, map);
        return bfs(res[1], map)[0];
    }

    private static int[] bfs(int start, Map<Integer, List<Integer>> graph) {
        int[] distances = new int[graph.keySet().size()];
        Arrays.fill(distances, -1);
        Queue<Integer> q = new LinkedList<>();
        q.offer(start);
        distances[start] = 0;
        while (!q.isEmpty()) {
            int polled = q.poll();
            for (int adj : graph.get(polled)) {
                if (distances[adj] == -1) {
                    q.offer(adj);
                    distances[adj] = distances[polled] + 1;
                }
            }
        }
        int[] max = new int[2];
        for (int i = 0; i < distances.length; i++) {
            if (max[0] < distances[i]) {
                max[0] = distances[i];
                max[1] = i;
            }
        }
        return max;
    }

}
