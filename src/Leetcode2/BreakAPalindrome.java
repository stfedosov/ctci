package Leetcode2;

public class BreakAPalindrome {

    public static void main(String[] args) {
        assert breakPalindrome("abccba").equals("aaccba");
        assert breakPalindrome("aabaa").equals("aabab");
    }

    // 1) if character is not 'a' then change it to 'a' - string is not a palindrome and lexicographically sorted
    // 2) if we reached the middle, it means we have not get the character other than 'a',
    // other half will be also the same, so just change the last character to 'b' and return the answer

    public static String breakPalindrome(String palindrome) {
        char[] chars = palindrome.toCharArray();

        if (chars.length <= 1) return "";

        for (int i = 0; i < chars.length / 2; i++) {
            if (chars[i] != 'a') {
                chars[i] = 'a';
                return new String(chars);
            }
        }
        chars[chars.length - 1] = 'b';
        return new String(chars);
    }

}
