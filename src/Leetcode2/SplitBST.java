package Leetcode2;

import Leetcode.TreeNode;

public class SplitBST {

    /*
        Given the root of a binary search tree (BST) and an integer target, split the tree into two subtrees
        where one subtree has nodes that are all smaller or equal to the target value,
        while the other subtree has all nodes that are greater than the target value.
        It Is not necessarily the case that the tree contains a node with the value target.
     */
    public TreeNode[] splitBST(TreeNode root, int target) {
        // res[0]: root with values > target
        // res[1]: root with values <= target

        if (root == null) {
            return new TreeNode[]{null, null};
        }

        if (root.val <= target) {
            // The cut off is somewhere in the right subtree relative to root

            TreeNode[] res = splitBST(root.right, target);

            // res[1] is the node for all values <= target found in the right sub tree
            // since it's form the right sub tree, all values must be greater than root
            // so safe to do the following
            root.right = res[1];

            // root along with everything in its left subtree are already <= target
            // so after setting root.right = res[1], now root represents all nodes <= target

            // the node for all values greater than target is still res[0], some node found on the right subtree

            return new TreeNode[]{res[0], root};
        } else {
            // The cut off is somewhere in the left subtree relative to root

            TreeNode[] res = splitBST(root.left, target);

            // res[0] is the node for all values > target found in the left sub tree
            // since it's from the left sub tree, all values must be less than root
            // so safe to do the following

            root.left = res[0];

            // root along with everything in its right subtree are already > target
            // so after setting root.left = res[0], now root represents all values > target

            // the node for all values less than target is still res[1], some node found on the left subtree

            return new TreeNode[]{root, res[1]};
        }
    }

}
