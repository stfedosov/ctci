package Leetcode2;

public class WordSearch {

    public static void main(String[] args) {
        assert exist(
                new char[][]{{'A' , 'B' , 'C' , 'E'},
                        {'S' , 'F' , 'E' , 'S'},
                        {'A' , 'D' , 'E' , 'E'}},
                "ABCESEEEFS");
        assert exist(
                new char[][]{{'A' , 'B' , 'C' , 'E'},
                        {'S' , 'F' , 'C' , 'S'},
                        {'A' , 'D' , 'E' , 'E'}},
                "SEE");
        assert !exist(
                new char[][]{{'A' , 'B' , 'C' , 'E'},
                        {'S' , 'F' , 'C' , 'S'},
                        {'A' , 'D' , 'E' , 'E'}},
                "ABCB");
        assert exist(new char[][]{{'a'}}, "a");

    }

    private static int[][] directions = new int[][]{{-1, 0}, {1, 0}, {0, 1}, {0, -1}, {0, 0}};

    public static boolean exist(char[][] board, String word) {
        for (int i = 0; i < board.length; i++)
            for (int j = 0; j < board[i].length; j++)
                if (dfs(board, word, 0, i, j, new boolean[board.length][board[0].length])) return true;
        return false;
    }

    // O(M * N * 4 ^ length(word))
    private static boolean dfs(char[][] board, String word, int idx, int x, int y, boolean[][] seen) {
        if (idx == word.length()) return true;
        for (int[] dir : directions) {
            int newX = x + dir[0], newY = y + dir[1];
            if (isValid(board, newX, newY) && board[newX][newY] == word.charAt(idx) && !seen[newX][newY]) {
                seen[newX][newY] = true;
                if (dfs(board, word, idx + 1, newX, newY, seen)) return true;
                seen[newX][newY] = false;
            }
        }
        return false;
    }

    private static boolean isValid(char[][] board, int x, int y) {
        return x >= 0 && x < board.length && y >= 0 && y < board[0].length;
    }
}
