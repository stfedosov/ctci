package Leetcode2;

import Leetcode.TreeNode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FindDuplicateSubtrees {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode left = new TreeNode(2);
        root.left = left;
        left.left = new TreeNode(4);
        TreeNode right = new TreeNode(3);
        root.right = right;
        right.right = new TreeNode(4);
        TreeNode left1 = new TreeNode(2);
        right.left = left1;
        left1.left = new TreeNode(4);
        //           _ 1 _
        //         /       \
        //        2         3
        //       /         / \
        //      4         2   4
        //               /
        //              4
        System.out.println(findDuplicateSubtrees(root)); // {2, 4}, {4}
    }

    public static List<TreeNode> findDuplicateSubtrees(TreeNode root) {
        Map<String, Integer> set = new HashMap<>();
        List<TreeNode> list = new ArrayList<>();
        dfs(root, set, list);
        return list;
    }

    private static String dfs(TreeNode root, Map<String, Integer> seen, List<TreeNode> list) {
        if (root == null) return "NULL";
        String current = root.val + "," +
                dfs(root.left, seen, list) + "," +
                dfs(root.right, seen, list);
        seen.put(current, seen.getOrDefault(current, 0) + 1);
        if (seen.get(current) == 2) list.add(root);
        return current;
    }

}
