package Leetcode2;

import java.util.HashMap;
import java.util.Map;

public class MaxOperations {

    public static void main(String[] args) {
        assert maxOperations(new int[]{1, 2, 3, 4}, 5) == 2;
    }

    public static int maxOperations(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        int count = 0;
        for (int x : nums) {
            if (map.containsKey(k - x)) {
                Integer val = map.get(k - x);
                if (val - 1 == 0) map.remove(k - x);
                else map.put(k - x, val - 1);
                count++;
            } else {
                map.put(x, map.getOrDefault(x, 0) + 1);
            }
        }
        return count;
    }

}
