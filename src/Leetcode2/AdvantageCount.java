package Leetcode2;

import java.util.Arrays;
import java.util.TreeMap;

public class AdvantageCount {

    public static void main(String[] args) {
        assert Arrays.equals(advantageCount(new int[]{2, 7, 11, 15}, new int[]{1, 10, 4, 11}), new int[]{2, 11, 7, 15});
        assert Arrays.equals(advantageCount(new int[]{12, 24, 8, 32}, new int[]{13, 25, 32, 11}), new int[]{24, 32, 8, 12});
        assert Arrays.equals(advantageCount(new int[]{2, 0, 4, 1, 2}, new int[]{1, 3, 0, 0, 2}), new int[]{2, 4, 1, 2, 0});
    }

    public static int[] advantageCount(int[] A, int[] B) {
        TreeMap<Integer, Integer> treeMap = new TreeMap<>();
        for (int x : A) {
            treeMap.put(x, treeMap.getOrDefault(x, 0) + 1);
        }
        for (int i = 0; i < B.length; i++) {
            Integer higherKey = treeMap.higherKey(B[i]);
            if (higherKey != null) {
                A[i] = higherKey;
            } else {
                A[i] = treeMap.firstKey();
            }
            treeMap.put(A[i], treeMap.get(A[i]) - 1);
            if (treeMap.get(A[i]) == 0) treeMap.remove(A[i]);
        }
        return A;
    }

}
