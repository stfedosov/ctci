package Leetcode2;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class AsFarFromLandAsPossible {

    private static int[][] directions = new int[][]{{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

    public static void main(String[] args) {
        assert maxDistance(new int[][]{{1, 0, 1}, {0, 0, 0}, {1, 0, 1}}) == 2;
        assert maxDistance(new int[][]{{1, 0, 0}, {0, 0, 0}, {0, 0, 0}}) == 4;
    }

    public static int maxDistance(int[][] grid) {
        Queue<Point> q = new LinkedList<>();
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == 1) {
                    q.offer(new Point(i, j, 0));
                }
            }
        }
        if (q.size() == 0 || q.size() == grid.length * grid[0].length) return -1;
        int max = 0;
        Set<String> seen = new HashSet<>();
        while (!q.isEmpty()) {
            Point next = q.poll();
            String pointStr = next.x + "," + next.y;
            if (seen.contains(pointStr)) continue;
            seen.add(pointStr);
            for (int[] dir : directions) {
                int next_x = next.x + dir[0];
                int next_y = next.y + dir[1];
                if (next_x >= 0 && next_y >= 0 && next_x < grid.length && next_y < grid[0].length) {
                    q.offer(new Point(next.x + dir[0], next.y + dir[1], next.dist + 1));
                }
            }
            max = Math.max(max, next.dist);
        }
        return max;
    }

    private static class Point {
        private int x, y, dist;

        public Point(int x, int y, int dist) {
            this.x = x;
            this.y = y;
            this.dist = dist;
        }
    }

}
