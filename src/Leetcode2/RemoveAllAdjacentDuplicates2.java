package Leetcode2;

import java.util.Stack;

public class RemoveAllAdjacentDuplicates2 {

    public static void main(String[] args) {
        assert removeDuplicates("deeedbbcccbdaa", 3).equals("aa");
        assert removeDuplicates("abcd", 2).equals("abcd");
        assert removeDuplicates("pbbcggttciiippooaais", 2).equals("ps");
        assert removeDuplicates("yfttttfbbbbnnnnffbgffffgbbbbgssssgthyyyy", 4).equals("ybth");
    }

    public static String removeDuplicates(String s, int k) {
        Stack<Node> st = new Stack<>();
        StringBuilder sb = new StringBuilder();

        for (char c : s.toCharArray()) {
            if (!st.isEmpty() && st.peek().c == c) st.push(new Node(c, st.peek().freq + 1));
            else st.push(new Node(c, 1));
            if (st.peek().freq == k) {
                int tmp = k;
                while (tmp-- > 0) st.pop();
            }
        }
        while (!st.isEmpty()) sb.append(st.pop().c);
        return sb.reverse().toString();
    }

    private static class Node {
        private final int freq;
        private final char c;

        public Node(char c, int f) {
            this.freq = f;
            this.c = c;
        }
    }

}
