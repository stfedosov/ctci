package Leetcode2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public class UpdateMatrix {

    public static void main(String[] args) {
        assert Arrays.deepEquals(updateMatrix(new int[][]{{0, 0, 0}, {0, 1, 0}, {1, 1, 1}}),
                new int[][]{{0, 0, 0}, {0, 1, 0}, {1, 2, 1}});

        assert Arrays.deepEquals(updateMatrix(new int[][]{{0, 0, 0}, {0, 1, 0}, {0, 0, 0}}),
                new int[][]{{0, 0, 0}, {0, 1, 0}, {0, 0, 0}});
    }

    public static int[][] updateMatrix(int[][] matrix) {
        List<int[]> ids = new ArrayList<>();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] == 1) {
                    ids.add(new int[]{i, j});
                }
            }
        }
        for (int[] id : ids) {
            bfs(matrix, id[0], id[1]);
        }
        return matrix;
    }

    static int[][] coordinates = new int[][]{{-1, 0}, {1, 0}, {0, 1}, {0, -1}};

    private static void bfs(int[][] matrix, int x, int y) {
        Queue<int[]> q = new LinkedList<>();
        q.offer(new int[]{x, y});
        int level = 0;
        Set<String> seen = new HashSet<>();
        seen.add(x + "," + y);
        while (!q.isEmpty()) {
            int qSize = q.size();
            while (qSize-- > 0) {
                int[] next = q.poll();
                int _x = next[0], _y = next[1];
                for (int[] coordinate : coordinates) {
                    int new_x = _x + coordinate[0];
                    int new_y = _y + coordinate[1];
                    if (isVal(matrix, new_x, new_y)) {
                        if (matrix[new_x][new_y] == 0) {
                            matrix[x][y] = level + 1;
                            return;
                        } else if (seen.add(new_x + "," + new_y)) {
                            q.offer(new int[]{new_x, new_y});
                        }
                    }
                }
            }
            level++;
        }
    }

    private static boolean isVal(int[][] matrix, int x, int y) {
        return x >= 0 && y >= 0 && x < matrix.length && y < matrix[0].length;
    }

}
