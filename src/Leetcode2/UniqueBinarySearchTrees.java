package Leetcode2;

public class UniqueBinarySearchTrees {

    public static void main(String[] args) {
        /*
        - For a given index 'i' from 1 -> n, how do we find the number of unique BST?
            - We can first find all possible left subtrees using the values from 1 -> 'i - 1'
                - Then find all possible right subtrees using the values from 'i + 1 -> n'
        - After finding all of the possible left subtrees and right subtrees,
            - We can use the current value as the root of all pairs of left/right subtrees
                - How many new BST can we get?
                    - it will be (number of left subtrees) x (number of right subtrees)
        */
        System.out.println(numTrees(1)); // 1
        System.out.println(numTrees(3)); // 5
        System.out.println(numTrees(5)); // 42
    }

    public static int numTrees(int n) {
        int[][] cache = new int[n + 1][n + 1];
        return helper(1, n, cache);
    }

    public static int helper(int start, int end, int[][] cache) {
        if (start >= end)
            return 1;
        if (cache[start][end] != 0) return cache[start][end];

        int sum = 0;
        for (int i = start; i <= end; i++)
            sum += helper(start, i - 1, cache) * helper(i + 1, end, cache);
        cache[start][end] = sum;
        return sum;
    }

}
