package Leetcode2;

import Leetcode.TreeNode;

import java.util.HashMap;
import java.util.Map;

public class PseudoPalindromicPaths {

    public static void main(String[] args) {

    }

    private int count = 0;

    public int pseudoPalindromicPaths (TreeNode root) {
        traverse(root, new HashMap<>());
        return count;
    }

    private void traverse(TreeNode root, Map<Integer, Integer> map) {
        if (root == null) return;
        map.put(root.val, map.getOrDefault(root.val, 0) + 1);
        if (root.left == null && root.right == null) {
            if (atMostOneOddNode(map)) count++;
        } else {
            traverse(root.left, map);
            traverse(root.right, map);
        }
        map.put(root.val, map.get(root.val) - 1);
    }

    private boolean atMostOneOddNode(Map<Integer, Integer> map) {
        return map.entrySet().stream().filter(e -> e.getValue() % 2 != 0).count() <= 1;
    }

}
