package Leetcode2;

import java.util.Stack;

public class MaxSumMinProduct {

    /*
    The min-product of an array is equal to the minimum value in the array multiplied by the array's sum.

    For example, the array [3,2,5] (minimum value is 2) has a min-product of 2 * (3+2+5) = 2 * 10 = 20.
    Given an array of integers nums, return the maximum min-product of any non-empty subarray of nums.
    Since the answer may be large, return it modulo 109 + 7.

    Note that the min-product should be maximized before performing the modulo operation.
    Testcases are generated such that the maximum min-product without modulo will fit in a 64-bit signed integer.

    A subarray is a contiguous part of an array.
     */

    public static void main(String[] args) {
        assert maxSumMinProduct(new int[]{1, 2, 3, 2}) == 14;
        assert maxSumMinProduct(new int[]{3, 1, 5, 6, 4, 2}) == 60;
        assert maxSumMinProduct(new int[]{3, 3, 2, 2, 3, 1, 1, 4, 1, 3}) == 26;
    }

    public static int maxSumMinProduct(int[] nums) {
        // left_bound_index: index of the farthest element greater or equal to nums[i] in the left side
        // right_bound_index: index of the farthest element greater or equal to nums[i] in the right side
        int n = nums.length;
        int[] left_bound = new int[n], right_bound = new int[n];
        Stack<Integer> st = new Stack<>();
        for (int i = 0; i < n; i++) {
            while (!st.isEmpty() && nums[st.peek()] >= nums[i]) st.pop();
            if (!st.isEmpty())
                left_bound[i] = st.peek() + 1;
            else
                left_bound[i] = 0;
            st.push(i);
        }
        st = new Stack<>();
        for (int i = n - 1; i >= 0; i--) {
            while (!st.isEmpty() && nums[st.peek()] >= nums[i]) st.pop();
            if (!st.isEmpty())
                right_bound[i] = st.peek() - 1;
            else
                right_bound[i] = n - 1;
            st.push(i);
        }
        long[] preSum = new long[n + 1];
        for (int i = 0; i < n; i++) preSum[i + 1] = preSum[i] + nums[i];
        long maxProduct = 0;
        // We treat nums[i] as minimum number in subarray which includes nums[i]
        for (int i = 0; i < n; i++)
            // left, right inclusive
            maxProduct = Math.max(maxProduct, (preSum[right_bound[i] + 1] - preSum[left_bound[i]]) * nums[i]);
        return (int) (maxProduct % 1_000_000_007);
    }
}
