package Leetcode2;

public class CountVowelStrings {

    public static void main(String[] args) {
        // The 5 sorted strings that consist of vowels only are ["a","e","i","o","u"]
        assert countVowelStrings(1) == 5;
        // The 15 sorted strings that consist of vowels only are
        // ["aa","ae","ai","ao","au","ee","ei","eo","eu","ii","io","iu","oo","ou","uu"].
        // Note that "ea" is not a valid string since 'e' comes after 'a' in the alphabet.
        assert countVowelStrings(2) == 15;
    }

    static int count = 0;
    static char[] chars = new char[]{'a', 'e', 'i', 'o', 'u'};

    public static int countVowelStrings(int n) {
        countVowelStrings(n, new StringBuilder());
        return count;
    }

    private static void countVowelStrings(int n, StringBuilder s) {
        if (s.length() == n) {
            count++;
        } else {
            for (char c : chars) {
                if (s.length() == 0 || s.charAt(s.length() - 1) <= c) {
                    s.append(c);
                    countVowelStrings(n, s);
                    s.setLength(s.length() - 1);
                }
            }
        }
    }

}
