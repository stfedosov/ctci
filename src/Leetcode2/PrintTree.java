package Leetcode2;

import Leetcode.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class PrintTree {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        TreeNode left = new TreeNode(2);
        left.left = new TreeNode(4);
        left.right = new TreeNode(5);
        root.left = left;
        TreeNode right = new TreeNode(3);
        right.left = new TreeNode(6);
        right.right = new TreeNode(7);
        root.right = right;
        //             _ 1 _
        //           /       \
        //          2         3
        //         / \       / \
        //        4   5     6   7
        System.out.println(printTree(root));
        // [["", "", "", "1", "", "", ""],
        // ["", "2", "", "", "", "3", ""],
        // ["4", "", "5", "", "6", "", "7"]]
    }

    public static List<List<String>> printTree(TreeNode root) {
        int height = findHeight(root);
        List<List<String>> res = new ArrayList<>(height);
        int gaps = ((int) Math.pow(2, height)) - 1;
        for (int i = 0; i < height; i++) {
            List<String> tmp = new ArrayList<>();
            for (int j = 0; j < gaps; j++) {
                tmp.add("");
            }
            res.add(tmp);
        }
        fill(root, res, 0, gaps, 0);
        return res;
    }

    private static void fill(TreeNode root, List<List<String>> res, int l, int r, int idx) {
        if (root == null) return;
        res.get(idx).set((l + r) / 2, String.valueOf(root.val));
        fill(root.left, res, l, (l + r) / 2, idx + 1);
        fill(root.right, res, (l + r + 1) / 2, r, idx + 1);
    }

    private static int findHeight(TreeNode root) {
        if (root == null) return 0;
        return 1 + Math.max(findHeight(root.left), findHeight(root.right));
    }

}
