package Leetcode2;

import java.util.TreeSet;

public class ContainsNearlyAlmostDuplicate {

    public static void main(String[] args) {
        assert containsNearbyAlmostDuplicate(new int[]{1, 2, 3, 1}, 3, 0);
        assert containsNearbyAlmostDuplicate(new int[]{1, 0, 1, 1}, 1, 2);
        assert !containsNearbyAlmostDuplicate(new int[]{1, 5, 9, 1, 5, 9}, 2, 3);
    }

    // O(n*log(k))
    public static boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
        TreeSet<Long> treeSet = new TreeSet<>();
        for (int i = 0; i < nums.length; i++) {
            Long bigger = treeSet.ceiling((long) nums[i]);
            if (bigger != null && bigger - nums[i] <= t) return true;
            Long smaller = treeSet.floor((long) nums[i]);
            if (smaller != null && nums[i] - smaller <= t) return true;
            treeSet.add((long) nums[i]);
            if (treeSet.size() > k) treeSet.remove((long) nums[i - k]);
        }
        return false;
    }

}
