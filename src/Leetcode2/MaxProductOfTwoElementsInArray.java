package Leetcode2;

public class MaxProductOfTwoElementsInArray {

    public static void main(String[] args) {
        assert maxProduct(new int[]{3, 4, 5, 2}) == 12;
        assert maxProduct(new int[]{1, 5, 4, 5}) == 16;
        assert maxProduct(new int[]{3, 7}) == 12;
    }

    public static int maxProduct(int[] nums) {
        int m = Integer.MIN_VALUE, n = m;
        for (int num : nums) {
            if (num >= m) {
                n = m;
                m = num;
            } else if (num > n) {
                n = num;
            }
        }
        return (m - 1) * (n - 1);
    }

}
