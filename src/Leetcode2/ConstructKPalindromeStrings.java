package Leetcode2;

public class ConstructKPalindromeStrings {

    public static void main(String[] args) {
        assert canConstruct("annabelle", 2);
        assert !canConstruct("leetcode", 3);
        assert canConstruct("true", 4);
        assert canConstruct("yzyzyzyzyzyzyzy", 2);
        assert !canConstruct("cr", 7);
    }

    public static boolean canConstruct(String s, int k) {
        if (s.length() < k) return false;
        int[] alphabet = new int[26];
        for (char c : s.toCharArray()) {
            alphabet[c - 'a']++;
        }
        int countOdd = 0;
        for (int x : alphabet) {
            if (x % 2 != 0) countOdd++;
        }
        return countOdd <= k;
    }

}
