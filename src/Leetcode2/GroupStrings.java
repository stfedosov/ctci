package Leetcode2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupStrings {

    // Group Shifted Strings
    // We can shift a string by shifting each of its letters to its successive letter.
    // For example, "abc" can be shifted to be "bcd"
    // We can keep shifting the string to form a sequence.
    // For example, we can keep shifting "abc" to form the sequence:
    // "abc" -> "bcd" -> ... -> "xyz".
    // Given an array of strings, group all strings[i] that belong
    // to the same shifting sequence. You may return the answer in any order.
    public static void main(String[] args) {
        System.out.println(groupStrings(new String[]{"abc", "bcd", "acef", "xyz", "az", "ba", "a", "z"}));
    }

    public static List<List<String>> groupStrings(String[] strings) {
        Map<String, List<String>> group = new HashMap<>();
        for (String string : strings) {
            group.computeIfAbsent(calcHashKey(string), x -> new ArrayList<>()).add(string);
        }
        return new ArrayList<>(group.values());
    }

    private static String calcHashKey(String string) {
        int offset = string.charAt(0) - 'a';
        var key = new StringBuilder();
        for (char c : string.toCharArray()) {
            char diff = (char) (c - offset);
            key.append(diff < 'a' ? diff + 26 : diff);
        }
        return key.toString();
    }

}
