package Leetcode2;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class ShortestBridge {

    public static void main(String[] args) {

        assert shortestBridge(new int[][]{
                {0, 1},
                {1, 0}
        }) == 1;
        assert shortestBridge(new int[][]{
                {0, 1, 0},
                {0, 0, 0},
                {0, 0, 1}
        }) == 2;
        assert shortestBridge(new int[][]{
                {1, 1, 1, 1, 1},
                {1, 0, 0, 0, 1},
                {1, 0, 1, 0, 1},
                {1, 0, 0, 0, 1},
                {1, 1, 1, 1, 1}
        }) == 1;
    }

    private final static int[][] dirs = new int[][]{{-1, 0}, {0, 0}, {1, 0}, {0, 1}, {0, -1}};

    // idea: DFS to localise the first island entirely,
    // then BFS to find the shortest path to reach the second island
    public static int shortestBridge(int[][] A) {
        Queue<int[]> q = new LinkedList<>();
        Set<String> set = new HashSet<>();
        OUTER:
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[i].length; j++) {
                if (A[i][j] == 1) {
                    dfs(q, i, j, set, A);
                    break OUTER;
                }
            }
        }
        int steps = 0;
        while (!q.isEmpty()) {
            int size = q.size();
            while (size-- > 0) {
                int[] next = q.poll();
                for (int[] dir : dirs) {
                    int[] new_next = new int[]{next[0] + dir[0], next[1] + dir[1]};

                    if (isValid(new_next[0], new_next[1], A, set)) {
                        if (A[new_next[0]][new_next[1]] == 1) return steps;
                        set.add(new_next[0] + "," + new_next[1]);
                        q.offer(new_next);
                    }
                }
            }
            steps++;
        }
        return steps;
    }

    private static void dfs(Queue<int[]> q, int i, int j, Set<String> set, int[][] A) {
        if (!isValid(i, j, A, set) || A[i][j] == 0) return;
        set.add(i + "," + j);
        q.offer(new int[]{i, j});
        for (int[] dir : dirs) {
            dfs(q, i + dir[0], j + dir[1], set, A);
        }
    }

    private static boolean isValid(int i, int j, int[][] A, Set<String> set) {
        String key = i + "," + j;
        return i >= 0 && j >= 0 && i < A.length && j < A[i].length && !set.contains(key);
    }

}
