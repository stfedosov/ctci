package Leetcode2;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MakeLargeIsland {

    public static void main(String[] args) {
        assert largestIslandBruteForce(new int[][]{{1, 0}, {0, 1}}) == 3;
        assert largestIslandBruteForce(new int[][]{{1, 1}, {0, 1}}) == 4;
        assert largestIslandBruteForce(new int[][]{{1, 1}, {1, 1}}) == 4;

        assert largestIslandOptimal(new int[][]{{1, 0}, {0, 1}}) == 3;
        assert largestIslandOptimal(new int[][]{{1, 1}, {0, 1}}) == 4;
        assert largestIslandOptimal(new int[][]{{1, 1}, {1, 1}}) == 4;
    }

    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    // OPTIMAL - color islands and then try to find neighboring islands, as they can be connected by one insert
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------

    static int[][] directions = new int[][]{{1, 0}, {0, 1}, {-1, 0}, {0, -1}};

    // 1. color islands into different colors, remember island sizes
    // 2. now look at the remaining zero cells - if they are neighbour to some different colors
    // we can combine them and track max sum
    public static int largestIslandOptimal(int[][] grid) {
        int color = 2, n = grid.length, m = grid[0].length;
        Map<Integer, Integer> islandSize = new HashMap<>();
        int max = 0;
        for (int i = 0; i < n; i++) { // coloring- Pass one
            for (int j = 0; j < m; j++) {
                if (grid[i][j] == 1) {
                    int size = paintAndGetSize(i, j, color, grid);
                    max = Math.max(max, size);
                    islandSize.put(color++, size);
                }
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) { // Convert a zero to one and count- pass two
                if (grid[i][j] == 0) {
                    Set<Integer> neighborColors = new HashSet<>();
                    for (int[] dir : directions) { // We gather the color of their neighborColors
                        int newI = i + dir[0];
                        int newJ = j + dir[1];
                        if (newI >= 0 && newI < n && newJ >= 0 && newJ < m && grid[newI][newJ] > 0)
                            neighborColors.add(grid[newI][newJ]);
                    }
                    int sum = 1;
                    for (int island : neighborColors) { // Count the size of neighbor islands + 1 (0 converted to 1)
                        sum += islandSize.get(island);
                    }
                    max = Math.max(max, sum);
                }
            }
        }
        return max;
    }

    private static int paintAndGetSize(int i, int j, int color, int[][] grid) { // Paint the island and return its size
        if (i < 0 || i >= grid.length || j < 0 || j >= grid[0].length || grid[i][j] != 1) return 0;
        grid[i][j] = color;
        int sum = 1;
        for (int[] dir : directions) sum += paintAndGetSize(i + dir[0], j + dir[1], color, grid);
        return sum;
    }

    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    // BRUTE FORCE - put 1 into a zero cell and count the island size
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------

    public static int largestIslandBruteForce(int[][] grid) {
        //iterate over the grid, try to mark each 0-node as 1 and check the result
        int max = -1;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == 0) {
                    //try this node
                    grid[i][j] = 1;
                    max = Math.max(max, dfs(grid, i, j, new boolean[grid.length][grid[0].length]));
                    grid[i][j] = 0;
                }
            }
        }
        //max = -1 means the whole grid is one island
        return max == -1 ? grid.length * grid[0].length : max;
    }

    private static int dfs(int[][] grid, int row, int col, boolean[][] visited) {
        if (row < 0 || row > grid.length - 1 || col < 0 || col > grid.length - 1 || grid[row][col] == 0 || visited[row][col])
            return 0;
        visited[row][col] = true;
        return 1 + dfs(grid, row, col - 1, visited) +
                dfs(grid, row, col + 1, visited) +
                dfs(grid, row - 1, col, visited) +
                dfs(grid, row + 1, col, visited);
    }

}
