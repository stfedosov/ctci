package Leetcode2;

public class NumOfWays {

    // You have a grid of size n x 3 and you want to paint each cell of the grid with exactly one of the three colors:
    // Red, Yellow, or Green while making sure that no two adjacent cells have the same color
    public static void main(String[] args) {
        assert numOfWays(1) == 12;
        assert numOfWays(2) == 54;
    }

    // Time: O(n * 27^2), where n is the number of rows of the grid, n <= 5000
    // Explain: There are total n*a0*b0*c0 = n*3*3*3 states in dfs(n,a0,b0,c0) function (don't count a one-time call dfs(n,0,0,0)),
    // each state needs maximum 3^3 (3 for loops) to calculate the result.
    // So time complexity = O(n * 3^3 * 3^3) = O(n * 27^2)

    // Space: O(n * 4^3)
    public static int numOfWays(int n) {
        int[][][][] dp = new int[n + 1][4][4][4];
        return dfs(n, 0, 0, 0, dp);
    }

    // dfs(n, a0, b0, c0) is the number of ways to color the first n rows of the grid
    //      keeping in mind that the previous row (n + 1) has colors a0, b0 and c0
    private static int dfs(int n, int a0, int b0, int c0, int[][][][] dp) {
        if (n == 0) return 1;
        if (dp[n][a0][b0][c0] != 0) return dp[n][a0][b0][c0];
        int ans = 0;
        int[] colors = new int[]{1, 2, 3}; // Red: 1, Yellow: 2, Green: 3
        for (int a : colors) {
            if (a == a0) continue; // Check if the same color with the below neighbor
            for (int b : colors) {
                if (b == b0 || b == a) continue; // Check if the same color with the below neighbor or the left neighbor
                for (int c : colors) {
                    if (c == c0 || c == b)
                        continue; // Check if the same color with the below neighbor or the left neighbor
                    ans += dfs(n - 1, a, b, c, dp);
                    ans %= 1000_000_007;
                }
            }
        }
        dp[n][a0][b0][c0] = ans;
        return ans;
    }

}
