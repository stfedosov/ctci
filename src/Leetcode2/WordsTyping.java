package Leetcode2;

public class WordsTyping {

    public static void main(String[] args) {
        assert wordsTyping(new String[]{"hello", "world"}, 2, 8) == 1;
        assert wordsTyping(new String[]{"a", "bcd", "e"}, 3, 6) == 2;
        assert wordsTyping(new String[]{"a", "bcd", "e"}, 6, 6) == 4;
        assert wordsTyping(new String[]{"I", "had", "apple", "pie"}, 4, 5) == 1;
        assert wordsTyping(new String[]{"I", "had", "apple", "pie"}, 5, 5) == 1;
        assert wordsTyping(new String[]{"I", "had", "apple", "pie"}, 6, 5) == 2;
    }

    public static int wordsTyping(String[] sentence, int rows, int cols) {
        int count = 0;
        int line = cols;
        int i = 0;
        while (i < sentence.length) {
            while (line >= sentence[i].length()) {
                line -= sentence[i].length() + 1;
                i++;
                if (i >= sentence.length) {
                    count++;
                    i = 0;
                }
            }
            rows--;
            line = cols;
            if (rows <= 0) break;
        }
        return count;
    }

}
