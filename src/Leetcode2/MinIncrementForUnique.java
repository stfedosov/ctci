package Leetcode2;

import java.util.Arrays;

public class MinIncrementForUnique {

    public static void main(String[] args) {
        assert minIncrementForUnique(new int[]{1, 2, 2}) == 1;
        assert minIncrementForUnique(new int[]{3, 2, 1, 2, 1, 7}) == 6;
    }

    // We just need to sort the array, and then increase each element - as needed -
    // to be larger than the previous element.
    public static int minIncrementForUnique(int[] A) {
        if (A.length == 0) return 0;
        Arrays.sort(A);
        int count = 0;
        for (int i = 1; i < A.length; i++) {
            count += Math.max(0, A[i - 1] - A[i] + 1);
            A[i] += Math.max(0, A[i - 1] - A[i] + 1);
        }
        return count;
    }

}
