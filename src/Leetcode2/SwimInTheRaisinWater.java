package Leetcode2;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class SwimInTheRaisinWater {

    public static void main(String[] args) {
        assert swimInWater(new int[][]{
                {0, 1, 2, 3, 4},
                {24, 23, 22, 21, 5},
                {12, 13, 14, 15, 16},
                {11, 17, 18, 19, 20},
                {10, 9, 8, 7, 6}
        }) == 16; // route from 0 -> 1 -> 2 -> ... -> 16

        assert swimInWater(new int[][]{
                {0, 2},
                {1, 3}
        }) == 3;
    }

    private static int[][] directions = new int[][]{{-1, 0}, {0, 1}, {1, 0}, {0, -1}};

    public static int swimInWater(int[][] grid) {
        Queue<int[]> minHeap =
                new PriorityQueue<>(Comparator.comparingInt(a -> grid[a[0]][a[1]]));
        minHeap.offer(new int[]{0, 0});
        int maxSoFar = 0;
        boolean[][] visited = new boolean[grid.length][grid[0].length];
        while (!minHeap.isEmpty()) {
            int[] next = minHeap.poll();
            maxSoFar = Math.max(maxSoFar, grid[next[0]][next[1]]);
            if (next[0] == grid.length - 1 && next[1] == grid[0].length - 1) {
                break;
            }
            for (int[] dir : directions) {
                int next_x = next[0] + dir[0];
                int next_y = next[1] + dir[1];
                if (next_x >= 0
                        && next_x < grid.length
                        && next_y >= 0
                        && next_y < grid[0].length
                        && !visited[next_x][next_y]) {
                    visited[next[0]][next[1]] = true;
                    minHeap.offer(new int[]{next_x, next_y});
                }
            }
        }
        return maxSoFar;
    }

}
