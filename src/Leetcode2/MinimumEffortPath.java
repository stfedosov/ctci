package Leetcode2;

import java.util.PriorityQueue;
import java.util.Queue;

public class MinimumEffortPath {

    // find a path with minimum difference between nodes
    public static void main(String[] args) {
        assert minimumEffortPath(new int[][]{{1, 2, 2}, {3, 8, 2}, {5, 3, 5}}) == 2;
    }

    static int[][] directions = new int[][]{{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

    public static int minimumEffortPath(int[][] heights) {
        int n = heights.length;
        int m = heights[0].length;
        Queue<int[]> pq = new PriorityQueue<>((a, b) -> a[2] - b[2]);
        pq.offer(new int[]{0, 0, 0});
        int max = 0;
        boolean[][] visited = new boolean[n][m];
        while (pq.size() > 0) {
            int[] data = pq.poll();
            visited[data[0]][data[1]] = true;
            // keep tracking the max difference between heights as we go
            max = Math.max(max, data[2]);
            if (data[0] == n - 1 && data[1] == m - 1) {
                break;
            }
            for (int[] d : directions) {
                int x = data[0] + d[0];
                int y = data[1] + d[1];
                if (x < 0 || y < 0 || x >= n || y >= m || visited[x][y]) {
                    continue;
                }
                pq.offer(new int[]{x, y, Math.abs(heights[x][y] - heights[data[0]][data[1]])});
            }
        }
        return max;
    }


}
