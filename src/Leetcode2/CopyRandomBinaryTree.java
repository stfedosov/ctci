package Leetcode2;

import java.util.HashMap;
import java.util.Map;

public class CopyRandomBinaryTree {

    public static void main(String[] args) {

    }

    public NodeCopy copyRandomBinaryTree(NodeCopy root) {
        Map<NodeCopy, NodeCopy> map = new HashMap<>();
        dfs(root, map);
        dfs2(root, map);
        return map.get(root);
    }

    private void dfs(NodeCopy root, Map<NodeCopy, NodeCopy> map) {
        if (root == null) return;
        map.put(root, new NodeCopy(root.val, null, null, null));
        dfs(root.left, map);
        dfs(root.right, map);
    }

    private void dfs2(NodeCopy root, Map<NodeCopy, NodeCopy> map) {
        if (root == null) return;
        map.get(root).left = map.get(root.left);
        map.get(root).right = map.get(root.right);
        map.get(root).random = map.get(root.random);
        dfs2(root.left, map);
        dfs2(root.right, map);
    }

    public static class NodeCopy {
        int val;
        NodeCopy left;
        NodeCopy right;
        NodeCopy random;

        NodeCopy(int val, NodeCopy left, NodeCopy right, NodeCopy random) {
            this.val = val;
            this.left = left;
            this.right = right;
            this.random = random;
        }
    }

}
