package Leetcode2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class NumSpecialEquivGroups {

    public static void main(String[] args) {
        assert numSpecialEquivGroups(new String[]{"abcd", "cdab", "cbad", "xyzz", "zzxy", "zzyx"}) == 3;
        assert numSpecialEquivGroups(new String[]{"abc", "acb", "bac", "bca", "cab", "cba"}) == 3;
        assert numSpecialEquivGroups(new String[]{"ababaa", "aaabaa"}) == 2;
    }

    public static int numSpecialEquivGroups(String[] A) {
        Map<String, Integer> groups = new HashMap<>();
        for (String word : A) {
            int[] charsEven = new int[26], charsOdd = new int[26];
            for (int i = 0; i < word.length(); i++) {
                if (i % 2 == 0) charsEven[word.charAt(i) - 'a']++;
                else charsOdd[word.charAt(i) - 'a']++;
            }
            String key = Arrays.toString(charsEven) + "," + Arrays.toString(charsOdd);
            groups.put(key, groups.getOrDefault(key, 0) + 1);
        }
        return groups.size();
    }

}
