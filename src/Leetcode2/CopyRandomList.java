package Leetcode2;

import java.util.HashMap;
import java.util.Map;

public class CopyRandomList {

    // List with random pointer
    public static void main(String[] args) {
        Node node1 = new Node(1, null, null);
        Node node10 = new Node(10, null, null);
        Node node11 = new Node(11, null, null);
        Node node13 = new Node(13, null, null);
        Node node7 = new Node(7, null, null);
        node1.random = node7;
        node10.random = node11;
        node13.random = node7;
        node7.random = null;
        node11.random = node1;
        node7.next = node13;
        node13.next = node11;
        node11.next = node10;
        node10.next = node1;
        System.out.println(copyRandomList(node7));
    }

    public static Node copyRandomList(Node head) {
        if (head == null) return null;
        Map<Node, Node> map = new HashMap<>();
        Node tmp1 = head;
        while (tmp1 != null) {
            map.put(tmp1, new Node(tmp1.val, null, null));
            tmp1 = tmp1.next;
        }
        Node tmp2 = head;
        while (tmp2 != null) {
            map.get(tmp2).next = map.get(tmp2.next);
            map.get(tmp2).random = map.get(tmp2.random);
            tmp2 = tmp2.next;
        }
        return map.get(head);
    }

    static class Node {
        public int val;
        public Node next;
        public Node random;

        public Node(int _val, Node _next, Node _random) {
            val = _val;
            next = _next;
            random = _random;
        }

        @Override
        public String toString() {
            return "NodeCopy{" +
                    "val=" + val +
                    ", next=" + next +
                    ", random=" + (random != null ? random.val : -1) +
                    '}';
        }
    }

}
