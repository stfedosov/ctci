package Leetcode2;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AlienOrder {

    public static void main(String[] args) {
        System.out.println(alienOrder(new String[]{"wrt", "wrf", "er", "ett", "rftt"})); // wertf
        assert alienOrder(new String[]{"z", "z"}).equals("z");
        assert alienOrder(new String[]{"abc", "ab"}).equals("");
    }

    // Time complexity: O(total length of all the words in the input)
    public static String alienOrder(String[] words) {
        Map<Character, Set<Character>> graph = new HashMap<>();
        for (int i = 0; i < words.length - 1; i++) {
            char[] cur = words[i].toCharArray(), next = words[i + 1].toCharArray();
            // if word is a prefix and goes after the current one - we can't build an order
            // as prefixes usually should go before
            if (cur.length > next.length && words[i].startsWith(words[i + 1])) {
                return "";
            }
            for (int j = 0; j < cur.length && j < next.length; j++) {
                if (cur[j] != next[j]) {
                    graph.computeIfAbsent(cur[j], x -> new HashSet<>()).add(next[j]);
                    break;
                }
            }
        }

        boolean[] visited = new boolean[26];
        StringBuilder result = new StringBuilder();

        for (String word : words) {
            for (char c : word.toCharArray()) {
                if (hasCycle(c, graph, visited, new boolean[26], result)) {
                    return "";
                }
            }
        }
        // why reverse? because we may have this case:
        //   /---------\
        //   |         V
        //   A -> B -> C
        // i.e. we should print "C" first and then "B" and "A" and reverse it
        return result.reverse().toString();
    }

    private static boolean hasCycle(char c, Map<Character, Set<Character>> graph, boolean[] visited, boolean[] visiting, StringBuilder result) {
        if (visited[c - 'a']) {
            return false;
        }
        if (visiting[c - 'a']) {
            return true;
        }
        visiting[c - 'a'] = true;
        for (char cc : graph.getOrDefault(c, Collections.emptySet())) {
            if (hasCycle(cc, graph, visited, visiting, result)) {
                return true;
            }
        }
        visiting[c - 'a'] = false;
        visited[c - 'a'] = true;
        result.append(c);
        return false;
    }
}
