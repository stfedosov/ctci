package Leetcode2;

import Leetcode.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class UniqueBinarySearchTreesGeneration {

    public List<TreeNode> generateTrees(int n) {
        return helper(1, n);
    }

    private List<TreeNode> helper(int start, int end) {
        List<TreeNode> total = new ArrayList<>();
        if (start > end) {
            total.add(null);
            return total;
        }
        for (int i = start; i <= end; i++) {
            List<TreeNode> left = helper(start, i - 1);
            List<TreeNode> right = helper(i + 1, end);
            for (TreeNode l : left) {
                for (TreeNode r : right) {
                    TreeNode root = new TreeNode(i);
                    root.left = l;
                    root.right = r;
                    total.add(root);
                }
            }
        }
        return total;
    }

}
