package Leetcode2;

public class ValidTicTacToe {

    public static void main(String[] args) {
        assert !validTicTacToe(new String[]{"XXX", "   ", "OOO"});
        o = 0;
        x = 0;
        assert !validTicTacToe(new String[]{"OXX", "XOX", "OXO"});
        o = 0;
        x = 0;
        assert validTicTacToe(new String[]{"XOX", "OOX", "XO "});
        o = 0;
        x = 0;
        assert !validTicTacToe(new String[]{"O  ", "   ", "   "});
        o = 0;
        x = 0;
        assert !validTicTacToe(new String[]{"XOX", " X ", "   "});
    }

    private static int o = 0, x = 0;

    public static boolean validTicTacToe(String[] board) {
        char[][] board2 = convert(board);
        if (!checkNumbers(board2)) return false;
        boolean isXWon = check(board2, 'X');
        boolean isOWon = check(board2, 'O');
        if (isXWon && isOWon) return false;
        if (isXWon) {
            return x - o == 1;
        } else if (isOWon) {
            return o - x == 0 || o - x == 1;
        }
        return true;
    }

    private static boolean checkNumbers(char[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == 'O') o++;
                else if (board[i][j] == 'X') x++;
            }
        }
        return !(Math.abs(o - x) > 1 || x < o);
    }

    private static char[][] convert(String[] board) {
        char[][] res = new char[board.length][board[0].length()];
        for (int i = 0; i < res.length; i++) {
            int x = 0;
            for (int j = 0; j < res[i].length; j++) {
                res[i][j] = board[i].charAt(x++);
            }
        }
        return res;
    }

    private static boolean check(char[][] board, char symbol) {
        return checkRows(board, symbol) ||
                checkColumns(board, symbol) ||
                checkDiagonals(board, symbol);
    }

    private static boolean checkRows(char[][] board, char symbol) {
        for (int i = 0; i < board.length; i++) {
            boolean passed = true;
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] != symbol) {
                    passed = false;
                    break;
                }
            }
            if (passed) return true;
        }
        return false;
    }

    private static boolean checkColumns(char[][] board, char symbol) {
        for (int i = 0; i < board[0].length; i++) {
            boolean passed = true;
            for (int j = 0; j < board.length; j++) {
                if (board[j][i] != symbol) {
                    passed = false;
                    break;
                }
            }
            if (passed) return true;
        }
        return false;
    }

    private static boolean checkDiagonals(char[][] board, char symbol) {
        return (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[2][2] == symbol)
                || (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[2][0] == symbol);
    }

}
