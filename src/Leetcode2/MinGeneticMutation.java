package Leetcode2;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class MinGeneticMutation {

    public static void main(String[] args) {
        System.out.println(minMutation("AACCGGTT", "AACCGGTA", new String[]{"AACCGGTA"}));
        System.out.println(minMutation("AACCGGTT", "AACCGGTA", new String[]{"AACCGGTA","AACCGCTA","AAACGGTA"}));
    }

    private static char[] collection = new char[]{'A', 'C', 'G', 'T'};

    public static int minMutation(String start, String end, String[] bank) {
        Set<String> bankSet = new HashSet<>(Arrays.asList(bank));
        Queue<String> q = new LinkedList<>();
        Set<String> seen = new HashSet<>();
        seen.add(start);
        q.offer(start);
        int level = 1;
        while (!q.isEmpty()) {
            int size = q.size();
            while (size-- > 0) {
                String polled = q.poll();
                char[] nextWord = polled.toCharArray();
                for (int i = 0; i < nextWord.length; i++) {
                    char tmp = nextWord[i];
                    for (char c : collection) {
                        nextWord[i] = c;
                        String modifiedWord = new String(nextWord);
                        if (!seen.contains(modifiedWord) && bankSet.contains(modifiedWord)) {
                            if (modifiedWord.equals(end)) {
                                return level;
                            } else {
                                seen.add(modifiedWord);
                                q.offer(modifiedWord);
                            }
                        }
                    }
                    nextWord[i] = tmp;
                }
            }
            level++;
        }
        return -1;
    }

}
