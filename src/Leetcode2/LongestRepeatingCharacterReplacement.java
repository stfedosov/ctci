package Leetcode2;

public class LongestRepeatingCharacterReplacement {

    public static void main(String[] args) {
        assert characterReplacement("ABAB", 2) == 4;
        assert characterReplacement("AABABBA", 1) == 4;
    }

    // we need to keep a maxCount of the character we have seen so far
    // once we went over the maxCount + k we should shrink the window

    public static int characterReplacement(String s, int k) {
        int start = 0, max = 0, maxCount = 0;
        int[] count = new int[26];
        for (int end = 0; end < s.length(); end++) {
            char c = s.charAt(end);
            count[c - 'A']++;
            maxCount = Math.max(maxCount, count[c - 'A']);

            while (end - start + 1 > k + maxCount) {
                c = s.charAt(start++);
                count[c - 'A']--;
                maxCount = Math.max(maxCount, count[c - 'A']);
            }

            max = Math.max(end - start + 1, max);
        }

        return max;
    }

}
