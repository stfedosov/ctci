package Leetcode2;

import java.util.HashMap;
import java.util.Map;

public class NumWays {

    public static void main(String[] args) {
        assert numWays(3, 2) == 4;
        // Explanation: There are 4 different ways to stay at index 0 after 3 steps.
        // Right, Left, Stay
        // Stay, Right, Left
        // Right, Stay, Left
        // Stay, Stay, Stay
        assert numWays(2, 4) == 2;
        // Explanation: There are 2 different ways to stay at index 0 after 2 steps
        // Right, Left
        // Stay, Stay
    }

    static int MOD = 1000_000_007;

    public static int numWays(int steps, int arrLen) {
        Map<String, Long> cache = new HashMap<>();
        return (int) numWays(steps, arrLen, 0, cache);
    }

    private static long numWays(int steps, int arrLen, int idx, Map<String, Long> cache) {
        String key = steps + "," + idx;
        if (cache.containsKey(key)) {
            return cache.get(key);
        } else if (idx == 0 && steps == 0) {
            return 1;
        } else if (idx < 0 || idx >= arrLen || steps < 0) {
            return 0;
        } else {
            long count = numWays(steps - 1, arrLen, idx - 1, cache) +
                    numWays(steps - 1, arrLen, idx + 1, cache) +
                    numWays(steps - 1, arrLen, idx, cache);
            cache.put(key, count %= MOD);
            return count;
        }
    }

}
