package Leetcode2;

public class TaxCalculator {

    public static void main(String[] args) {
        /*
        Calculate tax if Salary and Tax Brackets are given as list in the form
        [ [10000, 0.3],[20000, 0.2], [30000, 0.1], [null, .1]]
        null being rest of the salary
         */
        Double[][] brackets = new Double[][]{
                {10000d, 0.3},
                {20000d, 0.2},
                {30000d, 0.1},
                {null, 0.1}};
        assert Double.compare(calculateTax(85000d, brackets), 12500d) == 0; // 10000 * 0,3 + 20000 * 0,2 + 30000 * 0,1 + 25000 * 0,1 = 12500
        assert Double.compare(calculateTax(25000d, brackets), 6000d) == 0; // 10000 * 0,3 + 15000 * 0,2 = 6000
        assert Double.compare(calculateTax(125000d, brackets), 16500d) == 0; // 10000 * 0,3 + 20000 * 0,2 + 30000 * 0,1 + 65000 * 0,1 = 16500
        assert Double.compare(calculateTax(35000d, brackets), 7500d) == 0; // 10000 * 0,3 + 20000 * 0,2 + 5000 * 0,1 = 7500
        assert Double.compare(calculateTax(10000d, brackets), 3000d) == 0; // 10000 * 0,3 = 3000
    }

    private static double calculateTax(double sum, Double[][] brackets) {
        double tax = 0.0d;
        for (Double[] bracket : brackets) {
            if (bracket[0] == null) {
                tax += sum * bracket[1];
            } else {
                if (bracket[0] >= sum) {
                    tax += sum * bracket[1];
                    return tax;
                } else {
                    tax += bracket[0] * bracket[1];
                    sum -= bracket[0];
                }
            }
        }
        return tax;
    }


}
