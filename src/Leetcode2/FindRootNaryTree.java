package Leetcode2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FindRootNaryTree {

    public static void main(String[] args) {
        Node node2 = new Node(2);
        Node node3 = new Node(3);
        Node node4 = new Node(4);
        Node node5 = new Node(5);
        Node node6 = new Node(6);
        Node node7 = new Node(7);
        Node node8 = new Node(8);
        List<Node> children1 = new ArrayList<>();
        children1.add(node2);
        children1.add(node3);
        children1.add(node4);
        children1.add(node5);
        List<Node> children3 = new ArrayList<>();
        children3.add(node6);
        children3.add(node7);
        node3.children = children3;

        List<Node> children4 = new ArrayList<>();
        children4.add(node8);
        node4.children = children4;
        Node node1 = new Node(1, children1);

        //         __ 1 __
        //       /   /  \  \
        //      /   |    |   \
        //     2    3    4   5
        //         / \   |
        //        6   7  8
        assert findRootOptimal(Arrays.asList(node3, node2, node1, node4, node5, node6, node8, node7)).val == 1;
        assert findRootSuboptimal(Arrays.asList(node3, node2, node1, node4, node5, node6, node8, node7)).val == 1;
    }

    // O(1) space, O(n) time
    public static Node findRootOptimal(List<Node> tree) {
        int allSum = 0, allChildrenSum = 0;
        for (Node node : tree) {
            allSum += node.val;
            for (Node child : node.children) {
                allChildrenSum += child.val;
            }
        }
        for (Node node : tree) {
            if (node.val == allSum - allChildrenSum) {
                return node;
            }
        }
        return null;
    }

    // O(n) space, O(n) time
    public static Node findRootSuboptimal(List<Node> tree) {
        Set<Node> children = new HashSet<>();
        for (Node n : tree) {
            dfs(n, children);
        }
        for (Node n : tree) {
            if (!children.contains(n)) return n;
        }
        return null;
    }

    private static void dfs(Node n, Set<Node> children) {
        for (Node nn : n.children) {
            if (!children.contains(nn)) {
                children.add(nn);
                dfs(n, children);
            }
        }
    }

    static class Node {
        public int val;
        public List<Node> children;

        public Node(int _val) {
            val = _val;
            children = new ArrayList<>();
        }

        public Node(int _val, List<Node> _children) {
            val = _val;
            children = _children;
        }
    }

}
