package Leetcode2;

import java.util.Arrays;
import java.util.Stack;

public class CanSeePersonsCount {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(canSeePersonsCount(new int[]{10, 6, 8, 5, 11, 9})));
    }

    public static int[] canSeePersonsCount(int[] heights) {
        int[] res = new int[heights.length];
        Stack<Integer> stack = new Stack<>();
        for (int i = heights.length - 1; i >= 0; i--) {
            while (!stack.isEmpty() && heights[stack.peek()] < heights[i]) {
                // Remove shorter people on the right side, because those people can't be seen anymore
                stack.pop();
                // i-th person can see shorter people, increase the count
                res[i]++;
            }
            // if stack is not empty then i-th person can see one more person which is taller than himself
            if (!stack.isEmpty()) res[i]++;
            stack.push(i);
        }
        return res;
    }

}
