package Chapter3;

import java.util.EmptyStackException;

/**
 * @author sfedosov.
 */
public class Exercise3_3 {

    public static void main(String[] args) {
        SetOfStacks setOfStacks = new SetOfStacks(3);
        setOfStacks.push(1);
        setOfStacks.push(2);
        setOfStacks.push(3);
        setOfStacks.push(4);
        setOfStacks.push(5);
        setOfStacks.push(6);
        assert setOfStacks.pop() == 6;
        assert setOfStacks.pop() == 5;
        assert setOfStacks.pop() == 4;
        assert setOfStacks.pop() == 3;
    }

    private static class SetOfStacks {

        private Stack<Stack<Integer>> set = new Stack<>();
        private Stack<Integer> currentStack = new Stack<>();

        private int threshold = 0;
        private int counter = 0;

        public SetOfStacks(int threshold) {
            this.threshold = threshold;
        }

        public void push(Integer item) {
            if (counter == threshold) {
                set.push(currentStack);
                currentStack = new Stack<>();
                counter = 0;
            }
            ++counter;
            currentStack.push(item);
        }

        public Integer pop() {
            if (currentStack.top == null) {
                if (set.isEmpty()) {
                    throw new EmptyStackException();
                } else {
                    currentStack = set.pop();
                    counter = threshold;
                }
            }
            Integer item = currentStack.top.data;
            currentStack.top = currentStack.top.next;
            --counter;
            return item;
        }
    }

}
