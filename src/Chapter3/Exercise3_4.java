package Chapter3;

/**
 * @author sfedosov.
 */
public class Exercise3_4 {

    public static void main(String[] args) {
        QueueWithTwoStacks queueWithTwoStacks = new QueueWithTwoStacks();
        queueWithTwoStacks.push(1);
        queueWithTwoStacks.push(2);
        queueWithTwoStacks.push(3);
        queueWithTwoStacks.push(4);
        assert queueWithTwoStacks.pop() == 1;
        assert queueWithTwoStacks.pop() == 2;
        assert queueWithTwoStacks.pop() == 3;
        assert queueWithTwoStacks.pop() == 4;
    }

    private static class QueueWithTwoStacks {

        private Stack<Integer> pushStack;
        private Stack<Integer> popStack;

        public QueueWithTwoStacks() {
            pushStack = new Stack<>();
            popStack = new Stack<>();
        }

        public void push(int item) {
            pushStack.push(item);
        }

        public int pop() {
            ensureThereAreItemsInPopStack();
            if (!popStack.isEmpty()) {
                return popStack.pop();
            }
            return -1;
        }

        public int peek() {
            ensureThereAreItemsInPopStack();
            if (!popStack.isEmpty()) {
                return popStack.peek();
            }
            return -1;
        }

        /*
          Is the queue empty?
        */
        public boolean empty() {
            return pushStack.isEmpty() && popStack.isEmpty();
        }

        private void ensureThereAreItemsInPopStack() {
            if (popStack.isEmpty()) {
                populatePopStack();
            }
        }

        private void populatePopStack() {
            while (!pushStack.isEmpty()) {
                popStack.push(pushStack.pop());
            }
        }

    }

}
