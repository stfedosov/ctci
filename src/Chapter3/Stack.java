package Chapter3;

import java.util.EmptyStackException;

/**
 * @author sfedosov.
 */
public class Stack<T> {

    public class StackNode<T> {
        public T data;
        public StackNode next;
        public StackNode(T data) {
            this.data = data;
        }

        @Override
        public String toString() {
            return "StackNode{" +
                    "data=" + data +
                    ", next=" + next +
                    '}';
        }
    }

    public StackNode<T> top;

    public void push(T item) {
        StackNode node = new StackNode<>(item);
        node.next = top;
        top = node;
    }

    public T pop() {
        if (top == null) {
            throw new EmptyStackException();
        }
        T item = top.data;
        top = top.next;
        return item;
    }

    public T peek() {
        if (top == null) {
            throw new EmptyStackException();
        }
        return top.data;
    }

    public boolean isEmpty() {
        return top == null;
    }

    @Override
    public String toString() {
        return "Stack{" +
                "top=" + top +
                '}';
    }
}

class MyStack<T> {

    private int index = 0;

    private T[] array = (T[]) new Object[100];

    public void push(T object) {
        this.array[index++] = object;
    }

    public T pop() {
        if (isEmpty()) {
            throw new EmptyStackException();
        }
        index--;
        T object = this.array[index];
        this.array[index] = null;
        return object;
    }

    public boolean isEmpty() {
        return index == 0;
    }

}
