package Chapter3;

/**
 * @author sfedosov.
 */
public class Exercise3_5 {

    public static void main(String[] args) {

        SortableStack stack = new SortableStack();

        stack.push(5);
        stack.push(3);
        stack.push(-1);
        stack.push(2);
        stack.push(7);
        stack.push(8);

        System.err.println(stack.sort());
    }

    private static class SortableStack extends Stack<Integer> {

        private Stack<Integer> temporalStack = new Stack<>();

        public Stack<Integer> sort() {
            while (!isEmpty()) {
                int max = pop();
                while (!temporalStack.isEmpty() && temporalStack.peek() > max) {
                    push(temporalStack.pop());
                }
                temporalStack.push(max);
            }
            while (!temporalStack.isEmpty()) {
                push(temporalStack.pop());
            }
            return this;
        }

    }

}
