package Chapter3;

import java.util.Arrays;
import java.util.EmptyStackException;

/**
 * @author sfedosov.
 */
public class Exercise3_1 {

    public static void main(String[] args) {
        FixedMultiStack multiStack = new FixedMultiStack(5);

        multiStack.push(1, 2);
        multiStack.push(1, 22);
        multiStack.push(1, 222);

        System.err.println(Arrays.toString(multiStack.getValues(1)));
        System.err.println(multiStack);
    }

    private static class FixedMultiStack {

        private int numberOfStacks = 3;
        private int stackCapacity;
        private int[] values;
        private int[] sizes;

        public FixedMultiStack(int stackCapacity) {
            this.stackCapacity = stackCapacity;
            values = new int[numberOfStacks * stackCapacity];
            sizes = new int[numberOfStacks];
        }

        public void push(int stackNum, int value) {
            stackNum = stackNum - 1;
            if (isFull(stackNum)) {
                throw new FullStackException();
            }
            sizes[stackNum]++;
            values[indexOfTop(stackNum)] = value;
        }

        public int pop(int stackNum) {
            stackNum = stackNum - 1;
            if (isEmpty(stackNum)) {
                throw new EmptyStackException();
            }
            int indexOfTop = indexOfTop(stackNum);
            int value = values[indexOfTop];
            values[indexOfTop] = 0;
            sizes[stackNum]--;
            return value;
        }

        public int peek(int stackNum) {
            stackNum = stackNum - 1;
            if (isEmpty(stackNum)) {
                throw new EmptyStackException();
            }
            return values[indexOfTop(stackNum)];
        }

        public int[] getValues(int stackNum) {
            int[] res = new int[stackCapacity];
            int offset = stackNum * stackCapacity;
            int x = 0;
            for (int i = offset - stackCapacity; i < offset; i++) {
                res[x] = values[i];
                x++;
            }
            return res;
        }

        public boolean isFull(int stackNum) {
            return sizes[stackNum] == stackCapacity;
        }

        public boolean isEmpty(int stackNum) {
            return sizes[stackNum] == 0;
        }

        public int indexOfTop(int stackNum) {
            int offset = stackNum * stackCapacity;
            int size = sizes[stackNum];
            return offset + size - 1;
        }

        @Override
        public String toString() {
            return "FixedMultiStack{" +
                    "numberOfStacks=" + numberOfStacks +
                    ", stackCapacity=" + stackCapacity +
                    ", values=" + Arrays.toString(values) +
                    ", sizes=" + Arrays.toString(sizes) +
                    '}';
        }
    }

    private static class FullStackException extends RuntimeException {
        public FullStackException() {
        }
    }
}
