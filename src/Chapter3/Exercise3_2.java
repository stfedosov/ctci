package Chapter3;

import java.util.EmptyStackException;

/**
 * @author sfedosov.
 */
public class Exercise3_2 {

    public static void main(String[] args) {
        StackWithMinElementTracking minElementTracking = new StackWithMinElementTracking();
        minElementTracking.push(5);
        assert minElementTracking.min().equals(5);

        minElementTracking.push(6);
        assert minElementTracking.min().equals(5);

        minElementTracking.push(3);
        assert minElementTracking.min().equals(3);

        minElementTracking.push(7);
        assert minElementTracking.min().equals(3);

        minElementTracking.pop();
        assert minElementTracking.min().equals(3);

        minElementTracking.pop();
        assert minElementTracking.min().equals(5);
    }

    private static class StackWithMinElementTracking extends Stack<Integer> {

        private Stack<Integer> minElements = new Stack<>();

        @Override
        public void push(Integer item) {
            if (minElements.isEmpty() || minElements.top.data.compareTo(item) > 0) {
                minElements.push(item);
            }
            StackNode node = new StackNode<>(item);
            node.next = top;
            top = node;
        }

        public Integer min() {
            return minElements.peek();
        }

        @Override
        public Integer pop() {
            if (top == null) {
                throw new EmptyStackException();
            }
            int item = top.data;
            if (minElements.top.data.compareTo(item) == 0) {
                minElements.pop();
            }
            top = top.next;
            return item;
        }
    }

}
