package educative;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FindKSumSubsets {

    public static void main(String[] args) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> list = Arrays.asList(8, 13, 3, 22, 17, 39, 87, 45, 36);
        get_k_sum_subsets_1(list, 125, result);
        System.out.println(result);
        list = Arrays.asList(1, 3, 5, 21, 19, 7, 2, 5);
        result = new ArrayList<>();
        get_k_sum_subsets_1(list, 10, result);
        System.out.println(result);
    }

    private static void get_k_sum_subsets_1(List<Integer> v,
                                            int target_sum,
                                            List<List<Integer>> sets) {
        get_k_sum_subsets_1(v, 0, target_sum, sets, new ArrayList<>());
    }

    private static void get_k_sum_subsets_1(List<Integer> v,
                                            int i,
                                            int target_sum,
                                            List<List<Integer>> sets,
                                            List<Integer> set) {
        if (i == v.size()) {
            if (target_sum == 0) {
                sets.add(new ArrayList<>(set));
            }
        } else {
            set.add(v.get(i));
            get_k_sum_subsets_1(v, i + 1, target_sum - v.get(i), sets, set);
            set.remove(set.size() - 1);
            get_k_sum_subsets_1(v, i + 1, target_sum, sets, set);
        }
    }
}
