package educative;

/**
 * @author sfedosov on 2/1/19.
 */
public class LeastCommonNumber {

    public static void main(String[] args) {
        System.out.println(find_least_common_number(new int[]{6, 7, 10, 25, 30, 63, 64},
                new int[]{-1, 4, 5, 6, 7, 8, 50},
                new int[]{1, 6, 10, 14}));
        System.out.println(find_least_common_number(new int[]{6, 7},
                new int[]{-1, 4, 5, 6},
                new int[]{6}));
    }

    private static int find_least_common_number(int[] arr1,
                                                int[] arr2,
                                                int[] arr3) {
        int iter1 = 0;
        int iter2 = 0;
        int iter3 = 0;
        int result = 0;
        while (iter1 < arr1.length && iter2 < arr2.length && iter3 < arr3.length) {
            if (arr1[iter1] == arr2[iter2] && arr1[iter1] == arr3[iter3]) {
                result = arr1[iter1];
                break;
            }
            if (arr1[iter1] < arr2[iter2] || arr1[iter1] < arr3[iter3]) iter1++;
            else if (arr2[iter2] < arr1[iter1] || arr2[iter2] < arr3[iter3]) iter2++;
            else if (arr3[iter3] < arr2[iter2] || arr3[iter3] < arr1[iter1]) iter3++;
        }
        return result;
    }

}
