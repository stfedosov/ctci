package educative;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

public class FindMaxSlidingWindow {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(maxSlidingWindow(new int[]{1, 2, 3, 4, 3, 2, 1, 2, 5}, 4)));
        System.out.println(Arrays.toString(maxSlidingWindow(new int[]{1, 2, 3, 4, 3, 2, 1, 2, 5}, 3)));
        System.out.println(Arrays.toString(maxSlidingWindow(new int[]{1, 2, 3, 4, 3, 2, 1, 2, 5}, 5)));
    }

    public static int[] maxSlidingWindow(int[] nums, int k) {
        Deque<Integer> deq = new ArrayDeque<>();
        int n = nums.length;
        if (n == 0 || k == 0) return new int[0];
        if (k == 1) return nums;

        int max_idx = 0;
        for (int i = 0; i < k; i++) {
            // remove indexes of elements not from sliding window
            if (!deq.isEmpty() && deq.getFirst() == i - k)
                deq.removeFirst();

            // remove from deq indexes of all elements
            // which are smaller than current element nums[i]
            while (!deq.isEmpty() && nums[i] > nums[deq.getLast()])
                deq.removeLast();
            deq.addLast(i);
            if (nums[i] > nums[max_idx]) max_idx = i;
        }
        int[] output = new int[n - k + 1];
        output[0] = nums[max_idx];

        // build output
        for (int i = k; i < n; i++) {
            if (!deq.isEmpty() && deq.getFirst() == i - k)
                deq.removeFirst();

            // remove from deq indexes of all elements
            // which are smaller than current element nums[i]
            while (!deq.isEmpty() && nums[i] > nums[deq.getLast()])
                deq.removeLast();
            deq.addLast(i);
            output[i - k + 1] = nums[deq.getFirst()];
        }
        return output;
    }
}
