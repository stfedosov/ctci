package educative;

import java.util.Arrays;

public class SumOfThree {

    public static void main(String[] args) {
        assert !find_sum_of_three(new int[]{3, 7, 1, 2, 8, 4, 5}, 21);
        assert find_sum_of_three(new int[]{3, 7, 1, 2, 8, 4, 5}, 20);
    }

    private static boolean find_sum_of_three(int arr[], int required_sum) {
        Arrays.sort(arr);
        for (int i = 0; i < arr.length - 2; i++) {
            for (int j = i + 1; j < arr.length - 1; j++) {
                int remaining_sum = required_sum - arr[i] - arr[j];
                int k = Arrays.binarySearch(arr, remaining_sum);
                if (k > 0 && k != i && k != j) return true;
            }
        }
        return false;
    }

}
