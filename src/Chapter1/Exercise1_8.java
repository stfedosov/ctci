package Chapter1;

/**
 * @author sfedosov.
 */
public class Exercise1_8 {

    public static void main(String[] args) {
        int[][] matrix = new int[][]{
                {1, 2, 3, 4},
                {5, 6, 0, 8},
                {0, 10, 11, 12},
                {13, 14, 15, 16}
        };
        int[][] nullifiedMatrix = nullifyMatrix(matrix);
        // As a result we should have following matrix:
        // 0 2 0 4
        // 0 0 0 0
        // 0 0 0 0
        // 0 14 0 16
        for (int[] aRotatedMatrix : nullifiedMatrix) {
            for (int j = 0; j < nullifiedMatrix.length; j++) {
                System.out.print(aRotatedMatrix[j] + " ");
            }
            System.out.println();
        }
    }

    private static int[][] nullifyMatrix(int[][] matrix) {
        int N = matrix.length;
        boolean[] rows = new boolean[N];
        boolean[] columns = new boolean[N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (matrix[i][j] == 0) {
                    rows[i] = true;
                    columns[j] = true;
                }
            }
        }
        for (int i = 0; i < N; i++) {
            if (rows[i]) {
                for (int ii = 0; ii < N; ii++) {
                    matrix[i][ii] = 0;
                }
            }
        }
        for (int i = 0; i < N; i++) {
            if (columns[i]) {
                for (int ii = 0; ii < N; ii++) {
                    matrix[ii][i] = 0;
                }
            }
        }
        return matrix;
    }

}
