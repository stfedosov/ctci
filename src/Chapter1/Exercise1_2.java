package Chapter1;

import java.util.Arrays;

/**
 * @author sfedosov.
 */
public class Exercise1_2 {

    public static void main(String[] args) {
        String word1 = args[0];
        String word2 = args[1];
        boolean isPermutation = isPermutationWithSorting(word1, word2);
        assert (isPermutation == isPermutationWithMassive(word1, word2));
        if (isPermutation) {
            System.out.println("Entered words are permutations of each other");
        } else {
            System.out.println("Entered words are not the permutations of each other");
        }
    }

    private static boolean isPermutationWithSorting(String word1, String word2) {
        char[] sorted1 = word1.toCharArray();
        Arrays.sort(sorted1);
        char[] sorted2 = word2.toCharArray();
        Arrays.sort(sorted2);
        return new String(sorted1).equalsIgnoreCase(new String(sorted2));
    }

    private static boolean isPermutationWithMassive(String word1, String word2) {
        int[] mass = new int[256];
        for (int i = 0; i < word1.length(); i++) {
            mass[word1.charAt(i)]++;
        }
        for (int i = 0; i < word2.length(); i++) {
            char c = word2.charAt(i);
            mass[c]--;
            if (mass[c] < 0) {
                return false;
            }
        }
        return true;
    }

}
