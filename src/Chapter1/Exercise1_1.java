package Chapter1;

/**
 * @author sfedosov.
 */
public class Exercise1_1 {

    public static void main(String[] args) {
        String word = args[0];
        boolean isUnique = checkForTheUniquenessWithMassive(word);
        assert (isUnique == checkForTheUniquenessWithBitVector(word));
        if (isUnique) {
            System.out.println("Entered word contains only unique characters.");
        } else {
            System.out.println("Entered word contains not only unique characters.");
        }
    }

    private static boolean checkForTheUniquenessWithMassive(String word) {
        int[] mass = new int[256];
        for (int i = 0; i < word.length(); i++) {
            mass[word.charAt(i)]++;
        }
        for (int i : mass) {
            if (i > 1) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkForTheUniquenessWithBitVector(String word) {
        int ch = 0;
        for (int i = 0; i < word.length(); i++) {
            int val = word.charAt(i) - 'a';
            if ((ch & (1 << val)) > 0) {
                return false;
            }
            ch |= (1 << val);
        }
        return true;
    }

/*

Iterations for the last method, based on a bit vector.

Entered word: "abcdc"

"a"
ch = 0000 0000 0000 0000
val = s.charAt(i) - 'a' = 0000 0000 0000 0000
1 << val = 0000 0000 0000 0001
(ch & (1 << val)) = 0000 0000 0000 0000
ch = 0000 0000 0000 0001

"b"
ch = 0000 0000 0000 0001
val = s.charAt(i) - 'a' = 0000 0000 0000 0001
1 << val = 0000 0000 0000 0010
(ch & (1 << val)) = 0000 0000 0000 0000
ch = 0000 0000 0000 0011

"c"
ch = 0000 0000 0000 0011
val = s.charAt(i) - 'a' = 0000 0000 0000 0010
1 << val = 0000 0000 0000 0100
(ch & (1 << val)) = 0000 0000 0000 0000
ch = 0000 0000 0000 0111

"d"
ch = 0000 0000 0000 0111
val = s.charAt(i) - 'a' = 0000 0000 0000 0011
1 << val = 0000 0000 0000 1000
(ch & (1 << val)) = 0000 0000 0000 0000
ch = 0000 0000 0000 1111

"c"
ch = 0000 0000 0000 1111
val = s.charAt(i) - 'a' = 0000 0000 0000 0010
1 << val = 0000 0000 0000 0100
(ch & (1 << val)) = 0000 0000 0000 0100

Since (ch & (1 << val)) > 0, we can conclude that we hit the bit, which was already in our bit vector

*/

}
