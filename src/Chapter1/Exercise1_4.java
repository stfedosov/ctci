package Chapter1;

/**
 * @author sfedosov.
 */
public class Exercise1_4 {

    public static void main(String[] args) {
        String word = args[0];
        boolean isPermutationOfPalindrome = isPermutationOfPalindrome(word);
        assert (isPermutationOfPalindrome == isPermutationOfPalindromeWithUsingBitVector(word));
        System.out.println("Entered word is " +
                (isPermutationOfPalindrome ? "permutation of palindrome" : "is not a permutation of palindrome"));
    }

    private static boolean isPermutationOfPalindrome(String word) {
        int[] mass = new int[256];
        for (int i = 0; i < word.length(); i++) {
            char charAt = word.charAt(i);
            if (charAt != ' ') {
                mass[charAt]++;
            }
        }
        int oddNumbersCounter = 0;
        for (int i : mass) {
            if (i % 2 != 0) {
                oddNumbersCounter++;
            }
        }
        return oddNumbersCounter <= 1;
    }

    private static boolean isPermutationOfPalindromeWithUsingBitVector(String word) {
        int ch = 0;
        for (int i = 0; i < word.length(); i++) {
            int val = word.charAt(i) - 'a';
            int mask = 1 << val;
            if ((ch & mask) == 0) {
                ch |= mask;
            } else {
                ch &= ~mask;
            }
        }
        return ch == 0 || (ch & (ch - 1)) == 0;
    }
}

/*
Iterations for the last method, based on a bit vector.

Entered word: "abbadeacca"

"a"
mask = 0000 0001
ch & mask = 0000 0000
ch | mask = 0000 0001

"b"
mask = 0000 0010
ch & mask = 0000 0000
ch | mask = 0000 0011

"b"
mask = 0000 0010
ch & mask = 0000 0010
ch & ~mask = 0000 0001

"a"
mask = 0000 0001
ch & mask = 0000 0001
ch & ~mask = 0000 0000

"d"
mask = 0000 1000
ch & mask = 0000 0000
ch | mask = 0000 1000

"e"
mask = 0001 0000
ch & mask = 0000 0000
ch | mask = 0001 1000

"a"
mask = 0000 0001
ch & mask = 0000 0000
ch | mask = 0001 1001

"c"
mask = 0000 0100
ch & mask = 0000 0000
ch | mask = 0001 1101

"c"
mask = 0000 0100
ch & mask = 0000 0100
ch & ~mask = 0001 1001

"a"
mask = 0000 0001
ch & mask = 0000 0001
ch & ~mask = 0001 1000

ch = 0001 1000 != 0000 0000
and
ch = 0001 1000 & 0001 0111 = 0001 0000 != 0000 0000
*/