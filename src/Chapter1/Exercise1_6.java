package Chapter1;

/**
 * @author sfedosov.
 */
public class Exercise1_6 {

    public static void main(String[] args) {
        String word = args[0];
        System.out.println("Entered string in compressed representation: " + compressString(word));
    }

    private static String compressString(String word) {
        int counter = 0;
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < word.length(); i++) {
            counter++;
            char current = word.charAt(i);
            if ((i + 1 < word.length() && word.charAt(i + 1) != current)
                    || i + 1 == word.length()) {
                stringBuilder.append(current);
                if (counter > 1) {
                    stringBuilder.append(counter);
                }
                counter = 0;
            }
        }
        return stringBuilder.toString();
    }

}
