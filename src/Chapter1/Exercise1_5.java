package Chapter1;

/**
 * @author sfedosov.
 */
public class Exercise1_5 {

    public static void main(String[] args) {
        String word1 = args[0];
        String word2 = args[1];
        System.out.println("Entered strings were edited " +
                (checkOneOrZeroEdits(word1, word2) ? "one or zero times" : "many times"));
    }

    private static boolean checkOneOrZeroEdits(String word1, String word2) {
        int stringDifference = Math.abs(word1.length() - word2.length());
        return stringDifference <= 1 && check(word1, word2);
    }

    private static boolean check(String word1, String word2) {
        int[] mass1 = new int[256];
        for (int i = 0; i < word1.length(); i++) {
            mass1[word1.charAt(i)]++;
        }
        for (int i = 0; i < word2.length(); i++) {
            mass1[word2.charAt(i)]--;
        }
        int countNegative = 0;
        for (int m : mass1) {
            if (m < 0) {
                countNegative++;
            }
        }
        return countNegative <= 1;
    }

}
