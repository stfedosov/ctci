package Chapter1;

/**
 * @author sfedosov.
 */
public class Exercise1_9 {

    public static void main(String[] args) {
        // s1 - args[0]
        // s2 - args[1]
        String s1 = args[0];
        String s2 = args[1];
        boolean isSubstring = false;
        if (s2.length() == s1.length()) {
            isSubstring = isSubstring(s2, s1);
            assert (isSubstring == isSubstring2(s2, s1));
        }
        System.out.println("Entered word s2 is" +
                (isSubstring ? " a substring of s1" : " not a substring of s1"));
    }

    private static boolean isSubstring(String s2, String s1) {
        return (s1 + s1).contains(s2);
    }

    private static boolean isSubstring2(String s2, String s1) {
        int[] mass = new int[256];
        for (int i = 0; i < s1.length(); i++) {
            mass[s1.charAt(i)]++;
        }
        for (int i = 0; i < s2.length(); i++) {
            mass[s2.charAt(i)]--;
        }
        for (int ii : mass) {
            if (ii < 0) {
                return false;
            }
        }
        return true;
    }

}
