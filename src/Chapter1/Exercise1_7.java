package Chapter1;

/**
 * @author sfedosov.
 */
public class Exercise1_7 {

    public static void main(String[] args) {
        int[][] matrix = new int[][]{
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16}
        };
        int[][] rotatedMatrix = rotateMatrix(matrix);
        // As a result we should have following matrix:
        // 13 9 5 1
        // 14 10 6 2
        // 15 11 7 3
        // 16 12 8 4
        for (int[] aRotatedMatrix : rotatedMatrix) {
            for (int j = 0; j < rotatedMatrix.length; j++) {
                System.out.print(aRotatedMatrix[j] + " ");
            }
            System.out.println();
        }
    }

    private static int[][] rotateMatrix(int[][] matrix) {
        int N = matrix.length;
        int[][] newMatrix = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                newMatrix[i][j] = matrix[N - j - 1][i];
            }
        }
        return newMatrix;
    }

}