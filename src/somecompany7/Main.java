package somecompany7;

/*
Implement a singly-linked list structure for generic value type (string/number/etc).
Design a minimal interface required to iterate through it.
Extend your implementation with below methods (in order of appearance):
 - method to return the number of elements in the list
 - method to retrieve a value at the specified position in this list
 - method to insert a value at the specified position in this list
*/

class Solution {
    public static void main(String[] args) {
        MyLinkedList list = new MyLinkedListImpl();
        list.insert(0, 1);
        list.insert(0, 2);
        list.insert(2, 3);
        assert list.extractElement(0) == 2;
        assert list.extractElement(1) == 1;
        assert list.extractElement(2) == 3;
        assert list.size() == 3;
    }
}


interface MyLinkedList {

    int size();

    int extractElement(int pos);

    void insert(int pos, int value);

}

class Node {

    public Node(int value) {
        this.value = value;
    }

    public int value;
    public Node next;

}

class MyLinkedListImpl implements MyLinkedList {

    private Node list;
    private int size = 0;

    public int size() {
        return size;
    }

    public int extractElement(int pos) {
        validatePosition(pos);
        Node current = list;
        while (pos-- > 0) {
            current = current.next;
        }
        return current.value;
    }

    private void validatePosition(int pos) {
        if (pos > size) {
            throw new RuntimeException("Incorrect position to insert!");
        }
    }

    public void insert(int pos, int value) {
        validatePosition(pos);
        size++;
        Node newNode = new Node(value);
        Node current = list;
        Node previous = null;
        if (list == null) {
            list = newNode;
            return;
        }
        while (pos >= 0) {
            if (pos == 0) {
                newNode.next = current;
                if (previous == null) {
                    current = newNode;
                    list = current;
                } else {
                    previous.next = newNode;
                }
            } else {
                previous = current;
                current = current.next;
            }
            pos--;
        }
    }

}

