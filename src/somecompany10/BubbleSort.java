package somecompany10;

import java.util.Arrays;

public class BubbleSort {

    public static void main(String[] args) {
        int[] array = {1, 6, 2, 3, 4, 7, 8, 9};
        bubbleSortNonOptimized(array);
        System.out.println(Arrays.toString(array));
        int[] array2 = {4, 5, 3, 1, 9, 10};
        bubbleSortOptimized(array2);
        System.out.println(Arrays.toString(array2));
    }

    private static void bubbleSortNonOptimized(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
    }

    private static void bubbleSortOptimized(int[] array) {
        for (int i = 0; i < array.length; i++) {
            boolean swapped = false;
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                    swapped = true;
                }
            }
            if (!swapped) break;
        }
    }

}
