package somecompany10;

public class RepeatedChars {

    public static void main(String[] args) {
        assert reduce("aaaaabbbbbcccdd", 2).equals("aabbccdd");
        assert reduce("aaaaabbbbbcccdd", 3).equals("aaabbbcccdd");
        assert reduce("aaaaabbbbbcccdd", 1).equals("abcd");
        assert reduce("aaaaabbbbbcccdd", -1).equals("aaaaabbbbbcccdd");
        assert reduce("abcd", 2).equals("abcd");
        assert reduce("", 2).equals("");
        assert reduce("", -2).equals("");
        assert reduce(null, 2) == null;
        assert reduce("abcd", 0).equals("");
        assert reduce("abcd", 1).equals("abcd");
    }

    private static String reduce(String input, int max) {
        if (input == null || input.isEmpty() || max < 0) return input;
        if (max == 0) return "";
        int count = 1;
        char current = input.charAt(0);
        StringBuilder result = new StringBuilder("" + input.charAt(0));
        for (int i = 1; i < input.length(); i++) {
            char c = input.charAt(i);
            if (current == c) {
                count++;
            } else {
                current = c;
                count = 1;
            }
            if (count <= max) {
                result.append(current);
            }
        }
        return result.toString();
    }


}
