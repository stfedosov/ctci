package Leetcode3;

import java.util.HashSet;
import java.util.Set;

public class NumSplits {

    public static void main(String[] args) {
    /*
    You are given a string s.

    A split is called good if you can split s into two non-empty strings s_left and s_right
    where their concatenation is equal to s (i.e., s_left + s_right = s) and the number of distinct
    letters in s_left and s_right is the same.

    Return the number of good splits you can make in s.
     */
        System.out.println(numSplits("aacaba") == 2);
        System.out.println(numSplits("abcd") == 1);
    }

    public static int numSplits(String s) {
        int[] left = new int[s.length()], right = new int[s.length()];
        Set<Character> set = new HashSet<>();
        int i1 = 0, i2 = right.length - 1;
        for (int x = 0; x < s.length(); x++) {
            set.add(s.charAt(x));
            left[i1++] = set.size();
        }
        set = new HashSet<>();
        for (int x = s.length() - 1; x >= 0; x--) {
            set.add(s.charAt(x));
            right[i2--] = set.size();
        }
        int total = 0;
        for (int i = 0; i < s.length() - 1; i++) {
            if (left[i] == right[i + 1]) {
                total += 1;
            }
        }
        return total;
    }

}
