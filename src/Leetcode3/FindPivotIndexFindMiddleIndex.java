package Leetcode3;

public class FindPivotIndexFindMiddleIndex {

    public static void main(String[] args) {
        System.out.println(findPivotIndexFindMiddleIndex(new int[]{1, 7, 3, 6, 5, 6})); // 3
        System.out.println(findPivotIndexFindMiddleIndex(new int[]{1, 2, 3})); // -1
        System.out.println(findPivotIndexFindMiddleIndex(new int[]{2, 1, -1})); // 0
        System.out.println(findPivotIndexFindMiddleIndex(new int[]{2, 3, -1, 8, 4})); // 3
    }

    /*
    Find the leftmost middleIndex (i.e., the smallest amongst all the possible ones).

    A middleIndex is an index where nums[0] + nums[1] + ... + nums[middleIndex-1] ==
    nums[middleIndex+1] + nums[middleIndex+2] + ... + nums[nums.length-1].
     */
    public static int findPivotIndexFindMiddleIndex(int[] nums) {
        int rightSum = 0, leftSum = 0;
        for (int x : nums) rightSum += x;
        for (int i = 0; i < nums.length; i++) {
            rightSum -= nums[i];
            if (rightSum == leftSum) {
                return i;
            }
            leftSum += nums[i];
        }
        return -1;
    }
}
