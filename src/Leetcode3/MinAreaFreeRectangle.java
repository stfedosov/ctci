package Leetcode3;

import java.util.HashSet;
import java.util.List;

public class MinAreaFreeRectangle {

    public static void main(String[] args) {

    }

    public static double minAreaFreeRect(int[][] points) {
        var pointSet = new HashSet<List<Integer>>();
        for (int[] point : points) {
            pointSet.add(List.of(point[0], point[1]));
        }

        int minArea = Integer.MAX_VALUE;
        for (int i = 0; i < points.length; i++) {
            for (int j = i + 1; j < points.length; j++) {
                for (int k = 0; k < points.length; k++) {
                    if (k == i || k == j) continue;
                    if (perpendicular(points[k], points[i], points[j])
                            && pointSet.contains(diagonal(points[k], points[i], points[j]))) {
                        minArea = Math.min(minArea, getArea(points[k], points[i], points[j]));
                    }
                }
            }
        }
        return minArea < Integer.MAX_VALUE ? minArea : 0;
    }

    // Suppose p1 = (x1, y1), p2 = (x2, y2), p3 = (x3, y3), then the area of the parallelogram
    // determined by segments p1p2 and p1p3 is |p1p2 * p1p3|, i.e., |(x2 - x1)(y3 - y1) - (x3 - x1)(y2 - y1)|.
    private static int getArea(int[] p1, int[] p2, int[] p3) {
        return Math.abs((p2[0] - p1[0]) * (p3[1] - p1[1]) - (p3[0] - p1[0]) * (p2[1] - p1[1]));
    }

    // p4 = p3 + p2 - p1
    private static List<Integer> diagonal(int[] p1, int[] p2, int[] p3) {
        return List.of(p2[0] + p3[0] - p1[0], p2[1] + p3[1] - p1[1]);
    }

    // Check whether p1p2 is perpendicular to p1p3
    // Suppose v1 = (x1, y1), v2 = (x2, y2), then v1 is perpendicular to v2 if and only if v1*v2 = 0, i.e., x1*x2 + y1*y2 = 0.
    private static boolean perpendicular(int[] p1, int[] p2, int[] p3) {
        return ((p2[0] - p1[0]) * (p3[0] - p1[0]) + (p2[1] - p1[1]) * (p3[1] - p1[1])) == 0;
    }
}
