package Leetcode3;

import java.util.LinkedList;
import java.util.Queue;

public class ShortestPath {

    public static void main(String[] args) {
        System.out.println(shortestPathLength(new int[][]{{1, 2, 3}, {0}, {0}, {0}})); // 4
    }

    // Time O(2^N*N^2)
    // The total number of possible states is O(2^N * N), because there are 2^N possibilities for mask,
    // each of which can be paired with one of N nodes.
    // At each state, we perform a for loop that loops through all the edges the given node has. That is also N;
    // ---------
    // Space O(2^N*N)
    // Depends on cache that can be the same size as the number of states when it is initialized
    public static int shortestPathLength(int[][] graph) {
        if (graph.length == 1) {
            return 0;
        }

        int n = graph.length;
        int endingMask = (1 << n) - 1; // visited all nodes, all ones
        boolean[][] visited = new boolean[n][endingMask];
        Queue<int[]> queue = new LinkedList<>();

        for (int i = 0; i < n; i++) {
            queue.add(new int[]{i, 1 << i}); // start from all nodes in the graph with the mask set to having only visited the given node
            visited[i][1 << i] = true; // set to visited only the given node
        }

        int steps = 0;
        while (!queue.isEmpty()) {
            Queue<int[]> nextQueue = new LinkedList<>();
            int size = queue.size();
            while (size-- > 0){
                int[] currentPair = queue.poll();
                int node = currentPair[0];
                int mask = currentPair[1];
                for (int neighbor : graph[node]) {
                    int nextMask = mask | (1 << neighbor);
                    if (nextMask == endingMask) {
                        return 1 + steps;
                    }

                    if (!visited[neighbor][nextMask]) {
                        visited[neighbor][nextMask] = true;
                        nextQueue.add(new int[]{neighbor, nextMask});
                    }
                }
            }
            steps++;
            queue = nextQueue;
        }

        return -1;
    }

}
