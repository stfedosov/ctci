package Leetcode3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FindAllRecipes {

    public static void main(String[] args) {
        System.out.println(findAllRecipes(new String[]{"sandwich", "bread"},
                List.of(List.of("bread", "meat"), List.of("yeast", "flour")),
                new String[]{"yeast", "flour", "meat"}));
        System.out.println(findAllRecipes(new String[]{"ju", "fzjnm", "x", "e", "zpmcz", "h", "q"},
                List.of(List.of("d"),
                        List.of("hveml", "f", "cpivl"),
                        List.of("cpivl", "zpmcz", "h", "e", "fzjnm", "ju"),
                        List.of("cpivl", "hveml", "zpmcz", "ju", "h"),
                        List.of("h", "fzjnm", "e", "q", "x"),
                        List.of("d", "hveml", "cpivl", "q", "zpmcz", "ju", "e", "x"),
                        List.of("f", "hveml", "cpivl")),
                new String[]{"f", "hveml", "cpivl", "d"}));
    }

    private static final int NOT_VISITED = 0;
    private static final int VISITING = 1;
    private static final int VISITED = 2;

    // time complexity: O(V+E)
    public static List<String> findAllRecipes(String[] recipes, List<List<String>> ingredients, String[] supplies) {
        Map<String, Integer> status = new HashMap<>();
        Map<String, List<String>> prereqs = new HashMap<>();

        for (int i = 0; i < recipes.length; ++i) {
            status.put(recipes[i], NOT_VISITED);
            prereqs.put(recipes[i], ingredients.get(i));
        }

        for (String s : supplies) {
            status.put(s, VISITED);
        }

        List<String> answer = new ArrayList<>();
        for (String s : recipes) {
            if (dfs(s, prereqs, status)) {
                answer.add(s);
            }
        }

        return answer;
    }

    public static boolean dfs(String s, Map<String, List<String>> prereqs, Map<String, Integer> status) {
        if (!status.containsKey(s)) {
            return false;
        }

        if (status.get(s) == VISITING) {
            return false;
        }

        if (status.get(s) == VISITED) {
            return true;
        }

        status.put(s, VISITING);
        for (String p : prereqs.get(s)) {
            if (!dfs(p, prereqs, status)) {
                return false;
            }
        }
        status.put(s, VISITED);
        return true;
    }

    public static List<String> findAllRecipesAlternative(String[] recipes, List<List<String>> ingredients, String[] supplies) {
        Set<String> supplements = new HashSet<>(List.of(supplies));
        List<String> answer = new ArrayList<>();

        for (int i = 0; i < ingredients.size(); i++) {
            OUTER:
            for (int j = 0; j < ingredients.size(); j++) {
                if (answer.contains(recipes[j])) continue;//recipe has already been made
                for (String ingredient : ingredients.get(j)) {
                    if (!supplements.contains(ingredient))
                        continue OUTER; //As all ingredients are not present in supplies
                }
                answer.add(recipes[j]); // add it to the answer
                supplements.add(recipes[j]); //add it to the supplies list so that another recipe can use this recipe
            }

        }
        return answer;
    }

}
