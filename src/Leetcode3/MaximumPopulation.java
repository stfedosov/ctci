package Leetcode3;

public class MaximumPopulation {

    /*

    You are given a 2D integer array logs where each logs[i] = [birthi, deathi] indicates
    the birth and death years of the ith person.

    The population of some year x is the number of people alive during that year.
    The ith person is counted in year x's population if x is in the inclusive range [birthi, deathi - 1].
    Note that the person is not counted in the year that they die.

    Return the earliest year with the maximum population.
     */

    public static void main(String[] args) {
        assert maximumPopulation(new int[][]{{1950, 1961}, {1960, 1971}, {1970, 1981}}) == 1960;
        assert maximumPopulation(new int[][]{{1993, 1999}, {2000, 2010}}) == 1993;
    }

    public static int maximumPopulation(int[][] logs) {
        int[] year = new int[101];

        for (int[] log : logs) {
            year[log[0] - 1950]++;
            year[log[1] - 1950]--;
        }

        int maxNum = year[0], maxYear = 1950;

        for (int i = 1; i < year.length; i++) {
            year[i] += year[i - 1];  // Prefix Sum
            if (year[i] > maxNum) {
                maxNum = year[i];
                maxYear = i + 1950;
            }
        }
        return maxYear;
    }

}
