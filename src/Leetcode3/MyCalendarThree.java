package Leetcode3;

import java.util.TreeMap;

public class MyCalendarThree {

    public static void main(String[] args) {
        MyCalendarThree calendarThree = new MyCalendarThree();
        System.out.println(calendarThree.book(10, 20));
        System.out.println(calendarThree.book(50, 60));
        System.out.println(calendarThree.book(10, 40));
        System.out.println(calendarThree.book(5, 15));
        System.out.println(calendarThree.book(5, 10));
        System.out.println(calendarThree.book(25, 55));
    }

    private final TreeMap<Integer, Integer> timeline = new TreeMap<>();

    public MyCalendarThree() {
    }

    /*
    We can log the start & end of each event on the timeline,
    each start add a new ongoing event at that time,
    each end terminate an ongoing event.
    Then we can scan the timeline to figure out the maximum number of ongoing event at any time.
    ---------------------------
    Time complexity:
    ---------------------------
    We have N calls to book, each of which requires 2 calls to put
    and a full iteration of the tree.
    So the time complexity is O(N * (2logN + N)) ~ N^2.
     */
    public int book(int start, int end) {
        timeline.put(start, timeline.getOrDefault(start, 0) + 1);
        timeline.put(end, timeline.getOrDefault(end, 0) - 1);
        int ongoingEvents = 0, maxCount = 0;
        for (int value : timeline.values()) {
            ongoingEvents += value;
            maxCount = Math.max(maxCount, ongoingEvents);
        }
        return maxCount;
    }
}
