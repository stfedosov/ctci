package Leetcode3;

import Leetcode.Interval;

import java.util.ArrayList;
import java.util.List;

public class MergeTwoInterval {

    /* merge two sorted intervals lists */
    public static void main(String[] args) {
        System.out.println(mergeTwoInterval(List.of(new Interval(1, 2), new Interval(3, 4)),
                List.of(new Interval(2, 3), new Interval(5, 6))));
    }

    /**
     * @param list1: one of the given list
     * @param list2: another list
     * @return the new sorted list of interval
     */
    public static List<Interval> mergeTwoInterval(List<Interval> list1, List<Interval> list2) {
        List<Interval> ans = new ArrayList<>();
        int i = 0, j = 0;

        Interval prev = null;

        while (i < list1.size() && j < list2.size()) {
            Interval curr, int1 = list1.get(i), int2 = list2.get(j);
            if (int1.start < int2.start) {
                curr = int1;
                i++;
            } else {
                curr = int2;
                j++;
            }
            prev = merge(ans, prev, curr);
        }

        while (i < list1.size()) {
            prev = merge(ans, prev, list1.get(i++));
        }

        while (j < list2.size()) {
            prev = merge(ans, prev, list2.get(j++));
        }

        if (prev != null) {
            ans.add(prev);
        }

        return ans;
    }

    private static Interval merge(List<Interval> result, Interval prev, Interval curr) {
        if (prev == null) {
            return curr;
        }
        if (prev.end >= curr.start) {
            prev.end = Math.max(prev.end, curr.end);
        } else {
            result.add(prev);
            prev = curr;
        }
        return prev;
    }
}
