package Leetcode3;

import java.util.Deque;
import java.util.LinkedList;

public class ShortestSubarray {

    // Shortest Subarray with Sum at Least K
    public static int shortestSubarray(int[] nums, int K) {
        long[] cumulative_array = new long[nums.length + 1];
        for (int i = 0; i < nums.length; i++) cumulative_array[i + 1] = cumulative_array[i] + (long) nums[i];

        // Find smallest i, j with cumulative_array[i] - cumulative_array[j] >= K
        int answer =  nums.length + 1; // nums.length + 1 is impossible
        Deque<Integer> deque = new LinkedList<>();
        for (int i = 0; i < cumulative_array.length; i++) {
            // Keep the deque increasing as it can help us make the subarray length shorter and sum bigger
            while (!deque.isEmpty() && cumulative_array[i] <= cumulative_array[deque.getLast()])
                deque.pollLast();
            // Check for elements where the sum from element 'getLast() - 1' to element 'i' is at least K.
            // These are potential solutions, so update the answer and remove from the front to test for a better answer.
            while (!deque.isEmpty() && cumulative_array[i] - cumulative_array[deque.getFirst()] >= K)
                answer = Math.min(answer, i - deque.pollFirst());

            deque.addLast(i);
        }
        return answer != nums.length + 1 ? answer : -1;
    }

}
