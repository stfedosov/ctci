package Leetcode3;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class IsPossibleToDivide {

    public static void main(String[] args) {
        System.out.println(isPossibleDivide(new int[]{1, 2, 3, 4, 5, 6}, 3)); // true
        System.out.println(isPossibleDivide(new int[]{1, 2, 3, 4}, 3)); // false
        System.out.println(isPossibleDivide(new int[]{3, 2, 1, 2, 3, 4, 3, 4, 5, 9, 10, 11}, 3)); // true
        System.out.println(isPossibleDivide(new int[]{1, 2, 3, 3, 4, 4, 5, 6}, 4)); // true
    }

    // Time: O(nlogn), space: O(n), where n = nums.length
    public static boolean isPossibleDivide(int[] nums, int k) {
        if (nums.length % k != 0) return false;
        TreeMap<Integer, Integer> map = new TreeMap<>();
        for (int x : nums) {
            map.put(x, map.getOrDefault(x, 0) + 1);
        }
        while (!map.isEmpty()) {
            int firstKey = map.firstKey();
            for (int i = firstKey; i < firstKey + k; i++) {
                if (!map.containsKey(i)) { // should have next number sequentially bigger by one
                    return false;
                } else {
                    int value = map.get(i);
                    if (value - 1 == 0) {
                        map.remove(i);
                    } else {
                        map.put(i, value - 1);
                    }
                }
            }
        }
        return true;
    }

    // O(n)
    public boolean isPossibleDivideLinearTime(int[] nums, int groupSize) {
        if (nums.length % groupSize != 0) {
            return false;
        }

        Map<Integer, Integer> cardCount = new HashMap<>();
        for (int card : nums) {
            cardCount.put(card, cardCount.getOrDefault(card, 0) + 1);
        }

        for (int card : nums) {
            int startCard = card;
            // Find the start of the potential straight sequence
            while (cardCount.getOrDefault(startCard - 1, 0) > 0) {
                startCard--;
            }

            // Process the sequence starting from startCard
            while (startCard <= card) {
                while (cardCount.getOrDefault(startCard, 0) > 0) {
                    // Check if we can form a consecutive sequence
                    // of groupSize cards
                    for (int nextCard = startCard; nextCard < startCard + groupSize; nextCard++) {
                        if (cardCount.getOrDefault(nextCard, 0) == 0) {
                            return false;
                        }
                        cardCount.put(nextCard, cardCount.get(nextCard) - 1);
                    }
                }
                startCard++;
            }
        }

        return true;
    }

}
