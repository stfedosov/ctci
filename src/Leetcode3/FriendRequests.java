package Leetcode3;

import java.util.Arrays;

public class FriendRequests {

    public static void main(String[] args) {
        System.out.println(
                Arrays.toString(friendRequests(3, new int[][]{{0, 1}}, new int[][]{{0, 2}, {2, 1}})));
        // true, false
    }

    // Find whether people from requests array can be friends given the restrictions array
    //
    // n indicating the number of people in a network
    //
    // 2D integer array restrictions, where restrictions[i] = [xi, yi]
    // means that person xi and person yi cannot become friends,
    // either directly or indirectly through other people.
    public static boolean[] friendRequests(int n, int[][] restrictions, int[][] requests) {
        int[] parent = new int[n];
        boolean[] res = new boolean[requests.length];
        for (int i = 0; i < parent.length; i++) {
            parent[i] = i;
        }
        for (int i = 0; i < requests.length; i++) {
            // finding the parents of the first person and second person of ith request
            int parent1 = find(parent, requests[i][0]);
            int parent2 = find(parent, requests[i][1]);
            // if they have same parents i.e. mutual friends they can be friends
            if (parent1 == parent2) {
                res[i] = true;
                continue;
            }
            boolean flag = true;
            // iterating through the restrictions array to find whether
            // the parents of first ans second person have a conflict
            for (int[] restriction : restrictions) {
                int restrict1 = find(parent, restriction[0]);
                int restrict2 = find(parent, restriction[1]);
                if (restrict1 == parent1 && restrict2 == parent2
                        || restrict1 == parent2 && restrict2 == parent1) {
                    flag = false;
                    res[i] = false;
                    break;
                }
            }
            if (flag) {
                res[i] = true;
                parent[parent1] = parent2;
            }
        }
        return res;
    }

    private static int find(int[] parent, int idx) {
        if (parent[idx] == idx) return idx;
        return parent[idx] = find(parent, parent[idx]);
    }

}
