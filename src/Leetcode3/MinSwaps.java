package Leetcode3;

import java.util.Stack;

public class MinSwaps {

    /*
    You are given a 0-indexed string s of even length n.
    The string consists of exactly n / 2 opening brackets '[' and n / 2 closing brackets ']'.

    A string is called balanced if and only if:
    - It is the empty string
    - It can be written as AB, where both A and B are balanced strings
    - It can be written as [C], where C is a balanced string

    You may swap the brackets at any two indices any number of times.

    Return the minimum number of swaps to make s balanced.
     */
    public static void main(String[] args) {
        System.out.println(minSwaps1("][][")); // 1
        System.out.println(minSwaps2("][][")); // 1
        System.out.println(minSwaps1("]]][[[")); // 2
        System.out.println(minSwaps2("]]][[[")); // 2
        System.out.println(minSwaps1("[]")); // 0
        System.out.println(minSwaps2("[]")); // 0
    }

    public static int minSwaps1(String s) {
        int open = 0;
        for (char c : s.toCharArray()) {
            if (c == '[') {
                open++;
            } else if (open > 0) {
                open--;
            }
        }
        return (open + 1) / 2;
    }

    public static int minSwaps2(String s) {
        Stack<Character> stack = new Stack<>();
        for (char c : s.toCharArray()) {
            switch (c) {
                case '[':
                    stack.push(c);
                    break;
                case ']':
                    if (!stack.isEmpty() && stack.peek() == '[') stack.pop();
                    break;

            }
        }
        return (stack.size() + 1) / 2;
    }

}
