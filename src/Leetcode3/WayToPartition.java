package Leetcode3;

import java.util.HashMap;
import java.util.Map;

public class WayToPartition {

    public static void main(String[] args) {
        System.out.println(waysToPartition(new int[]{1, 2, 2, 1}, -1));
    }

    public static int waysToPartition(int[] nums, int k) {
        Map<Long, Long> left = new HashMap<>(), right = new HashMap<>();

        long totalSum = 0;
        for (int val : nums) totalSum += val;

        long leftSum = totalSum;
        long rightSum = 0;
        for (int i = nums.length - 1; i > 0; i--) {
            rightSum += nums[i];
            leftSum -= nums[i];
            long diff = leftSum - rightSum;
            right.put(diff, right.getOrDefault(diff, 0L) + 1);
        }

        rightSum = totalSum;
        leftSum = 0;
        int res = (int) (long) right.getOrDefault(0L, 0L);
        for (int num : nums) {
            // What happens if we change num[i] to k?
            // All differences to the left of i will increase by nums[i] - k.
            // All differences starting from i will decrease by nums[i] - k.
            // ...
            // Here we are finding the number of pivot indexes when nums[i] is changed to k.
            res = Math.max(res, (int) (left.getOrDefault((long) k - num, 0L) + right.getOrDefault((long) num - k, 0L)));

            leftSum += num;
            rightSum -= num;
            long diff = leftSum - rightSum;

            // transfer the current element from right to left
            left.put(diff, left.getOrDefault(diff, 0L) + 1);
            right.put(diff, right.getOrDefault(diff, 0L) - 1);
        }

        return res;
    }

    // ------====== Brute Force, slow ======------

    public static int waysToPartition2(int[] nums, int k) {
        int max = helper(nums);
        for (int i = 0; i < nums.length; i++) {
            int tmp = nums[i];
            nums[i] = k;
            max = Math.max(max, helper(nums));
            nums[i] = tmp;
        }
        return max;
    }

    private static int helper(int[] nums) {
        int[] cumulative_sum = new int[nums.length + 1];
        for (int i = 1; i <= nums.length; i++) {
            cumulative_sum[i] = cumulative_sum[i - 1] + nums[i - 1];
        }
        int i = 1, count = 0;
        while (i < nums.length) {
            if (cumulative_sum[i] - cumulative_sum[0] ==
                    cumulative_sum[cumulative_sum.length - 1] - cumulative_sum[i]) {
                count++;
            }
            i++;
        }
        return count;
    }

}
