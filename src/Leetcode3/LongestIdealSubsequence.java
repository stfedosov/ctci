package Leetcode3;

import java.util.Arrays;

public class LongestIdealSubsequence {

    public static void main(String[] args) {
        // Given a string s consisting of lowercase letters and an integer k.
        // We call a string t ideal if the following conditions are satisfied:
        // - t is a subsequence of the string s.
        // - The absolute difference in the alphabet order of every two adjacent letters in t is less
        //   than or equal to k.
        // Return the length of the longest ideal string.

        System.out.println(longestIdealString("acfgbd", 2)); // The longest ideal string is "acbd"
        System.out.println(longestIdealString("abcd", 3)); // The longest ideal string is "abcd"
    }

    private static int solveMemo(String s, int idx, int k, int[][] cache, char prev) {
        // Base Case
        if (idx >= s.length()) {
            return 0;
        }

        // step-2 => if already calculated just return it
        if (cache[idx][prev] != -1) {
            return cache[idx][prev];
        }

        // step-3 => if not calculated yet just calculate it and return
        int take = 0;
        // Case of take it
        if (Math.abs(prev - s.charAt(idx)) <= k || prev == '#') {
            take = 1 + solveMemo(s, idx + 1, k, cache, s.charAt(idx));
        }
        return cache[idx][prev] = Math.max(take, solveMemo(s, idx + 1, k, cache, prev));
    }

    public static int longestIdealString(String s, int k) {
        // DP + Memoization
        int[][] dp = new int[s.length() + 1][130];
        for (int[] d : dp) {
            Arrays.fill(d, -1);
        }
        return solveMemo(s, 0, k, dp, '#');
    }

}
