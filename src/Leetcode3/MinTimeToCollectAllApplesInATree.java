package Leetcode3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MinTimeToCollectAllApplesInATree {

    /*
     Given an undirected tree consisting of n vertices numbered from 0 to n-1,
     which has some apples in their vertices.
     You spend 1 second to walk over one edge of the tree.
     Return the minimum time in seconds you have to spend to collect all apples in the tree,
     starting at vertex 0 and coming back to this vertex.
     */

    public static void main(String[] args) {
        System.out.println(minTime(
                7,
                new int[][]{{0, 1}, {0, 2}, {1, 4}, {1, 5}, {2, 3}, {2, 6}},
                Arrays.asList(false, false, true, false, true, true, false))); // 8
    }

    public static int minTime(int n, int[][] edges, List<Boolean> hasApple) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        for (int[] edge : edges) {
            map.computeIfAbsent(edge[0], x -> new ArrayList<>()).add(edge[1]);
            map.computeIfAbsent(edge[1], x -> new ArrayList<>()).add(edge[0]);
        }
        return dfs(map, 0, new boolean[edges.length + 1], hasApple);
    }

    private static int dfs(Map<Integer, List<Integer>> map,
                           int node,
                           boolean[] seen,
                           List<Boolean> hasApple) {
        int res = 0;
        seen[node] = true;
        for (int neighbour : map.getOrDefault(node, Collections.emptyList())) {
            if (!seen[neighbour]) {
                res += dfs(map, neighbour, seen, hasApple);
            }
        }
        if ((res > 0 || hasApple.get(node)) && node > 0) res += 2;
        return res;
    }


}
