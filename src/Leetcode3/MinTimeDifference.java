package Leetcode3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MinTimeDifference {

    public static void main(String[] args) {
        System.out.println(findMinDifference(List.of("23:59", "00:00"))); // 1
        System.out.println(findMinDifference(List.of("00:00", "23:59", "00:00"))); // 0
        System.out.println(findMinDifference(List.of("12:12","00:13"))); // 719
    }

    public static int findMinDifference(List<String> timePoints) {
        List<Integer> list = new ArrayList<>();
        for (String time : timePoints) {
            list.add(convert(time));
        }
        Collections.sort(list);
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < list.size() - 1; i++) {
            min = Math.min(min, Math.abs(list.get(i + 1) - list.get(i)));
        }
        int corner = list.get(0) + (1440 - list.get(list.size() - 1)); // 24 hours * 60 minutes = 1440 minutes
        return Math.min(min, corner);
    }

    private static int convert(String time) {
        String[] split = time.split(":");
        return Integer.parseInt(split[0]) * 60 + Integer.parseInt(split[1]);
    }

}
