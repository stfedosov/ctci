package Leetcode3;

import java.util.HashMap;
import java.util.Map;

public class CountPairs {

    // Given two arrays of integers a and b of the same length, find the number of pairs (i, j)
    // such that i <= j and a[i] - b[j] = a[j] - b[i].
    public static void main(String[] args) {
        int[] a = {1, 4, 20, 3, 10, 5};
        int[] b = {9, 6, 1, 7, 11, 6};
        System.out.println(countPairs(a, b)); // 4

        // (1, 5): a[1] – b[5] = 1 – 1 = 0, a[5] – b[1] = 1 – 1 = 0
        // (2, 4): a[2] – b[4] = 2 – 2 = 0, a[4] – b[2] = 2 – 2 = 0
        a = new int[]{1, 2, 3, 2, 1};
        b = new int[]{1, 2, 3, 2, 1};
        System.out.println(countPairs(a, b)); // 2
    }

    // The idea is to transform the given expression a[i] – b[j] = a[j] – b[i] into
    // the form a[i] + b[i] = a[j] + b[j] and then calculate pairs satisfying the condition.
    private static int countPairs(int[] a, int[] b) {
        int[] c = new int[a.length];

        for (int i = 0; i < a.length; i++) {
            c[i] = a[i] + b[i];
        }

        Map<Integer, Integer> freqCount = new HashMap<>();

        for (int i = 0; i < a.length; i++) {
            freqCount.put(c[i], freqCount.getOrDefault(c[i], 0) + 1);
        }

        int numOfPairs = 0;

        // if we have 2 pairs with the same sum, it means that we can build 1 equality a[i] – b[j] = a[j] – b[i]
        // in other words, we can apply the summing formula to sum up (n - 1) elements:
        // 1 + 2 + 3 + ... + n - 1 = n * (n - 1) / 2
        for (Map.Entry<Integer, Integer> entry : freqCount.entrySet()) {
            int val = entry.getValue();
            numOfPairs += val * (val - 1) / 2;
        }

        return numOfPairs;
    }

}
