package Leetcode3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FindRLEArray {

    /*
    You are given two run-length encoded arrays encoded1 and encoded2
    representing full arrays nums1 and nums2 respectively.
    Both nums1 and nums2 have the same length.
    Each encoded1[i] = [vali, freqi] describes the ith segment of nums1,
    and each encoded2[j] = [valj, freqj] describes the jth segment of nums2.

    Return the product of encoded1 and encoded2.
     */
    public static void main(String[] args) {
        System.out.println(findRLEArray(new int[][]{{1, 3}, {2, 3}},
                new int[][]{{6, 3}, {3, 3}})); // [6,6,6,3,3,3] -> [[6,6]]
        System.out.println(findRLEArray(new int[][]{{1, 3}, {2, 1}, {3, 2}},
                new int[][]{{2, 3}, {3, 3}})); // [2,2,2,3,3,3] -> [[2,3],[6,1],[9,2]]
    }

    public static List<List<Integer>> findRLEArray(int[][] encoded1, int[][] encoded2) {
        int[] last1 = null, last2 = null;
        int i = 0, j = 0, n = encoded1.length, m = encoded2.length;
        List<List<Integer>> res = new ArrayList<>();
        while (i < n || j < m) {
            int[] next1 = last1 == null ? i < n ? encoded1[i] : null : last1,
                    next2 = last2 == null ? j < m ? encoded2[j] : null : last2;
            if (next1 == null) {
                addToTheEnd(res, new int[]{next2[0], next2[1]});
                j++;
            } else if (next2 == null) {
                addToTheEnd(res, new int[]{next1[0], next1[1]});
                i++;
            } else if (next1[1] == next2[1]) {
                addToTheEnd(res, new int[]{next1[0] * next2[0], next1[1]});
                last1 = null;
                last2 = null;
                i++;
                j++;
            } else {
                int min = Math.min(next1[1], next2[1]);
                addToTheEnd(res, new int[]{next1[0] * next2[0], min});
                if (next1[1] > min) {
                    last1 = new int[]{next1[0], next1[1] - min};
                    last2 = null;
                    j++;
                } else {
                    last2 = new int[]{next2[0], next2[1] - min};
                    last1 = null;
                    i++;
                }
            }
        }
        return res;
    }

    private static void addToTheEnd(List<List<Integer>> res, int[] tmp) {
        int lastIdx = res.size() - 1;
        List<Integer> prev = res.isEmpty() ? null : res.get(lastIdx);
        if (prev != null && prev.get(0) == tmp[0]) {
            res.set(lastIdx, Arrays.asList(tmp[0], prev.get(1) + tmp[1]));
        } else {
            res.add(Arrays.asList(tmp[0], tmp[1]));
        }
    }

}
