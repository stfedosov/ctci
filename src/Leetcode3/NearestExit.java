package Leetcode3;

import java.util.LinkedList;
import java.util.Queue;

public class NearestExit {

    private static int[][] directions = new int[][]{{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

    public static int nearestExit(char[][] maze, int[] entrance) {
        int steps = 0;
        Queue<int[]> q = new LinkedList<>();
        q.offer(entrance);
        boolean[][] visited = new boolean[maze.length][maze[0].length];
        while (!q.isEmpty()) {
            int size = q.size();
            while (size-- > 0) {
                int[] next = q.poll();
                if ((next[0] != entrance[0] || next[1] != entrance[1])
                        && (next[0] == 0
                        || next[0] == maze.length - 1
                        || next[1] == maze[0].length - 1
                        || next[1] == 0)) {
                    return steps;
                }
                for (int[] dir : directions) {
                    int newX = next[0] + dir[0], newY = next[1] + dir[1];
                    if (newX < maze.length
                            && newY < maze[0].length
                            && newX >= 0
                            && newY >= 0
                            && !visited[newX][newY]
                            && maze[newX][newY] == '.') {
                        visited[newX][newY] = true;
                        q.offer(new int[]{newX, newY});
                    }
                }

            }
            steps++;
        }
        return -1;
    }

}
