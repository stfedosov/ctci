package Leetcode3;

import java.util.Arrays;

public class KRadiusSubarrayAverages {

    /*
    The k-radius average for a subarray of nums centered at some index i with the radius k is the average
    of all elements in nums between the indices i - k and i + k (inclusive).
    If there are less than k elements before or after the index i, then the k-radius average is -1.

    Build and return an array avgs of length n where avgs[i]
    is the k-radius average for the subarray centered at index i.
     */

    public static int[] getAverages(int[] nums, int k) {
        int[] res = new int[nums.length];
        Arrays.fill(res, -1);
        if (k >= nums.length) return res;
        long[] cumulativeSum = new long[nums.length];
        cumulativeSum[0] = nums[0];
        for (int i = 1; i < nums.length; i++) {
            cumulativeSum[i] = cumulativeSum[i - 1] + nums[i];
        }
        for (int i = k; i < nums.length - k; i++) {
            res[i] = (int) (cumulativeSum[i + k] - ((i - k - 1) < 0 ? 0 : cumulativeSum[i - k - 1])) / (2 * k + 1);
        }
        return res;
    }

}
