package Leetcode3;

import Leetcode.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class ZigZagLevelOrder {

    public static void main(String[] args) {

    }

    public static List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        travel(root, result, 0);
        return result;
    }

    private static void travel(TreeNode curr, List<List<Integer>> result, int level) {
        if (curr == null) return;

        if (result.size() <= level) {
            result.add(new ArrayList<>());
        }

        List<Integer> tmpResult = result.get(level);
        if (level % 2 == 0) tmpResult.add(curr.val);
        else tmpResult.add(0, curr.val);

        travel(curr.left, result, level + 1);
        travel(curr.right, result, level + 1);
    }

}
