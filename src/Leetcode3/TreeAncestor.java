package Leetcode3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TreeAncestor {

    public static void main(String[] args) {
        TreeAncestor treeAncestor = new TreeAncestor(7, new int[]{-1, 0, 0, 1, 1, 2, 2});
        System.out.println(treeAncestor.getKthAncestor(3, 1));
        System.out.println(treeAncestor.getKthAncestor(5, 2));
        System.out.println(treeAncestor.getKthAncestor(6, 3));

        treeAncestor = new TreeAncestor(5, new int[]{-1,0,0,0,3});
        System.out.println(treeAncestor.getKthAncestor(1, 5));
        System.out.println(treeAncestor.getKthAncestor(3, 2));
        System.out.println(treeAncestor.getKthAncestor(0, 1));


    }

    class Node {
        int val;
        List<Node> parent = new ArrayList<>();

        public Node(int val) {
            this.val = val;
        }
    }

    private Map<Integer, Node> map = new HashMap<>();

    public TreeAncestor(int n, int[] parent) {
        for (int i = 0; i < n; i++) {
            map.put(i, new Node(i));
        }
        int i = 0;
        for (Node nn : map.values()) {
            Node e = map.get(parent[i++]);
            nn.parent.add(e);
            if (e != null && e.parent != null) {
                nn.parent.addAll(e.parent);
            }
        }
    }

    public int getKthAncestor(int node, int k) {
        List<Node> n = map.get(node).parent;
        Node nn = k < n.size() ? n.get(k - 1) : null;
        return nn != null ? nn.val : -1;
    }

}
