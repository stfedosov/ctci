package Leetcode3;

import Leetcode.TreeNode;

public class SumNumbers {

    /*
    You are given the root of a binary tree containing digits from 0 to 9 only.

    Each root-to-leaf path in the tree represents a number.

    For example, the root-to-leaf path 1 -> 2 -> 3 represents the number 123.
    Return the total sum of all root-to-leaf numbers.
     */
    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        //      1
        //     / \
        //    2   3

        // sum of 12 + 13 = 25

        System.out.println(sumNumbers(root));
    }

    public static int sumNumbers(TreeNode root) {
        return sumNumbers(root, 0);
    }

    private static int sumNumbers(TreeNode root, int sum) {
        if (root == null) return 0;

        sum = sum * 10 + root.val;
        if (root.left == null && root.right == null)
            return sum;

        return sumNumbers(root.left, sum) + sumNumbers(root.right, sum);
    }

}
