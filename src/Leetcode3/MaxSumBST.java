package Leetcode3;

import Leetcode.TreeNode;

public class MaxSumBST {

    /*
    Given a binary tree root,
    return the maximum sum of all keys of any sub-tree which is also a Binary Search Tree (BST).
     */
    public static void main(String[] args) {

    }

    int max;

    public int maxSumBST(TreeNode root) {
        max = 0;
        findMaxSum(root);
        return max;
    }

    //int[]{isBST(0/1), largest, smallest, sum}
    public int[] findMaxSum(TreeNode root) {
        if (root == null) {
            return new int[]{1, Integer.MIN_VALUE, Integer.MAX_VALUE, 0};
        }
        int[] left = findMaxSum(root.left);
        int[] right = findMaxSum(root.right);
        boolean isBST = left[0] == 1 && right[0] == 1 && root.val > left[1] && root.val < right[2];
        int sum = root.val + left[3] + right[3];
        if (isBST) max = Math.max(max, sum);
        return new int[]{isBST ? 1 : 0, Math.max(root.val, right[1]), Math.min(root.val, left[2]), sum};
    }

}
