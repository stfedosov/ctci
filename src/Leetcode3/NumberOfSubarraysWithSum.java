package Leetcode3;

import java.util.HashMap;
import java.util.Map;

public class NumberOfSubarraysWithSum {

    /*
    Given a binary array nums and an integer goal,
    return the number of non-empty subarrays with a sum goal.
     */
    public static void main(String[] args) {
        System.out.println(numSubarraysWithSum(new int[]{1, 0, 1, 0, 1}, 2)); // 4
        System.out.println(numSubarraysWithSumSlidingWindow(new int[]{1, 0, 1, 0, 1}, 2)); // 4
    }

    public static int numSubarraysWithSum(int[] nums, int goal) {
        int count = 0, sum = 0;
        Map<Integer, Integer> map = new HashMap<>();
        for (int x : nums) {
            sum += x;
            if (map.containsKey(sum - goal)) {
                count += map.get(sum - goal);
            }
            if (sum == goal) count++;
            map.put(sum, map.getOrDefault(sum, 0) + 1);
        }
        return count;
    }

    public static int numSubarraysWithSumSlidingWindow(int[] nums, int goal) {
        if (goal == 0) {
            return numSubarraysWithAtMostGoal(nums, 0);
        }
        return numSubarraysWithAtMostGoal(nums, goal) - numSubarraysWithAtMostGoal(nums, goal - 1);
    }

    private static int numSubarraysWithAtMostGoal(int[] nums, int goal) {
        int sum = 0, start = 0, end = 0, count = 0;
        while (end < nums.length) {
            sum += nums[end++];
            while (sum > goal) {
                sum -= nums[start++];
            }
            count += (end - start);
        }
        return count;
    }

}
