package Leetcode3;

import java.util.HashSet;
import java.util.Set;

public class CountTriplets {

    /*
    A square triple (a,b,c) is a triple where a, b, and c are integers and a^2 + b^2 = c^2.

    Given an integer n, return the number of square triples such that 1 <= a, b, c <= n.
     */
    public static void main(String[] args) {
        // 1,2,3,4,5
        // 1^2 + 2^2 = 5
        // 2^2 + 3^2 = 13
        // 3^2 + 4^2 = 25
        System.out.println(countTriplesConstantSpace(5));
    }

    public static int countTriplesConstantSpace(int n) {
        int count = 0;
        for (int i = 1; i <= n; i++) {
            for (int j = i + 1; j <= n; j++) {
                int sq = i * i + j * j;
                int c = (int) Math.sqrt(sq);
                if (c * c == sq && c <= n)
                    // The square triples are (3,4,5) and (4,3,5) for case of n = 5, thus we add 2
                    count += 2;
            }
        }
        return count;
    }

    public static int countTriples(int n) {
        int count = 0;
        Set<Integer> s = new HashSet<>();
        for (int i = 1; i <= n; i++) {
            s.add(i * i);
        }

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                if (s.contains(i * i + j * j))
                    count++;
            }
        }
        return count;
    }

}
