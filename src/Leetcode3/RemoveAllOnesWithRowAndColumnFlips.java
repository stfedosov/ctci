package Leetcode3;

public class RemoveAllOnesWithRowAndColumnFlips {

    // Read first ROW and if you find any value to be 1, then flip the whole COLUMN
    // Read first COLUMN and if you find any value to be 1, then flip the whole ROW
    // ----
    // NOW, read all the grid[i][j] and if you find any 1,
    // that means we cannot get complete 0 grid because if we flip now,
    // then some other 0 will flip to 1 and the cycle will continue
    public static void main(String[] args) {
        System.out.println(removeOnes(new int[][]{{0, 1, 0}, {1, 0, 1}, {0, 1, 0}}));
        System.out.println(removeOnes(new int[][]{{1, 1, 0}, {0, 0, 0}, {0, 0, 0}}));
    }

    public static boolean removeOnes(int[][] grid) {
        for (int i = 0; i < grid.length; i++) {
            if (grid[i][0] == 1) {
                flip(grid, i, true);
            }
        }
        for (int i = 0; i < grid[0].length; i++) {
            if (grid[0][i] == 1) {
                flip(grid, i, false);
            }
        }
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == 1) return false;
            }
        }
        return true;
    }

    private static void flip(int[][] grid, int idx, boolean isRow) {
        if (isRow) {
            for (int i = 0; i < grid[idx].length; i++) {
                grid[idx][i] = 1 - grid[idx][i];
            }
        } else {
            for (int i = 0; i < grid.length; i++) {
                grid[i][idx] = 1 - grid[i][idx];
            }
        }
    }

}
