package Leetcode3;

import java.util.ArrayList;
import java.util.List;

public class IpToCIDR {

    public static void main(String[] args) {
        System.out.println(ipToCIDR("255.0.0.7", 10)); // ["255.0.0.7/32","255.0.0.8/29","255.0.0.16/32"]
    }

    public static List<String> ipToCIDR(String ip, int ipRange) {
        long ipAsLong = ipToLong(ip);
        List<String> result = new ArrayList<>();

        while (ipRange > 0) {
            long count = Long.lowestOneBit(ipAsLong);
            // this count value here means if we don't change the current start ip, how many
            // more ips we can represent with CIDR
            while (count > ipRange) {
                count >>= 1;
            }
            ipRange -= count;
            result.add(buildCIDR(ipAsLong, (int) count));
            ipAsLong += count;
        }

        return result;
    }

    private static String buildCIDR(long ipLong, int count) {
        long[] ip = longToIP(ipLong);
        int commonPrefix = 33;

        // this while loop to know how many digits of count is in binary
        // for example, 00001000 here the len will be 4.
        while (count > 0) {
            commonPrefix--;
            count >>= 1;
        }

        return ip[0] + "." + ip[1] + "." + ip[2] + "." + ip[3] + "/" + commonPrefix;
    }

    private static long ipToLong(String ip) {
        String[] splits = ip.split("\\.");
        long ipLong = 0;

        for (int i = 0; i < 4; i++) {
            ipLong <<= 8;
            ipLong += Integer.parseInt(splits[i]);
        }

        return ipLong;
    }

    private static long[] longToIP(long ip) {
        long[] arr = new long[4];
        for (int i = 3; i >= 0; i--) {
            arr[i] = ip & 255;
            ip >>= 8;
        }

        return arr;
    }

}
