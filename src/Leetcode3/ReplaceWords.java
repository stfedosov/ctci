package Leetcode3;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ReplaceWords {

    public static void main(String[] args) {
        System.out.println(replaceWords(Arrays.asList("a", "aa", "aaa", "aaaa"),
                "a aa a aaaa aaa aaa aaa aaaaaa bbb baba ababa"));
    }

    // O(total_words_in_sentence * avg_length_of_word) ~ O(N)
    public static String replaceWords(List<String> dictionary, String sentence) {
        Set<String> dict = new HashSet<>();
        for (String word : dictionary) dict.add(word);
        return dfs(dict, sentence.split(" "), 0, new StringBuilder());
    }

    private static String dfs(Set<String> dict, String[] array, int idx, StringBuilder res) {
        if (idx >= array.length) return res.toString();
        String next = array[idx];
        for (int i = 1; i <= next.length(); i++) {
            String substring = next.substring(0, i);
            if (dict.contains(substring)) {
                return dfs(dict, array, idx + 1, res.length() == 0 ? new StringBuilder(substring) : res.append(" ").append(substring));
            }
        }
        return dfs(dict, array, idx + 1, res.length() == 0 ? new StringBuilder(next) : res.append(" ").append(next));
    }

    // --- Trie approach

    public static String replaceWords2(List<String> roots, String sentence) {
        TrieNode trie = new TrieNode();
        for (String root : roots) {
            TrieNode cur = trie;
            for (char letter : root.toCharArray()) {
                if (cur.children[letter - 'a'] == null)
                    cur.children[letter - 'a'] = new TrieNode();
                cur = cur.children[letter - 'a'];
            }
            cur.word = root;
        }

        StringBuilder ans = new StringBuilder();

        for (String word : sentence.split("\\s+")) {
            if (ans.length() > 0) ans.append(" ");

            TrieNode cur = trie;
            for (char letter : word.toCharArray()) {
                if (cur.children[letter - 'a'] == null || cur.word != null) break;
                cur = cur.children[letter - 'a'];
            }
            ans.append(cur.word != null ? cur.word : word);
        }
        return ans.toString();
    }

    private static class TrieNode {
        TrieNode[] children;
        String word;

        TrieNode() {
            children = new TrieNode[26];
        }
    }

}
