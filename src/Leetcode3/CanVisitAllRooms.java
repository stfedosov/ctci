package Leetcode3;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CanVisitAllRooms {

    public static void main(String[] args) {
        System.out.println(canVisitAllRooms(List.of(
                List.of(1), List.of(2), List.of(3), Collections.emptyList())));
        System.out.println(canVisitAllRooms(List.of(
                List.of(1, 3), List.of(3, 0, 1), List.of(2), List.of(0))));
    }

    public static boolean canVisitAllRooms(List<List<Integer>> rooms) {
        Set<Integer> visited = new HashSet<>();
        addKey(0, rooms, visited);
        return visited.size() == rooms.size();
    }

    private static void addKey(int room, List<List<Integer>> rooms, Set<Integer> visited) {
        visited.add(room);
        for (int key : rooms.get(room)) {
            if (!visited.contains(key)) {
                addKey(key, rooms, visited);
            }
        }
    }

}
