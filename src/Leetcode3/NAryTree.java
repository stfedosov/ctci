package Leetcode3;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Stack;

public class NAryTree {

    public static void main(String[] args) {
        Node node2 = new Node(2), node3 = new Node(3), node4 = new Node(4), node5 = new Node(5);
        Node node6 = new Node(6), node7 = new Node(7), node8 = new Node(8), node11 = new Node(11);
        Node node12 = new Node(12), node9 = new Node(9), node10 = new Node(10);
        node11.children = List.of(node12);
        node8.children = List.of(node9, node10, node11);
        node3.children = List.of(node6, node7);
        node4.children = List.of(node8);
        Node root = new Node(1, List.of(node2, node3, node4, node5));
        //         __ 1 __
        //       /   /  \  \
        //      /   |    |   \
        //     2    3    4   5
        //         / \   |
        //        6   7  8
        //             / | \
        //            9 10 11
        //                  |
        //                 12

        System.out.println(lcaNAryTreeSinglePass(root, Set.of(6, 7))); // 3
        System.out.println(lcaNAryTreeSinglePass(root, Set.of(6, 7, 8))); // 1
        System.out.println(lcaNAryTreeSinglePass(root, Set.of(3, 4, 8))); // 1
        System.out.println(lcaNAryTreeSinglePass(root, Set.of(6))); // 6
        System.out.println(lcaNAryTreeSinglePass(root, Set.of(8, 11))); // 8
        System.out.println(lcaNAryTreeSinglePass(root, Set.of(9, 10, 11))); // 8
        System.out.println(lcaNAryTreeSinglePass(root, Set.of(5, 11))); // 1
        System.out.println(lcaNAryTreeSinglePass(root, Set.of(2, 10))); // 1
        System.out.println(lcaNAryTreeSinglePass(root, Set.of(12, 10))); // 8
        System.out.println(lcaNAryTreeSinglePass(root, Set.of(12, 8))); // 8
    }

    public static Node lcaNAryTreeSinglePass(Node root, Set<Integer> nodes) {
        Pair pair = dfs(root, nodes);
        return pair.node;
    }

    private static Pair dfs(Node root, Set<Integer> nodes) {
        if (root == null) return new Pair(0, null);
        int count = 0;
        for (Node child : root.children) {
            Pair pair = dfs(child, nodes);
            if (pair.count == -1) return pair;
            count += pair.count;
        }
        if (nodes.contains(root.val)) {
            count++;
        }
        if (count == nodes.size()) return new Pair(-1, root);
        return new Pair(count, null);
    }

    // N-ary tree
    // Time complexity: O(Number of nodes), Space complexity: O(Height of the tree)
    public static Node lcaNAryTree(Node root, int p, int q) {
        Stack<Node> stack1 = findAPathToNode(root, p), stack2 = findAPathToNode(root, q);
        Node lca = null;
        while (!stack1.isEmpty() && !stack2.isEmpty()) {
            Node pop1 = stack1.pop(), pop2 = stack2.pop();
            if (pop1.val == pop2.val) {
                lca = pop1;
            } else {
                break;
            }
        }
        return lca;
    }

    private static Stack<Node> findAPathToNode(Node root, int toFind) {
        if (root == null) return null;
        if (root.val == toFind) {
            Stack<Node> list = new Stack<>();
            list.add(root);
            return list;
        }
        for (Node child : root.children) {
            Stack<Node> res = findAPathToNode(child, toFind);
            if (res != null) {
                res.push(root);
                return res;
            }
        }
        return null;
    }

    static class Node {
        public int val;
        public List<Node> children;

        public Node(int _val) {
            val = _val;
            children = new ArrayList<>();
        }

        public Node(int _val, List<Node> _children) {
            val = _val;
            children = _children;
        }

        @Override
        public String toString() {
            return "Node{" +
                    "val=" + val + '}';
        }
    }

    static class Pair {
        int count;
        Node node;

        public Pair(int val, Node node) {
            this.count = val;
            this.node = node;
        }

        @Override
        public String toString() {
            return "Pair{" +
                    "val=" + count +
                    ", node=" + node +
                    '}';
        }
    }
}
