package Leetcode3;

import java.util.Arrays;

public class ReplaceElementsWithGreatestElementOnRightSide {

    /*
    Given an array arr, replace every element in that array with the greatest element
    among the elements to its right, and replace the last element with -1.
     */
    public static void main(String[] args) {
        assert Arrays.equals(replaceElements(new int[]{17, 18, 5, 4, 6, 1}), new int[]{18, 6, 6, 6, 1, -1});
        assert Arrays.equals(replaceElements(new int[]{400}), new int[]{-1});
    }

    public static int[] replaceElements(int[] arr) {
        int[] right = new int[arr.length];
        int max = -1;
        for (int i = arr.length - 1; i >= 0; i--) {
            right[i] = max;
            max = Math.max(max, arr[i]);
        }
        return right;
    }

}
