package Leetcode3;

import Leetcode.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class AllElementsInTwoBinarySearchTrees {

    public static void main(String[] args) {
        TreeNode root1 = new TreeNode(2);
        root1.left = new TreeNode(1);
        root1.right = new TreeNode(4);
        //        2
        //      /   \
        //     1     4
        TreeNode root2 = new TreeNode(1);
        root2.left = new TreeNode(0);
        root2.right = new TreeNode(3);
        //        1
        //      /   \
        //     0     3
        System.out.println(getAllElements(root1, root2)); // [0,1,1,2,3,4]
    }

    public static List<Integer> getAllElements(TreeNode root1, TreeNode root2) {
        List<Integer> l1 = new ArrayList<>();
        inorder(root1, l1);
        List<Integer> l2 = new ArrayList<>();
        inorder(root2, l2);
        return merge(l1, l2);
    }

    private static void inorder(TreeNode root, List<Integer> l) {
        if (root == null) return;
        inorder(root.left, l);
        l.add(root.val);
        inorder(root.right, l);
    }

    private static List<Integer> merge(List<Integer> a, List<Integer> b) {
        List<Integer> result = new ArrayList<>();
        int i = 0, j = 0;
        while (i < a.size() && j < b.size()) {
            if (a.get(i).equals(b.get(j))) {
                result.add(a.get(i));
                result.add(b.get(j));
                i++;
                j++;
            } else if (a.get(i) > b.get(j)) {
                result.add(b.get(j));
                j++;
            } else {
                result.add(a.get(i));
                i++;
            }
        }
        while (i < a.size()) {
            result.add(a.get(i++));
        }
        while (j < b.size()) {
            result.add(b.get(j++));
        }
        return result;
    }

}
