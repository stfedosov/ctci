package Leetcode3;

public class ReversePairs {

    /*
    Given an integer array nums, return the number of reverse pairs in the array.

    A reverse pair is a pair (i, j) where 0 <= i < j < nums.length and nums[i] > 2 * nums[j].
     */
    public static void main(String[] args) {
        System.out.println(reversePairs(new int[]{1, 3, 2, 3, 1})); // 2;
        System.out.println(reversePairs(new int[]{2, 4, 3, 5, 1})); // 3;
    }

    public static int reversePairs(int[] nums) {
        int n = nums.length;
        return mergeSort(nums, 0, n - 1);
    }

    private static int mergeSort(int[] nums, int i, int j) {
        if (i >= j) {
            return 0;
        }
        int mid = i + (j - i) / 2;
        int sum = 0;
        sum += mergeSort(nums, i, mid);
        sum += mergeSort(nums, mid + 1, j);
        sum += merge(nums, i, mid, mid + 1, j);
        return sum;
    }

    private static int merge(int[] nums, int start1, int end1, int start2, int end2) {
        int[] temp = new int[end2 - start1 + 1];
        int start = start1, i = 0, sum = 0, s1 = start1, s2 = start2;
        while (s1 <= end1 && s2 <= end2) {
            if ((long) nums[s1] > (long) (nums[s2]) * 2) {
                sum += end2 - s2 + 1;
                s1++;
            } else {
                s2++;
            }
        }
        while (start1 <= end1 && start2 <= end2) {
            if (nums[start1] > nums[start2]) {
                temp[i++] = nums[start1++];
            } else {
                temp[i++] = nums[start2++];
            }
        }
        while (start1 <= end1) {
            temp[i++] = nums[start1++];
        }
        while (start2 <= end2) {
            temp[i++] = nums[start2++];
        }
        for (int k : temp) {
            nums[start++] = k;
        }
        return sum;
    }

}
