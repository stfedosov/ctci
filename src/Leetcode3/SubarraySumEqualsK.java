package Leetcode3;

public class SubarraySumEqualsK {

    public static void main(String[] args) {
        assert subarraySumToTarget(new int[]{23, 5, 4, 7, 2, 11}, 20);
        assert subarraySumToTarget(new int[]{1, 3, 5, 23, 2}, 8);
        assert !subarraySumToTarget(new int[]{1, 3, 5, 23, 2}, 7);
        assert subarraySumToTarget(new int[]{1, 1, 1, 1, 2}, 2);
        assert subarraySumToTarget(new int[]{1, 1, 1, 1, 2}, 6);
        assert !subarraySumToTarget(new int[]{1, 1, 1, 1, 2}, 8);
        assert !subarraySumToTarget(new int[]{1}, 8);
        assert subarraySumToTarget(new int[]{1, 2, 3, 4}, 5);
        assert subarraySumToTarget(new int[]{1, 2, 3, 4}, 7);
        assert subarraySumToTarget(new int[]{1, 2, 3, 4}, 9);
    }

    private static boolean subarraySumToTarget(int[] A, int T) {
        int sum = 0, j = 0;
        for (int i = 0; i < A.length; i++) {
            while (j < A.length && sum < T) {
                sum += A[j];
                j++;
            }
            if (sum == T) {
                return true;
            }
            sum -= A[i];
        }
        return false;
    }

}
