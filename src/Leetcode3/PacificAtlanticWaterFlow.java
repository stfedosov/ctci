package Leetcode3;

import java.util.ArrayList;
import java.util.List;

public class PacificAtlanticWaterFlow {

    public static void main(String[] args) {

    }

    public static List<List<Integer>> pacificAtlantic(int[][] heights) {
        boolean[][] visited1 = new boolean[heights.length][heights[0].length];
        for (int i = 0; i < heights.length; i++) {
            for (int j = 0; j < heights[i].length; j++) {
                if (i == 0 || j == 0) {
                    dfs(heights, i, j, visited1, heights[i][j]);
                }
            }
        }
        boolean[][] visited2 = new boolean[heights.length][heights[0].length];
        for (int i = 0; i < heights.length; i++) {
            for (int j = 0; j < heights[i].length; j++) {
                if (i == heights.length - 1 || j == heights[i].length - 1) {
                    dfs(heights, i, j, visited2, heights[i][j]);
                }
            }
        }
        List<List<Integer>> res = new ArrayList<>();
        for (int i = 0; i < heights.length; i++) {
            for (int j = 0; j < heights[i].length; j++) {
                if (visited1[i][j] && visited2[i][j]) {
                    res.add(List.of(i, j));
                }
            }
        }
        return res;
    }

    static int[][] dirs = new int[][]{{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

    private static void dfs(int[][] heights, int i, int j, boolean[][] visited, int prev) {
        visited[i][j] = true;
        for (int[] dir : dirs) {
            int newI = i + dir[0], newJ = j + dir[1];
            if (isValid(heights.length, heights[0].length, newI, newJ)
                    && !visited[newI][newJ]
                    && heights[newI][newJ] >= prev) {
                dfs(heights, newI, newJ, visited, heights[newI][newJ]);
            }
        }
    }

    private static boolean isValid(int m, int n, int i, int j) {
        return i >= 0 && i < m && j >= 0 && j < n;
    }

}
