package Leetcode3;

import java.util.Map;
import java.util.Stack;
import java.util.function.BiFunction;

public class ExpressionTree {

    public static void main(String[] args) {
        TreeBuilder obj = new TreeBuilder();
        Node expTree = obj.buildTree(new String[]{"3", "4", "+", "2", "*", "7", "/"});
        int ans = expTree.evaluate();
        System.out.println(ans);
        expTree = obj.buildTree(new String[]{"4", "5", "2", "7", "+", "-", "*"});
        ans = expTree.evaluate();
        System.out.println(ans);
    }

    static class Node {
        static final Map<String, BiFunction<Integer, Integer, Integer>> OPERATIONS =
                Map.ofEntries(
                        Map.entry("+", (op1, op2) -> op1 + op2),
                        Map.entry("-", (op1, op2) -> op1 - op2),
                        Map.entry("*", (op1, op2) -> op1 * op2),
                        Map.entry("/", (op1, op2) -> op1 / op2)
                );

        Node(String val, Node left, Node right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }

        final Node left;
        final Node right;
        final String val;

        public int evaluate() {
            if (OPERATIONS.containsKey(val)) {
                return OPERATIONS.get(val).apply(left.evaluate(), right.evaluate());
            }
            return Integer.parseInt(val);
        }
    }

    static class TreeBuilder {
        Node buildTree(String[] postfix) {
            Stack<Node> stack = new Stack<>();
            for (String token : postfix) {
                if (Node.OPERATIONS.containsKey(token)) {
                    // operator
                    Node o2 = stack.pop();
                    Node o1 = stack.pop();
                    stack.push(new Node(token, o1, o2));
                } else {
                    // operand
                    stack.push(new Node(token, null, null));
                }
            }
            return stack.pop();
        }
    }


/**
 * Your TreeBuilder object will be instantiated and called as such:
 * TreeBuilder obj = new TreeBuilder();
 * Node expTree = obj.buildTree(postfix);
 * int ans = expTree.evaluate();
 */

}
