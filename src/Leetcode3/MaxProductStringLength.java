package Leetcode3;

public class MaxProductStringLength {

    public static void main(String[] args) {
        System.out.println(maxProduct(new String[]{"abcw", "baz", "foo", "bar", "xtfn", "abcdef"}));
        System.out.println(maxProduct(new String[]{"a", "ab", "abc", "d", "cd", "bcd", "abcd"}));
    }

    public static int maxProduct(String[] words) {
        int n = words.length;
        int[] masks = new int[n];
        int[] lens = new int[n];

        for (int i = 0; i < n; ++i) {
            int bitmask = 0;
            for (char c : words[i].toCharArray()) {
                // add bit number bit_number in bitmask
                bitmask |= 1 << (c - 'a');
            }
            masks[i] = bitmask;
            lens[i] = words[i].length();
        }

        int maxVal = 0;
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if ((masks[i] & masks[j]) == 0)
                    maxVal = Math.max(maxVal, lens[i] * lens[j]);
            }
        }

        return maxVal;
    }

}
