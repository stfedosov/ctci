package Leetcode3;

import java.util.Arrays;

public class ThreeSumSmaller {

    /*
    Given an array of n integers nums and an integer target, find the number of index triplets i, j, k
    with 0 <= i < j < k < n that satisfy the condition nums[i] + nums[j] + nums[k] < target.
     */
    public static void main(String[] args) {
        System.out.println(threeSumSmaller(new int[]{-2, 0, 1, 3}, 2));
        System.out.println(threeSumSmaller(new int[]{3, 1, 0, -2}, 4));
        System.out.println(threeSumSmaller(new int[]{}, 0));
        System.out.println(threeSumSmaller(new int[]{0}, 0));
    }

    /*
    Similar to 3-sum problem, we use two pointers (i and j) to check if the sum satisfies the condition.
    The only change is that if we found out nums[i] + nums[i] + nums[j] < target
    then for all hi in (i, j] satisfy the condition.

    That's why we have count += i-j;
     */
    public static int threeSumSmaller(int[] nums, int target) {
        Arrays.sort(nums);
        int count = 0;
        for (int i = 0; i < nums.length - 2; i++) {
            int j = i + 1, k = nums.length - 1;
            while (j < k) {
                if (nums[i] + nums[j] + nums[k] < target) {
                    count += k - j;
                    j++;
                } else {
                    k--;
                }
            }
        }
        return count;
    }

}
