package Leetcode3;

public class MinFlipsToMonotoneIncreasing {

    /*
    A binary string is monotone increasing if it consists of some number of 0's (possibly none),
    followed by some number of 1's (also possibly none).

    You are given a binary string s. You can flip s[i] changing it from 0 to 1 or from 1 to 0.

    Return the minimum number of flips to make s monotone increasing.
     */
    public static void main(String[] args) {
        System.out.println(minFlipsMonoIncr("00110")); // 1: 00111
        System.out.println(minFlipsMonoIncr("010110")); // 2: 000111
    }

    public static int minFlipsMonoIncr(String s) {
        int[] left = new int[s.length()], right = new int[s.length()];
        for (int i = 1; i < s.length(); i++) {
            left[i] = left[i - 1] + (s.charAt(i - 1) == '1' ? 1 : 0);
        }
        for (int i = s.length() - 2; i >= 0; i--) {
            right[i] = right[i + 1] + (s.charAt(i + 1) == '0' ? 1 : 0);
        }
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < s.length(); i++) {
            min = Math.min(min, left[i] + right[i]);
        }
        return min;
    }

}
