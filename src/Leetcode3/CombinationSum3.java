package Leetcode3;

import java.util.ArrayList;
import java.util.List;

public class CombinationSum3 {

    public static void main(String[] args) {
        System.out.println(combinationSum3(3, 7)); // [[1,2,4]]
    }

    /*
    Find all valid combinations of k numbers that sum up to n such that the following conditions are true:

    - Only numbers 1 through 9 are used.
    - Each number is used at most once.

    Return a list of all possible valid combinations.
    The list must not contain the same combination twice, and the combinations may be returned in any order.
     */
    public static List<List<Integer>> combinationSum3(int k, int n) {
        List<List<Integer>> result = new ArrayList<>();
        dfs(k, n, 1, result, new ArrayList<>());
        return result;
    }

    // The number of exploration we need to make in the worst case would be P(9, K) = 9!/(9-K)!
    private static void dfs(int k, int n, int idx, List<List<Integer>> result, List<Integer> tmp) {
        // O(K) time to make a copy of combination.
        if (n == 0 && tmp.size() == k) {
            result.add(new ArrayList<>(tmp));
            return;
        }
        for (int i = idx; i <= 9; i++) {
            tmp.add(i);
            dfs(k, n - i, i + 1, result, tmp);
            tmp.remove(tmp.size() - 1);
        }
    }

}
