package Leetcode3;

public class CountNumberOfTeams {

    public static void main(String[] args) {
        System.out.println(numTeams(new int[]{2, 5, 3, 4, 1})); // 3
        System.out.println(numTeams(new int[]{1, 2, 3, 4})); // 4
        System.out.println(numTeams(new int[]{2, 1, 3})); // 0
    }

    // First, we calculate this case rating[i] > rating[j] > rating[k] given that i > j > k.
    // Then, calculate the case rating[i] < rating[j] < rating[k].
    // Define dp[i]: How many items in rating from 0 to i - 1 are smaller than rating[i].
    // For every time we found rating[i] > rating[j], we accumulate dp[j].
    // Here, dp[j] means how many rating[k] exist from 0 to j - 1.
    // That is, how many cases satisfy rating[i] > rating[j] > rating[k].
    // --------------
    // Time: O(N^2), Space: O(N)
    public static int numTeams(int[] rating) {
        int n = rating.length, ans = 0;
        int[] dp = new int[n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < i; j++) {
                if (rating[i] > rating[j]) {
                    dp[i]++;
                    ans += dp[j];
                }
            }
        }
        dp = new int[n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < i; j++) {
                if (rating[i] < rating[j]) {
                    dp[i]++;
                    ans += dp[j];
                }
            }
        }
        return ans;
    }

}
