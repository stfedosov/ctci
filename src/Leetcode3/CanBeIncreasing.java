package Leetcode3;

public class CanBeIncreasing {

    public static void main(String[] args) {
        assert canBeIncreasing(new int[]{1, 2, 10, 5, 7});
        assert !canBeIncreasing(new int[]{2, 3, 1, 2});
        assert !canBeIncreasing(new int[]{1, 1, 1});
    }

    public static boolean canBeIncreasing(int[] nums) {
        int count = 0;
        int idx = 0;
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] >= nums[i + 1]) {
                count++;
                idx = i;
            }
        }
        if (count > 1) {
            return false;
        } else if (count == 1) {
            if (idx == 0 || idx == nums.length - 2) return true;
            return nums[idx + 1] > nums[idx - 1] || nums[idx + 2] > nums[idx];
        }
        return true;
    }

}
