package Leetcode3;

public class SubArraySum {

    /*
    Given an array of positive integers arr, return the sum of all possible odd-length subarrays of arr.

    A subarray is a contiguous subsequence of the array.

    Example 1:

    Input: arr = [1,4,2,5,3]
    Output: 58
    Explanation: The odd-length subarrays of arr and their sums are:
    [1] = 1
    [4] = 4
    [2] = 2
    [5] = 5
    [3] = 3
    [1,4,2] = 7
    [4,2,5] = 11
    [2,5,3] = 10
    [1,4,2,5,3] = 15
    If we add all these together we get 1 + 4 + 2 + 5 + 3 + 7 + 11 + 10 + 15 = 58
     */
    public static void main(String[] args) {
        System.out.println(sumOddLengthSubarrays(new int[]{1, 4, 2, 5, 3}) == 58);
    }

    // prefix sum approach, Time O(n^2), Space O(n)
    public static int sumOddLengthSubarrays(int[] arr) {
        int[] accum = new int[arr.length + 1];
        for (int i = 1; i <= arr.length; i++) {
            accum[i] = accum[i - 1] + arr[i - 1];
        }
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j <= arr.length; j++) {
                if ((j - i) % 2 != 0) {
                    sum += accum[j] - accum[i];
                }
            }
        }
        return sum;
    }

    // Time O(n^2), Space O(1)
    public static int sumOddLengthSubarrays2(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i]; // always add current num
            int tempSum = arr[i];
            for (int j = i + 1; j < arr.length; j++) {
                tempSum += arr[j]; // temp variable to store variables at odd + even indexes
                if ((j - i + 1) % 2 != 0) { // add to the sum when index is odd such as 1,3,5 ...
                    sum += tempSum;
                }
            }
        }
        return sum;
    }


}
