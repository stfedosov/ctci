package Leetcode3;

public class MinimumTimeToCompleteTrips {

    public static void main(String[] args) {
        System.out.println(minimumTime(new int[]{1, 2, 3}, 5)); // 3
    }

    // You are given an array time where time[i] denotes the time taken by the i-th bus to complete one trip.
    // You are also given an integer totalTrips, which denotes the number of trips all buses should make in total.
    // Return the minimum time required for all buses to complete at least totalTrips trips.
    public static long minimumTime(int[] time, int totalTrips) {
        long l = 0, r = Long.MAX_VALUE;
        while (l <= r) {
            long mid = l + (r - l) / 2;
            //  mid is the time to check if it is possible to complete totalTrips
            if (isPossible(time, totalTrips, mid)) {
                // since we're aiming for minimum time we need to check
                // if there is a value that is less than current mid that satisfies our requirement
                r = mid - 1;
            } else {
                l = mid + 1;
            }
        }
        return l;
    }

    //  is it possible to complete total trips in given newTime
    private static boolean isPossible(int[] time, int total, long newTime) {
        long currTrips = 0;
        for (int currentTime : time) {
            currTrips += newTime / currentTime;
            if (currTrips >= total) {
                return true;
            }
        }
        return false;
    }

}
