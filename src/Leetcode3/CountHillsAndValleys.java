package Leetcode3;

public class CountHillsAndValleys {

    /*
    An index i is part of a hill in nums if the closest non-equal neighbors of i are smaller than nums[i].
    Similarly, an index i is part of a valley in nums if the closest non-equal neighbors of i are larger than nums[i].
    Adjacent indices i and j are part of the same hill or valley if nums[i] == nums[j].

    Note that for an index to be part of a hill or valley, it must have a non-equal neighbor on both the left and right of the index.

    Return the number of hills and valleys in nums.
     */
    public static void main(String[] args) {
        System.out.println(countHillValley(new int[]{2, 4, 1, 1, 6, 5})); // 3
    }

    public static int countHillValley(int[] nums) {
        int last = nums[0];
        int hills = 0, valleys = 0;

        for (int i = 1; i < nums.length - 1; i++) {
            if (nums[i] == last) {
                last = nums[i];
                continue;
            }

            if (nums[i] > last && nums[i] > nums[i + 1]) {
                hills++;
                last = nums[i];
            } else if (nums[i] < last && nums[i] < nums[i + 1]) {
                valleys++;
                last = nums[i];
            }

        }

        return hills + valleys;
    }

}
