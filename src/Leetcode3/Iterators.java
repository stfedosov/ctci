package Leetcode3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Iterators {

    public static void main(String[] args) {
        Iterator<Integer> it1 = List.of(2, 5, 1, 0).iterator(),
                it2 = List.of(1).iterator(),
                it3 = List.of(9, 2, 10, 4, 11).iterator();
        Iterators iterators = new Iterators(List.of(it1, it2, it3));
        List<Integer> result = new ArrayList<>();
        while (iterators.hasNext()) {
            result.add(iterators.next());
        }
        assert result.equals(List.of(2, 1, 9, 5, 2, 1, 10, 0, 4, 11));
    }

    private final Queue<Iterator<Integer>> q;

    public Iterators(List<Iterator<Integer>> iteratorList) {
        this.q = new LinkedList<>();
        for (Iterator<Integer> it : iteratorList) {
            q.offer(it);
        }
    }

    public int next() {
        Iterator<Integer> next = q.poll();
        int res = next.next();
        if (next.hasNext()) {
            q.offer(next);
        }
        return res;
    }

    public boolean hasNext() {
        return !q.isEmpty();
    }

}
