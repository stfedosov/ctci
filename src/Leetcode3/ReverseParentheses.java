package Leetcode3;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class ReverseParentheses {

    public static void main(String[] args) {
        System.out.println(reverseParentheses("(abcd)")); //dcba
        System.out.println(reverseParentheses("(u(love)i)")); //iloveu
    }

    // recursive
    public static String reverseParentheses(String s) {
        Queue<Character> q = new LinkedList<>();
        for (char c : s.toCharArray()) {
            q.offer(c);
        }
        return dfs(q);
    }

    private static String dfs(Queue<Character> q) {
        if (q.isEmpty()) return "";
        StringBuilder sb = new StringBuilder();
        while (!q.isEmpty()) {
            Character c = q.poll();
            if (c != '(' && c != ')') {
                sb.append(c);
            } else if (c == '(') {
                sb.append(dfs(q));
            } else { // ')' closed brace
                return sb.reverse().toString();
            }
        }
        return sb.toString();
    }

    // non-recursive
    public static String reverseParentheses2(String s) {
        Stack<StringBuilder> stack = new Stack<>();
        stack.push(new StringBuilder());
        for (char c : s.toCharArray()) {
            if (c == '(') {
                stack.push(new StringBuilder());
            } else if (c == ')') {
                StringBuilder last = stack.pop();
                stack.peek().append(last.reverse());
            } else {
                stack.peek().append(c);
            }
        }
        return stack.pop().toString();
    }
}
