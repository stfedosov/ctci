package Leetcode3;

import java.util.HashMap;
import java.util.Map;

public class NumberOfPairsOfStringsWithConcatenationEqualsToTarget {

    // Given an array of digit strings nums and a digit string target,
    // return the number of pairs of indices (i, j) (where i != j)
    // such that the concatenation of nums[i] + nums[j] equals target.
    //
    // Input: nums = ["777","7","77","77"], target = "7777"
    // Output: 4
    // Explanation: Valid pairs are:
    //  (0, 1): "777" + "7"
    //  (1, 0): "7" + "777"
    //  (2, 3): "77" + "77"
    //  (3, 2): "77" + "77"

    public static void main(String[] args) {
        System.out.println(numOfPairsBruteForce(new String[]{"777","7","77","77"}, "7777"));
    }

    // O(n), where n = Math.max(nums.length, target.length)
    public static int numOfPairsOptimal(String[] nums, String target) {
        Map<String, Integer> map = new HashMap<>();
        for (String num : nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }

        int count = 0;
        String s1, s2;
        for (int i = 1; i < target.length(); i++) {
            s1 = target.substring(0, i);
            s2 = target.substring(i);
            if (map.containsKey(s1) && map.containsKey(s2)) {
                if (s1.equals(s2)) count += map.get(s1) * (map.get(s1) - 1);
                else count += map.get(s1) * map.get(s2);
            }
        }
        return count;
    }

    // O(n^3), n^2 is the double for loop and n is the strings processing
    public static int numOfPairsBruteForce(String[] nums, String target) {
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            StringBuilder sb1 = new StringBuilder(nums[i]);
            for (int j = i + 1; j < nums.length; j++) {
                int length = sb1.length();
                sb1.append(nums[j]);
                if (sb1.toString().equals(target)) count++;
                sb1.setLength(length);
                if ((nums[j] + sb1).equals(target)) count++;
            }
        }
        return count;
    }

}
