package Leetcode3;

public class MaxSumTwoNoOverlap {

    /*
    Given an integer array nums and two integers firstLen and secondLen,
    return the maximum sum of elements in two non-overlapping subarrays with lengths firstLen and secondLen.

    The array with length firstLen could occur before or after the array with length secondLen,
    but they have to be non-overlapping.
     */
    public static void main(String[] args) {
        maxSumTwoNoOverlap(new int[]{1,2,3,4,5,6}, 2, 2);
    }

    // O(n) time and space complexity
    public static int maxSumTwoNoOverlap(int[] A, int L, int M) {
        // L and M could be at left or right
        // so we need to calculate the both to get the max non-overlapping sum of entire array
        return Math.max(calculate(A, L, M), calculate(A, M, L));
    }

    private static int calculate(int[] A, int L, int M) {
        // calculate the prefix sum from A[0] to A[i]
        int[] prefixSum = new int[A.length];
        prefixSum[0] = A[0];
        for (int i = 1; i < A.length; i++) {
            prefixSum[i] = prefixSum[i - 1] + A[i];
        }

        // calculate the max sum with length L with rightmost position at A[i],
        // A[i] doesn't have to be included
        int[] leftSum = new int[A.length];
        leftSum[L - 1] = prefixSum[L - 1];
        for (int i = L; i < A.length; i++) {
            leftSum[i] = Math.max(leftSum[i - 1], prefixSum[i] - prefixSum[i - L]);
        }

        // calculate the suffix sum from A[i] to A[len-1]
        int[] suffixSum = new int[A.length];
        suffixSum[A.length - 1] = A[A.length - 1];
        for (int i = A.length - 2; i >= 0; i--) {
            suffixSum[i] = suffixSum[i + 1] + A[i];
        }

        // calculate the max sum sum with length M with leftmost position at A[i],
        // A[i] doesn't have to be included
        int[] rightSum = new int[A.length];
        rightSum[A.length - M] = suffixSum[A.length - M];
        for (int i = A.length - M - 1; i >= 0; i--) {
            rightSum[i] = Math.max(rightSum[i + 1], suffixSum[i] - suffixSum[i + M]);
        }

        // now we have all the data for max sum with length L from the left
        // and max sum with length M from the right
        // just iterate and add them up to find the max non-overlapping sum
        // note the i + 1 index is for non-overlapping
        int res = Integer.MIN_VALUE;
        for (int i = L - 1; i <= A.length - M - 1; i++) {
            res = Math.max(leftSum[i] + rightSum[i + 1], res);
        }

        return res;
    }

}
