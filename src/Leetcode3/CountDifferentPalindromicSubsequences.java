package Leetcode3;

import java.util.HashSet;
import java.util.Set;

public class CountDifferentPalindromicSubsequences {

    public static void main(String[] args) {
        System.out.println(countPalindromicSubsequences("bccb"));
    }

    public static int countPalindromicSubsequences(String S) {
        Set<String> seen = new HashSet<>();
        palindromicSubsequences("", 0, S, seen);
        return seen.size() - 1;
    }

    private static void palindromicSubsequences(String cur, int i, String s, Set<String> seen) {
        if (i == s.length()) {
            if (isPal(cur)) seen.add(cur);
            return;
        }
        palindromicSubsequences(cur, i + 1, s, seen);
        palindromicSubsequences(cur + s.charAt(i), i + 1, s, seen);
    }

    private static boolean isPal(String s) {
        int l = 0, r = s.length() - 1;
        while (l < r && s.charAt(l) == s.charAt(r)) {
            l++;
            r--;
        }
        return l >= r;
    }

}
