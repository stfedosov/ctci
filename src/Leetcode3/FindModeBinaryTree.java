package Leetcode3;

import Leetcode.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FindModeBinaryTree {

    // mode - the most frequently occurred element
    public static void main(String[] args) {
        TreeNode root = new TreeNode(2);
        TreeNode left = new TreeNode(1);
        root.left = left;
        left.left = new TreeNode(1);
        TreeNode right = new TreeNode(3);
        right.left = new TreeNode(3);
        root.right = right;
        //      1
        //       \
        //        2
        //       /
        //      2
        System.out.println(Arrays.toString(findMode(root))); // [2]
    }

    private static int maxCount;
    private static int currentCount;
    private static int prev = Integer.MIN_VALUE;

    private static final List<Integer> totalList = new ArrayList<>();

    public static int[] findMode(TreeNode root) {
        inOrderTraversal(root);
        int[] result = new int[totalList.size()];
        int pos = 0;
        for (int i : totalList) {
            result[pos++] = i;
        }
        return result;
    }

    // We know that inorder Traversal of BST will contain all the same elements together
    // i.e. the inorder traversal will result in a sorted array.
    // so we can calculate the mode with given information
    private static void inOrderTraversal(TreeNode root) {
        if (root == null) {
            return;
        }
        inOrderTraversal(root.left);
        processCurrentNode(root);
        inOrderTraversal(root.right);
    }

    private static void processCurrentNode(TreeNode root) {
        if (prev != root.val) {
            currentCount = 0;
            prev = root.val;
        }
        currentCount++;
        if (currentCount > maxCount) {
            maxCount = currentCount;
            totalList.clear();
            totalList.add(root.val);
        } else if (currentCount == maxCount) {
            totalList.add(root.val);
        }
    }

}
