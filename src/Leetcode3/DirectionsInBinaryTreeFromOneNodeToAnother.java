package Leetcode3;

import Leetcode.TreeNode;

public class DirectionsInBinaryTreeFromOneNodeToAnother {

    /*
    In Binary Tree find the shortest path starting from node s and ending at node t.
    Generate step-by-step directions of such path as a string consisting of only the uppercase letters
    'L', 'R' and 'U'.
    Each letter indicates a specific direction:

    'L' means to go from a node to its left child node.
    'R' means to go from a node to its right child node.
    'U' means to go from a node to its parent node.
     */
    public String getDirections(TreeNode root, int startValue, int destValue) {
        TreeNode lcaNode = findLca(root, startValue, destValue);

        var path1 = new StringBuilder();
        path(lcaNode, startValue, path1, true);
        var path2 = new StringBuilder();
        path(lcaNode, destValue, path2, false);
        path1.append(path2.reverse());
        return path1.toString();
    }

    public boolean path(TreeNode root, int target, StringBuilder currPath, boolean goingUp) {
        if (root == null) return false;
        if (root.val == target) return true;

        if (path(root.left, target, currPath, goingUp))
            if (goingUp) currPath.append("U");
            else currPath.append("L");
        else if (path(root.right, target, currPath, goingUp))
            if (goingUp) currPath.append("U");
            else currPath.append("R");

        return currPath.length() > 0;
    }

    private TreeNode findLca(TreeNode root, int startValue, int destValue) {
        if (root == null || root.val == startValue || root.val == destValue) return root;
        TreeNode left = findLca(root.left, startValue, destValue);
        TreeNode right = findLca(root.right, startValue, destValue);
        return (left != null && right != null) ? root : left == null ? right : left;
    }

}
