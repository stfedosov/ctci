package Leetcode3;

public class MaxLengthOfSubarrayPositiveProduct {

    public static void main(String[] args) {
        System.out.println(getMaxLen(new int[]{1, -2, -3, 4}));
        System.out.println(getMaxLen(new int[]{0, 1, -2, -3, -4}));
    }

    public static int getMaxLen(int[] nums) {
        int max = 0;
        int pos_count = 0, neg_count = 0;
        for (int num : nums) {
            if (num == 0) {
                pos_count = 0;
                neg_count = 0;
            } else if (num > 0) {
                if (neg_count > 0) {
                    neg_count++;
                }
                // Upon encountering a positive value, increase the length of the positive subarray
                pos_count++;
            } else {
                // Upon encountering a negative value, the lengths of the positive and negative
                // subarrays need to be swapped.
                int tmp = pos_count;
                pos_count = neg_count > 0 ? neg_count + 1 : 0;
                neg_count = tmp + 1;
            }
            max = Math.max(max, pos_count);
        }
        return max;
    }

}
