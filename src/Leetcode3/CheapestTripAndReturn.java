package Leetcode3;

public class CheapestTripAndReturn {

    public static void main(String[] args) {
        System.out.println(cheapest(new int[]{1, 4, 6, 7, 8}, new int[]{2, 5, 10, 8, 6})); // 3
        System.out.println(cheapest(new int[]{6, 3, 6, 7, 8}, new int[]{3, 5, 10, 4, 6})); // 7
    }

    /*
    Given 2 arrays of cost of departure and return [1,4,6,7,8] and [2,5,10,8,6]
    Find the min cost of round travel. Same day round travel can be possible.
     */
    private static int cheapest(int[] tripCost, int[] returnCost) {
        int minReturnCostFromTheEnd = Integer.MAX_VALUE, minTotal = Integer.MAX_VALUE;
        for (int i = tripCost.length - 1; i >= 0; i--) {
            minReturnCostFromTheEnd = Math.min(minReturnCostFromTheEnd, returnCost[i]);
            minTotal = Math.min(minTotal, minReturnCostFromTheEnd + tripCost[i]);
        }
        return minTotal;
    }

}
