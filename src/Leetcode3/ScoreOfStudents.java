package Leetcode3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

public class ScoreOfStudents {

    public static void main(String[] args) {
        scoreOfStudents("7+3*1*2", new int[]{20, 13, 42});
    }

    public static int scoreOfStudents(String s, int[] answers) {
        int rightAnswer = calcRightAnswer(s);
        Set<Integer> potentialAnswers = new HashSet<>(calcAllPossibleAnswers(s));
        int sum = 0;
        Map<Integer, Integer> map = new HashMap<>();
        for (int ans : answers) map.put(ans, map.getOrDefault(ans, 0) + 1);
        for (int k : map.keySet()) {
            if (k == rightAnswer) sum += map.get(k) * 5;
            else if (potentialAnswers.contains(k)) sum += map.get(k) * 2;
        }
        return sum;
    }

    private static int calcRightAnswer(String s) {
        s = s.replaceAll(" ", "");
        Queue<Character> q = new LinkedList<>();
        for (char c : s.toCharArray()) q.offer(c);
        q.offer('+');
        return helper(q);
    }

    private static int helper(Queue<Character> q) {
        Stack<Integer> stack = new Stack<>();
        int num = 0, sum = 0;
        char sign = '+';
        while (!q.isEmpty()) {
            char c = q.poll();
            if (Character.isDigit(c)) {
                num = num * 10 + (c - '0');
            } else {
                if (sign == '+') {
                    stack.push(num);
                } else if (sign == '-') {
                    stack.push(-num);
                } else if (sign == '*') {
                    stack.push(stack.pop() * num);
                }
                num = 0;
                sign = c;
            }
        }
        while (!stack.isEmpty()) sum += stack.pop();
        return sum;
    }

    private static List<Integer> calcAllPossibleAnswers(String s) {
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '+' || c == '-' || c == '*') {
                List<Integer> list1 = calcAllPossibleAnswers(s.substring(0, i));
                List<Integer> list2 = calcAllPossibleAnswers(s.substring(i + 1));
                for (int x1 : list1) {
                    for (int x2 : list2) {
                        if (c == '+') {
                            res.add(x1 + x2);
                        }
                        if (c == '-') {
                            res.add(x1 - x2);
                        }
                        if (c == '*') {
                            res.add(x1 * x2);
                        }
                    }
                }
            }
        }
        if (res.isEmpty()) res.add(Integer.parseInt(s));
        return res;
    }

}
