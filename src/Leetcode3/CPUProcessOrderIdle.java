package Leetcode3;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class CPUProcessOrderIdle {

    /*
    Single threaded CPU
    You are given n tasks labeled from 0 to n - 1 represented by a 2D integer array tasks,
    where tasks[i] = [enqueueTimei, processingTimei] means that the i-th task will be available to process
    at enqueueTimei and will take processingTimei to finish processing.

    Return the order in which the CPU will process the tasks.
     */

    public static void main(String[] args) {
        System.out.println(Arrays.toString(getOrder(new int[][]{{5, 2}, {7, 2}, {9, 4}, {6, 3}, {5, 10}, {1, 1}})));
        System.out.println(Arrays.toString(getOrder(new int[][]{{1, 2}, {2, 4}, {3, 2}, {4, 1}})));
        System.out.println(Arrays.toString(getOrder(new int[][]{{7, 10}, {7, 12}, {7, 5}, {7, 4}, {7, 2}})));
    }

    public static int[] getOrder(int[][] tasks) {
        Queue<int[]> tasksQueue = new PriorityQueue<>(Comparator.comparingInt(a -> a[0])),
                nextTaskQueue = new PriorityQueue<>((a, b) -> a[1] - b[1] == 0 ? a[2] - b[2] : a[1] - b[1]);
        for (int i = 0; i < tasks.length; i++) {
            tasksQueue.offer(new int[]{tasks[i][0], tasks[i][1], i});
        }
        int[] res = new int[tasks.length];
        int i = 0, startTime = tasksQueue.peek()[0];
        while (!tasksQueue.isEmpty() || !nextTaskQueue.isEmpty()) {
            while (!tasksQueue.isEmpty() && tasksQueue.peek()[0] <= startTime) {
                nextTaskQueue.offer(tasksQueue.poll());
            }
            if (!nextTaskQueue.isEmpty()) {
                int[] polledTask = nextTaskQueue.poll();
                startTime += polledTask[1];
                res[i++] = polledTask[2];
            } else {
                startTime = tasksQueue.peek()[0];
            }
        }
        return res;
    }

}
