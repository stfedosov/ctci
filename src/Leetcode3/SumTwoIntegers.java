package Leetcode3;

public class SumTwoIntegers {

    public static void main(String[] args) {
        System.out.println(sum(1, 2));
        System.out.println(sum(5, 3));
    }

    public static int sum(int num1, int num2) {
        while (num2 != 0) {
            int carryOver = (num1 & num2) << 1;
            // carry over is what we have when we sum up 1 and 1, we "leave" the remaining 1 in memory.
            // we shift it because it should be added to the number on the left.
            num1 ^= num2; // Now num1 is the sum without carry over, XOR is just an equivalent to adding numbers.
            num2 = carryOver; // num2 is the carry over now, we need to work with the carry on our next iteration.
        }
        return num1;
    }

}
